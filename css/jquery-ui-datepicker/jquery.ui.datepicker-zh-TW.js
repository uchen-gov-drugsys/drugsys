/* Chinese initialisation for the jQuery UI date picker plugin. */
/* Written by Ressol (ressol@gmail.com). */
jQuery(function($){
    var old_generateMonthYearHeader = $.datepicker._generateMonthYearHeader;
    var old_get = $.datepicker._get;
    var old_CloseFn = $.datepicker._updateDatepicker;
    $.extend($.datepicker, {
        _generateMonthYearHeader: function (inst, drawMonth, drawYear, minDate, maxDate, secondary, monthNames, monthNamesShort) {
            var htmlYearMonth = old_generateMonthYearHeader.apply(this, [inst, drawMonth, drawYear, minDate, maxDate, secondary, monthNames, monthNamesShort]);
            if ($(htmlYearMonth).find(".ui-datepicker-year").length > 0) {
                htmlYearMonth = $(htmlYearMonth).find(".ui-datepicker-year").find("option").each(function (i, e) {
                    if (Number(e.value) - 1911 > 0) $(e).text("民國" + (Number(e.innerText) - 1911) + "年");
                }).end().end().get(0).outerHTML;
            }
            return htmlYearMonth;
        },
        _get: function (a, b) {
            a.selectedYear = a.selectedYear - 1911 < 0 ? a.selectedYear + 1911 : a.selectedYear;
            a.drawYear = a.drawYear - 1911 < 0 ? a.drawYear + 1911 : a.drawYear;
            a.curreatYear = a.curreatYear - 1911 < 0 ? a.curreatYear + 1911 : a.curreatYear;

            return old_get.apply(this, [a, b]);
        },
        _updateDatepicker: function (inst) {
            old_CloseFn.call(this, inst);
            $(this).datepicker("widget").find(".ui-datepicker-buttonpane").children(":last")
                   .click(function (e) {
                       inst.input.val("");
                   });
        },
        _setDateDatepicker: function (a, b) {
            if (a = this._getInst(a)) { this._setDate(a, b); this._updateDatepicker(a); this._updateAlternate(a) }
        },
        _widgetDatepicker: function () {
            return this.dpDiv
        },
        formatDate: function (format, date, settings) {
            var d = date.getDate();
            var m = date.getMonth() ;
            var y = date.getFullYear();			
            var fm = function(v){			
                return (v<10 ? '0' : '')+v;
            };
            
            return (y-1911) +''+ fm(m) +''+ fm(d);
        },
        parseDate: function (format, value, settings) {
            var v = new String(value);
            var Y,M,D;
            if(v.length==9){/*100-12-15*/
                Y = v.substring(0,3)-0+1911;
                M = v.substring(4,6)-0;
                D = v.substring(7,8)-0;
                return (new Date(Y,M,D));
            }else if(v.length==8){/*98-12-15*/
                Y = v.substring(0,2)-0+1911;
                M = v.substring(3,5)-0;
                D = v.substring(6,7)-0;
                return (new Date(Y,M,D));
            }
            
            return (new Date());
        }
 
    });
	$.datepicker.regional['zh-TW'] = {
		closeText: '關閉',
		prevText: '&#x3C;上月',
		nextText: '下月&#x3E;',
		currentText: '今天',
		monthNames: ['一月','二月','三月','四月','五月','六月',
		'七月','八月','九月','十月','十一月','十二月'],
		monthNamesShort: ['一月','二月','三月','四月','五月','六月',
		'七月','八月','九月','十月','十一月','十二月'],
		dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
		dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
		dayNamesMin: ['日','一','二','三','四','五','六'],
		weekHeader: '周',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: true,
        yearRange: "c-20:*",
		// yearSuffix: '年',
        onSelect: function (dateText, inst) {
            var dateFormate = inst.settings.dateFormat == null ? "yy-mm-dd" : inst.settings.dateFormat; //取出格式文字
            var reM = /m+/g;
            var reD = /d+/g;
            var objDate = {
                y: inst.selectedYear - 1911 < 0 ? inst.selectedYear : (inst.selectedYear - 1911),
                m: padLeft(String(inst.selectedMonth + 1),2,"0"),
                d: padLeft(String(inst.selectedDay),2,"0")
            };
            $.each(objDate, function (k, v) {
                var re = new RegExp(k + "+");
                dateFormate = dateFormate.replace(re, v);
            });
            inst.input.val(dateFormate);
        }
    };
	$.datepicker.setDefaults($.datepicker.regional['zh-TW']);
});
function padLeft(str, length, sign) {
    if (str.length >= length) return str;
    else return padLeft(sign + str, length, sign);
}