-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- 主機： localhost:8889
-- 產生時間： 2021 年 06 月 01 日 14:42
-- 伺服器版本： 5.7.26
-- PHP 版本： 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `db`
--

-- --------------------------------------------------------

--
-- 資料表結構 `bankvirtualcode`
--

CREATE TABLE `bankvirtualcode` (
  `BVC_num` int(11) NOT NULL,
  `BVC_no` int(16) NOT NULL COMMENT '虛擬碼號碼',
  `BVC_fnum` varchar(50) NOT NULL COMMENT '處分書號碼(含罰緩/怠金)\r\nfine_doc.fd_num/surchange_doc.sd_num',
  `BVC_snum` int(11) NOT NULL COMMENT '犯嫌人號碼',
  `BVC_sic` varchar(50) NOT NULL COMMENT '犯嫌身份證字號/suspect.s_id',
  `BVC_cnum` varchar(50) NOT NULL COMMENT '案件編號'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `call_doc`
--

CREATE TABLE `call_doc` (
  `call_num` int(11) NOT NULL,
  `call_no` varchar(40) NOT NULL COMMENT '催繳文號（發文本號）',
  `call_bno` int(11) NOT NULL DEFAULT '0' COMMENT '催繳支號(1～9)max:9',
  `call_date` date DEFAULT NULL COMMENT '發文日期',
  `call_grace` varchar(30) NOT NULL COMMENT '寬限期',
  `call_delivery_date` date DEFAULT NULL COMMENT '催繳送達日期',
  `call_projectnum` int(11) DEFAULT NULL COMMENT '催繳專案編號',
  `call_delivery_status` varchar(30) DEFAULT NULL COMMENT '催繳送達情況',
  `call_delivery_doc` varchar(1000) DEFAULT NULL COMMENT '催繳回收的送達證書',
  `call_snum` int(11) NOT NULL,
  `call_sic` varchar(30) NOT NULL,
  `call_cnum` varchar(30) NOT NULL,
  `call_fdnum` int(11) NOT NULL,
  `call_empno` varchar(30) DEFAULT NULL COMMENT '最後修改/裁罰人',
  `call_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最後修改時間',
  `call_status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `call_project`
--

CREATE TABLE `call_project` (
  `cp_num` int(11) NOT NULL,
  `cp_name` varchar(30) NOT NULL COMMENT '專案編號(同催繳發文號)',
  `cp_date` date DEFAULT NULL COMMENT '專案建立日期',
  `cp_type` varchar(11) DEFAULT NULL COMMENT '類型(一般/公示)',
  `cp_status` varchar(11) DEFAULT NULL COMMENT '狀態(寄出/結案)',
  `cp_empno` varchar(30) NOT NULL COMMENT '修改人',
  `cp_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `cases`
--

CREATE TABLE `cases` (
  `c_num` varchar(20) NOT NULL COMMENT '案號',
  `rm_num` varchar(20) DEFAULT NULL COMMENT '移送文號(移送地檢)',
  `rm_time` date DEFAULT NULL COMMENT '移送日期(移送地檢)',
  `e_date` timestamp NULL DEFAULT NULL COMMENT '建立時間',
  `e_empno` varchar(30) DEFAULT NULL,
  `s_time` varchar(30) DEFAULT NULL COMMENT '大約發生的時間',
  `s_office` varchar(25) DEFAULT NULL COMMENT '查獲單位',
  `r_office` varchar(25) DEFAULT NULL COMMENT '移送分局',
  `r_office1` varchar(10) DEFAULT NULL COMMENT '移送分局-依據字號',
  `s_date` date DEFAULT NULL,
  `r_county` varchar(20) DEFAULT NULL,
  `r_district` varchar(20) DEFAULT NULL,
  `r_zipcode` varchar(20) DEFAULT '100',
  `r_address` varchar(20) DEFAULT NULL,
  `s_place` varchar(100) DEFAULT NULL,
  `detection_process` varchar(100) DEFAULT NULL,
  `doc_file` varchar(100) DEFAULT NULL,
  `other_doc` varchar(100) DEFAULT NULL,
  `drug_pic` varchar(100) DEFAULT NULL,
  `e_ed_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `e_ed_empno` varchar(100) DEFAULT NULL,
  `RejectNo` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `delete_status` tinyint(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `dep_list`
--

CREATE TABLE `dep_list` (
  `dep_id` int(11) NOT NULL,
  `dep_nm` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT '派出所名稱/大隊名稱',
  `dep_com` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT '所屬分局/所屬大隊',
  `dep_priority` char(2) COLLATE utf8_unicode_ci DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `disciplinary_project`
--

CREATE TABLE `disciplinary_project` (
  `dp_num` int(11) NOT NULL,
  `dp_name` varchar(30) NOT NULL,
  `dp_send_date` date DEFAULT NULL,
  `dp_send_no` varchar(50) DEFAULT NULL,
  `dp_status` varchar(30) DEFAULT NULL COMMENT '狀態:送批/公示',
  `dp_odoc` varchar(100) DEFAULT NULL COMMENT '公文上傳',
  `dp_doc_test` varchar(30) DEFAULT NULL,
  `dp_doc` varchar(1000) DEFAULT NULL,
  `dp_sign` varchar(1000) DEFAULT NULL COMMENT '簽',
  `dp_an` varchar(1000) DEFAULT NULL COMMENT '公告',
  `dp_van` int(11) DEFAULT NULL COMMENT '電子公告',
  `dp_inan` int(11) DEFAULT NULL COMMENT '在監公告',
  `dp_empno` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `drug_double_check`
--

CREATE TABLE `drug_double_check` (
  `ddc_num` int(11) NOT NULL,
  `ddc_level` varchar(30) DEFAULT NULL,
  `ddc_ingredient` varchar(100) DEFAULT NULL,
  `ddc_NW` float NOT NULL DEFAULT '0' COMMENT '淨重',
  `ddc_RW` float NOT NULL DEFAULT '0' COMMENT '驗後餘重',
  `ddc_PNW` float DEFAULT NULL COMMENT '純質淨重',
  `ddc_cnum` varchar(30) NOT NULL,
  `ddc_drug` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `drug_evidence`
--

CREATE TABLE `drug_evidence` (
  `e_id` varchar(100) NOT NULL,
  `e_c_num` varchar(30) NOT NULL,
  `e_date` timestamp NULL DEFAULT NULL,
  `e_empno` varchar(10) NOT NULL COMMENT '建立者',
  `e_ed_empno` varchar(10) NOT NULL COMMENT '最後修改者',
  `e_ed_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最後修改時間',
  `e_count` int(4) DEFAULT NULL,
  `e_type` varchar(50) DEFAULT NULL,
  `e_name` varchar(10) DEFAULT NULL,
  `e_1_N_W` varchar(20) DEFAULT NULL COMMENT '初驗淨重(g)',
  `e_doc` varchar(1000) DEFAULT NULL COMMENT '毒報',
  `e_suspect` varchar(10) DEFAULT NULL,
  `e_s_ic` varchar(30) DEFAULT NULL,
  `e_state` varchar(20) NOT NULL DEFAULT '待送驗',
  `e_place` varchar(100) DEFAULT NULL,
  `e_pic` varchar(100) DEFAULT NULL COMMENT 'URC',
  `e_del_sta` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `drug_first_check`
--

CREATE TABLE `drug_first_check` (
  `df_num` int(10) UNSIGNED NOT NULL,
  `df_level` int(4) DEFAULT NULL COMMENT '毒品級別1～5',
  `df_ingredient` varchar(20) DEFAULT NULL COMMENT '成分',
  `df_drug` varchar(10) NOT NULL COMMENT 'drug_evidence.e_id',
  `df_cnum` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `drug_option`
--

CREATE TABLE `drug_option` (
  `drug_level` int(11) NOT NULL,
  `drug_name` varchar(30) NOT NULL,
  `drug_prio` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `drug_rec`
--

CREATE TABLE `drug_rec` (
  `drug_rec_num` int(11) NOT NULL,
  `drug_rec_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `drug_rec_eid` varchar(30) NOT NULL,
  `drug_rec_estatus` varchar(30) NOT NULL,
  `drug_rec_eplace` varchar(30) NOT NULL,
  `drug_rec_empno` varchar(30) NOT NULL,
  `drug_rec_office` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `drug_source`
--

CREATE TABLE `drug_source` (
  `sou_num` int(11) NOT NULL,
  `sou_pic` varchar(100) DEFAULT NULL,
  `sou_name` varchar(10) DEFAULT NULL,
  `sou_nname` varchar(30) DEFAULT NULL,
  `sou_feature` varchar(30) DEFAULT NULL,
  `sou_phone` varchar(30) DEFAULT NULL,
  `sou_time` varchar(30) DEFAULT NULL COMMENT '交易時間',
  `sou_place` varchar(30) DEFAULT NULL,
  `sou_amount` varchar(30) DEFAULT NULL,
  `sou_count` varchar(30) DEFAULT NULL,
  `sou_s_ic` varchar(30) NOT NULL,
  `sou_cnum` varchar(30) NOT NULL,
  `sou_s_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `employees`
--

CREATE TABLE `employees` (
  `em_num` int(11) NOT NULL,
  `em_ic` varchar(30) DEFAULT NULL,
  `em_mailId` varchar(50) NOT NULL COMMENT '聯絡簿.mail',
  `em_pw` varchar(100) NOT NULL,
  `em_firstname` varchar(10) DEFAULT NULL,
  `em_centername` varchar(10) DEFAULT NULL,
  `em_lastname` varchar(10) NOT NULL,
  `em_job` varchar(30) NOT NULL,
  `em_officeAll` varchar(30) NOT NULL COMMENT '辦公室',
  `em_office` varchar(30) NOT NULL,
  `em_office1` varchar(30) NOT NULL COMMENT '分局單位(分局簡稱)',
  `em_roffice` varchar(30) DEFAULT NULL,
  `em_priority` varchar(30) NOT NULL,
  `em_IsAdmin` char(1) NOT NULL DEFAULT '0' COMMENT '是否有管理權限 0：否; 1：是',
  `em_3permit` varchar(7) DEFAULT NULL COMMENT '3階權限分配',
  `em_change_code` char(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `employee_test`
--

CREATE TABLE `employee_test` (
  `em_num` int(11) NOT NULL,
  `em_ic` varchar(30) DEFAULT NULL,
  `em_mailId` varchar(50) NOT NULL COMMENT '聯絡簿.mail',
  `em_pw` varchar(100) NOT NULL,
  `em_firstname` varchar(10) DEFAULT NULL,
  `em_centername` varchar(10) DEFAULT NULL,
  `em_lastname` varchar(10) NOT NULL,
  `em_job` varchar(30) NOT NULL,
  `em_officeAll` varchar(30) NOT NULL COMMENT '辦公室',
  `em_office` varchar(30) NOT NULL,
  `em_office1` varchar(30) NOT NULL COMMENT '分局單位(分局簡稱)',
  `em_roffice` varchar(30) DEFAULT NULL,
  `em_priority` varchar(30) NOT NULL,
  `em_IsAdmin` char(1) NOT NULL DEFAULT '0' COMMENT '是否有管理權限 0：否; 1：是',
  `em_3permit` varchar(7) DEFAULT NULL COMMENT '3階權限分配',
  `em_change_code` char(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_card`
--

CREATE TABLE `fine_card` (
  `f_no` int(11) NOT NULL,
  `fi_no` int(11) NOT NULL COMMENT 'fine_import.f_no',
  `f_type` char(1) DEFAULT NULL COMMENT '類型',
  `f_certno` varchar(50) NOT NULL COMMENT '憑證編號',
  `f_date` varchar(10) NOT NULL COMMENT '核發日期',
  `f_debtno` varchar(30) NOT NULL COMMENT '債權憑證編號',
  `f_amount` int(11) NOT NULL COMMENT '金額',
  `f_cardmount` int(11) NOT NULL COMMENT '憑證金額',
  `f_moveno` varchar(10) NOT NULL COMMENT '移送案號'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_change_project`
--

CREATE TABLE `fine_change_project` (
  `fcp_num` int(11) NOT NULL,
  `fcp_no` varchar(50) NOT NULL,
  `fcp_type` varchar(50) DEFAULT NULL,
  `fcp_createdate` date NOT NULL,
  `fp_office_num` varchar(50) DEFAULT NULL COMMENT '公文號',
  `fcp_empno` varchar(50) NOT NULL,
  `fcp_status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_doc`
--

CREATE TABLE `fine_doc` (
  `fd_num` varchar(30) NOT NULL COMMENT '罰緩處分書案件編號',
  `fd_date` date NOT NULL COMMENT '發文日期',
  `fd_send_num` varchar(50) NOT NULL COMMENT '發文字號',
  `fd_listed_no` varchar(50) NOT NULL COMMENT '處分書列管編號(與處分書案件編號一樣)',
  `fd_cnum` varchar(50) NOT NULL COMMENT '案件編號cases.c_num',
  `fd_snum` varchar(30) NOT NULL COMMENT '犯嫌人號suspect.s_num',
  `fd_sic` varchar(50) NOT NULL COMMENT '犯嫌人身份證號suspect.s_ic',
  `fd_dely` varchar(30) DEFAULT NULL COMMENT '送達日期',
  `fd_remarks` text COMMENT '處分書備註',
  `fd_drug_msg` text COMMENT '毒品信息(for處分書)',
  `fd_dis_msg` text COMMENT '尿液信息與裁罰情形(for處分書)',
  `fd_fact_text` text COMMENT '完整事實text',
  `fd_target` varchar(50) DEFAULT NULL COMMENT '發文對象',
  `fd_address` varchar(50) DEFAULT NULL COMMENT '發文地址',
  `fd_wantdis` varchar(10) DEFAULT '是',
  `fd_zipcode` varchar(30) DEFAULT NULL,
  `fd_lec_date` date DEFAULT NULL COMMENT '講習日期',
  `fd_lec_time` varchar(30) DEFAULT '09:30-16:30' COMMENT '講習時間',
  `fd_lec_place` varchar(30) NOT NULL DEFAULT '臺北市非政府(NGO)會館1樓 多功能資料室' COMMENT '講習地點',
  `fd_lec_address` varchar(30) NOT NULL DEFAULT '臺北市中正區青島東路8號' COMMENT '講習地址',
  `fd_empno` varchar(50) DEFAULT NULL COMMENT '修改人/裁罰人',
  `fd_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
  `fd_psystem` varchar(60) DEFAULT NULL COMMENT '警政署系統編號'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_doc_delivery`
--

CREATE TABLE `fine_doc_delivery` (
  `fdd_num` int(11) NOT NULL,
  `fdd_date` date DEFAULT NULL COMMENT '送達時間',
  `fdd_status` varchar(30) DEFAULT NULL COMMENT '送達情形',
  `fdd_doc` varchar(1000) DEFAULT NULL COMMENT '送達文件',
  `fdd_projectpublic_num` varchar(30) DEFAULT NULL COMMENT '公示送達專案\r\nfine_doc_publicproject.fdp_num',
  `fdd_fdnum` varchar(50) NOT NULL COMMENT '處分書編號',
  `fdd_snum` int(11) NOT NULL COMMENT '犯嫌編號',
  `fdd_cnum` varchar(50) NOT NULL COMMENT '案件編號'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_doc_publicproject`
--

CREATE TABLE `fine_doc_publicproject` (
  `fdp_num` int(11) NOT NULL,
  `fpd_no` varchar(30) NOT NULL COMMENT '公示送達專案編號',
  `fpd_status` varchar(30) DEFAULT NULL COMMENT '公示送達專案狀態',
  `fdp_delivery_date` date DEFAULT NULL COMMENT '公告時間',
  `fdp_sign` varchar(1000) DEFAULT NULL COMMENT '簽',
  `fdp_an` varchar(1000) DEFAULT NULL COMMENT '公告',
  `fdp_prison` varchar(1000) DEFAULT NULL COMMENT '在監公告',
  `fdp_empno` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_import`
--

CREATE TABLE `fine_import` (
  `f_no` int(11) NOT NULL COMMENT '流水號',
  `f_oldno` int(11) NOT NULL COMMENT '匯入檔案的舊流水號',
  `type` char(1) NOT NULL COMMENT 'A:罰緩;B:怠金',
  `f_year` char(5) NOT NULL COMMENT '年度',
  `f_caseid` char(10) NOT NULL COMMENT '案件編號',
  `f_username` varchar(20) NOT NULL COMMENT '受處分人姓名',
  `f_userid` char(10) NOT NULL COMMENT '身份證編號',
  `f_amount` decimal(10,0) DEFAULT NULL COMMENT '罰鍰/怠金（元）',
  `f_donedate` varchar(10) DEFAULT NULL COMMENT '完納日期',
  `f_doneamount` decimal(10,0) DEFAULT NULL COMMENT '完納金額',
  `f_paydate` varchar(10) DEFAULT NULL COMMENT '繳款日期',
  `f_paymoney` decimal(10,0) DEFAULT NULL COMMENT '本期核銷金額',
  `f_virtualcode` text COMMENT '虛擬帳戶',
  `f_paylog` text COMMENT '分期LOG',
  `f_execlog` text COMMENT '執行命令LOG',
  `f_movelog` text COMMENT '移送LOG',
  `f_cardlog` text COMMENT '憑證LOG',
  `f_dellog` text COMMENT '撤銷註銷LOG',
  `f_abmormal` int(11) DEFAULT '0' COMMENT '異常處理狀態(0:異常/無異常;1:已處理)',
  `f_comment` text COMMENT '備註'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帳務資料匯入資料-暫時用';

-- --------------------------------------------------------

--
-- 資料表結構 `fine_partpay`
--

CREATE TABLE `fine_partpay` (
  `fpart_num` int(11) NOT NULL,
  `f_project_id` varchar(100) DEFAULT NULL COMMENT 'fine_project.fp_no/fine_trafproject.ftp_name',
  `fpart_fpnum` int(11) NOT NULL COMMENT 'fine_rec.fp_num / fine_import.f_no',
  `sort_no` int(11) DEFAULT NULL COMMENT '順序',
  `fpart_amount` int(11) NOT NULL COMMENT '分期金額',
  `fpart_type` varchar(30) DEFAULT NULL COMMENT '繳款方式',
  `f_ticket_no` varchar(100) DEFAULT NULL COMMENT '支匯票號碼',
  `f_receipt_no` varchar(100) DEFAULT NULL COMMENT '收據號碼',
  `fpart_date` varchar(10) DEFAULT NULL COMMENT '分期日期',
  `f_fee` int(11) NOT NULL DEFAULT '0' COMMENT '執行費',
  `f_turnsub` int(11) NOT NULL DEFAULT '0' COMMENT '轉正怠金',
  `f_refund` int(11) NOT NULL DEFAULT '0' COMMENT '退費',
  `f_source` varchar(100) DEFAULT NULL COMMENT '來源',
  `f_office_num` varchar(100) DEFAULT NULL COMMENT '支匯票公文號',
  `f_turnsub_log` text COMMENT '轉正紀錄(日期_金額_原案號)',
  `f_status` varchar(30) DEFAULT NULL COMMENT '繳納狀態',
  `f_comment` text COMMENT '備註'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_petition`
--

CREATE TABLE `fine_petition` (
  `petition_num` int(11) NOT NULL,
  `petition_date` date NOT NULL,
  `petition_answer` varchar(50) NOT NULL COMMENT '答辯書',
  `petition_doc1` varchar(50) DEFAULT NULL COMMENT '訴願公文',
  `petition_doc2` varchar(50) DEFAULT NULL COMMENT '訴願公文',
  `petition_doc3` varchar(50) DEFAULT NULL COMMENT '訴願公文',
  `petition_doc_ap1` varchar(50) DEFAULT NULL COMMENT '行政訴願公文',
  `petition_doc_ap2` varchar(50) DEFAULT NULL COMMENT '行政訴願公文',
  `petition_doc_ap3` varchar(50) DEFAULT NULL COMMENT '行政訴願公文',
  `petition_fdnum` varchar(50) NOT NULL COMMENT '處分書編號',
  `petition_cnum` varchar(50) NOT NULL COMMENT '案件編號',
  `petition_snum` int(11) NOT NULL COMMENT '犯嫌人號'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_project`
--

CREATE TABLE `fine_project` (
  `fp_num` int(11) NOT NULL,
  `fp_no` varchar(100) NOT NULL,
  `fp_type` varchar(30) NOT NULL COMMENT '罰單類型',
  `fp_paytype` varchar(30) NOT NULL COMMENT '付款方式',
  `fp_office_num` varchar(50) DEFAULT NULL,
  `fp_createdate` date NOT NULL COMMENT '專案建立時間',
  `fp_empno` varchar(30) NOT NULL,
  `fp_status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_rec`
--

CREATE TABLE `fine_rec` (
  `f_num` int(11) NOT NULL,
  `f_name` varchar(30) DEFAULT NULL COMMENT '帳務修改過後名稱	',
  `f_date` date NOT NULL COMMENT '繳款期限日期',
  `f_damount` int(11) NOT NULL DEFAULT '0' COMMENT '處分書金額',
  `f_samount` int(11) NOT NULL DEFAULT '0' COMMENT '怠金金額',
  `f_payamount` int(11) NOT NULL DEFAULT '0' COMMENT '完納金額',
  `f_paynowamount` int(11) DEFAULT NULL COMMENT '繳納金額',
  `f_payday` date DEFAULT NULL COMMENT '繳款日期',
  `f_paytype` varchar(50) DEFAULT NULL COMMENT '繳納方式',
  `f_fee` int(11) DEFAULT '0' COMMENT '執行費',
  `f_refund` int(11) DEFAULT '0' COMMENT '退費',
  `f_turnsub` int(11) DEFAULT '0' COMMENT '轉正怠金',
  `f_certno` varchar(100) DEFAULT NULL,
  `f_certDate` date DEFAULT NULL COMMENT '憑證日期',
  `f_certIssue` date DEFAULT NULL COMMENT '核發日期',
  `f_certamount` int(11) DEFAULT NULL COMMENT '憑證金額',
  `f_certdoc` varchar(100) DEFAULT NULL COMMENT '憑證文件',
  `f_BVC` varchar(1000) NOT NULL COMMENT 'disciplinary_detail.BVC 虛擬碼',
  `f_check_unit` varchar(50) DEFAULT NULL COMMENT '支匯票來文單位',
  `f_check_receipt` varchar(50) DEFAULT NULL COMMENT '支匯票收據',
  `f_check_officenum` varchar(50) DEFAULT NULL COMMENT '支匯票公文號',
  `f_check_checknum` varchar(50) DEFAULT NULL COMMENT '支票號碼',
  `f_remark` text COMMENT '備註',
  `f_cancelAmount` int(11) NOT NULL DEFAULT '0' COMMENT '註銷補助款',
  `f_change_type` varchar(30) DEFAULT NULL COMMENT '轉正類型',
  `f_project_num` varchar(50) DEFAULT NULL,
  `fcp_no` varchar(50) DEFAULT NULL,
  `f_cnum` varchar(50) NOT NULL,
  `f_snum` varchar(50) NOT NULL,
  `f_surcnum` varchar(30) DEFAULT NULL COMMENT '怠金編號',
  `f_sic` varchar(50) NOT NULL,
  `f_empno` varchar(30) NOT NULL COMMENT '修改人',
  `f_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_traf`
--

CREATE TABLE `fine_traf` (
  `ft_num` int(11) NOT NULL,
  `ft_no` varchar(30) NOT NULL COMMENT '移送案號',
  `ft_send_no` varchar(30) DEFAULT NULL COMMENT '發文字號',
  `ft_send_bno` char(2) DEFAULT NULL COMMENT '發文支號',
  `ft_send_date` date DEFAULT NULL COMMENT '移送發文日期',
  `ft_projectnum` varchar(11) DEFAULT NULL COMMENT '專案號',
  `ft_delivery_date` date DEFAULT NULL COMMENT '送達日期',
  `ft_delivery_status` varchar(30) DEFAULT NULL COMMENT '送達狀態',
  `ft_delivery_doc` varchar(100) DEFAULT NULL COMMENT '送達證書',
  `ft_target` varchar(30) DEFAULT '臺北' COMMENT '目標地檢',
  `ft_target_address` varchar(100) DEFAULT '台北市中山南京東路二段1號',
  `ft_target_post` varchar(30) DEFAULT '行政院' COMMENT '郵局',
  `ft_snum` int(11) NOT NULL,
  `ft_sic` varchar(30) NOT NULL,
  `ft_cnum` varchar(30) NOT NULL,
  `ft_dnum` int(11) DEFAULT NULL,
  `ft_surcnum` int(11) DEFAULT NULL,
  `ft_callnum` varchar(30) DEFAULT NULL,
  `ft_propertydoc` varchar(100) DEFAULT NULL COMMENT '財產csv',
  `ft_housedoc` varchar(100) DEFAULT NULL COMMENT '戶役政文件',
  `ft_empno` varchar(30) NOT NULL,
  `ft_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `fine_trafproject`
--

CREATE TABLE `fine_trafproject` (
  `ftp_num` int(11) NOT NULL,
  `ftp_name` int(11) NOT NULL COMMENT '專案編號',
  `ftp_status` int(11) DEFAULT NULL,
  `ftp_empno` varchar(30) NOT NULL,
  `ftp_date` date NOT NULL COMMENT '專案建立日期',
  `ftp_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `phone`
--

CREATE TABLE `phone` (
  `p_num` int(11) NOT NULL,
  `p_no` varchar(30) NOT NULL,
  `p_oppw` varchar(30) DEFAULT NULL,
  `p_picpw` varchar(30) DEFAULT NULL,
  `p_imei` varchar(50) DEFAULT NULL,
  `p_conf` varchar(10) NOT NULL DEFAULT '否' COMMENT '查扣手機',
  `p_s_ic` varchar(30) NOT NULL,
  `p_s_cnum` varchar(30) NOT NULL,
  `p_s_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `phone_rec`
--

CREATE TABLE `phone_rec` (
  `pr_num` int(11) NOT NULL,
  `pr_path` varchar(10) NOT NULL,
  `pr_phone` varchar(30) DEFAULT NULL,
  `pr_name` varchar(30) DEFAULT NULL,
  `pr_time` varchar(30) DEFAULT NULL,
  `pr_relationship` varchar(30) DEFAULT NULL,
  `pr_has_drug` varchar(10) NOT NULL DEFAULT '否',
  `pr_user` varchar(10) NOT NULL DEFAULT '否',
  `pr_seller` varchar(10) NOT NULL DEFAULT '否',
  `pr_s_ic` varchar(30) NOT NULL,
  `pr_s_cnum` varchar(30) NOT NULL,
  `pr_p_no` varchar(30) NOT NULL,
  `pr_p_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `prison_list`
--

CREATE TABLE `prison_list` (
  `prison_name` varchar(30) NOT NULL COMMENT '監所名稱',
  `prison_sname` varchar(30) NOT NULL COMMENT '監所簡稱',
  `prison_type` varchar(10) DEFAULT NULL COMMENT '監獄性質',
  `prison_phone` varchar(30) NOT NULL COMMENT '電話',
  `prison_address` varchar(30) NOT NULL COMMENT '地址',
  `prison_website` text COMMENT '監所網站',
  `prison_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `reward_project1`
--

CREATE TABLE `reward_project1` (
  `rp_num` int(11) NOT NULL,
  `rp_name` varchar(30) NOT NULL,
  `rp_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rp_roffice` varchar(30) NOT NULL,
  `rp_soffice` varchar(30) DEFAULT NULL,
  `rp_status` varchar(30) DEFAULT NULL,
  `rp_rremarks` text COMMENT '備註',
  `rp_project_num` varchar(30) DEFAULT NULL COMMENT 'reward_project2.rp_num',
  `rp3_project_num` varchar(11) DEFAULT NULL COMMENT ' reward_project3.rp_num ',
  `rp_empno` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `reward_project2`
--

CREATE TABLE `reward_project2` (
  `rp_num` int(11) NOT NULL,
  `rp_name` varchar(30) NOT NULL,
  `rp_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rp_roffice` varchar(30) NOT NULL,
  `rp_status` varchar(30) DEFAULT NULL,
  `rp_rremarks` text COMMENT '備註',
  `rp3_project_num` varchar(30) DEFAULT NULL COMMENT 'reward_project3.rp3_num',
  `rp_empno` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `reward_project3`
--

CREATE TABLE `reward_project3` (
  `rp3_num` int(11) NOT NULL,
  `rp3_name` varchar(30) NOT NULL,
  `rp3_date` date NOT NULL,
  `rp3_status` varchar(30) DEFAULT NULL,
  `rp3_remarks` text,
  `rp2_project_num` int(11) DEFAULT NULL COMMENT ' reward_project2.num ',
  `rp3_empno` varchar(30) DEFAULT NULL COMMENT '修改者',
  `rp3_edtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `social_media`
--

CREATE TABLE `social_media` (
  `social_media_name` varchar(30) DEFAULT NULL,
  `social_media_ac1` varchar(30) DEFAULT NULL,
  `social_media_ac2` varchar(30) DEFAULT NULL,
  `social_media_num` int(30) NOT NULL,
  `social_media_ac3` varchar(30) DEFAULT NULL,
  `s_cnum` varchar(30) DEFAULT NULL,
  `s_ic` varchar(30) DEFAULT NULL,
  `s_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `source_sm`
--

CREATE TABLE `source_sm` (
  `sou_sm_num` int(11) NOT NULL,
  `sou_sm_name` varchar(30) DEFAULT NULL,
  `sou_sm_ac1` varchar(30) DEFAULT NULL,
  `sou_sm_ac2` varchar(30) DEFAULT NULL,
  `sou_sm_ac3` varchar(30) DEFAULT NULL,
  `sou_sm_s_ic` varchar(30) NOT NULL,
  `sou_s_num` int(11) DEFAULT NULL,
  `sou_sm_s_cnum` varchar(30) NOT NULL,
  `sou_sm_phone` varchar(30) NOT NULL,
  `sou_sounum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `surcharges`
--

CREATE TABLE `surcharges` (
  `surc_num` int(11) NOT NULL,
  `surc_no` varchar(50) NOT NULL COMMENT '怠金編號',
  `surc_listed_no` varchar(30) NOT NULL COMMENT '列管編號(原處分書)',
  `surc_lastnum` varchar(30) DEFAULT NULL COMMENT '上一個怠金處分書編號	',
  `surc_basenum` varchar(30) DEFAULT NULL COMMENT '依據文號',
  `surc_basenumdate` date DEFAULT NULL,
  `surc_amount` int(11) DEFAULT NULL COMMENT '怠金處分書金額',
  `surc_findate` date DEFAULT NULL COMMENT '怠金處分書繳納期限',
  `surc_BVC` varchar(30) DEFAULT NULL COMMENT '怠金虛擬嗎',
  `surc_delivery_date` date DEFAULT NULL COMMENT '怠金送達日期',
  `surc_delivery_status` varchar(30) DEFAULT NULL COMMENT '怠金送達狀態',
  `surc_delivery_doc` varchar(100) DEFAULT NULL COMMENT '怠金送達文件',
  `surc_cnum` varchar(30) NOT NULL COMMENT '案件編號',
  `surc_snum` int(11) NOT NULL COMMENT '犯嫌人號',
  `surc_sic` varchar(30) NOT NULL COMMENT '犯嫌人身份證',
  `surc_projectnum` varchar(50) DEFAULT NULL COMMENT '怠金專案處理',
  `surc_date` date DEFAULT NULL COMMENT '發文日期/裁處日期',
  `surc_target` varchar(30) DEFAULT NULL COMMENT '發文目標',
  `surc_address` varchar(50) DEFAULT NULL COMMENT '發文地址',
  `surc_zipcode` varchar(10) DEFAULT NULL COMMENT '發文郵遞區號',
  `surc_psend_num` varchar(30) DEFAULT NULL COMMENT '警局發文字號',
  `surc_send_num` varchar(20) DEFAULT NULL COMMENT '發文字號/裁處字號',
  `surc_send_bnum` varchar(3) DEFAULT NULL COMMENT '發文支號/裁處支號',
  `surc_olec_date` date DEFAULT NULL COMMENT '原講習時間',
  `surc_inprison` varchar(10) NOT NULL DEFAULT '否' COMMENT '是否在監',
  `surc_prison` varchar(50) DEFAULT NULL COMMENT '所在監所',
  `surc_prisonaddress` varchar(50) DEFAULT NULL COMMENT '監所地址',
  `surc_prison_doc` varchar(1000) DEFAULT NULL COMMENT '監所文件',
  `surc_lec_date` date DEFAULT NULL COMMENT '怠金重講習日期',
  `surc_lec_time` varchar(20) DEFAULT NULL COMMENT '怠金重講習時間',
  `surc_lec_place` varchar(30) DEFAULT NULL COMMENT '怠金重講習地點',
  `surc_lec_address` varchar(100) DEFAULT NULL COMMENT '怠金重講習地址',
  `surc_empno` varchar(10) NOT NULL COMMENT '最後修改人/裁罰人',
  `surc_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `surc_psystem` varchar(60) DEFAULT NULL COMMENT '警政署系統編號',
  `surc_other_doc` varchar(1000) DEFAULT NULL COMMENT '毒防公文,清冊',
  `surc_excel` varchar(100) DEFAULT NULL COMMENT 'excel清冊'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `surcharge_petition`
--

CREATE TABLE `surcharge_petition` (
  `petition_num` int(11) NOT NULL,
  `petition_date` date NOT NULL,
  `petition_answer` varchar(50) NOT NULL COMMENT '答辯書',
  `petition_doc1` varchar(50) DEFAULT NULL COMMENT '訴願公文',
  `petition_doc2` varchar(50) DEFAULT NULL COMMENT '訴願公文',
  `petition_doc3` varchar(50) DEFAULT NULL COMMENT '訴願公文',
  `petition_doc_ap1` varchar(50) DEFAULT NULL COMMENT '行政訴願公文',
  `petition_doc_ap2` varchar(50) DEFAULT NULL COMMENT '行政訴願公文',
  `petition_doc_ap3` varchar(50) DEFAULT NULL COMMENT '行政訴願公文',
  `petition_surcnum` varchar(50) NOT NULL COMMENT '怠金編號',
  `petition_cnum` varchar(50) NOT NULL COMMENT '案件編號',
  `petition_snum` int(11) NOT NULL COMMENT '犯嫌人號'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `surcharge_project`
--

CREATE TABLE `surcharge_project` (
  `sp_num` int(11) NOT NULL,
  `sp_date` date DEFAULT NULL,
  `sp_status` varchar(30) DEFAULT NULL,
  `sp_no` varchar(100) NOT NULL,
  `sp_type` varchar(30) DEFAULT NULL COMMENT '類型',
  `sp_reason2` varchar(30) DEFAULT NULL COMMENT '不裁罰原因2',
  `sp_reason1` varchar(30) DEFAULT NULL COMMENT '不裁罰原因',
  `sp_an` varchar(100) DEFAULT NULL COMMENT '公告',
  `sp_ebulletin` varchar(100) DEFAULT NULL COMMENT '電子公報',
  `sp_empno` varchar(30) DEFAULT NULL,
  `sp_time` date NOT NULL COMMENT '專案建立時間',
  `sp_sign` int(11) DEFAULT NULL COMMENT '簽'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `surc_traf`
--

CREATE TABLE `surc_traf` (
  `ft_num` int(11) NOT NULL,
  `ft_no` varchar(30) NOT NULL COMMENT '移送案號',
  `ft_send_no` varchar(30) DEFAULT NULL COMMENT '發文字號',
  `ft_send_bno` char(2) DEFAULT NULL COMMENT '發文支號',
  `ft_send_date` date DEFAULT NULL COMMENT '移送發文日期',
  `ft_projectnum` varchar(11) DEFAULT NULL COMMENT '專案號',
  `ft_delivery_date` date DEFAULT NULL COMMENT '送達日期',
  `ft_delivery_status` varchar(30) DEFAULT NULL COMMENT '送達狀態',
  `ft_delivery_doc` varchar(100) DEFAULT NULL COMMENT '送達證書',
  `ft_target` varchar(30) DEFAULT '臺北' COMMENT '目標地檢',
  `ft_target_address` varchar(100) DEFAULT '台北市中山南京東路二段1號',
  `ft_target_post` varchar(30) DEFAULT '行政院' COMMENT '郵局',
  `ft_snum` int(11) NOT NULL,
  `ft_sic` varchar(30) NOT NULL,
  `ft_cnum` varchar(30) NOT NULL,
  `ft_dnum` int(11) DEFAULT NULL,
  `ft_surcnum` varchar(30) DEFAULT NULL,
  `ft_callnum` varchar(30) DEFAULT NULL,
  `ft_propertydoc` varchar(100) DEFAULT NULL COMMENT '財產csv',
  `ft_housedoc` varchar(100) DEFAULT NULL COMMENT '戶役政文件',
  `ft_empno` varchar(30) NOT NULL,
  `ft_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `surc_trafproject`
--

CREATE TABLE `surc_trafproject` (
  `stp_num` int(11) NOT NULL,
  `stp_name` varchar(30) NOT NULL,
  `stp_status` varchar(30) DEFAULT NULL,
  `stp_empno` varchar(30) NOT NULL,
  `stp_date` date NOT NULL,
  `st[_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `suspect`
--

CREATE TABLE `suspect` (
  `s_num` int(11) NOT NULL,
  `s_ic` varchar(30) DEFAULT NULL COMMENT '身份證字號',
  `s_gender` varchar(11) DEFAULT NULL COMMENT '性別',
  `s_name` varchar(25) DEFAULT NULL,
  `s_nname` varchar(20) DEFAULT NULL COMMENT '綽號',
  `s_nation` varchar(25) DEFAULT NULL,
  `s_feature` varchar(20) DEFAULT NULL COMMENT '特徵',
  `s_birth` date DEFAULT NULL COMMENT '出生日期',
  `s_job` varchar(20) DEFAULT NULL,
  `s_edu` varchar(10) DEFAULT NULL COMMENT '教育程度-1=未受教育-2=國小...-7=博士',
  `s_U_Col` varchar(10) DEFAULT NULL COMMENT '列管尿液採檢人口  ',
  `s_dpcounty` varchar(40) DEFAULT NULL COMMENT '戶籍地',
  `s_dpdistrict` varchar(30) DEFAULT NULL,
  `s_dpzipcode` varchar(30) DEFAULT NULL,
  `s_dpaddress` varchar(50) DEFAULT NULL,
  `s_rpcounty` varchar(30) DEFAULT NULL COMMENT '現住縣市',
  `s_rpdistrict` varchar(30) DEFAULT NULL COMMENT '現住鄉鎮',
  `s_rpzipcode` varchar(30) DEFAULT NULL COMMENT '現住zipcode',
  `s_rpaddress` varchar(50) DEFAULT NULL COMMENT '現住路門牌',
  `s_prison` varchar(60) DEFAULT NULL COMMENT '所在監所',
  `s_prison_doc` varchar(100) DEFAULT NULL,
  `s_gang` varchar(100) DEFAULT NULL COMMENT '幫派堂口名稱',
  `s_pic` varchar(100) DEFAULT NULL,
  `s_CMethods` varchar(30) DEFAULT NULL COMMENT '犯罪手法',
  `s_cnum` varchar(30) NOT NULL,
  `s_sac_state` varchar(60) DEFAULT NULL,
  `s_utest_num` varchar(30) DEFAULT NULL,
  `s_utest_doc` varchar(1000) DEFAULT NULL,
  `s_resource` varchar(10) DEFAULT NULL COMMENT '是否未指認',
  `s_noresource` varchar(100) DEFAULT NULL COMMENT '未指認原因',
  `s_reward_project` varchar(50) DEFAULT NULL,
  `s_reward_project2` varchar(50) DEFAULT NULL COMMENT '分局獎金專案',
  `s_reward_project3` varchar(50) DEFAULT NULL COMMENT '警局專案 ',
  `s_reward_status` varchar(30) DEFAULT NULL,
  `s_reward_fine` int(11) DEFAULT NULL COMMENT '獎金金額',
  `s_reward_date` date DEFAULT NULL COMMENT '送出獎金審核時間',
  `s_reward_remarks` text COMMENT '獎金備註',
  `s_reward_rej` tinyint(4) DEFAULT '0',
  `s_go_no` varchar(50) NOT NULL COMMENT '分局發文字號\r\n',
  `sp_doc` varchar(1000) DEFAULT NULL COMMENT '涉刑事案件司法文書',
  `s_roffice` varchar(30) DEFAULT NULL COMMENT '移送分局',
  `s_roffice1` varchar(10) DEFAULT '文一' COMMENT '依據單位',
  `s_fno` varchar(30) DEFAULT NULL COMMENT '依據字號',
  `s_fdate` date DEFAULT NULL,
  `s_dp_project` varchar(50) DEFAULT NULL,
  `s_delivered_st` varchar(30) DEFAULT NULL COMMENT '處分書送達狀態',
  `s_delivered_date` date DEFAULT NULL COMMENT '處分書送達時間',
  `s_edtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `suspect_fadai`
--

CREATE TABLE `suspect_fadai` (
  `s_fadai_num` int(11) NOT NULL,
  `s_fadai_name` varchar(30) DEFAULT NULL,
  `s_fadai_gender` varchar(3) DEFAULT NULL COMMENT '性別',
  `s_fadai_ic` varchar(30) DEFAULT NULL,
  `s_fadai_sic` varchar(30) DEFAULT NULL COMMENT 'suspect.s_ic',
  `s_fadai_county` varchar(30) DEFAULT NULL,
  `s_fadai_district` varchar(30) DEFAULT NULL,
  `s_fadai_zipcode` varchar(30) DEFAULT NULL,
  `s_fadai_address` varchar(50) DEFAULT NULL,
  `s_fadai_birth` date DEFAULT NULL,
  `s_fadai_phone` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `suspect_punishment`
--

CREATE TABLE `suspect_punishment` (
  `sp_num` int(11) NOT NULL,
  `sp_go_no` varchar(50) DEFAULT NULL,
  `sp_doc` varchar(30) DEFAULT NULL,
  `sp_snum` int(11) DEFAULT NULL,
  `sp_cnum` varchar(30) DEFAULT NULL,
  `sp_s_ic` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `susp_check`
--

CREATE TABLE `susp_check` (
  `sc_num` int(11) NOT NULL,
  `sc_ingredient` varchar(100) NOT NULL,
  `sc_level` varchar(30) NOT NULL,
  `sc_cnum` varchar(30) NOT NULL,
  `sc_snum` int(11) NOT NULL COMMENT 'suspect.s_num'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `upload_log`
--

CREATE TABLE `upload_log` (
  `ser` int(11) NOT NULL,
  `f_belong` char(1) NOT NULL COMMENT '所屬項目A:裁罰;B:帳務;C:執行命令;D:移送;E:憑證;F:撤註銷',
  `f_year` varchar(5) NOT NULL COMMENT '年度',
  `f_filename` varchar(250) NOT NULL COMMENT '檔案名稱',
  `f_ori_filename` varchar(250) NOT NULL COMMENT '原始檔案名稱',
  `f_upload_date` datetime NOT NULL COMMENT '上傳日期',
  `f_status` varchar(100) NOT NULL COMMENT '上傳狀態',
  `emp_no` varchar(100) NOT NULL COMMENT '上傳者'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='整合上傳紀錄檔';

-- --------------------------------------------------------

--
-- 資料表結構 `vehicle`
--

CREATE TABLE `vehicle` (
  `v_type` varchar(30) DEFAULT NULL,
  `v_license_no` varchar(30) DEFAULT NULL,
  `v_color` varchar(30) DEFAULT NULL,
  `v_num` int(11) NOT NULL,
  `v_owner` varchar(30) DEFAULT NULL COMMENT 'suspect.s_ic',
  `v_cnum` varchar(30) NOT NULL,
  `v_s_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `wordlib_courses`
--

CREATE TABLE `wordlib_courses` (
  `ser` int(20) NOT NULL,
  `c_date` varchar(20) NOT NULL COMMENT '日期',
  `c_week` char(2) NOT NULL COMMENT '星期',
  `c_time` varchar(100) NOT NULL COMMENT '時間',
  `c_place` text NOT NULL COMMENT '地點',
  `c_addr` text NOT NULL COMMENT '地址'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='講習課程列表';

-- --------------------------------------------------------

--
-- 資料表結構 `wordlib_prison`
--

CREATE TABLE `wordlib_prison` (
  `prison_num` int(11) NOT NULL,
  `prison_name` varchar(30) NOT NULL COMMENT '監所名稱',
  `prison_sname` varchar(30) NOT NULL COMMENT '監所簡稱',
  `prison_type` varchar(10) DEFAULT NULL COMMENT '監獄性質',
  `prison_phone` varchar(30) NOT NULL COMMENT '電話',
  `prison_address` varchar(30) NOT NULL COMMENT '地址',
  `prison_website` text COMMENT '監所網站'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='監所列表';

--
-- 傾印資料表的資料 `wordlib_prison`
--

INSERT INTO `wordlib_prison` (`prison_num`, `prison_name`, `prison_sname`, `prison_type`, `prison_phone`, `prison_address`, `prison_website`) VALUES
(1, '法務部矯正署臺北監獄', '', '監獄', '03-3191119', '桃園市龜山區宏德新村2號', 'http://www.tpp.moj.gov.tw'),
(2, '法務部矯正署桃園監獄', '', '監獄', '03-3603612', '桃園市桃園區延壽街158號', 'http://www.typ.moj.gov.tw'),
(3, '法務部矯正署桃園女子監獄', '', '監獄', '03-4807959', '桃園市龍潭區富林里中正路三林段617號', 'http://www.tyw.moj.gov.tw'),
(4, '法務部矯正署新竹監獄', '', '監獄', '03-5222577', '新竹市延平路1段108號', 'http://www.scp.moj.gov.tw'),
(5, '法務部矯正署臺中監獄', '', '監獄', '04-23891296', '臺中市南屯區培德路9號', 'http://www.tcp.moj.gov.tw'),
(6, '法務部矯正署臺中女子監獄', '', '監獄', '04-23840936', '臺中市南屯區培德路9之3號', 'http://www.tcw.moj.gov.tw'),
(7, '法務部矯正署彰化監獄', '', '監獄', '04-8964800', '彰化縣二林鎮二溪路3段240號', 'http://www.chp.moj.gov.tw'),
(8, '法務部矯正署雲林監獄', '', '監獄', '05-6339660', '雲林縣虎尾鎮興南里仁愛新村1號', 'http://www.ulp.moj.gov.tw'),
(9, '法務部矯正署雲林第二監獄', '', '監獄', '05-6362788', '雲林縣虎尾鎮建國里建國四村5之18號', 'http://www.ulp2.moj.gov.tw'),
(10, '法務部矯正署嘉義監獄', '', '監獄', '05-3621865', '嘉義縣鹿草鄉豐稠村維新新村1號', 'http://www.cyp.moj.gov.tw'),
(11, '法務部矯正署臺南監獄', '', '監獄', '06-2781116共3線', '臺南市歸仁區武東里明德新村1號', 'http://www.tnp.moj.gov.tw'),
(12, '法務部矯正署明德外役監獄', '', '監獄', '06-5783498', '臺南市山上區玉峰里明德山莊1號', 'http://www.mtp.moj.gov.tw'),
(13, '法務部矯正署高雄監獄', '', '監獄', '07-7882548', '高雄市大寮區仁德新村1號', 'http://www.ksp.moj.gov.tw'),
(14, '法務部矯正署高雄第二監獄', '', '監獄', '07-6152646', '高雄市燕巢區正德新村1號', 'http://www.ksd.moj.gov.tw'),
(15, '法務部矯正署高雄女子監獄', '', '監獄', '07-7920586', '高雄市大寮區內坑里淑德新村1號', 'http://www.ksw.moj.gov.tw'),
(16, '法務部矯正署屏東監獄', '', '監獄', '08-7785438', '屏東縣竹田鄉永豐村永豐路132號', 'http://www.ptp.moj.gov.tw'),
(17, '法務部矯正署臺東監獄', '', '監獄', '089-310185', '臺東市廣東路317號', 'http://www.ttp.moj.gov.tw'),
(18, '法務部矯正署花蓮監獄', '', '監獄', '03-8521141-4', '花蓮縣吉安鄉吉安路6段700號', 'http://www.hlp.moj.gov.tw'),
(19, '法務部矯正署自強外役監獄', '', '監獄', '03-8703914', '花蓮縣光復鄉大全村建國路1段1號', 'http://www.jcp.moj.gov.tw'),
(20, '法務部矯正署宜蘭監獄', '', '監獄', '03-9894166', '宜蘭縣三星鄉拱照村三星路3段365巷安農新村1號', 'http://www.ilp.moj.gov.tw'),
(21, '法務部矯正署基隆監獄', '', '監獄', '02-24651146', '基隆市東光路199號', 'http://www.klp.moj.gov.tw'),
(22, '法務部矯正署澎湖監獄', '', '監獄', '06-9211151', '澎湖縣湖西鄉鼎灣村1之1號', 'http://www.php.moj.gov.tw'),
(23, '法務部矯正署綠島監獄', '', '監獄', '089-672502', '臺東縣綠島鄉中寮村192號', 'http://www.gip.moj.gov.tw'),
(24, '法務部矯正署金門監獄', '', '監獄', '082-332283', '金門縣金湖鎮復興路1之5號', 'http://www.kmp.moj.gov.tw'),
(25, '法務部矯正署八德外役監獄', '', '監獄', '03-3689830', '桃園市八德區懷德街100號', 'http://www.bdo.moj.gov.tw'),
(26, '法務部矯正署臺南第二監獄', '', '監獄', '06-6987797', '臺南市六甲區甲東里曾文街161號', 'http://www.tn2p.moj.gov.tw'),
(27, '法務部矯正署臺北看守所', '', '看守所', '02-22611711', '新北市土城區立德路2號', 'http://www.tpd.moj.gov.tw'),
(28, '法務部矯正署臺北女子看守所', '', '看守所', '02-22748959', '新北市土城區青雲路33號', 'http://www.sld.moj.gov.tw'),
(29, '法務部矯正署新竹看守所', '', '看守所', '03-5222083', '新竹市延平路1段110號', 'http://www.scd.moj.gov.tw'),
(30, '法務部矯正署苗栗看守所', '', '看守所', '037-361510', '苗栗縣苗栗市南勢里20鄰南勢100號', 'http://www.mld.moj.gov.tw'),
(31, '法務部矯正署臺中看守所', '', '看守所', '04-23853880', '臺中市南屯區培德路11號', 'http://www.tcd.moj.gov.tw'),
(32, '法務部矯正署彰化看守所', '', '看守所', '04-8321381', '彰化縣員林市法院街73號', 'http://www.chd.moj.gov.tw'),
(33, '法務部矯正署南投看守所', '', '看守所', '049-2241876', '南投縣南投市嘉和1路1號', 'http://www.ntd.moj.gov.tw'),
(34, '法務部矯正署嘉義看守所', '', '看守所', '05-3623889', '嘉義縣鹿草鄉豐稠村信義新村1號', 'http://www.cyd.moj.gov.tw'),
(35, '法務部矯正署臺南看守所', '', '看守所', '06-2782935', '臺南市歸仁區武東里明德新村2號', 'http://www.tnd.moj.gov.tw'),
(36, '法務部矯正署屏東看守所', '', '看守所', '08-7782274', '屏東縣竹田鄉永豐村永豐路130號', 'http://www.ptd.moj.gov.tw'),
(37, '法務部矯正署花蓮看守所', '', '看守所', '03-8227252', '花蓮縣花蓮市美崙日新崗1號', 'http://www.hld.moj.gov.tw'),
(38, '法務部矯正署基隆看守所', '', '看守所', '02-24653240', '基隆市信義區崇法街64號', 'http://www.kld.moj.gov.tw'),
(39, '法務部矯正署新店戒治所', '', '戒治所', '02-86666432', '新北市新店區莒光路42號', 'http://www.sdb.moj.gov.tw'),
(40, '法務部矯正署臺中戒治所', '', '戒治所', '04-23803642', '臺中市南屯區培德路3號', 'http://www.tcj.moj.gov.tw'),
(41, '法務部矯正署高雄戒治所', '', '戒治所', '07-6154080', '高雄市燕巢區正德新村5號', 'http://www.ksb.moj.gov.tw'),
(42, '法務部矯正署臺東戒治所', '', '戒治所', '089-581014', '臺東縣鹿野鄉瑞豐村永嶺路270號', 'http://www.ttb.moj.gov.tw'),
(43, '法務部矯正署泰源技能訓練所', '', '技能訓練所', '089-891881', '臺東縣東河鄉北源村32號', 'http://www.tuv.moj.gov.tw'),
(44, '法務部矯正署東成技能訓練所', '', '技能訓練所', '089-570747', '臺東縣卑南鄉美農村班鳩1號', 'http://www.dcv.moj.gov.tw'),
(45, '法務部矯正署岩灣技能訓練所', '', '技能訓練所', '089-224711', '臺東縣臺東市興安路2段642號', 'http://www.ywv.moj.gov.tw'),
(46, '法務部矯正署臺北少年觀護所', '', '少年觀護所', '02-22611181', '新北市土城區石門路4號', 'http://www.tpj.moj.gov.tw'),
(47, '法務部矯正署臺南少年觀護所', '', '少年觀護所', '06-2150583', '臺南市南區大林路161號', 'http://www.tnj.moj.gov.tw'),
(48, '誠正中學', '', '學校', '03-5575845', '新竹縣新豐鄉松柏村20鄰德昌街231號', 'http://www.ctg.moj.gov.tw'),
(49, '明陽中學', '', '學校', '07-6152115', '高雄市燕巢區正德新村6號', 'http://www.myg.moj.gov.tw'),
(50, '法務部矯正署桃園少年輔育院', '', '少年輔育院', '03-3253152', '桃園市桃園區向善街98號', 'http://www.tyr.moj.gov.tw'),
(51, '法務部矯正署彰化少年輔育院', '', '少年輔育院', '04-8742111', '彰化縣田中鎮山腳路5段360巷170號', 'http://www.chr.moj.gov.tw');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `bankvirtualcode`
--
ALTER TABLE `bankvirtualcode`
  ADD PRIMARY KEY (`BVC_num`);

--
-- 資料表索引 `call_doc`
--
ALTER TABLE `call_doc`
  ADD PRIMARY KEY (`call_num`);

--
-- 資料表索引 `call_project`
--
ALTER TABLE `call_project`
  ADD PRIMARY KEY (`cp_num`);

--
-- 資料表索引 `cases`
--
ALTER TABLE `cases`
  ADD PRIMARY KEY (`c_num`);

--
-- 資料表索引 `dep_list`
--
ALTER TABLE `dep_list`
  ADD PRIMARY KEY (`dep_id`);

--
-- 資料表索引 `disciplinary_project`
--
ALTER TABLE `disciplinary_project`
  ADD PRIMARY KEY (`dp_num`);

--
-- 資料表索引 `drug_double_check`
--
ALTER TABLE `drug_double_check`
  ADD PRIMARY KEY (`ddc_num`);

--
-- 資料表索引 `drug_evidence`
--
ALTER TABLE `drug_evidence`
  ADD PRIMARY KEY (`e_id`);

--
-- 資料表索引 `drug_first_check`
--
ALTER TABLE `drug_first_check`
  ADD PRIMARY KEY (`df_num`);

--
-- 資料表索引 `drug_rec`
--
ALTER TABLE `drug_rec`
  ADD PRIMARY KEY (`drug_rec_num`);

--
-- 資料表索引 `drug_source`
--
ALTER TABLE `drug_source`
  ADD PRIMARY KEY (`sou_num`);

--
-- 資料表索引 `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`em_num`);

--
-- 資料表索引 `employee_test`
--
ALTER TABLE `employee_test`
  ADD PRIMARY KEY (`em_num`);

--
-- 資料表索引 `fine_card`
--
ALTER TABLE `fine_card`
  ADD PRIMARY KEY (`f_no`);

--
-- 資料表索引 `fine_change_project`
--
ALTER TABLE `fine_change_project`
  ADD PRIMARY KEY (`fcp_num`);

--
-- 資料表索引 `fine_doc`
--
ALTER TABLE `fine_doc`
  ADD PRIMARY KEY (`fd_num`);

--
-- 資料表索引 `fine_doc_delivery`
--
ALTER TABLE `fine_doc_delivery`
  ADD PRIMARY KEY (`fdd_num`);

--
-- 資料表索引 `fine_doc_publicproject`
--
ALTER TABLE `fine_doc_publicproject`
  ADD PRIMARY KEY (`fdp_num`);

--
-- 資料表索引 `fine_import`
--
ALTER TABLE `fine_import`
  ADD PRIMARY KEY (`f_no`);

--
-- 資料表索引 `fine_partpay`
--
ALTER TABLE `fine_partpay`
  ADD PRIMARY KEY (`fpart_num`);

--
-- 資料表索引 `fine_petition`
--
ALTER TABLE `fine_petition`
  ADD PRIMARY KEY (`petition_num`);

--
-- 資料表索引 `fine_project`
--
ALTER TABLE `fine_project`
  ADD PRIMARY KEY (`fp_num`);

--
-- 資料表索引 `fine_rec`
--
ALTER TABLE `fine_rec`
  ADD PRIMARY KEY (`f_num`);

--
-- 資料表索引 `fine_traf`
--
ALTER TABLE `fine_traf`
  ADD PRIMARY KEY (`ft_num`);

--
-- 資料表索引 `fine_trafproject`
--
ALTER TABLE `fine_trafproject`
  ADD PRIMARY KEY (`ftp_num`);

--
-- 資料表索引 `phone`
--
ALTER TABLE `phone`
  ADD PRIMARY KEY (`p_num`);

--
-- 資料表索引 `phone_rec`
--
ALTER TABLE `phone_rec`
  ADD PRIMARY KEY (`pr_num`);

--
-- 資料表索引 `prison_list`
--
ALTER TABLE `prison_list`
  ADD PRIMARY KEY (`prison_num`);

--
-- 資料表索引 `reward_project1`
--
ALTER TABLE `reward_project1`
  ADD PRIMARY KEY (`rp_num`);

--
-- 資料表索引 `reward_project2`
--
ALTER TABLE `reward_project2`
  ADD PRIMARY KEY (`rp_num`);

--
-- 資料表索引 `reward_project3`
--
ALTER TABLE `reward_project3`
  ADD PRIMARY KEY (`rp3_num`);

--
-- 資料表索引 `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`social_media_num`),
  ADD KEY `s_ic` (`s_ic`);

--
-- 資料表索引 `source_sm`
--
ALTER TABLE `source_sm`
  ADD PRIMARY KEY (`sou_sm_num`);

--
-- 資料表索引 `surcharges`
--
ALTER TABLE `surcharges`
  ADD PRIMARY KEY (`surc_num`);

--
-- 資料表索引 `surcharge_petition`
--
ALTER TABLE `surcharge_petition`
  ADD PRIMARY KEY (`petition_num`);

--
-- 資料表索引 `surcharge_project`
--
ALTER TABLE `surcharge_project`
  ADD PRIMARY KEY (`sp_num`);

--
-- 資料表索引 `surc_traf`
--
ALTER TABLE `surc_traf`
  ADD PRIMARY KEY (`ft_num`);

--
-- 資料表索引 `surc_trafproject`
--
ALTER TABLE `surc_trafproject`
  ADD PRIMARY KEY (`stp_num`);

--
-- 資料表索引 `suspect`
--
ALTER TABLE `suspect`
  ADD PRIMARY KEY (`s_num`);

--
-- 資料表索引 `suspect_fadai`
--
ALTER TABLE `suspect_fadai`
  ADD PRIMARY KEY (`s_fadai_num`);

--
-- 資料表索引 `suspect_punishment`
--
ALTER TABLE `suspect_punishment`
  ADD PRIMARY KEY (`sp_num`);

--
-- 資料表索引 `susp_check`
--
ALTER TABLE `susp_check`
  ADD PRIMARY KEY (`sc_num`);

--
-- 資料表索引 `upload_log`
--
ALTER TABLE `upload_log`
  ADD PRIMARY KEY (`ser`);

--
-- 資料表索引 `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`v_num`);

--
-- 資料表索引 `wordlib_courses`
--
ALTER TABLE `wordlib_courses`
  ADD PRIMARY KEY (`ser`);

--
-- 資料表索引 `wordlib_prison`
--
ALTER TABLE `wordlib_prison`
  ADD PRIMARY KEY (`prison_num`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `bankvirtualcode`
--
ALTER TABLE `bankvirtualcode`
  MODIFY `BVC_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `call_doc`
--
ALTER TABLE `call_doc`
  MODIFY `call_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `call_project`
--
ALTER TABLE `call_project`
  MODIFY `cp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `dep_list`
--
ALTER TABLE `dep_list`
  MODIFY `dep_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `disciplinary_project`
--
ALTER TABLE `disciplinary_project`
  MODIFY `dp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `drug_double_check`
--
ALTER TABLE `drug_double_check`
  MODIFY `ddc_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `drug_first_check`
--
ALTER TABLE `drug_first_check`
  MODIFY `df_num` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `drug_rec`
--
ALTER TABLE `drug_rec`
  MODIFY `drug_rec_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `drug_source`
--
ALTER TABLE `drug_source`
  MODIFY `sou_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `employees`
--
ALTER TABLE `employees`
  MODIFY `em_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `employee_test`
--
ALTER TABLE `employee_test`
  MODIFY `em_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_card`
--
ALTER TABLE `fine_card`
  MODIFY `f_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_change_project`
--
ALTER TABLE `fine_change_project`
  MODIFY `fcp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_doc_delivery`
--
ALTER TABLE `fine_doc_delivery`
  MODIFY `fdd_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_doc_publicproject`
--
ALTER TABLE `fine_doc_publicproject`
  MODIFY `fdp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_import`
--
ALTER TABLE `fine_import`
  MODIFY `f_no` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號';

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_partpay`
--
ALTER TABLE `fine_partpay`
  MODIFY `fpart_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_petition`
--
ALTER TABLE `fine_petition`
  MODIFY `petition_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_project`
--
ALTER TABLE `fine_project`
  MODIFY `fp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_rec`
--
ALTER TABLE `fine_rec`
  MODIFY `f_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_traf`
--
ALTER TABLE `fine_traf`
  MODIFY `ft_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `fine_trafproject`
--
ALTER TABLE `fine_trafproject`
  MODIFY `ftp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `phone`
--
ALTER TABLE `phone`
  MODIFY `p_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `phone_rec`
--
ALTER TABLE `phone_rec`
  MODIFY `pr_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `prison_list`
--
ALTER TABLE `prison_list`
  MODIFY `prison_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `reward_project1`
--
ALTER TABLE `reward_project1`
  MODIFY `rp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `reward_project2`
--
ALTER TABLE `reward_project2`
  MODIFY `rp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `reward_project3`
--
ALTER TABLE `reward_project3`
  MODIFY `rp3_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `social_media`
--
ALTER TABLE `social_media`
  MODIFY `social_media_num` int(30) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `source_sm`
--
ALTER TABLE `source_sm`
  MODIFY `sou_sm_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `surcharges`
--
ALTER TABLE `surcharges`
  MODIFY `surc_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `surcharge_petition`
--
ALTER TABLE `surcharge_petition`
  MODIFY `petition_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `surcharge_project`
--
ALTER TABLE `surcharge_project`
  MODIFY `sp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `surc_traf`
--
ALTER TABLE `surc_traf`
  MODIFY `ft_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `surc_trafproject`
--
ALTER TABLE `surc_trafproject`
  MODIFY `stp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `suspect`
--
ALTER TABLE `suspect`
  MODIFY `s_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `suspect_fadai`
--
ALTER TABLE `suspect_fadai`
  MODIFY `s_fadai_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `suspect_punishment`
--
ALTER TABLE `suspect_punishment`
  MODIFY `sp_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `susp_check`
--
ALTER TABLE `susp_check`
  MODIFY `sc_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `upload_log`
--
ALTER TABLE `upload_log`
  MODIFY `ser` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `v_num` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `wordlib_courses`
--
ALTER TABLE `wordlib_courses`
  MODIFY `ser` int(20) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `wordlib_prison`
--
ALTER TABLE `wordlib_prison`
  MODIFY `prison_num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
