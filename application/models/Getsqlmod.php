<?php
class getsqlmod extends CI_Model {
    function __construct() { // 建構值
        parent::__construct (); // 改寫父親
        {
			
            $this->load->database (); // 載入database資料庫設定
			$this->db->query('SET SESSION sql_mode =
                  REPLACE(REPLACE(REPLACE(
                  @@sql_mode,
                  "ONLY_FULL_GROUP_BY,", ""),
                  ",ONLY_FULL_GROUP_BY", ""),
                  "ONLY_FULL_GROUP_BY", "")');
        }
    }
    function r_office1($office){//獲得分局依據字號
            switch ($office) {
                case '中正第一分局':
                    $roffice1='中一';
                    return $roffice1;
                    break;
                case '中正第二分局':
                    $roffice1='中二';
                    return $roffice1;
                    break;
                case '文山第一分局':
                    $roffice1='文一';
                    return $roffice1;
                    break;
                case '文山第二分局':
                    $roffice1='文二';
                    return $roffice1;
                    break;
                case '信義分局':
                    $roffice1='信';
                    return $roffice1;
                    break;
                case '大安分局':
                    $roffice1='安';
                    return $roffice1;
                    break;
                case '中山分局':
                    $roffice1='中';
                    return $roffice1;
                    break;
                case '松山分局':
                    $roffice1='松';
                    return $roffice1;
                    break;
                case '大同分局':
                    $roffice1='同';
                    return $roffice1;
                    break;
                case '萬華分局':
                    $roffice1='萬';
                    return $roffice1;
                    break;
                case '南港分局':
                    $roffice1='南';
                    return $roffice1;
                    break;
                case '內湖分局':
                    $roffice1='內';
                    return $roffice1;
                    break;
                case '士林分局':
                    $roffice1='士';
                    return $roffice1;
                    break;
                case '北投分局':
                    $roffice1='投';
                    return $roffice1;
                    break;
                case '刑事警察大隊偵查第一隊':
                    $roffice1='刑大一';
                    return $roffice1;
                    break;
                case '刑事警察大隊偵查第二隊':
                    $roffice1='刑大二';
                    break;
                case '刑事警察大隊偵查第三隊':
                    $roffice1='刑大三';
                    return $roffice1;
                    break;
                case '刑事警察大隊偵查第四隊':
                    $roffice1='刑大四';
                    return $roffice1;
                    break;
                case '刑事警察大隊偵查第五隊':
                    $roffice1='刑大五';
                    return $roffice1;
                    break;
                case '刑事警察大隊偵查第七隊':
                    $roffice1='刑大七';
                    return $roffice1;
                    break;
                case '刑事警察大隊偵查第八隊':
                    $roffice1='刑大八';
                    return $roffice1;
                    break;
                case '刑事警察大隊偵查第六隊':
                    $roffice1='毒緝';
                    return $roffice1;
                    break;
                case '科偵隊':
                    $roffice1='科偵';
                    return $roffice1;
                    break;
                case '肅竊組':
                    $roffice1='肅竊';
                    return $roffice1;
                    break;
                case '少年警察隊':
                    $roffice1='少偵';
                    return $roffice1;
                    break;
                case '婦幼警察隊':
                    $roffice1='婦偵';
                    return $roffice1;
                    break;
            }
    }
    function getdata($table,$id) { // getdata副程式
        $this->db->order_by('c_num','DESC');
        $this->db->limit(1); 
        return $this->db->get('cases');
    }
    function getempno($id) { 
        $this->db->where('em_ic',$id); 
        return $this->db->get('employees');
    }
    function getempno_mail($id) { 
        $this->db->where('em_mailId',$id); 
        return $this->db->get('employees');
    }
    function getempno_code($office,$priority,$com) { 
        $this->db->where('em_roffice',$office); 
        $this->db->where('em_priority',$priority); 
        $this->db->where('em_office',$com); 
        $this->db->where('em_Isadmin',1); 
        return $this->db->get('employees');
    }
    function getempno_com($office) { 
        $this->db->where('em_office',$office); 
        $this->db->where('em_priority',2); 
        $this->db->where('em_Isadmin',1); 
        return $this->db->get('employees');
    }
    function getempno_baoda() { 
        $this->db->where('em_Isadmin','2'); 
        return $this->db->get('employees');
    }
    function getempnolist($id) { 
        $this->db->where('em_roffice',$id); 
        return $this->db->get('employees');
    }
    
    function addcases($data)
    {
        $this->db->insert('cases', $data);
    }
    function adddrug($data)
    {
        $this->db->insert('drug_evidence', $data);
    }
    function adddrug1($data)
    {
        $this->db->insert_batch('drug_first_check', $data);
    }
    function addcar($data)
    {
        $this->db->insert('vehicle', $data);
    }
    function addsocialmedia($data)
    {
        $this->db->insert_batch('social_media', $data);
    }
    function addphone_rec($data)
    {
        $this->db->insert('phone_rec', $data);
    }
    function addphone($data)
    {
        $this->db->insert('phone', $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
    }
    function adddelivery($data)
    {
        $this->db->insert('fine_doc_delivery', $data);
    }
    function adddrug2($data)
    {
        $this->db->insert_batch('drug_first_check', $data);
    }
    function addsuspect($data)
    {
        $this->db->insert('suspect', $data);
    }
    function addFR($data)
    {
        $this->db->insert('fine_rec', $data);
    }
    function addFD($data)
    {
        $this->db->insert('fine_doc', $data);
    }
    function addDD($data)
    {
        $this->db->insert('disciplinary_detail', $data);
    }
    function addddc($data)
    {
        $this->db->insert('drug_double_check', $data);
    }
    function addsc($data)
    {
        $this->db->insert('susp_check', $data);
    }
    function addSuspFadai($data)
    {
        $this->db->insert('suspect_fadai', $data);
    }
    function addSusp($data)
    {
        $this->db->insert('suspect', $data);
    }
    
    function updateddc($data)
    {
        $this->db->replace('drug_double_check', $data);
    }
    
    
    function updatesc($data)
    {
        $this->db->replace('susp_check', $data);
    }
    
    
    function getCases($id)
    {
        $this->db->where('c_num',$id); 
        return $this->db->get('cases');
    }   
    function getDrug_option()
    {
        $this->db->order_by('drug_prio','DESC');
        return $this->db->get('drug_option');
    }   
    function get1Drug_ed($id)
    {
        $this->db->where('e_c_num',$id); 
        return $this->db->get('drug_evidence');
    }     
    function get1Drug_id($id)
    {
        $this->db->where('e_id',$id); 
        return $this->db->get('drug_evidence');
    }     
    function get1Drug_ed1($id)
    {
        return $this->db->get('drug_evidence');
    }     
    function get1Drug_ic($id)//ic
    {
        $this->db->where('e_s_ic',$id); 
        return $this->db->get('drug_evidence');
    }
	function get1Drug_ic_ind($id)//ic
    {
		$this->db->select("e_id,e_name,e_count,e_unit,e_1_N_W,GROUP_CONCAT(df_level,'級',df_ingredient) as e_type");
		$this->db->join('drug_first_check', 'df_drug = e_id', 'LEFT');
        $this->db->where('e_s_ic',$id); 
		$this->db->where('df_type', '複驗');
		$this->db->group_by("e_id");
        return $this->db->get('drug_evidence');
    }     
    function getprison()
    {
        return $this->db->get('prison_list');
    }   
    function get1Susp_ed($table,$id)
    {
        $this->db->order_by('s_num','DESC');
        $this->db->limit(1); 
        $this->db->where($table,$id); 
        return $this->db->get('suspect');
    }   
    function getfineR($table,$id)
    {
        $this->db->where($table,$id); 
        return $this->db->get('fine_rec');
    }   
    function get1SuspOrder($table,$id)
    {
        $this->db->order_by('s_num','ASC');
        $this->db->where($table,$id); 
        return $this->db->get('suspect');
    }   
    function get1SuercOrder($table,$id)
    {
        $this->db->order_by('surc_num','DESC');
        $this->db->where($table,$id); 
        return $this->db->get('surcharges');
    }   
    function get1Susp_ed1($table,$id)
    {
        //$this->db->order_by('s_ic','DESC');
        //$this->db->limit(1); 
        $this->db->where($table,$id); 
        return $this->db->get('suspect');
    }   
    function get1Susp_ed2($table,$id)
    {
        $this->db->select('s_dp_project,s_cnum');
        $this->db->where($table,$id); 
        return $this->db->get('suspect');
    }   
    function get2Susp_ed($table,$id)
    {
        $this->db->order_by('sc_snum','DESC');
        $this->db->limit(1); 
        $this->db->where($table,$id); 
        return $this->db->get('susp_check');
    }   
    function getCaseslist($id)
    {
        $this->db->select('s_ic,c_num,s_date,s_time,r_county,r_district,r_zipcode,r_address,detection_process,s_office,r_office,s_place, doc_file, other_doc,drug_doc, GROUP_CONCAT(distinct s_name) as s_name,GROUP_CONCAT(e.e_name) as e_name,GROUP_CONCAT(e.e_type) as e_type', FALSE);		
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
		$this->db->join('(select * from drug_evidence where e_del_sta=0) e', 'e.e_c_num = s_num', 'LEFT',FALSE);
        $this->db->where('r_office',$id);
		// $this->db->like('s_office', $id, 'after');
        $this->db->where('delete_status',0);
		$this->db->where('c_status',null);
		$this->db->where('s_edtime > ', '2021-09-01');
        $this->db->from('cases');
        $this->db->group_by("c_num"); 
        return $this->db->get();
    }
	function getSuspslist($id)
    {
        //$this->db->select('s_ic,c_num,s_date,r_county,r_district,r_zipcode,r_address,detection_process,s_office,r_office, doc_file, other_doc, GROUP_CONCAT(s_name) as s_name');
        $this->db->where('s_cnum',NULL);
		$this->db->or_where('s_cnum','');
		$this->db->like('s_roffice', $id, 'after');
		$this->db->where('s_edtime > ', '2021-09-01');
        return $this->db->get('suspect');
    }
	function checkSuspexist($id){
		$this->db->where('s_ic', $id);
		return $this->db->get('suspect');
	}
    function getCaseslist1($id)
    {
        $this->db->select('s_ic,c_num,s_date,r_county,r_district,r_zipcode,r_address,detection_process,s_office,r_office, doc_file, other_doc, GROUP_CONCAT(s_name) as s_name');
        $this->db->where('e_empno',$id);
        $this->db->where('delete_status',0);
        $this->db->from('cases');
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        $this->db->group_by("c_num"); 
        return $this->db->get();
    }
    function getSourcelist($id)
    {
        /*$this->db->where('c_num',$id);
        $query = $this->db->get('cases');
        return $query->result();*/
        $this->db->select('*');
        $this->db->where('r_office',$id);
        $this->db->from('suspect');
        $this->db->join('cases', 'c_num = s_cnum','LEFT'); 
        return $this->db->get();
    }
    function get1Drug($id)
    {
        $this->db->select('e_state,e_place,e_doc,e_id,e_suspect,e_c_num,e_count,e_type,e_name,e_1_N_W,e_togethe,e_PNW,e_NW,e_PNWS, GROUP_CONCAT(df_ingredient ) as df_ingredient,GROUP_CONCAT(df_level ) as df_level');
        $this->db->from('drug_evidence');
        $this->db->join('drug_first_check', 'e_id = df_drug' ,'LEFT'); 
		$this->db->join('suspect', 's_num = e_c_num' ,'LEFT'); 
        $this->db->where('s_cnum',$id); 
        $this->db->where('e_del_sta',0); 
        $this->db->group_by("e_id"); 
        return $this->db->get();
    }
    function get1DrugwithIC($id)
    {
        $this->db->select('e_state,e_place,e_doc,e_id,e_suspect,e_c_num,e_count,e_type,e_name,e_1_N_W, GROUP_CONCAT(df_ingredient ) as df_ingredient,GROUP_CONCAT(df_level ) as df_level');
        $this->db->from('drug_evidence');
        $this->db->join('drug_first_check', 'e_id = df_drug' ,'LEFT'); 
        $this->db->where('e_s_ic',$id); 
        $this->db->group_by("e_id"); 
        return $this->db->get();
    }
    function get1DrugAll($id)
    {
        $this->db->where('e_id',$id); 
        return $this->db->get('drug_evidence');
    }
    function getuser($id)
    {
        //$this->db->where('e_id',$id); 
        return $this->db->get('employees');
    }
    function get1DrugCount()
    {
        $this->db->order_by('e_id','DESC');
        $this->db->limit(1); 
        return $this->db->get('drug_evidence');
    }
	function get1DrugCkfirst($id)
    {
        $this->db->where('df_drug',$id);
		$this->db->where('df_type','初驗');
        return $this->db->get('drug_first_check');
    }
	function get1DrugCksec($id)
    {
        $this->db->where('df_drug',$id);
		$this->db->where('df_type','複驗');
        return $this->db->get('drug_first_check');
    }
    function get1DrugCk($id)
    {
        $this->db->where('df_drug',$id);
		// $this->db->where('df_type','初驗');
        return $this->db->get('drug_first_check');
    }
    function get2DrugCk($table,$id)
    {
        $this->db->where($table,$id); 
        return $this->db->get('drug_double_check');
    }
    function get2SuspCk($table,$id)
    {
        $this->db->where($table,$id); 
        return $this->db->get('susp_check');
    }
    function get1DrugSusp($id)
    {
        $this->db->where('s_num',$id); 
        return $this->db->get('suspect');
    }
    function getdep($id)
    {
        $this->db->where('dep_com',$id); 
        return $this->db->get('dep_list');
    }
    function get1DrugCkdel($id)
    {
        $this->db->where('df_num',$id); 
        return $this->db->get('drug_first_check');
    }
    function deleteDrugCk($id){
        $this->db->where('df_num', $id);
        $this->db->delete('drug_first_check');
    }   
    function deleteDrugCk2($id){
        $this->db->where('ddc_num', $id);
        $this->db->delete('drug_double_check');
    }   
    function deletesuspCk2($id){
        $this->db->where('sc_num', $id);
        $this->db->delete('susp_check');
    }   
    function deleteDrugCkfor_Ed($eid,$id){
        $this->db->where('df_drug', $eid);
        $this->db->where('df_ingredient', $id);
        $this->db->delete('drug_first_check');
    }   
    function getdrugRec($id)
    {
		$this->db->join('drug_evidence','e_id = drug_rec_eid', 'LEFT');
        $this->db->where('drug_rec_office',$id); 
        $this->db->order_by('drug_rec_num',  'DESC');
        return $this->db->get('drug_rec',50);
    }
    function getdrugRecdownload($id)
    {
		$this->db->select('drug_rec.drug_rec_eid as "證物編號", drug_evidence.e_name as "證物內容", drug_rec.drug_rec_estatus as "證物狀態",drug_rec.drug_rec_eplace as "證物地點",drug_rec.drug_rec_empno as "異動人員",drug_rec.drug_rec_office as "異動單位",drug_rec.drug_rec_time as "異動時間" ', false);
		$this->db->join('drug_evidence','e_id = drug_rec_eid', 'LEFT');
        $this->db->where('drug_rec_office',$id); 
        $this->db->order_by('drug_rec_num',  'DESC');
        return $this->db->get('drug_rec');
    }
    function getdrugRecAll()
    {
		$this->db->join('drug_evidence','e_id = drug_rec_eid', 'LEFT');
        $this->db->order_by('drug_rec_num',  'DESC');
        return $this->db->get('drug_rec',50);
    }
    function getdrugRecAll_Dowload()
    {
		$this->db->select('drug_rec.drug_rec_eid as "證物編號", drug_evidence.e_name as "證物內容", drug_rec.drug_rec_estatus as "證物狀態",drug_rec.drug_rec_eplace as "證物地點",drug_rec.drug_rec_empno as "異動人員",drug_rec.drug_rec_office as "異動單位",drug_rec.drug_rec_time as "異動時間" ', false);
		$this->db->join('drug_evidence','e_id = drug_rec_eid', 'LEFT');
        $this->db->order_by('drug_rec_num',  'DESC');
        return $this->db->get('drug_rec');
    }
    function getReward_Dowload()
    {
        $this->db->order_by('drug_rec_num',  'DESC');
        return $this->db->get('drug_rec');
    }
    function get1Susp($id)
    {
        $this->db->where('s_cnum',$id); 
		// $this->db->where('s_sac_state', null);
        return $this->db->get('suspect');
    }
    function getSusp($id)//想一下把證物一階資料帶進這裡
    {
		$this->db->join('suspect', 's_num = e_c_num' ,'left'); 
        $this->db->join('cases', 'c_num = s_cnum' ,'left'); 		
        $this->db->where('r_office',$id); 
		$this->db->where('e_del_sta',0);
        $this->db->where('e_state != ','已銷毀'); 
		$this->db->where('e_state != ','單位保管中'); 
        $this->db->where(' IFNULL(e_place,"") <>','市警局'); 
        return $this->db->get('drug_evidence');
    }
	function getSuspinCases2($id, $type)//想一下把證物一階資料帶進這裡
    {
		$this->db->join('suspect', 's_num = e_c_num' ,'left'); 
        $this->db->join('cases', 'c_num = s_cnum' ,'left'); 		
        $this->db->where('r_office',$id); 
		$this->db->where('e_del_sta',0);
        $this->db->where('e_state',$type); 		
        // $this->db->where(' IFNULL(e_place,"") <>','市警局'); 
        return $this->db->get('drug_evidence');
    }
    function getSuspDrug($id, $e_state)
    {
        // $this->db->join('drug_evidence', 'c_num = e_c_num' ,'INNER'); 
		// $this->db->join('suspect','s_cnum = c_num');		
		// // $this->db->join('fine_doc','fd_snum = s_num','LEFT');
        // $this->db->where('e_state',$e_state); 
        // // $this->db->OR_where('e_state','已銷毀'); 
        // return $this->db->get('cases');
		$this->db->join('suspect', 's_num = e_c_num' ,'left'); 
        $this->db->join('cases', 'c_num = s_cnum' ,'left'); 	
		$this->db->join('fine_doc','fd_snum = s_num','LEFT');	
		$this->db->where('e_del_sta',0);
        $this->db->where('e_state',$e_state); 
		$this->db->where('s_name is not null');
		// if($e_state == '已送警察局')
		// {
		// 	$this->db->or_where('e_state','沒入物室');
		// }
        return $this->db->get('drug_evidence');
    }
    function getrewardproject1($id)//加入旧案
    {
        $this->db->where('rp_soffice',$id); 
        $this->db->where('rp_project_num',null); 
        return $this->db->get('reward_project1',10);
    }
    function getrewardproject($id)//獎金分局table
    {
        $this->db->where('rp_roffice',$id); 
        return $this->db->get('reward_project2');
    }
    function getrewardproject_normal($id)//獎金分局table
    {
        $this->db->where('rp_roffice',$id); 
        return $this->db->get('reward_project1',10);
    }
    function getrewardproject_normal2($id)//加入旧案
    {
        $this->db->where('rp_soffice',$id); 
        $this->db->where('rp_project_num',null); 
        return $this->db->get('reward_project1');
    }
    function getrewardproject_table1($id)//二阶奖金专案table（保大only）	
    {	
        $this->db->where('rp_roffice',$id); 	
        return $this->db->get('reward_project2');	
    }
    function getrewardproject_table2($id)//一阶奖金专案table
    {
        $this->db->where('rp_roffice',$id); 
        return $this->db->get('reward_project1');
    }
    function getfineproject()//帳務專案
    {
        $this->db->where('fp_status',null); 
        $this->db->order_by('fp_createdate', 'DESC');
        return $this->db->get('fine_project');
    }
    function getfineproject_done()//帳務專案-已完成專案
    {
        $this->db->where('fp_status is NOT NULL',null, FALSE); 
        $this->db->order_by('fp_createdate', 'DESC');
        return $this->db->get('fine_project');
    }
    function getfineCproject()//轉正專案
    {
        $this->db->where('fcp_status',null); 
        $this->db->order_by('fcp_no DESC, fcp_createdate DESC');
        return $this->db->get('fine_change_project');
    }
    
    function getfineproject_ed($id)//帳務專案
    {
        $this->db->where('fp_no',$id); 
        return $this->db->get('fine_project');
    }

    function getfineCproject_ed($id)//指定轉正專案
    {
        $this->db->where('fcp_no',$id); 
        return $this->db->get('fine_change_project');
    }
    
    function getrewardprojectAll()//獎金警局table(獎金審核)
    {
        $this->db->where('rp_status',NULL); 
        return $this->db->get('reward_project2');
    }
    function get3rewardprojectAll()//獎金警局table
    {
        $this->db->where('rp3_status','審核中'); 
        return $this->db->get('reward_project3');
    }
    function get3rewardprojectFin()//獎金警局已送出審核table
    {
        $this->db->where('rp3_status!=','審核中'); 
        return $this->db->get('reward_project3');
    }
    function getrewardproject3()//獎金警局table
    {
        //$this->db->where('rp_roffice',$id); 
        return $this->db->get('reward_project3',10);
    }
    
    function getrewardproject3ALl()//獎金警局table
    {
        //$this->db->where('rp_roffice',$id); 
        return $this->db->get('reward_project3');
    }
    
    function getRP3_num($id)//
    {
        $this->db->where('rp3_num',$id); 
        return $this->db->get('reward_project3');
    }
    
    function getdpproject($id)
    {
        $this->db->where('dp_status',null); 
		$this->db->not_like('dp_name', 'P', 'after');
        $this->db->order_by('dp_num', 'desc');
        return $this->db->get('disciplinary_project');
    }
    function getdpproject123($id)//用num 讀取
    {
        $this->db->where('dp_num',$id); 
        return $this->db->get('disciplinary_project');
    }
    function getcpproject($id)
    {
        $this->db->where('cp_status',null); 
        return $this->db->get('call_project');
    }
    function getftproject($id)
    {
        $this->db->where('ftp_status',null); 
        return $this->db->get('fine_trafproject');
    }
    function getstproject($id)
    {
        $this->db->where('stp_status',null); 
        return $this->db->get('surc_trafproject');
    }
    function getcpprojecttype($id)
    {
        $this->db->where('cp_name',$id); 
        return $this->db->get('call_project');
    }
    function getftprojecttype($id)
    {
        $this->db->where('ftp_name',$id); 
        return $this->db->get('fine_trafproject');
    }
    function getstprojecttype($id)
    {
        $this->db->where('stp_name',$id); 
        return $this->db->get('surc_trafproject');
    }
    function getfdproject1()
    {
		$this->db->join('disciplinary_project','disciplinary_project.dp_projectpublic_num = fine_doc_publicproject.fpd_no');
        // $this->db->where('fpd_status',null); 
        return $this->db->get('fine_doc_publicproject');
    }
    function getfdproject($id)
    {
        return $this->db->get('fine_doc_publicproject');
    }
	function getfdprojectbyid($id)
    {
		$this->db->where('fdp_num',$id); 
        return $this->db->get('fine_doc_publicproject');
    }
    function getdpproject_Ready($id)
    {
        $this->db->where('dp_status!=',null); 
        $this->db->where('dp_status!=','已寄出'); 
		$this->db->not_like('dp_name', 'P', 'after');
        $this->db->order_by('dp_num', 'desc');
        return $this->db->get('disciplinary_project');
    }
    function get3Susp($id)
    {
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 's_num = sp_snum ' ,'LEFT'); 
        $this->db->where('r_office',$id); 
        $this->db->where('s_sac_state','待裁罰'); 
        $this->db->or_where('s_sac_state','退回補正'); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('cases');
    }
    function getdp($id)
    {
        $this->db->join('cases', 'c_num = s_cnum' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 's_num = sp_snum ' ,'LEFT'); 
        //$this->db->where('s_date >=',date("Y-m-d",strtotime('-3 month'))); //正式版要關掉
        $this->db->where('s_sac_state','確認送出裁罰'); 
        $this->db->where('s_dp_project ',null); 
		// $this->db->where('suspect.s_num NOT IN (select fd_snum from fine_doc)',NULL,FALSE);
        $this->db->order_by('s_edtime', 'desc');
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    
    function getCallList()
    {
		return $this->db->query("select * from suspect susp 
		left join fine_doc fd on susp.s_num = fd.fd_snum
		left join fine_doc_delivery fdd on susp.s_num = fdd.fdd_snum
		where fd.fd_limitdate < '".date('Y-m-d')."' and fd_limitdate is not NULL and fd.fd_amount > 0 and fd.fd_cnum not in (select call_cnum from call_doc) or fd.fd_num in (select f_caseid from fine_import where f_donedate <> '0000-00-00')");
        // $this->db->join('call_doc', 'call_snum = s_num' ,'LEFT'); 
        // $this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        // $this->db->join('fine_doc_delivery', 'fdd_snum = s_num' ,'LEFT'); 
        // $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
		// $this->db->join('fine_import', 'f_caseid = fd_num', 'RIGHT');
        // $this->db->where('call_projectnum',NULL); 
        // $this->db->where('fd_amount >', 0); 
		// $this->db->where('f_donedate <>', '0000-00-00'); 
        // $where = "`fd_amount`>`f_payamount`";
        // $this->db->where($where); 
		// $this->db->where('fd_limitdate is not ', NULL); 
        // $this->db->where('fd_limitdate < ', date('Y-m-d')); 
        //$this->db->group_by("c_num"); 
        // return $this->db->get('suspect');
    }
    function getFTList()
    {
        $this->db->join('call_doc', 'call_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc_delivery', 'fdd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_traf', 'ft_snum = s_num' ,'LEFT'); 
        $this->db->where('ft_projectnum',NULL); 
        $this->db->where('f_damount >', 0); 
        $where = "`f_damount`>`f_payamount`";
        $this->db->where($where); 
        $this->db->where('f_date < ', date('Y-m-d')); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getFTListFine()
    {
        $this->db->join('call_doc', 'call_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc_delivery', 'fdd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_traf', 'ft_snum = s_num' ,'LEFT'); 
        $this->db->where('ft_projectnum',NULL); 
        //$this->db->where('f_damount >', 0); 
        $where = "`f_damount`>`f_payamount`";
        $this->db->where($where); 
        //$this->db->where('call_grace < ', date('Y-m-d')); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getFTListSurc()
    {
        $this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('surcharges', 'surc_snum = s_num' ,'LEFT'); 
        $this->db->join('surc_traf', 'ft_snum = s_num' ,'LEFT'); 
        $this->db->where('ft_projectnum',NULL); 
        $this->db->where('f_samount >', 0); 
        $where = "`f_samount`>`f_payamount`";
        $this->db->where($where); 
        $this->db->where('surc_findate < ', date('Y-m-d')); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getFTListfromProject($id)
    {
        $this->db->join('call_doc', 'call_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc_delivery', 'fdd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_traf', 'ft_snum = s_num' ,'LEFT'); 
        $this->db->where('ft_projectnum',$id); 
        $this->db->where('f_damount >', 0); 
        $where = "`f_damount`>`f_payamount`";
        $this->db->where($where); 
        $this->db->where('f_date < ', date('Y-m-d')); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getFTListfromProjectSurc($id)
    {
        //$this->db->join('call_doc', 'call_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        //$this->db->join('fine_doc_delivery', 'fdd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('surc_traf', 'ft_snum = s_num' ,'LEFT'); 
        $this->db->join('surcharges', 'surc_snum = s_num' ,'LEFT'); 
        $this->db->where('ft_projectnum',$id); 
        $this->db->where('f_samount >', 0); 
        $where = "`f_samount`>`f_payamount`";
        $this->db->where($where); 
        $this->db->where('surc_findate < ', date('Y-m-d')); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getCallListfromProject($id)
    {
        $this->db->join('call_doc', 'call_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc_delivery', 'fdd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->where('call_no', $id); 
        //$this->db->where('f_damount >', 0); 
        //$where = "`f_damount`>`f_payamount`";
        //$this->db->where($where); 
        //$this->db->where('f_date < ', date('Y-m-d')); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    
    function getCalldetail($id)
    {
        $this->db->join('call_doc', 'call_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc_delivery', 'fdd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('surcharges', 'surc_snum = s_num' ,'LEFT'); 
        $this->db->where('f_snum', $id); 
        return $this->db->get('suspect');
    }
    function getSurcTdetail($id)
    {
        $this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('surcharges', 'surc_snum = s_num' ,'LEFT'); 
        $this->db->where('surc_no', $id); 
        return $this->db->get('suspect');
    }
    function getfine_trafwithsnum($id)
    {
        $this->db->where('ft_snum', $id); 
        return $this->db->get('fine_traf');
    }
    function getsurcTrac_withsnum($id)
    {
        $this->db->where('ft_surcnum', $id); 
        return $this->db->get('surc_traf');
    }
    function getCalldelivery()
    {
        $this->db->join('call_doc', 'call_snum = s_num' ,'INNER'); 
        $this->db->join('call_project', 'cp_name = call_projectnum' ,'INNER'); 
        //$this->db->join('fine_rec', 'f_snum = s_num' ,'LEFT'); 
        //$this->db->join('fine_doc_delivery', 'fdd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'INNER'); 
        //$this->db->where('f_date >=','2020-08-01'); 
        //$this->db->where('f_damount >', 0); 
        //$where = "`f_damount`>`f_payamount`";
        //$this->db->where($where); 
        $this->db->where('cp_status', '寄出'); 
        return $this->db->get('suspect');
    }
    function getdeliverylist()//送達登入列表
    {
        $this->db->join('disciplinary_project', 's_dp_project = dp_name' ,'LEFT'); 
        $this->db->join('fine_doc ', 's_num = fd_snum' ,'LEFT'); 
        $this->db->join('fine_doc_delivery ', 'fdd_fdnum = fd_num' ,'LEFT'); 
        $this->db->where('dp_status','已寄出'); 
		$this->db->where('fdd_status !=', '已送達');
		// $this->db->or_where('fdd_status is null');
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getsurchangelist()//點收怠金案件table
    {
        // $this->db->join('suspect ', 's_num = surc_snum ' ,'INNER'); 
        // $this->db->join('cases ', 'c_num = surc_cnum ' ,'INNER'); 
        // $this->db->join('fine_doc ', 'fd_listed_no = surc_listed_no ' ,'INNER'); 
        // $this->db->join('phone ', 'p_s_num = surc_snum ' ,'INNER'); 
        // $this->db->join('fine_rec ', 'f_snum = surc_snum ' ,'inner'); //過後改成inner
        // $this->db->join('fine_doc_delivery ', 'fdd_snum = surc_snum ' ,'left'); //過後改成inner
		$this->db->where('surc_rollback !=', 1);
        $this->db->where('surc_projectnum',NULL); 
		$this->db->order_by('surc_num', 'asc');
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }
    
    function getsurchangelisttable()//怠金專案詳細列表
    {
        $this->db->join('suspect ', 's_num = surc_snum ' ,'INNER'); 
        $this->db->join('cases ', 'c_num = surc_cnum ' ,'INNER'); 
        //$this->db->join('fine_doc ', 'fd_listed_no = surc_listed_no ' ,'INNER'); 
       // $this->db->join('phone ', 'p_s_num = surc_snum ' ,'INNER'); 
        //$this->db->join('fine_rec ', 'f_snum = surc_snum ' ,'left'); //過後改成inner
        //$this->db->join('fine_doc_delivery ', 'fdd_snum = surc_snum ' ,'left'); //過後改成inner
        $this->db->join('surcharge_project ', 'sp_no = surc_projectnum ' ,'left'); 
        $this->db->where('sp_status!=',null); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }
    function getsurchangelist1($id)//怠金專案詳細列表
    {
        // $this->db->join('suspect ', 's_num = surc_snum ' ,'INNER'); 
        // $this->db->join('cases ', 'c_num = surc_cnum ' ,'INNER'); 
        // $this->db->join('fine_doc ', 'fd_listed_no = surc_listed_no ' ,'INNER'); 
        // $this->db->join('phone ', 'p_s_num = surc_snum ' ,'INNER'); 
        // //$this->db->join('fine_rec ', 'f_snum = surc_snum ' ,'INNER'); //過後改成inner
        // $this->db->join('fine_doc_delivery ', 'fdd_snum = surc_snum ' ,'INNER'); //過後改成inner
        $this->db->where('surc_projectnum',$id); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }
    function getsurchangelistSP($id)//怠金專案詳細列表
    {
        // $this->db->join('suspect ', 's_num = surc_snum ' ,'INNER'); 
        // $this->db->join('cases ', 'c_num = surc_cnum ' ,'INNER'); 
        // $this->db->join('fine_doc ', 'fd_listed_no = surc_listed_no ' ,'INNER'); 
        // $this->db->join('phone ', 'p_s_num = surc_snum ' ,'INNER'); 
        // $this->db->join('fine_rec ', 'f_snum = surc_snum ' ,'left'); //過後改成inner
        // $this->db->join('fine_doc_delivery ', 'fdd_snum = surc_snum ' ,'left'); //過後改成inner
        $this->db->join('surcharge_project ', 'sp_no = surc_projectnum ' ,'left'); 
        $this->db->where('surc_projectnum',$id); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }
    function getsurchangelist2($id)//怠金專案詳細列表
    {
        $this->db->join('suspect ', 's_num = surc_snum ' ,'INNER'); 
        $this->db->join('cases ', 'c_num = surc_cnum ' ,'INNER'); 
        $this->db->join('fine_doc ', 'fd_listed_no = surc_listed_no ' ,'INNER'); 
        $this->db->join('phone ', 'p_s_num = surc_snum ' ,'INNER'); 
        $this->db->join('fine_rec ', 'f_snum = surc_snum ' ,'left'); //過後改成inner
        $this->db->join('fine_doc_delivery ', 'fdd_snum = surc_snum ' ,'left'); //過後改成inner
        $this->db->where('surc_no',$id); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }
    function getpetitionlist()//獲得petition資料
    {
        $this->db->join('disciplinary_project', 's_dp_project = dp_name' ,'LEFT'); 
        $this->db->join('fine_doc ', 's_num = fd_snum ' ,'LEFT'); 
        $this->db->join('fine_doc_delivery ', 'fdd_fdnum = fd_num ' ,'LEFT'); 
        $this->db->join('fine_petition ', 'petition_fdnum = fd_num ' ,'LEFT'); 
        $this->db->where('dp_status','已寄出'); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getdeliverylist_forpublic()//公示送達選擇列表(戶籍地未送達)
    {
        $this->db->join('disciplinary_project', 's_dp_project = dp_name' ,'LEFT'); 
        $this->db->join('fine_doc ', 's_num = fd_snum ' ,'LEFT'); 
        $this->db->join('fine_doc_delivery ', 'fdd_fdnum = fd_num ' ,'LEFT'); 
        $this->db->where('dp_status','已寄出'); 
        $this->db->where('fdd_status','戶籍地未送達'); 
        $this->db->where('fdd_projectpublic_num',null); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getdeliverylist_forpublicProject($id)//公示送達選擇列表(戶籍地未送達)
    {
        $this->db->join('disciplinary_project', 's_dp_project = dp_name' ,'LEFT'); 
        $this->db->join('fine_doc ', 's_num = fd_snum ' ,'LEFT'); 
        $this->db->join('fine_doc_delivery ', 'fdd_fdnum = fd_num ' ,'LEFT'); 
        //$this->db->where('dp_status','已寄出'); 
        //$this->db->where('fdd_status','戶籍地未送達'); 
        $this->db->where('dp_num',$id); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getdeliverylist_forpublicProjectSurc($id)//公示送達選擇列表(戶籍地未送達)
    {
        //$this->db->join('disciplinary_project', 's_dp_project = dp_name' ,'LEFT'); 
        $this->db->join('fine_doc ', 's_num = fd_snum ' ,'LEFT'); 
        $this->db->join('surcharges', 's_num = surc_snum ' ,'LEFT'); 
        //$this->db->where('dp_status','已寄出'); 
        //$this->db->where('fdd_status','戶籍地未送達'); 
        $this->db->where('surc_projectnum',$id); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getdelivery($table,$id)//送達登入列表
    {
        $this->db->where($table,$id); 
        return $this->db->get('fine_doc_delivery');
    }
    
    function getsuspect($table,$id)//犯嫌人
    {
        $this->db->where($table,$id); 
        return $this->db->get('suspect');
    }
    function getdp2($id)
    {
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        //$this->db->join('fine_doc', 'c_num = s_cnum' ,'inner'); 
        //$this->db->join('suspect_punishment ', 's_num = sp_snum ' ,'LEFT'); 
        //$this->db->where('r_office',$id); 
        $this->db->where('s_sac_state','確認送出裁罰'); 
        $this->db->where('s_dp_project ',$id); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('cases');
    }
    
    function getfp3($id)
    {
        $this->db->join('cases', 'c_num = s_cnum' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'left'); 
        $this->db->where('s_sac_state','確認送出裁罰'); 
        $this->db->where('s_dp_project ',$id); 
        //$this->db->limit(1); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    function getdpprojectlist($id)
    {
        //$this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        $this->db->where('s_dp_project ',$id); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
    
    function getfdprojectlist($id)
    {
        //$this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        $this->db->where('fdd_projectpublic_num ',$id); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('fine_doc_delivery');
    }
    function getpetition($id)
    {
        $this->db->where('petition_fdnum',$id); 
        return $this->db->get('fine_petition');
    }
    
    function getpetitionSurc($id)
    {
        $this->db->where('petition_surcnum',$id); 
        return $this->db->get('surcharge_petition');
    }
    function get3RewardProject($id1,$id2)
    {
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        /*if($this -> session -> userdata('uoffice') == "保安警察大隊" ) {
            $this->db->like('s_office', '第', 'after');
            $this->db->like('s_office', '中隊', 'before');
        }
        else if($this -> session -> userdata('uroffice') == "特勤中隊" ) $this->db->where('s_office',$id1);
        //else $this->db->where('r_office',$id1); */
        $this->db->where('s_reward_project',$id2); 
        return $this->db->get('cases');
    }
    function get3RewardProject2($id1,$id2)	
    {	
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 	
        /*if($this -> session -> userdata('IsAdmin') == "2") {	
            $this->db->like('s_office', '第', 'after');	
            $this->db->like('s_office', '中隊', 'before');	
        }	
        else if($this -> session -> userdata('uroffice') == "特勤中隊" ) $this->db->where('s_office',$id1);	
        else if($this -> session -> userdata('uoffice') == "刑事警察大隊" ) $this->db->where('r_office',$this -> session -> userdata('uofficeAll'));	
        else {	
            $this->db->where('r_office',$id1); 	
           //$this->db->not_like('s_office','中隊'); 	
        }*/	
        $this->db->where('s_reward_project2',$id2); 	
        return $this->db->get('cases');	
    }
    function get3RewardProject3($id1,$id2)
    {
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 's_num = sp_snum ' ,'LEFT'); 
        //if($id2=='保安警察大隊')$this->db->like('s_office','中隊'); 
        //else $this->db->where('r_office',$id2); 
        $this->db->where('s_reward_project2',$id1); 
        return $this->db->get('cases');
    }
    function get3RewardProject3_n($id2)
    {
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 's_num = sp_snum ' ,'LEFT'); 
        //$this->db->where('r_office',$id1); 
        $this->db->where('s_reward_project3',$id2); 
        return $this->db->get('cases');
    }
    function get3RewardProject3_search($name,$ic,$firstDate,$secondDate)
    {
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 's_num = sp_snum ' ,'LEFT'); 
        $this->db->like('s_name',$name); 
        $this->db->where('s_reward_date >=', $firstDate);
        $this->db->where('s_reward_date <=', $secondDate); 
        //if($ic!="")$this->db->where('s_ic',$ic); 
        return $this->db->get('cases');
    }
    function get3Petition_search($name,$ic,$fd_num,$first,$last)
    {
        $this->db->join('suspect', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_petition', 'fd_num = petition_fdnum' ,'LEFT'); 
        $this->db->like('s_name',$name); 
        $this->db->like('fd_num',$fd_num); 
        $this->db->where('fd_date >=',$first);//(date('Y')-1).'-'.date('m-d'));
        $this->db->where('fd_date <=', $last); 
        if($ic!="")$this->db->where('s_ic',$ic); 
        return $this->db->get('fine_doc');
    }
    
    function getSurcPetition_search($name,$ic,$surc_no,$first,$last)
    {
        $this->db->join('suspect', 'surc_snum = s_num' ,'left'); 
        $this->db->join('surcharge_petition', 'surc_no = petition_surcnum' ,'LEFT'); 
        $this->db->like('s_name',$name); 
        $this->db->like('surc_no',$surc_no); 
        $this->db->where('surc_date >=',$first);//(date('Y')-1).'-'.date('m-d'));
        $this->db->where('surc_date <=', $last); 
        if($ic!="")$this->db->where('s_ic',$ic); 
        return $this->db->get('surcharges');
    }
    function getfine_search($name,$ic,$BVC,$cnum,$s_go)//,$firstDate,$secondDate
    {
        $this->db->join('suspect', 'f_snum = s_num' ,'LEFT'); 
        //$this->db->join('surcharges ', 'f_surcno = surc_no ' ,'LEFT'); 
        //$this->db->join('fine_doc', 'f_fdno = fd_num ' ,'LEFT'); 
        $this->db->join('surcharges ', 'f_snum = surc_snum' ,'LEFT'); 
        $this->db->join('fine_doc', 'f_snum = fd_snum' ,'LEFT'); 
        $this->db->like('s_name',$name); 
        $this->db->like('f_BVC',$BVC); 
        if(substr($cnum,0,1)=="A") $this->db->like('surc_no',$cnum); 
        else $this->db->like('fd_num',$cnum); 
        //$this->db->like('ft_no',$s_go); 
        //$this->db->where('f_date >=', $firstDate);
        //$this->db->where('f_date <=', $secondDate); 
        if($ic!="")$this->db->where('s_ic',$ic); 
        $this->db->where('f_project_num',null); 
        return $this->db->get('fine_rec');
    }
    function getfine_search2($name,$ic,$BVC,$cnum,$type,$firstDate,$secondDate,$where,$where1,$where2,$where3)//,$firstDate,$secondDate
    {
        $this->db->join('suspect', 'f_snum = s_num' ,'LEFT'); 
        //$this->db->join('surcharges ', 'f_surcno = surc_no ' ,'LEFT'); 
        $this->db->join('fine_traf ', 'f_snum = ft_snum ' ,'RIGHT'); 
        $this->db->join('surcharges ', 'f_snum = surc_snum' ,'LEFT'); 
        $this->db->join('fine_doc', 'f_snum = fd_snum' ,'LEFT'); 
        $this->db->join('fine_project ', 'fp_no = f_project_num ' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 'f_snum = sp_snum ' ,'LEFT'); 
        $this->db->like('s_name',$name); 
        $this->db->like('f_BVC',$BVC); 
        $this->db->like('s_cnum',$cnum); 
        $this->db->where('f_date >=', $firstDate);
        $this->db->where('f_date <=', $secondDate); 
        if($type == '罰緩'){
            $type ="surc_no regexp '^A'";
            $this->db->where($type);} 
        else if($type == '怠金'){
            $type ="SUBSTR(fd_num,1,1) in (1,2,3,4,5,6,7,8,9,0)";
            $this->db->where($type);
        }
        if($ic!="")$this->db->where('s_ic',$ic); 
        //$this->db->where('fp_type != ',$type); 
        //$this->db->where('fcp_no',NULL); 
        if($where!=""){
            $this->db->where($where);
            //$this->db->where($where1);
        } 
        if($where2!=""){
            //$this->db->where($where2);
            //$this->db->where($where3);
        } 
        return $this->db->get('fine_rec');
    }
    function getfine_search3($name,$ic,$BVC,$cnum,$type,$firstDate,$secondDate,$where)//,$firstDate,$secondDate
    {
        $this->db->join('suspect', 'f_snum = s_num' ,'INNER'); 
        //$this->db->join('fine_project ', 'fp_no = f_project_num ' ,'LEFT'); 
        $this->db->join('fine_traf ', 'f_snum = fine_traf.ft_snum ' ,'inner'); 
        $this->db->join('surc_traf ', 'surc_traf.ft_snum = f_snum ' ,'inner'); 
        $this->db->join('fine_doc ', 'f_snum = fd_snum ' ,'inner'); 
        /*$this->db->like('s_name',$name); 
        $this->db->like('f_BVC',$BVC); 
        $this->db->like('s_cnum',$cnum); 
        $this->db->where('f_date >=', $firstDate);
        $this->db->where('f_date <=', $secondDate); 
        if($ic!="")$this->db->where('s_ic',$ic); 
        //$this->db->where('fp_type!=',$type); 
        //$this->db->where('fcp_no',NULL); 
        //if($where!="")$this->db->where($where); */
        return $this->db->get('fine_rec');
    }
    function getFinewithf_num($id)//,$firstDate,$secondDate
    {
        $this->db->join('suspect', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_project ', 'fp_no = f_project_num ' ,'LEFT'); 
        $this->db->join('fine_traf ', 'f_snum = ft_snum ' ,'inner'); 
        $this->db->join('fine_doc ', 'f_snum = fd_snum ' ,'inner'); 
        $this->db->where('f_num',$id); 
        // $this->db->from('fine_rec');
        //if($where!="")$this->db->where($where); 
        return $this->db->get('fine_rec');
        // return $this->db->get_compiled_select();
    }
    function getfine_list_complete($id)//帳務專案內案件
    {
        $this->db->join('suspect', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_project ', 'fp_no = f_project_num ' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 'f_snum = sp_snum ' ,'LEFT'); 
        //$this->db->where('r_office',$id1); 
        $this->db->where('fp_status','簽核完成'); 
        return $this->db->get('fine_rec');
    }
    function getfine_list($id)//帳務專案內案件
    {
        $this->db->join('suspect', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'f_snum = fd_snum ' ,'LEFT'); 
        $this->db->join('fine_traf ', 'f_snum = ft_snum ' ,'Left'); 
        $this->db->join('surcharges', 'f_snum = surc_snum ' ,'LEFT'); 
        //$this->db->where('r_office',$id1); 
        $this->db->where('f_project_num',$id); 
        return $this->db->get('fine_rec');
    }
    function getfine_list1($id)//帳務專案內案件
    {
        $this->db->select('*');
        $this->db->join('fine_doc', 'f_snum = fd_snum ' ,'LEFT'); 
        $this->db->join('surcharges', 'f_snum = surc_snum ' ,'LEFT'); 
        $this->db->where('f_project_num',$id); 
        return $this->db->get('fine_rec');
    }
    function getfine_list2($id)//帳務專案內案件
    {
        $this->db->select('*');
        $this->db->where('f_num',$id); 
        return $this->db->get('fine_rec');
    }
    function getfine_Clist($id)//轉正專案內案件
    {
        $this->db->join('suspect', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('surcharges', 'surc_snum = s_num' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 'f_snum = sp_snum ' ,'LEFT'); 
        $this->db->join('fine_change_project ', '(fine_change_project.fcp_no) = (fine_rec.fcp_no) ' ,'LEFT'); 
        //$this->db->where('r_office',$id1); 
        $this->db->where('(fine_rec.fcp_no)',$id); 
        return $this->db->get('fine_rec');
    }
    function getfine_Clistfd($id)//帳務專案內案件
    {
        // echo 'getfine_Clistfd';
        $this->db->join('suspect', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_traf', 'ft_snum = s_num' ,'LEFT'); 
        $this->db->join('surcharges', 'surc_snum = s_num' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 'f_snum = sp_snum ' ,'LEFT'); 
        $this->db->join('fine_change_project ', '(fine_change_project.fcp_no) = (fine_rec.fcp_no) ' ,'LEFT'); 
        //$this->db->where('r_office',$id1); 
        $this->db->where('(fd_num)',$id); 
        return $this->db->get('fine_rec');
    }
    function getfine_Clistsurc($id)//帳務專案內案件
    {
        // echo 'getfine_Clistsurc';
        $this->db->join('suspect', 'f_snum = s_num' ,'LEFT'); 
        $this->db->join('fine_doc', 'fd_snum = s_num' ,'LEFT'); 
        $this->db->join('surcharges', 'surc_snum = s_num' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 'f_snum = sp_snum ' ,'LEFT'); 
        $this->db->join('fine_change_project ', '(fine_change_project.fcp_no) = (fine_rec.fcp_no) ' ,'LEFT'); 
        //$this->db->where('r_office',$id1); 
        $this->db->where('(surc_no)',$id); 
        return $this->db->get('fine_rec');
    }
    function getfine_partpay($id)//帳務專案內案件
    {
        $this->db->where('fpart_fpnum',$id); 
        return $this->db->get('fine_partpay');
    }
    function getSuspReward_normal($id)
    {
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        if($this -> session -> userdata('IsAdmin') == "2") {
            $this->db->like('s_office', '第', 'after');
            $this->db->like('s_office', '中隊', 'before');
        }
        else if($this -> session -> userdata('uoffice') == "刑事警察大隊" && $this -> session -> userdata('uroffice') != "特勤中隊") $this->db->where('r_office',$this -> session -> userdata('uofficeAll')); 
        else if($this -> session -> userdata('uroffice') == "警備隊") $this->db->where('r_office',$this -> session -> userdata('uoffice')); 
        else $this->db->where('s_office',$id); 
        //$this->db->where('s_office',$id); 
        $this->db->where('s_name != ',null); 
        $this->db->where('s_reward_project',null); 
        $this->db->where('s_reward_project2',null); 
        $this->db->where('s_reward_project3',null); 
        return $this->db->get('cases');
    }
    function get3SuspReward($id)
    {
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        if($this -> session -> userdata('IsAdmin') == "2") {
            $this->db->like('s_office', '第', 'after');
            $this->db->like('s_office', '中隊', 'before');
        }
        else if($this -> session -> userdata('uoffice') == "刑事警察大隊") $this->db->where('r_office',$this -> session -> userdata('uofficeAll')); 
        else if($this -> session -> userdata('uroffice') == "警備隊") $this->db->where('r_office',$this -> session -> userdata('uoffice')); 
        else $this->db->where('r_office',$id); 
        //$this->db->where('s_office',$id); 
        $this->db->where('s_name != ',null); 
        $this->db->where('s_reward_project',null); 
        $this->db->where('s_reward_project2',null); 
        $this->db->where('s_reward_project3',null); 
        return $this->db->get('cases');
    }
    function get3Susp_other($id)
    {
        $this->db->join('suspect', 'c_num = s_cnum' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 's_num = sp_snum ' ,'LEFT'); 
        $this->db->where('r_office',$id); 
        $this->db->where('s_sac_state','免裁罰'); 
        $this->db->or_where('s_sac_state','移送法院'); 
        return $this->db->get('cases');
    }
    function get3SP1($id){
        $this->db->where('fd_snum',$id); 
        return $this->db->get('fine_doc');
    }
    function getfdAll(){//用於查看最後的編號
        $this->db->order_by('fd_num','DESC');
        $this->db->limit(1); 
        return $this->db->get('fine_doc');
    }
    function getfine_traf()
    {
        $this->db->order_by('ft_no','DESC');
        return $this->db->get('fine_traf');
    }
    function getSurc_traf()
    {
        $this->db->order_by('ft_no','DESC');
        return $this->db->get('surc_traf');
    }
    function getsurcAll(){//用於查看最後的編號
        $this->db->order_by('surc_no','DESC');
        $this->db->limit(1); 
        return $this->db->get('surcharges');
    }
    function getsurc($id){//用於查看最後的編號
        $this->db->where('surc_projectnum',$id);
        return $this->db->get('surcharges');
    }
    function getsurc1($id){//用於查看最後的編號
        $this->db->where('surc_no',$id);
        return $this->db->get('surcharges');
    }
    function getRP1($id){
        $this->db->where('rp_name',$id); 
        return $this->db->get('reward_project1');
    }
    function getRP2($id){
        $this->db->where('rp_name',$id); 
        return $this->db->get('reward_project2');
    }
    function getRP1_num2($id){
        $this->db->where('rp_num',$id); 
        return $this->db->get('reward_project2');
    }
    function getRP1_num($id){
        $this->db->where('rp_num',$id); 
        return $this->db->get('reward_project1');
    }
    function getDP1($id){
        $this->db->where('dp_name',$id); 
        return $this->db->get('disciplinary_project');
    }
	function getDP1bynum($id){
        $this->db->where('dp_num',$id); 
        return $this->db->get('disciplinary_project');
    }
    function getSurcharge_Project(){
        $this->db->where('sp_status',null); 
		$this->db->where('sp_type is not NULL');
		$this->db->not_like('sp_no', 'P', 'after');
        $this->db->order_by('sp_num', 'desc');
        return $this->db->get('surcharge_project');
    }
	function getSurcharge_normal_Project(){
        $this->db->where('sp_status',null); 
		$this->db->where('sp_type', '裁罰');
		$this->db->not_like('sp_no', 'P', 'after');
        $this->db->order_by('sp_time', 'asc');
        return $this->db->get('surcharge_project');
    }
	function getSurcharge_prison_Project(){
		$this->db->where('sp_status',null); 
        $this->db->where('sp_type','在監'); 
		$this->db->not_like('sp_no', 'P', 'after');
        $this->db->order_by('sp_num', 'desc');
        return $this->db->get('surcharge_project');
    }
	function getSurcharge_return_Project(){
		$this->db->where('sp_status',null); 
        $this->db->where('sp_type','不裁罰'); 
		$this->db->or_where('sp_type','在監'); 
		$this->db->not_like('sp_no', 'P', 'after');
        $this->db->order_by('sp_time', 'asc');
        return $this->db->get('surcharge_project');
    }
	function getSurcharge_send_Project(){
        $this->db->where('sp_status','送批'); 
		$this->db->or_where('sp_status','已送批'); 
		$this->db->not_like('sp_no', 'P', 'after');
        $this->db->order_by('sp_num', 'desc');
        return $this->db->get('surcharge_project');
    }
	function getSurcharge_rb_Project(){
		$this->db->where('sp_status',null); 
		$this->db->like('sp_no', 'P', 'after');
        $this->db->order_by('sp_num', 'desc');
        return $this->db->get('surcharge_project');
    }
	
	function getSurcharge_public_Project(){
		$this->db->where('sp_status','公示'); 
		$this->db->or_where('sp_status','公示送達');
        $this->db->order_by('sp_num', 'desc');
        return $this->db->get('surcharge_project');
    }
    function getSurcharge_Project1($id){
        $this->db->where('sp_no',$id); 
        return $this->db->get('surcharge_project');
    }
	function getSurcharge_Project2($id){
        $this->db->where('sp_num',$id); 
        return $this->db->get('surcharge_project');
    }
    function updateSurcharge_Project_sp($id,$data){
        $this->db->set('sp_status', $data);
        $this->db->where('sp_num',$id); 
        $this->db->update('surcharge_project');
    }
    function updateCall_Project_sp($id,$data){
        $this->db->set('cp_status', $data);
        $this->db->where('cp_name',$id); 
        $this->db->update('call_project');
    }
    function updatestp_sp($id,$data){
        $this->db->set('stp_status', $data);
        $this->db->where('stp_name',$id); 
        $this->db->update('surc_trafproject');
    }
    function get3Drug($id)
    {
        $this->db->join('drug_double_check ', 'e_id = ddc_drug ' ,'INNER'); 
        $this->db->where('e_s_ic',$id); 
        return $this->db->get('drug_evidence');
    }
	function get3Drugfirst($id)
    {
        $this->db->join('drug_first_check ', 'e_id = df_drug ' ,'INNER'); 
		$this->db->where('df_type', '複驗');
        $this->db->where('e_c_num',$id); 
        return $this->db->get('drug_evidence');
    }
    function get3Drugtest($id,$id2)
    {
        $this->db->join('drug_double_check ', 'e_id = ddc_drug ' ,'INNER'); 
        $this->db->where('e_s_ic',$id); 
        $this->db->where('e_c_num',$id2); 
        return $this->db->get('drug_evidence');
    }
    function get3Drug2($id)
    {
        $this->db->join('drug_double_check ', 'e_id = ddc_drug ' ,'INNER'); 
        $this->db->where('e_id',$id);
        return $this->db->get('drug_evidence');
    }
	function get3Drug2Double($id)
    {
        $this->db->join('drug_first_check ', 'e_id = df_drug ' ,'INNER'); 
		$this->db->where('df_type', '複驗');
        $this->db->where('e_id',$id);
        return $this->db->get('drug_evidence');
    }
    function get2Susp($id)
    {
        $this->db->where('sc_snum',$id);
        $this->db->from('susp_check');
        //$this->db->join('suspect', 'sc_cnum = s_cnum' ,'LEFT'); 
        //$this->db->group_by("s_num"); 
        return $this->db->get();
    }
    function getSusped($id,$id2)
    {
        /*$this->db->where('c_num',$id);
        $query = $this->db->get('cases');
        return $query->result();*/
        $this->db->select('s_ic,s_num,s_name');
        $this->db->from('suspect');
        $this->db->where('s_ic',$id); 
        $this->db->where('s_cnum',$id2); 
        return $this->db->get();
    }
    function get1SuspAll($id){
        $this->db->where('s_cnum',$id); 
        return $this->db->get('suspect');
    }
    function get1SuspFadai($id){
        $this->db->where('s_fadai_sic',$id); 
        return $this->db->get('suspect_fadai');
    }
    function get1Car($table,$id){
        $this->db->where($table,$id); 
        return $this->db->get('vehicle');
    }
    function getSocial($table,$id){
        $this->db->where($table,$id); 
        return $this->db->get('social_media');
    }
    function changedep($id, $data){//人員異動
        $this->db->where('em_mailId', $id);
        $this->db->update('employees', $data);
    }    
    function updateAdminStatus($id, $data){
        $this->db->where('em_ic', $id);
        $this->db->update('employees', $data);
    }    
    function updateemail($id, $data){//警員更新
        $this->db->where('em_mailId', $id);
        $this->db->update('employee_test', $data);
    }    
    function addemail($data){//新增員警(如果不存在)
        $this->db->insert('employee_test', $data);
    }    
    function updateCase($id, $data){
        $this->db->where('c_num', $id);
        //unset($data['id']);
        $this->db->update('cases', $data);
    }    
    function updateReward_fin($id, $data){
        $this->db->where('c_num', $id);
        //unset($data['id']);
        $this->db->update('reward_project3', $data);
    }    
    function updateCase2($id, $data){
        $this->db->where('c_num', $id);
        //unset($data['id']);
        $this->db->update('cases', $data);
    }    
    function updateDrug1($id, $data){
        $this->db->where('e_id', $id);
        $this->db->update('drug_evidence', $data);
    } 
	function updateDrug1byS_ic($id, $data){
        $this->db->where('e_s_ic', $id);
		$this->db->where('e_c_num', null);
        $this->db->update('drug_evidence', $data);
    }    
    function updateSusp($id, $data){
        $this->db->where('s_num', $id);
        $this->db->update('suspect', $data);
    }  
    function updateSurc($id, $data){
        $this->db->where('surc_num', $id);
        $this->db->update('surcharges', $data);
    }    
    function updateSurcPro($id, $data){
        $this->db->where('surc_projectnum', $id);
        $this->db->update('surcharges', $data);
    }    
    function updatefdp($id, $data){
        $this->db->where('fpd_no', $id);
        $this->db->update('fine_doc_publicproject', $data);
    }    
    function updatefdp1($id, $data){
        $this->db->where('fdp_num', $id);
        $this->db->update('fine_doc_publicproject', $data);
    }    
    function updatepetition($id, $data){
        $this->db->where('petition_fdnum ', $id);
        $this->db->update('fine_petition', $data);
    }    
    function updatepetitionSurc($id, $data){
        $this->db->where('petition_surcnum ', $id);
        $this->db->update('surcharge_petition', $data);
    }    
    function addpetition($data){
        $this->db->insert('fine_petition', $data);
    }    
    function addpetitionSurc($data){
        $this->db->insert('surcharge_petition', $data);
    }    
    function updateFine_bank($id, $data){
        $this->db->where('f_num', $id);
        $this->db->update('fine_rec', $data);
    }    
    function updateFine_bank1($id, $data){
        $this->db->where('f_snum', $id);
        $this->db->update('fine_rec', $data);
    }    
    function addFine_bank($data){
        $this->db->insert('fine_rec', $data);
    }    
    function addFinePart_bank($data){
        $this->db->insert('fine_partpay', $data);
    }    
    function addsurc_receive($data){//新增怠金收文
        $this->db->insert('surcharges', $data);
    }    
    function updateRP1($id, $data){//修改一階獎金專案
        $this->db->where('rp_name', $id);
        $this->db->update('reward_project1', $data);
    }    
    function updateRP1A($id, $data){//修改一階獎金專案用查詢rp_project_num
        $this->db->where('rp_project_num', $id);
        $this->db->update('reward_project1', $data);
    }    
    function updateRP2($id, $data){
        $this->db->where('rp_name', $id);
        $this->db->update('reward_project2', $data);
    }    
    function updatecalldoc($id,$data)//更新催繳
    {
        $this->db->where('call_snum', $id);
        $this->db->update('call_doc', $data);
    }
    function updatest($id,$data)//更新罰緩移送
    {
        $this->db->where('ft_surcnum', $id);
        $this->db->update('surc_traf', $data);
    }
    function updateft($id,$data)//更新罰緩移送
    {
        $this->db->where('ft_snum', $id);
        $this->db->update('fine_traf', $data);
    }
    function updateRP3($id, $data){
        $this->db->where('rp3_num', $id);
        $this->db->update('reward_project3', $data);
    }    
    function updateSuspReward_snum($id, $data){	
        $this->db->where('s_num', $id);	
        $this->db->update('suspect', $data);	
    } 
    function updateSuspReward($id, $data){
        $this->db->where('s_reward_project', $id);
        $this->db->update('suspect', $data);
    }    
    function updateSuspReward2($id, $data){
        $this->db->where('s_reward_project2', $id);
        $this->db->update('suspect', $data);
    }    
    function updateFine($id, $data){
        $this->db->where('f_snum', $id);
        $this->db->update('fine_rec', $data);
    }    
    function updateSuspReward3($id, $data){
        $this->db->where('s_reward_project3', $id);
        $this->db->update('suspect', $data);
    }    
    function updateSusp1($id, $data){
        $this->db->where('s_cnum', $id);
        //unset($data['id']);
        $this->db->update('suspect', $data);
    }    
    function updateSuspFadai($id, $data){
        $this->db->where('s_fadai_sic', $id);
        //unset($data['id']);
        $this->db->update('suspect_fadai', $data);
    }    
    function updateCar($id, $data){
        $this->db->where('v_num', $id);
        //unset($data['id']);
        $this->db->update('vehicle', $data);
    }    
    function updateSocial($id, $data){
        $this->db->where('social_media_num', $id);
        //unset($data['id']);
        $this->db->update('social_media', $data);
    }    
    function updatedpdate($id, $data){
        // $this->db->set('dp_send_date', $data);
        $this->db->where('dp_num', $id);
        $this->db->update('disciplinary_project', $data);
    }    
    function updatedpdateAll($id, $data){
        // $this->db->set('fd_date', $data);
        $this->db->where('fd_snum', $id);
        $this->db->update('fine_doc', $data);
    }    
    function uploadfp_officenum($id, $data){
        $this->db->set('fp_office_num', $data);
        $this->db->where('fp_no', $id);
        $this->db->update('fine_project');

        $this->db->set('fp_office_num', $data);
        $this->db->where('fcp_no', $id);
        $this->db->update('fine_change_project');
    }    
    function updateCPDate($id, $data){
        $this->db->set('call_date', $data);
        $this->db->where('call_no', $id);
        $this->db->update('call_doc');
    }    
    function updatepublicdate($id, $data){
        $this->db->set('call_delivery_status', '公示送達');
        $this->db->set('call_delivery_date', $data);
        $this->db->where('call_no', $id);
        $this->db->update('call_doc');
    }    
    function updateFTdate($id, $data){
        $this->db->set('ft_send_date', $data);
        $this->db->where('ft_projectnum', $id);
        $this->db->update('fine_traf');
    }    
    
    function updateSusp_sac_state($id, $data){
        $this->db->set('s_sac_state', $data);
        $this->db->where('s_num', $id);
        $this->db->update('suspect');
    }    
    function updatepw($id, $data){
        $this->db->set('em_pw', $data);
        $this->db->set('em_change_code', null);
        $this->db->where('em_ic', $id);
        $this->db->update('employees');
    }    
    
    function Updatepermit($id, $permit,$priority){
        $this->db->set('em_3permit', $permit);
        $this->db->set('em_priority', $priority);
        $this->db->where('em_mailId', $id);
        $this->db->update('employees');
    }    
    
    function changead($id,$data){
        $this->db->set('em_Isadmin', $data);
        $this->db->where('em_mailId', $id);
        $this->db->update('employees');
    }    
    
    function changepriority($id,$data){
        $this->db->set('em_priority', $data);
        $this->db->where('em_mailId', $id);
        $this->db->update('employees');
    }  
    
    function changeoffice($id){
        $this->db->set('em_office', $this -> session -> userdata('uoffice'));
        $this->db->set('em_roffice', $this -> session -> userdata('uroffice'));
        $this->db->set('em_priority', 2);
        $this->db->where('em_mailId', $id);
        $this->db->update('employees');
    }    
    
    function updatepw_mail($id, $data){
        $this->db->set('em_pw', $data);
        $this->db->set('em_change_code', null);
        $this->db->where('em_mailId', $id);
        $this->db->update('employees');
    }    
    
    function updatepw_first($mail, $pw,$id){
        $this->db->set('em_pw', $pw);
        // $this->db->set('em_ic', $id);
        $this->db->set('em_change_code', null);
        $this->db->where('em_mailId', $mail);
        $this->db->update('employees');
    }    

    function updatechangecode_mail($id){
        $length=5;
        $info="";
        $pattern = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for($i=0;$i<$length;$i++) {
            $info .= $pattern[mt_rand(0,35)];    //生成php随机数
        } 
        strtoupper($info);
        $this->db->set('em_change_code', $info);
        $this->db->where('em_mailId', $id);
        $this->db->update('employees');
    }    
    function updatechangecode_ic($id){
        $length=5;
        $info="";
        $pattern = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for($i=0;$i<$length;$i++) {
            $info .= $pattern[mt_rand(0,35)];    //生成php随机数
        } 
        strtoupper($info);
        $this->db->set('em_change_code', $info);
        $this->db->where('em_ic', $id);
        $this->db->update('employees');
    }    

    function updateSTdate($id, $data){
        $this->db->set('ft_send_date', $data);
        $this->db->where('ft_projectnum', $id);
        $this->db->update('surc_traf');
    }    
    function getPhone($table,$id){
        $this->db->where($table,$id); 
        $this->db->order_by('p_num','DESC');
        //$this->db->limit(1); 
        return $this->db->get('phone');
    }
    function getPhonedel($id1,$id2){
        $this->db->where('p_s_ic',$id1); 
        $this->db->where('p_s_cnum',$id2); 
        return $this->db->get('phone');
    }
    function getFD($table,$id){//fine_doc
        $this->db->where($table,$id); 
        return $this->db->get('fine_doc');
    }
    function getFDL50(){//fine_doc
        $this->db->limit(100); 
        $this->db->order_by('fd_edtime','DESC');
        return $this->db->get('fine_doc');
    }
    function getSurcL50(){//fine_doc
        $this->db->limit(100); 
        $this->db->order_by('surc_edtime','DESC');
        return $this->db->get('surcharges');
    }
    function getTrafL50(){//fine_doc
        $this->db->limit(100); 
        $this->db->order_by('ft_edtime','DESC');
        return $this->db->get('fine_traf');
    }
    function getAccL50(){//fine_doc
        $this->db->limit(100); 
        $this->db->order_by('f_edtime','DESC');
        $this->db->join('suspect', 'f_snum = s_num' ,'LEFT'); 
        return $this->db->get('fine_rec');
    }
    function getRewardL50(){//fine_doc
        $this->db->limit(100); 
        $this->db->order_by('s_edtime','DESC');
        $this->db->join('drug_evidence', 's_ic = e_s_ic' ,'LEFT'); 
        return $this->db->get('suspect');
    }
    function getFR($table,$id){
        $this->db->where($table,$id); 
        return $this->db->get('fine_rec');
    }
    function getPhoneRec($table,$id){
        $this->db->where($table,$id); 
        return $this->db->get('phone_rec');
    }
    function updateFD($id, $data){
        $this->db->where('fd_snum', $id);
        $this->db->update('fine_doc', $data);
    }    
    function updateFR($id, $data){
        $this->db->where('f_snum', $id);
        $this->db->update('fine_rec', $data);
    }    
    function updateFRSurc($id, $data){
        $this->db->where('f_surcnum', $id);
        $this->db->update('fine_rec', $data);
    }    
    function updatePhoneRec($id, $data){
        $this->db->update_batch('phone_rec', $data,'pr_num');
    }    
	function updatePhoneRec2($id, $data){ 
        $this->db->where('pr_p_num', $id);
        $this->db->update('phone_rec', $data);
    }
    function updatePhone($id, $data){ 
        // $this->db->where('p_num', $id);
        $this->db->where('p_s_num', $id);
        $this->db->update('phone', $data);
    }    
    function updatedelivery($id, $data){
        $this->db->where('fdd_snum', $id);
        $this->db->update('fine_doc_delivery', $data);
    }    
    
    function updateCalldelivery($id, $data){
        $this->db->where('call_snum', $id);
        $this->db->update('call_doc', $data);
    }    
    function uploadgongwen($table,$id){//上傳公文資料
        $this->db->set('dp_odoc', $id);
        $this->db->where('dp_num', $table);
        $this->db->update('disciplinary_project');
    }   
    function updatedproject($table,$id){//更改處分書狀態
        $this->db->set('dp_status', $id);
        // $this->db->where('dp_name', $table);
		$this->db->where('dp_num', $table);
        $this->db->update('disciplinary_project');
    }   
    function complete($id){//確帳務簽核
        $this->db->set('fp_status', '簽核完成');
        $this->db->where('fp_no', $id);
        $this->db->update('fine_project');
    }   
    function deletePhonerec($id){
        $this->db->where('pr_num', $id);
        $this->db->delete('phone_rec');
    }   
    function getSource($table,$id){
        $this->db->where($table,$id); 
        $this->db->order_by('sou_num','DESC');
        $this->db->limit(1); 
        return $this->db->get('drug_source');
    }
    function getSource1($table,$id){//毒品溯源
        $this->db->where($table,$id); 
        return $this->db->get('drug_source');
    }
    function getSourceSM($table,$id){
        $this->db->where($table,$id); 
        return $this->db->get('source_sm');
    }
    function getcalldoc($table,$id){
        $this->db->where($table,$id); 
        return $this->db->get('call_doc');
    }
    function addrp3($data)
    {
        $this->db->insert('reward_project3', $data);
    }
    function addrp1($data)
    {
        $this->db->insert('reward_project1', $data);
    }
    function addrp2($data)
    {
        $this->db->insert('reward_project2', $data);
    }
    function addfp1($data)
    {
        $this->db->insert('fine_project', $data);
    }
    function addfcp1($data)
    {
        $this->db->insert('fine_change_project', $data);
    }
    function adddp($data)
    {
        $this->db->insert('disciplinary_project', $data);
    }
    function addSurc($data)
    {
        $this->db->insert('surcharge_project', $data);
    }
    function addcalldoc($data)//新增催繳
    {
        $this->db->insert('call_doc', $data);
    }
    function addft($data)//新增罰緩移送
    {
        $this->db->insert('fine_traf', $data);
    }
    function addst($data)//新增罰緩移送
    {
        $this->db->insert('surc_traf', $data);
    }
    function addcallProject($data)//新增催繳專案
    {
        $this->db->insert('call_project', $data);
    }
    function addftp($data)//新增罰緩移送專案
    {
        $this->db->insert('fine_trafproject', $data);
    }
    function addstp($data)//新增罰緩移送專案
    {
        $this->db->insert('surc_trafproject', $data);
    }
    function addfdp($data)
    {
        $this->db->insert('fine_doc_publicproject', $data);
    }
    function adddrugrec($data)
    {
        $this->db->insert('drug_rec', $data);
    }
    function addsource_sm($data)
    {
        $this->db->insert_batch('source_sm', $data);
    }
    function adddrugSou($data)
    {
        $this->db->insert('drug_source', $data);
    }
    function deleteSource($id){
        $this->db->where('sou_sm_num', $id);
        $this->db->delete('source_sm');
    }   
    function updateSource($id, $data){
        $this->db->where('sou_num', $id);
        $this->db->update('drug_source', $data);
    }  
    function updatesc_up($id, $data){
        $this->db->where('sc_snum', $id);
        $this->db->update('susp_check', $data);
    }  
//delete
    function deleteCaseS($id){
        $this->deleteCase('c_num',$id);
        $this->deleteDrugc('e_c_num',$id);
        $this->deleteDrugckc('df_cnum',$id);
        $this->deleteSuspc('s_cnum',$id);
        $this->deleteSourceS('sou_cnum',$id);
        $this->deleteSourceSM('sou_sm_s_cnum',$id);
        $this->deletePhone('p_s_cnum',$id);
        $this->deletePhoneS('pr_s_cnum',$id);
        $this->deleteCar('v_cnum',$id);
        $this->deleteSocial('s_cnum',$id);
    }   
    function deleteDrug($id){
        $this->deleteDrugc($id);
        $this->deleteDrugckc($id);
    }   
    function deleteSusp($id){
        $this->deleteSuspc('s_num',$id);
        $this->deleteSourceS('sou_s_num',$id);
        $this->deletePhone('p_s_num',$id);
        $this->deleteCar('v_s_num',$id);
        $this->deleteSocial('s_num',$id);
    }   
    function deletePho($id,$id2){
        $this->deletePhone('p_num',$id);
        $this->deletePhoneS('pr_p_no',$id2);
    }   
    function deleteSou($id){
        $this->deleteSourceS('sou_num',$id);
        $this->deleteSourceSM('sou_sounum',$id);
    }   
    function deleteSoc($id){
        $this->deleteSocial('social_media_num',$id);
    }   
    function deleteVec($id){
        $this->deleteCar('v_num',$id);
    }   
    function deleteCase($table,$id){
        $this->db->set('delete_status', 1);
        $this->db->where('c_num', $id);
        $this->db->update('cases');
    }   
    function deleteDrugID($id){
        $this->db->set('e_del_sta', 1);
        $this->db->where('e_id', $id);
        $this->db->update('drug_evidence');
    }   
    function deleteDrugc($table,$id){
        $this->db->set('e_del_sta', 1);
        $this->db->where('e_c_num', $id);
        $this->db->update('drug_evidence');
    }   
    function deleteDrugckc($table,$id){
        $this->db->where($table, $id);
        $this->db->delete('drug_first_check');
    }   
    function deleteSuspc($table,$id){
        $this->db->where($table, $id);
        $this->db->delete('suspect');
    }   
    function deleteSourceS($table,$id){
        $this->db->where($table, $id);
        $this->db->delete('drug_source');
    }   
    function deleteSourceSM($table,$id){
        $this->db->where($table, $id);
        $this->db->delete('source_sm');
    }   
    function deletePhone($table,$id){
        $this->db->where($table, $id);
        $this->db->delete('phone');
    }   
    function deletePhoneS($table,$id){
        $this->db->where($table, $id);
        $this->db->delete('phone_rec');
    }   
    function deleteCar($table,$id){
        $this->db->where($table, $id);
        $this->db->delete('vehicle');
    }   
    function deleteSocial($table,$id){
        $this->db->where($table, $id);
        $this->db->delete('social_media');
    }   

	/** 帳務資料匯入-檢查是否存在 */
	function checkFineImport($data){
		$this->db->where('f_caseid', $data['f_caseid']);
        $this->db->where('f_userid', $data['f_userid']);

        $this->db->from('fine_import');
		if($this->db->count_all_results() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/** 帳務資料匯入-檢查更新項是否不相同 */
	function checkUpdateFineImport($data){
		$this->db->where('f_caseid', $data['f_caseid']);
        $this->db->where('f_userid', $data['f_userid']);
		$this->db->where('f_execlog', $data['f_execlog']);
		$this->db->where('f_movelog', $data['f_movelog']);
		$this->db->where('f_cardlog', $data['f_cardlog']);
		$this->db->where('f_dellog', $data['f_dellog']);

        $this->db->from('fine_import');
		if($this->db->count_all_results() > 0)
		{
			// 相同
			return true;
		}
		else
		{
			// 不相同，要進行update
			return false;
		}
	}
	/** 帳務資料匯入-檢查是否存在，存在即更新 */
	function updateFineImport($data){
		$updatedata = array(
			"f_virtualcode" => $data['f_virtualcode'],
			// "f_paylog" => $data['f_paylog'],
			"f_execlog" => $data['f_execlog'],
			"f_movelog" => $data['f_movelog'],
			"f_cardlog" => $data['f_cardlog'],
			"f_dellog" => $data['f_dellog'],
		);
		$this->db->where('f_caseid', $data['f_caseid']);
		$this->db->where('f_userid', $data['f_userid']);
		$this->db->update('fine_import', $updatedata);
	}
    /** 帳務資料匯入-新增 */
    function addFineImport($data){
        $this->db->insert('fine_import', $data);
		return $this->db->insert_id();
        
    }

    /** 帳務專案-新增空白帳 */
    function addEmptyFineImport($data){
        $this->db->insert('fine_import', $data);
        return $this->db->insert_id();
    }

    /**
     * 刪除每日帳
     */
    function delFineProject($id)
    {
        $this->db->select('fp_no');
        $this->db->where('fp_num', $id);
        $no = $this->db->get('fine_project')->result()[0]->fp_no;

        if($this->db->count_all_results() > 0)
        {
            $this->db->where('fp_no', $no);
            $this->db->delete('fine_project');

            $this->db->where('f_project_id', $no);
            $this->db->delete('fine_partpay');
        }
    }

    /**
     * 刪除轉正專案
     */
    function delFineCProject($id)
    {

        $this->db->select('fcp_no');
        $this->db->where('fcp_no', $id);
        $this->db->get('fine_change_project')->result();

        if($this->db->count_all_results() > 0)
        {
            $this->db->where('fcp_no', $id);
            $this->db->delete('fine_change_project');

            $this->db->where('f_project_id', $id);
            $this->db->delete('fine_partpay');
        }
        
    }
	/**
	 * 檢查帳務專案是否已有新增金額
	 */
	function checkFineProject_edit($pjid)
	{
		$this->db->where('f_project_id', $pjid);
		$this->db->where('fpart_amount is ', null);
		$this->db->from('fine_partpay');
		return $this->db->count_all_results();
	}

    /** 帳務資料匯入-過濾搜尋 */

    /** 
     * 撈出全部異常帳務資料 (移送日期晚於完納日期)
     * */
    function filterFineabnormal1($ishowAll){
        if($ishowAll === 'true')
            $str = '';
        else
            $str = ' and  f_abmormal <> 1';

        return  $this->db->query("SELECT *, 'A' as abnormal FROM fine_import where (f_movelog <> '' and f_movelog is not NULL) and f_donedate <> '0000-00-00' and DATE(CONCAT( CASE WHEN length(SUBSTRING_INDEX(SUBSTRING_INDEX(f_movelog, '\n', -1),'_', 1)) = 7 THEN left(SUBSTRING_INDEX(SUBSTRING_INDEX(f_movelog, '\n', -1),'_', 1),3)+1911 ELSE left(SUBSTRING_INDEX(SUBSTRING_INDEX(f_movelog, '\n', -1),'_', 1),2)+1911 END , right(SUBSTRING_INDEX(SUBSTRING_INDEX(f_movelog, '\n', -1),'_', 1),4))) > f_donedate". $str ." order by f_abmormal,f_no ;");
        // exit;
    }
    /** 
     * 撈出全部異常帳務資料 (執行日期晚於完納日期)
     * */
    function filterFineabnormal2($ishowAll){
        if($ishowAll === 'true')
            $str = '';
        else
            $str = ' and  f_abmormal <> 1';

        return $this->db->query("SELECT *, 'B' as abnormal FROM fine_import 
        where (f_execlog <> '' and f_execlog is not NULL) and f_donedate <> '0000-00-00' and DATE(CONCAT( CASE WHEN length(SUBSTRING_INDEX(SUBSTRING_INDEX(f_execlog, '\n', -1),'_', 1)) = 7 THEN left(SUBSTRING_INDEX(SUBSTRING_INDEX(f_execlog, '\n', -1),'_', 1),3)+1911 ELSE left(SUBSTRING_INDEX(SUBSTRING_INDEX(f_execlog, '\n', -1),'_', 1),2)+1911 END , right(SUBSTRING_INDEX(SUBSTRING_INDEX(f_execlog, '\n', -1),'_', 1),4))) > f_donedate".$str." order by f_abmormal, f_no ;");
    }
    /** 
     * 撈出全部異常帳務資料 (備註有值)
     * */
    function filterFineabnormal3($ishowAll){
        return $this->db->query("SELECT *, 'C' as abnormal FROM fine_import 
        where f_comment <> '' and f_comment is not NULL".(($ishowAll)?'':' and  f_abmormal <> 1').";");
    }

    /** 
     * 撈出全部帳務資料
     * @param 類型：罰緩、怠金
     * @param 年度 
     * */
    function filterFineAll($type, $year){
        $this->db->where('type =', $type);
        $this->db->where('f_year =', $year); 
        return $this->db->get('fine_import');
    }

    /** 
     * 撈出全部帳務資料
     * @param 類型：姓名、身分證
     * @param 關鍵字 
     * */
    function filterFineinKeyworkAll($type, $word){
        if($type === 'id')
        {
            $this->db->like('f_userid', $word);
        }
        elseif($type === 'name')
        {
            $this->db->like('f_username', $word);
        }
        elseif ($type === 'movenum') 
        {
            $this->db->like('f_movelog', $word);
        }
        elseif ($type === 'caseid') 
        {
            $this->db->like('f_caseid', $word);
        }
        else
        {
            $this->db->like('f_virtualcode', $word);
        }

        return $this->db->get('fine_import');
    }

    /** 
     * 撈出全部帳務資料，未包含『已加入每日帳專案之物件』
     * @param 類型：罰緩、怠金
     * @param 年度 
     * */
    function filterFine($type, $year){
        $this->db->where('type =', $type);
        $this->db->where('f_year =', $year); 
        $this->db->where('f_project_id', null);

        return $this->db->get('fine_import');
    }

    /** 
     * 撈出全部帳務資料，未包含『已加入每日帳專案之物件』
     * @param 類型：姓名、身分證
     * @param 關鍵字 
     * */
    function filterFineinKeywork($type, $word){
        if($type === 'id')
        {
            $this->db->like('f_userid', $word);
        }
        elseif($type === 'name')
        {
            $this->db->like('f_username', $word);
        }
        else
        {
            $this->db->like('f_virtualcode', $word);
        }
        $this->db->where('f_project_id', null);
        
        return $this->db->get('fine_import');
    }

    /**
     * 新增需轉正之帳務資料至每日專案
     * 
     * @param array 轉正之帳務資料
     */
    function add_finepartpay_projectid($data)
    {
        $this->db->insert('fine_partpay', $data);
    }

    /**
     * 判斷是否已新增過當日帳
     * @param 帳務專案f_no
     * @return 筆數
     */
    function check_fine_project_exist($id)
    {
        $this->db->where('fp_no', $id);
        $this->db->from('fine_project');

        return $this->db->count_all_results();
    }

    /**
     * 撈取單一帳務資料
     * @param f_no
     */
    function getFineimport_data($f_no)
    {
        // $this->db->select('f_paylog');
        // $this->db->where('f_no', $f_no);
        // return $this->db->get('fine_import');

        return $this->db->query("select f_paylog, (CASE WHEN type = 'A' THEN f_amount * 10000 ELSE f_amount END) as f_amount, f_donedate,f_doneamount, f_paydate,f_paymoney,f_movelog,f_depmovelog,type,f_caseid,f_username,f_userid,f_execlog  from fine_import where f_no=".$f_no."");
    }

	/**
     * 撈取單一帳務資料
     * @param f_no
     */
    function getFineimport_databycaseid($caseid)
    {
        // $this->db->select('f_paylog');
        // $this->db->where('f_no', $f_no);
        // return $this->db->get('fine_import');

        return $this->db->query("select f_paylog, (CASE WHEN type = 'A' THEN f_amount * 10000 ELSE f_amount END) as f_amount, f_donedate,f_doneamount, f_paydate,f_paymoney,f_movelog,f_depmovelog,type,f_caseid,f_username,f_userid,f_execlog  from fine_import where f_caseid='".$caseid."'");
    }

    /**
     * 撈取專案最大sort值
     * 
     */
    function getFineProject_maxsort($pjid)
    {
        $this->db->select_max('sort_no');
        $this->db->where('f_project_id', $pjid);
        return $this->db->get('fine_partpay');
        
        
    }

    /**
     * 撈取個人帳務資料
     * @param f_username
     */
    function getFineimport_data_in_person($f_username, $pjid)
    {
        return $this->db->query("SELECT * FROM fine_import WHERE f_username LIKE '%".$f_username."%' and f_no not in (select fpart_fpnum from fine_partpay where f_project_id = '".$pjid."')");
        // $this->db->where('f_caseid <>', $f_caseid);
        // $this->db->like('f_username', $f_username);
        // return $this->db->get('fine_import');
    }

    /** 帳務專案底下之帳務資料 */

    /**
     * 撈取全部
     * @param 帳務專案f_no
     */
    public function get_finepoject_body_data($id)
    {
        $this->db->join('fine_import', 'fine_import.f_no = fine_partpay.fpart_fpnum');
        $this->db->where('f_project_id', $id);
        $this->db->order_by('fine_partpay.sort_no', 'ASC');
        return $this->db->get('fine_partpay');
    }

    /**
     * 撈取全部 - full join
     * @param 帳務專案f_no
     */
    public function get_finepoject_body_data_full($id)
    {
        return $this->db->query("select *,a.f_comment as body_comment from fine_partpay a left join fine_import b on b.f_no = a.fpart_fpnum where f_project_id = '".$id."' order by a.sort_no ASC");
    }

     /**
     * 撈取單一帳務資料
     * @param 帳務專案f_no
     */
    public function get_finepoject_body_single_data($id)
    {
        $this->db->where('f_no', $id);
        return $this->db->get('fine_import');
    }

    /**
     * 撈取特定專案底下個人帳務資料
     * @param 帳務ID f_no
     * @param 專案ID pjid
     */
    function getFinepart_data_in_person($ser, $pjid)
    {
		$this-> db->select('fine_import.*, fine_partpay.*, fine_partpay.f_comment as fp_comment');
        $this->db->join('fine_import', 'fine_import.f_no = fine_partpay.fpart_fpnum');
        $this->db->where('f_project_id', $pjid);
        // $this->db->where('fpart_fpnum', $f_no);
		$this->db->where('fpart_num', $ser);
        return $this->db->get('fine_partpay');
    }

    /**
     * 檢查分期紀錄是否存在，沒有即新增
     * @param 分期資料
     */
    public function import_check_finepayport_data($data)
    {
        $this->db->where('fpart_fpnum', $data['fpart_fpnum']);
        // $this->db->where('fpart_date <=',$data['fp_createdate']);
        // $this->db->where('fpart_date <>', '0000-00-00');
        // $this->db->order_by('fpart_num', 'ASC');
        $this->db->where('fpart_amount', $data['fpart_amount']);
        $this->db->where('fpart_date', $data['fpart_date']);

        $this->db->from('fine_partpay');

        if($this->db->count_all_results() == 0)
        {
            $this->db->insert('fine_partpay', $data);
        }

    }

    /**
     * 檢查分期紀錄是否存在
     * @param 分期資料
     */
    public function check_finepayport_data($data)
    {
        $this->db->where('fpart_fpnum', $data['fpart_fpnum']);
        // $this->db->where('fpart_date <=',$data['fp_createdate']);
        $this->db->where('fpart_date <>', '0000-00-00');
        $this->db->order_by('fpart_num', 'ASC');
        // $this->db->where('fpart_amount', $data['fpart_amount']);
        // $this->db->where('fpart_date', $data['fpart_date']);

        // $this->db->get('fine_partpay');

        return $this->db->get('fine_partpay');
    }

    /**
     * 取得分期總額 - 分期金額
     * @param 分期資料
     */
    public function check_finepayport_data_payamount($data)
    {
        $this->db->select_sum('fpart_amount');
        $this->db->where('fpart_fpnum', $data['fpart_fpnum']);
        // $this->db->where('fpart_date <=',$data['fp_createdate']);
        $this->db->where('fpart_date <>', '0000-00-00');
        // $this->db->where('f_project_id', $data['f_project_id']);
        // $this->db->where('fpart_amount', $data['fpart_amount']);
        // $this->db->where('fpart_date', $data['fpart_date']);

        // $this->db->get('fine_partpay');

        return $this->db->get('fine_partpay');

        // return $this->db->query("select sum(fpart_amount) as fpart_amount from fine_partpay where fpart_fpnum='".$data['fpart_fpnum']."' and fpart_date <> '0000-00-00' and fpart_date <= '".$data['fp_createdate']."'");
    }
    /**
     * 取得分期總額 - 轉正金額
     * @param 分期資料
     */
    public function check_finepayport_data_turnsub($data)
    {
        $this->db->select_sum('f_turnsub');
        $this->db->where('fpart_fpnum', $data['fpart_fpnum']);
        // $this->db->where('fpart_date <=',$data['fp_createdate']);
        $this->db->where('fpart_date <>', '0000-00-00');
        // $this->db->where('f_project_id', $data['f_project_id']);
        // $this->db->where('fpart_amount', $data['fpart_amount']);
        // $this->db->where('fpart_date', $data['fpart_date']);

        // $this->db->get('fine_partpay');

        return $this->db->get('fine_partpay');

        // return $this->db->query("select sum(fpart_amount) as fpart_amount from fine_partpay where fpart_fpnum='".$data['fpart_fpnum']."' and fpart_date <> '0000-00-00' and fpart_date <= '".$data['fp_createdate']."'");
    }

    /**
     * 獲取分期紀錄
     * @param 分期資料
     */
    public function get_finepayport_data($data)
    {
       
    }

    /**
     * 新增分期付款，並更新帳務資料
     * @param 分期資料
     * @param 分期log
     * @param 帳務資料ID
     * @param 金融帳專案ID
     */
    public function add_finepayport_data($data1, $data2, $id, $pjid)
    {   
        if(isset($data1))
        {
            $this->db->where('f_project_id', $pjid);
            $this->db->where('fpart_fpnum', $id);
            $this->db->update('fine_partpay', $data1);
        }
        $this->db->where('f_no', $id);
        $this->db->update('fine_import', $data2);
        
    }

    /**
     * 並更新分期付款的退費
     * @param 分期資料
     * @param 帳務資料ID
     * @param 金融帳專案ID
     */
    public function update_finepayport_data($data1, $id, $ser, $pjid)
    {   
        $this->db->where('f_project_id', $pjid);
        // $this->db->where('fpart_fpnum', $id);
		$this->db->where('fpart_num', $ser);
        $this->db->update('fine_partpay', $data1);
    }

    /**
     * 更新帳務資料 - 分期Log
     * @param 分期log
     * @param 帳務資料ID
     */
    public function update_fineimport_data($data2, $id)
    {
        $this->db->where('f_no', $id);
        $this->db->update('fine_import', $data2);
        
    }

    /**
     * 新增帳務專案之帳務資料(初始)
     * @param 初始帳務專案表身（f_project_id,fpart_fpnum）
     */
    public function add_finepoject_body_data($data)
    {
        $this->db->insert('fine_partpay', $data);
    }

    /**
     * 更新異常帳務之處理狀態 -> 已完成
     * @param 主鍵 f_no
     * @param 更新資料
     */
    public function update_fine_abnormal($id, $data)
    {
        $this->db->where('f_no', $id);
        $this->db->update('fine_import', $data);
    }

    /**
     * 刪除帳務專案下之帳務資料
     * 
     * @param 帳務ID
     * @param 專案ID
     */
    public function delete_finepayport_data($id, $pjid)
    {
        $this->db->where('fpart_num', $id);
        $this->db->where('f_project_id', $pjid);
        $this->db->delete('fine_partpay');
    }

    /**
     * 更新帳務專案下之帳務資料的順序
     * 
     * @param 帳務ID
     * @param 專案ID
     */
    public function update_finepayport_data_sort($sort_type, $id, $pjid)
    {
        $ch_ser = "";
        $ch_itemsort = 0;
        $ori_ser = "";
        $ori_itemsort = 0;
        switch ($sort_type) {
            case 'up':
                $query = $this->db->query("SELECT a.fpart_num, a.sort_no, b.sort_no as ori_sort, b.fpart_num as ori_ser FROM fine_partpay a left join (select f_project_id, fpart_num, sort_no from fine_partpay where fpart_num = ".$id." and f_project_id = '".$pjid."') b on a.f_project_id=b.f_project_id WHERE a.f_project_id = '".$pjid."' and a.sort_no < b.sort_no ORDER BY a.sort_no desc LIMIT 1")->result();

                $ch_ser = $query[0]->fpart_num;
                $ch_itemsort = $query[0]->sort_no;
                $ori_ser = $query[0]->ori_ser;
                $ori_itemsort = $query[0]->ori_sort;
                break;
            case 'down':
                $query = $this->db->query("SELECT a.fpart_num, a.sort_no, b.sort_no as ori_sort, b.fpart_num as ori_ser FROM fine_partpay a left join (select f_project_id, fpart_num, sort_no from fine_partpay where fpart_num = ".$id." and f_project_id = '".$pjid."') b on a.f_project_id=b.f_project_id WHERE a.f_project_id = '".$pjid."' and a.sort_no > b.sort_no ORDER BY a.sort_no asc LIMIT 1")->result();
                $ch_ser = $query[0]->fpart_num;
                $ch_itemsort = $query[0]->sort_no;
                $ori_ser = $query[0]->ori_ser;
                $ori_itemsort = $query[0]->ori_sort;
                break;
            default:
                # code...
                break;
        }
        $update_data = array(
            "sort_no" => $ch_itemsort
        );
        $this->db->where('fpart_num', $ori_ser);
        $this->db->update('fine_partpay', $update_data);

        $update_data2 = array(
            "sort_no" => $ori_itemsort
        );
        $this->db->where('fpart_num', $ch_ser);
        $this->db->update('fine_partpay', $update_data2);
    }

    /**
     * 取得常用來源
     * @param source
     */
    public function getUsefulSource($source){
        
        return $this->db->query("select distinct(SUBSTRING_INDEX(SUBSTRING_INDEX(f_source, '_', -2), '_', 1)) as source_uni from fine_partpay where f_source LIKE '%".$source."%' order by `fpart_date`  desc LIMIT 5");
    }

    /** 帳務匯出 */

    /** 繳納清冊 */
    public function export_finePJ_lists($id)
    {
        return $this->db->query("SELECT cASE WHEN `fine_partpay`.`f_refund` > 0 THEN ''  ELSE (CASE WHEN length(SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_',-3)) > 20 THEN SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_',-2),'_',1) ELSE SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_',-1) END) END as f_movelog,`fine_import`.f_movelog as calc_movelog,`fine_project`.`fp_no`, `fine_project`.`fp_type`, CONCAT(`fine_import`.`f_year`, CASE WHEN `fine_import`.f_dellog is null THEN '' ELSE '(註)' END) as f_year, `fine_import`.`f_caseid`, `fine_import`.`f_username`, `fine_project`.`fp_createdate`,`fine_project`.`fp_office_num`, ((CASE WHEN `fine_partpay`.fpart_amount >= 0 THEN `fine_partpay`.fpart_amount ELSE 0 END) + `fine_partpay`.f_fee + (CASE WHEN `fine_partpay`.`f_turnsub` < 0 THEN 0-`fine_partpay`.`f_turnsub` ELSE `fine_partpay`.`f_turnsub` END) + `fine_partpay`.`f_refund`) as pay_tamount, CASE WHEN `fine_import`.`f_doneamount` = 0 THEN '分期' ELSE '完納' END as payway, CASE WHEN `fine_partpay`.`f_turnsub` < 0 THEN 0 ELSE `fine_partpay`.`fpart_amount` END as fpart_amount, `fine_partpay`.`f_fee`, (CASE WHEN `fine_partpay`.`f_turnsub` < 0 THEN 0-`fine_partpay`.`f_turnsub` ELSE `fine_partpay`.`f_turnsub` END) as `f_turnsub`, `fine_partpay`.`f_refund`,fine_partpay.f_turnsub_log FROM `fine_project` LEFT JOIN `fine_partpay` ON `fine_partpay`.`f_project_id` = `fine_project`.`fp_no` LEFT JOIN `fine_import` ON `fine_partpay`.`fpart_fpnum` = `fine_import`.`f_no` WHERE `fine_partpay`.`f_project_id` = '".$id."' order by `fine_partpay`.sort_no, `fine_import`.`f_userid`,`fine_import`.`f_caseid`");
    }

    /** 繳納清冊受處分人 */
    public function get_finePJ_lists_user_in_distinct($id)
    {
        return $this->db->query("SELECT `fine_project`.`fp_no`,`fine_project`.`fp_type`,`fine_import`.`f_no`,`fine_import`.`f_donedate`,`fine_import`.`type`,`fine_import`.`f_caseid`,`fine_import`.`f_username`, CASE WHEN `fine_import`.`f_donedate` = '0000-00-00' and `fine_import`.`f_doneamount` = 0 and `fine_import`.`f_paylog` is not null THEN '分期' WHEN `fine_import`.`f_donedate` <> '0000-00-00' and `fine_import`.`f_doneamount` <> 0 and length(`fine_import`.`f_paylog`) < 15 THEN '完納' ELSE '分期完納' END as listtype from `fine_project` INNER JOIN `fine_partpay` ON  `fine_partpay`.`f_project_id` = `fine_project`.`fp_no` LEFT JOIN `fine_import` ON `fine_partpay`.`fpart_fpnum` = `fine_import`.`f_no` where `fine_project`.`fp_no`='".$id."' group by `fine_import`.`f_caseid`,`fine_import`.`f_username` order by `fine_partpay`.sort_no, listtype");
    }

    /** 繳納清冊處分人付款明細 */
    public function get_finePJ_list_user_pay_detail($id)
    {
        return $this->db->query("SELECT (left(`fine_partpay`.`fpart_date`,4)-1911) as year,SUBSTRING(`fine_partpay`.`fpart_date`, 6, 2) as month, `fine_import`.`f_no`, CONCAT(`fine_import`.`f_year`, CASE WHEN `fine_import`.f_dellog is null THEN '' ELSE '(註)' END) as f_year,`fine_import`.`f_caseid`,`fine_import`.`f_username`,CONCAT((left(`fine_partpay`.`fpart_date`,4)-1911),SUBSTRING(`fine_partpay`.`fpart_date`, 6, 2),SUBSTRING_INDEX(`fine_partpay`.`fpart_date`, '-', -1)) as paydate,`fine_partpay`.`fpart_type`,CASE WHEN `fine_partpay`.`f_turnsub` > 0 and  `fine_partpay`.`f_turnsub` is not null THEN `fine_partpay`.`f_turnsub` ELSE  `fine_partpay`.`fpart_amount` END as fpart_amount,`fine_import`.`f_amount`,`fine_partpay`.`f_fee`,`fine_partpay`.`f_refund`, CASE WHEN `fine_partpay`.`f_status` <> '' Then `fine_partpay`.`f_status` ELSE '分期' END as f_status,`fine_partpay`.`f_cstatus`,`fine_partpay`.`f_comment`,CONCAT((left(`fine_import`.`f_donedate`,4)-1911),SUBSTRING(`fine_import`.`f_donedate`, 6, 2),SUBSTRING_INDEX(`fine_import`.`f_donedate`, '-', -1)) as donedate,(left(`fine_import`.`f_donedate`,4)-1911) as d_year,SUBSTRING(`fine_import`.`f_donedate`, 6, 2) as d_month,`fine_import`.`f_doneamount`,`fine_import`.`type` from `fine_partpay` LEFT JOIN `fine_import` ON `fine_import`.`f_no` =  `fine_partpay`.`fpart_fpnum` where `fine_import`.`f_no`='".$id."'");
    }

      /** 公文函受處分人 */
      public function get_finePJ_lists_officedoc_user_in_distinct($id)
      {
          return $this->db->query("SELECT `fine_project`.`fp_no`,`fine_project`.`fp_type`,`fine_import`.`f_caseid`,`fine_import`.`f_username`,`fine_import`.`f_userid`, CASE WHEN `fine_import`.`f_donedate` = '0000-00-00' and `fine_import`.`f_doneamount` = 0 and `fine_import`.`f_paylog` is not null THEN '分期' WHEN `fine_import`.`f_donedate` <> '0000-00-00' and `fine_import`.`f_doneamount` <> 0 and length(`fine_import`.`f_paylog`) < 15 THEN '完納' ELSE '分期完納' END as listtype from `fine_project` INNER JOIN `fine_partpay` ON `fine_partpay`.`f_project_id` = `fine_project`.`fp_no` LEFT JOIN `fine_import` ON `fine_import`.`f_no` = `fine_partpay`.`fpart_fpnum` where fine_import.f_movelog is not null and `fine_partpay`.fpart_amount is not null and `fine_project`.`fp_no`='".$id."' group by `fine_import`.`f_userid` order by `fine_partpay`.`sort_no`, listtype");
      }

	  /** 公文函受處分人 - 支匯票*/
      public function get_finePJ_lists_ticketofficedoc_user_in_distinct($id)
      {
          return $this->db->query("SELECT `fine_project`.`fp_no`,`fine_project`.`fp_type`,`fine_import`.`f_caseid`,`fine_partpay`.`f_ticket_no`,`fine_import`.`f_username`,`fine_import`.`f_userid`, CASE WHEN `fine_import`.`f_donedate` = '0000-00-00' and `fine_import`.`f_doneamount` = 0 and `fine_import`.`f_paylog` is not null THEN '分期' WHEN `fine_import`.`f_donedate` <> '0000-00-00' and `fine_import`.`f_doneamount` <> 0 and length(`fine_import`.`f_paylog`) < 15 THEN '完納' ELSE '分期完納' END as listtype from `fine_project` INNER JOIN `fine_partpay` ON `fine_partpay`.`f_project_id` = `fine_project`.`fp_no` LEFT JOIN `fine_import` ON `fine_import`.`f_no` = `fine_partpay`.`fpart_fpnum` where fine_import.f_movelog is not null and `fine_partpay`.fpart_amount is not null and `fine_project`.`fp_no`='".$id."' group by `fine_partpay`.`f_ticket_no` order by `fine_partpay`.`sort_no`, listtype");
      }

      /** 公文函受處分人明細*/
      public function get_finePJ_lists_officedoc_user_detail($id, $userid)
      {
          return $this->db->query("SELECT CASE WHEN length(SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_',-3)) > 20 THEN SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_',-2),'_',1) ELSE SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_',-1) END as f_movelog, CASE WHEN length(SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_',-3)) > 20 THEN SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_',-2),'_',-1) ELSE SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_',-1) END as f_movelog_depart,`fine_import`.f_movelog as calc_movelog,`fine_project`.`fp_no`, `fine_project`.`fp_type`, `fine_import`.`f_year`, CASE WHEN `fine_import`.`type` = 'A' THEN '罰鍰' ELSE '怠金' END as fine_type, `fine_import`.`f_caseid`, CONCAT(left(`fine_import`.`f_username`, 1), 'O', right(`fine_import`.`f_username`, 1)) as username,`fine_import`.`f_username` as fullusername,`fine_import`.`f_userid`, `fine_project`.`fp_createdate`,`fine_project`.`fp_office_num`,  `fine_partpay`.`f_status` as payway, (CASE WHEN fine_import.type = 'A' THEN fine_import.f_amount * 10000 ELSE fine_import.f_amount END) as amount, (CASE WHEN `fine_partpay`.`f_turnsub` = 0 THEN `fine_partpay`.`fpart_amount` ELSE (CASE WHEN `fine_partpay`.`f_turnsub` < 0 THEN 0-`fine_partpay`.`f_turnsub` ELSE `fine_partpay`.`f_turnsub` END) END + `fine_partpay`.`f_fee`) as totalnowamount, (CASE WHEN `fine_partpay`.`f_turnsub` = 0 THEN `fine_partpay`.`fpart_amount` ELSE (CASE WHEN `fine_partpay`.`f_turnsub` < 0 THEN 0-`fine_partpay`.`f_turnsub` ELSE `fine_partpay`.`f_turnsub` END) END) as nowamount,CASE WHEN fine_import.f_paymoney = 0 THEN (CASE WHEN fine_import.type = 'A' THEN fine_import.f_amount * 10000 ELSE fine_import.f_amount END) ELSE (CASE WHEN fine_import.f_paymoney < 0 THEN 0 ELSE fine_import.f_paymoney END) END  as grandamount,`fine_partpay`.`f_source`, fine_partpay.fpart_type, ((CASE WHEN fine_import.type = 'A' THEN fine_import.f_amount * 10000 ELSE fine_import.f_amount END) - fine_import.f_paymoney - fine_import.f_doneamount) as balance, `fine_partpay`.`f_fee`, (CASE WHEN `fine_partpay`.`f_turnsub` < 0 THEN 0-`fine_partpay`.`f_turnsub` ELSE `fine_partpay`.`f_turnsub` END) as `f_turnsub`, `fine_partpay`.`f_refund` FROM `fine_project`  LEFT JOIN `fine_partpay` ON `fine_partpay`.`f_project_id` = `fine_project`.`fp_no` LEFT JOIN `fine_import` ON `fine_partpay`.`fpart_fpnum` = `fine_import`.`f_no` where fine_import.f_movelog is not null and `fine_partpay`.fpart_amount is not null and `fine_partpay`.`fpart_date` = fine_project.fp_createdate and  `fine_project`.`fp_no` = '".$id."' and `fine_import`.`f_userid` = '".$userid."' order by `fine_partpay`.`sort_no`");
      }

      

      /** 公文函受處分人明細-支匯票 */
      public function get_finePJ_lists_ticketofficedoc_user_detail($id, $ticketno)
      {
          return $this->db->query("SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(fine_import.f_movelog, '\n', -1),'_', -2) as f_movelog,`fine_import`.f_movelog as calc_movelog,`fine_project`.`fp_no`, `fine_project`.`fp_type`, `fine_import`.`f_year`, CASE WHEN `fine_import`.`type` = 'A' THEN '罰鍰' ELSE '怠金' END as fine_type, `fine_import`.`f_caseid`, CONCAT(left(`fine_import`.`f_username`, 1), 'O', right(`fine_import`.`f_username`, 1)) as username,`fine_import`.`f_username` as fullusername,`fine_import`.`f_userid`, `fine_project`.`fp_createdate`,`fine_partpay`.`f_office_num`,`fine_partpay`.`f_ticket_no`,`fine_partpay`.`f_receipt_no`,  CASE WHEN `fine_import`.`f_doneamount` = 0 THEN '分期' ELSE '完納' END as payway,(CASE WHEN fine_import.type = 'A' THEN fine_import.f_amount * 10000 ELSE fine_import.f_amount END) as amount, (CASE WHEN `fine_partpay`.`f_turnsub` = 0 THEN `fine_partpay`.`fpart_amount` ELSE (CASE WHEN `fine_partpay`.`f_turnsub` < 0 THEN 0-`fine_partpay`.`f_turnsub` ELSE `fine_partpay`.`f_turnsub` END) END + `fine_partpay`.`f_fee`) as totalnowamount, (CASE WHEN `fine_partpay`.`f_turnsub` = 0 THEN `fine_partpay`.`fpart_amount` ELSE (CASE WHEN `fine_partpay`.`f_turnsub` < 0 THEN 0-`fine_partpay`.`f_turnsub` ELSE `fine_partpay`.`f_turnsub` END) END) as nowamount, CASE WHEN fine_import.f_paymoney = 0 THEN (CASE WHEN fine_import.type = 'A' THEN fine_import.f_amount * 10000 ELSE fine_import.f_amount END) ELSE (CASE WHEN fine_import.f_paymoney < 0 THEN 0 ELSE fine_import.f_paymoney END) END  as grandamount,`fine_partpay`.`f_source`, fine_partpay.fpart_type, ((CASE WHEN fine_import.type = 'A' THEN fine_import.f_amount * 10000 ELSE fine_import.f_amount END) - fine_import.f_paymoney - fine_import.f_doneamount) as balance, `fine_partpay`.`f_fee`, `fine_partpay`.`f_turnsub`, `fine_partpay`.`f_refund` FROM `fine_project` LEFT JOIN `fine_partpay` ON `fine_partpay`.`f_project_id` = `fine_project`.`fp_no` LEFT JOIN `fine_import` ON `fine_import`.`f_no` = `fine_partpay`.`fpart_fpnum` WHERE fine_import.f_movelog is not null and `fine_partpay`.fpart_amount is not null and `fine_partpay`.`fpart_date` = fine_project.fp_createdate and  `fine_project`.`fp_no` = '".$id."' and `fine_partpay`.`f_ticket_no`='".$ticketno."' order by `fine_partpay`.`f_ticket_no`, `fine_partpay`.`sort_no`");
      }

      /** 收據號碼 - 支匯票 */
      public function export_finePJ_lists_ticket_receipt_report($id)
      {
          return $this->db->query("SELECT c.`fp_createdate`,a.`f_project_id`,f_comment,f_receipt_no, CASE WHEN a.f_turnsub = 0 THEN sum(a.fpart_amount+a.f_fee) ELSE sum(a.f_turnsub) END as paymoney, b.fpart_amount FROM `fine_partpay` a left join (select f_project_id, SUM(CASE WHEN f_turnsub = 0 THEN fpart_amount+f_fee  ELSE f_turnsub END) as fpart_amount from `fine_partpay` WHERE `f_project_id` = '{$id}' and `fpart_type` = '支票') b on a.`f_project_id` = b.`f_project_id` left join `fine_project` c on a.`f_project_id` = c.`fp_no`  WHERE a.`f_project_id` = '{$id}' and (a.`fpart_type` = '支票' or `f_comment` = '作廢') group by f_receipt_no order by a.`f_receipt_no`");
      }

      /** 繳納清冊受處分人 - 轉正專案 */
    public function get_fineCPPJ_lists_user_in_distinct($id)
    {
        return $this->db->query("SELECT `fine_change_project`.`fcp_no`,`fine_import`.`f_no`,`fine_import`.`f_caseid`,`fine_import`.`f_username`, CASE WHEN `fine_import`.`f_donedate` = '0000-00-00' and `fine_import`.`f_doneamount` = 0 and `fine_import`.`f_paylog` is not null THEN '分期' WHEN `fine_import`.`f_donedate` <> '0000-00-00' and `fine_import`.`f_doneamount` <> 0 and length(`fine_import`.`f_paylog`) < 15 THEN '完納' ELSE '分期完納' END as listtype from `fine_change_project` INNER JOIN `fine_partpay` ON  `fine_partpay`.`f_project_id` = `fine_change_project`.`fcp_no` LEFT JOIN `fine_import` ON `fine_partpay`.`fpart_fpnum` = `fine_import`.`f_no` where `fine_change_project`.`fcp_no`='".$id."' group by `fine_import`.`f_caseid`,`fine_import`.`f_username` order by listtype");
    }
    /** 繳納清冊處分人付款明細 - 轉正專案 */
    public function get_fineCPPJ_list_user_pay_detail($id)
    {
        return $this->db->query("SELECT (left(`fine_partpay`.`fpart_date`,4)-1911) as year,SUBSTRING(`fine_partpay`.`fpart_date`, 6, 2) as month, `fine_import`.`f_no`,`fine_import`.`f_year`,`fine_import`.`f_caseid`,`fine_import`.`f_username`,CONCAT((left(`fine_partpay`.`fpart_date`,4)-1911),SUBSTRING(`fine_partpay`.`fpart_date`, 6, 2),SUBSTRING_INDEX(`fine_partpay`.`fpart_date`, '-', -1)) as paydate,`fine_partpay`.`fpart_type`,CASE WHEN `fine_partpay`.`f_turnsub` > 0 and  `fine_partpay`.`f_turnsub` is not null THEN `fine_partpay`.`f_turnsub` ELSE  `fine_partpay`.`fpart_amount` END as fpart_amount,`fine_import`.`f_amount`,`fine_partpay`.`f_fee`, CASE WHEN `fine_partpay`.`f_status` <> '' Then `fine_partpay`.`f_status` ELSE '分期' END as f_status,`fine_partpay`.`f_comment`,CONCAT((left(`fine_import`.`f_donedate`,4)-1911),SUBSTRING(`fine_import`.`f_donedate`, 6, 2),SUBSTRING_INDEX(`fine_import`.`f_donedate`, '-', -1)) as donedate,(left(`fine_import`.`f_donedate`,4)-1911) as d_year,SUBSTRING(`fine_import`.`f_donedate`, 6, 2) as d_month,`fine_import`.`f_doneamount`,`fine_import`.`type` from `fine_partpay` LEFT JOIN `fine_import` ON `fine_import`.`f_no` =  `fine_partpay`.`fpart_fpnum` where `fine_import`.`f_no`='".$id."'");
    }

      /**  毒品罰鍰罰金明細表 - 收繳金額*/
      public function get_finereport_month_case($startdate, $enddate, $type)
      {
        // $casenum = $this->db->query("SELECT a.f_year, 
        //   CASE WHEN b.refNr IS NULL THEN 0 ELSE b.refNr END as casenum, 
        //   CASE WHEN c.receivemoney IS NULL THEN 0 ELSE c.receivemoney END as receivemoney
        //   FROM fine_import a 
        //     LEFT JOIN ( SELECT f_year, COUNT(*) AS refNr FROM fine_import where (f_dellog is null or f_dellog = '') and (f_donedate BETWEEN '".$startdate."' and '".$enddate."') and type='".$type."' GROUP BY f_year) b ON a.f_year = b.f_year 
            
        //     LEFT JOIN ( SELECT f_year, SUM(case when b.fpart_amount is null then a.f_doneamount else (CASE WHEN b.fpart_date BETWEEN '".$startdate."' and '".$enddate."' THEN b.fpart_amount ELSE 0 end) end) as receivemoney FROM fine_import a left join (select fpart_fpnum, fpart_date, fpart_amount from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is null or a.f_dellog = '') and  ((a.f_donedate BETWEEN '".$startdate."' and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."')) and a.type='".$type."' group by a.f_year) c ON a.f_year = c.f_year 
        //     where a.f_year <> '0000' GROUP BY a.f_year order by cast(a.f_year as DECIMAL(10,0))")->result();

        // $del_casenum = $this->db->query("SELECT a.f_year, 
        // CASE WHEN d.refNr IS NULL THEN 0 ELSE d.refNr END as del_casenum, 
        // CASE WHEN e.receivemoney IS NULL THEN 0 ELSE e.receivemoney END as del_receivemoney
        // FROM fine_import a 
        // LEFT JOIN ( SELECT f_year, COUNT(*) AS refNr FROM fine_import where (f_dellog is not null and f_dellog <> '') and (f_donedate BETWEEN '".$startdate."' and '".$enddate."') and type='".$type."' GROUP BY f_year) d ON a.f_year = d.f_year 
        
        // LEFT JOIN ( SELECT f_year, SUM(case when b.fpart_amount is null then a.f_doneamount else (CASE WHEN b.fpart_date BETWEEN '".$startdate."' and '".$enddate."' THEN b.fpart_amount ELSE 0 end) end) as receivemoney FROM fine_import a left join (select fpart_fpnum, fpart_date, fpart_amount from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is not null and a.f_dellog <> '') and  ((a.f_donedate BETWEEN '".$startdate."' and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."')) and a.type='".$type."' group by a.f_year) e ON a.f_year = e.f_year 
        // where a.f_year <> '0000' GROUP BY a.f_year order by cast(a.f_year as DECIMAL(10,0))")->result();

        // $grandcasenum = $this->db->query("SELECT a.f_year,
        // CASE WHEN f.refNr IS NULL THEN 0 ELSE f.refNr END as grandcasenum, 
        // CASE WHEN g.receivemoney IS NULL THEN 0 ELSE g.receivemoney END as grandreceivemoney
        // FROM fine_import a
        // LEFT JOIN ( SELECT f_year, COUNT(*) AS refNr FROM fine_import where (f_dellog is null or f_dellog = '') and (f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') and type='".$type."' GROUP BY f_year) f ON a.f_year = f.f_year 
        
        // LEFT JOIN ( SELECT f_year, SUM(case when b.fpart_amount is null then a.f_doneamount else (CASE WHEN b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."' THEN b.fpart_amount ELSE 0 end) end) as receivemoney FROM fine_import a left join (select fpart_fpnum, fpart_date, fpart_amount from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is null or a.f_dellog = '') and  ((a.f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') or (b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."')) and a.type='".$type."' group by a.f_year) g ON a.f_year = g.f_year 
        // where a.f_year <> '0000' GROUP BY a.f_year order by cast(a.f_year as DECIMAL(10,0))")->result();

        // $del_grandcasenum = $this->db->query("SELECT a.f_year,
        // CASE WHEN h.refNr IS NULL THEN 0 ELSE h.refNr END as del_grandcasenum, 
        // CASE WHEN i.receivemoney IS NULL THEN 0 ELSE i.receivemoney END as del_grandreceivemoney
        // FROM fine_import a 
        // LEFT JOIN ( SELECT f_year, COUNT(*) AS refNr FROM fine_import where (f_dellog is not null and f_dellog <> '') and (f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') and type='".$type."' GROUP BY f_year) h ON a.f_year = h.f_year 
        
        // LEFT JOIN ( SELECT f_year, SUM(case when b.fpart_amount is null then a.f_doneamount else (CASE WHEN b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."' THEN b.fpart_amount ELSE 0 end) end) as receivemoney FROM fine_import a left join (select fpart_fpnum, fpart_date, fpart_amount from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is not null and a.f_dellog <> '') and  ((a.f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') or (b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."')) and a.type='".$type."' group by a.f_year) i ON a.f_year = i.f_year
        // where a.f_year <> '0000' GROUP BY a.f_year order by cast(a.f_year as DECIMAL(10,0))")->result();

        // return array([
        //     'casenum' => $casenum,
        //     'del_casenum' =>  $del_casenum,
        //     'grandcasenum' =>  $grandcasenum,
        //     'del_grandcasenum' =>  $del_grandcasenum
        // ]);

        return $this->db->query("SELECT a.f_year, 
        CASE WHEN b.refNr IS NULL THEN 0 ELSE b.refNr END as casenum, 
        CASE WHEN c.receivemoney IS NULL THEN 0 ELSE c.receivemoney END as receivemoney, 
        CASE WHEN d.refNr IS NULL THEN 0 ELSE d.refNr END as del_casenum, 
        CASE WHEN e.receivemoney IS NULL THEN 0 ELSE e.receivemoney END as del_receivemoney,
        CASE WHEN f.refNr IS NULL THEN 0 ELSE f.refNr END as grandcasenum, 
        CASE WHEN g.receivemoney IS NULL THEN 0 ELSE g.receivemoney END as grandreceivemoney,
        CASE WHEN h.refNr IS NULL THEN 0 ELSE h.refNr END as del_grandcasenum, 
        CASE WHEN i.receivemoney IS NULL THEN 0 ELSE i.receivemoney END as del_grandreceivemoney
        FROM fine_import a 
        LEFT JOIN ( SELECT f_year, COUNT(*) AS refNr FROM fine_import where (f_dellog is null or f_dellog = '') and (f_donedate BETWEEN '".$startdate."' and '".$enddate."') and type='".$type."' GROUP BY f_year) b ON a.f_year = b.f_year 
        
        LEFT JOIN ( SELECT f_year, SUM(case when b.fpart_amount is null then a.f_doneamount else (CASE WHEN b.fpart_date BETWEEN '".$startdate."' and '".$enddate."' THEN b.fpart_amount ELSE 0 end) end) as receivemoney FROM fine_import a inner join (select fpart_fpnum, fpart_date, fpart_amount from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is null or a.f_dellog = '') and  ((a.f_donedate BETWEEN '".$startdate."' and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."')) and a.type='".$type."' group by a.f_year) c ON a.f_year = c.f_year 
        
        LEFT JOIN ( SELECT f_year, COUNT(*) AS refNr FROM fine_import where (f_dellog is not null and f_dellog <> '') and (f_donedate BETWEEN '".$startdate."' and '".$enddate."') and type='".$type."' GROUP BY f_year) d ON a.f_year = d.f_year 
        
        LEFT JOIN ( SELECT f_year, SUM(case when b.fpart_amount is null then a.f_doneamount else (CASE WHEN b.fpart_date BETWEEN '".$startdate."' and '".$enddate."' THEN b.fpart_amount ELSE 0 end) end) as receivemoney FROM fine_import a inner join (select fpart_fpnum, fpart_date, fpart_amount from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is not null and a.f_dellog <> '') and  ((a.f_donedate BETWEEN '".$startdate."' and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."')) and a.type='".$type."' group by a.f_year) e ON a.f_year = e.f_year 
        
        LEFT JOIN ( SELECT f_year, COUNT(*) AS refNr FROM fine_import where (f_dellog is null or f_dellog = '') and (f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') and type='".$type."' GROUP BY f_year) f ON a.f_year = f.f_year 
        
        LEFT JOIN ( SELECT f_year, SUM(case when b.fpart_amount is null then a.f_doneamount else (CASE WHEN b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."' THEN b.fpart_amount ELSE 0 end) end) as receivemoney FROM fine_import a inner join (select fpart_fpnum, fpart_date, fpart_amount from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is null or a.f_dellog = '') and  ((a.f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') or (b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."')) and a.type='".$type."' group by a.f_year) g ON a.f_year = g.f_year 
        
        LEFT JOIN ( SELECT f_year, COUNT(*) AS refNr FROM fine_import where (f_dellog is not null and f_dellog <> '') and (f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') and type='".$type."' GROUP BY f_year) h ON a.f_year = h.f_year 
        
        LEFT JOIN ( SELECT f_year, SUM(case when b.fpart_amount is null then a.f_doneamount else (CASE WHEN b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."' THEN b.fpart_amount ELSE 0 end) end) as receivemoney FROM fine_import a inner join (select fpart_fpnum, fpart_date, fpart_amount from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is not null and a.f_dellog <> '') and  ((a.f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') or (b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."')) and a.type='".$type."' group by a.f_year) i ON a.f_year = i.f_year 
        
        where a.f_year <> '0000' and a.f_year <> '-1' GROUP BY a.f_year order by cast(a.f_year as DECIMAL(10,0))");
      }

      /**  毒品罰鍰罰金明細表 - 執行費*/
      public function get_finereport_month_case_fee($startdate, $enddate, $type)
      {
    

        return $this->db->query("SELECT a.f_year, 
        CASE WHEN c.receivefee IS NULL THEN 0 ELSE c.receivefee END as receivefee, 
        CASE WHEN e.receivefee IS NULL THEN 0 ELSE e.receivefee END as del_receivefee,
        CASE WHEN g.receivefee IS NULL THEN 0 ELSE g.receivefee END as grandreceivefee,
        CASE WHEN i.receivefee IS NULL THEN 0 ELSE i.receivefee END as del_grandreceivefee
        FROM fine_import a         
        LEFT JOIN ( SELECT f_year, SUM(CASE WHEN b.fpart_date BETWEEN '".$startdate."' and '".$enddate."' THEN b.f_fee ELSE 0 end) as receivefee FROM fine_import a inner join (select fpart_fpnum, fpart_date, f_fee from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is null or a.f_dellog = '') and  ((a.f_donedate BETWEEN '".$startdate."' and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."')) and a.type='".$type."' group by a.f_year) c ON a.f_year = c.f_year 
                
        LEFT JOIN ( SELECT f_year, SUM(CASE WHEN b.fpart_date BETWEEN '".$startdate."' and '".$enddate."' THEN b.f_fee ELSE 0 end) as receivefee FROM fine_import a inner join (select fpart_fpnum, fpart_date, f_fee from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is not null and a.f_dellog <> '') and  ((a.f_donedate BETWEEN '".$startdate."' and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."')) and a.type='".$type."' group by a.f_year) e ON a.f_year = e.f_year 
        
        LEFT JOIN ( SELECT f_year, SUM(CASE WHEN b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."' THEN b.f_fee ELSE 0 end) as receivefee FROM fine_import a inner join (select fpart_fpnum, fpart_date, f_fee from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is null or a.f_dellog = '') and  ((a.f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') or (b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."')) and a.type='".$type."' group by a.f_year) g ON a.f_year = g.f_year 
        
        LEFT JOIN ( SELECT f_year, SUM(CASE WHEN b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."' THEN b.f_fee ELSE 0 end) as receivefee FROM fine_import a inner join (select fpart_fpnum, fpart_date, f_fee from fine_partpay ) b on a.f_no=b.fpart_fpnum where (a.f_dellog is not null and a.f_dellog <> '') and  ((a.f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') or (b.fpart_date BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."')) and a.type='".$type."' group by a.f_year) i ON a.f_year = i.f_year 
        
        where a.f_year <> '0000' and a.f_year <> '-1' GROUP BY a.f_year order by cast(a.f_year as DECIMAL(10,0))");
      }

      /**  毒品罰鍰罰金明細表 - 明細內容*/
      function get_finereport_month_case_detail($startdate, $enddate, $type)
      {
          return array(
            "in_range_notdel" => $this->db->query("SELECT a.type,a.f_caseid,a.f_username,a.f_userid,a.f_amount,a.f_donedate,a.f_doneamount,a.f_movelog,a.f_cardlog,a.f_dellog,a.f_execlog,a.f_comment,b.fpart_date, b.fpart_amount,b.fpart_type,b.f_status FROM fine_import a inner join (select fpart_fpnum, fpart_date, fpart_amount,fpart_type,f_status from fine_partpay ) b on a.f_no=b.fpart_fpnum where a.type='".$type."' and (a.f_dellog is null or a.f_dellog = '') and  ((a.f_donedate BETWEEN '".$startdate."' and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."'))")->result(),
            "in_range_del" => $this->db->query("SELECT a.type,a.f_caseid,a.f_username,a.f_userid,a.f_amount,a.f_donedate,a.f_doneamount,a.f_movelog,a.f_cardlog,a.f_dellog,a.f_execlog,a.f_comment,b.fpart_date, b.fpart_amount,b.fpart_type,b.f_status FROM fine_import a inner join (select fpart_fpnum, fpart_date, fpart_amount,fpart_type,f_status from fine_partpay ) b on a.f_no=b.fpart_fpnum where a.type='".$type."' and (a.f_dellog is not null and a.f_dellog <> '') and  ((a.f_donedate BETWEEN '".$startdate."' and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."'))")->result(),
            "notin_range_notdel" => $this->db->query("SELECT a.type,a.f_caseid,a.f_username,a.f_userid,a.f_amount,a.f_donedate,a.f_doneamount,a.f_movelog,a.f_cardlog,a.f_dellog,a.f_execlog,a.f_comment,b.fpart_date, b.fpart_amount,b.fpart_type,b.f_status FROM fine_import a inner join (select fpart_fpnum, fpart_date, fpart_amount,fpart_type,f_status from fine_partpay ) b on a.f_no=b.fpart_fpnum where a.type='".$type."' and (a.f_dellog is null or a.f_dellog = '') and  ((a.f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."'))")->result(),
            "notin_range_del" => $this->db->query("SELECT a.type,a.f_caseid,a.f_username,a.f_userid,a.f_amount,a.f_donedate,a.f_doneamount,a.f_movelog,a.f_cardlog,a.f_dellog,a.f_execlog,a.f_comment,b.fpart_date, b.fpart_amount,b.fpart_type,b.f_status FROM fine_import a inner join (select fpart_fpnum, fpart_date, fpart_amount,fpart_type,f_status from fine_partpay ) b on a.f_no=b.fpart_fpnum where a.type='".$type."' and (a.f_dellog is not null and a.f_dellog <> '') and  ((a.f_donedate BETWEEN CONCAT(YEAR('".$startdate."'),'-01-01') and '".$enddate."') or (b.fpart_date BETWEEN '".$startdate."' and '".$enddate."'))")->result()
          );
      }

      /** 收入憑證月報表 */
      public function get_finereport_ticket_receipt($startdate, $enddate)
      {
          return $this->db->query("SELECT a.`f_project_id`,a.f_comment,a.f_receipt_no, CASE WHEN a.f_turnsub = 0 THEN sum(a.fpart_amount+a.f_fee) ELSE sum(a.f_turnsub) END as paymoney, b.fpart_amount FROM `fine_partpay` a left join (select f_project_id, SUM(CASE WHEN f_turnsub = 0 THEN fpart_amount+f_fee  ELSE f_turnsub END) as fpart_amount from `fine_partpay` WHERE `fpart_type` = '支票') b on a.`f_project_id` = b.`f_project_id` WHERE (a.fpart_date BETWEEN '".$startdate."' and '".$enddate."') and (a.`fpart_type` = '支票' or a.`f_comment` = '作廢') group by a.f_receipt_no order by a.`f_receipt_no`");
      }

      /**
     * 判斷是否已新增過轉正專案
     * @param 帳務專案f_no
     * @return 筆數
     */
    function check_fineCP_project_exist($id)
    {
        $this->db->where('fcp_no', $id);
        $this->db->from('fine_change_project');

        return $this->db->count_all_results();
    }

    /**
     * 取得當日轉正專案總數量
     */
    function getCPFineProject_today_count($date)
    {
        $this->db->where('fcp_createdate', $date);
        $this->db->from('fine_change_project');

        return $this->db->count_all_results();
    }
    /**
     * 轉正專案-確認簽核
     */
    function CPcomplete($id){
        $this->db->set('fcp_status', '簽核完成');
        $this->db->where('fcp_no', $id);
        $this->db->update('fine_change_project');
    }   

    /**
     * 轉正專案 - 已完成專案
     */
    function getfineCPproject_done()
    {
        $this->db->where('fcp_status is NOT NULL',null, FALSE); 
        $this->db->order_by('fcp_createdate', 'DESC');
        return $this->db->get('fine_change_project');
    }

    /**
     * 刪除所有測試帳務資料、專案
     */
    public function deleteAllTestData()
    {
        try {
            $this->db->truncate('fine_partpay');
            $this->db->truncate('fine_import');
            $this->db->truncate('fine_project');
            $this->db->truncate('fine_change_project');

            return true;
        } catch (\Throwable $th) {
            return false;
        }
        
    }

    /** 
     * 取得最新罰鍰憑證編號
     */
    public function getFineCert_no_lastest()
    {
        return $this->db->select('CASE WHEN f_certno is null THEN "0000000" ELSE f_certno END AS f_certno', FALSE)
                ->where('f_type', 'A')
                ->order_by('f_certno', 'DESC')
                ->limit(1)
                ->get('fine_card')->result();
    }

    /** 
     * 取得最新怠金憑證編號
     */
    public function getLazyCert_no_lastest()
    {
        return $this->db->select('CASE WHEN f_certno is null THEN "0000000" ELSE f_certno END AS f_certno', FALSE)
                    ->where('f_type', 'B')
                    ->order_by('f_certno', 'DESC')
                    ->limit(1)
                    ->get('fine_card')->result();
    }

    /** 
     * 匯入/新增罰鍰(怠金)憑證編號
     */
    public function addFinecardData($data)
    {
        return $this->db->insert('fine_card', $data);
    }

	/**
     * 撈取個人 案件編號 姓名 身分證
     * @param f_caseid
     */
    function getFine_empty_filter_caseid($f_caseid)
    {
		$this->db->select('f_no, f_caseid, f_username, f_userid');
        $this->db->like('f_caseid', $f_caseid, 'after');
        return $this->db->get('fine_import');
    }

	/**
	 * ==============處分書管理============
	 */

    /**
     * 取得辭彙庫 -  監所
     */
    function getWordlibPrison()
    {
        return $this->db->get('wordlib_prison');
    }  

    /** 
     * 匯入/新增講習資料
     */
    public function addCourseData($data)
    {
            
        return $this->db->insert('wordlib_courses', $data);
    }

	/** 
     * 新增毒品成分
     */
	function adddrugInd($data)
    {
        $this->db->insert('drug_first_check', $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
    }

	/** 
     * 更新毒品成分
     */
	function updatedrugInd($id, $data)
    {
		$this->db->where('df_num', $id);
		$this->db->update('drug_first_check', $data);
    }

	/** 
     * 新增尿檢成分
     */
	function addurineInd($data)
    {
        $this->db->insert('susp_check', $data);
		$insert_id = $this->db->insert_id();

   		return  $insert_id;
    }

	/** 
     * 獲取毒品成分
     */
	function getdrugInd($id)
    {
		$this->db->where('df_drug', $id);
		$this->db->where('df_level !=', 0);
		return $this->db->get('drug_first_check');
    }

	/** 
     * 更新尿檢成分
     */
	function updateurineInd($id, $data)
    {
		$this->db->where('sc_num', $id);
		$this->db->update('susp_check', $data);
    }

	/**
	 * 獲取 某 毒品成分
	 */
	function getdrugInds($id)
	{
		$this->db->join('drug_first_check', 'df_drug = e_id', 'left');
		$this->db->where('e_id', $id);
		$this->db->where('df_level !=', 0);
		return $this->db->get('drug_evidence');
	}

	/**
	 * 獲取 某 毒品成分(複驗)
	 */
	function getdrugInds_double($id)
	{
		$this->db->join('drug_first_check', 'df_drug = e_id', 'left');
		$this->db->where('e_id', $id);
		$this->db->where('df_type', '複驗');
		$this->db->where('df_level !=', 0);
		return $this->db->get('drug_evidence');
	}
	/**
	 * 刪除 某 毒品 與成份
	 */
	function deldrugInds($id)
	{
		$this->db->where('e_id', $id);
		$this->db->delete('drug_evidence');

		$this->db->where('df_drug', $id);
		$this->db->delete('drug_first_check');
	}

	/**
	 * 獲取 某 尿檢成分
	 */
	function geturineInds($id)
	{
		$this->db->where('sc_num', $id);
		return $this->db->get('susp_check');
	}

    /** 
     * 取得 講習資料
     */
    public function getCourseData()
    {
        return $this->db->get('wordlib_courses');
    }

    /** 
     * 清空講習資料
     */
    public function emptyCourseData()
    {
        $this->db->truncate('wordlib_courses');
    }

	/** 
     * 新案件／案件清單
     */
	function getnewdp($id)
    {

        $this->db->join('cases', 'c_num = s_cnum' ,'LEFT'); 
		$this->db->join('fine_doc', 'fd_cnum = c_num' ,'LEFT'); 
		$this->db->join('disciplinary_project', 's_dp_project = dp_name' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 's_num = sp_snum ' ,'LEFT'); 
        //$this->db->where('s_date >=',date("Y-m-d",strtotime('-3 month'))); //正式版要關掉
        // $this->db->or_where('fd_rollback',0); 
		// $this->db->where('dp_status', '已寄出'); 
		$this->db->where('s_sac_state', '確認送出裁罰');
		$this->db->where('s_dp_project', null);
		// $this->db->where('cases.r_office', $id);
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }
	
	/** 
     * 重處／案件清單
     */
	function getrollbackdp($id)
    {

        $this->db->join('cases', 'c_num = fd_cnum' ,'LEFT'); 
		$this->db->join('suspect', 'c_num = s_cnum' ,'LEFT');
		$this->db->join('disciplinary_project', 's_dp_project = dp_name' ,'LEFT'); 
        //$this->db->join('suspect_punishment ', 's_num = sp_snum ' ,'LEFT'); 
        //$this->db->where('s_date >=',date("Y-m-d",strtotime('-3 month'))); //正式版要關掉
        $this->db->where('fd_rollback',1); 
		// $this->db->where('dp_status', '已寄出'); 
		$this->db->where('s_dp_project', null);
		// $this->db->where('cases.r_office', $id);
        //$this->db->group_by("c_num"); 
        return $this->db->get('fine_doc');
    }

	/**
	 *  專案處理 - 新增專案繳款日期
	 */

	function updatedp_expirdate($id, $expdate, $empno){
        $this->db->set('dp_expirdate', $expdate);
		$this->db->set('dp_empno', $empno);
        $this->db->where('dp_num', $id);
        $this->db->update('disciplinary_project');
    }  

	/**
	 *  專案處理 - 新增專案繳款日期
	 */

	function updatedp_expirdate_noemp($id, $expdate){
        $this->db->set('dp_expirdate', $expdate);
        $this->db->where('dp_num', $id);
        $this->db->update('disciplinary_project');
    }  

	/**
	 *  專案處理 - 新增專案講習
	 */

	function updatedp_course($id, $data){
        $this->db->set('dp_course', $data);
        $this->db->where('dp_num', $id);
        $this->db->update('disciplinary_project');
    }  

	/**
	 * 取得單一專案（條件：專案名）
	 */
	function getdpprojectname($id)//dp_name 讀取
    {
        $this->db->where('dp_num',$id); 
        return $this->db->get('disciplinary_project');
    }

	/**
	 * 個案講習判斷重複
	 */
	function getcase_coursedup($params)//dp_name 讀取
    {
		$this->db->where('fd_lec_date', $params['fd_lec_date']);
		$this->db->where('fd_lec_time', $params['fd_lec_time']);
		$this->db->where('fd_lec_place', $params['fd_lec_place']);
		$this->db->where('fd_sic', $params['fd_sic']);
		$this->db->from('fine_doc');
		return $this->db->count_all_results();
    }

	/**
	 * 送達登陸 - 已送達
	 */
	function getdeliverysuccesslist()
    {
        $this->db->join('disciplinary_project', 's_dp_project = dp_name' ,'LEFT'); 
        $this->db->join('fine_doc ', 's_num = fd_snum ' ,'LEFT'); 
        $this->db->join('fine_doc_delivery ', 'fdd_fdnum = fd_num ' ,'LEFT'); 
        $this->db->where('fdd_status','已送達'); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('suspect');
    }

	/**
	 * 送達登陸 - 退回重處
	 */
	function updaterollback_fd($snum, $data)
	{
		$this->db->where('fd_snum', $snum);
        $this->db->update('fine_doc', $data);
	}

	/**
	 * 送達登陸 - 退回重處
	 */
	function updaterollback_fdd($snum, $data)
	{
		$this->db->where('fdd_snum', $snum);
        $this->db->update('fine_doc_delivery', $data);
	}
	/**
	 *  送達登陸 - 退回重處
	 */

	function updatesp_dp_project($id, $data){
        $this->db->where('s_num', $id);
        $this->db->update('suspect', $data);
    } 
	/**
	 * 送達登陸 - 超過45天的案件數量
	 */
	function getdiff_fd()
	{
		return $this->db->query('SELECT count(*) as overdate from fine_doc_delivery where fdd_maildate is not null and fdd_status <> "已送達" and DATEDIFF(CURRENT_DATE(), fdd_maildate) >= 45');

	}
	/**
	 * 點收本轄案件 - 現有重處專案
	 */
	function getdpproject_rollback()
    {
        $this->db->where('dp_status',null); 
		$this->db->like('dp_name', 'P', 'after');
        $this->db->order_by('dp_num', 'desc');
        return $this->db->get('disciplinary_project');
    }
	/**
	 * 重處作業 - 送批列表
	 */
	function getdpproject_Ready_rollback($id)
    {
        $this->db->where('dp_status!=',null); 
        $this->db->where('dp_status!=','已寄出'); 
		$this->db->where('dp_status!=','公示'); 
		$this->db->like('dp_name', 'P', 'after');
        $this->db->order_by('dp_num', 'desc');
        return $this->db->get('disciplinary_project');
    }
	/**
	 * 送出成公示專案
	 */
	function updatedrbproject($table,$data){
        $this->db->where('dp_num', $table);
        $this->db->update('disciplinary_project', $data);
    }  
	/**
	 * 公示專案 - 更新送達日期
	 */
	function updatefdp1_susp($id, $data){
		// $this->db->join('suspect', 's_num = fdd_snum' ,'LEFT'); 
		// $this->db->join('fine_doc_publicproject', 's_dp_project = fpd_no', 'LEFT');
        $this->db->where('fdd_fdnum', $id);
        $this->db->update('fine_doc_delivery', $data);
    }   

	/**
	 * ==========怠金============
	 */
	function getSP1($id){
        $this->db->where('sp_no',$id); 
        return $this->db->get('surcharge_project');
    }
	function getsplist($id)
    {
        // $this->db->join('cases', 'c_num = s_cnum' ,'LEFT'); 
        // $this->db->join('fine_doc', 'fd_snum = s_num' ,'left'); 
        // $this->db->where('s_sac_state','確認送出裁罰'); 
        $this->db->where('surc_projectnum ',$id); 
        //$this->db->limit(1); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }
	function get1SurOrder($table,$id)
    {
        $this->db->order_by('surc_num','ASC');
        $this->db->where($table,$id); 
        return $this->db->get('surcharges');
    }
	function get1Sur_ed($table,$id)
    {
        // $this->db->order_by('surc_num','DESC');
        // $this->db->limit(1); 
        $this->db->where($table,$id); 
        return $this->db->get('surcharges');
    }
	/**
	 * 個案講習判斷重複
	 */
	function getsur_coursedup($params)//dp_name 讀取
    {
		$this->db->where('surc_lec_date', $params['surc_lec_date']);
		$this->db->where('surc_lec_time', $params['surc_lec_time']);
		$this->db->where('surc_lec_place', $params['surc_lec_place']);
		$this->db->where('surc_sic', $params['surc_sic']);
		$this->db->from('surcharges');
		return $this->db->count_all_results();
    }

	/**
	 *  專案處理 - 新增專案繳款日期
	 */

	function updatesp_expirdate($id, $expdate, $empno){
        $this->db->set('sp_expirdate', $expdate);
		$this->db->set('sp_empno', $empno);
        $this->db->where('sp_num', $id);
        $this->db->update('surcharge_project');
    } 
	/**
	 *  專案處理 - 新增專案講習
	 */

	function updatesp_course($id, $data){
        $this->db->set('sp_course', $data);
        $this->db->where('sp_num', $id);
        $this->db->update('surcharge_project');
    }  

	function updateSurc_updatesanc($id, $data){
        $this->db->where('surc_num', $id);
        $this->db->update('surcharges', $data);
    }  

	/**
     * 刪除怠金專案
     */
    function delSurcProject($id)
    {
        $this->db->select('sp_no');
        $this->db->where('sp_num', $id);
        $no = $this->db->get('surcharge_project')->result()[0]->sp_no;

        if($this->db->count_all_results() > 0)
        {
            $this->db->where('sp_no', $no);
            $this->db->delete('surcharge_project');

			$this->db->set('surc_projectnum', null);
            $this->db->where('surc_projectnum', $no);
            $this->db->update('surcharges');
        }
    }
	// 專案內案件是否已編輯過
	function getsurc_hasedit($id){//用於查看最後的編號
        $this->db->where('surc_projectnum',$id);
		$this->db->where('surc_no is not NULL');
        return $this->db->get('surcharges');
    }
	/** 更新發文日期 字號 */
	function updatespdate($id, $data){
        // $this->db->set('dp_send_date', $data);
        $this->db->where('sp_num', $id);
        $this->db->update('surcharge_project', $data);
    } 
	/** 更新專案內案件 發文日期 */
	function updatespdateAll($id, $data){
        // $this->db->set('fd_date', $data);
        $this->db->where('surc_num', $id);
        $this->db->update('surcharges', $data);
    } 
	/** 怠金處分書寄出 */
	function addSdddelivery($data)
    {
        $this->db->insert('surc_doc_delivery', $data);
    }

	/**
	 * 送出成公示專案
	 */
	function updatedrbspproject($table,$data){
        $this->db->where('sp_num', $table);
        $this->db->update('surcharge_project', $data);
    }

	function updatedsurcelivery($id, $data){
        $this->db->where('sdd_fdnum', $id);
        $this->db->update('surc_doc_delivery', $data);
    } 

	// 怠金送達登陸清單
	function getsurcdeliverylist()//送達登入列表
    {
        $this->db->join('surcharge_project', 'surc_projectnum = sp_no' ,'LEFT'); 
        // $this->db->join('fine_doc ', 's_num = fd_snum' ,'LEFT'); 
        $this->db->join('surc_doc_delivery ', 'sdd_fdnum = surc_no' ,'LEFT'); 
        $this->db->where('sp_status','已寄出'); 
		$this->db->where('sdd_status !=', '已送達');
		// $this->db->or_where('fdd_status is null');
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }

	/**
	 * 送達登陸 - 已送達
	 */
	function getsurcdeliverysuccesslist()
    {
        // $this->db->join('surcharge_project', 'surc_projectnum = sp_no' ,'LEFT'); 
        // $this->db->join('fine_doc ', 's_num = fd_snum ' ,'LEFT'); 
        $this->db->join('surc_doc_delivery ', 'sdd_fdnum = surc_no' ,'LEFT'); 
        $this->db->where('sdd_status','已送達'); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }
	/**
	 * 送達登陸 - 超過45天的案件數量
	 */
	function getdiff_surc()
	{
		return $this->db->query('SELECT count(*) as overdate from surc_doc_delivery where sdd_maildate is not null and sdd_status <> "已送達" and DATEDIFF(CURRENT_DATE(), sdd_maildate) >= 45');

	}

	function updateSdd_doc($id, $data){
        $this->db->where('sdd_fdnum', $id);
        $this->db->update('surc_doc_delivery', $data);
    } 

	function getSurc_by_field($table,$id){//fine_doc
        $this->db->where($table,$id); 
        return $this->db->get('surcharges');
    }

	function getSurcproject_by_field($table,$id){//fine_doc
        $this->db->where($table,$id); 
        return $this->db->get('surcharge_project');
    }

	/**
	 * 送達登陸 - 退回重處
	 */
	function updaterollback_surc($snum, $data)
	{
		$this->db->where('surc_num', $snum);
        $this->db->update('surcharges', $data);
	}

	/**
	 * 送達登陸 - 退回重處
	 */
	function updaterollback_sdd($snum, $data)
	{
		$this->db->where('sdd_num', $snum);
        $this->db->update('surc_doc_delivery', $data);
	}
	/**
	 * 點收本轄案件 - 現有重處專案
	 */
	function getspproject_rollback()
    {
        $this->db->where('sp_status',null); 
		$this->db->like('sp_no', 'P', 'after');
        $this->db->order_by('sp_num', 'desc');
        return $this->db->get('surcharge_project');
    }

	function getsurchangelist_rb()//點收怠金案件table
    {
        // $this->db->join('suspect ', 's_num = surc_snum ' ,'INNER'); 
        // $this->db->join('cases ', 'c_num = surc_cnum ' ,'INNER'); 
        // $this->db->join('fine_doc ', 'fd_listed_no = surc_listed_no ' ,'INNER'); 
        // $this->db->join('phone ', 'p_s_num = surc_snum ' ,'INNER'); 
        // $this->db->join('fine_rec ', 'f_snum = surc_snum ' ,'inner'); //過後改成inner
        // $this->db->join('fine_doc_delivery ', 'fdd_snum = surc_snum ' ,'left'); //過後改成inner
		$this->db->where('surc_rollback', 1);
        $this->db->where('surc_projectnum',NULL); 
		$this->db->order_by('surc_num', 'asc');
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }
	/**
	 * 重處作業 - 送批列表
	 */
	function getspproject_Ready_rollback()
    {
        $this->db->where('sp_status !=',null); 
        $this->db->where('sp_status !=','已寄出'); 
		$this->db->where('sp_status !=','公示'); 
		$this->db->like('sp_no', 'P', 'after');
        $this->db->order_by('sp_num', 'desc');
        return $this->db->get('surcharge_project');
    }
	function updatesurcdelivery($id, $data){
        $this->db->where('sdd_fdnum', $id);
        $this->db->update('surc_doc_delivery', $data);
    } 

	function getsurchangelistSP_public($id)//怠金專案詳細列表
    {
        // $this->db->join('suspect ', 's_num = surc_snum ' ,'INNER'); 
        // $this->db->join('cases ', 'c_num = surc_cnum ' ,'INNER'); 
        // $this->db->join('fine_doc ', 'fd_listed_no = surc_listed_no ' ,'INNER'); 
        // $this->db->join('phone ', 'p_s_num = surc_snum ' ,'INNER'); 
        // $this->db->join('fine_rec ', 'f_snum = surc_snum ' ,'left'); //過後改成inner
        $this->db->join('surc_doc_delivery ', 'sdd_fdnum = surc_no ' ,'left'); //過後改成inner
        $this->db->join('surcharge_project ', 'sp_no = surc_projectnum ' ,'left'); 
        $this->db->where('surc_projectnum',$id); 
        //$this->db->group_by("c_num"); 
        return $this->db->get('surcharges');
    }

	/**
	 * 個案講習判斷重複
	 */
	function getsurccase_coursedup($params)//dp_name 讀取
    {
		$this->db->where('surc_lec_date', $params['surc_lec_date']);
		$this->db->where('surc_lec_time', $params['surc_lec_time']);
		$this->db->where('surc_lec_place', $params['surc_lec_place']);
		$this->db->where('surc_sic', $params['surc_sic']);
		$this->db->from('surcharges');
		return $this->db->count_all_results();
    }
	/** 備註不裁罰單一個案 */
	function uploadsurc_comment($id, $data){
        $this->db->set('surc_comment', $data);
        $this->db->where('surc_num', $id);
        $this->db->update('surcharges');
    }  

	function getSurc_by_checkimprot($p1,$p2,$p3){//用於查看最後的編號
        $this->db->where('surc_listed_no',$p1);
		$this->db->where('surc_olec_date',$p2);
		$this->db->where('surc_sic',$p3);
        return $this->db->get('surcharges');
    }

	function getthirdempno($unselect){
		return $this->db->query('select CONCAT(em_lastname, (case when em_firstname is null then "" else em_firstname end)) as id, CONCAT(em_lastname, (case when em_firstname is null then "" else em_firstname end)) as text from employees where `em_priority`="3" and  CONCAT(em_lastname, (case when em_firstname is null then "" else em_firstname end)) <> "'.$unselect.'"');
	}
	function getthirdempnobysearch($search, $unselect){
		return $this->db->query('select CONCAT(em_lastname, (case when em_firstname is null then "" else em_firstname end)) as id, CONCAT(em_lastname, (case when em_firstname is null then "" else em_firstname end)) as text from employees where `em_priority`="3" and   CONCAT(em_lastname, (case when em_firstname is null then "" else em_firstname end)) like "%'.$search.'%" and  CONCAT(em_lastname, (case when em_firstname is null then "" else em_firstname end)) <> "'.$unselect.'"');
	}
	function getotherdep1($unselect){
		return $this->db->query('select distinct dep_com as id, dep_com as text from dep_list where dep_com <> "'.$unselect.'"');
	}
	function getotherdep1bysearch($search, $unselect){
		return $this->db->query('select distinct dep_com as id, dep_com as text from dep_list where dep_com like "%'.$search.'%" and  dep_com <> "'.$unselect.'"');
	}
	function getotherdep2($parent,$unselect){
		return $this->db->query('select dep_nm as id, dep_nm as text from dep_list where dep_com = "'.$parent.'" and dep_nm <> "'.$unselect.'"');
	}
	function getotherdep2bysearch($parent, $search, $unselect){
		return $this->db->query('select dep_nm as id, dep_nm as text from dep_list where dep_com = "'.$parent.'" and dep_nm like "%'.$search.'%" and  dep_nm <> "'.$unselect.'"');
	}
	function updateSuspRoffice($id, $data){
		$this->db->where('s_num', $id);
        $this->db->update('suspect', $data);
	}

	/**
	 * ===========移送============
	 */
	function updateCall_delivery($id, $data){
        $this->db->where('call_no', $id);
        $this->db->update('call_doc', $data);
    } 

	/**
	 * ==========資料整合功能============
	 */

    /** 
     * 取得 上傳紀錄
     */
    public function getIntegrationData()
    {
        $this->db->order_by('f_belong asc, cast(f_year as unsigned) asc');

        return $this->db->get('upload_log');
    }
    /** 
     * 取得 罰鍰上傳紀錄
     */
    public function getIntegrationFHData()
    {
        $this->db->select('f_year, f_filename');
        $this->db->where('f_belong', 'A');
        $this->db->order_by('cast(f_year as unsigned) asc');

        return $this->db->get('upload_log');
    }
    /** 
     * 取得 怠金上傳紀錄
     */
    public function getIntegrationDJData()
    {
        $this->db->select('f_year, f_filename');
        $this->db->where('f_belong', 'DJA');
        $this->db->order_by('cast(f_year as unsigned) asc');

        return $this->db->get('upload_log');
    }

    /** 
     * 取得 移送上傳紀錄
     */
    public function getIntegrationESData()
    {
        $this->db->select('f_year, f_filename');
        $this->db->where('f_belong', 'D');
        $this->db->order_by('cast(f_year as unsigned) asc');

        return $this->db->get('upload_log');
    }

    /** 
     * 匯入/新增 整合檔案上傳紀錄
     */
    public function addIntegrationLog($data)
    {
        $this->db->where('f_belong', $data['f_belong']);
        $this->db->where('f_year', $data['f_year']);
        $this->db->where('f_filename', $data['f_filename']);

        $this->db->from('upload_log');
        // $id = $this->db->get('fine_import')->result()[0]->f_no;
        if($this->db->count_all_results() > 0)
        {
            $this->db->set('f_ori_filename', $data['f_ori_filename']);
            $this->db->set('f_upload_date', $data['f_upload_date']);
            $this->db->set('f_status', $data['f_status']);
            $this->db->set('emp_no', $data['emp_no']);
            $this->db->where('f_belong', $data['f_belong']);
            $this->db->where('f_year', $data['f_year']);
            
            return $this->db->update('upload_log');
        }
        else
        {
            return $this->db->insert('upload_log', $data);
        }
        
    }
	function addsuspect_lastid($data)
    {
        $this->db->insert('suspect', $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
    }

    /**
     *  3碼郵遞區號
     */
    public function getSuspJson()
    {
        ini_set("allow_url_fopen", 1);
        return file_get_contents(base_url('assets/susp100.json'));
    }
	
	/**
     *  3碼郵遞區號
     */
    public function getZipCode()
    {
        ini_set("allow_url_fopen", 1);
        return file_get_contents(base_url('assets/taiwan_districts.json'));
    }

	/**
     *  藥物成分
     */
    public function getDrugLevel($druglv)
    {
		$this->db->select('drug_name');
        $this->db->where('drug_level', (int)$druglv);

        return $this->db->get('drug_option');
    }

	/**
	 * 用身分證and移送案號搜尋總表
	 */
	public function getFineimportBymovelog($params)
    {
		
        $this->db->where('f_userid', trim($params['userid']));
		$this->db->like('f_movelog', trim($params['moveno'])); 
		
        return $this->db->get('fine_import');
		// return $this->db->query("SELECT * FROM `fine_import` WHERE `f_movelog` LIKE CONCAT('%','".$params['moveno']."','%')  AND `f_userid` = '".$params['userid']."'");
    }
	/**
	 * 用移送案號搜尋總表的個案
	 */
	public function getFineimportBymoveno($params)
    {
		$this->db->like('f_movelog', '_'.trim($params['moveno'])); 
		
        return $this->db->get('fine_import');
		// return $this->db->query("SELECT * FROM `fine_import` WHERE `f_movelog` LIKE CONCAT('%','".$params['moveno']."','%')  AND `f_userid` = '".$params['userid']."'");
    }
	/**
	 * 新增股別log到總表
	 */
	public function addMoveDepLog($id, $data)
    {
		$this->db->set('f_depmovelog', $data);
		$this->db->where('f_no', $id);
        $this->db->update('fine_import');
    }

	/**
	 * 新增執行命令log到總表
	 */
	public function addMoveExecLog($id, $data)
    {
		$this->db->set('f_execlog', $data);
		$this->db->where('f_caseid', $id);
        $this->db->update('fine_import');
    }

	/** paytaipei名單*/
	public function getpaylist(){
		$this->db->where('f_donedate','0000-00-00');
		return $this->db->get('fine_import');
	}

	/** 憑證查詢 */
	public function getFineCertList(){
		$this->db->order_by('f_no','DESC');
		return $this->db->get('fine_card');
	}

	/** 處分書專案當日命名 */
	public function getDisPJnameToday(){
		$this->db->like('dp_name',(date('Y')-1911).date('md'),'after');
		return $this->db->get('disciplinary_project');
	}

	/**
     * 刪除fh處分書專案
     */
    function delDCProject($id)
    {
        $this->db->select('dp_num, dp_name');
        $this->db->where('dp_num', $id);
        $dp_name = $this->db->get('disciplinary_project')->result()[0]->dp_name;

        if($this->db->count_all_results() > 0)
        {
            $this->db->where('dp_num', $id);
            $this->db->delete('disciplinary_project');

			$this->db->set('s_dp_project', null);
			$this->db->where('s_dp_project', $dp_name);
			$this->db->update('suspect');
        }
    }

	/**
	 * 檢查毒物蟲上傳的檔案資料是否有重複的
	 */
	function checkDrugbugCases($name, $finddate, $findtime, $findplace)
    {
		$this->db->join('cases', 's_cnum = c_num ' ,'left');
        $this->db->where('s_name',$name); 
		$this->db->where('s_date',$finddate); 
		$this->db->where('s_time',$findtime); 
		$this->db->where('s_place',$findplace); 
        $this->db->from('suspect');

		return $this->db->count_all_results();
    }   

	/**
	 * 證物入庫清單 group by 查獲分局
	 */
	function getDrugRoffice($searchrange)
    {
		$this->db->distinct('r_office');
		$this->db->select('r_office');
        $this->db->join('drug_evidence', 'c_num = e_c_num' ,'INNER'); 
		$this->db->join('fine_doc','fd_cnum = c_num');
		$this->db->join('suspect','s_num = fd_snum');
        $this->db->where('s_fdate >=',$searchrange['startdate']); 
		$this->db->where('s_fdate <=',$searchrange['enddate']); 
        $this->db->where('e_state',$searchrange['e_state']); 
        return $this->db->get('cases');
    }

	/**
	 * 證物入庫清單 group by 查獲分局
	 */
	function getDrugRoffice_detail($searchrange, $r_office)
    {
		$this->db->select('r_office,s_fno,fd_psystem,fd_num,fd_target,fd_sic,e_name,e_type,e_1_N_W');
        $this->db->join('drug_evidence', 'c_num = e_c_num' ,'INNER'); 
		$this->db->join('fine_doc','fd_cnum = c_num');
		$this->db->join('suspect','s_num = fd_snum');
        $this->db->where('s_date >=',$searchrange['startdate']); 
		$this->db->where('s_date <=',$searchrange['enddate']); 
		$this->db->where('e_state',$searchrange['e_state']);
		$this->db->where('r_office',$r_office); 
        return $this->db->get('cases');
    }

	/**
	 * 證物入庫清單 group by 查獲分局
	 */
	function getDrugRoffice_detail_byCases($searchrange, $r_office)
    {
		$this->db->select('r_office,s_fno,s_name,s_ic,e_name,e_type,e_1_N_W');		
		$this->db->join('suspect','c_num = s_cnum');
        $this->db->join('drug_evidence', 's_num = e_c_num' ,'INNER'); 
        $this->db->where('s_date >=',$searchrange['startdate']); 
		$this->db->where('s_date <=',$searchrange['enddate']); 
		$this->db->where('e_state',$searchrange['e_state']);
		$this->db->where('r_office',$r_office); 
        return $this->db->get('cases');
    }

	function searchSnumbyCnum($cnum){
		$this->db->where('s_cnum', $cnum);
		return $this->db->get('suspect')->result_array();
	}
	function updateSuspCnum($snum, $data){
		$this->db->where('s_num', $snum);
		$this->db->update('suspect', $data);
	}
	function get1DrugSuspbyCnum($cnum)
    {
        $this->db->where('s_cnum',$cnum); 
        return $this->db->get('suspect');
    }
	function getAllCases(){
		$this->db->where('c_status', null);
		return $this->db->get('cases');
	}
	function getCase($cnum)
	{
		$this->db->where('c_num', $cnum);
		return $this->db->get('cases');
	}
	function checkCaseInput($cnum){
		return $this->db->query("select s.nourine, s1.needurine,count(d.casehaddrug) as casehaddrug, count(d1.doublecheck) as doublecheck from cases left join (select s_cnum,count(*) as nourine from suspect where (s_utest_num is null or s_utest_num='') and s_utest_doc is null group by s_cnum) s on s.s_cnum=c_num left join (select s_cnum,count(*) as needurine from suspect group by s_cnum) s1 on s1.s_cnum=c_num left join (select s_cnum,e_id as casehaddrug from suspect left join drug_evidence on s_num=e_c_num where e_del_sta=0 group by e_id) d on d.s_cnum=c_num left join (select df_drug as doublecheck from drug_first_check where df_type='複驗' group by df_drug) d1 on d1.doublecheck=d.casehaddrug where cases.c_num='".$cnum."' and cases.drug_doc is not null");
	}
	function updateDrug($field,$value, $data){
        $this->db->where($field, $value);
		$this->db->where('e_del_sta', 0);
        $this->db->update('drug_evidence', $data);
    } 
	function checkSuspGono($field, $val)
	{
		$this->db->where($field, $val);
		$this->db->where('s_go_no','');
		$this->db->or_where('s_go_no',null);
		return $this->db->get('suspect');
	}
	function updateSuspSacNull($id, $data){
        $this->db->where('s_num', $id);
		$this->db->where('s_sac_state', null);
        $this->db->update('suspect', $data);
    } 
  }
