<?php
class getsqlmod extends CI_Model {
    function __construct() { // 建構值
        parent::__construct (); // 改寫父親
        {
            $this->load->helper('url');
            $this->load->library('zip');        }
        }
    }
    public function createzip(){
        // Read files from directory
        if($this->input->post('but_createzip2') != NULL){
            // File name
            $filename = "backup.zip";
            // Directory path (uploads directory stored in project root)
            $path = 'uploads';

            // Add directory to zip
            $this->zip->read_dir($path);

            // Save the zip file to archivefiles directory
            $this->zip->archive(FCPATH.'/archivefiles/'.$filename);

            // Download
            $this->zip->download($filename);
        }

        // Load view
        $this->load->view('index_view');
    } 

}