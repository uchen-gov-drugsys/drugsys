<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Reward extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }
    
    function table_project_reward($id,$table) {//2階分局獎金 列表
        $this->load->library('table');
        $query = $this->getsqlmod->get3RewardProject3($id,$table)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','初驗淨重','淨重','建議獎金','退回次數');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $drug = $this->getsqlmod->get3Drugtest($susp->s_ic,$susp->s_cnum)->result() ; // 抓sic
            $drug_doc = $this->getsqlmod->get1DrugWithIC($susp->s_ic)->result(); // 抓scnum
            //$query2 = $this->getsqlmod->get2DrugCk('ddc_drug',$drug_doc[0]->e_id); 
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            if(preg_match("/e/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('cases2/editCases2_Reward/' . $susp->s_cnum, $susp->s_ic);//
            else $table_row[] = $susp->s_ic;//
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            /*if($drug_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }*/
            $drug2val=Null;
            $drug2nw=Null;
            $drug2count=NULL;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $drug2val = Null;
                    $drug2nw = Null;
                }
                else{
                    $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
                    $drug2nw = $drug2nw . $drug2->ddc_NW . '<br>';
                    switch ($drug2->ddc_level) {
                        case 1:
                            $drugcount[0] = $drugcount[0] + $drug2->ddc_NW;
                            break;
                        case 2:
                            $drugcount[1] = $drugcount[1] + $drug2->ddc_NW;
                            break;
                        case 3:
                            $drugcount[2] = $drugcount[2] + $drug2->ddc_NW;
                            break;
                        case 4:
                            $drugcount[3] = $drugcount[3] + $drug2->ddc_NW;
                            break;
                        case 5:
                            $drugcount[4] = $drugcount[4] + $drug2->ddc_NW;
                            break;
                    }
                }
            }
            $drugreward = 0;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $$drugreward = 0;
                }
                else{
                    switch ($drug2->ddc_level) {
                        case 1:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 50) $drugreward = $$drugreward + 2500;
                            else if($drug2->ddc_NW >= 50) $drugreward = $drugreward + 5000;
                            break;
                        case 2:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 100) $drugreward = $drugreward + 2000;
                            else if($drug2->ddc_NW >= 100 && $drug2->ddc_NW < 250) $drugreward = $drugreward + 2500;
                            else if($drug2->ddc_NW >= 250 ) $drugreward = $drugreward + 5000;
                            break;
                        case 3:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                        case 4:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                    }
                }
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2nw;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                if($drugcount[0]>0){
                    $drug2count = '1級 = ' . $drugcount[0] . '<br>';
                }
                if($drugcount[1]>0){
                    $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
                }
                if($drugcount[2]>0){
                    $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
                }
                if($drugcount[3]>0){
                    $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
                }
                if($drugcount[4]>0){
                    $drug2count = $drug2count . '5級 = ' . $drugcount[4];
                }
                $table_row[] = $drug2count;
                //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
            }
            if($susp->s_reward_fine == null ){
                if(preg_match("/e/i", $this->session-> userdata('3permit'))) $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$drugreward.'"></input>';
                else $table_row[] = $drugreward;
            }
            else{
                if(preg_match("/e/i", $this->session-> userdata('3permit'))) $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$susp->s_reward_fine.'"></input>';
                else $table_row[] = $susp->s_reward_fine;
            }
            $table_row[] = $susp->s_reward_rej;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_project_reward3($id2) {//2階分局獎金 列表
        $this->load->library('table');
        $query = $this->getsqlmod->get3RewardProject3_n($id2)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','毒品成分','初驗淨重','淨重','核發獎金','退回次數','備註','毒報');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $drug_doc = $this->getsqlmod->get1DrugwithIC($susp->s_ic)->result(); // 抓scnum
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drug_ingredient = Null;
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            $drug2val=Null;
            $drug2nw=Null;
            $drug2count=Null;
            foreach ($drug_doc as $drug3){
                if($drug3->e_id == null){}
                else{
                    $drug_ingredient = $drug_ingredient .'<a href="http://localhost/main2/reward/editdrug2/'.$drug3->e_id .'">'.$drug3->e_id .'</a><br>';
                    $query2 = $this->getsqlmod->get2DrugCk('ddc_drug',$drug3->e_id); 
                    foreach ($query2->result() as $drug2){
                        if($drug2->ddc_ingredient == null){
                            //$drug2val = Null;
                            //$drug2nw = Null;
                        }
                        else{
                            $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
                            $drug2nw = $drug2nw . $drug2->ddc_NW . '<br>';
                            switch ($drug2->ddc_level) {
                                case 1:
                                    $drugcount[0] = $drugcount[0] + $drug2->ddc_NW;
                                    break;
                                case 2:
                                    $drugcount[1] = $drugcount[1] + $drug2->ddc_NW;
                                    break;
                                case 3:
                                    $drugcount[2] = $drugcount[2] + $drug2->ddc_NW;
                                    break;
                                case 4:
                                    $drugcount[3] = $drugcount[3] + $drug2->ddc_NW;
                                    break;
                                case 5:
                                    $drugcount[4] = $drugcount[4] + $drug2->ddc_NW;
                                    break;
                            }
                        }
                    }
                }
            }
            $table_row[] = $susp->s_ic;
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            if($drug_ingredient == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug_ingredient ;
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2nw;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                if($drugcount[0]>0){
                    $drug2count = '1級 = ' . $drugcount[0] . '<br>';
                }
                if($drugcount[1]>0){
                    $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
                }
                if($drugcount[2]>0){
                    $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
                }
                if($drugcount[3]>0){
                    $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
                }
                if($drugcount[4]>0){
                    $drug2count = $drug2count . '5級 = ' . $drugcount[4];
                }
                $table_row[] = $drug2count;
                //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
            }
            if($susp->s_reward_fine == null){
                $table_row[] = '<input id="reward" name="reward['.$susp->s_num.']" type="number" ></input>';
            }
            else{
                $table_row[] = '<input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$susp->s_reward_fine.'"></input><p style="visibility:hidden">'.$susp->s_reward_fine.'</p>';
            }
            //$table_row[] = anchor('reward/rej/' .$susp->s_num, '確認退回').$susp->s_reward_rej;
            $table_row[] = '<a href="#change" data-toggle="modal" data-ln-id="'.$susp->s_num.'" class="callModal">退回</a><br>退回次數：'.$susp->s_reward_rej;
            $table_row[] = $susp->s_reward_remarks;
            if(!isset($drug_doc[0]->e_doc)){
                $table_row[] = '未上傳';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $drug_doc[0]->e_doc, '毒報');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_project_reward3fin($id2) {//2階分局獎金 列表
        $this->load->library('table');
        $query = $this->getsqlmod->get3RewardProject3_n($id2)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','毒品成分','初驗淨重','淨重','核發獎金','退回次數','備註','毒報');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $drug_doc = $this->getsqlmod->get1DrugwithIC($susp->s_ic)->result(); // 抓scnum
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drug_ingredient = Null;
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            $drug2val=Null;
            $drug2nw=Null;
            $drug2count=Null;
            foreach ($drug_doc as $drug3){
                if($drug3->e_id == null){}
                else{
                    $drug_ingredient = $drug_ingredient .$drug3->e_id .'<br>';
                    $query2 = $this->getsqlmod->get2DrugCk('ddc_drug',$drug3->e_id); 
                    foreach ($query2->result() as $drug2){
                        if($drug2->ddc_ingredient == null){
                            //$drug2val = Null;
                            //$drug2nw = Null;
                        }
                        else{
                            $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
                            $drug2nw = $drug2nw . $drug2->ddc_NW . '<br>';
                            switch ($drug2->ddc_level) {
                                case 1:
                                    $drugcount[0] = $drugcount[0] + $drug2->ddc_NW;
                                    break;
                                case 2:
                                    $drugcount[1] = $drugcount[1] + $drug2->ddc_NW;
                                    break;
                                case 3:
                                    $drugcount[2] = $drugcount[2] + $drug2->ddc_NW;
                                    break;
                                case 4:
                                    $drugcount[3] = $drugcount[3] + $drug2->ddc_NW;
                                    break;
                                case 5:
                                    $drugcount[4] = $drugcount[4] + $drug2->ddc_NW;
                                    break;
                            }
                        }
                    }
                }
            }
            $table_row[] = $susp->s_ic;
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2nw;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                if($drugcount[0]>0){
                    $drug2count = '1級 = ' . $drugcount[0] . '<br>';
                }
                if($drugcount[1]>0){
                    $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
                }
                if($drugcount[2]>0){
                    $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
                }
                if($drugcount[3]>0){
                    $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
                }
                if($drugcount[4]>0){
                    $drug2count = $drug2count . '5級 = ' . $drugcount[4];
                }
                $table_row[] = $drug2count;
                //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
            }
            $table_row[] = $susp->s_reward_fine;
            $table_row[] = $susp->s_reward_rej;
            $table_row[] = $susp->s_reward_remarks;
            if(!isset($drug_doc[0]->e_doc)){
                $table_row[] = '未上傳';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $drug_doc[0]->e_doc, '毒報');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_rewardProject1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getrewardprojectAll($id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號', '申請分局','申請時間','申請狀態');
        $table_row = array();
        foreach ($query->result() as $rp)
        {
            $table_row = NULL;
            $table_row[] = $rp->rp_num;
            $table_row[] = anchor('reward/listrp1/' . $rp->rp_name.'/'.$rp->rp_roffice, $rp->rp_name);
            $table_row[] = $rp->rp_roffice;
            $table_row[] = $rp->rp_datetime;
            if($rp->rp_status==null){
                $table_row[] = '待審核';
            }
            else{
                $table_row[] = $rp->rp_status;
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    function table_rewardProject3($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get3rewardprojectAll($id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號','申請時間','申請狀態');
        $table_row = array();
        foreach ($query->result() as $rp)
        {
            $table_row = NULL;
            $table_row[] = $rp->rp3_num;
            $table_row[] = anchor('reward/listrp3/' . $rp->rp3_name, $rp->rp3_name);
            //$table_row[] = $rp->rp3_roffice;
            $table_row[] = $rp->rp3_date;
            if($rp->rp3_status==null){
                $table_row[] = '待審核';
            }
            else{
                $table_row[] = $rp->rp3_status;
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    function table_rewardProject3FIN($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get3rewardprojectFin($id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號','申請時間','申請狀態');
        $table_row = array();
        foreach ($query->result() as $rp)
        {
            $table_row = NULL;
            $table_row[] = $rp->rp3_num;
            $table_row[] = anchor('reward/listrp3fin/' . $rp->rp3_name, $rp->rp3_name);
            //$table_row[] = $rp->rp3_roffice;
            $table_row[] = $rp->rp3_date;
            if($rp->rp3_status==null){
                $table_row[] = '待審核';
            }
            else{
                $table_row[] = $rp->rp3_status;
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
        
    public function listRewardProject() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject3()->result();
        $test_table = $this->table_rewardProject1($table);
        $data['s_table'] = $test_table;
        $type_options = array();
        foreach ($rp as $rp1){
            $type_options[$rp1->rp3_name] = $rp1->rp3_name;
        }
        $data['title'] = "分局專案";
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/e/i", $this->session-> userdata('3permit'))) $data['include'] = 'reward/2rewardproject';
        else $data['include'] = 'reward_query/2rewardproject';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function list3RewardProject() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject3()->result();
        $test_table = $this->table_rewardProject3($table);
        $data['s_table'] = $test_table;
        $type_options = array();
        foreach ($rp as $rp1){
            $type_options[$rp1->rp3_name] = $rp1->rp3_name;
        }
        $data['title'] = "審核專案";
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/e/i", $this->session-> userdata('3permit'))) $data['include'] = 'reward/3rewardproject';
        else $data['include'] = 'reward_query/3rewardproject';
        //$data['include'] = 'reward/3rewardproject';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function listRewardProject_fin() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject3()->result();
        $test_table = $this->table_rewardProject3FIN($table);
        $data['s_table'] = $test_table;
        $type_options = array();
        $data['title'] = "已送出審核專案";
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/e/i", $this->session-> userdata('3permit'))) $data['include'] = 'reward/3rewardprojectFin';
        else $data['include'] = 'reward_query/3rewardprojectFin';
        //$data['include'] = 'reward/3rewardprojectFin';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function listrp1() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $table = $this->uri->segment(4);
        $test_table = $this->table_project_reward($id,$table);
        $data['s_table'] = $test_table;
        $data['title'] = "獎金專案:".$id;
        $data['rp_name'] = $id;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/e/i", $this->session-> userdata('3permit'))) $data['include'] = 'reward/3RPlist';
        else $data['include'] = 'reward_query/3RPlist';
        //$data['include'] = 'reward/3RPlist';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function listrp3fin() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->get3RewardProject3_n($id)->result(); // 使用getsqlmod裡的getdata功能
        //$rp = $this->getsqlmod->getRP1($id)->result();
        $test_table = $this->table_project_reward3fin($id);
        //$drugck = $this->getsqlmod->get1DrugCk($id)->result();
        $data['title'] = "獎金專案:".$id;
        $data['rp_name'] = $id;
        $data['query'] = $query;
        $data['s_table'] = $test_table;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/e/i", $this->session-> userdata('3permit'))) $data['include'] = 'reward/3RPlist_fin';
        else $data['include'] = 'reward_query/3RPlist_fin';
        //$data['include'] = 'reward/3RPlist_fin';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function listrp3() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->get3RewardProject3_n($id)->result(); // 使用getsqlmod裡的getdata功能
        //$rp = $this->getsqlmod->getRP1($id)->result();
        $test_table = $this->table_project_reward3($id);
        //$drugck = $this->getsqlmod->get1DrugCk($id)->result();
        $data['title'] = "獎金專案:".$id;
        $data['rp_name'] = $id;
        $data['query'] = $query;
        $data['s_table'] = $test_table;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/e/i", $this->session-> userdata('3permit'))) $data['include'] = 'reward/3RPlist1';
        else $data['include'] = 'reward_query/3RPlist1';
        //$data['include'] = 'reward/3RPlist1';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function list_SearchALl() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        //$rp = $this->getsqlmod->getRP1($id)->result();
        $this->load->library('table');
        //$db = new mysqli("192.168.31.2","root","test123","db");
        $tj = " 1 = 1 ";
        $tj2 = " 1 = 1 ";
        $name="";
        $ic="";
        $dateRanges[0]=date("Y-m-d");
        $dateRanges[1]=date("Y-m-d");
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
            $tj = " s_name like '%{$name}%'";
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
            //echo $ic;
        } 
        if(!empty($_POST['datepicker'])){
            $dateRanges = explode(' - ', $_POST['datepicker']);
        }
        $query = $this->getsqlmod->get3RewardProject3_search($name,$ic,$dateRanges[0],$dateRanges[1])->result(); 
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','案件編號','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','淨重','驗後淨重','純質淨重','犯罪手法','退回次數');
        $table_row = array();
        $i=0;
        /*foreach ($query as $susp)
        {
            $i++;
            $drug_doc = $this->getsqlmod->get1DrugwithIC($susp->s_ic)->result(); // 抓scnum
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drug_ingredient = Null;
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            $drug2val=Null;
            $drug2nw=Null;
            $drug2count=Null;
            foreach ($drug_doc as $drug3){
                if($drug3->e_id == null){}
                else{
                    $drug_ingredient = $drug_ingredient .$drug3->e_count.$drug3->e_type.$drug3->e_name .'<br>';
                    $query2 = $this->getsqlmod->get2DrugCk('ddc_drug',$drug3->e_id); 
                    foreach ($query2->result() as $drug2){
                        if($drug2->ddc_ingredient == null){
                            //$drug2val = Null;
                            //$drug2nw = Null;
                        }
                        else{
                            $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
                            $drug2nw = $drug2nw . $drug2->ddc_NW . '<br>';
                            switch ($drug2->ddc_level) {
                                case 1:
                                    $drugcount[0] = $drugcount[0] + $drug2->ddc_NW;
                                    break;
                                case 2:
                                    $drugcount[1] = $drugcount[1] + $drug2->ddc_NW;
                                    break;
                                case 3:
                                    $drugcount[2] = $drugcount[2] + $drug2->ddc_NW;
                                    break;
                                case 4:
                                    $drugcount[3] = $drugcount[3] + $drug2->ddc_NW;
                                    break;
                                case 5:
                                    $drugcount[4] = $drugcount[4] + $drug2->ddc_NW;
                                    break;
                            }
                        }
                    }
                }
            }
            $table_row[] = $susp->s_ic;
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            if($drug_ingredient == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug_ingredient ;
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2nw;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                if($drugcount[0]>0){
                    $drug2count = '1級 = ' . $drugcount[0] . '<br>';
                }
                if($drugcount[1]>0){
                    $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
                }
                if($drugcount[2]>0){
                    $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
                }
                if($drugcount[3]>0){
                    $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
                }
                if($drugcount[4]>0){
                    $drug2count = $drug2count . '5級 = ' . $drugcount[4];
                }
                $table_row[] = $drug2count;
                //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
            }
            if(preg_match("/d/i", $this->session-> userdata('3permit'))){
                if($susp->s_reward_fine == null){
                    $table_row[] = '<input id="reward" name="reward['.$susp->s_num.']" type="number" ></input>';
                }
                else{
                    $table_row[] = '<input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$susp->s_reward_fine.'"></input><p style="visibility:hidden">'.$susp->s_reward_fine.'</p>';
                }
            }else $table_row[] = $susp->s_reward_fine;
            $table_row[] = '<a href="#change" data-toggle="modal" data-ln-id="'.$susp->s_num.'" class="callModal">退回</a><br>退回次數：'.$susp->s_reward_rej;
            if(preg_match("/d/i", $this->session-> userdata('3permit'))){
                if($susp->s_reward_remarks == null){
                    $table_row[] = '<textarea id="s_reward_remarks" name="s_reward_remarks['.$susp->s_num.']" ></textarea>';
                }
                else{
                    $table_row[] = '<textarea id="s_reward_remarks" name="s_reward_remarks['.$susp->s_num.']">'.$susp->s_reward_remarks.'</textarea><p style="visibility:hidden">'.$susp->s_reward_remarks.'</p>';
                }
            }else $table_row[] = $susp->s_reward_remarks;
            if(!isset($drug_doc[0]->e_doc)){
                $table_row[] = '未上傳';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $drug_doc[0]->e_doc, '毒報');
            }
            $this->table->add_row($table_row);
        }  */
        $data['title'] = "查詢案件";
        $data['s_table'] = $this->table->generate();
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/e/i", $this->session-> userdata('3permit'))) $data['include'] = 'reward/3RPlistALL';
        else $data['include'] = 'reward_query/3RPlistALL';
        //$data['include'] = 'reward/3RPlistALL';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function addRewardProject3(){//獎金審核 整合為獎金專案
        //var_dump($_POST);
        $this->load->helper('form');
        $data['title'] = "案件處理系統:待裁罰列表";
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $query = $this->getsqlmod->getrewardproject3ALl();
        $countid = $query->num_rows();
        $countid++;
        $value = str_pad($countid,2,'0',STR_PAD_LEFT);
        $now = date('md'); 
        $now2 = date('Y-m-d');
        $taiwan_date = date('Y')-1911; 
        $rp_num = $taiwan_date.$now.$value;
        //echo $rp_num;
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $rp_project=$this->getsqlmod->getRP1_num2($value)->result();
                $data = array(
                    's_reward_project3' => $rp_num,
                    's_reward_status' => '警局承辦人受理中',
                );
                $rp1 = array(
                        'rp_status' => '警局承辦人受理中',
                        'rp3_project_num' => $rp_num,
                );
                $rp2 = array(
                        'rp_status' => '警局承辦人受理中',
                        'rp3_project_num' => $rp_num,
                );
                $this->getsqlmod->updateRP1A($rp_project[0]->rp_name,$rp1);
                $this->getsqlmod->updateRP2($rp_project[0]->rp_name,$rp2);
                $this->getsqlmod->updateSuspReward2($rp_project[0]->rp_name,$data);
            }
            $data1 = array(
                    'rp3_name' => $rp_num,
                    'rp3_date' => $now2,
                    'rp3_empno' => $user,
                    'rp3_status' => '審核中',
                    'rp2_project_num' => $rp_project[0]->rp_name,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addrp3($data1);
            redirect('reward/list3RewardProject/'); 
        }
        else if($_POST['s_status']=='0'){
            foreach ($s_cnum as $key => $value) {
                $rp_project=$this->getsqlmod->getRP1_num2($value)->result();
                $data = array(
                    's_reward_project3' => $_POST['rp_num'],
                    's_reward_status' => '警局承辦人受理中',
                );
                $this->getsqlmod->updateSuspReward2($rp_project[0]->rp_name,$data);
                $rp1 = array(
                        'rp_status' => '警局承辦人受理中',
                        'rp_project_num' => $_POST['rp_num'],
                );
                $rp2 = array(
                        'rp_status' => '警局承辦人受理中',
                        'rp3_project_num' => $_POST['rp_num'],
                );
                $this->getsqlmod->updateRP1A($rp_project[0]->rp_name,$rp1);
                $this->getsqlmod->updateRP2($rp_project[0]->rp_name,$rp2);
            }
            //var_dump($_POST);
            redirect('reward/list3RewardProject/'); 
        }
    }

    public function addRewardProjectFin(){
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $query = $this->getsqlmod->getrewardproject3ALl();
        $now = date('Y-m-d');
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $rp_project=$this->getsqlmod->getRP3_num($value)->result();
                $data = array(
                    's_reward_status' => '已送出審核',
                    's_reward_date' => date('Y-m-d'),
                );
                $data1 = array(
                    'rp3_status' => '已送出審核',
                );
                echo $rp_project[0]->rp3_name;
                $this->getsqlmod->updateSuspReward3($rp_project[0]->rp3_name,$data);
                $this->getsqlmod->updateRP3($value,$data1);
                $rp1 = array(
                        'rp_status' => '已送出審核',
                );
                $this->getsqlmod->updateRP1A($rp_project[0]->rp2_project_num,$rp1);
                $this->getsqlmod->updateRP2($rp_project[0]->rp2_project_num,$rp1);
            }
            //var_dump($_POST);
            redirect('reward/listRewardProject_fin/'); 
        }
    }

    function editdrug2(){
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $drugOpt = $this->getsqlmod->getDrug_option()->result();
        $drugAll = $this->getsqlmod->get1DrugAll($id)->result();
        $Susp = $this->getsqlmod->get1DrugSusp($drugAll[0]->e_c_num)->result();
        $drugck = $this->getsqlmod->get1DrugCk($id)->result();
        $drugck2 = $this->getsqlmod->get2DrugCk('ddc_drug',$id)->result();
        //echo $casesrepair[1]->c_num;
        $type_options = array(
            '毒品粉末、碎塊' => '毒品粉末、碎塊' ,
            '藥碇' => '藥碇',
            '梅碇' => '梅碇',
            '即溶包' => '即溶包',
            '香菸(含菸蒂)' => '香菸(含菸蒂)',
            '菸草' => '菸草',
            '濾嘴' => '濾嘴',
            '吸食器' => '吸食器',
            '吸管' => '吸管',
            '卡片' => '卡片',
            '括盤' => '括盤',
            '棉花棒' => '棉花棒',
            '殘渣袋' => '殘渣袋',
            '大麻' => '大麻',
            '手機殼' => '手機殼',
            '植株' => '植株',
            '種子' => '種子',
       );   
        $type_options1 = array(
            '包' => '包' ,
            '顆' => '顆',
            '支' => '支',
            '張' => '張',
            '個' => '個',
            '組' => '組',
            '株' => '株',
        );
        $type_options2 = array(
            '海洛因' => '海洛因' ,
            '安非他命' => '安非他命',
            '搖頭丸(MDMA)' => '搖頭丸(MDMA)',
            '愷他命(KET)' => '愷他命(KET)',
            '一粒眠' => '一粒眠',
            '大麻' => '大麻',
            '卡西酮類' => '卡西酮類',
            '古柯鹼' => '古柯鹼',
            '無毒品反應' => '無毒品反應',
        );
        $type_options3 = array('' => '請選擇');
        foreach ($Susp as $Susp1){
            $type_options3[$Susp1->s_ic] = $Susp1->s_name;
        }
        $type_options4 = array('' => '請選擇');
        foreach ($drugOpt as $drugOpt1){
            $type_options4[$drugOpt1->drug_level .'級' .$drugOpt1->drug_name] = $drugOpt1->drug_level . '級' . $drugOpt1->drug_name;
        }
        $data['drugAll'] = $drugAll[0];
        $data['drugck'] = $drugck;
        $data['drugck2'] = $drugck2;
        //var_dump($drugck2);
        $data['nameopt'] = $type_options;
        $data['countopt'] = $type_options1;
        $data['nwopt'] = $type_options2;
        $data['test'] = $type_options3;
        $data['drug_opt'] = $type_options4;
        $data['e_id'] = $id;
        $data['e_c_num'] = $drugAll[0]->e_c_num;
        $data['title'] = "毒品修改:". $id;
        $data['nav'] = "navbar2";
        $data['user'] = $this -> session -> userdata('uic');
        $data['headline'] = "測試案件處理系統";
        $data['include'] = 'reward/drug2_edit';
        $this->load->view('template', $data);
    }  

    public function editRewardProject(){
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        foreach ($s_cnum1 as $key => $value) {
            if(isset($_POST['reward'][$value])){
                $data = array(
                    's_reward_fine' => $_POST['reward'][$value],
                    's_reward_remarks' => $_POST['s_reward_remarks'][$value],
                    's_reward_status' => '承辦人送出審核',
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
        };
        redirect('reward/list3RewardProject/'); 
    }
    
    public function rej(){
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $susp = $this->getsqlmod->get1Susp_ed1('s_num',$_POST['snum'])->result();
        $value = $susp[0]->s_reward_rej;
        $value++;
        $data = array(
                's_reward_rej' => $value,
                's_reward_remarks' => $_POST['s_reward_remarks'],
                's_reward_project' => null,
                's_reward_project2' => null,
                's_reward_project3' => null,
                's_reward_status' => null,
        );
        $this->getsqlmod->updateSusp($_POST['snum'],$data);
        //redirect('reward/listrp3/'.$susp[0]->s_reward_project3); 
    }


    public function update_drug2() { //有問題
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed1 ('s_ic',$_POST['e_s_ic'])->result(); 
        //echo $susp[0]->s_reward_project3;
        foreach ($this->input->post('df') as $key => $value) {
            $dataSet1 = array();
            switch ($value) {
                case '海洛因':
                    $dataSet1[] = array (
                          'df_level' => '1',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '安非他命':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '搖頭丸(MDMA)':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '愷他命(KET)':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '一粒眠':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '大麻':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '卡西酮類':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '古柯鹼':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                default:
                    echo " ";
                    break;
            }
            //echo '<br>';
            $this->getsqlmod->deleteDrugCkfor_Ed($_POST['e_id'],$value);
            $this->getsqlmod->adddrug1($dataSet1);
            //var_dump($dataSet1);
        }
        if(!empty($_FILES['e_doc']['name'])){
            $_FILES['e_doc']['name']  = $_POST['e_id'] . $_FILES['e_doc']['name'];
            $config['upload_path']          = 'drugdoc/';
            $config['allowed_types']        = 'doc|pdf|docx';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('e_doc')){
                $error = array('error' => $this->upload->display_errors());
                //redirect('cases2/editCases2/' . $_POST['s_cnum'],'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['e_doc']=$uploadData['file_name'];
                if(!isset($_POST['df2'])) $_POST['df2']=array();
                for($i=0, $count = count($_POST['df2']);$i<$count;$i++) {
                    $dataSet2 = array();
                    $ddc = explode("級",$_POST['df2'][$i]);
                    if(!isset($ddc[1])){
                        $dataSet2 = array (
                            'ddc_level' => null,
                            'ddc_ingredient' => null,
                            'ddc_NW' => $_POST['ddc_NW'][$i],
                            'ddc_RW' => $_POST['ddc_RW'][$i],
                            'ddc_PNW' => $_POST['ddc_PNW'][$i],
                            'ddc_drug' => $_POST['e_id'],
                            'ddc_cnum' => $_POST['e_c_num'],
                            'ddc_num' => $_POST['ddc_num'][$i],
                        );
                    }else{
                        $dataSet2 = array (
                            'ddc_level' => $ddc[0],
                            'ddc_ingredient' => $ddc[1],
                            'ddc_NW' => $_POST['ddc_NW'][$i],
                            'ddc_RW' => $_POST['ddc_RW'][$i],
                            'ddc_PNW' => $_POST['ddc_PNW'][$i],
                            'ddc_num' => $_POST['ddc_num'][$i],
                            'ddc_drug' => $_POST['e_id'],
                            'ddc_cnum' => $_POST['e_c_num'],
                        );
                    }
                    $this->getsqlmod->updateddc($dataSet2);
                }
                if($_POST["link"]=="1"){
                    if(count($_POST['df2'])>0){
                        for($i=0, $count = count($_POST['df2']);$i<$count;$i++) {
                            $ddc = explode("級",$_POST['df2'][$i]);
                            $data = array(
                            'ddc_level' => $ddc[0],
                            'ddc_ingredient' => $ddc[1],
                            'ddc_NW' => $_POST['ddc_NW'][$i],
                            'ddc_RW' => $_POST['ddc_RW'][$i],
                            'ddc_PNW' => $_POST['ddc_PNW'][$i],
                            'ddc_drug' => $_POST['e_id'],
                            'ddc_cnum' => $_POST['e_c_num'],
                            );
                        }
                    }else{
                        $data = array(
                            'ddc_drug' => $_POST['e_id'],
                            'ddc_cnum' => $_POST['e_c_num'],
                        );
                    }
                    $this->getsqlmod->addddc($data);
                    redirect('cases2/editdrug2/' . $_POST['e_id'],'refresh'); 
                }else{
                    unset($_POST['df']);
                    unset($_POST['df2']);
                    unset($_POST['ddc_NW']);
                    unset($_POST['ddc_RW']);
                    unset($_POST['ddc_PNW']);
                    unset($_POST['ddc_num']);
                    unset($_POST['link']);
                    //var_dump($_POST);
                    $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['e_s_ic'])->result();
                    $_POST['e_suspect']=$susp[0]->s_name;
                    $this->getsqlmod->updateDrug1($_POST['e_id'],$_POST);
                    redirect('reward/listrp3/' . $susp[0]->s_reward_project3,'refresh'); 
                }
            }
        }
        else{
            if(!isset($_POST['df2'])) $_POST['df2']=array();
            for($i=0, $count = count($_POST['df2']);$i<$count;$i++) {
                $dataSet2 = array();
                $ddc = explode("級",$_POST['df2'][$i]);
                if(!isset($ddc[1])){
                    $dataSet2 = array (
                        'ddc_level' => null,
                        'ddc_ingredient' => null,
                        'ddc_NW' => $_POST['ddc_NW'][$i],
                        'ddc_RW' => $_POST['ddc_RW'][$i],
                        'ddc_PNW' => $_POST['ddc_PNW'][$i],
                        'ddc_drug' => $_POST['e_id'],
                        'ddc_cnum' => $_POST['e_c_num'],
                        'ddc_num' => $_POST['ddc_num'][$i],
                    );
                }else{
                    $dataSet2 = array (
                        'ddc_level' => $ddc[0],
                        'ddc_ingredient' => $ddc[1],
                        'ddc_NW' => $_POST['ddc_NW'][$i],
                        'ddc_RW' => $_POST['ddc_RW'][$i],
                        'ddc_PNW' => $_POST['ddc_PNW'][$i],
                        'ddc_num' => $_POST['ddc_num'][$i],
                        'ddc_drug' => $_POST['e_id'],
                        'ddc_cnum' => $_POST['e_c_num'],
                    );
                }
                $this->getsqlmod->updateddc($dataSet2);
            }
            if($_POST["link"]=="1"){
                if(count($_POST['df2'])>0){
                    for($i=0, $count = count($_POST['df2']);$i<$count;$i++) {
                        $ddc = explode("級",$_POST['df2'][$i]);
                        $data = array(
                        'ddc_level' => $ddc[0],
                        'ddc_ingredient' => $ddc[1],
                        'ddc_NW' => $_POST['ddc_NW'][$i],
                        'ddc_RW' => $_POST['ddc_RW'][$i],
                        'ddc_PNW' => $_POST['ddc_PNW'][$i],
                        'ddc_drug' => $_POST['e_id'],
                        'ddc_cnum' => $_POST['e_c_num'],
                        );
                    }
                }else{
                    $data = array(
                        'ddc_drug' => $_POST['e_id'],
                        'ddc_cnum' => $_POST['e_c_num'],
                    );
                }
                $this->getsqlmod->addddc($data);
                redirect('reward/editdrug2/' . $_POST['e_id'],'refresh'); 
            }else{
                unset($_POST['df']);
                unset($_POST['df2']);
                unset($_POST['ddc_NW']);
                unset($_POST['ddc_RW']);
                unset($_POST['ddc_PNW']);
                unset($_POST['ddc_num']);
                unset($_POST['link']);
                //var_dump($_POST);
                $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['e_s_ic'])->result();
                $_POST['e_suspect']=$susp[0]->s_name;
                $this->getsqlmod->updateDrug1($_POST['e_id'],$_POST);
                redirect('reward/listrp3/' . $susp[0]->s_reward_project3,'refresh'); 
            }
        }
    }
    
    public function exportRewardCSV(){
        $this->load->dbutil();
        $this->load->helper('download');
        $id = $this->uri->segment(3);
        $filename = '獎金審核'.date('Ymd').'.csv';
        $query = $this->getsqlmod->get3RewardProject3_n($id); 
        $data = $this->dbutil->csv_from_result($query);
        force_download($filename, "\xEF\xBB\xBF" . $data);
    }
    
    function table_reward1($id) {//獎金 列表
        $this->load->library('table');
        $query = $this->getsqlmod->getSuspReward_normal($id)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','案件編號','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','淨重','驗後淨重','純質淨重','犯罪手法','退回次數');
        $table_row = array();
        $i=0;
        $query2='';
        return $this->table->generate();
    }

    function rewardApplication() { //獎金申請json
        include 'config.php';

        $query = 1;
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Date search value
        $searchByFromdate = mysqli_real_escape_string($con,$_POST['searchByFromdate']);
        $searchByTodate = mysqli_real_escape_string($con,$_POST['searchByTodate']);

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (s_ic like '%".$searchValue."%' or 
                s_name like '%".$searchValue."%' or 
                s_reward_status like '%".$searchValue."%' or 
                s_CMethods like '%".$searchValue."%' or 
                s_reward_fine like '%".$searchValue."%' or 
                s_reward_rej like '%".$searchValue."%' or 
                s_cnum like'%".$searchValue."%' ) ";
        }

        ## Date filter
        if($searchByFromdate != '' && $searchByTodate != ''){
            $searchQuery .= " and (s_date between '".$searchByFromdate."' and '".$searchByTodate."' ) ";
        }
        
        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE 
                                 cases.c_num = suspect.s_cnum AND 1 = '".$query."'".$searchQuery);
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from cases INNER JOIN suspect WHERE cases.c_num = suspect.s_cnum AND 1 = '".$query."'".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            $drug = "select * from drug_evidence INNER JOIN drug_double_check WHERE drug_evidence.e_id = drug_double_check.ddc_drug AND drug_evidence.e_s_ic = '".$row['s_ic']."'";//." AND e_c_num ='". $row['s_cnum']."'"; 
            $drugRecords = mysqli_query($con, $drug);
            $drug2val=NULL;
            $drug2nw=Null;
            $drugcount=NULL;
            $drugcount[0]=$drugcount[1]=$drugcount[2]=$drugcount[3]=NULL;
            $drugrw[0]=$drugrw[1]=$drugrw[2]=$drugrw[3]=NULL;
            $drugpnw[0]=$drugpnw[1]=$drugpnw[2]=$drugpnw[3]=NULL;
            while ($row1 = mysqli_fetch_assoc($drugRecords)) {
                //$drug2val  = $row1['ddc_ingredient']; 
                    if(!$row1['ddc_ingredient']){
                        $drug2val = '<strong>未輸入</strong>';
                        //$drug2nw = Null;
                    }
                    else{
                        $drug2val = $drug2val .$row1['ddc_level'].'級' . $row1['ddc_ingredient'].'<br>';
                        //$drug2nw = $drug2nw . $row1['ddc_NW'] . '<br>';
                        switch ($row1['ddc_level']) {
                            case 1:
                                $drugcount[0] = ($drugcount[0] + $row1['ddc_NW']);
                                $drugrw[0] = ($drugrw[0] + $row1['ddc_RW']);
                                $drugpnw[0] = ($drugpnw[0] + $row1['ddc_PNW']);
                                break;
                            case 2:
                                $drugcount[1] = ($drugcount[1] + $row1['ddc_NW']);
                                $drugrw[1] = ($drugrw[1] + $row1['ddc_RW']);
                                $drugpnw[1] = ($drugpnw[1] + $row1['ddc_PNW']);
                                break;
                            case 3:
                                $drugcount[2] = ($drugcount[2] + $row1['ddc_NW']);
                                $drugrw[2] = ($drugrw[2] + $row1['ddc_RW']);
                                $drugpnw[2] = ($drugpnw[2] + $row1['ddc_PNW']);
                                break;
                            case 4:
                                $drugcount[3] = ($drugcount[3] + $row1['ddc_NW']);
                                $drugrw[3] = ($drugrw[3] + $row1['ddc_RW']);
                                $drugpnw[3] = ($drugpnw[3] + $row1['ddc_PNW']);
                                break;
                        }
                    }
            }
            if($drugcount[0]!=NULL) $drugcount[0] = '第一級:'.$drugcount[0]; 
            if($drugcount[1]!=NULL) $drugcount[1] = '<br>第二級:'.$drugcount[1]; 
            if($drugcount[2]!=NULL) $drugcount[2] = '<br>第三級:'.$drugcount[2]; 
            if($drugcount[3]!=NULL) $drugcount[3] = '<br>第四級:'.$drugcount[3]; 
            if($drugrw[0]!=NULL) $drugrw[0] = '第一級:'.$drugrw[0]; 
            if($drugrw[1]!=NULL) $drugrw[1] = '<br>第二級:'.$drugrw[1]; 
            if($drugrw[2]!=NULL) $drugrw[2] = '<br>第三級:'.$drugrw[2]; 
            if($drugrw[3]!=NULL) $drugrw[3] = '<br>第四級:'.$drugrw[3]; 
            if($drugpnw[0]!=NULL) $drugpnw[0] = '第一級:'.$drugpnw[0]; 
            if($drugpnw[1]!=NULL) $drugpnw[1] = '<br>第二級:'.$drugpnw[1]; 
            if($drugpnw[2]!=NULL) $drugpnw[2] = '<br>第三級:'.$drugpnw[2]; 
            if($drugpnw[3]!=NULL) $drugpnw[3] = '<br>第四級:'.$drugpnw[3]; 
            $address = $row['r_county'].$row['r_district'].$row['r_zipcode'].$row['r_address'];
            $data[] = array(
                    "s_num"=>$row['s_num'],
                    "s_ic"=>$row['s_ic'],
                    "s_name"=>$row['s_name'],
                    "s_reward_status"=>$row['s_reward_status'],
                    "s_reward_fine"=>$row['s_reward_fine'],
                    "s_date"=>$row['s_date'],
                    "s_cnum"=>$row['s_cnum'],
                    "s_CMethods"=>$row['s_CMethods'],
                    "s_reward_rej"=>$row['s_reward_rej'],
                    "address"=>$address,
                    "s_office"=>$row['s_office'],
                    "ddc_ingredient"=>$drug2val,
                    "drug2nw"=>$drugcount[0].$drugcount[1].$drugcount[2].$drugcount[3],
                    "drug2rw"=>$drugrw[0].$drugrw[1].$drugrw[2].$drugrw[3],
                    "drug2pnw"=>$drugpnw[0].$drugpnw[1].$drugpnw[2].$drugpnw[3],
                    "s_reward_status" =>'<a href="#change" data-toggle="modal" data-ln-id="'.$row['s_num'].'" class="callModal">退回</a>('.$row['s_reward_rej'].')<br>'.$row['s_reward_status'],
                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }
    
    
}
