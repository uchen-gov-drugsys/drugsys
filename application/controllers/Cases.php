<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Cases extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this -> load -> library('Session/session');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->model ( 'getsqlmod' ); // 載入model
    }    
    function table_cases() {
        $this->load->library('table');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $query = $this->getsqlmod->getCaseslist1($this -> session -> userdata('email')); // 
        $isadmin = $this -> session -> userdata('IsAdmin'); 
        $tmpl = array (
            'table_open' => '<table border="0"  class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '案件編號', '查獲時間', '查獲地點',
            '偵破過程', '查獲單位', '收案單位', '犯嫌人' , ' 戶役政/筆錄/搜扣','其他檔案(逮捕通知書/拘票/同意書)');
        $table_row = array();
        foreach ($query->result() as $cases)
        {
            $taiwan_date = new DateTime($cases->s_date);
            $taiwan_date->modify("-1911 year");
            $new_taiwan_date=$this->adtorc($taiwan_date);
            //由於Y為四位數，利用ltrim去0
            $table_row = NULL;
            if($this -> session -> userdata('IsAdmin') != 2) $table_row[] = anchor('cases/query/' . $cases->c_num, $cases->c_num);
            else $table_row[] = anchor('cases/edit/' . $cases->c_num, $cases->c_num);
            //$table_row[] = $cases->c_num;
            if($cases->s_date==null){
                $table_row[] = '<strong>未輸入日期</strong>';
            }
            else{
                $table_row[] = $new_taiwan_date; 
            }
            if($cases->r_county==null ||$cases->r_district==null ||$cases->r_zipcode==null ||$cases->r_address==null){
                $table_row[] = '<strong>未輸入地點</strong>';
            }
            else{
                $table_row[] = ($cases->r_county . $cases->r_district  . $cases->r_zipcode  . $cases->r_address);
            }
            if($cases->detection_process==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $cases->detection_process;
            }
            $table_row[] = $cases->s_office;
            if($cases->r_office==null){
                $table_row[] = '<strong>未選選</strong>';
            }
            else{
                $table_row[] = $cases->r_office;
            }
            if($cases->s_name==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $cases->s_name;
            }
            if($cases->doc_file==null){
                $table_row[] = '<strong>未上傳文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('uploads/' . $cases->doc_file, '戶役政/筆錄/搜扣');
            }         
            if($cases->other_doc==null){
                $table_row[] = '<strong>未上傳文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('uploads/' . $cases->other_doc, '其他檔案(逮捕通知書/拘票/同意書)');
            }
            //$table_row[] = anchor('cases/delcases/' . $cases->c_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_drug1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Drug($id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '毒品編號', '數量', '毒品名稱','初驗淨重(g)', '毒品級別', '毒品成分', '擁有人', '');
        $table_row = array();
        foreach ($query->result() as $drug)
        {
            $table_row = NULL;
            if($this -> session -> userdata('IsAdmin') != 2) $table_row[] = anchor('cases/querydrug1/' . $drug->e_id, $drug->e_id);
            else $table_row[] = anchor('cases/editdrug1/' . $drug->e_id, $drug->e_id);
            if($drug->e_type == null){
                $table_row[] = '<strong>未輸入日期</strong>';
            }
            else{
                $table_row[] = $drug->e_type;
            }
            if($drug->e_name==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug->e_name;
            }
            $table_row[] = $drug->e_1_N_W;
            if($drug->df_level == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug->df_level;
            }
            if($drug->df_ingredient == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug->df_ingredient;
            }
            if($drug->e_suspect == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug->e_suspect;
            }
            $table_row[] = anchor('cases/deldrug/' . $drug->e_id, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_susp1($id) {
        $this->load->library('table');
        $this->load->helper('url');
        $query = $this->getsqlmod->get1Susp($id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '身份證', '犯嫌人姓名', '犯罪手法','照片','');
        $table_row = array();
        foreach ($query->result() as $susp)
        {
            $table_row = NULL;
            if($this -> session -> userdata('IsAdmin') != 2) $table_row[] = anchor('cases/querysusp1/' . $susp->s_num, $susp->s_ic);
            else $table_row[] = anchor('cases/editsusp1/' . $susp->s_num, $susp->s_ic);
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;
            }
            if($susp->s_CMethods==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_CMethods;
            }
            if($susp->s_pic == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = anchor_popup('1susppic/' . $susp->s_pic, '<img src= '.base_url("/1susppic/").''.$susp->s_pic.' alt="" width="50" height="50">');
            }
            $table_row[] = anchor('cases/delsusp/' . $susp->s_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_car1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Car('v_s_num',$id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '車牌號碼', '交通工具種類', '顏色','');
        $table_row = array();
        foreach ($query->result() as $car)
        {
            $table_row = NULL;
            if($this -> session -> userdata('IsAdmin') != 2)  $table_row[] = anchor('cases/querycar/' . $car->v_num, $car->v_license_no);
            else $table_row[] = anchor('cases/editcar/' . $car->v_num, $car->v_license_no);
            if($car->v_type == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $car->v_type;
            }
            if($car->v_color==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $car->v_color;
            }
            if($this -> session -> userdata('IsAdmin') != 2) ;
            else $table_row[] = anchor('cases/delcar/' . $car->v_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    function table_social($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getSocial('s_num',$id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '社群軟體名稱', '社群帳號1', '社群帳號2','社群帳號3','');
        $table_row = array();
        foreach ($query->result() as $social)
        {
            $table_row = NULL;
            if($this -> session -> userdata('IsAdmin') != 2)  $table_row[] = anchor('cases/querysocialmedia/' . $social->social_media_num, $social->social_media_name);
            else $table_row[] = anchor('cases/editsocialmedia/' . $social->social_media_num, $social->social_media_name);
            if($social->social_media_ac1 == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $social->social_media_ac1;
            }
            if($social->social_media_ac2==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $social->social_media_ac2;
            }
            if($social->social_media_ac3==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $social->social_media_ac3;
            }
            if($this -> session -> userdata('IsAdmin') != 2) ;
            else $table_row[] = anchor('cases/delsocial/' . $social->social_media_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_phone($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getPhone('p_s_num',$id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '手機號碼', '手機序號(IMEI)', '查扣手機','');
        $table_row = array();
        foreach ($query->result() as $phone)
        {
            $table_row = NULL;
            if($this -> session -> userdata('IsAdmin') != 2)  $table_row[] = anchor('cases/queryphone/' . $phone->p_num, $phone->p_no);
            else $table_row[] = anchor('cases/editphone/' . $phone->p_num, $phone->p_no);
            if($phone->p_imei == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $phone->p_imei;
            }
            if($phone->p_conf==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $phone->p_conf;
            }
            if($this -> session -> userdata('IsAdmin') != 2) ;
            else $table_row[] = anchor('cases/delphone/' . $phone->p_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_source($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getSource1('sou_s_num',$id);
        //var_dump($query->result());
        //echo $id;
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '姓名', '手機號碼', '交易時間', '交易地點', '交易金額', '交易數量','');
        $table_row = array();
        foreach ($query->result() as $source)
        {
            $table_row = NULL;
            if($this -> session -> userdata('IsAdmin') != 2) $table_row[] = anchor('cases/querysource/' . $source->sou_num, $source->sou_name);
            else $table_row[] = anchor('cases/editsource/' . $source->sou_num, $source->sou_name);
            if($source->sou_phone == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_phone;
            }
            if($source->sou_time == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_time;
            }
            if($source->sou_place == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_place;
            }
            if($source->sou_amount == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_amount;
            }
            if($source->sou_count == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_count;
            }
            if($this -> session -> userdata('IsAdmin') != 2) ;
            else {$table_row[] = anchor('cases/delsource/' . $source->sou_num, '刪除');};
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    public function listCases() {
       // generate HTML table from query results
        $this->load->helper('form');
        $test_table = $this->table_cases();
        $data['data_table'] = $test_table;
        $data['title'] = "案件處理系統:列表";
        $data['user'] = $this -> session -> userdata('uic');
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_list_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_list_ad';
        else $data['nav'] = 'navbar1_list';
        $data['include'] = 'cases/cases_list';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $this->load->view('template1', $data);
    }

    public function createcases_num() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $query = $this->getsqlmod->getdata(1,1)->result();
        $countid = $query[0]->c_num;
        $lastNum = (int)mb_substr($countid, -4, 4);
        $lastNum++;
        $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $data['c_num']='CN' . $taiwan_date.$value;
        $_POST['s_office']=$this -> session -> userdata('uroffice');
        $_POST['r_office']=$this -> session -> userdata('uoffice');
        $_POST['r_office1']=$this -> getsqlmod -> r_office1($_POST['r_office']);
        $_POST['c_num']=$data['c_num'];
        $_POST['e_empno']=$this -> session -> userdata('email');
        $_POST['e_ed_empno']=$this -> session -> userdata('email');
        //var_dump($_POST);
        //$_POST['rm_num']="12312342";
        $this->getsqlmod->addcases($_POST);
        redirect('cases/newcases/' . $data['c_num'],'refresh'); 
    }
    public function newcases() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        $data['row'] = $casesrepair[0];
        $type_options = array(
            '中正第一分局' => '中正第一分局' ,
            '中正第二分局' => '中正第二分局',
            '文山第一分局' => '文山第一分局',
            '文山第二分局' => '文山第二分局',
            '信義分局' => '信義分局',
            '大安分局' => '大安分局',
            '中山分局' => '中山分局',
            '松山分局' => '松山分局',
            '大同分局' => '大同分局',
            '萬華分局' => '萬華分局',
            '南港分局' => '南港分局',
            '內湖分局' => '內湖分局',
            '士林分局' => '士林分局',
            '北投分局' => '北投分局',
            '刑事警察大隊偵查第一隊' => '刑事警察大隊偵查第一隊',
            '刑事警察大隊偵查第二隊' => '刑事警察大隊偵查第二隊',
            '刑事警察大隊偵查第三隊' => '刑事警察大隊偵查第三隊',
            '刑事警察大隊偵查第四隊' => '刑事警察大隊偵查第四隊',
            '刑事警察大隊偵查第五隊' => '刑事警察大隊偵查第五隊',
            '刑事警察大隊偵查第七隊' => '刑事警察大隊偵查第七隊',
            '刑事警察大隊偵查第八隊' => '刑事警察大隊偵查第八隊',
            '刑事警察大隊偵查第六隊' => '毒緝中心',
            '刑事警察大隊科技犯罪偵查隊' => '科偵隊',
            '刑事警察大隊肅竊組' => '肅竊組',
        );
        $data['c_num']=$data['row']->c_num;
        $data['s_office']=$data['row']->s_office;
        $data['title'] = "案件處理系統:新增";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        $data['uoffice'] = $this -> session -> userdata('uoffice');
        $data['opt'] = $type_options;
        $data['drug_table'] = $this->table_drug1($id);
        $data['s_table'] = $this->table_susp1($id);
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_cases';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    public function createcases() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('table');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $casesrepair = $this->getsqlmod->getCases($_POST['c_num'])->result();
        $data['drug_table'] = $this->table_drug1($_POST['c_num']);
        $data['s_table'] = $this->table_susp1($_POST['c_num']);
        $data['row'] = $casesrepair[0];
        $data['c_num']=$data['row']->c_num;
        $data['s_office']=$data['row']->s_office;
        $link = $_POST['link'];
        $data['title'] = "案件處理系統:新增案件";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases/new_cases';
        $_POST['drug_pic'] = null;
        $_POST['doc_file'] = null;
        $_POST['other_doc'] = null;
        if(!empty($_FILES['drugpic']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['drugpic']['name'];
            $_FILES['file']['type'] = $_FILES['drugpic']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['drugpic']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['drugpic']['error'];
            $_FILES['file']['size'] = $_FILES['drugpic']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['drug_pic']=$uploadData['file_name'];
            }
        }
        if(!empty($_FILES['doc_file']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['doc_file']['name'];
            $_FILES['file']['type'] = $_FILES['doc_file']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['doc_file']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['doc_file']['error'];
            $_FILES['file']['size'] = $_FILES['doc_file']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['doc_file']=$uploadData['file_name'];
            }
        }
        if(!empty($_FILES['other_doc']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['other_doc']['name'];
            $_FILES['file']['type'] = $_FILES['other_doc']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['other_doc']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['other_doc']['error'];
            $_FILES['file']['size'] = $_FILES['other_doc']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['other_doc']=$uploadData['file_name'];
            }
        }
        $_POST['r_address'] = strtoupper($_POST['r_address']);
        $_POST['s_place'] = strtoupper($_POST['s_place']);
        $_POST['s_date'] = $this->rctoad($_POST['s_date']);
        unset($_POST['link']);
        $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
        redirect($link,'refresh'); 
    }
    
    public function newsuspect() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        //$casesrepair = $this->getsqlmod->getCases($id)->result();
        $data['s_cnum']=$id;
        $data['title'] = "案件處理系統:新增犯嫌人";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_suspect';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    public function create_suspect1() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        /*$data['title'] = "案件處理系統";
        $data['user'] = $this -> session -> userdata('uic');
        $data['nav'] = 'navbar1';
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_drug';*/
        $_POST['s_birth'] = $this->rctoad($_POST['s_birth']);
        $link=$_POST['link'];
        $_POST['s_roffice'] = $this->session-> userdata('uroffice');;
        unset($_POST['link']);
        //var_dump($_POST);
            //redirect($link,'refresh'); 
        $_POST['s_CMethods'] = implode(',',$_POST['s_CMethods']); 
        if(!empty($_FILES['susppic']['name'])){
            $_FILES['susppic']['name'] = $_POST['s_cnum'] . $_FILES['susppic']['name'];
            $config['upload_path']          = '1susppic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('susppic')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                redirect($link,'refresh'); 
            }
            else{
                //$this->getsqlmod->addsuspect($_POST);
                $uploadData = $this->upload->data();
                $_POST['s_pic']=$uploadData['file_name'];
                $this->getsqlmod->addsuspect($_POST);
                redirect($link,'refresh'); 
            }
        }
        else{
            $_POST['s_pic']=Null;
            //var_dump($_POST);
            $this->getsqlmod->addsuspect($_POST);
            redirect($link,'refresh'); 
        }
    }

    function edit(){
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        //echo $casesrepair[1]->c_num;
        $type_options = array(
            '中正第一分局' => '中正第一分局' ,
            '中正第二分局' => '中正第二分局',
            '文山第一分局' => '文山第一分局',
            '文山第二分局' => '文山第二分局',
            '信義分局' => '信義分局',
            '大安分局' => '大安分局',
            '中山分局' => '中山分局',
            '松山分局' => '松山分局',
            '大同分局' => '大同分局',
            '萬華分局' => '萬華分局',
            '南港分局' => '南港分局',
            '內湖分局' => '內湖分局',
            '士林分局' => '士林分局',
            '北投分局' => '北投分局',
            '刑事警察大隊偵查第一隊' => '刑事警察大隊偵查第一隊',
            '刑事警察大隊偵查第二隊' => '刑事警察大隊偵查第二隊',
            '刑事警察大隊偵查第三隊' => '刑事警察大隊偵查第三隊',
            '刑事警察大隊偵查第四隊' => '刑事警察大隊偵查第四隊',
            '刑事警察大隊偵查第五隊' => '刑事警察大隊偵查第五隊',
            '刑事警察大隊偵查第七隊' => '刑事警察大隊偵查第七隊',
            '刑事警察大隊偵查第八隊' => '刑事警察大隊偵查第八隊',
            '刑事警察大隊偵查第六隊' => '毒緝中心',
            '刑事警察大隊科技犯罪偵查隊' => '科偵隊',
            '刑事警察大隊肅竊組' => '肅竊組',
        );
        $type_options1 = array(
            '路檢攔查' => '路檢攔查' ,
            '臨檢' => '臨檢',
            '專案勤務(無令狀)' => '專案勤務(無令狀)',
            '毒品調驗人口' => '毒品調驗人口',
            '拘提/搜索' => '拘提/搜索',
            '通知到案' => '通知到案',
            '借訊(提)' => '借訊(提)',
            '報案(110)' => '報案(110)',
            '99' => '其他',
        );
        //$test = array($casesrepair[0]->r_office, $casesrepair[0]->r_office);
        $data['row'] = $casesrepair[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['c_num'] = $id;
        $data['title'] = "案件修改:". $id;
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        $data['drug_table'] = $this->table_drug1($id);
        $data['s_table'] = $this->table_susp1($id);
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/cases_edit';
        $this->load->view('template', $data);
    }  
    
    function query(){///只能查詢
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        $data['row'] = $casesrepair[0];
        $data['c_num'] = $id;
        $data['title'] = "案件修改:". $id;
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        $data['drug_table'] = $this->table_drug1($id);
        $data['s_table'] = $this->table_susp1($id);
        $data['include'] = 'cases_query/cases_edit';
        $this->load->view('template', $data);
    }  
    
    public function new_drug() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $Susp = $this->getsqlmod->get1DrugSusp($id)->result();
        $c_value = substr($id,-3);
        $type_options = array("" => '請選擇');
        foreach ($Susp as $Susp1){
            $type_options[$Susp1->s_ic] = $Susp1->s_name;
        }
        $query = $this->getsqlmod->get1DrugCount()->result(); 
        $countid = $query[0]->e_id;
        $lastNum = (int)mb_substr($countid, -5, 5);
        $lastNum++;
        $value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $data['e_id']='EN' . $taiwan_date . $value;        
        $data['e_c_num']=$id;
        $data['opt']=$type_options;
        $data['e_empno']=$this -> session -> userdata('email');
        $data['e_ed_empno']=$this -> session -> userdata('email');
        $data['title'] = "案件處理系統:新增毒品證物";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_drug';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    public function create_drug1() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        //var_dump($_POST);
        echo '<br>';
        foreach ($this->input->post('df') as $key => $value) {
            $dataSet1 = array();
            switch ($value) {
                case '海洛因':
                    $dataSet1[] = array (
                          'df_level' => '1',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '安非他命':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '搖頭丸(MDMA)':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '愷他命(KET)':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '一粒眠':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '大麻':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '卡西酮類':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '古柯鹼':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                default:
                    echo " ";
                    break;
            }
            echo '<br>';
            $this->getsqlmod->adddrug1($dataSet1);
            //var_dump($dataSet1);
        }
        //echo '<br>';
        unset($_POST['df']);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['e_s_ic'])->result();
        $_POST['e_suspect']=$susp[0]->s_name;
        $this->getsqlmod->adddrug($_POST);
        //var_dump($_POST);
        redirect('cases/edit/' . $_POST['e_c_num'],'refresh'); 
    }
    
    function editdrug1(){
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $drugAll = $this->getsqlmod->get1DrugAll($id)->result();
        $Susp = $this->getsqlmod->get1DrugSusp($drugAll[0]->e_c_num)->result();
        $drugck = $this->getsqlmod->get1DrugCk($id)->result();
        //echo $casesrepair[1]->c_num;
        $type_options = array(
            '毒品粉末、碎塊' => '毒品粉末、碎塊' ,
            '藥碇' => '藥碇',
            '梅碇' => '梅碇',
            '即溶包' => '即溶包',
            '香菸(含菸蒂)' => '香菸(含菸蒂)',
            '菸草' => '菸草',
            '濾嘴' => '濾嘴',
            '吸食器' => '吸食器',
            '吸管' => '吸管',
            '卡片' => '卡片',
            '括盤' => '括盤',
            '棉花棒' => '棉花棒',
            '殘渣袋' => '殘渣袋',
            '大麻' => '大麻',
            '手機殼' => '手機殼',
            '植株' => '植株',
            '種子' => '種子',
       );   
        $type_options1 = array(
            '包' => '包' ,
            '顆' => '顆',
            '支' => '支',
            '張' => '張',
            '個' => '個',
            '組' => '組',
            '株' => '株',
        );
        $type_options2 = array(
            '海洛因' => '海洛因' ,
            '安非他命' => '安非他命',
            '搖頭丸(MDMA)' => '搖頭丸(MDMA)',
            '愷他命(KET)' => '愷他命(KET)',
            '一粒眠' => '一粒眠',
            '大麻' => '大麻',
            '卡西酮類' => '卡西酮類',
            '古柯鹼' => '古柯鹼',
            '無毒品反應' => '無毒品反應',
        );
        $type_options3 = array('' => '請選擇');
        foreach ($Susp as $Susp1){
            $type_options3[$Susp1->s_ic] = $Susp1->s_name;
        }
        $data['drugAll'] = $drugAll[0];
        $data['drugck'] = $drugck;
        $data['nameopt'] = $type_options;
        $data['countopt'] = $type_options1;
        $data['nwopt'] = $type_options2;
        $data['test'] = $type_options3;
        $data['e_id'] = $id;
        $data['e_c_num'] = $drugAll[0]->e_c_num;
        $data['title'] = "毒品修改:". $id;
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        $data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/drug1_edit';
        $this->load->view('template', $data);
    }  
    
    function querydrug1(){
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $drugAll = $this->getsqlmod->get1DrugAll($id)->result();
        $Susp = $this->getsqlmod->get1DrugSusp($drugAll[0]->e_c_num)->result();
        $drugck = $this->getsqlmod->get1DrugCk($id)->result();
        //echo $casesrepair[1]->c_num;
        $type_options = array(
            '毒品粉末、碎塊' => '毒品粉末、碎塊' ,
            '藥碇' => '藥碇',
            '梅碇' => '梅碇',
            '即溶包' => '即溶包',
            '香菸(含菸蒂)' => '香菸(含菸蒂)',
            '菸草' => '菸草',
            '濾嘴' => '濾嘴',
            '吸食器' => '吸食器',
            '吸管' => '吸管',
            '卡片' => '卡片',
            '括盤' => '括盤',
            '棉花棒' => '棉花棒',
            '殘渣袋' => '殘渣袋',
            '大麻' => '大麻',
            '手機殼' => '手機殼',
            '植株' => '植株',
            '種子' => '種子',
       );   
        $type_options1 = array(
            '包' => '包' ,
            '顆' => '顆',
            '支' => '支',
            '張' => '張',
            '個' => '個',
            '組' => '組',
            '株' => '株',
        );
        $type_options2 = array(
            '海洛因' => '海洛因' ,
            '安非他命' => '安非他命',
            '搖頭丸(MDMA)' => '搖頭丸(MDMA)',
            '愷他命(KET)' => '愷他命(KET)',
            '一粒眠' => '一粒眠',
            '大麻' => '大麻',
            '卡西酮類' => '卡西酮類',
            '古柯鹼' => '古柯鹼',
            '無毒品反應' => '無毒品反應',
        );
        $type_options3 = array('' => '請選擇');
        foreach ($Susp as $Susp1){
            $type_options3[$Susp1->s_ic] = $Susp1->s_name;
        }
        $data['drugAll'] = $drugAll[0];
        $data['drugck'] = $drugck;
        $data['nameopt'] = $type_options;
        $data['countopt'] = $type_options1;
        $data['nwopt'] = $type_options2;
        $data['test'] = $type_options3;
        $data['e_id'] = $id;
        $data['e_c_num'] = $drugAll[0]->e_c_num;
        $data['title'] = "毒品修改:". $id;
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        $data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases_query/drug1_edit';
        $this->load->view('template', $data);
    }  
    
    public function update_drug1() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        //var_dump($_POST);
        echo '<br>';
        foreach ($this->input->post('df') as $key => $value) {
            $dataSet1 = array();
            switch ($value) {
                case '海洛因':
                    $dataSet1[] = array (
                          'df_level' => '1',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '安非他命':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '搖頭丸(MDMA)':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '愷他命(KET)':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '一粒眠':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '大麻':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '卡西酮類':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '古柯鹼':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                default:
                    echo " ";
                    break;
            }
            echo '<br>';
            $this->getsqlmod->deleteDrugCkfor_Ed($_POST['e_id'],$value);
            $this->getsqlmod->adddrug1($dataSet1);
            //var_dump($dataSet1);
        }
        //echo '<br>';
        unset($_POST['df']);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['e_s_ic'])->result();
        $_POST['e_suspect']=$susp[0]->s_name;
        $this->getsqlmod->updateDrug1($_POST['e_id'],$_POST);
        //var_dump($_POST);
        redirect('cases/edit/' . $_POST['e_c_num'],'refresh'); 
    }
    
    function deleteCk(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $drugck = $this->getsqlmod->get1DrugCkdel($id)->result();
        $this->getsqlmod->deleteDrugCk($id);
        //var_dump ($drugck);
        redirect('cases/editdrug1/' . $drugck[0]->df_drug,'refresh');
    }
    function editsusp1(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        //var_dump ($susp[0]);
        $image_properties = array(
                  'src' => 'img/logo.gif',
                  'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                  'class' => 'post_images',
                  'width' => '50',
                  'height' => '50',
        );     
        $type_options = array(
            '未受教育' => '未受教育' ,
            '國小' => '國小',
            '國中' => '國中',
            '高中' => '高中',
            '大學' => '大學',
            '碩士' => '碩士',
            '博士' => '博士',
        );
        $type_options1 = array(
            ' ' => '請選擇' ,
            '忘記來源' => '忘記來源',
            '否認施用' => '否認施用',
            '國外購入' => '國外購入',
            '6' => '其他',
        );
        $methods = mb_split(",",$susp[0]->s_CMethods);
        $data['持有'] = '';
        $data['意圖販售而持有'] = '';
        $data['販賣'] = '';
        $data['運輸'] = '';
        $data['栽種'] = '';
        $data['施用'] = '';
        $data['製造'] = '';
        $data['轉讓'] = '';
        foreach($methods as $value){
            $data[$value] = $value;
            //echo $data[$value];
        }
        //var_dump($data);
        $data['row'] = $susp[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['img'] = $image_properties;
        $data['s_cnum'] = $id;
        $data['sourcetable'] = $this->table_source($susp[0]->s_num);
        $data['phonetable'] = $this->table_phone($susp[0]->s_num);
        $data['socialtable'] = $this->table_social($susp[0]->s_num);
        $data['cartable'] = $this->table_car1($susp[0]->s_num);
        $data['title'] = "犯嫌人修改:" .$susp[0]->s_name;
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/edit_suspect';
        $this->load->view('template', $data);
    }  
    
    function querysusp1(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        //var_dump ($susp[0]);
        $image_properties = array(
                  'src' => 'img/logo.gif',
                  'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                  'class' => 'post_images',
                  'width' => '50',
                  'height' => '50',
        );     
        $type_options = array(
            '未受教育' => '未受教育' ,
            '國小' => '國小',
            '國中' => '國中',
            '高中' => '高中',
            '大學' => '大學',
            '碩士' => '碩士',
            '博士' => '博士',
        );
        $type_options1 = array(
            ' ' => '未選擇' ,
            '忘記來源' => '忘記來源',
            '否認施用' => '否認施用',
            '國外購入' => '國外購入',
            '6' => '其他',
        );
        $methods = mb_split(",",$susp[0]->s_CMethods);
        $data['持有'] = '';
        $data['意圖販售而持有'] = '';
        $data['販賣'] = '';
        $data['運輸'] = '';
        $data['栽種'] = '';
        $data['施用'] = '';
        $data['製造'] = '';
        $data['轉讓'] = '';
        foreach($methods as $value){
            $data[$value] = $value;
            //echo $data[$value];
        }
        //var_dump($data);
        $data['row'] = $susp[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['img'] = $image_properties;
        $data['s_cnum'] = $id;
        $data['sourcetable'] = $this->table_source($susp[0]->s_num);
        $data['phonetable'] = $this->table_phone($susp[0]->s_num);
        $data['socialtable'] = $this->table_social($susp[0]->s_num);
        $data['cartable'] = $this->table_car1($susp[0]->s_num);
        $data['title'] = "犯嫌人修改:" .$susp[0]->s_name;
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases_query/edit_suspect';
        $this->load->view('template', $data);
    }  
    
    public function createcar() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['s_ic'])->result();
        //echo $susp[0]->s_cnum;
        for($i=0, $count = count($_POST['v_color']);$i<$count;$i++) {
            $dataSet1 = array();
            $dataSet1[] = array (
                'v_type' => $_POST['v_type'][$i],
                'v_license_no' => $_POST['v_license_no'][$i],
                'v_owner ' => $_POST['s_ic'],
                'v_s_num ' => $_POST['s_num'],
                'v_color' => $_POST['v_color'][$i],
            );
            //var_dump($dataSet1);
            $this->getsqlmod->addcar($dataSet1);
        }
        redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    public function newcar() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_num']=$susp[0]->s_num;
        $data['title'] = "案件處理系統:新增車輛";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_car';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    function editcar(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $car = $this->getsqlmod->get1Car('v_num',$id)->result();
        //var_dump ($susp[0]);
        $type_options = array(
            '機車' => '機車' ,
            '客車' => '客車',
            '貨車' => '貨車',
        );
        $data['row'] = $car[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['s_cnum'] = $id;
        $data['title'] = "車輛修改:".$car[0]->v_license_no;
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/car_edit';
        $this->load->view('template', $data);
    }  
    function querycar(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $car = $this->getsqlmod->get1Car('v_num',$id)->result();
        //var_dump ($susp[0]);
        $type_options = array(
            '機車' => '機車' ,
            '客車' => '客車',
            '貨車' => '貨車',
        );
        $data['row'] = $car[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['s_cnum'] = $id;
        $data['title'] = "車輛查詢:".$car[0]->v_license_no;
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases_query/car_edit';
        $this->load->view('template', $data);
    }  
    
    public function update_car() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $this->getsqlmod->updateCar($_POST['v_num'],$_POST);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['v_owner'])->result();
        //var_dump($susp[0]);
        if($this -> session -> userdata('IsAdmin')!=2) redirect('cases/querysusp1/' . $susp[0]->s_num,'refresh'); 
        else redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }

    public function casesUpdate(){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $now = date('Y-m-d H:i:s');
        $link = $_POST['link'];
        //$_POST['drug_pic'] = null;
        //$_POST['doc_file'] = null;
        //$_POST['other_doc'] = null;
        if(!empty($_FILES['drug_pic']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['drug_pic']['name'];
            $_FILES['file']['type'] = $_FILES['drug_pic']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['drug_pic']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['drug_pic']['error'];
            $_FILES['file']['size'] = $_FILES['drug_pic']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['drug_pic']=$uploadData['file_name'];
            }
        }
        if(!empty($_FILES['doc_file']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['doc_file']['name'];
            $_FILES['file']['type'] = $_FILES['doc_file']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['doc_file']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['doc_file']['error'];
            $_FILES['file']['size'] = $_FILES['doc_file']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['doc_file']=$uploadData['file_name'];
            }
        }
        if(!empty($_FILES['other_doc']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['other_doc']['name'];
            $_FILES['file']['type'] = $_FILES['other_doc']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['other_doc']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['other_doc']['error'];
            $_FILES['file']['size'] = $_FILES['other_doc']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['other_doc']=$uploadData['file_name'];
            }
        }
        $_POST['e_ed_date']=$now;
        //$_POST['e_ed_empno']=;
        unset($_POST['link']);
        if(isset($_POST['c_num'])) $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
        redirect($link,'refresh'); 
    }
    public function suspect1Update(){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $link = $_POST['link'];
        $_POST['s_CMethods'] = implode(',',$_POST['s_CMethods']); 
		$_POST['s_birth'] = $this->tranfer2ADyear($_POST['s_birth']);
        if(!empty($_FILES['susppic']['name'])){
            $_FILES['susppic']['name'] = $_POST['s_cnum'] . $_FILES['susppic']['name'];
            $config['upload_path']          = '1susppic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('susppic')){
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
                //$this->load->view('upload_form', $error);
            }
            else{
                $uploadData = $this->upload->data();
                unset($_POST['link']);
                $_POST['s_pic']=$uploadData['file_name'];
                $this->getsqlmod->updateSusp($_POST['s_num'],$_POST);
                redirect($link,'refresh'); 
            }
        }
        else{
            //var_dump($_POST);
            unset($_POST['link']);
            $this->getsqlmod->updateSusp($_POST['s_num'],$_POST);
            redirect($link,'refresh'); 
        }
        //$now = date('Y-m-d H:i:s');
    }
    
    public function createsocialmedia() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['s_ic'])->result();
        //echo $susp[0]->s_cnum;
        for($i=0, $count = count($_POST['social_media_name']);$i<$count;$i++) {
            $dataSet1 = array();
            $dataSet1[] = array (
                'social_media_name' => $_POST['social_media_name'][$i],
                'social_media_ac1' => $_POST['social_media_ac1'][$i],
                'social_media_ac2 ' => $_POST['social_media_ac2'][$i],
                'social_media_ac3' => $_POST['social_media_ac3'][$i],
                's_ic' => $_POST['s_ic'],
                's_cnum' => $_POST['s_cnum'],
                's_num' => $_POST['s_num'],
            );
            //var_dump($dataSet1);
            $this->getsqlmod->addsocialmedia($dataSet1);
        }
        redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    public function newsocialmedia() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_cnum']= $susp[0]->s_cnum;
        $data['s_num']= $susp[0]->s_num;
        $data['title'] = "案件處理系統:新增社交媒體";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_socialmedia';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    function editsocialmedia(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $social = $this->getsqlmod->getSocial('social_media_num',$id)->result();
        //var_dump ($susp[0]);
        $data['row'] = $social[0];
        //$data['test'] = $test;
        $data['title'] = "修改社交媒體";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/socialmedia_edit';
        $this->load->view('template', $data);
    }  
    
    function querysocialmedia(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $social = $this->getsqlmod->getSocial('social_media_num',$id)->result();
        //var_dump ($susp[0]);
        $data['row'] = $social[0];
        //$data['test'] = $test;
        $data['title'] = "查詢社交媒體";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';

        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases_query/socialmedia_edit';
        $this->load->view('template', $data);
    }  
    
    public function updatesocialmedia() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $this->getsqlmod->updateSocial($_POST['social_media_num'],$_POST);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['s_ic'])->result();
        //var_dump($susp[0]);
        if($this -> session -> userdata('IsAdmin')!=2) redirect('cases/querysusp1/' . $susp[0]->s_num,'refresh'); 
        else redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }

    public function createphone() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['p_s_ic'])->result();
        for($i=0, $count = count($_POST['pr_phone']);$i<$count;$i++) {
            $dataSet1[] = array();
            $dataSet1[$i] = array (
                'pr_path' => $_POST['pr_path'][$i],
                'pr_phone' => $_POST['pr_phone'][$i],
                'pr_name ' => $_POST['pr_name'][$i],
                'pr_time' => $_POST['pr_time'][$i],
                'pr_relationship' => $_POST['pr_relationship'][$i],
                'pr_has_drug' => $_POST['pr_has_drug'][$i],
                'pr_user' => $_POST['pr_user'][$i],
                'pr_seller' => $_POST['pr_seller'][$i],
                'pr_s_ic ' => $_POST['p_s_ic'],
                'pr_s_cnum' => $_POST['p_s_cnum'],
                'pr_p_no' => $_POST['p_no'],
            );
        }
        unset($_POST['pr_path']);
        unset($_POST['pr_phone']);
        unset($_POST['pr_name']);
        unset($_POST['pr_time']);
        unset($_POST['pr_relationship']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_user']);
        unset($_POST['pr_seller']);
        //var_dump($_POST);
        $this->getsqlmod->addphone($_POST);
        $phone = $this->getsqlmod->getPhone('p_s_ic',$_POST['p_s_ic'])->result();
        for($i=0, $count1 = count($dataSet1);$i<$count1;$i++) {
            $dataSet1[$i]['pr_p_num'] = $phone[0]->p_num;
        }
        $this->getsqlmod->addphone_rec($dataSet1);
        redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    public function newphone() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_cnum']= $susp[0]->s_cnum;
        $data['s_num']= $susp[0]->s_num;
        $data['title'] = "案件處理系統:新增手機";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_phone';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    function editphone(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $phone = $this->getsqlmod->getPhone('p_num',$id)->result();
        $phonerec = $this->getsqlmod->getPhoneRec('pr_p_num',$phone[0]->p_num)->result();
        $type_options = array(
            '是' => '是' ,
            '否' => '否',
        );
        $type_options1 = array(
            '撥入' => '撥入' ,
            '撥出' => '撥出',
        );
        //var_dump($phonerec);
        //echo count($phonerec);
        $data['row'] = $phone[0];
        $data['phonerec'] = $phonerec;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['title'] = "手機修改";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/phone_edit';
        $this->load->view('template', $data);
    }  
    
    function queryphone(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $phone = $this->getsqlmod->getPhone('p_num',$id)->result();
        $phonerec = $this->getsqlmod->getPhoneRec('pr_p_num',$phone[0]->p_num)->result();
        $type_options = array(
            '是' => '是' ,
            '否' => '否',
        );
        $type_options1 = array(
            '撥入' => '撥入' ,
            '撥出' => '撥出',
        );
        //var_dump($phonerec);
        //echo count($phonerec);
        $data['row'] = $phone[0];
        $data['phonerec'] = $phonerec;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['title'] = "手機查詢";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases_query/phone_edit';
        $this->load->view('template', $data);
    }  
    
    public function updatephone() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['p_s_ic'])->result();
        if(isset($_POST['pr_path'])){
            for($i=0, $count = count($_POST['pr_path']);$i<$count;$i++) {
                $dataSet1 = array();
                $dataSet1[] = array (
                    'pr_num' => $_POST['pr_num'][$i],
                    'pr_path' => $_POST['pr_path'][$i],
                    'pr_phone' => $_POST['pr_phone'][$i],
                    'pr_name ' => $_POST['pr_name'][$i],
                    'pr_time' => $_POST['pr_time'][$i],
                    'pr_relationship' => $_POST['pr_relationship'][$i],
                    'pr_has_drug' => $_POST['pr_has_drug'][$i],
                    'pr_user' => $_POST['pr_user'][$i],
                    'pr_seller' => $_POST['pr_seller'][$i],
                    'pr_s_ic ' => $_POST['p_s_ic'],
                    'pr_s_cnum' => $_POST['p_s_cnum'],
                    'pr_p_no' => $_POST['p_no'],
                    'pr_p_num' => $_POST['p_num'],
                );
                if(!empty($_POST['pr_num'][$i])) $this->getsqlmod->deletePhonerec($_POST['pr_num'][$i]);
                $this->getsqlmod->addphone_rec($dataSet1);
            }
        }
        echo "<br>";
        unset($_POST['pr_num']);
        unset($_POST['pr_path']);
        unset($_POST['pr_phone']);
        unset($_POST['pr_name']);
        unset($_POST['pr_time']);
        unset($_POST['pr_relationship']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_user']);
        unset($_POST['pr_seller']);
        //var_dump($_POST);
        $this->getsqlmod->updatePhone($_POST['p_num'],$_POST);
        if($this->session->userdata('IsAdmin')!=2)redirect('cases/querysusp1/' . $susp[0]->s_num,'refresh'); 
        else redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    function delphonerec(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $phonerec = $this->getsqlmod->getPhoneRec('pr_num',$id)->result();
        $this->getsqlmod->deletePhonerec($id);
        redirect('cases/editphone/' . $phonerec[0]->pr_p_num,'refresh');
    }

    public function createsource() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['sou_s_ic'])->result();
        for($i=0, $count = count($_POST['sou_sm_name']);$i<$count;$i++) {
            $dataSet1[] = array();
            $dataSet1[$i] = array (
                'sou_sm_name' => $_POST['sou_sm_name'][$i],
                'sou_sm_ac1' => $_POST['sou_sm_ac1'][$i],
                'sou_sm_ac2 ' => $_POST['sou_sm_ac2'][$i],
                'sou_sm_ac3' => $_POST['sou_sm_ac3'][$i],
                'sou_sm_s_ic ' => $_POST['sou_s_ic'],
                'sou_sm_s_cnum' => $_POST['sou_cnum'],
                'sou_sm_phone' => $_POST['sou_phone'],
                'sou_s_num' => $_POST['sou_s_num'],
            );
        }
        //$this->getsqlmod->addsource_sm($dataSet1);
        unset($_POST['sou_sm_name']);
        unset($_POST['sou_sm_ac1']);
        unset($_POST['sou_sm_ac2']);
        unset($_POST['sou_sm_ac3']);
        if(!empty($_FILES['sourcepic']['name'])){
            $_FILES['sourcepic']['name'] = $_POST['sou_s_ic'] . $_FILES['sourcepic']['name'];
            $config['upload_path']          = 'sourcedrugpic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('sourcepic')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['sou_pic']=$uploadData['file_name'];
                $this->getsqlmod->adddrugSou($_POST);
                $source = $this->getsqlmod->getSource('sou_s_ic',$_POST['sou_s_ic'])->result();
                for($i=0, $count1 = count($dataSet1);$i<$count1;$i++) {
                    $dataSet1[$i]['sou_sounum'] = $source[0]->sou_num;
                }
                //var_dump($dataSet1);
                $this->getsqlmod->addsource_sm($dataSet1);
                redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
        }
        else{
            $_POST['sou_pic']=null;
            $this->getsqlmod->adddrugSou($_POST);
            $source = $this->getsqlmod->getSource('sou_s_ic',$_POST['sou_s_ic'])->result();
            for($i=0, $count1 = count($dataSet1);$i<$count1;$i++) {
                $dataSet1[$i]['sou_sounum'] = $source[0]->sou_num;
            }
            $this->getsqlmod->addsource_sm($dataSet1);
            redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
        }
    }
    
    public function newsource() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        //var_dump($susp);
        if(isset($susp[0]->s_cnum)){
            $data['s_ic']=$id;
            $data['s_cnum']= $susp[0]->s_cnum;
            $data['s_num']= $susp[0]->s_num;
            $data['s_num']= $susp[0]->s_num;
            $data['s_name']= $susp[0]->s_name;
        }else{
            $data['s_ic']=$id;
            $data['s_cnum']= '';
            $data['s_num']='';
            $data['s_name']= '';
        }
        $data['title'] = "案件處理系統:溯源";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_source';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    function editsource(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $source = $this->getsqlmod->getSource('sou_num',$id)->result();
        $sourcesm = $this->getsqlmod->getSourceSM('sou_sounum',$source[0]->sou_num)->result();
        $data['row'] = $source[0];
        $data['sourcesm'] = $sourcesm;
        $data['title'] = "修改溯源";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/source_edit';
        $this->load->view('template', $data);
    }  
    
    function querysource(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $source = $this->getsqlmod->getSource('sou_num',$id)->result();
        $sourcesm = $this->getsqlmod->getSourceSM('sou_sounum',$source[0]->sou_num)->result();
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$this -> session -> userdata('uid'))->result();
        $data['row'] = $source[0];
        $data['snum'] = $susp[0]->s_num;
        $data['sourcesm'] = $sourcesm;
        $data['title'] = "查詢溯源";
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases_query/source_edit';
        $this->load->view('template', $data);
    }  
    
    public function updatesource() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['sou_s_ic'])->result();
        if(isset($_POST['sou_sm_name'])){
            for($i=0, $count = count($_POST['sou_sm_name']);$i<$count;$i++) {
                $dataSet1 = array();
                $dataSet1[] = array (
                    'sou_sm_name' => $_POST['sou_sm_name'][$i],
                    'sou_sm_ac1' => $_POST['sou_sm_ac1'][$i],
                    'sou_sm_ac2 ' => $_POST['sou_sm_ac2'][$i],
                    'sou_sm_ac3' => $_POST['sou_sm_ac3'][$i],
                    'sou_sm_s_ic ' => $_POST['sou_s_ic'],
                    'sou_sm_s_cnum' => $_POST['sou_cnum'],
                    'sou_sm_phone' => $_POST['sou_phone'],
                    'sou_sounum' => $_POST['sou_num'],
                );
                if(!empty($_POST['sou_sm_num'][$i])) $this->getsqlmod->deleteSource($_POST['sou_sm_num'][$i]);
                $this->getsqlmod->addsource_sm($dataSet1);
            }
        }
        unset($_POST['sou_sm_num']);
        unset($_POST['sou_sm_name']);
        unset($_POST['sou_sm_ac1']);
        unset($_POST['sou_sm_ac2']);
        unset($_POST['sou_sm_ac3']);
        if(!empty($_FILES['sourcepic']['name'])){
            $_FILES['sourcepic']['name'] = $_POST['sou_s_ic'] . $_FILES['sourcepic']['name'][$i];
            $config['upload_path']          = 'sourcedrugpic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('sourcepic')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
            else{
                //$this->getsqlmod->addsuspect($_POST);
                $uploadData = $this->upload->data();
                $_POST['sou_pic']=$uploadData['file_name'];
                $this->getsqlmod->updateSource($_POST['sou_num'],$_POST);
                redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
        }
        else{
            $_POST['sou_pic']=null;
            $this->getsqlmod->updateSource($_POST['sou_num'],$_POST);
            if($this -> session -> userdata('IsAdmin')!=2) redirect('cases/querysusp1/' . $susp[0]->s_num,'refresh'); 
            else redirect('cases/querysusp1/' . $susp[0]->s_num,'refresh'); 
        }
    }
    
    function delsourcesm(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $source = $this->getsqlmod->getSourceSM('sou_sm_num',$id)->result();
        $this->getsqlmod->deleteSource($id);
        //var_dump ($source[0]);
        redirect('cases/editsource/' . $source[0]->sou_sounum,'refresh');
    }
    function delcases(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $this->getsqlmod->deleteCaseS($id);
        //var_dump ($source[0]);
        redirect('cases/listCases','refresh');
    }
    function delsusp(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        $this->getsqlmod->deleteSusp($id);
        //var_dump ($source[0]);
        redirect('cases/edit/'. $susp[0]->s_cnum ,'refresh');
    }
    function delsource(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->getSource('sou_num',$id)->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_ic',$susp[0]->sou_s_ic)->result();
        $this->getsqlmod->deleteSou($id);
        redirect('cases/editsusp1/'. $susp1[0]->s_num ,'refresh');
    }
    function delphone(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->getPhone('p_num',$id)->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_ic',$susp[0]->p_s_ic)->result();
        $this->getsqlmod->deletePho($id,$susp[0]->p_no);
        redirect('cases/editsusp1/'. $susp1[0]->s_num ,'refresh');
    }
    function delsocial(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->getSocial('social_media_num',$id)->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_ic',$susp[0]->s_ic)->result();
        $this->getsqlmod->deleteSoc($id);
        redirect('cases/editsusp1/'. $susp1[0]->s_num ,'refresh');
    }
    function delcar(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->get1Car('v_num',$id)->result();
        $this->getsqlmod->deleteVec($id);
        redirect('cases/editsusp1/'. $susp[0]->v_s_num ,'refresh');
    }
    function deldrug(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $this->getsqlmod->deleteDrugID($id);
        $drug = $this->getsqlmod->get1Drug_id($id)->result();
        redirect('cases/edit/'. $drug[0]->e_c_num ,'refresh');
    }

    public function getChangeCode(){
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $code = rand(0,9999);
        $this->db->set('em_change_code', $code);
        $this->db->where('em_ic', $id);
        $this->db->update('employees');
        $data['title'] = "驗證碼";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases/ChangePw';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['code'] = $code;
        $this->load->view('template', $data);

    }
    public function editAdmin(){
        ///Wvar_dump($_POST);
        $this->load->helper('form');
        $this->load->model('getsqlmod');
        $enum =mb_split(",",$_POST['enum']);
        ///var_dump($enum);
        foreach ($enum as $key => $value) {
        if(isset($_POST['em_IsAdmin'][$value])){
            $data = array(
                   'em_IsAdmin' => $_POST['em_IsAdmin'][$value],
            );
            $this->getsqlmod->updateAdminStatus($value,$data);
        }
        };
        redirect('cases/admin/'); 
    }

    public function table_rewardProject3($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getrewardproject_normal2($id); 
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號', '申請派出所','申請時間','申請狀態');
        $table_row = array();
        foreach ($query->result() as $rp)
        {
            $table_row = NULL;
            $table_row[] = $rp->rp_num;
            $table_row[] = anchor('cases/listrp1/' . $rp->rp_name, $rp->rp_name);
            $table_row[] = $rp->rp_soffice;
            $table_row[] = $rp->rp_datetime;
            if($rp->rp_status==null){
                $table_row[] = '待審核';
            }
            else{
                $table_row[] = $rp->rp_status;
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    public function table_rewardProject2($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getrewardproject_table2($id); 
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('專案編號', '申請派出所','申請時間','申請狀態');
        $table_row = array();
        foreach ($query->result() as $rp)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases/listrp1/' . $rp->rp_name, $rp->rp_name);
            $table_row[] = $rp->rp_soffice;
			// $table_row[] = $rp->rp_datetime;
            $table_row[] = $this->tranfer2RCyearTrad2($rp->rp_datetime);
            if($rp->rp_status==null){
                $table_row[] = '待審核';
            }
            else{
                $table_row[] = $rp->rp_status;
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    public function listRewardProject() { // 一階員警整合的專案
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject($table)->result();
        $test_table = $this->table_rewardProject3($table);
        $data['s_table'] = $test_table;
        $type_options = array();
        foreach ($rp as $rp1){
            $type_options[$rp1->rp_name] = $rp1->rp_name;
        }
        $data['title'] = "一階員警上傳專案";
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases/2rewardproject';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $this->load->view('template', $data);
    }
    
    public function addRewardProject2(){
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $query = $this->getsqlmod->getrewardproject3ALl();
        $countid = $query->num_rows();
        $countid++;
        $value = str_pad($countid,2,'0',STR_PAD_LEFT);
        $now = date('md'); 
        $now2 = date('Y-m-d');
        $taiwan_date = date('Y')-1911; 
        $rp_num = $taiwan_date.$now.$value;
        //echo $rp_num;
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value1) {
                $rp_project=$this->getsqlmod->getRP1_num($value1)->result();
                $data = array(
                    's_reward_project2' => $rp_num,
                );
                if(!isset($rp_project[0])) $this->getsqlmod->updateSuspReward_snum($value1,$data);
                else $this->getsqlmod->updateSuspReward($rp_project[0]->rp_name,$data);
            }
            $data1 = array(
                    'rp_name' => $rp_num,
                    'rp_roffice' => $office,
                    'rp_empno' => $user,
            );
            $rp1 = array(
                    //'rp_status' => '警局承辦人受理中',
                    'rp_project_num' => $rp_num,
            );
            if($rp_project[0]->rp_name!=NULL) $this->getsqlmod->updateRP1($rp_project[0]->rp_name,$rp1);
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addrp2($data1);
            //var_dump($_POST);
            redirect('cases/list3RewardProject/'); 
        }
        else if($_POST['s_status']=='0'){
            foreach ($s_cnum as $key => $value) {
                $rp_project=$this->getsqlmod->getRP1_num($value)->result();
                $data = array(
                    's_reward_project2' => $_POST['rp_num'],
                );
                //echo $rp_project[0]->rp_name;
                if(isset($rp_project[0]->rp_name)) $this->getsqlmod->updateSuspReward($rp_project[0]->rp_name,$data);
                else $this->getsqlmod->updateSuspReward_snum($value,$data);
                $rp1 = array(
                        'rp_project_num' => $_POST['rp_num'],
                );
                if($rp_project[0]->rp_name!=NULL) $this->getsqlmod->updateRP1($rp_project[0]->rp_name,$rp1);
            }
                //var_dump($_POST);
            redirect('cases/list3RewardProject/'); 
        }
    }

    public function list3Rewardlist() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uroffice');
        if($this -> session -> userdata('IsAdmin')=="2") $rp=$this->getsqlmod->getrewardproject($this->session-> userdata('uoffice'))->result();
        else $rp=$this->getsqlmod->getrewardproject_normal2($table)->result();
        $test_table = $this->table_reward1($table);
        $type_options = array();
        foreach ($rp as $rp1){
            $type_options[$rp1->rp_name] = $rp1->rp_name;
        }
        $data['s_table'] = $test_table;
        $data['title'] = "獎金申請";
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if($this -> session -> userdata('IsAdmin')=="2") {
            $data['nav'] = 'navbar1_ad2';
            $data['include'] = 'cases/3rewardlist_ad';
        }
        else if($this -> session -> userdata('IsAdmin')=="1") {
            $data['nav'] = 'navbar1_ad'; #目前修改
            $data['include'] = 'cases/3rewardlist';
        } 
        else{
            $data['nav'] = 'navbar1';
            $data['include'] = 'cases/3rewardlist';
        }
        $this->load->view('template', $data);
    }
    
    public function list3RewardProject() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject($table)->result();
        $data['title'] = "獎金專案";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases/3rewardproject';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $test_table = $this->table_rewardProject1($table);
        else $test_table = $this->table_rewardProject2($table);
        $data['s_table'] = $test_table;
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $this->load->view('template', $data);
    }
    
    public function listrp1() {
        $this->load->helper('form');
        if($this -> session -> userdata('IsAdmin')=="2") $table = $this->session-> userdata('uoffice');
        else if($this -> session -> userdata('IsAdmin')=="1") $table = $this->session-> userdata('uroffice');
        else $table = $this->session-> userdata('uroffice');
        $id = $this->uri->segment(3);
        $rp = $this->getsqlmod->getRP1($id)->result();
        $test_table = $this->table_project_reward($table,$id);
        $data['s_table'] = $test_table;
        $data['title'] = "獎金專案:".$rp[0]->rp_name;
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases/3RPlist';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $this->load->view('template', $data);
    }
    
    public function addRewardProject(){
        ////var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $soffice = $this -> session -> userdata('uroffice');
        $roffice = $this -> session -> userdata('uoffice');
        $data['include'] = 'cases/3caseslist';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $taiwan_date = date('Y')-1911; 
         date_default_timezone_set("Asia/Taipei");
        $now = date('mdHi'); 
        $rp_num = $taiwan_date.$now;
        ////var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_reward_project' => $rp_num,
                    's_reward_fine' => $_POST['reward'][$value],
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            $data1 = array(
                    'rp_name' => $rp_num,
                    'rp_roffice' => $roffice,
                    'rp_soffice' => $soffice,
                    'rp_empno' => $user,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addrp1($data1);
            redirect('cases/list3Rewardlist/'); 
        }
        else if($_POST['s_status']=='0'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_reward_project' => $_POST['rp_num'],
                    's_reward_fine' => $_POST['reward'][$value],
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            redirect('cases/list3Rewardlist/'); 
        }
        
    }

    public function editRewardProject(){
        ////var_dump($_POST);
        $this->load->helper('form');
        $data['title'] = "案件處理系統:待裁罰列表";
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uroffice');
        $data['include'] = 'cases/3caseslist';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $taiwan_date = date('Y')-1911; 
        $now = date('mdGi'); 
        $rp_num = $taiwan_date.$now;
        ////var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        foreach ($s_cnum1 as $key => $value) {
            $data = array(
                's_reward_fine' => $_POST['reward'][$value],
            );
            $this->getsqlmod->updateSusp($value,$data);
        };
        redirect('cases/list3RewardProject/'); 
    }

    function table_reward1($id) {//獎金 列表
        $this->load->library('table');
        $query = $this->getsqlmod->getSuspReward_normal($id)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','案件編號','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','淨重','驗後淨重','純質淨重','犯罪手法','退回次數');
        $table_row = array();
        $i=0;
        $query2='';
		// echo $id;
		// var_dump($query);
		// exit;
		// foreach ($query as $susp)
        // {
        //     $i++;
        //     $drug = $this->getsqlmod->get3Drugtest($susp->s_ic,$susp->s_cnum)->result() ; // 抓sic
        //     $drug_doc = $this->getsqlmod->get1DrugWithIC($susp->s_ic)->result(); // 抓scnum
        //     if(isset($drug_doc[0])) $query2 = $this->getsqlmod->get2DrugCk('ddc_drug',$drug_doc[0]->e_id); 
        //     $table_row = NULL;
        //     $table_row[] = $susp->s_num;
        //     //$table_row[] = '<input type="checkbox" name="a">';
        //     $drugcount[0]=0;
        //     $drugcount[1]=0;
        //     $drugcount[2]=0;
        //     $drugcount[3]=0;
        //     $drugcount[4]=0;
        //     $drugcount[5]=0;
        //     $table_row[] = $susp->s_cnum;//
        //     $table_row[] = anchor('cases/editCases2_Reward/' . $susp->s_cnum, $susp->s_ic);//
        //     if($susp->s_name == null){
        //         $table_row[] = '<strong>未輸入姓名</strong>';
        //     }
        //     else{
        //         $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
        //     }
        //     if($susp->s_date == null){
        //         $table_row[] = '<strong>未選擇</strong>';
        //     }
        //     else{
        //         $table_row[] = $this->tranfer2RCyearTrad($susp->s_date);
        //     }
        //     if($susp->r_zipcode == null){
        //         $table_row[] = '<strong>未選擇</strong>';
        //     }
        //     else{
        //         $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
        //     }
        //     if($susp->s_office == null){
        //         $table_row[] = '<strong>未選擇</strong>';
        //     }
        //     else{
        //         $table_row[] = $susp->s_office;
        //     }
        //     $drug2val=Null;
        //     $drug2nw=Null;
        //     $drug2count=NULL;
        //     foreach ($drug as $drug2){
        //         if($drug2->ddc_ingredient == null){
        //             $drug2val = Null;
        //             $drug2nw = Null;
        //         }
        //         else{
        //             $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
        //             $drug2nw = $drug2nw . $drug2->ddc_NW . '<br>';
        //             switch ($drug2->ddc_level) {
        //                 case 1:
        //                     $drugcount[0] = $drugcount[0] + $drug2->ddc_NW;
        //                     break;
        //                 case 2:
        //                     $drugcount[1] = $drugcount[1] + $drug2->ddc_NW;
        //                     break;
        //                 case 3:
        //                     $drugcount[2] = $drugcount[2] + $drug2->ddc_NW;
        //                     break;
        //                 case 4:
        //                     $drugcount[3] = $drugcount[3] + $drug2->ddc_NW;
        //                     break;
        //                 case 5:
        //                     $drugcount[4] = $drugcount[4] + $drug2->ddc_NW;
        //                     break;
        //             }
        //         }
        //     }
        //     if($drug2val == null){
        //         $table_row[] = '<strong>未輸入</strong>';
        //     }
        //     else{
        //         $table_row[] = $drug2val;
        //     }
        //     if($drug2nw == null){
        //         $table_row[] = '<strong>未輸入</strong>';
        //     }
        //     else{
        //         $table_row[] = $drug2nw;
        //     }
        //     if($drug2nw == null){
        //         $table_row[] = '<strong>未輸入</strong>';
        //     }
        //     else{
        //         if($drugcount[0]>0){
        //             $drug2count = '1級 = ' . $drugcount[0] . '<br>';
        //         }
        //         if($drugcount[1]>0){
        //             $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
        //         }
        //         if($drugcount[2]>0){
        //             $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
        //         }
        //         if($drugcount[3]>0){
        //             $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
        //         }
        //         if($drugcount[4]>0){
        //             $drug2count = $drug2count . '5級 = ' . $drugcount[4];
        //         }
        //         $table_row[] = $drug2count;
        //         //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
        //     }
        //     // if($susp->s_reward_fine == null || $susp->s_reward_fine == 0){
        //     //     $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$drugreward.'"></input>';
        //     // }
        //     // else{
        //     //     $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$susp->s_reward_fine.'"></input>';
        //     // }
		// 	$table_row[] = [];
        //     // $table_row[] = $susp->s_reward_rej;
		// 	$table_row[] = $susp->s_CMethods;
        //     $table_row[] = anchor('cases/editsp/' .$susp->s_num,'編輯');
        //     $this->table->add_row($table_row);
        // }   
        return $this->table->generate();
    }

    function rewardApplication() { //獎金申請json
        include 'config.php';

        $query = $this -> session -> userdata('uroffice');
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Date search value
        $searchByFromdate = mysqli_real_escape_string($con,$_POST['searchByFromdate']);
        $searchByTodate = mysqli_real_escape_string($con,$_POST['searchByTodate']);

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (s_ic like '%".$searchValue."%' or 
                s_name like '%".$searchValue."%' or 
                s_reward_status like '%".$searchValue."%' or 
                s_CMethods like '%".$searchValue."%' or 
                s_reward_fine like '%".$searchValue."%' or 
                s_reward_rej like '%".$searchValue."%' or 
                s_cnum like'%".$searchValue."%' ) ";
        }

        ## Date filter
        if($searchByFromdate != '' && $searchByTodate != ''){
            $searchQuery .= " and (s_date between '".$searchByFromdate."' and '".$searchByTodate."' ) ";
        }
        
        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        if($this -> session -> userdata('IsAdmin') == "2") {
            $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE 
                                 cases.c_num = suspect.s_cnum AND suspect.s_reward_project IS
                                 NULL AND suspect.s_reward_project2 IS NULL  AND 
                                 suspect.s_reward_project3 IS NULL AND s_office like '中隊%' ".$searchQuery);
        }
        else if($this -> session -> userdata('uoffice') == "刑事警察大隊" && $this -> session -> userdata('uroffice') != "特勤中隊") {
            $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE 
                                 cases.c_num = suspect.s_cnum AND suspect.s_reward_project IS
                                 NULL AND suspect.s_reward_project2 IS NULL  AND 
                                 suspect.s_reward_project3 IS NULL AND r_office = '".$this -> session -> userdata('uofficeAll')."'".$searchQuery);
        }
        else if($this -> session -> userdata('uroffice') == "警備隊") {
            $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE 
                                 cases.c_num = suspect.s_cnum AND suspect.s_reward_project IS
                                 NULL AND suspect.s_reward_project2 IS NULL  AND 
                                 suspect.s_reward_project3 IS NULL AND r_office = '".$this -> session -> userdata('uoffice')."'".$searchQuery);
        }
        else {
            $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE 
                                 cases.c_num = suspect.s_cnum AND suspect.s_reward_project IS
                                 NULL AND suspect.s_reward_project2 IS NULL  AND 
                                 suspect.s_reward_project3 IS NULL AND s_office = '".$query."'".$searchQuery);
        }
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum AND suspect.s_reward_project IS NULL AND suspect.s_reward_project2 IS NULL  AND suspect.s_reward_project3 IS NULL AND s_office = '".$query."'".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            $drug = "select * from drug_evidence INNER JOIN drug_double_check WHERE drug_evidence.e_id = drug_double_check.ddc_drug AND drug_evidence.e_s_ic = '".$row['s_ic']."'";//." AND e_c_num ='". $row['s_cnum']."'"; 
            $drugRecords = mysqli_query($con, $drug);
            $drug2val=NULL;
            $drug2nw=Null;
            $drugcount=NULL;
            $drugcount[0]=$drugcount[1]=$drugcount[2]=$drugcount[3]=NULL;
            $drugrw[0]=$drugrw[1]=$drugrw[2]=$drugrw[3]=NULL;
            $drugpnw[0]=$drugpnw[1]=$drugpnw[2]=$drugpnw[3]=NULL;
            while ($row1 = mysqli_fetch_assoc($drugRecords)) {
                //$drug2val  = $row1['ddc_ingredient']; 
                    if(!$row1['ddc_ingredient']){
                        $drug2val = '<strong>未輸入</strong>';
                        //$drug2nw = Null;
                    }
                    else{
                        $drug2val = $drug2val .$row1['ddc_level'].'級' . $row1['ddc_ingredient'].'<br>';
                        //$drug2nw = $drug2nw . $row1['ddc_NW'] . '<br>';
                        switch ($row1['ddc_level']) {
                            case 1:
                                $drugcount[0] = ($drugcount[0] + $row1['ddc_NW']);
                                $drugrw[0] = ($drugrw[0] + $row1['ddc_RW']);
                                $drugpnw[0] = ($drugpnw[0] + $row1['ddc_PNW']);
                                break;
                            case 2:
                                $drugcount[1] = ($drugcount[1] + $row1['ddc_NW']);
                                $drugrw[1] = ($drugrw[1] + $row1['ddc_RW']);
                                $drugpnw[1] = ($drugpnw[1] + $row1['ddc_PNW']);
                                break;
                            case 3:
                                $drugcount[2] = ($drugcount[2] + $row1['ddc_NW']);
                                $drugrw[2] = ($drugrw[2] + $row1['ddc_RW']);
                                $drugpnw[2] = ($drugpnw[2] + $row1['ddc_PNW']);
                                break;
                            case 4:
                                $drugcount[3] = ($drugcount[3] + $row1['ddc_NW']);
                                $drugrw[3] = ($drugrw[3] + $row1['ddc_RW']);
                                $drugpnw[3] = ($drugpnw[3] + $row1['ddc_PNW']);
                                break;
                        }
                    }
            }
            if($drugcount[0]!=NULL) $drugcount[0] = '第一級:'.$drugcount[0]; 
            if($drugcount[1]!=NULL) $drugcount[1] = '<br>第二級:'.$drugcount[1]; 
            if($drugcount[2]!=NULL) $drugcount[2] = '<br>第三級:'.$drugcount[2]; 
            if($drugcount[3]!=NULL) $drugcount[3] = '<br>第四級:'.$drugcount[3]; 
            if($drugrw[0]!=NULL) $drugrw[0] = '第一級:'.$drugrw[0]; 
            if($drugrw[1]!=NULL) $drugrw[1] = '<br>第二級:'.$drugrw[1]; 
            if($drugrw[2]!=NULL) $drugrw[2] = '<br>第三級:'.$drugrw[2]; 
            if($drugrw[3]!=NULL) $drugrw[3] = '<br>第四級:'.$drugrw[3]; 
            if($drugpnw[0]!=NULL) $drugpnw[0] = '第一級:'.$drugpnw[0]; 
            if($drugpnw[1]!=NULL) $drugpnw[1] = '<br>第二級:'.$drugpnw[1]; 
            if($drugpnw[2]!=NULL) $drugpnw[2] = '<br>第三級:'.$drugpnw[2]; 
            if($drugpnw[3]!=NULL) $drugpnw[3] = '<br>第四級:'.$drugpnw[3]; 
            $address = $row['r_county'].$row['r_district'].$row['r_zipcode'].$row['r_address'];
            $data[] = array(
                    "s_num"=>$row['s_num'],
                    "s_ic"=>$row['s_ic'],
                    "s_name"=>$row['s_name'],
                    "s_reward_status"=>$row['s_reward_status'],
                    "s_reward_fine"=>$row['s_reward_fine'],
                    "s_date"=> $this->tranfer2RCyearTrad($row['s_date']),
                    "s_cnum"=>$row['s_cnum'],
                    "s_CMethods"=>$row['s_CMethods'],
                    "s_reward_rej"=>$row['s_reward_rej'],
                    "address"=>$address,
                    "s_office"=>$row['s_office'],
                    "ddc_ingredient"=>$drug2val,
                    "drug2nw"=>$drugcount[0].$drugcount[1].$drugcount[2].$drugcount[3],
                    "drug2rw"=>$drugrw[0].$drugrw[1].$drugrw[2].$drugrw[3],
                    "drug2pnw"=>$drugpnw[0].$drugpnw[1].$drugpnw[2].$drugpnw[3],

                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }
    
    function table_project_reward($id,$id2) {//裁罰 列表
        $this->load->library('table');
        $query = $this->getsqlmod->get3RewardProject($id,$id2)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','初驗淨重','淨重','建議獎金','退回次數');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $drug = $this->getsqlmod->get3Drugtest($susp->s_ic,$susp->s_cnum)->result() ; // 抓sic
            $drug_doc = $this->getsqlmod->get1DrugWithIC($susp->s_ic)->result(); // 抓scnum
            //$query2 = $this->getsqlmod->get2DrugCk('ddc_drug',$drug_doc[0]->e_id); 
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            //$table_row[] = anchor('cases/editCases2_Reward/' . $susp->s_cnum, $susp->s_ic);//
            $table_row[] = $susp->s_ic;//
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad($susp->s_date);
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            /*if($drug_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }*/
            $drug2val=Null;
            $drug2nw=Null;
            $drug2count=NULL;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $drug2val = Null;
                    $drug2nw = Null;
                }
                else{
                    $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
                    $drug2nw = $drug2nw . $drug2->ddc_NW . '<br>';
                    switch ($drug2->ddc_level) {
                        case 1:
                            $drugcount[0] = $drugcount[0] + $drug2->ddc_NW;
                            break;
                        case 2:
                            $drugcount[1] = $drugcount[1] + $drug2->ddc_NW;
                            break;
                        case 3:
                            $drugcount[2] = $drugcount[2] + $drug2->ddc_NW;
                            break;
                        case 4:
                            $drugcount[3] = $drugcount[3] + $drug2->ddc_NW;
                            break;
                        case 5:
                            $drugcount[4] = $drugcount[4] + $drug2->ddc_NW;
                            break;
                    }
                }
            }
            $drugreward = 0;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $$drugreward = 0;
                }
                else{
                    switch ($drug2->ddc_level) {
                        case 1:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 50) $drugreward = $$drugreward + 2500;
                            else if($drug2->ddc_NW >= 50) $drugreward = $drugreward + 5000;
                            break;
                        case 2:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 100) $drugreward = $drugreward + 2000;
                            else if($drug2->ddc_NW >= 100 && $drug2->ddc_NW < 250) $drugreward = $drugreward + 2500;
                            else if($drug2->ddc_NW >= 250 ) $drugreward = $drugreward + 5000;
                            break;
                        case 3:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                        case 4:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                    }
                }
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2nw;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                if($drugcount[0]>0){
                    $drug2count = '1級 = ' . $drugcount[0] . '<br>';
                }
                if($drugcount[1]>0){
                    $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
                }
                if($drugcount[2]>0){
                    $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
                }
                if($drugcount[3]>0){
                    $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
                }
                if($drugcount[4]>0){
                    $drug2count = $drug2count . '5級 = ' . $drugcount[4];
                }
                $table_row[] = $drug2count;
                //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
            }
            if($susp->s_reward_fine == null || $susp->s_reward_fine == 0){
                $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$drugreward.'"></input>';
            }
            else{
                $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$susp->s_reward_fine.'"></input>';
            }
            $table_row[] = $susp->s_reward_rej;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    
    function table_rewardProject1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getrewardproject_table1($id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '專案編號', '申請狀態');
        $table_row = array();
        foreach ($query->result() as $rp)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases/listrp2/' . $rp->rp_name, $rp->rp_name);
            if($rp->rp_status==null){
                $table_row[] = '申請中';
            }
            else{
                $table_row[] = $rp->rp_status;
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_project_reward2($id,$id2) {//裁罰 列表
        $this->load->library('table');
        $query = $this->getsqlmod->get3RewardProject2($id,$id2)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','初驗淨重','淨重','退回次數');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $drug = $this->getsqlmod->get3Drugtest($susp->s_ic,$susp->s_cnum)->result() ; // 抓sic
            $drug_doc = $this->getsqlmod->get1DrugWithIC($susp->s_ic)->result(); // 抓scnum
            //$query2 = $this->getsqlmod->get2DrugCk('ddc_drug',$drug_doc[0]->e_id); 
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            $table_row[] = anchor('cases2/editCases2_Reward/' . $susp->s_cnum, $susp->s_ic);//
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            /*if($drug_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }*/
            $drug2val=Null;
            $drug2nw=Null;
            $drug2count=NULL;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $drug2val = Null;
                    $drug2nw = Null;
                }
                else{
                    $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
                    $drug2nw = $drug2nw . $drug2->ddc_NW . '<br>';
                    switch ($drug2->ddc_level) {
                        case 1:
                            $drugcount[0] = $drugcount[0] + $drug2->ddc_NW;
                            break;
                        case 2:
                            $drugcount[1] = $drugcount[1] + $drug2->ddc_NW;
                            break;
                        case 3:
                            $drugcount[2] = $drugcount[2] + $drug2->ddc_NW;
                            break;
                        case 4:
                            $drugcount[3] = $drugcount[3] + $drug2->ddc_NW;
                            break;
                        case 5:
                            $drugcount[4] = $drugcount[4] + $drug2->ddc_NW;
                            break;
                    }
                }
            }
            $drugreward = 0;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $$drugreward = 0;
                }
                else{
                    switch ($drug2->ddc_level) {
                        case 1:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 50) $drugreward = $$drugreward + 2500;
                            else if($drug2->ddc_NW >= 50) $drugreward = $drugreward + 5000;
                            break;
                        case 2:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 100) $drugreward = $drugreward + 2000;
                            else if($drug2->ddc_NW >= 100 && $drug2->ddc_NW < 250) $drugreward = $drugreward + 2500;
                            else if($drug2->ddc_NW >= 250 ) $drugreward = $drugreward + 5000;
                            break;
                        case 3:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                        case 4:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                    }
                }
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2nw;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                if($drugcount[0]>0){
                    $drug2count = '1級 = ' . $drugcount[0] . '<br>';
                }
                if($drugcount[1]>0){
                    $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
                }
                if($drugcount[2]>0){
                    $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
                }
                if($drugcount[3]>0){
                    $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
                }
                if($drugcount[4]>0){
                    $drug2count = $drug2count . '5級 = ' . $drugcount[4];
                }
                $table_row[] = $drug2count;
                //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
            }
            $table_row[] = $susp->s_reward_rej;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
        
    public function listrp2() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $rp = $this->getsqlmod->getRP2($id)->result();
        $test_table = $this->table_project_reward2($table,$id);
        $data['s_table'] = $test_table;
        $data['title'] = "獎金專案:".$rp[0]->rp_name;
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases/3RPlist';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    

    public function admin() {//管理權限
        $this->load->helper('form');
        $this->load->library('table');
        $this->load->model('getsqlmod');
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '賬號', '姓名', '分局','職稱','單位','權限','驗證碼');
        $data['s_table'] = $this->table->generate();
        $data['title'] = "管理權限獲取驗證碼";
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/IsAdmin';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $this->load->view('template', $data);
    }
    
    function testjson() { //帳號
        include 'config.php';

        $query = $this -> session -> userdata('uoffice');
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (em_mailId like '%".$searchValue."%' or 
                em_lastname like '%".$searchValue."%' or 
                em_centername like '%".$searchValue."%' or 
                em_firstname like '%".$searchValue."%' or 
                em_officeAll like '%".$searchValue."%' or 
                em_office like '%".$searchValue."%' or 
                em_roffice like'%".$searchValue."%' ) ";
        }

        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees WHERE em_office = '".$query."'");//.$searchQuery);
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from employees WHERE em_office = '".$query."'".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            if($row['em_IsAdmin'] == '1') $link = anchor('login/changead/1/' . $row['em_mailId'], '更改為一般使用者');
            if($row['em_IsAdmin'] == '0') $link = anchor('login/changead/0/' . $row['em_mailId'], '更改為管理員');
            if($row['em_IsAdmin'] == '2') $link = '特殊管理者(僅警局承辦人可更改)';
            $data[] = array(
                    "em_lastname"=>$row['em_lastname'].$row['em_centername'].$row['em_firstname'],
                    "em_officeAll"=>$row['em_officeAll'],
                    "em_office"=>$row['em_office'],
                    "em_job"=>$row['em_job'],
                    "em_IsAdmin"=> $link,
                    //"em_IsAdmin"=> anchor('cases/edit/' . $cases->c_num, $cases->c_num),
                    "em_change_code"=>$row['em_change_code'],
                    "em_mailId"=>$row['em_mailId'],
                    "em_roffice"=>$row['em_roffice']
                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }

    public function rewardstate() {//查詢獎金狀態
        $this->load->helper('form');
        $this->load->library('table');
        $this->load->model('getsqlmod');
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '案件編號', '犯嫌人姓名', '身份證','查獲時間','獎金金額','申請狀態');
        $data['s_table'] = $this->table->generate();
        $data['title'] = "查詢獎金狀態";
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/rewardstate';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $this->load->view('template', $data);
    }
    
    function rewardjson() { //獎金狀態json
        include 'config.php';

        $query = $this -> session -> userdata('email');
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Date search value
        $searchByFromdate = mysqli_real_escape_string($con,$_POST['searchByFromdate']);
        $searchByTodate = mysqli_real_escape_string($con,$_POST['searchByTodate']);

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (s_ic like '%".$searchValue."%' or 
                s_name like '%".$searchValue."%' or 
                s_reward_status like '%".$searchValue."%' or 
                s_reward_fine like '%".$searchValue."%' or 
                s_cnum like'%".$searchValue."%' ) ";
        }

        ## Date filter
        if($searchByFromdate != '' && $searchByTodate != ''){
            $searchQuery .= " and (s_date between '".$searchByFromdate."' and '".$searchByTodate."' ) ";
        }
        
        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum AND suspect.s_reward_project IS NULL AND suspect.s_reward_project2 IS NULL  AND suspect.s_reward_project3 IS NULL AND e_ed_empno = '".$query."'".$searchQuery);
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum AND suspect.s_reward_project IS NULL AND suspect.s_reward_project2 IS NULL  AND suspect.s_reward_project3 IS NULL AND e_ed_empno = '".$query."'".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            $data[] = array(
                    "s_ic"=>$row['s_ic'],
                    "s_name"=>$row['s_name'],
                    "s_reward_status"=>$row['s_reward_status'],
                    "s_reward_fine"=>$row['s_reward_fine'],
                    "s_date"=>$row['s_date'],
                    "s_cnum"=>$row['s_cnum'],
                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }

    function adtorc($date) {
        $taiwan_date_string = explode("-", $date->format('Y-m-d'));
        return intval($taiwan_date_string[0]).'-'.$taiwan_date_string[1].'-'.$taiwan_date_string[2];  
    }

    function rctoad($date) {
        if(strlen($date)==7){
            $year=intval(substr($date, 0, 3))+1911;
            $month=substr($date, 3, 2);
            $day=substr($date, 5, 2);
            return $year.'-'.$month.'-'.$day;  
        }else if (strlen($date)==6){
            $year=intval(substr($date, 0, 2))+1911;
            $month=substr($date, 2, 2);
            $day=substr($date, 4, 2);
            return $year.'-'.$month.'-'.$day;  
        }
    }
	/** 轉西元年 */
    public function tranfer2ADyear($date)
    {
        if(strlen($date) == 6)
        {
            $ad = ((int)substr($date, 0, 2)) + 1911;
            return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
        }
        elseif(strlen($date) == 7)
        {
            $ad = ((int)substr($date, 0, 3)) + 1911;
            return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
        }
        else
        {
            return '';
        }
    }
	/** 轉民國年 中文年-月-日 */
	function tranfer2RCyearTrad($date)
	{
		$date = str_replace('-', '', $date);
		$rc = ((int)substr($date, 0, 4)) - 1911;
		return (string)$rc . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2) ;
	}
	/** 轉民國年 中文年-月-日 時:分:秒 */
	function tranfer2RCyearTrad2($datetime)
	{
		$date = substr($datetime, 0, 10);
		$date = str_replace('-', '', $date);
		$rc = ((int)substr($date, 0, 4)) - 1911;
		return (string)$rc . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2)  . ' ' . substr($datetime, 11, 19) ;
	}
}
