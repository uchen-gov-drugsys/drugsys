<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Acc_cert extends CI_Controller {//帳務憑證
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }


    public function index() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        //$rp = $this->getsqlmod->getRP1($id)->result();
        $this->load->library('table');
        ///$db = new mysqli("localhost","root","test123","db");
        $name="";
        $ic="";
        $BVC="";
        $cnum="";
        $s_go="";
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
            $tj = " s_name like '%{$name}%'";
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        $dateRanges[0]=date("Y-m-d");
        $dateRanges[1]=date("Y-m-d");
        if(!empty($_POST['cnum']))
        {
            $cnum = $_POST['cnum'];
        }
        if(!empty($_POST['BVC']))
        {
            $BVC = $_POST['BVC'];
        }
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        if(!empty($_POST['datepicker'])){
            $dateRanges = explode(' - ', $_POST['datepicker']);
            $data['a']= $dateRanges[0];
            $data['b']= $dateRanges[1];
        }
        $query = $this->getsqlmod->getfine_search($name,$ic,$BVC,$cnum,$s_go)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號', '犯嫌人姓名', '身份證','年度繳納金額','完納金額','分期資訊','移送案號','分局','憑證核發日期','憑證編號','註銷保留款','虛擬賬號');
        $table_row = array();
        foreach ($query as $susp)
        {
            $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            $ft = $this->getsqlmod->getfine_trafwithsnum($susp->f_snum)->result(); 
            $ft_no="";
            if(isset($ft[0]->ft_no))$ft_no = $ft[0]->ft_no;
            $partpay=null;
            foreach($fpart as $fp){
                if(isset($fp)){
                    $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
                }else{
                    $partpay = null;   
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            if(isset($susp->surc_no)&&$susp->f_samount<=0){
                $table_row[] = $susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no;
            }else   $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->s_ic;
            $table_row[] = $susp->f_damount+$susp->f_samount;
            $table_row[] = ($susp->f_damount+$susp->f_samount)-$susp->f_payamount;
            if($partpay == null){
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else{
                $table_row[] = $partpay;
            }
            $table_row[] = $ft_no;
            $table_row[] = $susp->s_roffice;
            if($susp->f_certno == null){
                $table_row[] = '<strong>未輸入憑證</strong>';
            }
            else{
                $table_row[] = $susp->f_certno;
            }
            if($susp->f_certDate == null){
                $table_row[] = '<strong>未輸入憑證日期</strong>';
            }
            else{
                $table_row[] = $susp->f_certDate;
            }
            if($susp->f_cancelAmount == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->f_cancelAmount;
            }
            if($susp->f_BVC == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->f_BVC;
            }
            //$table_row[] = '<input type="checkbox" name="a">';
            $this->table->add_row($table_row);
        }   
        $data['title'] = "每日帳作業";
        $data['s_table'] = $this->table->generate();
        $data['user'] = $this -> session -> userdata('uic');
        $rp=$this->getsqlmod->getfineproject()->result();
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/3RPlistALL';
        else $data['include'] = 'acc_cert_query/3RPlistALL';
        foreach ($rp as $rp1){
            $projectoption[$rp1->fp_no] = $rp1->fp_no.$rp1->fp_paytype.(($rp1->fp_paytype != "支匯票")?$rp1->fp_type:'');
        }
        if(isset($projectoption))$data['opt'] = $projectoption;
        else $data['opt'] = null;
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    public function indexFineCProject() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        //$rp = $this->getsqlmod->getRP1($id)->result();
        $this->load->library('table');
        ///$db = new mysqli("localhost","root","test123","db");
        $name="";
        $ic="";
        $BVC="";
        $cnum="";
        $s_go="";
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
            $tj = " s_name like '%{$name}%'";
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        $dateRanges[0]=date("Y-m-d");
        $dateRanges[1]=date("Y-m-d");
        if(!empty($_POST['cnum']))
        {
            $cnum = $_POST['cnum'];
        }
        if(!empty($_POST['BVC']))
        {
            $BVC = $_POST['BVC'];
        }
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        if(!empty($_POST['datepicker'])){
            $dateRanges = explode(' - ', $_POST['datepicker']);
            $data['a']= $dateRanges[0];
            $data['b']= $dateRanges[1];
        }
        $query = $this->getsqlmod->getfine_search($name,$ic,$BVC,$cnum,$s_go)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號', '犯嫌人姓名', '身份證','年度繳納金額','完納金額','分期資訊','移送案號','分局','憑證核發日期','憑證編號','註銷保留款','虛擬賬號');
        $table_row = array();
        foreach ($query as $susp)
        {
            $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            $ft = $this->getsqlmod->getfine_trafwithsnum($susp->f_snum)->result(); 
            $ft_no="";
            if(isset($ft[0]->ft_no))$ft_no = $ft[0]->ft_no;
            $partpay=null;
            foreach($fpart as $fp){
                if(isset($fp)){
                    $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
                }else{
                    $partpay = null;   
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            if(isset($susp->surc_no)&&$susp->f_samount<=0){
                $table_row[] = $susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no;
            }else   $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->s_ic;
            $table_row[] = $susp->f_damount+$susp->f_samount;
            $table_row[] = ($susp->f_damount+$susp->f_samount)-$susp->f_payamount;
            if($partpay == null){
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else{
                $table_row[] = $partpay;
            }
            $table_row[] = $ft_no;
            $table_row[] = $susp->s_roffice;
            if($susp->f_certno == null){
                $table_row[] = '<strong>未輸入憑證</strong>';
            }
            else{
                $table_row[] = $susp->f_certno;
            }
            if($susp->f_certDate == null){
                $table_row[] = '<strong>未輸入憑證日期</strong>';
            }
            else{
                $table_row[] = $susp->f_certDate;
            }
            if($susp->f_cancelAmount == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->f_cancelAmount;
            }
            if($susp->f_BVC == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->f_BVC;
            }
            //$table_row[] = '<input type="checkbox" name="a">';
            $this->table->add_row($table_row);
        }   
        
        $data['title'] = "轉正作業";
        $data['s_table'] = $this->table->generate();
        $data['user'] = $this -> session -> userdata('uic');
        $rp=$this->getsqlmod->getfineCproject()->result();
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/3RPlistCPALL';
        else $data['include'] = 'acc_cert_query/3RPlistCPALL';
        foreach ($rp as $rp1){
            $projectoption[$rp1->fcp_no] = $rp1->fcp_no.$rp1->fcp_type;
        }
        if(isset($projectoption))$data['opt'] = $projectoption;
        else $data['opt'] = null;
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function addAcc_Project(){
        //var_dump($_POST);
        $this->load->helper('form');
        $data['title'] = "案件處理系統:待裁罰列表";
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $now = date('md'); 
        $now2 = date('Y-m-d');
        $project_name = ((isset($_POST['project_name']))?$_POST['project_name']:date('Y-m-d'));
        if($_POST['project_pay_type']=='金融'&&$_POST['project_type']=='罰鍰')
            $type ='1';
        if($_POST['project_pay_type']=='金融'&&$_POST['project_type']=='怠金')
            $type ='2';
        if($_POST['project_pay_type']=='支匯票')
            $type ='3';

        $taiwan_date = date('Y')-1911; 
        //$fp_num = $taiwan_date.$now.$type;
        $fp_num = $this->tranfer2RCyear($project_name) .$type;
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        
        
       
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $i = (count($this->getsqlmod->getFineProject_maxsort($fp_num)->result()) > 0)?((int)$this->getsqlmod->getFineProject_maxsort($fp_num)->result()[0]->sort_no + 1):1;
                $data = array(
                    // 'f_project_num' => $fp_num,
                    'sort_no' => $i,
                    'f_project_id' => $fp_num,
                    'fpart_fpnum' => $value
                );
                // $this->getsqlmod->updateFine($value,$fine_rec);
                $this->getsqlmod->add_finepoject_body_data($data);
            }
            $fp = array(
                    'fp_no' => $fp_num,
                    'fp_createdate' => $project_name,
                    'fp_empno' => $user,
                    'fp_type' => $_POST['project_type'],
                    'fp_paytype' => $_POST['project_pay_type'],
                    //'fp_status' => '審核中',
            );
            if($_POST['s_cnum'] != NULL && $this->getsqlmod->check_fine_project_exist($fp_num) == 0)
                $this->getsqlmod->addfp1($fp);

            echo 'acc_cert/listFineProject';
            // redirect('acc_cert/listFineProject/'); 
        }
        else if($_POST['s_status']=='0'){
            
            foreach ($s_cnum as $key => $value) {
                $i = (count($this->getsqlmod->getFineProject_maxsort($_POST['fp_num'])->result()) > 0)?((int)$this->getsqlmod->getFineProject_maxsort($_POST['fp_num'])->result()[0]->sort_no + 1):1;
                $data = array(
                    'sort_no' => $i,
                    'f_project_id' => $_POST['fp_num'],
                    'fpart_fpnum' => $value
                );
                $this->getsqlmod->add_finepoject_body_data($data);
            }
            //var_dump($_POST);
            echo 'acc_cert/listFineProject';
            // redirect('acc_cert/listFineProject'); 
        }
    }
	public function addAcc_Empty_Project(){
		$user = $this -> session -> userdata('uic');
		$project_name = ((isset($_POST['project_name']))?$_POST['project_name']:date('Y-m-d'));
        if($_POST['project_pay_type']=='金融'&&$_POST['project_type']=='罰鍰')
            $type ='1';
        if($_POST['project_pay_type']=='金融'&&$_POST['project_type']=='怠金')
            $type ='2';
        if($_POST['project_pay_type']=='支匯票')
            $type ='3';

        $taiwan_date = date('Y')-1911; 
        //$fp_num = $taiwan_date.$now.$type;
        $fp_num = $this->tranfer2RCyear($project_name) .$type;

		$fp = array(
				'fp_no' => $fp_num,
				'fp_createdate' => $project_name,
				'fp_empno' => $user,
				'fp_type' => $_POST['project_type'],
				'fp_paytype' => $_POST['project_pay_type'],
				//'fp_status' => '審核中',
		);

		if($this->getsqlmod->check_fine_project_exist($fp_num) == 0)
			$this->getsqlmod->addfp1($fp);
		
	}
    public function update_Fine_abnormal(){
        //var_dump($_POST);
        // $this->load->helper('form');
        if(isset($_POST['s_cnum']))
        {
            $s_cnum =mb_split(",",$_POST['s_cnum']);
            foreach ($s_cnum as $key => $value) {

                $fine_rec = array(
                    'f_abmormal' => '1'
                );
                // $this->getsqlmod->updateFine($value,$fine_rec);
                $this->getsqlmod->update_fine_abnormal($value,$fine_rec);
            }

            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        
    }
    
    public function listFineProject() {
        $this->load->helper('form');
        $this->load->library('table');
        /** 新專案 */
        $query = $this->getsqlmod->getFineproject(); 
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號', '專案類型','專案建立時間','憑證用紙','便簽','每日帳清冊/收據報表','分期收繳登記','公文函','動作');
        $table_row = array();
        foreach ($query->result() as $fp)
        {
			$hasedit = $this->getsqlmod->checkFineProject_edit($fp->fp_no);
            $table_row = NULL;
            $table_row[] = $fp->fp_num;
            if(isset($fp->fp_office_num) && !empty($fp->fp_office_num))
            {
                if($fp->fp_paytype!='支匯票')
                    $str = '<br/><small>公文號：'.$fp->fp_office_num.' <a href="#" data-toggle="modal" data-target="#documentModal" onclick="modalEvent('. $fp->fp_no .',\''.$fp->fp_office_num.'\')"><span class="fa fa-pencil"></span></a></small>';
                else
                    $str = '';
            }
            else
            {
                $str = '<br/><span class="label label-success"'.(($fp->fp_paytype=='支匯票')?'data-id="'.$fp->fp_no.'"':'  data-toggle="modal" data-target="#documentModal" data-whatever="'. $fp->fp_no).'" onclick="'.(($fp->fp_paytype=='支匯票')?'saveEvent($(this), -1)">':'modalEvent('. $fp->fp_no .')">').(($fp->fp_paytype=='支匯票')?'進行審核':'新增公文號').'</span>';
            }
            
            if($fp->fp_type=='罰鍰'&&$fp->fp_paytype=="金融")$table_row[] = anchor('Acc_cert/listfp1/' . $fp->fp_no, $fp->fp_no).$str;
            if($fp->fp_type=='怠金'&&$fp->fp_paytype=="金融")$table_row[] = anchor('Acc_cert/listfp1/' . $fp->fp_no, $fp->fp_no).$str;
            if($fp->fp_paytype=='支匯票')$table_row[] = anchor('Acc_cert/listfp2/' . $fp->fp_no, $fp->fp_no).$str;


            if($fp->fp_paytype=='金融'){
                if($fp->fp_type == '罰鍰')
                    $table_row[] = '<span class="label label-danger">'.$fp->fp_paytype.$fp->fp_type.'</span>';
                else
                    $table_row[] = '<span class="label label-warning">'.$fp->fp_paytype.$fp->fp_type.'</span>';
            }
            else{
                $table_row[] = '<span class="label label-info">'.$fp->fp_paytype.'</span>';
            }
            $table_row[] =  $this->tranfer2RCyear2($fp->fp_createdate) . (($hasedit == 0)?"<br><strong>尚未編輯</strong>":'');
            
			if($fp->fp_paytype=='支匯票' && $hasedit > 0)
			{
				// $table_row[] =  anchor('PDFcreate/fineproject_card_print/' . $fp->fp_no,'憑證');
				// $table_row[] =  anchor('Acc_cert/downloadmemoxml/' . $fp->fp_no,'便簽di');
				// $table_row[] =  anchor('Acc_cert/exportCSV/' . $fp->fp_no,'罰鍰清冊');
				$table_row[] = [];
				$table_row[] = [];
				$table_row[] = anchor('PhpspreadsheetController/export_finePJ_lists_receipt/' . $fp->fp_no,'收據報表'). ' ('.anchor('PDFcreate/export_finePJ_lists_receipt/'. $fp->fp_no,'PDF <i class="fa fa-download" ').')';;
				// $table_row[] =  anchor('Acc_cert/exportCSVAll/' . $fp->fp_no,'分期收繳登記');
				$table_row[] =  anchor('PhpspreadsheetController/export_finePJ_lists_by_indiv/' . $fp->fp_no,'收繳登記'). ' ('.anchor('PDFcreate/export_finePJ_lists_by_indiv/'. $fp->fp_no,'PDF <i class="fa fa-download" ').')';

				$table_row[] =  '<button type="button" class="btn-link" onclick="checkhasdone(\''.base_url('Acc_cert/downloadticketofficexml/' . $fp->fp_no).'\')" >公文函di</button>';
				// anchor('#','公文函di', 'onclick="checkhasdone(\''.base_url('Acc_cert/downloadticketofficexml/' . $fp->fp_no).'\')" ');
			}
			elseif ($fp->fp_paytype=='金融' && $hasedit > 0) 
			{
				$table_row[] =  anchor('PDFcreate/fineproject_card_print/' . $fp->fp_no,'憑證');
				$table_row[] =  anchor('Acc_cert/downloadmemoxml/' . $fp->fp_no,'便簽di');
				// $table_row[] =  anchor('Acc_cert/exportCSV/' . $fp->fp_no,'罰鍰清冊');
				$table_row[] = anchor('PhpspreadsheetController/export_finePJ_lists/'. $fp->fp_no,$fp->fp_type.'清冊') . ' ('.anchor('PDFcreate/export_finePJ_lists/'. $fp->fp_no,'PDF <i class="fa fa-download" ').')';
				// $table_row[] =  anchor('Acc_cert/exportCSVAll/' . $fp->fp_no,'分期收繳登記');
				$table_row[] =  anchor('PhpspreadsheetController/export_finePJ_lists_by_indiv/' . $fp->fp_no,'收繳登記') . ' ('.anchor('PDFcreate/export_finePJ_lists_by_indiv/'. $fp->fp_no,'PDF <i class="fa fa-download" ').')';

				if(isset($fp->fp_office_num) && !empty($fp->fp_office_num))
				{						
					$table_row[] =  '<button type="button" class="btn-link" onclick="checkhasdone(\''.base_url('Acc_cert/downloadofficexml/' . $fp->fp_no).'\')" >公文函di</button>';
					// anchor('#','公文函di', 'onclick="checkhasdone(\''.base_url('Acc_cert/downloadofficexml/' . $fp->fp_no).'\')" ');
				}
				else
				{
					$table_row[] = [];
				}
			}
			else
			{
				$table_row[] = [];
				$table_row[] = [];
				$table_row[] = [];
				$table_row[] = [];
				$table_row[] = [];
			}
            
            //$table_row[] =  anchor('Acc_cert/paper/' . $fp->fp_no,'憑證用紙');
            
            if(preg_match("/d/i", $this->session-> userdata('3permit')) && isset($fp->fp_office_num) && !empty($fp->fp_office_num))
            {
                $table_row[] =  anchor('Acc_cert/complete/' . $fp->fp_no,'進行簽核', ' class="btn btn-warning" ');
				// "<a href='".base_url()."Acc_cert/complete/".$fp->fp_no."' class='btn btn-warning'>進行簽核</a>";
                // anchor('Acc_cert/complete/' . $fp->fp_no,'簽核完成');
            }            
            else 
            {
                $table_row[] =  '';
            }
                
            $this->table->add_row($table_row);
        }   
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table->generate();
        $data['s_table'] = $test_table;

        /** 舊專案 */
        $query2 = $this->getsqlmod->getfineproject_done(); 
        //var_dump($query->result());
        $tmpl2 = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table2">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl2);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '專案編號', '專案類型','專案建立時間','憑證用紙','便簽','每日帳清冊/收據報表','分期收繳登記','公文函','');
        $table_row2 = array();
        foreach ($query2->result() as $fp)
        {
            $table_row2 = NULL;
            if(isset($fp->fp_office_num) && !empty($fp->fp_office_num))
            {
                $str = '<br/><small>'.(($fp->fp_office_num == -1 || $fp->fp_office_num == '-1')?'':'公文號：'.$fp->fp_office_num).'</small>';
            }
            else
            {
                $str = '<br/><span class="label label-success" data-toggle="modal" data-target="#documentModal" data-whatever="'. $fp->fp_no .'">新增公文號</span>';
            }
            
            if($fp->fp_type=='罰鍰'&&$fp->fp_paytype=="金融")$table_row2[] = anchor('Acc_cert/listfp1/' . $fp->fp_no, $fp->fp_no).$str;
            if($fp->fp_type=='怠金'&&$fp->fp_paytype=="金融")$table_row2[] = anchor('Acc_cert/listfp1/' . $fp->fp_no, $fp->fp_no).$str;
            if($fp->fp_paytype=='支匯票')$table_row2[] = anchor('Acc_cert/listfp2/' . $fp->fp_no, $fp->fp_no).$str;


            if($fp->fp_paytype=='金融'){
                if($fp->fp_type == '罰鍰')
                    $table_row2[] = '<span class="label label-danger">'.$fp->fp_paytype.$fp->fp_type.'</span>';
                else
                    $table_row2[] = '<span class="label label-warning">'.$fp->fp_paytype.$fp->fp_type.'</span>';
            }
            else{
                $table_row2[] = '<span class="label label-info">'.$fp->fp_paytype.'</span>';
            }

            $table_row2[] =  $this->tranfer2RCyear2($fp->fp_createdate);
            if($fp->fp_paytype=='支匯票')
			{
				// $table_row[] =  anchor('PDFcreate/fineproject_card_print/' . $fp->fp_no,'憑證');
				// $table_row[] =  anchor('Acc_cert/downloadmemoxml/' . $fp->fp_no,'便簽di');
				// $table_row[] =  anchor('Acc_cert/exportCSV/' . $fp->fp_no,'罰鍰清冊');
				$table_row2[] = [];
				$table_row2[] = [];
				$table_row2[] = anchor('PhpspreadsheetController/export_finePJ_lists_receipt/' . $fp->fp_no,'收據報表'). ' ('.anchor('PDFcreate/export_finePJ_lists_receipt/'. $fp->fp_no,'PDF <i class="fa fa-download" ').')';;
				// $table_row[] =  anchor('Acc_cert/exportCSVAll/' . $fp->fp_no,'分期收繳登記');
				$table_row2[] =  anchor('PhpspreadsheetController/export_finePJ_lists_by_indiv/' . $fp->fp_no,'收繳登記'). ' ('.anchor('PDFcreate/export_finePJ_lists_by_indiv/'. $fp->fp_no,'PDF <i class="fa fa-download" ').')';

				// $table_row2[] =  '<button type="button" class="btn-link" onclick="checkhasdone(\''.base_url('Acc_cert/downloadticketofficexml/' . $fp->fp_no).'\')" >公文函di</button>';
				$table_row2[] = anchor('Acc_cert/downloadticketofficexml/' . $fp->fp_no,'公文函di', '');
			}
			elseif ($fp->fp_paytype=='金融') 
			{
				$table_row2[] =  anchor('PDFcreate/fineproject_card_print/' . $fp->fp_no,'憑證');
				$table_row2[] =  anchor('Acc_cert/downloadmemoxml/' . $fp->fp_no,'便簽di');
				// $table_row[] =  anchor('Acc_cert/exportCSV/' . $fp->fp_no,'罰鍰清冊');
				$table_row2[] = anchor('PhpspreadsheetController/export_finePJ_lists/'. $fp->fp_no,$fp->fp_type.'清冊') . ' ('.anchor('PDFcreate/export_finePJ_lists/'. $fp->fp_no,'PDF <i class="fa fa-download" ').')';
				// $table_row[] =  anchor('Acc_cert/exportCSVAll/' . $fp->fp_no,'分期收繳登記');
				$table_row2[] =  anchor('PhpspreadsheetController/export_finePJ_lists_by_indiv/' . $fp->fp_no,'收繳登記') . ' ('.anchor('PDFcreate/export_finePJ_lists_by_indiv/'. $fp->fp_no,'PDF <i class="fa fa-download" ').')';

				if(isset($fp->fp_office_num) && !empty($fp->fp_office_num))
				{						
					// $table_row2[] =  '<button type="button" class="btn-link" onclick="checkhasdone(\''.base_url('Acc_cert/downloadofficexml/' . $fp->fp_no).'\')" >公文函di</button>';
					$table_row2[] = anchor('Acc_cert/downloadofficexml/' . $fp->fp_no,'公文函di', '');
				}
				else
				{
					$table_row2[] = [];
				}
			}
			else
			{
				$table_row2[] = [];
				$table_row2[] = [];
				$table_row2[] = [];
				$table_row2[] = [];
				$table_row2[] = [];
			}
            $table_row2[] =  $fp->fp_status;
                
            $this->table->add_row($table_row2);
        }   
        $table2 = $this->session-> userdata('uoffice');
        $test_table2 = $this->table->generate();
        $data['s_table_done'] = $test_table2;

        $data['title'] = "金融支匯票專案";
        $data['sub_title'] = "金融支匯票專案";
        $data['pj_type'] = 'P';
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/fineproject';
        else $data['include'] = 'acc_cert_query/fineproject';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

    public function delete_Fine_project(){
        if(isset($_POST['fp_num']))
        {
            if($_POST['pj_type'] == 'CP')
            {
                $this->getsqlmod->delFineCProject($_POST['fp_num']);
            }
            else
            {
                $this->getsqlmod->delFineProject($_POST['fp_num']);
            }
            

            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        
    }

    public function listfp1() {
        // var_dump($_POST);
        // exit;
        
        $this->load->library('table');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $query = $this->getsqlmod->getfine_list($id)->result(); 
        $query = $this->getsqlmod->get_finepoject_body_data($id)->result();
        $fproj = $this->getsqlmod->getfineproject_ed($id)->result(); 
        $sum_payamount = 0;
        $tmpl = array (
            'table_open' => '<table style="width:1800px" border="0" cellpadding="3" cellspacing="0" class="table  table-bordered" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        // $this->table->set_heading( '','處分書編號', '犯嫌人姓名', '身份證','年度繳納金額','完納金額','分期資訊','移送案號','分局','憑證核發日期','憑證編號','註銷保留款','虛擬賬號');
        $this->table->set_heading('','', '類型', '案件編號', '受處分人姓名', '身份證編號', '罰鍰/怠金', '完納日期/退費日期', '完納金額', '已繳金額', '未繳金額', '轉正金額', '退費金額','虛擬帳號', '分期Log', '移送Log', '憑證Log', '撤銷註銷Log', '執行命令Log','');
        $table_row = array();
        
        foreach ($query as $susp)
        {
            // $ft = $this->getsqlmod->getfine_trafwithsnum($susp->f_snum)->result(); 
            // $ft_no="";
            // $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            // $partpay=null;
            // $partpayamount=0;
            // foreach($fpart as $fp){
            //     if(isset($fp)){
            //         $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
            //         $partpayamount = $partpayamount + $fp->fpart_amount;
            //     }else{
            //         $partpay = null;   
            //     }
            // }
            $table_row = NULL;
            // $table_row[] = $susp->f_num;
            
            $table_row[] = $susp->f_no;
			$table_row[] = $susp->fpart_num;
            
            $table_row[] = (($susp->type === 'A' )?'<span class="label label-danger">罰鍰</span>':'<span class="label label-warning">怠金</span>');
            // if(isset($susp->surc_no)&&$susp->f_samount<=0){
            //     $table_row[] = $susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no;
            // }else   $table_row[] = $susp->fd_num;
            $table_row[] = $susp->f_caseid . ((isset($susp->f_dellog) && !empty($susp->f_dellog))?'<span class="text-danger"> (註)</span>':'');

            // $table_row[] = $susp->s_name;

            $table_row[] = $susp->f_username. ((isset($susp->f_turnsub_log) && !empty($susp->f_turnsub_log))?'<br/><small class="text-warning">自'.explode('_',$susp->f_turnsub_log)[2].'轉正</small>':'');

            // $table_row[] = $susp->s_ic;
            $table_row[] = $susp->f_userid;

            // $table_row[] = $susp->f_damount+$susp->f_samount;
            $table_row[] = (($susp->type === 'A')?(int)$susp->f_amount * 10000:$susp->f_amount);

            $table_row[] = ($susp->f_donedate == '0000-00-00')?'':$this->tranfer2RCyear2($susp->f_donedate);

            $table_row[] = $susp->f_doneamount;

            $temp_data = array(
                "fpart_fpnum" => ((isset($susp->f_no))?$susp->f_no:0),
                "fp_createdate"=>$fproj[0]->fp_createdate,
                "f_project_id"=>$id
            );
            
            // $temp_paymoney = ((isset($susp->f_paymoney))?$susp->f_paymoney:$this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount);
			$temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
            $temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

            $table_row[] = (($temp_paymoney < 0)?0:($temp_paymoney+$temp_turnsub));

            $table_row[] = ((((($susp->type === 'A')?(int)$susp->f_amount * 10000:$susp->f_amount) - (int)$susp->f_doneamount - (int)$temp_paymoney - (int)$temp_turnsub) >= 0)?((($susp->type === 'A')?(int)$susp->f_amount * 10000:$susp->f_amount) - (int)$susp->f_doneamount - (int)$temp_paymoney - (int)$temp_turnsub):0);

            $table_row[] = (($susp->f_turnsub < 0)?(0-$susp->f_turnsub):$susp->f_turnsub);
            $table_row[] = $susp->f_refund;

            $table_row[] = $susp->f_virtualcode;

            // if(empty($susp->f_paylog) || is_null($susp->f_paylog))
            // {
            //     $table_row[] = '<strong>無分期繳款記錄</strong>';
            // }
            // else
            // {
            //     $paylogAry = mb_split("\n",$susp->f_paylog);
            //     $paylogAryStr = '';
            //     foreach ($paylogAry as $value) {
            //         $temp_v = explode('_', $value);
            //         $paylogAry_date = $temp_v[0];
            //         $paylogAry_money = ((isset($temp_v[1]))?$temp_v[1]:$value);
            //         $paylogAryStr .= ($this->tranfer2ADyear((string)$paylogAry_date) . '已繳' . $paylogAry_money . '元' . '<br/>') ;

            //         $temp_data = array(
            //             "fpart_fpnum" => $susp->f_no,
            //             "fpart_amount" => $paylogAry_money,
            //             "fpart_type" => '',
            //             "fpart_date" => $this->tranfer2ADyear($paylogAry_date)
            //         );

            //         // $this->getsqlmod->check_finepayport_data($temp_data);
            //     }
            //     $table_row[]  = $paylogAryStr;
            // }
            $query_body_indv =  $this->getsqlmod->check_finepayport_data($temp_data)->result();
            if(count($query_body_indv) <= 0)
            {
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else
            {
                // $paylogAry = mb_split("\n",$susp->f_paylog);
                $paylogAryStr = '';
                foreach ($query_body_indv as $value) {
                    // $temp_v = explode('_', $value);
                    $paylogAry_date = $value->fpart_date;
                    $paylogAry_money = ((($value->fpart_amount >= 0 )?$value->fpart_amount:0) + $value->f_refund + $value->f_turnsub);
                    $paylogAryStr .= ($this->tranfer2RCyear2($paylogAry_date) . '已繳' . (((int)$paylogAry_money < 0)?('<span class="text-danger">'.$paylogAry_money.'</span>'):$paylogAry_money ) . '元' .'_'. '<span style="border:1px solid #AAA;border-radius:100%;" class="'.((mb_substr($value->fpart_type,0,1) == "金")?'bg-danger':'bg-info').'">'. mb_substr($value->fpart_type,0,1) . '</span><br/>') ;

                    // $temp_data = array(
                    //     "fpart_fpnum" => $susp->f_no,
                    //     "fpart_amount" => $paylogAry_money,
                    //     "fpart_type" => '',
                    //     "fpart_date" => $this->tranfer2ADyear($paylogAry_date)
                    // );

                    // $this->getsqlmod->check_finepayport_data($temp_data);
                }
                $table_row[]  = $paylogAryStr;
            }
			$calcmovelog = explode("\n", $susp->f_movelog);
			usort($calcmovelog, function($a, $b){ 
				$movedateA = explode('_', $a)[0];
				$movedateB = explode('_', $b)[0];
				return ($movedateA < $movedateB)? 1 : -1; 
			}); 
            $table_row[] = join('<br/>', $calcmovelog);
            $table_row[] = str_replace("\n","<br/>", $susp->f_cardlog);
            $table_row[] = str_replace("\n","<br/>", $susp->f_dellog);
            $table_row[] = str_replace("\n","<br/>", $susp->f_execlog);
            $table_row[] = (($susp->sort_no)?$susp->sort_no:1);

            // $sum_payamount = $sum_payamount +($susp->fpart_amount + $susp->f_fee + $susp->f_turnsub + $susp->f_refund);
			$sum_payamount = $sum_payamount + ((($susp->fpart_amount >= 0)?$susp->fpart_amount:0) + $susp->f_fee + (($susp->f_turnsub < 0)?0-$susp->f_turnsub:$susp->f_turnsub)  + $susp->f_refund);
            $sort_no = (($susp->sort_no)?$susp->sort_no:1);
            // if($partpay == null){
            //     $table_row[] = '<strong>無分期繳款記錄</strong>';
            // }
            // else{
            //     $table_row[] = $partpay;
            // }
            // $table_row[] = $ft_no;
            // $table_row[] = $susp->s_roffice;
            // if($susp->f_certno == null){
            //     $table_row[] = '<strong>未輸入憑證</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_certno;
            // }
            // if($susp->f_certDate == null){
            //     $table_row[] = '<strong>未輸入憑證日期</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_certDate;
            // }
            // if($susp->f_cancelAmount == null){
            //     $table_row[] = '<strong>未輸入姓名</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_cancelAmount;
            // }
            // if($susp->f_BVC == null){
            //     $table_row[] = '<strong>未輸入姓名</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_BVC;
            // }
            //$table_row[] = '<input type="checkbox" name="a">';
            $this->table->add_row($table_row);
        }   
        $data['title'] = "帳務專案:".$id;
        $data['fpid'] = $id;
        $data['total'] = "$ " . number_format($sum_payamount); 
        $data['max_sort'] = ((isset($sort_no))?$sort_no:0);
        $data['fp_status'] = $fproj[0]->fp_status;
        if(isset($fproj[0]))
        {
            $data['fp_num'] = $fproj[0]->fp_office_num;
            $data['fp_type'] = $fproj[0]->fp_type;
            $data['fp_paytype'] = $fproj[0]->fp_paytype;
            $data['fp_date'] = $this->tranfer2RCyear2($fproj[0]->fp_createdate);
        }
        else 
        {
            $data['fp_num'] =null;
            $data['fp_type'] = null;
            $data['fp_paytype'] = null;
            $data['fp_date'] = null;
        }
        $data['user'] = $this -> session -> userdata('uic');
        $data['s_table'] = $this->table->generate();
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/fplist';
        else $data['include'] = 'acc_cert_query/fplist';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

    public function get_fine_in_person()
    {
        if(isset($_GET['name']) && !empty($_GET['name']))
        {
            $query = $this->getsqlmod->getFineimport_data_in_person($_GET['name'], $_GET['pjid'])->result(); 
        }
        else
        {
            $query = []; 
        }
        
        
        echo json_encode(array('data' => $query));
    }

    public function get_finepart_in_person()
    {
        if(isset($_POST['f_no']) && !empty($_POST['f_no']) && isset($_POST['pjid']) && !empty($_POST['pjid']))
        {
            $query = $this->getsqlmod->getFinepart_data_in_person($_POST['fpart_num'], $_POST['pjid'])->result(); 
        }
        else
        {
            $query = []; 
        }
        
        
        echo json_encode(array('data' => $query));
    }

    public function add_empty_fine()
    {
        if(isset($_POST['em_f_username']) && isset($_POST['em_f_paymoney']) && isset($_POST['em_f_project_id']))
        {
			$max_sort = (int)$_POST['max_sort'];
            $rowData = array(
                "type" => $_POST['em_type'],
                "f_oldno" => -1,                
                "f_year" => date('Y')-1911,
                "f_caseid" => $_POST['em_f_caseid'],
                "f_username" => $_POST['em_f_username'],
                "f_userid" => '',
                "f_amount" => 0,
                "f_donedate" => '0000-00-00',
                "f_doneamount" => 0,
                "f_paydate" => $this->tranfer2ADyear2($_POST['em_f_paydate']),
                "f_paymoney" => $_POST['em_f_paymoney'],
                "f_virtualcode" => '',
                "f_paylog" => null,
                "f_execlog" => null,
                "f_movelog" => null,
                "f_cardlog" => null,
                "f_dellog" => null,
                "f_comment" => null
            );
			$query = $this->getsqlmod->get_finepoject_body_single_data($_POST['em_f_no']);
			if($query->num_rows() > 0)
			{
				$insert_id = $_POST['em_f_no'];
			}
			else
			{
				$insert_id = $this->getsqlmod->addEmptyFineImport($rowData);
			}
            
            $rowData_part = array(
                'f_project_id' => $_POST['em_f_project_id'],
                'fpart_fpnum' => $insert_id,
				'sort_no' => ($max_sort + 1),
                'fpart_amount' => $_POST['em_f_paymoney'],
                'fpart_date' => $this->tranfer2ADyear2($_POST['em_f_paydate']),
                'f_refund' => $_POST['em_returnamount']
            );
            $this->getsqlmod->add_finepoject_body_data($rowData_part);
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        
    }

	public function fine_empty_filter_caseid()
	{
		if(isset($_GET['caseid']))
        {
            $query = $this->getsqlmod->getFine_empty_filter_caseid($_GET['caseid'])->result(); 
        }
        else
        {
            $query = []; 
        }
        
        
        echo json_encode(array('data' => $query));
	}

    public function add_invaild_receipt()
    {
        if(isset($_POST['em_f_receipt_no']) && isset($_POST['em_f_paymoney']) && isset($_POST['em_f_project_id']))
        {
            $i = (count($this->getsqlmod->getFineProject_maxsort($_POST['em_f_project_id'])->result()) > 0)?((int)$this->getsqlmod->getFineProject_maxsort($_POST['em_f_project_id'])->result()[0]->sort_no + 1):1;

			$rowData = array(
                "type" => 'C',         
                "f_year" => date('Y')-1911,
                "f_amount" => 0,
                "f_donedate" => $this->tranfer2ADyear2($_POST['em_f_paydate']),
                "f_doneamount" => $_POST['em_f_paymoney']
            );
			$insert_id = $this->getsqlmod->addEmptyFineImport($rowData);

            $rowData_part = array(
                'sort_no' => $i,
                'f_project_id' => $_POST['em_f_project_id'],
                'f_receipt_no' => $_POST['em_f_receipt_no'],
                'fpart_amount' => $_POST['em_f_paymoney'],
                'fpart_date' => $this->tranfer2ADyear2($_POST['em_f_paydate']),
                'f_comment' => '作廢',
				'fpart_fpnum' => $insert_id
            );
            $this->getsqlmod->add_finepoject_body_data($rowData_part);
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
    }

    public function addAcc_Project_by_person()
    {
        
        if(isset($_POST['s_cnum']) && !empty($_POST['s_cnum']) && isset($_POST['project_id']))
        {
            $s_cnum =mb_split(",",$_POST['s_cnum']);
            $project_id = $_POST['project_id'];
            $oricaseid = $_POST['caseid'];
            $t_payamount = (int)$_POST['t_payamount'];
            $amountAry = json_decode($_POST['amountAry']);
            $paydate = $this->tranfer2ADyear2($_POST['paydate']);
            $pj_type = $_POST['pj_type'];
            $balance = $t_payamount;
            $max_sort = (int)$_POST['max_sort'];

            for ($i=0; $i < count($s_cnum); $i++) { 
                $this_amount = $amountAry[$i]->amount;
                $this_doneamount = $amountAry[$i]->doneamount;
                $this_payamount = $amountAry[$i]->payamount;

                $this_balance = ($this_amount - $this_doneamount - $this_payamount);

                // 繳納後剩餘...
                $balance = $balance - $this_balance;

                if($amountAry[$i]->type != $pj_type) // 異項轉正(罰->怠 or 怠->鍰)
                {
                    if($balance >= 0)
                    {
                        $data = array(
                            'sort_no' => ($max_sort + 1),
                            'f_project_id' => $project_id,
                            'fpart_fpnum' => $s_cnum[$i],
                            'fpart_amount' => 0,
                            'fpart_date' => $paydate,
                            'f_turnsub' => $this_balance,
                            'f_turnsub_log' =>$this->tranfer2RCyear($paydate) . '_' .$this_balance . '_' . $oricaseid
                        );
                    }
                    else
                    {
                        $data = array(
                            'sort_no' => ($max_sort + 1),
                            'f_project_id' => $project_id,
                            'fpart_fpnum' => $s_cnum[$i],
                            'fpart_amount' => 0,
                            'fpart_date' => $paydate,
                            'f_turnsub' => $this_amount + $balance,
                            'f_turnsub_log' =>$this->tranfer2RCyear($paydate) . '_' . ($this_amount + $balance) . '_' . $oricaseid
                        );
                    }
                }
                else
                {
                    if($balance >= 0)
                    {
                        $data = array(
                            'sort_no' => ($max_sort + 1),
                            'f_project_id' => $project_id,
                            'fpart_fpnum' => $s_cnum[$i],
                            'fpart_amount' => $this_balance,
                            'fpart_date' => $paydate,
                            'f_turnsub' => 0,
                            'f_turnsub_log' =>$this->tranfer2RCyear($paydate) . '_' . $this_balance . '_' . $oricaseid
                        );
                    }
                    else
                    {
                        $data = array(
                            'sort_no' => ($max_sort + 1),
                            'f_project_id' => $project_id,
                            'fpart_fpnum' => $s_cnum[$i],
                            'fpart_amount' => $this_amount + $balance,
                            'fpart_date' => $paydate,
                            'f_turnsub' => 0,
                            'f_turnsub_log' =>$this->tranfer2RCyear($paydate) . '_' .($this_amount + $balance) . '_' . $oricaseid
                        );
                    }
                }
                $query = $this->getsqlmod->add_finepartpay_projectid($data); 

                $pay_log = $this->getsqlmod->getFineimport_data($s_cnum[$i])->result_array();

                if(isset($pay_log[0]['f_paylog']) && !empty($pay_log[0]['f_paylog']))
                {
                    $str = "\n";
                }
                else
                {
                    $str = "";
                }

                if($balance >= 0)
                {
                    $f_donedate = $paydate;
                    $f_doneamount = $this_amount;
                    $f_paydate = '0000-00-00';
                    $f_paymoney = 0;
                    if($balance == 0)
                    {
                        $paylog = "";
                    }
                    else
                    {
                        $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($paydate). '_' . $this_balance;
                    }
                }
                else
                {
                    $f_donedate = '0000-00-00';
                    $f_doneamount = 0;
                    $f_paydate = $paydate;
                    $f_paymoney = $this_amount + $balance;

                    $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($paydate). '_' . ($this_amount + $balance);
                }
                
                $temp_data2 = array(
                    "f_paydate" => $f_paydate,
                    "f_paymoney" => $f_paymoney,
                    "f_donedate" => $f_donedate,
                    "f_doneamount" => $f_doneamount,
                    "f_paylog" =>  $paylog,
                );
                $this->getsqlmod->update_fineimport_data($temp_data2, $s_cnum[$i]);
                
            }
            
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
    }
    public function delete_Fine_project_body(){
        if(isset($_POST['row_ser']) && isset($_POST['fpid']))
        {
            $noAry = explode(",", $_POST['row_ser']);
            foreach ($noAry as $key => $value) {
                $this->getsqlmod->delete_finepayport_data($value, $_POST['fpid']);
            }

            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        
    }

    public function fine_project_body_sort(){
        if(isset($_POST['row_id']) && isset($_POST['fpid']))
        {
            $this->getsqlmod->update_finepayport_data_sort($_POST['sort_type'], $_POST['row_id'], $_POST['fpid']);

            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        
    }

    

    public function add_lisfp1_paypart(){
        if(!empty($_POST['f_no']) && isset($_POST['f_no']))
        {
            // update Table fine_paypart
            $temp_data1 = array(
                "fpart_amount" => $_POST['f_paymoney'],
                "fpart_type" => $_POST['f_payway'],
                "fpart_date" => $this->tranfer2ADyear2($_POST['f_paydate']),
                "f_fee" => $_POST['f_paycost'],
                "f_status" => $_POST['f_status'],
				"f_cstatus" => $_POST['f_cstatus'],
                "f_source" => (($_POST['source'] == "義務人")?$_POST['source']:(($_POST['source'] == "其他")?($_POST['source']."_".$_POST['source_other'] . "_" . $_POST['source_other']):($_POST['source']."_".$_POST['source_unit'] . "_" . $_POST['source_other']))),
                "f_comment" => $_POST['f_paycomment'],
                "f_refund" => $_POST['f_returnamount'],
                "f_turnsub" => $_POST['f_turnaround']
            );
            $temp_data_clear = array();
            $this->getsqlmod->update_finepayport_data($temp_data1, $_POST['f_no'], $_POST['fpart_num'],$_POST['project_id']);

            $f_paymoney = (((int)$_POST['f_amount'] - (int)$_POST['f_balance']) + (int)$_POST['f_paymoney']+ (int)$_POST['f_turnaround']);    
            
            $pay_log = $this->getsqlmod->getFineimport_data($_POST['f_no'])->result_array();

            if(isset($pay_log[0]['f_paylog']) && !empty($pay_log[0]['f_paylog']))
            {
                $str = "\n";
            }
            else
            {
                $str = "";
            }

            if(((int)$_POST['f_balance'] - (int)$_POST['f_paymoney'] - (int)$_POST['f_turnaround']) <= 0 )
            {                
                $f_donedate = $this->tranfer2ADyear2($_POST['f_paydate']);
                $f_doneamount = $_POST['f_amount'];
                $f_paydate = '0000-00-00';
                $f_paymoney = 0;
                if(((int)$_POST['f_amount'] - (int)$_POST['f_paymoney']) == 0)
                {
                    $paylog = "";
                }
                else
                {
                    $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])). '_' . $_POST['f_paymoney'];
                }

                // 以下判斷：歸零本次核銷紀錄
                $temp_data = array(
                    "fpart_fpnum" => $_POST['f_no']
                );
                $temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
                $temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

                // 已繳納總額
                $f_apaymoney = ($temp_paymoney + $temp_turnsub) ;
                $temp_paylog_ary = mb_split("\n", $pay_log[0]['f_paylog']);
                foreach($temp_paylog_ary as $paylogkey => $paylogvalue) {
                    if(strpos($paylogvalue, $this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate']))) !== false)
                        unset($temp_paylog_ary[$paylogkey]);
                }

                if(count($temp_paylog_ary) == 0)
                {
                    $str = "";
                }
                else
                {
                    $str = "\n";
                }
                $paylog = implode("\n", $temp_paylog_ary) . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])). '_' . ((((int)$_POST['f_paymoney'] >=0)?(int)$_POST['f_paymoney']:0) + (int)$_POST['f_turnaround']);

                if($f_apaymoney < $_POST['f_amount'])
                {
                    $temp_data_clear = array(
                        "f_paydate" => $this->tranfer2ADyear2($_POST['f_paydate']),
                        "f_paymoney" => $f_apaymoney,
                        "f_donedate" => '0000-00-00',
                        "f_doneamount" => 0,
                        "f_paylog" =>  $paylog,
                    );
                }
            }
            else
            {
                $f_donedate = '0000-00-00';
                $f_doneamount = 0;
                $f_paydate = $this->tranfer2ADyear2($_POST['f_paydate']);
                if($pay_log[0]['f_paydate'] != $this->tranfer2ADyear2($_POST['f_paydate']))
                {                    
                    $f_paymoney = $f_paymoney;

                    $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])). '_' . ((int)$_POST['f_paymoney'] + (int)$_POST['f_turnaround']);
                }
                else
                {
                    $temp_data = array(
                        "fpart_fpnum" => $_POST['f_no'],
						"fpart_num" => $_POST['fpart_num'],
                        "fp_createdate"=>$this->tranfer2ADyear2($_POST['f_paydate'])
                    );
                    
                    $temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
                    $temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

                    $f_apaymoney = ($temp_paymoney + $temp_turnsub) ;
                    $temp_paylog_ary = mb_split("\n", $pay_log[0]['f_paylog']);
                    foreach($temp_paylog_ary as $paylogkey => $paylogvalue) {
                        if(strpos($paylogvalue, $this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate']))) !== false)
                            unset($temp_paylog_ary[$paylogkey]);
                    }

                    if(count($temp_paylog_ary) == 0)
                    {
                        $str = "";
                    }
                    else
                    {
                        $str = "\n";
                    }
                    $paylog = implode("\n", $temp_paylog_ary) . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])). '_' . ((((int)$_POST['f_paymoney'] >=0)?(int)$_POST['f_paymoney']:0) + (int)$_POST['f_turnaround']);

                    // 以下判斷：歸零本次核銷紀錄
                    if($f_apaymoney < $_POST['f_amount'])
                    {
                        $temp_data_clear = array(
                            "f_paydate" => $this->tranfer2ADyear2($_POST['f_paydate']),
                            "f_paymoney" => $f_apaymoney,
                            "f_donedate" => '0000-00-00',
                            "f_doneamount" => 0,
                            "f_paylog" =>  $paylog,
                        );
                    }
                }
            }
            
            if(!empty($this->tranfer2ADyear2($_POST['f_paydate'])) && count($temp_data_clear) == 0)
            {
                // update Table fine_import
                $temp_data2 = array(
                    "f_paydate" => $f_paydate,
                    "f_paymoney" => $f_paymoney,
                    "f_donedate" => $f_donedate,
                    "f_doneamount" => $f_doneamount,
                    "f_paylog" =>  $paylog,
                );
                
            }
            else
            {
                $temp_data2 = $temp_data_clear;
            }
            $this->getsqlmod->add_finepayport_data(null, $temp_data2, $_POST['f_no'],$_POST['project_id']);
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
    }

    public function listfp1_ed() {
        $this->load->library('table');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $query = $this->getsqlmod->getfine_list($id)->result(); 
        $fproj = $this->getsqlmod->getfineproject_ed($id)->result(); 
        $query = array();
        foreach (explode(",",$_GET['param']) as $key => $value) {
            $obj = $this->getsqlmod->getFinepart_data_in_person($value, $id)->result(); 
            array_push($query, $obj);
        }
        
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table  table-bordered " id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('', '類型', '案件編號', '受處分人姓名', '身份證編號', '罰鍰/怠金', '完納日期', '完納金額', '已繳金額', '未繳金額', '轉正金額', '退費金額','虛擬帳號', '分期Log', '移送Log', '憑證Log', '撤銷註銷Log', '執行命令Log','');
        $table_row = array();
        asort($query);
		
        foreach ($query as $susp)
        {
            $table_row = NULL;
            
            $table_row[] = $susp[0]->f_no;
            
            $table_row[] = (($susp[0]->type === 'A' )?'<span class="label label-danger">罰鍰</span>':'<span class="label label-warning">怠金</span>');
            $table_row[] = $susp[0]->f_caseid . ((isset($susp[0]->f_dellog) && !empty($susp[0]->f_dellog))?'<span class="text-danger"> (註)</span>':'');

            $table_row[] = $susp[0]->f_username. ((isset($susp[0]->f_turnsub_log) && !empty($susp[0]->f_turnsub_log))?'<br/><small class="text-warning">自'.explode('_',$susp[0]->f_turnsub_log)[2].'轉正</small>':'');

            $table_row[] = $susp[0]->f_userid;

            $table_row[] = (($susp[0]->type === 'A')?(int)$susp[0]->f_amount * 10000:$susp[0]->f_amount);

            $table_row[] = ($susp[0]->f_donedate == '0000-00-00')?'':$this->tranfer2RCyear2($susp[0]->f_donedate);;

            $table_row[] = $susp[0]->f_doneamount;

            $temp_data = array(
                "fpart_fpnum" => ((isset($susp[0]->f_no))?$susp[0]->f_no:0),
                "fp_createdate"=>$fproj[0]->fp_createdate,
                "f_project_id"=>$id
            );
            
            $temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
			$temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

            $table_row[] = (($temp_paymoney < 0)?0:($temp_paymoney+$temp_turnsub));

            $table_row[] = ((((($susp[0]->type === 'A')?(int)$susp[0]->f_amount * 10000:$susp[0]->f_amount) - (int)$susp[0]->f_doneamount - (int)$temp_paymoney - (int)$temp_turnsub) >= 0)?((($susp[0]->type === 'A')?(int)$susp[0]->f_amount * 10000:$susp[0]->f_amount) - (int)$susp[0]->f_doneamount - (int)$temp_paymoney - (int)$temp_turnsub):0);

            $table_row[] = (($susp[0]->f_turnsub < 0)?(0-$susp[0]->f_turnsub):$susp[0]->f_turnsub);
            $table_row[] = $susp[0]->f_refund;

            $table_row[] = $susp[0]->f_virtualcode;

            $logs =  $this->getsqlmod->check_finepayport_data($temp_data)->result();
            if(count($logs) <= 0)
            {
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else
            {
                // $paylogAry = mb_split("\n",$susp->f_paylog);
                $paylogAryStr = '';
                foreach ($logs as $value) {
                    // $temp_v = explode('_', $value);
                    $paylogAry_date = $value->fpart_date;
                    $paylogAry_money = ($value->fpart_amount + $value->f_refund + $value->f_turnsub);
                    $paylogAryStr .= ($this->tranfer2RCyear2($paylogAry_date) . '已繳' . (((int)$paylogAry_money < 0)?('<span class="text-danger">'.$paylogAry_money.'</span>'):$paylogAry_money ) . '元' .'_'. '<span style="border:1px solid #AAA;border-radius:100%;" class="'.((mb_substr($value->fpart_type,0,1) == "金")?'bg-danger':'bg-info').'">'. mb_substr($value->fpart_type,0,1) . '</span><br/>') ;

                    // $temp_data = array(
                    //     "fpart_fpnum" => $susp->f_no,
                    //     "fpart_amount" => $paylogAry_money,
                    //     "fpart_type" => '',
                    //     "fpart_date" => $this->tranfer2ADyear($paylogAry_date)
                    // );

                    // $this->getsqlmod->check_finepayport_data($temp_data);
                }
                $table_row[]  = $paylogAryStr;
            }
			$calcmovelog = explode("\n", $susp[0]->f_movelog);
			usort($calcmovelog, function($a, $b){ 
				$movedateA = explode('_', $a)[0];
				$movedateB = explode('_', $b)[0];
				return ($movedateA < $movedateB)? 1 : -1; 
			}); 
            $table_row[] = join('<br/>', $calcmovelog);
            $table_row[] = str_replace("\n","<br/>", $susp[0]->f_cardlog);
            $table_row[] = str_replace("\n","<br/>", $susp[0]->f_dellog);
            $table_row[] = str_replace("\n","<br/>", $susp[0]->f_execlog);
			$table_row[] = $susp[0]->sort_no;
            $this->table->add_row($table_row);
        }
        
        $data['title'] = "帳務專案:".$id;
        $data['f_no'] = $id;
        $data['fpid'] = $id;
        $data['f_no_all'] = $_GET['param'];
        if(isset($fproj[0]))
        {
            $data['fp_num'] = $fproj[0]->fp_office_num;
            $data['fp_type'] = $fproj[0]->fp_type;
            $data['fp_paytype'] = $fproj[0]->fp_paytype;
            $data['fp_date'] = $this->tranfer2RCyear2($fproj[0]-> fp_createdate);
        }
        else 
        {
            $data['fp_num'] =null;
            $data['fp_type'] = null;
            $data['fp_paytype'] = null;
            $data['fp_date'] = null;
        }
        sort($query);
        $data['user'] = $this -> session -> userdata('uic');
        $data['s_table'] = $this->table->generate();
        $data['data'] = json_decode(json_encode($query));
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/fplisted';
        else $data['include'] = 'acc_cert_query/fplisted';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

    public function add_lisfp1_ed_paypart(){
        if(!empty($_POST['id']) && isset($_POST['id']))
        {
            $idAry = explode(",", $_POST['id']);
			$serAry = explode(",", $_POST['fpart_num']);
			$i = 0;
            foreach ($idAry as $key => $value) {
               // update Table fine_paypart
                $temp_data1 = array(
                    "fpart_fpnum" => $value,
                    "fpart_amount" => $_POST['f_paymoney'][$value],
                    "fpart_type" => $_POST['f_payway'][$value],
                    "fpart_date" => $this->tranfer2ADyear2($_POST['f_paydate'][$value]),
                    "f_fee" => $_POST['f_paycost'][$value],                    
                    "f_status" => $_POST['f_status'][$value],   
					"f_cstatus" => $_POST['f_cstatus'][$value],                 
                    "f_source" => (($_POST['source'][$value] == "義務人")?$_POST['source'][$value]:(($_POST['source'][$value] == "其他")?($_POST['source'][$value]."_".$_POST['source_other'][$value] . "_" . $_POST['source_other'][$value]):($_POST['source'][$value]."_".$_POST['source_unit'][$value] . "_" . $_POST['source_other'][$value]))),
                    "f_comment" => $_POST['f_paycomment'][$value],
                    "f_refund" => $_POST['f_returnamount'][$value],
                    "f_turnsub" => $_POST['f_turnaround'][$value]
                );
				$temp_data_clear = array();
                $this->getsqlmod->update_finepayport_data($temp_data1, $value, $serAry[$i],$_POST['project_id']);

                $f_paymoney = (((int)$_POST['f_amount'][$value] - (int)$_POST['f_balance'][$value]) + (int)$_POST['f_paymoney'][$value] + (int)$_POST['f_turnaround'][$value]);
                $pay_log = $this->getsqlmod->getFineimport_data($value)->result_array();

                if(isset($pay_log[0]['f_paylog']) && !empty($pay_log[0]['f_paylog']))
                {
                    $str = "\n";
                }
                else
                {
                    $str = "";
                }

                if(((int)$_POST['f_balance'][$value] - (int)$_POST['f_paymoney'][$value] - (int)$_POST['f_turnaround'][$value]) <= 0)
                {
                    $f_donedate = $this->tranfer2ADyear2($_POST['f_paydate'][$value]);
                    $f_doneamount = $_POST['f_amount'][$value];
                    $f_paydate = '0000-00-00';
                    $f_paymoney = 0;
                    if(((int)$_POST['f_amount'][$value] - (int)$_POST['f_paymoney'][$value]) == 0)
                    {
                        $paylog = "";
                    }
                    else
                    {
                        $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])). '_' . $_POST['f_paymoney'][$value];
                    }

					// 以下判斷：歸零本次核銷紀錄
					$temp_data = array(
						"fpart_fpnum" => $value
					);
					$temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
					$temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;
	
					// 已繳納總額
					$f_apaymoney = ($temp_paymoney + $temp_turnsub) ;
					$temp_paylog_ary = mb_split("\n", $pay_log[0]['f_paylog']);
					foreach($temp_paylog_ary as $paylogkey => $paylogvalue) {
						if(strpos($paylogvalue, $this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value]))) !== false)
							unset($temp_paylog_ary[$paylogkey]);
					}
	
					if(count($temp_paylog_ary) == 0)
					{
						$str = "";
					}
					else
					{
						$str = "\n";
					}
					$paylog = implode("\n", $temp_paylog_ary) . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])). '_' . ((int)$_POST['f_paymoney'][$value] + (int)$_POST['f_turnaround'][$value]);
	
					if($f_apaymoney < $_POST['f_amount'][$value])
					{
						$temp_data_clear = array(
							"f_paydate" => $this->tranfer2ADyear2($_POST['f_paydate'][$value]),
							"f_paymoney" => $f_apaymoney,
							"f_donedate" => '0000-00-00',
							"f_doneamount" => 0,
							"f_paylog" =>  $paylog,
						);
					}
                }
                else
                {
                    $f_donedate = '0000-00-00';
                    $f_doneamount = 0;
                    $f_paydate = $this->tranfer2ADyear2($_POST['f_paydate'][$value]);

                    if($pay_log[0]['f_paydate'] != $this->tranfer2ADyear2($_POST['f_paydate']))
                    {                    
                        $f_paymoney = $f_paymoney;

                        $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])). '_' . $_POST['f_paymoney'][$value];
                    }
                    else
                    {
                        $temp_data = array(
                            "fpart_fpnum" => $value,
							"fpart_num" => $serAry[$i],
                            "fp_createdate"=>$this->tranfer2ADyear2($_POST['f_paydate'][$value])
                        );
                        
                        $temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
						$temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

                        $f_apaymoney = ($temp_paymoney + $temp_turnsub);
                        $temp_paylog_ary = mb_split("\n", $pay_log[0]['f_paylog']);

                        foreach($temp_paylog_ary as $paylogkey => $paylogvalue) {
                            if(strpos($paylogvalue, $this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value]))) !== false)
                                unset($temp_paylog_ary[$paylogkey]);
                        }
                        if(count($temp_paylog_ary) == 0)
                        {
                            $str = "";
                        }
                        else
                        {
                            $str = "\n";
                        }
                        $paylog = implode("\n", $temp_paylog_ary) . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])). '_' . ((int)$_POST['f_paymoney'][$value] + (int)$_POST['f_turnaround'][$value]);

						// 以下判斷：歸零本次核銷紀錄
						if($f_apaymoney < $_POST['f_amount'][$value])
						{
							$temp_data_clear = array(
								"f_paydate" => $this->tranfer2ADyear2($_POST['f_paydate'][$value]),
								"f_paymoney" => $f_apaymoney,
								"f_donedate" => '0000-00-00',
								"f_doneamount" => 0,
								"f_paylog" =>  $paylog,
							);
						}
                    }
                }
                if(!empty($this->tranfer2ADyear2($_POST['f_paydate'][$value])) && count($temp_data_clear) == 0)
                {
                    // update Table fine_import
                    $temp_data2 = array(
                        "f_paydate" => $f_paydate,
                        "f_paymoney" => $f_paymoney,
                        "f_donedate" => $f_donedate,
                        "f_doneamount" => $f_doneamount,
                        "f_paylog" => $paylog 
                    );
            
                    
                }
				else
				{
					$temp_data2 = $temp_data_clear;
				}
				$this->getsqlmod->add_finepayport_data(null, $temp_data2, $value, $_POST['project_id']);

				$i++;
            }
            
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
    }
   
    public function editfplist(){
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        foreach ($s_cnum1 as $key => $value) {
            $fine_rec = $this->getsqlmod->getfine_list2($value)->result();
            $fine = array(
                'f_payamount' => $fine_rec[0]->f_payamount + $_POST['fpart_amount'][$value],
                'f_paynowamount' => $_POST['f_paynowamount'][$value],
                'f_payday' => date("Y-m-d"),
                'f_paytype' => $_POST['f_paytype'][$value],
                'f_fee' => $_POST['f_fee'][$value],
                'f_turnsub' => $_POST['f_turnsub'][$value],
                'f_refund' => $_POST['f_refund'][$value],
            );
            $fine_part = array(
                'fpart_amount' => $_POST['fpart_amount'][$value],
                'fpart_date' => date('Y-m-d'),
                'fpart_fpnum' => $value,
                'fpart_type' => '金融',
            );
            //var_dump($fine);
            $this->getsqlmod->updateFine_bank($value,$fine);
            if($_POST['fpart_amount'][$value]!=0)$this->getsqlmod->addFinePart_bank($fine_part);
        };
        redirect('Acc_cert/listfp1/'.$_POST['f_no']); 
    }
    
    public function listfp2() {//支匯票作業檢視
        $this->load->library('table');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $query = $this->getsqlmod->getfine_list($id)->result(); 
        // $fproj = $this->getsqlmod->getfineproject_ed($id)->result(); 
        $query = $this->getsqlmod->get_finepoject_body_data_full($id)->result();
        $fproj = $this->getsqlmod->getfineproject_ed($id)->result(); 
        $sum_payamount = 0;
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-bordered" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        // $this->table->set_heading( '','處分書編號', '犯嫌人姓名', '身份證','年度繳納金額','完納金額','分期資訊','移送案號','分局','憑證核發日期','憑證編號','註銷保留款','虛擬賬號');
        $this->table->set_heading('','', '類型', '案件編號', '受處分人姓名', '身份證編號', '罰鍰/怠金', '完納日期', '完納金額', '已繳金額', '未繳金額', '轉正金額', '退費金額','收據號碼','虛擬帳號', '分期Log', '移送Log', '憑證Log', '撤銷註銷Log', '執行命令Log','備註','');
        $table_row = array();
        foreach ($query as $susp)
        {
            

            $table_row = NULL;
            $table_row[] = $susp->f_no;
			$table_row[] = $susp->fpart_num;
            $table_row[] = (($susp->type === 'A' )?'<span class="label label-danger">罰鍰</span>':(($susp->type === 'B' )?'<span class="label label-warning">怠金</span>':''));
            $table_row[] = $susp->f_caseid . ((isset($susp->f_dellog) && !empty($susp->f_dellog))?'<span class="text-danger"> (註)</span>':'');
            $table_row[] = $susp->f_username. ((isset($susp->f_turnsub_log) && !empty($susp->f_turnsub_log))?'<br/><small class="text-warning">自'.explode('_',$susp->f_turnsub_log)[2].'轉正</small>':'');
            $table_row[] = $susp->f_userid;
            $table_row[] = (($susp->type === 'A')?(int)$susp->f_amount * 10000:$susp->f_amount);
            $table_row[] = ($susp->f_donedate == '0000-00-00')?'':$this->tranfer2RCyear2($susp->f_donedate);
            $table_row[] = $susp->f_doneamount;

            $temp_data = array(
                "fpart_fpnum" => ((isset($susp->f_no))?$susp->f_no:0),
                "fp_createdate"=>$fproj[0]->fp_createdate,
                "f_project_id"=>$id
            );
            
            $temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
			$temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

            $table_row[] = (($temp_paymoney < 0)?0:($temp_paymoney+$temp_turnsub));

            $table_row[] = ((((($susp->type === 'A')?(int)$susp->f_amount * 10000:$susp->f_amount) - (int)$susp->f_doneamount - (int)$temp_paymoney - (int)$temp_turnsub) >= 0)?((($susp->type === 'A')?(int)$susp->f_amount * 10000:$susp->f_amount) - (int)$susp->f_doneamount - (int)$temp_paymoney - (int)$temp_turnsub):0);

            $table_row[] = (($susp->f_turnsub < 0)?(0-$susp->f_turnsub):$susp->f_turnsub);
            $table_row[] = $susp->f_refund;
            $table_row[] = $susp->f_receipt_no;

            $table_row[] = $susp->f_virtualcode;

            

            $logs =  $this->getsqlmod->check_finepayport_data($temp_data)->result();
            if(count($logs) <= 0)
            {
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else
            {
                // $paylogAry = mb_split("\n",$susp->f_paylog);
                $paylogAryStr = '';
                foreach ($logs as $value) {
                    // $temp_v = explode('_', $value);
                    $paylogAry_date = $value->fpart_date;
                    $paylogAry_money = ($value->fpart_amount + $value->f_refund + $value->f_turnsub);
                    $paylogAryStr .= ($this->tranfer2RCyear2($paylogAry_date) . '已繳' . (((int)$paylogAry_money < 0)?('<span class="text-danger">'.$paylogAry_money.'</span>'):$paylogAry_money ) . '元' .'_'. '<span style="border:1px solid #AAA;border-radius:100%;" class="'.((mb_substr($value->fpart_type,0,1) == "金")?'bg-danger':'bg-info').'">'. mb_substr($value->fpart_type,0,1) . '</span><br/>') ;

                    // $temp_data = array(
                    //     "fpart_fpnum" => $susp->f_no,
                    //     "fpart_amount" => $paylogAry_money,
                    //     "fpart_type" => '',
                    //     "fpart_date" => $this->tranfer2ADyear($paylogAry_date)
                    // );

                    // $this->getsqlmod->check_finepayport_data($temp_data);
                }
                $table_row[]  = $paylogAryStr;
            }
			$calcmovelog = explode("\n", $susp->f_movelog);
			usort($calcmovelog, function($a, $b){ 
				$movedateA = explode('_', $a)[0];
				$movedateB = explode('_', $b)[0];
				return ($movedateA < $movedateB)? 1 : -1; 
			}); 
            $table_row[] = join('<br/>', $calcmovelog);
            $table_row[] = str_replace("\n","<br/>", $susp->f_cardlog);
            $table_row[] = str_replace("\n","<br/>", $susp->f_dellog);
            $table_row[] = str_replace("\n","<br/>", $susp->f_execlog);
            $table_row[] = $susp->body_comment;
            $table_row[] = $susp->sort_no;
			
            $sum_payamount = $sum_payamount + ((($susp->fpart_amount >= 0)?$susp->fpart_amount:0) + $susp->f_fee + (($susp->f_turnsub < 0)?0-$susp->f_turnsub:$susp->f_turnsub)  + $susp->f_refund);
            $sort_no = (($susp->sort_no)?$susp->sort_no:1);
            $this->table->add_row($table_row);
            // $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            // $partpay=null;
            // $partpayamount=0;
            // foreach($fpart as $fp){
            //     if(isset($fp)){
            //         $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
            //         $partpayamount = $partpayamount + $fp->fpart_amount;
            //     }else{
            //         $partpay = null;   
            //     }
            // }
            // $table_row = NULL;
            // $table_row[] = $susp->f_num;
            // if(isset($susp->surc_no)&&$susp->f_samount<=0){
            //     $table_row[] = $susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no;
            // }else   $table_row[] = $susp->fd_num;
            // $table_row[] = $susp->s_name;
            // $table_row[] = $susp->s_ic;
            // $table_row[] = $susp->f_damount+$susp->f_samount;
            // $table_row[] = $susp->f_payamount;
            // if($partpay == null){
            //     $table_row[] = '<strong>無分期繳款記錄</strong>';
            // }
            // else{
            //     $table_row[] = $partpay;
            // }
            // $table_row[] = $susp->ft_no;//過後要改
            // $table_row[] = $susp->s_roffice;
            // if($susp->f_certno == null){
            //     $table_row[] = '<strong>未輸入憑證</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_certno;
            // }
            // if($susp->f_certDate == null){
            //     $table_row[] = '<strong>未輸入憑證日期</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_certDate;
            // }
            // if($susp->f_cancelAmount == null){
            //     $table_row[] = '<strong>0</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_cancelAmount;
            // }
            // if($susp->f_BVC == null){
            //     $table_row[] = '<strong>未輸入</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_BVC;
            // }
            // //$table_row[] = '<input type="checkbox" name="a">';
            // $this->table->add_row($table_row);
        }   
        $data['title'] = "帳務專案:".$id;
        $data['fpid'] = $id;
        $data['total'] = "$ " . number_format($sum_payamount); 
        $data['max_sort'] = ((isset($sort_no))?$sort_no:0);
        $data['fp_status'] = $fproj[0]->fp_status;
        if(isset($fproj[0]))
        {
            $data['fp_num'] = $fproj[0]->fp_office_num;
            $data['fp_type'] = $fproj[0]->fp_type;
            $data['fp_paytype'] = $fproj[0]->fp_paytype;
            $data['fp_date'] = $this->tranfer2RCyear2($fproj[0]->fp_createdate);
        }
        else 
        {
            $data['fp_num'] =null;
            $data['fp_type'] = null;
            $data['fp_paytype'] = null;
            $data['fp_date'] = null;
        }
        // if(isset($fproj[0]))$data['fp_num'] = $fproj[0]->fp_office_num;
        // else $data['fp_num'] =null;
        $data['user'] = $this -> session -> userdata('uic');
        $data['s_table'] = $this->table->generate();
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/fplist2';
        else $data['include'] = 'acc_cert_query/fplist2';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

    public function add_lisfp2_paypart(){
        if(!empty($_POST['f_no']) && isset($_POST['f_no']))
        {
            // update Table fine_paypart
            $temp_data1 = array(
                "fpart_amount" => $_POST['f_paymoney'],
                "fpart_type" => $_POST['f_payway'],
                "fpart_date" => $this->tranfer2ADyear2($_POST['f_paydate']),
                "f_fee" => $_POST['f_paycost'],
                "f_status" => $_POST['f_status'],
				"f_cstatus" => $_POST['f_cstatus'],
                "f_source" => (($_POST['source'] == "義務人")?$_POST['source']:(($_POST['source'] == "其他")?($_POST['source']."_".$_POST['source_other'] . "_" . $_POST['source_other']):($_POST['source']."_".$_POST['source_unit'] . "_" . $_POST['source_other']))),
                "f_comment" => $_POST['f_paycomment'],
                "f_refund" => $_POST['f_returnamount'],
                "f_turnsub" => $_POST['f_turnaround'],
                "f_office_num" => $_POST['f_office_num'],
                "f_ticket_no" => $_POST['f_ticket_no'],
                "f_receipt_no" => $_POST['f_receipt_no']
            );
			$temp_data_clear = array();
            $this->getsqlmod->update_finepayport_data($temp_data1, $_POST['f_no'], $_POST['fpart_num'], $_POST['project_id']);

            $f_paymoney = (((int)$_POST['f_amount'] - (int)$_POST['f_balance']) + (((int)$_POST['f_paymoney'] >=0)?(int)$_POST['f_paymoney']:0)+ (int)$_POST['f_turnaround']);    
            
            $pay_log = $this->getsqlmod->getFineimport_data($_POST['f_no'])->result_array();

            if(isset($pay_log[0]['f_paylog']) && !empty($pay_log[0]['f_paylog']))
            {
                $str = "\n";
            }
            else
            {
                $str = "";
            }

            if(((int)$_POST['f_balance'] - (int)$_POST['f_paymoney'] - (int)$_POST['f_turnaround']) <= 0)
            {
                $f_donedate = $this->tranfer2ADyear2($_POST['f_paydate']);
                $f_doneamount = $_POST['f_amount'];
                $f_paydate = '0000-00-00';
                $f_paymoney = 0;
                if(((int)$_POST['f_amount'] - (int)$_POST['f_paymoney']) == 0)
                {
                    $paylog = "";
                }
                else
                {
                    $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])). '_' . $_POST['f_paymoney'];
                }

				// 以下判斷：歸零本次核銷紀錄
                $temp_data = array(
                    "fpart_fpnum" => $_POST['f_no']
                );
                $temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
                $temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

                // 已繳納總額
                $f_apaymoney = ($temp_paymoney + $temp_turnsub) ;
                $temp_paylog_ary = mb_split("\n", $pay_log[0]['f_paylog']);
                foreach($temp_paylog_ary as $paylogkey => $paylogvalue) {
                    if(strpos($paylogvalue, $this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])).'_'.$_POST['f_paymoney']) !== false)
                        unset($temp_paylog_ary[$paylogkey]);
                }

                if(count($temp_paylog_ary) == 0)
                {
                    $str = "";
                }
                else
                {
                    $str = "\n";
                }
                $paylog = implode("\n", $temp_paylog_ary) . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])). '_' . ((((int)$_POST['f_paymoney'] >=0)?(int)$_POST['f_paymoney']:0) + (int)$_POST['f_turnaround']);

                if($f_apaymoney < $_POST['f_amount'])
                {
                    $temp_data_clear = array(
                        "f_paydate" => $this->tranfer2ADyear2($_POST['f_paydate']),
                        "f_paymoney" => $f_apaymoney,
                        "f_donedate" => '0000-00-00',
                        "f_doneamount" => 0,
                        "f_paylog" =>  $paylog,
                    );
                }
            }
            else
            {
                $f_donedate = '0000-00-00';
                $f_doneamount = 0;
                $f_paydate = $this->tranfer2ADyear2($_POST['f_paydate']);


                if($pay_log[0]['f_paydate'] != $this->tranfer2ADyear2($_POST['f_paydate']))
                {                    
                    $f_paymoney = $f_paymoney;

                    $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])). '_' . ((int)$_POST['f_paymoney'] + (int)$_POST['f_turnaround']);
					// echo $paylog;
					// exit;
                }
                else
                {
                    $temp_data = array(
                        "fpart_fpnum" => $_POST['f_no'],
						"fpart_num" => $_POST['fpart_num'],
                        "fp_createdate"=>$this->tranfer2ADyear2($_POST['f_paydate'])
                    );
                    
                    $temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
					$temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

                    $f_apaymoney = ($temp_paymoney + $temp_turnsub) ;
                    $temp_paylog_ary = mb_split("\n", $pay_log[0]['f_paylog']);
					// var_dump($temp_paylog_ary);
                    foreach($temp_paylog_ary as $paylogkey => $paylogvalue) {
						// echo $paylogvalue;
                        if(strpos($paylogvalue, $this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])).'_'.$_POST['f_paymoney']) !== false)
                            unset($temp_paylog_ary[$paylogkey]);
                    }
					// exit;
                    if(count($temp_paylog_ary) == 0)
                    {
                        $str = "";
                    }
                    else
                    {
                        $str = "\n";
                    }
                    $paylog = implode("\n", $temp_paylog_ary) . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'])). '_' . ((int)$_POST['f_paymoney'] + (int)$_POST['f_turnaround']);

					// 以下判斷：歸零本次核銷紀錄
                    if($f_apaymoney < $_POST['f_amount'])
                    {
                        $temp_data_clear = array(
                            "f_paydate" => $this->tranfer2ADyear2($_POST['f_paydate']),
                            "f_paymoney" => $f_apaymoney,
                            "f_donedate" => '0000-00-00',
                            "f_doneamount" => 0,
                            "f_paylog" =>  $paylog,
                        );
                    }
                }
            }
            
            if(!empty($this->tranfer2ADyear2($_POST['f_paydate'])) && count($temp_data_clear) == 0)
            {
                // update Table fine_import
                $temp_data2 = array(
                    "f_paydate" => $f_paydate,
                    "f_paymoney" => $f_paymoney,
                    "f_donedate" => $f_donedate,
                    "f_doneamount" => $f_doneamount,
                    "f_paylog" =>  $paylog
                );
            }
            else
            {
                $temp_data2 = $temp_data_clear;
            }
            $this->getsqlmod->add_finepayport_data(null, $temp_data2, $_POST['f_no'],$_POST['project_id']);
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
    }
    

    public function listfp2_ed() {//支匯票作業編輯
        $this->load->library('table');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $query = $this->getsqlmod->getfine_list($id)->result(); 
        $fproj = $this->getsqlmod->getfineproject_ed($id)->result();
        $query = array();
        foreach (explode(",",$_GET['param']) as $key => $value) {
            $obj = $this->getsqlmod->getFinepart_data_in_person($value, $id)->result(); 
            array_push($query, $obj);
        }
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-bordered" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('', '類型', '案件編號', '受處分人姓名', '身份證編號', '罰鍰/怠金', '完納日期', '完納金額', '已繳金額', '未繳金額', '轉正金額', '退費金額','虛擬帳號', '分期Log', '移送Log', '憑證Log', '撤銷註銷Log', '執行命令Log','');
        // $this->table->set_heading( '','處分書編號', '犯嫌人姓名', '身份證','年度繳納金額','完納金額','分期資訊','移送案號','分局','憑證核發日期','憑證編號','註銷保留款','虛擬賬號','繳納金額','繳納日期','分期/完納核銷金額','執行費','支匯票來文單位','收據編號','公文號','支票號碼');
        $table_row = array();
        asort($query);
		$f_no_all = array();
        foreach ($query as $susp)
        {
            // $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            // $partpay=null;
            // $partpayamount=0;
            // foreach($fpart as $fp){
            //     if(isset($fp)){
            //         $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
            //         $partpayamount = $partpayamount + $fp->fpart_amount;
            //     }else{
            //         $partpay = null;   
            //     }
            // }
            $table_row = NULL;
            $table_row[] = $susp[0]->f_no;
			array_push($f_no_all, $susp[0]->f_no);
            
            $table_row[] = (($susp[0]->type === 'A' )?'<span class="label label-danger">罰鍰</span>':'<span class="label label-warning">怠金</span>');
            $table_row[] = $susp[0]->f_caseid . ((isset($susp[0]->f_dellog) && !empty($susp[0]->f_dellog))?'<span class="text-danger"> (註)</span>':'');

            $table_row[] = $susp[0]->f_username. ((isset($susp[0]->f_turnsub_log) && !empty($susp[0]->f_turnsub_log))?'<br/><small class="text-warning">自'.explode('_',$susp[0]->f_turnsub_log)[2].'轉正</small>':'');

            $table_row[] = $susp[0]->f_userid;

            $table_row[] = (($susp[0]->type === 'A')?(int)$susp[0]->f_amount * 10000:$susp[0]->f_amount);

            $table_row[] = ($susp[0]->f_donedate == '0000-00-00')?'':$this->tranfer2RCyear2($susp[0]->f_donedate);

            $table_row[] = $susp[0]->f_doneamount;

            $temp_data = array(
                "fpart_fpnum" => ((isset($susp[0]->f_no))?$susp[0]->f_no:0),
                "fp_createdate"=>$fproj[0]->fp_createdate,
                "f_project_id"=>$id
            );
            
            $temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
			$temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

            $table_row[] = (($temp_paymoney < 0)?0:($temp_paymoney+$temp_turnsub));

            $table_row[] = ((((($susp[0]->type === 'A')?(int)$susp[0]->f_amount * 10000:$susp[0]->f_amount) - (int)$susp[0]->f_doneamount - (int)$temp_paymoney - (int)$temp_turnsub) >= 0)?((($susp[0]->type === 'A')?(int)$susp[0]->f_amount * 10000:$susp[0]->f_amount) - (int)$susp[0]->f_doneamount - (int)$temp_paymoney - (int)$temp_turnsub):0);

            $table_row[] = (($susp[0]->f_turnsub < 0)?(0-$susp[0]->f_turnsub):$susp[0]->f_turnsub);
            $table_row[] = $susp[0]->f_refund;

            $table_row[] = $susp[0]->f_virtualcode;

            $logs =  $this->getsqlmod->check_finepayport_data($temp_data)->result();
            if(count($logs) <= 0)
            {
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else
            {
                // $paylogAry = mb_split("\n",$susp->f_paylog);
                $paylogAryStr = '';
                foreach ($logs as $value) {
                    // $temp_v = explode('_', $value);
                    $paylogAry_date = $value->fpart_date;
                    $paylogAry_money = ($value->fpart_amount + $value->f_refund + $value->f_turnsub);
                    $paylogAryStr .= ($this->tranfer2RCyear2($paylogAry_date) . '已繳' . (((int)$paylogAry_money < 0)?('<span class="text-danger">'.$paylogAry_money.'</span>'):$paylogAry_money ) . '元' .'_'. '<span style="border:1px solid #AAA;border-radius:100%;" class="'.((mb_substr($value->fpart_type,0,1) == "金")?'bg-danger':'bg-info').'">'. mb_substr($value->fpart_type,0,1) . '</span><br/>') ;

                    // $temp_data = array(
                    //     "fpart_fpnum" => $susp->f_no,
                    //     "fpart_amount" => $paylogAry_money,
                    //     "fpart_type" => '',
                    //     "fpart_date" => $this->tranfer2ADyear($paylogAry_date)
                    // );

                    // $this->getsqlmod->check_finepayport_data($temp_data);
                }
                $table_row[]  = $paylogAryStr;
            }
			$calcmovelog = explode("\n", $susp[0]->f_movelog);
			usort($calcmovelog, function($a, $b){ 
				$movedateA = explode('_', $a)[0];
				$movedateB = explode('_', $b)[0];
				return ($movedateA < $movedateB)? 1 : -1; 
			}); 
            $table_row[] = join('<br/>', $calcmovelog);
            $table_row[] = str_replace("\n","<br/>", $susp[0]->f_cardlog);
            $table_row[] = str_replace("\n","<br/>", $susp[0]->f_dellog);
            $table_row[] = str_replace("\n","<br/>", $susp[0]->f_execlog);
			$table_row[] = $susp[0]->sort_no;
            // $table_row[] = $susp->f_num;
            // if(isset($susp->surc_no)&&$susp->f_samount<=0){
            //     $table_row[] = $susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no;
            // }else   $table_row[] = $susp->fd_num;
            // $table_row[] = $susp->s_name;
            // $table_row[] = $susp->s_ic;
            // $table_row[] = $susp->f_damount+$susp->f_samount;
            // $table_row[] = $susp->f_payamount;
            // if($partpay == null){
            //     $table_row[] = '<strong>無分期繳款記錄</strong>';
            // }
            // else{
            //     $table_row[] = $partpay;
            // }
            // $table_row[] = $susp->ft_no;
            // $table_row[] = $susp->s_roffice;
            // if($susp->f_certno == null){
            //     $table_row[] = '<strong>未輸入憑證</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_certno;
            // }
            // if($susp->f_certDate == null){
            //     $table_row[] = '<strong>未輸入憑證日期</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_certDate;
            // }
            // if($susp->f_cancelAmount == null){
            //     $table_row[] = '<strong>未輸入姓名</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_cancelAmount;
            // }
            // if($susp->f_BVC == null){
            //     $table_row[] = '<strong>未輸入姓名</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_BVC;
            // }
            // $table_row[] = '<input name="f_paynowamount['.$susp->f_num.']" type="number" required="required" value=0 /><input name="f_paytype['.$susp->f_num.']" type="text" hidden value="支匯票"/>';
            // $table_row[] = '<input name="f_payday['.$susp->f_num.']" type="date" required="required"/>';
            // $table_row[] = '<input name="fpart_amount['.$susp->f_num.']" type="number" required="required" value=0 />';
            // $table_row[] = '<input name="f_fee['.$susp->f_num.']" type="number" required="required" value=0  />';
            // $table_row[] = '<input name="f_check_unit['.$susp->f_num.']" type="text" required="required" />';
            // $table_row[] = '<input name="f_check_receipt['.$susp->f_num.']" type="text" required="required" />';
            // $table_row[] = '<input name="f_check_officenum['.$susp->f_num.']" type="text" required="required"/>';
            // $table_row[] = '<input name="f_check_checknum['.$susp->f_num.']" type="text" required="required"/>';
            $this->table->add_row($table_row);
        }   
        $data['title'] = "帳務專案:".$id;
        $data['f_no'] = $id;
        $data['fpid'] = $id;
        $data['fpart_num_all'] = $_GET['param'];
		$data['f_no_all'] = join(',', $f_no_all);
        if(isset($fproj[0]))
        {
            $data['fp_num'] = $fproj[0]->fp_office_num;
            $data['fp_type'] = $fproj[0]->fp_type;
            $data['fp_paytype'] = $fproj[0]->fp_paytype;
            $data['fp_date'] = $this->tranfer2RCyear2($fproj[0]-> fp_createdate);
        }
        else 
        {
            $data['fp_num'] =null;
            $data['fp_type'] = null;
            $data['fp_paytype'] = null;
            $data['fp_date'] = null;
        }
        sort($query);
        $data['user'] = $this -> session -> userdata('uic');
        $data['s_table'] = $this->table->generate();
        $data['data'] = json_decode(json_encode($query));
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/fplisted2';
        else $data['include'] = 'acc_cert_query/fplisted2';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

    public function add_lisfp2_ed_paypart(){
        if(!empty($_POST['id']) && isset($_POST['id']))
        {
            $idAry = explode(",", $_POST['id']);
			$serAry = explode(",", $_POST['fpart_num']);
			$i = 0;
            foreach ($idAry as $key => $value) {
               // update Table fine_paypart
                $temp_data1 = array(
                    "fpart_fpnum" => $value,
                    "fpart_amount" => $_POST['f_paymoney'][$value],
                    "fpart_type" => $_POST['f_payway'][$value],
                    "fpart_date" => $this->tranfer2ADyear2($_POST['f_paydate'][$value]),
                    "f_fee" => $_POST['f_paycost'][$value],
                    "f_status" => $_POST['f_status'][$value],
					"f_cstatus" => $_POST['f_cstatus'][$value],
                    "f_source" => (($_POST['source'][$value] == "義務人")?$_POST['source'][$value]:(($_POST['source'][$value] == "其他")?($_POST['source'][$value]."_".$_POST['source_other'][$value] . "_" . $_POST['source_other'][$value]):($_POST['source'][$value]."_".$_POST['source_unit'][$value] . "_" . $_POST['source_other'][$value]))),
                    "f_comment" => $_POST['f_paycomment'][$value],
                    "f_refund" => $_POST['f_returnamount'][$value],
                    "f_turnsub" => $_POST['f_turnaround'][$value],
                    "f_office_num" => $_POST['f_office_num'][$value],
                    "f_ticket_no" => $_POST['f_ticket_no'][$value],
                    "f_receipt_no" => $_POST['f_receipt_no'][$value]
                );
				$temp_data_clear = array();
                $this->getsqlmod->update_finepayport_data($temp_data1, $value, $serAry[$i], $_POST['project_id']);

                $f_paymoney = (((int)$_POST['f_amount'][$value] - (int)$_POST['f_balance'][$value]) + (int)$_POST['f_paymoney'][$value] + (int)$_POST['f_turnaround'][$value]);
                $pay_log = $this->getsqlmod->getFineimport_data($value)->result_array();

                if(isset($pay_log[0]['f_paylog']) && !empty($pay_log[0]['f_paylog']))
                {
                    $str = "\n";
                }
                else
                {
                    $str = "";
                }

                if(((int)$_POST['f_balance'][$value] - (int)$_POST['f_paymoney'][$value] - (int)$_POST['f_turnaround'][$value]) <= 0)
                {
                    $f_donedate = $this->tranfer2ADyear2($_POST['f_paydate'][$value]);
                    $f_doneamount = $_POST['f_amount'][$value];
                    $f_paydate = '0000-00-00';
                    $f_paymoney = 0;
                    if(((int)$_POST['f_amount'][$value] - (int)$_POST['f_paymoney'][$value]) == 0)
                    {
                        $paylog = "";
                    }
                    else
                    {
                        $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])). '_' . $_POST['f_paymoney'][$value];
                    }

					// 以下判斷：歸零本次核銷紀錄
					$temp_data = array(
						"fpart_fpnum" => $value
					);
					$temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
					$temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;
	
					// 已繳納總額
					$f_apaymoney = ($temp_paymoney + $temp_turnsub) ;
					$temp_paylog_ary = mb_split("\n", $pay_log[0]['f_paylog']);
					foreach($temp_paylog_ary as $paylogkey => $paylogvalue) {
						if(strpos($paylogvalue, $this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])).'_'.$_POST['f_paymoney'][$value]) !== false)
							unset($temp_paylog_ary[$paylogkey]);
					}
	
					if(count($temp_paylog_ary) == 0)
					{
						$str = "";
					}
					else
					{
						$str = "\n";
					}
					$paylog = implode("\n", $temp_paylog_ary) . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])). '_' . ((int)$_POST['f_paymoney'][$value] + (int)$_POST['f_turnaround'][$value]);
	
					if($f_apaymoney < $_POST['f_amount'][$value])
					{
						$temp_data_clear = array(
							"f_paydate" => $this->tranfer2ADyear2($_POST['f_paydate'][$value]),
							"f_paymoney" => $f_apaymoney,
							"f_donedate" => '0000-00-00',
							"f_doneamount" => 0,
							"f_paylog" =>  $paylog,
						);
					}
                }
                else
                {
                    $f_donedate = '0000-00-00';
                    $f_doneamount = 0;
                    $f_paydate = $_POST['f_paydate'][$value];

                    if($pay_log[0]['f_paydate'] != $this->tranfer2ADyear2($_POST['f_paydate']))
                    {                    
                        $f_paymoney = $f_paymoney;

                        $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])). '_' . $_POST['f_paymoney'][$value];
                    }
                    else
                    {
                        $temp_data = array(
                            "fpart_fpnum" => $value,
							"fpart_num" => $serAry[$i],
                            "fp_createdate"=>$this->tranfer2ADyear2($_POST['f_paydate'][$value])
                        );
                        
                        $temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
						$temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;

                        $f_apaymoney = ($temp_paymoney + $temp_turnsub) ;
                        $temp_paylog_ary = mb_split("\n", $pay_log[0]['f_paylog']);
                        foreach($temp_paylog_ary as $paylogkey => $paylogvalue) {
                            if(strpos($paylogvalue, $this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])).'_'.$_POST['f_paymoney'][$value]) !== false)
                                unset($temp_paylog_ary[$paylogkey]);
                        }
                        if(count($temp_paylog_ary) == 0)
                        {
                            $str = "";
                        }
                        else
                        {
                            $str = "\n";
                        }
                        $paylog = implode("\n", $temp_paylog_ary) . $str .$this->tranfer2RCyear($this->tranfer2ADyear2($_POST['f_paydate'][$value])). '_' . ((int)$_POST['f_paymoney'][$value] + (int)$_POST['f_turnaround'][$value]);

						// 以下判斷：歸零本次核銷紀錄
						if($f_apaymoney < $_POST['f_amount'][$value])
						{
							$temp_data_clear = array(
								"f_paydate" => $this->tranfer2ADyear2($_POST['f_paydate'][$value]),
								"f_paymoney" => $f_apaymoney,
								"f_donedate" => '0000-00-00',
								"f_doneamount" => 0,
								"f_paylog" =>  $paylog,
							);
						}
                    }
                }
                
                if(!empty($this->tranfer2ADyear2($_POST['f_paydate'][$value])) && count($temp_data_clear) == 0)
                {
                    // update Table fine_import
                    $temp_data2 = array(
                        "f_paydate" => $f_paydate,
                        "f_paymoney" => $f_paymoney,
                        "f_donedate" => $f_donedate,
                        "f_doneamount" => $f_doneamount,
                        "f_paylog" => $paylog 
                    );
                }
				else
				{
					$temp_data2 = $temp_data_clear;
				}
				$this->getsqlmod->add_finepayport_data(null, $temp_data2, $value, $_POST['project_id']);

				$i++;
            }
            
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
    }
   
    public function editfplist2(){//支匯票update
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        foreach ($s_cnum1 as $key => $value) {
            $fine_rec = $this->getsqlmod->getfine_list2($value)->result();
            $fine = array(
                'f_payamount' => $fine_rec[0]->f_payamount + $_POST['fpart_amount'][$value],
                'f_paynowamount' => $_POST['f_paynowamount'][$value],
                'f_paytype' => $_POST['f_paytype'][$value],
                'f_fee' => $_POST['f_fee'][$value],
                'f_check_unit' => $_POST['f_check_unit'][$value],
                'f_check_receipt' => $_POST['f_check_receipt'][$value],
                'f_check_officenum' => $_POST['f_check_officenum'][$value],
                'f_check_checknum' => $_POST['f_check_checknum'][$value],
            );
            $fine_part = array(
                'fpart_amount' => $_POST['fpart_amount'][$value],
                'fpart_date' => date('Y-m-d'),
                'fpart_fpnum' => $value,
                'fpart_type' => '支匯票',
            );
            //var_dump($fine);
            //echo '<br>';
            $this->getsqlmod->updateFine_bank($value,$fine);
            if($_POST['fpart_amount'][$value]!=0)$this->getsqlmod->addFinePart_bank($fine_part);
        };
        redirect('Acc_cert/listfp2/'.$_POST['fp_no']); 
    }
    
    public function insertofficenum(){
        if(isset($_POST['fp_no']))
        {
            $this->getsqlmod->uploadfp_officenum($_POST['fp_no'],$_POST['fp_num']);
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        // 
        // redirect('Acc_cert/listfp1/'.$_POST['fp_no']); 
        
    }

    public function getFine_useful_source(){
        $query = $this->getsqlmod->getUsefulSource($_GET['source'])->result(); 
        
        echo json_encode(array('data' => $query));
        
    }
    
    public function complete(){
        $id = $this->uri->segment(3);
        

		$hasedit = $this->getsqlmod->checkFineProject_edit($id);
		if($hasedit > 0)
		{
			$this->getsqlmod->complete($id);
			redirect('Acc_cert/listFineProject/'); 
		}
		else
		{
			echo "<strong>尚未編輯，不可進行簽核</strong>";
		}
        
    }

    public function CPcomplete(){
        $id = $this->uri->segment(3);
        $this->getsqlmod->CPcomplete($id);
        redirect('Acc_cert/listFineCProject/'); 
    }
    
    public function downloadmemoxml(){
        //var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        $data = $this->getsqlmod->export_finePJ_lists($id)->result();
		$fp = $this->getsqlmod->getfineproject_ed($id)->result();
        $pay_tamount_sum = 0;
        $pay_tfpart_amount_sum = 0;
        $pay_tfee_sum = 0;
        $pay_tturnsub_sum = 0;
        $first_username = '';
        $bemove_first_username = '';
        $bemove_sum = 0;
        $fp_type = '';
        $turnsub_str = '';
        $name = array();
        $bemove_name = array();
        for ($i=0; $i < count($data); $i++) { 
            $pay_tamount_sum = ($pay_tamount_sum + $data[$i]->pay_tamount);
            $pay_tfpart_amount_sum = ($pay_tfpart_amount_sum + $data[$i]->fpart_amount);
            $pay_tfee_sum = ($pay_tfee_sum + $data[$i]->f_fee);
            $pay_tturnsub_sum = ($pay_tturnsub_sum + $data[$i]->f_turnsub);
            $first_username = $data[0]->f_username;
            $fp_type = $data[0]->fp_type;
            array_push($name, $data[$i]->f_username);
            if(isset($data[$i]->f_movelog) && !empty($data[$i]->f_movelog))
            {
                $bemove_sum++;
                array_push($bemove_name, $data[$i]->f_username);
                if($bemove_sum == 1)
                {
                    $bemove_first_username = $data[$i]->f_username;
                }
                
            }
        }
        $length = strlen($pay_tamount_sum);
        if($length >4){ 
            $payt =(substr($pay_tamount_sum,-4));
            $pay_tamount_sum =$pay_tamount_sum - $payt;
            $str = (floor($pay_tamount_sum * 0.001) * 0.1)."萬";
            $str1 = $str.number_format($payt);
        }
        else
        {
            $payt =(substr($pay_tamount_sum,-4));
            $str1 = number_format($payt);
        }

        $fpart_amount_length = strlen($pay_tfpart_amount_sum);
        if($length >4){ 
            $fpart_amount_payt =(substr($pay_tfpart_amount_sum,-4));
            $pay_tfpart_amount_sum =$pay_tfpart_amount_sum - $fpart_amount_payt;
            $fpart_amount_str = (floor($pay_tfpart_amount_sum * 0.001) * 0.1)."萬";
            $fpart_amount_str1 = $fpart_amount_str.number_format($fpart_amount_payt);
        }
        else
        {
            $fpart_amount_payt =(substr($pay_tfpart_amount_sum,-4));
            $fpart_amount_str1 = number_format($fpart_amount_payt);
        }

        $length_turnsub = strlen($pay_tturnsub_sum);
        if($length_turnsub >4){ 
            $turnsub_payt =(substr($pay_tturnsub_sum,-4));
            $pay_tturnsub_sum =$pay_tturnsub_sum - $turnsub_payt;
            $turnsub_str = (floor($pay_tturnsub_sum * 0.001) * 0.1)."萬";
            $turnsub_str1 = $turnsub_str.number_format($turnsub_payt);
        }
        else
        {
            $turnsub_payt =(substr($pay_tturnsub_sum,-4));
            $turnsub_str1 = number_format($turnsub_payt);
        }
        // $query = $this->getsqlmod->getfine_list($id); 
        // $count = $query->num_rows();
        // $su = $query->result();
        // if(!isset($su[0]->f_project_num)){
        //     redirect('Acc_cert/listfp1/'.$_POST['f_no']); 
        // }
        // $fp = $this->getsqlmod->getfineproject_ed($su[0]->f_project_num)->result(); 
        // $f_paynowamount =0;
        // foreach ($query->result() as $susp)
        // {
        //     $f_paynowamount=$f_paynowamount+$susp->f_paynowamount;
        // }
        // $length = strlen($f_paynowamount);
        // if($length >4){ 
        //     $payt =(substr($f_paynowamount,-4));
        //     $f_paynowamount =$f_paynowamount - $payt;
        //     $str = (floor($f_paynowamount * 0.001) * 0.1)."萬";
        //     $str1 = $str.$payt;
        // }
        //echo $str1;
        // $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
		$datet = $this->tranfer2RCyearTrad($fp[0]->fp_createdate);
        $dom = new DOMDocument("1.0","utf-8"); 
        $implementation = new DOMImplementation();
        $dom->appendChild($implementation->createDocumentType('便簽 SYSTEM "104_8_utf8.dtd" [<!ENTITY 表單 SYSTEM "便簽.sw" NDATA DI><!NOTATION DI SYSTEM ""><!NOTATION _X SYSTEM "">]')); 
        // display document in browser as plain text 
        // for readability purposes 
        header("Content-Type: text/plain"); 
         
        // create root element 
        $root = $dom->createElement("便簽"); 
        $dom->appendChild($root); 
        // create child element 
         
        $item = $dom->createElement("檔號"); 
        $root->appendChild($item); 
        
        $item2 = $dom->createElement("年度"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(date('Y')-1911); 
        $item2->appendChild($text); 
        $item2 = $dom->createElement("分類號"); 
        $item->appendChild($item2); 
        // $text = $dom->createTextNode($fp[0]->fp_office_num); 
        $text = $dom->createTextNode($data[0]->fp_office_num); 
        $item2->appendChild($text); 
        $item2 = $dom->createElement("案次號"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("1"); 
        $item2->appendChild($text); 
         
        $item = $dom->createElement("保存年限"); 
        $root->appendChild($item); 
        $text = $dom->createTextNode("3"); 
        $item->appendChild($text); 

        $item = $dom->createElement("稿面註記"); 
        $root->appendChild($item); 
         
        $item2 = $dom->createElement("擬辦方式"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("簽稿併陳"); 
        $item2->appendChild($text); 
        $item2 = $dom->createElement("決行層級"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("一層決行"); 
        $item2->appendChild($text); 
        $item2 = $dom->createElement("應用限制"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("限制開放"); 
        $item2->appendChild($text); 
        
        $item = $dom->createElement("於"); 
        $root->appendChild($item); 
        $text = $dom->createTextNode(""); 
        $item->appendChild($text); 
        
        $item = $dom->createElement("年月日"); 
        $root->appendChild($item); 
        $text = $dom->createTextNode($datet); 
        $item->appendChild($text); 
        
        $item = $dom->createElement("機關"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("全銜"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("毒品查緝中心"); 
        $item2->appendChild($text); 
        
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "一、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("本案係".$datet."本大隊".$fp_type."專戶收繳匯款核銷案。"); 
        $item3->appendChild($text); 
        
        // $str1 = 0;
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "二、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        // $text = $dom->createTextNode("案內義務人".$su[0]->s_name."等".$count."人匯款繳納罰鍰計新臺幣".$str1."元（詳如附件，繳納清冊）。"); 
        
        $text = $dom->createTextNode("案內義務人".$first_username."等".count(array_unique($name))."人匯款繳納罰鍰計新臺幣".$str1."元".(($pay_tturnsub_sum != 0)?"，其中".$fp_type."計".$fpart_amount_str1."元":"").(($pay_tfee_sum != 0)?"、執行費".$pay_tfee_sum."元":"").(($pay_tturnsub_sum != 0)?"、轉正".(($fp_type == '罰鍰')?'怠金':'罰鍰').$turnsub_str1."元":"")."（詳如附件，繳納清冊）。"); 
        $item3->appendChild($text); 

        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "三、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("另義務人".$bemove_first_username."等".count(array_unique($bemove_name))."人因逾繳納期限，已移送法務部行政執行署所屬分署執行（詳如附件，繳納清冊）。"); 
        $item3->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "四、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("奉核後，影送本大隊會計室".(($pay_tfee_sum != 0)?"及行政組":"")."辦理登記".(($pay_tturnsub_sum != 0)?"及轉正".(($fp_type == '罰鍰')?'怠金':'罰鍰'):"")."；另函法務部行政執行署所屬分署知照（敘稿如後）。"); 
        $item3->appendChild($text); 
        
        $item = $dom->createElement("敬陳"); 
        $root->appendChild($item); 
        $text = $dom->createTextNode("敬陳　大隊長"); 
        $item->appendChild($text); 
        // echo $dom->saveXML();  
        $this->load->helper('download');
        force_download($datet.(($fp[0]->fp_type == '罰鍰')?'a':'b').'.di', $dom->saveXML());
        redirect('Acc_cert/listFineProject/'); 
    }

    public function downloadmemoword(){
        redirect('Acc_cert/listFineProject/'); 
    }
    
    public function downloadofficexml(){
        //var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $username = $this -> session -> userdata('uname');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $query = $this->getsqlmod->getfine_list($id); 
        $query = $this->getsqlmod->get_finePJ_lists_officedoc_user_in_distinct($id);
        $count = $query->num_rows();
        $su = $query->result();
        // $fp = $this->getsqlmod->getfineproject_ed($su[0]->f_project_num)->result(); 
        $fp = $this->getsqlmod->getfineproject_ed($id)->result();
        // if(!isset($fp)) return false;
        //echo $str1;
        // $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
        $datet = $this->tranfer2RCyearTrad($fp[0]->fp_createdate);
        //$xmls[];
        $i = 0;
        foreach ($query->result() as $susp)
        {
            
            $dom = new DOMDocument("1.0","utf-8"); 
            $implementation = new DOMImplementation();
            $dom->appendChild($implementation->createDocumentType('函 SYSTEM "104_8_utf8.dtd" [<!ENTITY 表單 SYSTEM "函.sw" NDATA DI><!NOTATION DI SYSTEM ""><!NOTATION _X SYSTEM "">]')); 
            // display document in browser as plain text 
            // for readability purposes 
            header("Content-Type: text/plain"); 
            // create root element 
            $root = $dom->createElement("函"); 
            $dom->appendChild($root); 
            // create child element 
             
            $item = $dom->createElement("檔號"); 
            $root->appendChild($item); 
            
            $item2 = $dom->createElement("年度"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode(date('Y')-1911); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("分類號"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("07270399");
			// $text = $dom->createTextNode(""); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("案次號"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("1"); 
            $item2->appendChild($text); 
             
            $item = $dom->createElement("保存年限"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("3"); 
            $item->appendChild($text); 

            $item = $dom->createElement("稿面註記"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("擬辦方式"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("簽稿併陳"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("決行層級"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("一層決行"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("應用限制"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("限制開放"); 
            $item2->appendChild($text); 
                    
            $item = $dom->createElement("發文機關"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("全銜"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("毒品查緝中心"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("機關代碼"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("379130200C"); 
            $item2->appendChild($text); 
            
            $item = $dom->createElement("函類別"); 
            $item->setAttribute("代碼", "函");  
            
            $item = $dom->createElement("地址"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("10042臺北市中正區武昌街一段69號"); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("承辦人：".$username); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("電話：(02)2393-2397"); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("傳真：(02)2393-2311"); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("電子信箱：equalszerow@tcpd.gov.tw"); 
            $item->appendChild($text); 

            $item = $dom->createElement("受文者"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("交換表"); 
            $item2->setAttribute("交換表單", "表單");  
            $item->appendChild($item2); 

            $item = $dom->createElement("發文日期"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("年月日"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode($datet); 
            $item2->appendChild($text); 
            
            $item = $dom->createElement("發文字號"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("字"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("北市警刑大毒緝"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("文號"); 
            $item->appendChild($item2); 
            $item3 = $dom->createElement("年度"); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode((date('Y')-1911)); 
            $item3->appendChild($text); 
            $item3 = $dom->createElement("流水號"); 
            $item2->appendChild($item3); 
            // $text = $dom->createTextNode($susp->f_project_num); //日後再確認
            // $text = $dom->createTextNode($susp->f_project_id);
            // $item3->appendChild($text); 
            // $item3 = $dom->createElement("支號"); 
            // $item2->appendChild($item3); 
            
            $item = $dom->createElement("速別"); 
            $item->setAttribute("代碼", "普通件");  
            $root->appendChild($item); 
            
            $item = $dom->createElement("密等及解密條件或保密期限"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("密等"); 
            $item->appendChild($item2); 
            $item2 = $dom->createElement("解密條件或保密期限"); 
            $item->appendChild($item2); 
            $item = $dom->createElement("附件"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("文字"); 
            $item->appendChild($item2); 
            
            
            $data = $this->getsqlmod->get_finePJ_lists_officedoc_user_detail($susp->fp_no, $susp->f_userid)->result();

            $move_no = array();
            $muticase_payamount = array();
            $amount = 0;
            $payamount = 0;
            $grandamount = 0;
            $fee = 0;
            $balance = 0;
            $casenum = 0;
            foreach ($data as $user) {
                $username = $user->username;
                $fullusername = $user->fullusername;
                $fp_type = $user->fine_type;
                // $depart = $user->f_movelog_depart;
				$calcmovelog = explode("\n", $user->calc_movelog);
				$temp_moveday = 0;
				$new_movelog = '';
				$new_movedepart = '';
				foreach ($calcmovelog as $key => $value) {
					$movedate = explode('_', $value)[0];

					if(($movedate * 1) > $temp_moveday)
					{
						$temp_moveday = ($movedate * 1);
						$new_movelog = explode('_', $value)[1];
						$new_movedepart = explode('_', $value)[2];
					}
						
				}
                // array_push($move_no, $user->f_movelog); 
				$depart = $new_movedepart;
				array_push($move_no, $new_movelog);
                $userid = $user->f_userid;
                $payway = $user->fpart_type;
                $source = (($user->f_source)?(($user->f_source == '義務人')?$user->f_source:((trim(explode('_',$user->f_source)[0]) == '機關')?'貴分署代義務人':((trim(explode('_',$user->f_source)[0]) == '銀行')?((explode('_', $user->f_source)[1] == NULL)?'':explode('_', $user->f_source)[1]) . explode('_', $user->f_source)[2]:explode('_', $user->f_source)[2]))):'');
                $amount = ($amount + $user->amount);
                $payamount = ($payamount+ $user->totalnowamount);
                $grandamount = ($grandamount+ $user->grandamount);
                $fee = ($fee+ $user->f_fee);
                $balance = ($balance+ $user->balance);
				
                array_push($muticase_payamount, array('userid'=>$userid, 'data' => array(
                    'fine_type'=>$user->fine_type,
                    'move_no'=>$new_movelog,
                    'fee' => $user->f_fee,
                    'amount'=>$user->amount,
                    'payamount'=>((($user->nowamount / 10000) >= 1)? (floor($user->nowamount / 10000) . '萬'):'') . ((($user->nowamount / 10000) >= 1 && ($user->nowamount % 10000) > 0)?(substr(number_format(($user->nowamount % 10000)+10000),1,5)):((($user->nowamount % 10000) == 0)?'':number_format(($user->nowamount % 10000)))) . '元', 
                    'grandamount'=>((($user->grandamount / 10000) >= 1)? (floor($user->grandamount / 10000) . '萬'):'') . ((($user->grandamount / 10000) >= 1 && ($user->grandamount % 10000) > 0)?(substr(number_format(($user->grandamount % 10000)+10000),1,5)):((($user->grandamount % 10000) == 0)?'':number_format(($user->grandamount % 10000)))) . '元',
                    'balance'=>$user->balance, 
                    'balancestr'=>((($user->balance / 10000) >= 1)? (floor($user->balance / 10000) . '萬'):'') . ((($user->balance / 10000) >= 1 && ($user->balance % 10000) > 0)?(substr(number_format(($user->balance % 10000)+10000),1,5)):((($user->balance % 10000) == 0)?'':number_format(($user->balance % 10000)))) . '元'
				)));

                $casenum++;
				
            }
            $amountstr = ((($amount / 10000) >= 1)? (floor($amount / 10000) . '萬'):'') . ((($amount / 10000) >= 1 && ($amount % 10000) > 0)?(substr(number_format(($amount % 10000)+10000),1,5)):((($amount % 10000) == 0)?'':number_format(($amount % 10000)))) . '元';
            $payamountstr = ((($payamount / 10000) >= 1)? (floor($payamount / 10000) . '萬'):'') . ((($payamount / 10000) >= 1 && ($payamount % 10000) > 0)?(substr(number_format(($payamount % 10000)+10000),1,5)):((($payamount % 10000) == 0)?'':number_format(($payamount % 10000)))) . '元';
            $grandamountstr = ((($grandamount / 10000) >= 1)? (floor($grandamount / 10000) . '萬'):'') . ((($grandamount / 10000) >= 1 && ($grandamount % 10000) > 0)?(substr(number_format(($grandamount % 10000)+10000),1,5)):((($grandamount % 10000) == 0)?'':number_format(($grandamount % 10000)))) . '元';
            $balancetstr = ((($balance / 10000) >= 1)? (floor($balance / 10000) . '萬'):'') . ((($balance / 10000) >= 1 && ($balance % 10000) > 0)?(substr(number_format(($balance % 10000)+10000),1,5)):((($balance % 10000) == 0)?'':number_format(($balance % 10000)))) . '元';

            
            $type_A = 0;
            $type_B = 0;
			$type_A_name = '罰鍰';
			$type_B_name = '怠金';
            $type_A_sum = 0;
            $type_b_sum = 0;
            $type_A_sumstr = '';
            $type_B_sumstr = '';
            if($casenum > 1)
            {
                for ($i=0; $i < count($muticase_payamount); $i++) 
                {
                    if($muticase_payamount[$i]['data']['fine_type'] != '罰鍰')
                    {
                        $type_B++; //異項
                        $type_b_sum = $type_b_sum + $muticase_payamount[$i]['data']['amount'];
                    }
                    else 
                    {
                        $type_A++; //同項
                        $type_A_sum = $type_A_sum + $muticase_payamount[$i]['data']['amount'];
                        // $fp_type_title = $fp_type;
						
                    }
                    $fp_type_title = $muticase_payamount[$i]['data']['fine_type'];
                } 
                $type_A_sumstr = ((($type_A_sum / 10000) >= 1)? (floor($type_A_sum / 10000) . '萬'):'') . ((($type_A_sum / 10000) >= 1 && ($type_A_sum % 10000) > 0)?(substr(number_format(($type_A_sum % 10000)+10000),1,5)):((($type_A_sum % 10000) == 0)?'':number_format(($type_A_sum % 10000)))) . '元';
                $type_B_sumstr = ((($type_b_sum / 10000) >= 1)? (floor($type_b_sum / 10000) . '萬'):'') . ((($type_b_sum / 10000) >= 1 && ($type_b_sum % 10000) > 0)?(substr(number_format(($type_b_sum % 10000)+10000),1,5)):((($type_b_sum % 10000) == 0)?'':number_format(($type_b_sum % 10000)))) . '元';

				$text = $dom->createTextNode("毒品".(($type_A > 0 && $type_B > 0)?"罰鍰暨怠金":$fp_type_title)."收繳登記簿、案件處分書及移送書"); 
				$item2->appendChild($text); 
				$item = $dom->createElement("主旨"); 
				$root->appendChild($item); 
				$item2 = $dom->createElement("文字"); 
				$item->appendChild($item2); 
                $text = $dom->createTextNode("有關義務人".$username.(($type_A > 0 && $type_B > 0)?"罰鍰暨怠金":$fp_type_title)."繳納情形，詳如說明二，請查照。"); 
                $item2->appendChild($text); 
            }
            else
            {
				$text = $dom->createTextNode("毒品".$fp_type."收繳登記簿、案件處分書及移送書"); 
				$item2->appendChild($text); 
				$item = $dom->createElement("主旨"); 
				$root->appendChild($item); 
				$item2 = $dom->createElement("文字"); 
				$item->appendChild($item2); 
                // $text = $dom->createTextNode("有關義務人".$username.$fp_type."繳納情形，詳如說明二，請查照。"); 
				$text = $dom->createTextNode("有關義務人".$username.$fp_type."繳納情形，詳如說明二，請查照。"); 
                $item2->appendChild($text); 
            }
            
            $item = $dom->createElement("段落"); 
            $item->setAttribute("段名", "說明：");  
            $root->appendChild($item); 
            $item2 = $dom->createElement("文字"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode(""); 
            $item2->appendChild($text); 
            
            $item2 = $dom->createElement('條列'); 
            $item2->setAttribute("序號", "一、");  
            $item->appendChild($item2); 
            $item3 = $dom->createElement('文字'); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode("相關移送案號：".join("、",$move_no)."。");
            $item3->appendChild($text); 

            if($casenum > 1)
            {
                $item2 = $dom->createElement('條列'); 
                $item2->setAttribute("序號", "二、");  
                $item->appendChild($item2); 
                $item3 = $dom->createElement('文字'); 
                $item2->appendChild($item3); 
                // $text = $dom->createTextNode("義務人".$susp->s_name."（身分證統一編號：".$su[0]->s_ic."）違反毒品危害防制條例案，經本局裁處罰鍰新臺幣（以下同）".$susp->f_damount/10000 ."萬元，已移送貴分署執行。經義務人於".$datet."以金融匯款繳納".$susp->f_paynowamount."元，累計繳納".$str."元。"); 
                $text = $dom->createTextNode("義務人".$fullusername."（身分證統一編號：".$userid."）違反毒品危害防制條例案，經本局裁處".(($type_A > 0 )?($type_A_name.$type_A):($type_B_name.$type_B))."件計新臺幣（以下同）".(($type_A > 0)?$type_A_sumstr:$type_B_sumstr). (($type_A > 0 && $type_B > 0)?('、怠金'.$type_B. '件' .$type_B_sumstr):'') ."，已移送貴分署執行。經".$source."於".$datet."以".$payway."繳納".$payamountstr."，核銷情形如下：");
                $item3->appendChild($text); 

                $ch_num = array('一','二','三','四','五','六','七', '八','九','十');
                for ($i=0; $i < count($muticase_payamount); $i++) { 
                    $item4 = $dom->createElement('文字'); 
                    $item2->appendChild($item4);
                    $text1 = $dom->createTextNode("(".$ch_num[$i].")".$muticase_payamount[$i]['data']['fine_type']."（移送案號：".$muticase_payamount[$i]['data']['move_no']."）核銷".$muticase_payamount[$i]['data']['payamount']."，累計繳納".$muticase_payamount[$i]['data']['grandamount'].(($muticase_payamount[$i]['data']['fee'] > 0)?"（另收取執行費".$muticase_payamount[$i]['data']['fee']."元）":"")."，".(($muticase_payamount[$i]['data']['balance'] == 0)?"本案已清償":("未納餘額".$muticase_payamount[$i]['data']['balancestr']))."。");
                    $item4->appendChild($text1); 
                }
                
            }
            else
            {
                switch ($susp-> listtype ) {
                    case '完納':
                    case '分期完納':
                        $item2 = $dom->createElement('條列'); 
                        $item2->setAttribute("序號", "二、");  
                        $item->appendChild($item2); 
                        $item3 = $dom->createElement('文字'); 
                        $item2->appendChild($item3); 
                        // $text = $dom->createTextNode("義務人".$susp->s_name."（身分證統一編號：".$su[0]->s_ic."）違反毒品危害防制條例案，經本局裁處罰鍰新臺幣（以下同）".$susp->f_damount/10000 ."萬元，已移送貴分署執行。經義務人於".$datet."以金融匯款繳納".$susp->f_paynowamount."元，累計繳納".$str."元。"); 
                        $text = $dom->createTextNode("義務人".$fullusername."（身分證統一編號：".$userid."）違反毒品危害防制條例案，經本局裁處".$fp_type."新臺幣（以下同）".$amountstr ."，已移送貴分署執行。經".$source."於".$datet."以".$payway."繳納".$payamountstr."，累計繳納".$grandamountstr.(($fee > 0)?"（另收取執行費".$fee."元）":"")."，本案已清償。");
                        $item3->appendChild($text); 
                        break;
                    case '分期':
                        $item2 = $dom->createElement('條列'); 
                        $item2->setAttribute("序號", "二、");  
                        $item->appendChild($item2); 
                        $item3 = $dom->createElement('文字'); 
                        $item2->appendChild($item3); 
                        // $text = $dom->createTextNode("義務人".$susp->s_name."（身分證統一編號：".$su[0]->s_ic."）違反毒品危害防制條例案，經本局裁處罰鍰新臺幣（以下同）".$susp->f_damount/10000 ."萬元，已移送貴分署執行。經義務人於".$datet."以金融匯款繳納".$susp->f_paynowamount."元，累計繳納".$str."元。"); 
                        $text = $dom->createTextNode("義務人".$fullusername."（身分證統一編號：".$userid."）違反毒品危害防制條例案，經本局裁處".$fp_type."新臺幣（以下同）".$amountstr ."，已移送貴分署執行。經".$source."於".$datet."以".$payway."繳納".$payamountstr."，累計繳納".$grandamountstr.(($fee > 0)?"（另收取執行費".$fee."元）":"")."，未納餘額".$balancetstr."。");
                        $item3->appendChild($text); 
                        break;
                    default:
                        break;
                }
            }
            
            
            
            $item2 = $dom->createElement('條列'); 
            $item2->setAttribute("序號", "三、");  
            $item->appendChild($item2); 
            $item3 = $dom->createElement('文字'); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode("有關本案繳款收據，依行政院函頒「各機關預算財務收支處理注意事項」規定因於金融匯款時已取得收據，故本大隊得不另掣發。"); 
            $item3->appendChild($text); 
            
            // $item2 = $dom->createElement('條列'); 
            // $item2->setAttribute("序號", "三、");  
            // $item->appendChild($item2); 
            // $item3 = $dom->createElement('文字'); 
            // $item2->appendChild($item3); 
            // $text = $dom->createTextNode("奉核後，影送本大隊會計室辦理登記；另函法務部行政執行署所屬分署知照（敘稿如後）。"); 
            // $item3->appendChild($text); 
            
            $item = $dom->createElement("正本"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("敬陳　大隊長"); 
            $item->appendChild($text); 
            $item2 = $dom->createElement('全銜'); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("法務部行政執行署".$depart."分署"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement('副本'); 
            $item->appendChild($item2); 
            $item2 = $dom->createElement('署名'); 
            $item->appendChild($item2); 

            //echo $dom->saveXML();  
            $xmls[] = $dom->saveXML();  

            $i++;
        }
        $this->load->library('zip');
        //var_dump($xmls);
        $i=0;
        foreach ($xmls as $key) {
            $i++;
            $name = $i.'.di';
            $data = $key;
            $this->zip->add_data($name, $data);
        }
        //$this->zip->archive('temp/'.$datet.'.zip');
        $this->zip->download($datet.(($fp[0]->fp_type == '罰鍰')?'A':'D').'.zip');
        unlink($datet.(($fp[0]->fp_type == '罰鍰')?'A':'D') .'.zip');
        //$this->load->helper('download');
        //force_download($datet.'.di', $dom->saveXML());
        redirect('Acc_cert/listFineProject/'); 
    }

    public function downloadticketofficexml(){
        //var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $username = $this -> session -> userdata('uname');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $query = $this->getsqlmod->getfine_list($id); 
        $query = $this->getsqlmod->get_finePJ_lists_ticketofficedoc_user_in_distinct($id);
        $count = $query->num_rows();
        $su = $query->result();
        // $fp = $this->getsqlmod->getfineproject_ed($su[0]->f_project_num)->result(); 
        $fp = $this->getsqlmod->getfineproject_ed($id)->result();
        
        //echo $str1;
        // $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
		$datet = $this->tranfer2RCyearTrad($fp[0]->fp_createdate);
        //$xmls[];
        $i = 0;
        foreach ($query->result() as $susp)
        {
            $data = $this->getsqlmod->get_finePJ_lists_ticketofficedoc_user_detail($susp->fp_no, $susp->f_ticket_no)->result();

            $move_no = array();
            $muticase_payamount = array();
            $amount = 0;
            $payamount = 0;
            $grandamount = 0;
            $fee = 0;
            $balance = 0;
            $casenum = 0;
            
            foreach ($data as $user) {
                $username = $user->username;
                $fullusername = $user->fullusername;
                $fp_type = $user->fine_type;
                // $depart = (isset($user->f_movelog)?explode('_', $user->f_movelog)[1]:"");
                // array_push($move_no, explode('_', $user->f_movelog)[0]);
				$calcmovelog = explode("\n", $user->calc_movelog);
				$temp_moveday = 0;
				$new_movelog = '';
				$new_movedepart = '';
				foreach ($calcmovelog as $key => $value) {
					$movedate = explode('_', $value)[0];

					if(($movedate * 1) > $temp_moveday)
					{
						$temp_moveday = ($movedate * 1);
						$new_movelog = explode('_', $value)[1];
						$new_movedepart = explode('_', $value)[2];
					}
						
				}
				$depart = $new_movedepart;
				array_push($move_no, $new_movelog);

                $userid = $user->f_userid;
                $payway = $user->fpart_type;
				
                $source = (($user->f_source)?(($user->f_source == '義務人')?$user->f_source:((trim(explode('_',$user->f_source)[0]) == '機關' || trim(explode('_',$user->f_source)[0]) == '銀行')?((explode('_', $user->f_source)[1] == NULL)?'':explode('_', $user->f_source)[1]) . explode('_', $user->f_source)[2]:explode('_', $user->f_source)[2])):'');
                $officenum = $user->f_office_num;
                $ticketno = $user->f_ticket_no;
                $amount = ($amount + $user->amount);
                $payamount = ($payamount+ $user->totalnowamount);
                $grandamount = ($grandamount+ $user->grandamount);
                $fee = ($fee+ $user->f_fee);
                $balance = ($balance+ $user->balance);
                array_push($muticase_payamount, array('userid'=>$userid, 'data' => array(
                    'fine_type'=>$user->fine_type,
                    'move_no'=>$new_movelog,
                    'fee' => $user->f_fee,
                    'amount'=>$user->amount,
                    'payamount'=>((($user->nowamount / 10000) >= 1)? (floor($user->nowamount / 10000) . '萬'):'') . ((($user->nowamount / 10000) >= 1 && ($user->nowamount % 10000) > 0)?(substr(number_format(($user->nowamount % 10000)+10000),1,5)):((($user->nowamount % 10000) == 0)?'':number_format(($user->nowamount % 10000)))) . '元', 
                    'grandamount'=>((($user->grandamount / 10000) >= 1)? (floor($user->grandamount / 10000) . '萬'):'') . ((($user->grandamount / 10000) >= 1 && ($user->grandamount % 10000) > 0)?(substr(number_format(($user->grandamount % 10000)+10000),1,5)):((($user->grandamount % 10000) == 0)?'':number_format(($user->grandamount % 10000)))) . '元',
                    'balance'=>$user->balance, 
                    'balancestr'=>((($user->balance / 10000) >= 1)? (floor($user->balance / 10000) . '萬'):'') . ((($user->balance / 10000) >= 1 && ($user->balance % 10000) > 0)?(substr(number_format(($user->balance % 10000)+10000),1,5)):((($user->balance % 10000) == 0)?'':number_format(($user->balance % 10000)))) . '元'
				)));

                $casenum++;
				
            }
            $amountstr = ((($amount / 10000) >= 1)? (floor($amount / 10000) . '萬'):'') . ((($amount / 10000) >= 1 && ($amount % 10000) > 0)?(substr(number_format(($amount % 10000)+10000),1,5)):((($amount % 10000) == 0)?'':number_format(($amount % 10000)))) . '元';
            $payamountstr = ((($payamount / 10000) >= 1)? (floor($payamount / 10000) . '萬'):'') . ((($payamount / 10000) >= 1 && ($payamount % 10000) > 0)?(substr(number_format(($payamount % 10000)+10000),1,5)):((($payamount % 10000) == 0)?'':number_format(($payamount % 10000)))) . '元';
            $grandamountstr = ((($grandamount / 10000) >= 1)? (floor($grandamount / 10000) . '萬'):'') . ((($grandamount / 10000) >= 1 && ($grandamount % 10000) > 0)?(substr(number_format(($grandamount % 10000)+10000),1,5)):((($grandamount % 10000) == 0)?'':number_format(($grandamount % 10000)))) . '元';
            $balancetstr = ((($balance / 10000) >= 1)? (floor($balance / 10000) . '萬'):'') . ((($balance / 10000) >= 1 && ($balance % 10000) > 0)?(substr(number_format(($balance % 10000)+10000),1,5)):((($balance % 10000) == 0)?'':number_format(($balance % 10000)))) . '元';

            
            
            $dom = new DOMDocument("1.0","utf-8"); 
            $implementation = new DOMImplementation();
            $dom->appendChild($implementation->createDocumentType('函 SYSTEM "104_8_utf8.dtd" [<!ENTITY 表單 SYSTEM "函.sw" NDATA DI><!NOTATION DI SYSTEM ""><!NOTATION _X SYSTEM "">]')); 
            // display document in browser as plain text 
            // for readability purposes 
            header("Content-Type: text/plain"); 
            // create root element 
            $root = $dom->createElement("函"); 
            $dom->appendChild($root); 
            // create child element 
             
            $item = $dom->createElement("檔號"); 
            $root->appendChild($item); 
            
            $item2 = $dom->createElement("年度"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode(date('Y')-1911); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("分類號"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("07270399"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("案次號"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("1"); 
            $item2->appendChild($text); 
             
            $item = $dom->createElement("保存年限"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("3"); 
            $item->appendChild($text); 

            $item = $dom->createElement("稿面註記"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("擬辦方式"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("以稿代簽  "); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("決行層級"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("一層決行  "); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("應用限制"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("限制開放"); 
            $item2->appendChild($text); 
                    
            $item = $dom->createElement("發文機關"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("全銜"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("毒品查緝中心"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("機關代碼"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("379130200C"); 
            $item2->appendChild($text); 
            
            $item = $dom->createElement("函類別"); 
            $item->setAttribute("代碼", "函");  
            
            $item = $dom->createElement("地址"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("10042臺北市中正區武昌街一段69號"); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("承辦人：".$username); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("電話：(02)2393-2397"); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("傳真：(02)2393-2311"); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("電子信箱：equalszerow@tcpd.gov.tw"); 
            $item->appendChild($text); 

            $item = $dom->createElement("受文者"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("交換表"); 
            $item2->setAttribute("交換表單", "表單");  
            $item->appendChild($item2); 

            $item = $dom->createElement("發文日期"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("年月日"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode($datet); 
            $item2->appendChild($text); 
            
            $item = $dom->createElement("發文字號"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("字"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("北市警刑大毒緝"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("文號"); 
            $item->appendChild($item2); 
            $item3 = $dom->createElement("年度"); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode((date('Y')-1911)); 
            $item3->appendChild($text); 
            $item3 = $dom->createElement("流水號"); 
            $item2->appendChild($item3); 
            // $text = $dom->createTextNode($susp->f_project_num); //日後再確認
            // $text = $dom->createTextNode($susp->f_project_id);
            // $item3->appendChild($text); 
            // $item3 = $dom->createElement("支號"); 
            // $item2->appendChild($item3); 
            
            $item = $dom->createElement("速別"); 
            $item->setAttribute("代碼", "普通件");  
            $root->appendChild($item); 
            
            $item = $dom->createElement("密等及解密條件或保密期限"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("密等"); 
            $item->appendChild($item2); 
            $item2 = $dom->createElement("解密條件或保密期限"); 
            $item->appendChild($item2); 
            $item = $dom->createElement("附件"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("文字"); 
            $item->appendChild($item2); 
             
            $type_A = 0;
            $type_B = 0;
			$type_A_name = '罰鍰';
			$type_B_name = '怠金';
            $type_A_sum = 0;
            $type_b_sum = 0;
            $type_A_sumstr = '';
            $type_B_sumstr = '';
            $fp_type_title = '';
            if($casenum > 1)
            {
                for ($i=0; $i < count($muticase_payamount); $i++) 
                {
                    if($muticase_payamount[$i]['data']['fine_type'] != "罰鍰")
                    {
                        $type_B++; // 異項
                        $type_b_sum = $type_b_sum + $muticase_payamount[$i]['data']['amount'];
                        
                    }
                    else 
                    {
                        $type_A++; // 同項
                        $type_A_sum = $type_A_sum + $muticase_payamount[$i]['data']['amount'];
                        
                    }
                    $fp_type_title = $muticase_payamount[$i]['data']['fine_type'];
                } 
                $type_A_sumstr = ((($type_A_sum / 10000) >= 1)? (floor($type_A_sum / 10000) . '萬'):'') . ((($type_A_sum / 10000) >= 1 && ($type_A_sum % 10000) > 0)?(substr(number_format(($type_A_sum % 10000)+10000),1,5)):((($type_A_sum % 10000) == 0)?'':number_format(($type_A_sum % 10000)))) . '元';
                $type_B_sumstr = ((($type_b_sum / 10000) >= 1)? (floor($type_b_sum / 10000) . '萬'):'') . ((($type_b_sum / 10000) >= 1 && ($type_b_sum % 10000) > 0)?(substr(number_format(($type_b_sum % 10000)+10000),1,5)):((($type_b_sum % 10000) == 0)?'':number_format(($type_b_sum % 10000)))) . '元';
				

                $text = $dom->createTextNode("毒品".(($type_A > 0 && $type_B > 0)?"罰鍰暨怠金":$fp_type_title)."收繳登記簿、案件處分書及移送書".(($payway == "支票")?"及收據1紙":"各1份")); 
                $item2->appendChild($text); 
                
                $item = $dom->createElement("主旨"); 
                $root->appendChild($item); 
                $item2 = $dom->createElement("文字"); 
                $item->appendChild($item2);

                $text = $dom->createTextNode("有關義務人".$username.(($type_A > 0 && $type_B > 0)?"罰鍰暨怠金":$fp_type_title)."繳納情形，詳如說明二，請查照。"); 
                $item2->appendChild($text); 
            }
            else
            {
                $text = $dom->createTextNode("毒品".$fp_type."收繳登記簿、案件處分書及移送書".(($payway == "支票")?"及收據1紙":"各1份")); 
                $item2->appendChild($text); 
                
                $item = $dom->createElement("主旨"); 
                $root->appendChild($item); 
                $item2 = $dom->createElement("文字"); 
                $item->appendChild($item2);

                $text = $dom->createTextNode("有關義務人".$username.$fp_type."繳納情形，詳如說明二，請查照。"); 
                $item2->appendChild($text); 
            }
            
            
            $item = $dom->createElement("段落"); 
            $item->setAttribute("段名", "說明：");  
            $root->appendChild($item); 
            $item2 = $dom->createElement("文字"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode(""); 
            $item2->appendChild($text); 
            
            $item2 = $dom->createElement('條列'); 
            $item2->setAttribute("序號", "一、");  
            $item->appendChild($item2); 
            $item3 = $dom->createElement('文字'); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode("相關移送案號：".join("、",$move_no)."。");
            $item3->appendChild($text); 

            if($casenum > 1)
            {
                $item2 = $dom->createElement('條列'); 
                $item2->setAttribute("序號", "二、");  
                $item->appendChild($item2); 
                $item3 = $dom->createElement('文字'); 
                $item2->appendChild($item3); 
                // $text = $dom->createTextNode("義務人".$susp->s_name."（身分證統一編號：".$su[0]->s_ic."）違反毒品危害防制條例案，經本局裁處罰鍰新臺幣（以下同）".$susp->f_damount/10000 ."萬元，已移送貴分署執行。經義務人於".$datet."以金融匯款繳納".$susp->f_paynowamount."元，累計繳納".$str."元。"); 
                $text = $dom->createTextNode("義務人".$fullusername."（身分證統一編號：".$userid."）違反毒品危害防制條例案，經本局裁處".(($type_A > 0 )?($type_A_name.$type_A):($type_B_name.$type_B))."件計新臺幣（以下同）".(($type_A > 0)?$type_A_sumstr:$type_B_sumstr). (($type_A > 0 && $type_B > 0)?('、怠金'.$type_B. '件' .$type_B_sumstr):'') ."，已移送貴分署執行。經". (($source == '義務人')?$source."開立繳款": $source . "依執行命令開立繳款").$payway."1張（票號：".$ticketno."），面額".$payamountstr."，核銷情形如下：");
                $item3->appendChild($text); 

                $ch_num = array('一','二','三','四','五','六','七', '八','九','十');
                for ($i=0; $i < count($muticase_payamount); $i++) { 
                    $item4 = $dom->createElement('文字'); 
                    $item2->appendChild($item4);
                    $text1 = $dom->createTextNode("(".$ch_num[$i].")".$muticase_payamount[$i]['data']['fine_type']."（移送案號：".$muticase_payamount[$i]['data']['move_no']."）核銷".$muticase_payamount[$i]['data']['payamount']."，累計繳納".$muticase_payamount[$i]['data']['grandamount'].(($muticase_payamount[$i]['data']['fee'] > 0)?"（另收取執行費".$muticase_payamount[$i]['data']['fee']."元）":"")."，".(($muticase_payamount[$i]['data']['balance'] == 0)?"本案已清償":("未納餘額".$muticase_payamount[$i]['data']['balancestr']))."。");
                    $item4->appendChild($text1); 
                }
                
            }
            else
            {
                switch ($susp-> listtype ) {
                    case '完納':
                    case '分期完納':
                        $item2 = $dom->createElement('條列'); 
                        $item2->setAttribute("序號", "二、");  
                        $item->appendChild($item2); 
                        $item3 = $dom->createElement('文字'); 
                        $item2->appendChild($item3); 
                        // $text = $dom->createTextNode("義務人".$susp->s_name."（身分證統一編號：".$su[0]->s_ic."）違反毒品危害防制條例案，經本局裁處罰鍰新臺幣（以下同）".$susp->f_damount/10000 ."萬元，已移送貴分署執行。經義務人於".$datet."以金融匯款繳納".$susp->f_paynowamount."元，累計繳納".$str."元。"); 
                        $text = $dom->createTextNode("義務人".$fullusername."（身分證統一編號：".$userid."）違反毒品危害防制條例案，經本局裁處".$fp_type."新臺幣（以下同）".$amountstr ."，已移送貴分署執行。經". (($source == '義務人')?$source."開立繳款": $source . "依執行命令開立繳款").$payway."1張（票號：".$ticketno."），面額".$payamountstr."，累計繳納".$grandamountstr.(($fee > 0)?"（另收取執行費".$fee."元）":"")."，本案已清償。");
                        $item3->appendChild($text); 
                        break;
                    case '分期':
                        $item2 = $dom->createElement('條列'); 
                        $item2->setAttribute("序號", "二、");  
                        $item->appendChild($item2); 
                        $item3 = $dom->createElement('文字'); 
                        $item2->appendChild($item3);
                        // $text = $dom->createTextNode("義務人".$susp->s_name."（身分證統一編號：".$su[0]->s_ic."）違反毒品危害防制條例案，經本局裁處罰鍰新臺幣（以下同）".$susp->f_damount/10000 ."萬元，已移送貴分署執行。經義務人於".$datet."以金融匯款繳納".$susp->f_paynowamount."元，累計繳納".$str."元。"); 

                        $text = $dom->createTextNode("義務人".$fullusername."（身分證統一編號：".$userid."）違反毒品危害防制條例案，經本局裁處".$fp_type."新臺幣（以下同）".$amountstr ."，已移送貴分署執行。經". (($source == '義務人')?$source."開立繳款": $source . "依執行命令開立繳款").$payway."1張（票號：".$ticketno."），面額".$payamountstr."，累計繳納".$grandamountstr.(($fee > 0)?"（另收取執行費".$fee."元）":"")."，未納餘額".$balancetstr."。");
                        $item3->appendChild($text); 
                        break;
                    default:
                        break;
                }
            }
            
            
            if($payway != '支票')
            {
                $item2 = $dom->createElement('條列'); 
                $item2->setAttribute("序號", "三、");  
                $item->appendChild($item2); 
                $item3 = $dom->createElement('文字'); 
                $item2->appendChild($item3); 
                $text = $dom->createTextNode("另本案義務人採郵政匯票繳款，購買郵政匯票時已取得郵局(金融機構)收據，依財政收支相關規定，得免掣發收款收據。"); 
                $item3->appendChild($text); 
            }
            
            
            // $item2 = $dom->createElement('條列'); 
            // $item2->setAttribute("序號", "三、");  
            // $item->appendChild($item2); 
            // $item3 = $dom->createElement('文字'); 
            // $item2->appendChild($item3); 
            // $text = $dom->createTextNode("奉核後，影送本大隊會計室辦理登記；另函法務部行政執行署所屬分署知照（敘稿如後）。"); 
            // $item3->appendChild($text); 
            
            $item = $dom->createElement("正本"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("敬陳　大隊長"); 
            $item->appendChild($text); 
            $item2 = $dom->createElement('全銜'); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("法務部行政執行署".$depart."分署"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement('副本'); 
            $item->appendChild($item2); 
            $item2 = $dom->createElement('署名'); 
            $item->appendChild($item2); 

            //echo $dom->saveXML();  
            $xmls[] = $dom->saveXML();  

            $i++;
        }
        $this->load->library('zip');
        //var_dump($xmls);
        $i=0;
        foreach ($xmls as $key) {
            $i++;
            $name = $i.'.di';
            $data = $key;
            $this->zip->add_data($name, $data);
        }
        //$this->zip->archive('temp/'.$datet.'.zip');
        $this->zip->download($datet.'C'.'.zip');
        unlink($datet.'C'.'.zip');
        //$this->load->helper('download');
        //force_download($datet.'.di', $dom->saveXML());
        redirect('Acc_cert/listFineProject/'); 
    }
    
    public function exportCSV(){
        $this->load->dbutil();
        $this->load->helper('download');
        $id = $this->uri->segment(3);
        $filename = '罰鍰清冊'.date('Ymd').'.csv';
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; "); 
        $usersData = $this->getsqlmod->getfine_list($id)->result();
        
        $file = fopen('php://output', 'w');
        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
        $header = array("編號","移送","年度","列管編號","受處分人","繳款日期","繳款金額","分期/完納","核銷罰鍰","執行費","轉正怠金","退費"); 
        fputcsv($file, $header);
        foreach ($usersData as $key=>$line){ 
            //$fpart = $this->getsqlmod->getfine_partpay($line->f_num)->result();
            //fputcsv($file,$line); 
            $fvalue= $line->f_paynowamount-$line->f_fee-$line->f_turnsub-$line->f_refund;
            if($line->f_damount-$line->f_payamount<=0)$status="完納";
            if($line->f_damount-$line->f_payamount>0)$status="分期";
            fputcsv($file,array($line->f_num,$line->f_num,$line->f_num,$line->f_num,$line->s_name,$line->f_payday,$line->f_paynowamount,$status,$fvalue,$line->f_fee,$line->f_turnsub,$line->f_refund));
        }
        fclose($file); 
        exit;
        //$query = $this->getsqlmod->getfine_list($id); 
        //$data = $this->dbutil->csv_from_result($query);
        //force_download($filename,  $data);
    }
    
    public function putcsv(){
    }
    public function exportCSVAll(){
        $this->load->dbutil();
        $this->load->helper('download');
        $id = $this->uri->segment(3);
        $filename = '罰鍰清冊'.date('Ymd').'.csv';
        $usersData = $this->getsqlmod->getfine_list($id)->result();
       /* header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/csv; "); */
        //$file = fopen('php://output', 'w');
        foreach ($usersData as $key=>$line){ 
            $file = fopen('D:\\xampp\\www\\main2\\temp\\'.$line->s_ic.'.csv', 'w');
            fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
            $header = array("年度","月份","流水號","處分年度","案件","受處分人","繳款日期","繳款方式","本期核銷金額","罰鍰金額","罰鍰餘額","執行費","備註","註銷保留"); 
            fputcsv($file, $header);
            $fpart = $this->getsqlmod->getfine_partpay($line->f_num)->result();
            foreach ($fpart as $key2=>$line2){ 
                $fvalue= $line->f_damount-$line->f_fee-$line->f_turnsub-$line->f_refund-$line2->fpart_amount;
                $month = date("m",strtotime($line2->fpart_date));
                $year = substr($line->f_cnum, 2, 3);
                fputcsv($file,array((date('Y')-1911),$month,$line->f_num,$year,$line->f_cnum,$line->s_name,$line2->fpart_date,$line2->fpart_type,$line2->fpart_amount,$line->f_payamount,$fvalue,$line->f_fee,'',$line->f_turnsub));
            }
        }
        fclose($file); 

        //exit;
        $this->load->library('zip');
        //var_dump($xmls);
        $i=0;
        foreach(glob('D:\\xampp\\www\\main2\\temp\\*.*') as $file) {
            $this->zip->read_file($file);
        }
        foreach(glob('D:\\xampp\\www\\main2\\temp\\*.*') as $file) {
            unlink($file);
        }
        $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
        $this->zip->download($datet.'.zip');
        unlink($datet.'.zip');
    }

    public function listfp3() {
        $this->load->helper('form');
        $this->load->library('table');
        $name="";
        $type="";
        $ic="";
        $BVC="";
        $cnum="";
        $where="";
        $where1="";
        $where2="";
        $where3="";
        if(!empty($_POST['type']))
        {
            $type = $_POST['type'];
        }
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        $dateRanges[0]= date("Y-m-d", strtotime("-1 month"));
        $dateRanges[1]=date("Y-m-d");
        if(!empty($_POST['datepicker'])){
            $dateRanges = explode(' - ', $_POST['datepicker']);
        }
        if(!empty($_POST['cnum']))
        {
            $cnum = $_POST['cnum'];
        }
        if(!empty($_POST['BVC']))
        {
            $BVC = $_POST['BVC'];
        }
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        if(!isset($_POST['status'])) $_POST['status'] = '3';
        if($_POST['status']=='0'){//分期
            $dateRanges[0]=date("Y-m-d", strtotime("-1 month"));
            $dateRanges[1]=date("Y-m-d");
            $where = "`f_damount` > `f_payamount`";
            $where2 = "`f_samount` > `f_payamount`";
        }
        if($_POST['status']=='1'){//完納
            $dateRanges[0]=date("Y-m-d", strtotime("-1 month"));
            $dateRanges[1]=date("Y-m-d");
            $where = "`f_damount` < `f_payamount`";
            $where2 = "`f_samount` < `f_payamount`";
        }
        $query = $this->getsqlmod->getfine_search2($name,$ic,$BVC,$cnum,$type,$dateRanges[0],$dateRanges[1],$where,$where1,$where2,$where3)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號', '犯嫌人姓名', '身份證','年度繳納金額','完納金額','分期資訊','移送案號','分局','憑證核發日期','憑證編號','註銷保留款','虛擬賬號');
        $table_row = array();
        foreach ($query as $susp)
        {
            $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            $partpay=null;
            foreach($fpart as $fp){
                if(isset($fp)){
                    $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
                }else{
                    $partpay = null;   
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            if(isset($susp->surc_no)&&$susp->f_samount<=0){
                $table_row[] = $susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no;
            }else   $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->s_ic;
            $table_row[] = $susp->f_damount+$susp->f_samount;
            $table_row[] = $susp->f_payamount;
            if($partpay == null){
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else{
                $table_row[] = $partpay;
            }
            $table_row[] = $susp->ft_no;
            $table_row[] = $susp->s_roffice;
            if($susp->f_certno == null){
                $table_row[] = '<strong>未輸入憑證</strong>';
            }
            else{
                $table_row[] = $susp->f_certno;
            }
            if($susp->f_certIssue == null){
                $table_row[] = '<strong>未輸入憑證日期</strong>';
            }
            else{
                $table_row[] = $susp->f_certIssue;
            }
            if($susp->f_cancelAmount == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->f_cancelAmount;
            }
            if($susp->f_BVC == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->f_BVC;
            }
            //$table_row[] = '<input type="checkbox" name="a">';
            $this->table->add_row($table_row);
        }   
        $data['title'] = "每日帳作業";
        $data['s_table'] = $this->table->generate();
        $data['user'] = $this -> session -> userdata('uic');
        $rcp=$this->getsqlmod->getfineCproject()->result();
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/fplisted3';
        else $data['include'] = 'acc_cert_query/fplisted3';
        foreach ($rcp as $rcp1){
            $projectoption[$rcp1->fcp_no] = $rcp1->fcp_no;
        }
        if(isset($projectoption))$data['opt'] = $projectoption;
        else $data['opt'] = null;
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

    public function listFineReport() {
        $this->load->helper('form');
        $this->load->library('table');
        
        $data['title'] = "報表查詢";
        $data['s_table'] = $this->table->generate();
        $data['user'] = $this -> session -> userdata('uic');
        $rcp=$this->getsqlmod->getfineCproject()->result();
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/finereport';
        else $data['include'] = 'acc_cert_query/finereport';
        foreach ($rcp as $rcp1){
            $projectoption[$rcp1->fcp_no] = $rcp1->fcp_no;
        }
        if(isset($projectoption))$data['opt'] = $projectoption;
        else $data['opt'] = null;
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

    public function filterAccAll(){
        if($_GET['condition'] === '1')
        {
            $query = $this->getsqlmod->filterFineAll($_GET['type'], $_GET['keyword'])->result(); 
        }
        elseif($_GET['condition'] === '2')
        {
            $query = $this->getsqlmod->filterFineinKeyworkAll($_GET['type'], $_GET['keyword'])->result(); 
        }
        else
        {
            $q1 = $this->getsqlmod->filterFineabnormal1($_GET['keyword'])->result();
            $q2 = $this->getsqlmod->filterFineabnormal2($_GET['keyword'])->result();
            // $q3 = $this->getsqlmod->filterFineabnormal3($_GET['keyword'])->result(); 
            $query = array_merge($q1, $q2);

        }

		$i = 0;
		foreach ($query as $key => $value) {
			$temp_data = array(
				'fpart_fpnum' => $query[$i]->f_no
			);
			$query_body_indv =  $this->getsqlmod->check_finepayport_data($temp_data)->result();
			$temp_paymoney = $this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount;
            $temp_turnsub = $this->getsqlmod->check_finepayport_data_turnsub($temp_data)->result()[0]->f_turnsub;
			
			if($query[$i]->type == 'A')
			{
				$thisAmount = $query[$i]->f_amount*10000;
			}
			else
			{
				$thisAmount = $query[$i]->f_amount;
			}

			if($thisAmount == $query[$i]->f_doneamount)
			{
				$query[$i]->f_paymoney = 0;
			}
			else
			{
				$query[$i]->f_paymoney = (($temp_paymoney < 0)?0:($temp_paymoney+$temp_turnsub));
			}
			
			
			if(count($query_body_indv) <= 0)
			{
				$query[$i]->f_paylog = '';
			}
			else
			{
				$paylogAryStr = '';
				foreach ($query_body_indv as $partv) {
					$paylogAry_date = $this->tranfer2RCyear($partv->fpart_date);
					$paylogAry_money = ((($partv->fpart_amount >= 0 )?$partv->fpart_amount:0) + $partv->f_refund + $partv->f_turnsub);
					$paylogAryStr .= ($paylogAry_date . '_' . (((int)$paylogAry_money < 0)?('<span class="text-danger">'.$paylogAry_money.'</span>'):$paylogAry_money ).'_'. '<span style="border:1px solid #AAA;border-radius:100%;" class="'.((mb_substr($partv->fpart_type,0,1) == "金")?'bg-danger':'bg-success').'">'. mb_substr($partv->fpart_type,0,1) . '</span><br/>') ;
				}
				$query[$i]->f_paylog  = $paylogAryStr;
				
			}
			$i++;
		}
		
        
        echo json_encode(array('data' => $query));
    }

    public function filterAccFilter(){
        if($_GET['condition'] === '1')
        {
            $query = $this->getsqlmod->filterFine($_GET['type'], $_GET['keyword'])->result(); 
        }
        else
        {
            $query = $this->getsqlmod->filterFineinKeywork($_GET['type'], $_GET['keyword'])->result(); 
        }
        
        
        echo json_encode(array('data' => $query));
    }

    public function addAcc_CProject(){
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $now = date('md'); 
        $now2 = date('Y-m-d');
        if($_POST['project_type']=='罰鍰')
            $type ='1';
        if($_POST['project_type']=='怠金')
            $type ='2';
        
        $row_num = $this->getsqlmod->getCPFineProject_today_count(date('Y-m-d'));
        $data_sortno = (($row_num == 0)?1:($row_num + 1));
        $taiwan_date = date('Y')-1911; 
        $fp_num = 'C'.$taiwan_date.$now.$data_sortno;
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        
       

        if($_POST['s_status']=='1'){
            
            foreach ($s_cnum as $key => $value) {
                $i = (count($this->getsqlmod->getFineProject_maxsort($fp_num)->result()) > 0)?((int)$this->getsqlmod->getFineProject_maxsort($fp_num)->result()[0]->sort_no + 1):1;
                // $fine_rec = array(
                //     'fcp_no' => $fp_num,
                // );
                // $this->getsqlmod->updateFine($value,$fine_rec);
                $data = array(
                    // 'f_project_num' => $fp_num,
                    'sort_no' => $i,
                    'f_project_id' => $fp_num,
                    'fpart_fpnum' => $value
                );
                // $this->getsqlmod->updateFine($value,$fine_rec);
                $this->getsqlmod->add_finepoject_body_data($data);
            }
            $fcp = array(
                    'fcp_no' => $fp_num,
                    'fcp_createdate' => $now2,
                    'fcp_empno' => $user
            );
            if($_POST['s_cnum']!=NULL && $this->getsqlmod->check_fineCP_project_exist($fp_num) == 0)
            {
                $this->getsqlmod->addfcp1($fcp);
            }
            // redirect('Acc_cert/listFineCProject/'); 
            echo 'Acc_cert/listFineCProject';
        }
        else if($_POST['s_status']=='0'){
            $i = (count($this->getsqlmod->getFineProject_maxsort($_POST['fp_num'])->result()) > 0)?((int)$this->getsqlmod->getFineProject_maxsort($_POST['fp_num'])->result()[0]->sort_no + 1):1;
            foreach ($s_cnum as $key => $value) {
                // $fine_rec = array(
                //     'fcp_no' => $_POST['fp_num'],
                // );
                // $this->getsqlmod->updateFine($value,$fine_rec);
                $data = array(
                    'sort_no' => $i,
                    'f_project_id' => $_POST['fp_num'],
                    'fpart_fpnum' => $value
                );
                $this->getsqlmod->add_finepoject_body_data($data);
            }
            // redirect('Acc_cert/listFineCProject/'); 
            echo 'Acc_cert/listFineCProject';
        }
    }
    
    public function listFineCProject() {
        $this->load->helper('form');
        $this->load->library('table');
        $query = $this->getsqlmod->getfineCproject(); 
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號','專案建立時間','分期收繳登記','');
        $table_row = array();
        foreach ($query->result() as $fp)
        {
            $table_row = NULL;
            $table_row[] = $fp->fcp_no;
            if(isset($fp->fp_office_num) && !empty($fp->fp_office_num))
            {
                $str = '<br/><small>公文號：'.$fp->fp_office_num.'</small>';
            }
            else
            {
                $str = '<br/><span class="label label-success" data-toggle="modal" data-target="#documentModal" data-whatever="'. $fp->fcp_no .'" onclick="modalEvent(\''. $fp->fcp_no .'\')">新增公文號</span>';
            }
            $table_row[] = anchor('Acc_cert/listfcp1_ed/' . $fp->fcp_no, $fp->fcp_no).$str;
            
            $table_row[] =  $this->tranfer2RCyear2($fp->fcp_createdate);
            if(isset($fp->fp_office_num) && !empty($fp->fp_office_num))
            {
                $table_row[] =  anchor('PhpspreadsheetController/export_fineCPPJ_lists_by_indiv/' . $fp->fcp_no,'分期收繳登記');
            }
            else
            {            
                    $table_row[] = '';
            }

            if(preg_match("/d/i", $this->session-> userdata('3permit')))
            {
                $table_row[] =  "<a href='../Acc_cert/CPcomplete/".$fp->fcp_no."' class='btn btn-warning'>進行簽核</a>";
            }            
            else 
            {
                $table_row[] =  '';
            }
            $this->table->add_row($table_row);
        }   
        
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table->generate();

        /** 舊專案 */
        $query2 = $this->getsqlmod->getfineCPproject_done(); 
        $tmpl2 = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table2">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl2);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '專案編號','專案建立時間','分期收繳登記','');
        $table_row2 = array();
        foreach ($query2->result() as $fp)
        {
            $table_row2 = NULL;

            $str = '<br/><small>公文號：'.$fp->fp_office_num.'</small>';
            $table_row2[] = anchor('Acc_cert/listfcp1_ed/' . $fp->fcp_no, $fp->fcp_no).$str;

            $table_row2[] =  $this->tranfer2RCyear2($fp->fcp_createdate);

            if(isset($fp->fp_office_num) && !empty($fp->fp_office_num))
            {
                $table_row2[] =  anchor('PhpspreadsheetController/export_fineCPPJ_lists_by_indiv/' . $fp->fcp_no,'分期收繳登記');
            }
            else
            {            
                    $table_row2[] = '';
            }

            $table_row2[] =  $fp->fcp_status;
        }
        
        $this->table->add_row($table_row2);
        $table2 = $this->session-> userdata('uoffice');
        $test_table2 = $this->table->generate();

        $data['s_table'] = $test_table;
        $data['s_table_done'] = $test_table2;
        $data['title'] = "轉正專案";
        $data['sub_title'] = "轉正退費專案";
        $data['pj_type'] = 'CP';
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/fineproject';
        else $data['include'] = 'acc_cert_query/fineproject';
        //$data['include'] = 'acc_cert/fineproject';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

    
    public function listfcp1() {//轉正作業
        $this->load->library('table');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getfine_Clist($id)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號', '犯嫌人姓名', '身份證','年度繳納金額','完納金額','分期資訊','移送案號','分局','憑證核發日期','憑證編號','註銷保留款','虛擬賬號');
        $table_row = array();
        foreach ($query as $susp)
        {
            $ft = $this->getsqlmod->getfine_trafwithsnum($susp->f_snum)->result(); 
            $ft_no="";
            if(isset($ft[0]->ft_no))$ft_no = $ft[0]->ft_no;
            $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            $partpay=null;
            $partpayamount=0;
            foreach($fpart as $fp){
                if(isset($fp)){
                    $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
                    $partpayamount = $partpayamount + $fp->fpart_amount;
                }else{
                    $partpay = null;   
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->f_num;
            if(isset($susp->surc_no)&&$susp->f_samount<=0){
                $table_row[] = $susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no;
            }else   $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->s_ic;
            $table_row[] = $susp->f_damount+$susp->f_samount;
            $table_row[] = $susp->f_payamount;
            if($partpay == null){
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else{
                $table_row[] = $partpay;
            }
            $table_row[] = $ft_no;
            $table_row[] = $susp->s_roffice;
            if($susp->f_certno == null){
                $table_row[] = '<strong>未輸入憑證</strong>';
            }
            else{
                $table_row[] = $susp->f_certno;
            }
            if($susp->f_certDate == null){
                $table_row[] = '<strong>未輸入憑證日期</strong>';
            }
            else{
                $table_row[] = $susp->f_certDate;
            }
            if($susp->f_cancelAmount == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->f_cancelAmount;
            }
            if($susp->f_BVC == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->f_BVC;
            }
            //$table_row[] = '<input type="checkbox" name="a">';
            $this->table->add_row($table_row);
        }   
        $data['title'] = "帳務專案:".$id;
        $data['fpid'] = $id;
        $data['user'] = $this -> session -> userdata('uic');
        $data['s_table'] = $this->table->generate();
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/fplist2';
        else $data['include'] = 'acc_cert_query/fplist2';
        //$data['include'] = 'acc_cert/fplist2';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

    public function listfcp1_ed() {
        $this->load->library('table');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $query = $this->getsqlmod->getfine_Clist($id)->result(); 
        $query = $this->getsqlmod->get_finepoject_body_data($id)->result();
        $fproj = $this->getsqlmod->getfineCproject_ed($id)->result();
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table  table-bordered" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        //$this->table->set_heading( '','處分書編號', '犯嫌人姓名', '身份證','年度繳納金額','完納金額','分期資訊','移送案號','分局','憑證核發日期','憑證編號','註銷保留款','虛擬賬號','執行費','轉正類型');
         $this->table->set_heading('', '類型', '案件編號', '受處分人姓名', '身份證編號', '罰鍰/怠金', '完納日期', '完納金額', '已繳金額', '未繳金額','執行費', '轉正金額', '退費金額','虛擬帳號', '分期Log', '移送Log', '憑證Log', '撤銷註銷Log', '執行命令Log','');
        $table_row = array();
        
        foreach ($query as $susp)
        {
            // $ft = $this->getsqlmod->getfine_trafwithsnum($susp->f_snum)->result(); 
            // $ft_no="";
            // if(isset($ft[0]->ft_no))$ft_no = $ft[0]->ft_no;
            // $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            // $partpay=null;
            // $partpayamount=0;
            // foreach($fpart as $fp){
            //     if(isset($fp)){
            //         $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
            //         $partpayamount = $partpayamount + $fp->fpart_amount;
            //     }else{
            //         $partpay = null;   
            //     }
            // }
             $table_row = NULL;
            // if($susp->f_change_type != null){
            //     if(isset($susp->surc_no)&&$susp->f_samount<=0){
            //         $table_row[] = array('bgcolor' => 'gray','data'=>$susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no);
            //     }else   $table_row[] = array('bgcolor' => 'gray','data'=>$susp->fd_num);
                
            //     if(isset($susp->surc_no)&&$susp->f_samount<=0){
            //         $table_row[] = array('bgcolor' => 'gray','data'=>$susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no);
            //     }else   $table_row[] = array('bgcolor' => 'gray','data'=>$susp->fd_num);
                
            //     $table_row[] = array('bgcolor' => 'gray','data'=>$susp->s_name);
            //     $table_row[] = array('bgcolor' => 'gray','data'=>$susp->s_ic);
            //     $table_row[] = array('bgcolor' => 'gray','data'=>$susp->f_damount+$susp->f_samount);
            //     $table_row[] = array('bgcolor' => 'gray','data'=>$susp->f_payamount);
            //     if($partpay == null){
            //         $table_row[] = '<strong>無分期繳款記錄</strong>';
            //     }
            //     else{
            //         $table_row[] = $partpay;
            //     }
            //     $table_row[] = array('bgcolor' => 'gray','data'=>$ft_no);
            //     $table_row[] = array('bgcolor' => 'gray','data'=>$susp->s_roffice);
            //     if($susp->f_certno == null){
            //         $table_row[] = '<strong>未輸入憑證</strong>';
            //     }
            //     else{
            //         $table_row[] = $susp->f_certno;
            //     }
            //     if($susp->f_certDate == null){
            //         $table_row[] = '<strong>未輸入憑證日期</strong>';
            //     }
            //     else{
            //         $table_row[] = array('bgcolor' => 'gray','data'=>$susp->f_certDate);
            //     }
            //     if($susp->f_cancelAmount == null){
            //         $table_row[] = '<strong>未輸入姓名</strong>';
            //     }
            //     else{
            //         $table_row[] = array('bgcolor' => 'gray','data'=>$susp->f_cancelAmount);
            //     }
            //     if($susp->f_BVC == null){
            //         $table_row[] = '<strong>未輸入姓名</strong>';
            //     }
            //     else{
            //         $table_row[] = array('bgcolor' => 'gray','data'=>$susp->f_BVC);
            //     }
            //     $table_row[] = array('bgcolor' => 'gray','data'=>$susp->f_fee);
            //     $table_row[] = array('bgcolor' => 'gray','data'=>$susp->f_change_type);
            // }else{
            //     if(isset($susp->surc_no)&&$susp->f_samount<=0){
            //         $table_row[] = $susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no;
            //     }else   $table_row[] = $susp->fd_num;                
            //     if(isset($susp->surc_no)&&$susp->f_samount<=0){
            //         $table_row[] = $susp->surc_no .'<br>原列管編號:'.$susp->fd_listed_no;
            //     }
            //     else{
            //         $table_row[] = $susp->fd_num ;
            //     }

            //     $table_row[] = $susp->s_name;
            //     $table_row[] = $susp->s_ic;
            //     $table_row[] = $susp->f_damount+$susp->f_samount;
            //     $table_row[] = $susp->f_payamount;
            //     if($partpay == null){
            //         $table_row[] = '<strong>無分期繳款記錄</strong>';
            //     }
            //     else{
            //         $table_row[] = $partpay;
            //     }
            //     $table_row[] = $ft_no;
            //     $table_row[] = $susp->s_roffice;
            //     if($susp->f_certno == null){
            //         $table_row[] = '<strong>未輸入憑證</strong>';
            //     }
            //     else{
            //         $table_row[] = $susp->f_certno;
            //     }
            //     if($susp->f_certDate == null){
            //         $table_row[] = '<strong>未輸入憑證日期</strong>';
            //     }
            //     else{
            //         $table_row[] = $susp->f_certDate;
            //     }
            //     if($susp->f_cancelAmount == null){
            //         $table_row[] = '<strong>未輸入姓名</strong>';
            //     }
            //     else{
            //         $table_row[] = $susp->f_cancelAmount;
            //     }
            //     if($susp->f_BVC == null){
            //         $table_row[] = '<strong>未輸入姓名</strong>';
            //     }
            //     else{
            //         $table_row[] = $susp->f_BVC;
            //     }
            //     $table_row[] = $susp->f_fee;
            //     $table_row[] = $susp->f_change_type;
            // }
            // $cell = array('bgcolor' => 'gray');
            $table_row[] = $susp->f_no;
            
            $table_row[] = (($susp->type === 'A' )?'<span class="label label-danger">罰鍰</span>':'<span class="label label-warning">怠金</span>');
            // if(isset($susp->surc_no)&&$susp->f_samount<=0){
            //     $table_row[] = $susp->surc_no.'<br>原列管編號:'.$susp->fd_listed_no;
            // }else   $table_row[] = $susp->fd_num;
            $table_row[] = $susp->f_caseid . ((isset($susp->f_dellog) && !empty($susp->f_dellog))?'<span class="text-danger"> (註)</span>':'');

            // $table_row[] = $susp->s_name;

            $table_row[] = $susp->f_username. ((isset($susp->f_turnsub_log) && !empty($susp->f_turnsub_log))?'<br/><small class="text-warning">自'.explode('_',$susp->f_turnsub_log)[2].'轉正</small>':'');

            // $table_row[] = $susp->s_ic;
            $table_row[] = $susp->f_userid;

            // $table_row[] = $susp->f_damount+$susp->f_samount;
            $table_row[] = (($susp->type === 'A')?(int)$susp->f_amount * 10000:$susp->f_amount);

            $table_row[] = ($susp->f_donedate == '0000-00-00')?'':$this->tranfer2RCyear2($susp->f_donedate);

            $table_row[] = $susp->f_doneamount;

            $temp_data = array(
                "fpart_fpnum" => ((isset($susp->f_no))?$susp->f_no:0),
                "fp_createdate"=>$fproj[0]->fcp_createdate,
                "f_project_id"=>$id
            );
            
            $temp_paymoney = ((isset($susp->f_paymoney))?$susp->f_paymoney:$this->getsqlmod->check_finepayport_data_payamount($temp_data)->result()[0]->fpart_amount);

            $table_row[] = $temp_paymoney; 

            $table_row[] = ((((($susp->type === 'A')?(int)$susp->f_amount * 10000:$susp->f_amount) - (int)$susp->f_doneamount - (int)$susp->f_paymoney) >= 0)?((($susp->type === 'A')?(int)$susp->f_amount * 10000:$susp->f_amount) - (int)$susp->f_doneamount - (int)$susp->f_paymoney):0);

            $table_row[] = $susp->f_fee;
            $table_row[] = $susp->f_turnsub;
            $table_row[] = $susp->f_refund;

            $table_row[] = $susp->f_virtualcode;

            $query =  $this->getsqlmod->check_finepayport_data($temp_data)->result();
            if(count($query) <= 0)
            {
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else
            {
                // $paylogAry = mb_split("\n",$susp->f_paylog);
                $paylogAryStr = '';
                foreach ($query as $value) {
                    // $temp_v = explode('_', $value);
                    if(isset($value->fpart_amount) && !empty($value->fpart_amount) && $value->fpart_amount != 0)
                    {
                        $paylogAry_date = $value->fpart_date;
                        $paylogAry_money = $value->fpart_amount;
                        $paylogAryStr .= ($this->tranfer2RCyear2($paylogAry_date) . '已繳' . (((int)$paylogAry_money < 0)?('<span class="text-danger">'.$paylogAry_money.'</span>'):$paylogAry_money ). '元<br/>') ;
                    }
                    

                    // $temp_data = array(
                    //     "fpart_fpnum" => $susp->f_no,
                    //     "fpart_amount" => $paylogAry_money,
                    //     "fpart_type" => '',
                    //     "fpart_date" => $this->tranfer2ADyear($paylogAry_date)
                    // );

                    // $this->getsqlmod->check_finepayport_data($temp_data);
                }
                $table_row[]  = $paylogAryStr;
            }
			$calcmovelog = explode("\n", $susp->f_movelog);
			usort($calcmovelog, function($a, $b){ 
				$movedateA = explode('_', $a)[0];
				$movedateB = explode('_', $b)[0];
				return ($movedateA < $movedateB)? 1 : -1; 
			}); 
            $table_row[] = join('<br/>', $calcmovelog);
            $table_row[] = $susp->f_cardlog;
            $table_row[] = $susp->f_dellog;
            $table_row[] = str_replace("\n","<br/>", $susp->f_execlog);
            $table_row[] = (($susp->sort_no)?$susp->sort_no:1);

            $sort_no = (($susp->sort_no)?$susp->sort_no:1);

            $this->table->add_row($table_row);
        }   
        $data['title'] = "帳務專案:".$id;
        $data['f_no'] = $id;
        $data['fpid'] = $id;
        $data['max_sort'] = $sort_no;
        if(isset($fproj[0]))
        {
            $data['fcp_num'] = $fproj[0]->fcp_num;
            $data['fcp_type'] = $fproj[0]->fcp_type;
            $data['fcp_createdate'] = $this->tranfer2RCyear2($fproj[0]-> fcp_createdate);
        }
        else 
        {
            $data['fcp_num'] =null;
            $data['fcp_type'] = null;
            $data['fcp_createdate'] = null;
        }
        $data['user'] = $this -> session -> userdata('uic');
        $data['s_table'] = $this->table->generate();
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/fpclisted';
        else $data['include'] = 'acc_cert_query/fpclisted';
        //$data['include'] = 'acc_cert/fpclisted';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
   
    public function add_Fine_change_project_body(){
        // var_dump($_POST);
        // exit;

        if(isset($_POST['ori_rows_selected']) && isset($_POST['rows_selected']))
        {
            // echo count($_POST['f_no']);
            // var_dump($_POST);
            // exit;
            $ori_rows_selected = explode(",", $_POST['ori_rows_selected']);
            $ori_rows_selected_caseid = explode(",", $_POST['ori_rows_selected_caseid']);
            $rows_selected = explode(",", $_POST['rows_selected']);
            $rows_tamount = $_POST['rows_tamount'];
            $t_paycost = 0;
            $max_sort = (int)$_POST['max_sort'];

            // 先處理選取要轉正的帳務
            for ($i=0; $i < count($_POST['f_no']); $i++) { 
                $data = array(
                    'sort_no' => ($max_sort + ($i+1)),
                    'f_project_id' => $_POST['fpid'],
                    'fpart_fpnum' => $_POST['f_no'][$i],
                    'fpart_amount' => $_POST['payamount'][$i],
                    'fpart_date' => $_POST['paydate'],
                    'f_fee' => $_POST['paycost'][$i],
                    'f_refund' => $_POST['returnamount'][$i],
                    'f_turnsub_log' =>$this->tranfer2RCyear($_POST['paydate']) . '_' . $_POST['payamount'][$i] . '_' . $_POST['ori_rows_selected_caseid']
                );
                $t_paycost = $t_paycost + $_POST['paycost'][$i];
                $query = $this->getsqlmod->add_finepartpay_projectid($data); 

                $pay_log = $this->getsqlmod->getFineimport_data($_POST['f_no'][$i])->result_array();

                if(isset($pay_log[0]['f_paylog']) && !empty($pay_log[0]['f_paylog']))
                {
                    $str = "\n";
                }
                else
                {
                    $str = "";
                }
                // 未繳金額
                $this_balance = ($pay_log[0]['f_amount'] - $pay_log[0]['f_doneamount'] - $pay_log[0]['f_paymoney']);

                // 繳納後剩餘...
                $balance = $_POST['payamount'][$i] - $this_balance;
                if($balance >= 0)
                {
                    $f_donedate = $_POST['paydate'];
                    $f_doneamount = $pay_log[0]['f_amount'];
                    $f_paydate = '0000-00-00';
                    $f_paymoney = 0;
                    if($balance == 0)
                    {
                        $paylog = "";
                    }
                    else
                    {
                        $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($_POST['paydate']). '_' . $_POST['payamount'][$i];
                    }
                }
                else
                {
                    $f_donedate = '0000-00-00';
                    $f_doneamount = 0;
                    $f_paydate = $_POST['paydate'];
                    $f_paymoney = $_POST['payamount'][$i];

                    $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($_POST['paydate']). '_' . $_POST['payamount'][$i];
                }
                
                $temp_data2 = array(
                    "f_paydate" => $f_paydate,
                    "f_paymoney" => $f_paymoney,
                    "f_donedate" => $f_donedate,
                    "f_doneamount" => $f_doneamount,
                    "f_paylog" =>  $paylog,
                );
                $this->getsqlmod->update_fineimport_data($temp_data2, $_POST['f_no'][$i]);
            }
            $balance = $rows_tamount;
            // 處理被轉正的帳務
            for ($j=0; $j < count($ori_rows_selected); $j++) { 
                $pay_log = $this->getsqlmod->getFineimport_data($ori_rows_selected[$j])->result_array();

                if(isset($pay_log[0]['f_paylog']) && !empty($pay_log[0]['f_paylog']))
                {
                    $str = "\n";
                }
                else
                {
                    $str = "";
                }
                // 已繳金額
                $this_balance = ($pay_log[0]['f_doneamount'] + $pay_log[0]['f_paymoney']);

                // 轉正總金額 扣除 已繳金額後剩餘...
                $balance = $balance - $this_balance;

                if($balance >= 0)
                {
                    $f_donedate = '0000-00-00';
                    $f_doneamount = 0;
                    $f_paydate = '0000-00-00';
                    $f_paymoney = 0;
                    $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($_POST['paydate']). '_' . (0-$this_balance);

                    $data = array(
                        'f_project_id' => $_POST['fpid'],
                        'fpart_fpnum' => $ori_rows_selected[$j],
                        'fpart_amount' => (0-$this_balance),
                        'fpart_date' => $_POST['paydate']
                        
                    );
                    // $query = $this->getsqlmod->add_finepartpay_projectid($data); 
                }
                else
                {
                    $f_donedate = '0000-00-00';
                    $f_doneamount = 0;
                    $f_paydate = '0000-00-00';
                    $f_paymoney = (0-$balance);

                    $paylog = $pay_log[0]['f_paylog'] . $str .$this->tranfer2RCyear($_POST['paydate']). '_' . (0-($this_balance+$balance));

                    $data = array(
                        'f_project_id' => $_POST['fpid'],
                        'fpart_fpnum' => $ori_rows_selected[$j],
                        'fpart_amount' => (0-($this_balance+$balance)),
                        'fpart_date' => $_POST['paydate'],
                        // 'f_fee' => $t_paycost
                        
                    );
                    // $query = $this->getsqlmod->add_finepartpay_projectid($data); 
                }
                
                $temp_data2 = array(
                    "f_paydate" => $f_paydate,
                    "f_paymoney" => $f_paymoney,
                    "f_donedate" => $f_donedate,
                    "f_doneamount" => $f_doneamount,
                    "f_paylog" =>  $paylog,
                );
                $this->getsqlmod->add_finepayport_data($data, $temp_data2, $ori_rows_selected[$j], $_POST['fpid']);
            }
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        
    }

    public function add_returnfund_in_person(){
        if(isset($_POST['em_f_no']) && isset($_POST['em_f_project_id']))
        {
            $data = array(
                'f_refund' => $_POST['returnmoney'],
                'f_comment' => $_POST['return_comment'],
                'fpart_date' => $_POST['return_paydate']                
            );
            $this->getsqlmod->update_finepayport_data($data, $_POST['em_f_no'], $_POST['em_f_project_id']);
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        
    }
    public function editfcplist1(){
        // var_dump($_POST);
        // echo mb_split("<br>",$_POST['s_cnum'])[0];
        // exit;
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        // var_dump($_POST);
        // $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum = mb_split("<br>",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        if($_POST['s_status']=='1') redirect('Acc_cert/listFineCProject/'); 
        if($_POST['s_status']=='merge'){
            $this->mergechange($s_cnum,$_POST['selcnum']);
            //redirect('Acc_cert/mergechange/');
        }
        if($_POST['s_status']=='transfer'){
            $this->transferchange($s_cnum,$s_cnum[0]);
            //redirect('Acc_cert/mergechange/');
        }
        if($_POST['s_status']=='disassemble'){
            $this->disassemblechange($s_cnum,$s_cnum[0]);
            //redirect('Acc_cert/mergechange/');
        }
        foreach ($s_cnum1 as $key => $value) {
        };
        //redirect('Acc_cert/listfcp1/'); 
    }
    
    public function mergechange($data1=array(),$sel) {
        $this->load->helper('form');
        
        $i=0;
        $amount=0;
        $payamount=0;
        $fee=0;
        // var_dump($data1);
        // exit;
        foreach ($data1 as $key => $value) {
            if(substr($value,0,1)!="A")$query = $this->getsqlmod->getfine_Clistfd($value)->result(); 
            else $query = $this->getsqlmod->getfine_Clistsurc($value)->result(); 
            $fine = array(
                'f_change_type' => '轉正合併',
            );
            // var_dump($query[0]->f_snum);
            // exit;
            $this->getsqlmod->updateFine_bank1($query[0]->f_snum,$fine);
            $i++;
            $data['fcp'.$i.'']= $query;
            $amount = $amount + $query[0]->f_damount;
            $payamount = $payamount + $query[0]->f_payamount;
            $fee = $fee + $query[0]->f_fee;
        };
        $data['amount'] = $amount;
        $data['payamount'] = $payamount;
        $data['fee'] = $fee;
        if(substr($sel,0,1)!="A")$sel = $this->getsqlmod->getfine_Clistfd($sel)->result(); 
        else $sel = $this->getsqlmod->getfine_Clistsurc($sel)->result(); 
        $data['fcp'] = $sel[0];
        $data['length'] = count($data1);
        $data['title'] = "合併案件";
        $data['nav'] = 'navbar3';
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/merge_change';
        else $data['include'] = 'acc_cert_query/merge_change';
        //$data['include'] = 'acc_cert/merge_change';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases2/new_cases' ); 
    }
    
    public function transferchange($data1=array(),$sel) {
        $this->load->helper('form');
        $i=0;
        $amount=0;
        $payamount=0;
        $fee=0;
        // print_r(substr($sel,0,1));
        if(substr($sel,0,1)!="A")$sel = $this->getsqlmod->getfine_Clistfd($sel)->result(); 
        else $sel = $this->getsqlmod->getfine_Clistsurc($sel)->result(); 

        // print_r($sel);
        // exit;
        $data['fcp'] = $sel[0];
        // $data['fcp'] = '10911171';
        $data['length'] = count($data1);
        //$id = $this->uri->segment(3);
        //$query = $this->getsqlmod->getfine_Clist1($id)->result(); 
        $data['title'] = "單筆轉移";
        $data['nav'] = 'navbar3';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/transfer_change';
        else $data['include'] = 'acc_cert_query/transfer_change';
        //$data['include'] = 'acc_cert/transfer_change';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases2/new_cases' ); 
    }
    public function disassemblechange($data1=array(),$sel) {
        $this->load->helper('form');
        $i=0;
        $amount=0;
        $payamount=0;
        $fee=0;
        if(substr($sel,0,1)!="A")$sel = $this->getsqlmod->getfine_Clistfd($sel)->result(); 
        else $sel = $this->getsqlmod->getfine_Clistsurc($sel)->result(); 
        $data['fcp'] = $sel[0];
        $data['length'] = count($data1);
        //$id = $this->uri->segment(3);
        //$query = $this->getsqlmod->getfine_Clist1($id)->result(); 
        $data['title'] = "拆解";
        $data['nav'] = 'navbar3';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/disassemble_change';
        else $data['include'] = 'acc_cert_query/disassemble_change';
        //$data['include'] = 'acc_cert/disassemble_change';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases2/new_cases' ); 
    }
    
    public function save_merge_change() {
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        var_dump($_POST);
        $fine = array(
            'f_change_type' => null,
            'f_damount' => $_POST['f_damount'],
            'f_payamount' => $_POST['f_payamount'],
            'f_fee' => $_POST['f_fee']
        );
        $this->getsqlmod->updateFine_bank1($_POST['f_snum'],$fine);
        redirect('Acc_cert/listfcp1_ed/'.$_POST['fcp_no']); 
    }
    
    public function save_transfer_change() {
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //var_dump($_POST);
        $fine = array(
            'f_change_type' => Null,
            'f_name' => $_POST['fd_num'],
            'f_damount' => $_POST['f_damount'],
            'f_samount' => $_POST['f_samount'],
            'f_payamount' => $_POST['f_payamount'],
            'fcp_no' => $_POST['fcp_no'],
            'f_date' => $_POST['f_date'],
            'f_project_num' => $_POST['f_project_num'],
            'f_payday' => $_POST['f_payday'],
            'f_paytype' => $_POST['f_paytype'],
            'f_refund' => $_POST['f_refund'],
            'f_turnsub' => $_POST['f_turnsub'],
            'f_certno' => $_POST['f_certno'],
            'f_certDate' => $_POST['f_certDate'],
            'f_BVC' => $_POST['f_BVC'],
            'f_check_unit' => $_POST['f_check_unit'],
            'f_check_receipt' => $_POST['f_check_receipt'],
            'f_check_officenum' => $_POST['f_check_officenum'],
            'f_check_checknum' => $_POST['f_check_checknum'],
            'f_cnum' => $_POST['f_cnum'],
            'f_snum' => $_POST['f_snum'],
            'f_sic' => $_POST['f_sic'],
            'f_empno' => $user,
            'f_fee' => $_POST['f_fee']
        );
        $fine1 = array(
            'f_change_type' => $_POST['f_change_type'],
        );
        $this->getsqlmod->addFine_bank($fine);
        $this->getsqlmod->updateFine_bank($_POST['f_num'],$fine1);
        redirect('Acc_cert/listfcp1_ed/'.$_POST['fcp_no']); 
    }
    public function save_disassemble_change() {
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //var_dump($_POST);
        //echo count($_POST['f_snum']);
        for($i=0, $count = count($_POST['f_snum']);$i<$count;$i++) {
            $dataSet1[] = array();
            $dataSet1[$i] = array (
            'f_change_type' => Null,
            'f_damount' => $_POST['f_damount'][$i],
            'f_samount' => $_POST['f_samount'][$i],
            'f_payamount' => $_POST['f_payamount'][$i],
            'fcp_no' => $_POST['fcp_no'][$i],
            'f_project_num' => $_POST['f_project_num'][$i],
            'f_payday' => $_POST['f_payday'][$i],
            'f_paytype' => $_POST['f_paytype'][$i],
            'f_refund' => $_POST['f_refund'][$i],
            'f_turnsub' => $_POST['f_turnsub'][$i],
            'f_certno' => $_POST['f_certno'][$i],
            'f_certDate' => $_POST['f_certDate'][$i],
            'f_BVC' => $_POST['f_BVC'][$i],
            'f_check_unit' => $_POST['f_check_unit'][$i],
            'f_check_receipt' => $_POST['f_check_receipt'][$i],
            'f_check_officenum' => $_POST['f_check_officenum'][$i],
            'f_check_checknum' => $_POST['f_check_checknum'][$i],
            'f_name' => $_POST['f_cnum'][$i],
            'f_cnum' => $_POST['f_cnum1'],
            'f_snum' => $_POST['f_snum'][$i],
            'f_sic' => $_POST['f_sic'][$i],
            'f_empno' => $user,
            'f_fee' => $_POST['f_fee'][$i]
            );
            //var_dump($dataSet1[$i]);
            //echo '<br>';
            $this->getsqlmod->addFine_bank($dataSet1[$i]);
        }
        $fine1 = array(
            'f_change_type' => '分解',
        );
        $this->getsqlmod->updateFine_bank($_POST['f_num'][0],$fine1);
        redirect('Acc_cert/listfcp1_ed/'.$_POST['fcp_no'][0]); 
    }
    
    
    public function CertSearch() {
        $this->load->helper('form');
        $this->load->library('table');
        $name="";
        $type="";
        $ic="";
        $BVC="";
        $cnum="";
        $where="";
        if(!empty($_POST['type']))
        {
            $type = $_POST['type'];
        }
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        $dateRanges[0]="2020-01-01";
        $dateRanges[1]=date("Y-m-d");
        if(!empty($_POST['datepicker'])){
            $dateRanges = explode(' - ', $_POST['datepicker']);
        }
        if(!empty($_POST['cnum']))
        {
            $cnum = $_POST['cnum'];
        }
        if(!empty($_POST['BVC']))
        {
            $BVC = $_POST['BVC'];
        }
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        if(!isset($_POST['status'])) $_POST['status'] = '3';
        if($_POST['status']=='0'){//分期
            $dateRanges[0]=date("Y-m-d", strtotime("-1 month"));
            $dateRanges[1]=date("Y-m-d");
            $where = "f_damount>f_payamount";
        }
        if($_POST['status']=='1'){//完納
            $dateRanges[0]=date("Y-m-d", strtotime("-1 month"));
            $dateRanges[1]=date("Y-m-d");
            $where = "f_damount<=f_payamount";
        }
        // $query = $this->getsqlmod->getfine_search3($name,$ic,$BVC,$cnum,$type,$dateRanges[0],$dateRanges[1],$where)->result(); 
		$query = $this->getsqlmod->getFineCertList()->result();
		// var_dump($query);
		// exit();
        $tmpl = array (
            'table_open' => '<table style="width:100%" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        // $this->table->set_heading( '','原處分書號', '犯嫌人姓名', '身份證','年度繳納金額','完納金額','分期資訊','移送案號','分局','憑證編號','憑證核發日期','註銷保留款','虛擬賬號');
		$this->table->set_heading( '','原處分書號', '犯嫌人姓名', '憑證編號','核發日期','債權憑證編號','金額','憑證金額','移送案號');
        $table_row = array();
        foreach ($query as $susp)
        {
			$fineimport = $this->getsqlmod->filterFineinKeyworkAll('movenum', $susp->f_moveno)->result_array();
            // $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            $partpay=null;
            // foreach($fpart as $fp){
            //     if(isset($fp)){
            //         $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
            //     }else{
            //         $partpay = null;   
            //     }
            // }
            $table_row = NULL;
			$table_row[] = $susp->f_no;
			$table_row[] = $fineimport[0]['f_caseid'];
			$table_row[] = $fineimport[0]['f_username'];
			$table_row[] = $susp->f_certno;
			$table_row[] = $susp->f_date;
			$table_row[] = $susp->f_debtno;
			$table_row[] = $susp->f_amount;
			$table_row[] = $susp->f_cardmount;
			$table_row[] = $susp->f_moveno;
            // $table_row[] = $susp->s_num;
            // $table_row[] =  anchor('Acc_cert/newcert/' . $susp->f_num,$susp->fd_num);
            // //$table_row[] = Anchor$susp->fd_num;
            // $table_row[] = $susp->s_name;
            // $table_row[] = $susp->s_ic;
            // $table_row[] = $susp->f_damount+$susp->f_samount;
            // $table_row[] = $susp->f_payamount;
            // if($partpay == null){
            //     $table_row[] = '<strong>無分期繳款記錄</strong>';
            // }
            // else{
            //     $table_row[] = $partpay;
            // }
            // $table_row[] = $susp->ft_no;
            // $table_row[] = $susp->s_roffice;
            // if($susp->f_certno == null){
            //     $table_row[] = '<strong>未輸入憑證</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_certno;
            // }
            // if($susp->f_certIssue == null){
            //     $table_row[] = '<strong>未輸入憑證日期</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_certIssue;
            // }
            // if($susp->f_cancelAmount == null){
            //     $table_row[] = '<strong>未輸入姓名</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_cancelAmount;
            // }
            // if($susp->f_BVC == null){
            //     $table_row[] = '<strong>未輸入姓名</strong>';
            // }
            // else{
            //     $table_row[] = $susp->f_BVC;
            // }
            //$table_row[] = '<input type="checkbox" name="a">';
            $this->table->add_row($table_row);
        }   
        $data['title'] = "憑證查詢";
        $data['s_table'] = $this->table->generate();
        $data['user'] = $this -> session -> userdata('uic');
        $rcp=$this->getsqlmod->getfineCproject()->result();
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/cert_list';
        else $data['include'] = 'acc_cert_query/cert_list';
        //$data['include'] = 'acc_cert/cert_list';
        foreach ($rcp as $rcp1){
            $projectoption[$rcp1->fcp_no] = $rcp1->fcp_no;
        }
        if(isset($projectoption))$data['opt'] = $projectoption;
        else $data['opt'] = null;
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    function newcert(){
        $id = $this->uri->segment(3);
        $rcp=$this->getsqlmod->getFinewithf_num($id)->result();
        // var_dump($rcp);
        // exit;
        $data['title'] = "寫入憑證";
        $data['user'] = $this -> session -> userdata('uic');
        $data['row'] = $rcp[0];
        if(preg_match("/d/i", $this->session-> userdata('3permit'))) $data['include'] = 'acc_cert/newcert';
        else $data['include'] = 'acc_cert_query/newcert';
        //$data['include'] = 'acc_cert/newcert';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    function updatecert(){
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $uname = $this -> session -> userdata('uname');
        $f_certdoc = NUll;
        if(!empty($_FILES['f_certdoc']['name'])){
            $_FILES['file']['name'] = $_POST['f_certno'].'_cert.'.'pdf';
            $_FILES['file']['type'] = $_FILES['f_certdoc']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['f_certdoc']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['f_certdoc']['error'];
            $_FILES['file']['size'] = $_FILES['f_certdoc']['size'];
            $config['upload_path'] = 'certdoc/'; 
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '50000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $f_certdoc = $uploadData['file_name'];
            }
        }
        $data = array(
            'f_certno' => $_POST['f_certno'],
            'f_certamount' => $_POST['f_certamount'],
            'f_certIssue' => $_POST['f_certIssue'],
            'f_certdoc' => $f_certdoc,
        );
        $this->getsqlmod->updateFine_bank($_POST['f_num'],$data);
        redirect('Acc_cert/CertSearch/'); 
    }
    public function delete_all_testing_data()
    {
        $query = $this->getsqlmod->deleteAllTestData();
        
        if($query)
        {
            echo 'ok';
        }
        else
        {
            echo 'error';
        }
    }

    public function getLatestFineCertNo()
    {
        if($this->getsqlmod->getFineCert_no_lastest())
        {
            $certno = $this->getsqlmod->getFineCert_no_lastest()[0]->f_certno;
            $response = array(
                'status' => 'OK',
                'message' => 'The Latest Fine Certno follow.',
                'CertNo' => (int)$certno + 1
            );
            $this->output
                    ->set_status_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
        }
        else
        {
            $response = array(
                'status' => 'OK',
                'message' => 'OMG',
                'CertNo' => (string)(date('Y')-1911).str_pad(1,4,'0',STR_PAD_LEFT)
            );
            $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
        }
        
        exit;
        
    }

    public function getLatestLazyCertNo()
    {        
        if($this->getsqlmod->getLazyCert_no_lastest())
        {
            $certno = $this->getsqlmod->getLazyCert_no_lastest()[0]->f_certno;
            $response = array(
                'status' => 'OK',
                'message' => 'The Latest Lazy Certno follow.',
                'CertNo' => substr($certno, 0, 1) . (((int)str_replace('A', '', $certno)) + 1)
            );
            $this->output
                    ->set_status_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
        }
        else
        {
            $response = array(
                'status' => 'OK',
                'message' => 'OMG',
                'CertNo' => 'A'.(string)(date('Y')-1911).str_pad(1,4,'0',STR_PAD_LEFT)
            );
            $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
        }
        
        exit;
        
    }

	
    
    /** 轉西元年 */
    public function tranfer2ADyear($date)
    {
        if(strlen($date) == 6)
        {
            $ad = ((int)substr($date, 0, 2)) + 1911;
            return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
        }
        elseif(strlen($date) == 7)
        {
            $ad = ((int)substr($date, 0, 3)) + 1911;
            return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
        }
        else
        {
            return '';
        }
    }
	/** 轉西元年 */
    public function tranfer2ADyear2($date)
    {
		$date = str_replace('-', '', $date);
        if(strlen($date) == 6)
        {
            $ad = ((int)substr($date, 0, 2)) + 1911;
            return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
        }
        elseif(strlen($date) == 7)
        {
            $ad = ((int)substr($date, 0, 3)) + 1911;
            return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
        }
        else
        {
            return '';
        }
    }
    /** 轉民國年 */
    public function tranfer2RCyear($date)
    {
        $datestr = str_replace('-', '', $date);
        
        $rc = ((int)substr($datestr, 0, 4)) - 1911;
        return (($rc > 0)?((string)$rc . substr($datestr, 4, 4)):'') ;
    }

	/** 轉民國年 */
    public function tranfer2RCyear2($date)
    {
        $datestr = explode('-', $date);
        
        $rc = ((int)$datestr[0]) - 1911;
        return (string)$rc . '-' . $datestr[1] . '-' .$datestr[2] ;
    }

    /** 轉民國年 中文年月日 */
    function tranfer2RCyearTrad($date)
    {
        $date = str_replace('-', '', $date);
        $rc = ((int)substr($date, 0, 4)) - 1911;
        return (string)$rc . '年' . substr($date, 4, 2) . '月' . substr($date, 6, 2) . '日';
    }
}
