<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Record extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }

    public function cases() {//處分書
        $this->load->library('table');
        $query = $this->getsqlmod->getFDl50()->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '處分書編號', '最後修改者','異動時間');
        $table_row = array();
        $i=0;
        foreach ($query as $drug_rec)
        {
            $table_row = NULL;
            $table_row[] = $drug_rec->fd_num;
            $table_row[] = $drug_rec->fd_empno;
            $table_row[] = $drug_rec->fd_edtime;
            $this->table->add_row($table_row);
        }   
        $data['s_table'] = $this->table->generate();
        $data['title'] = "處分書日誌記錄";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'record/evilistrec';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    public function surc() {//處分書
        $this->load->library('table');
        $query = $this->getsqlmod->getSurcL50()->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '怠金處分書編號', '最後修改者','異動時間');
        $table_row = array();
        $i=0;
        foreach ($query as $drug_rec)
        {
            $table_row = NULL;
            $table_row[] = $drug_rec->surc_no;
            $table_row[] = $drug_rec->surc_empno;
            $table_row[] = $drug_rec->surc_edtime;
            $this->table->add_row($table_row);
        }   
        $data['s_table'] = $this->table->generate();
        $data['title'] = "怠金處分書日誌記錄";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'record/evilistrec';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    function traf() {
        $this->load->library('table');
        $query = $this->getsqlmod->getTrafL50()->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '怠金處分書編號', '最後修改者','異動時間');
        $table_row = array();
        $i=0;
        foreach ($query as $drug_rec)
        {
            $table_row = NULL;
            $table_row[] = $drug_rec->ft_no;
            $table_row[] = $drug_rec->ft_empno;
            $table_row[] = $drug_rec->ft_edtime;
            $this->table->add_row($table_row);
        }   
        $data['s_table'] = $this->table->generate();
        $data['title'] = "移送日誌記錄";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'record/evilistrec';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    function acc() {
        $this->load->library('table');
        $query = $this->getsqlmod->getAccL50()->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '受處分人', '最後修改者','異動時間');
        $table_row = array();
        $i=0;
        foreach ($query as $drug_rec)
        {
            $table_row = NULL;
            $table_row[] = $drug_rec->s_name;
            $table_row[] = $drug_rec->f_empno;
            $table_row[] = $drug_rec->f_edtime;
            $this->table->add_row($table_row);
        }   
        $data['s_table'] = $this->table->generate();
        $data['title'] = "帳務日誌記錄";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'record/evilistrec';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    function testajax() {
        $table = 'suspect';
        $primaryKey = 's_num';
        $columns = array(
            array( 'db' => 's_name', 'dt' => 0 ),
            array( 'db' => 's_ic',  'dt' => 1 ),
            array( 'db' => 's_gender',   'dt' => 2 ),
        );
         
        // SQL server connection information
        $sql_details = array(
            'user' => 'root',
            'pass' => 'test123',
            'db'   => 'db',
            'host' => '127.0.0.1:3306'
        );
        require( 'ssp.class.php' );
         
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
        $data['title'] = "獎金日誌記錄";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'record/test';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    function reward() {
        $this->load->library('table');
        $query = $this->getsqlmod->getRewardL50()->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '受處分人', '最後修改者','異動時間');
        $table_row = array();
        $i=0;
        foreach ($query as $drug_rec)
        {
            $table_row = NULL;
            $table_row[] = $drug_rec->s_name;
            $table_row[] = $drug_rec->e_ed_empno;
            $table_row[] = $drug_rec->s_edtime;
            $this->table->add_row($table_row);
        }   
        $data['s_table'] = $this->table->generate();
        $data['title'] = "獎金日誌記錄";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'record/evilistrec';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    function admin() {
        $this->load->helper('form');
        $data['title'] = "修改3階權限";
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getempno_mail($id)->result(); // 使用getsqlmod裡的getdata功能
        $data['user'] = $this -> session -> userdata('uic');
        $data['uic'] = $id;
        $data['include'] = 'record/account';
        $data['permit'] = $query[0]->em_3permit;
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    function updateadmin() {
        $permit ='0';
        $priority = 2;
        if(isset($_POST['em_3permit'])){
            $permit = implode('',$_POST['em_3permit']); 
            $priority = 3; 
        } 
        $this->getsqlmod->Updatepermit($_POST['uid'],$permit,$priority);
        redirect('record/permit','refresh'); 
    }
    
    function permit(){
        if(preg_match("/s/i", $this->session-> userdata('3permit'))){
            $this->load->helper('form');
            $this->load->library('table');
            $this->load->model('getsqlmod');
            $tmpl = array (
                'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
                'heading_row_start' => '<tr>',
                'row_start' => '<tr>'
                );
            $this->table->set_template($tmpl);
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading( '賬號', '姓名', '分局','職稱','單位','權限','三階權限','驗證碼');
            $data['s_table'] = $this->table->generate();
            $data['title'] = "管理權限";
            $data['user'] = $this -> session -> userdata('uic');
            //$data['headline'] = "測試案件處理系統";
            $data['include'] = 'record/IsAdmin';
            $IsAdmin = $this -> session -> userdata('IsAdmin');
            $data['nav'] = 'navbar3';
            $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
            $this->load->view('template', $data);
        }
        else {
            $data['title'] = "沒有權限";
            $data['user'] = $this -> session -> userdata('uic');
            $data['include'] = 'record/nopermit';
            $data['nav'] = 'navbar3';
            $this->load->view('template', $data);
        }
    }
    function testjson() {
        include 'config.php';

        $query = 1;
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (em_mailId like '%".$searchValue."%' or 
                em_lastname like '%".$searchValue."%' or 
                em_centername like '%".$searchValue."%' or 
                em_firstname like '%".$searchValue."%' or 
                em_officeAll like '%".$searchValue."%' or 
                em_3permit like '%".$searchValue."%' or 
                em_office like '%".$searchValue."%' or 
                em_change_code like '%".$searchValue."%' or 
                em_roffice like'%".$searchValue."%' ) ";
        }

        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees WHERE 1 = '".$query."'".$searchQuery);
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from employees WHERE 1 = '".$query."'".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            $edit ='';
            $showpermit ='';
            if($row['em_priority'] == '3' || $row['em_roffice'] == '偵查第六隊') {
                $edit = anchor('record/Admin/' . $row['em_mailId'], '修改三階權限');
                if($row['em_3permit'] != '0') $showpermit = '('.$row['em_3permit'].')';
                else $showpermit = '(二階權限)';
            }
            if($row['em_IsAdmin'] == '1') $link = anchor('login/changead/1/' . $row['em_mailId'], '更改為一般使用者');
            if($row['em_IsAdmin'] == '0') $link = anchor('login/changead/0/' . $row['em_mailId'], '更改為管理員');
            if($row['em_IsAdmin'] == '2' && $row['em_office'] == '保安警察大隊') $link = anchor('login/changead/0/' . $row['em_mailId'], '更改為管理員').'<br>'.anchor('login/changead/1/' . $row['em_mailId'], '更改為一般使用者');
            if($row['em_IsAdmin'] == '1' && $row['em_office'] == '保安警察大隊') $link = anchor('login/changead/2/' . $row['em_mailId'], '更改為獎金特殊權限').'<br>'.anchor('login/changead/1/' . $row['em_mailId'], '更改為一般使用者');
            if($row['em_IsAdmin'] == '0' && $row['em_office'] == '保安警察大隊') $link = anchor('login/changead/2/' . $row['em_mailId'], '更改為獎金特殊權限').'<br>'.anchor('login/changead/0/' . $row['em_mailId'], '更改為管理員');
            $data[] = array(
                    "em_lastname"=>$row['em_lastname'].$row['em_centername'].$row['em_firstname'],
                    "em_officeAll"=>$row['em_officeAll'],
                    "em_office"=>$row['em_office'],
                    "em_job"=>$row['em_job'],
                    "em_IsAdmin"=> $link,
                    "em_3permit"=> $edit.$showpermit,
                    "em_mailId"=>$row['em_mailId'],
                    "em_change_code"=>$row['em_change_code'],
                    "em_roffice"=>$row['em_roffice']
                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }

}