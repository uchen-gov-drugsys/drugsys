<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Cancel extends CI_Controller {//帳務憑證
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }

    public function Test() {
        $this->load->helper('form');
        $this->load->library('table');
        $name="";
        $type="";
        $ic="";
        $BVC="";
        $cnum="";
        $where="";
        if(!empty($_POST['type']))
        {
            $type = $_POST['type'];
        }
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        $dateRanges[0]="2020-01-01";
        $dateRanges[1]=date("Y-m-d");
        if(!empty($_POST['datepicker'])){
            $dateRanges = explode(' - ', $_POST['datepicker']);
        }
        if(!empty($_POST['cnum']))
        {
            $cnum = $_POST['cnum'];
        }
        if(!empty($_POST['BVC']))
        {
            $BVC = $_POST['BVC'];
        }
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
        } 
        if(!isset($_POST['status'])) $_POST['status'] = '3';
        if($_POST['status']=='0'){//分期
            $dateRanges[0]=date("Y-m-d", strtotime("-1 month"));
            $dateRanges[1]=date("Y-m-d");
            $where = "f_damount>f_payamount";
        }
        if($_POST['status']=='1'){//完納
            $dateRanges[0]=date("Y-m-d", strtotime("-1 month"));
            $dateRanges[1]=date("Y-m-d");
            $where = "f_damount<=f_payamount";
        }
        $query = $this->getsqlmod->getfine_search3($name,$ic,$BVC,$cnum,$type,$dateRanges[0],$dateRanges[1],$where)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','年度', '列管編號(罰單)', '受處分人','繳款日期','分期完納','分期資訊');
        $table_row = array();
        foreach ($query as $susp)
        {
            $taiwan_date = date('Y')-1911; 
            $fpart = $this->getsqlmod->getfine_partpay($susp->f_num)->result(); 
            $partpay=null;
            foreach($fpart as $fp){
                if(isset($fp)){
                    $partpay = $partpay.$fp->fpart_date.'日，繳'.$fp->fpart_amount.'元<br>';
                }else{
                    $partpay = null;   
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            $table_row[] =  $taiwan_date;
            //$table_row[] = Anchor$susp->fd_num;
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->f_date;
            if($susp->f_damount+$susp->f_samount <= $susp->f_payamount)$table_row[] = '完納';
            if($susp->f_damount+$susp->f_samount > $susp->f_payamount)$table_row[] = '分期';
            //$table_row[] = $susp->f_payamount;
            if($partpay == null){
                $table_row[] = '<strong>無分期繳款記錄</strong>';
            }
            else{
                $table_row[] = $partpay;
            }
            $this->table->add_row($table_row);
        }   
        $data['title'] = "測試註銷";
        $data['s_table'] = $this->table->generate();
        $data['user'] = $this -> session -> userdata('uic');
        $rcp=$this->getsqlmod->getfineCproject()->result();
        if(preg_match("/s/i", $this->session-> userdata('3permit'))) $data['include'] = 'cancel/cancellist';
        else $data['include'] = 'cancel/cancellist';
        //$data['include'] = 'acc_cert/cert_list';
        foreach ($rcp as $rcp1){
            $projectoption[$rcp1->fcp_no] = $rcp1->fcp_no;
        }
        if(isset($projectoption))$data['opt'] = $projectoption;
        else $data['opt'] = null;
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
}  
?>