<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Call extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }

    function table_dp() {//
        $this->load->library('table');
        $query = $this->getsqlmod->getCallList()->result(); 
		// var_dump($query);
		// exit;
        $tmpl = array (
            'table_open' => '<table style="width: 100%;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限','送達情形');
        $table_row = array();
        foreach ($query as $susp)
        {
            $date = new DateTime($susp->fd_limitdate); // Y-m-d
            $date->add(new DateInterval('P30D'));
            if($date->format('d')>25){
                $date->add(new DateInterval('P5D'));
            }
            $day = $date->format('d')%5;
            //$day = 6;
            if($day < 5){
                while($day  < 5){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            else if($day < 10){
                while($day  < 10){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->fd_zipcode;
            $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            $table_row[] = $susp->fd_limitdate;
            // if($susp->call_delivery_status !=null){
                // $table_row[] = $susp->call_delivery_date.';'.$susp->call_delivery_status;
            // }else{
                $table_row[] = '';
            // }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    public function index() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $cp=$this->getsqlmod->getcpproject($table)->result();
        $test_table = $this->table_dp($table);
        $type_options = array();
        foreach ($cp as $rp1){
            $type_options[$rp1->cp_name] = $rp1->cp_name.$rp1->cp_type;
        }
        $data['s_table'] = $test_table;
        $data['title'] = "待催繳罰緩案件";
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Call/listdp';
        else $data['include'] = 'Call_query/listdp';
        //$data['include'] = 'Call/listdp';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function updateProject() {//加入專案
        var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $uname = $this -> session -> userdata('uname');
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        //var_dump($_POST['s_cnum']);
        if($_POST['s_status']=='1' && $_POST['s_cnum']!='' && $_POST['projectname']!=''){//一般催繳
            foreach ($s_cnum as $key => $value) {
                $query = $this->getsqlmod->getCalldetail($value)->result(); 
                $susp = $query[0]; 
                $date = new DateTime($susp->f_date); // Y-m-d
                $date->add(new DateInterval('P30D'));
                if($date->format('d')>25){
                    $date->add(new DateInterval('P5D'));
                }
                $day = $date->format('d')%5;
                //$day = 6;
                if($day < 5){
                    while($day  < 5){
                        $day++;
                        $date->add(new DateInterval('P1D'));
                    }
                }
                else if($day < 10){
                    while($day  < 10){
                        $day++;
                        $date->add(new DateInterval('P1D'));
                    }
                }
                $data = array(
                    'call_no' => $_POST['projectname'],
                    'call_projectnum' => $_POST['projectname'],
                    'call_empno' => $uname,
                    'call_grace' => $date->format('Y-m-d'),
                    'call_snum' => $susp->f_snum,
                    'call_sic' => $susp->f_sic,
                    'call_cnum' => $susp->f_cnum,
                    'call_fdnum' => $susp->f_num,
                );
                if(!isset($susp->call_snum))$this->getsqlmod->addcalldoc($data);
                else $this->getsqlmod->updatecalldoc($value,$data);
            }
            $data1 = array(
                    'cp_name' => $_POST['projectname'],
                    'cp_date' => date('Y-m-d'),
                    'cp_type' => '一般',
                    'cp_empno' => $uname,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addcallProject($data1);
            redirect('Call/ProjectList/'); 
        }
        if($_POST['s_status']=='2' && $_POST['s_cnum']!='' && $_POST['projectname1']!=''){//公示催繳
            foreach ($s_cnum as $key => $value) {
                //echo '<br>';
                $query = $this->getsqlmod->getCalldetail($value)->result(); 
                $susp = $query[0]; 
                $date = new DateTime($susp->f_date); // Y-m-d
                $date->add(new DateInterval('P30D'));
                if($date->format('d')>25){
                    $date->add(new DateInterval('P5D'));
                }
                $day = $date->format('d')%5;
                //$day = 6;
                if($day < 5){
                    while($day  < 5){
                        $day++;
                        $date->add(new DateInterval('P1D'));
                    }
                }
                else if($day < 10){
                    while($day  < 10){
                        $day++;
                        $date->add(new DateInterval('P1D'));
                    }
                }
                $data = array(
                    'call_no' => $_POST['projectname1'],
                    'call_projectnum' => $_POST['projectname1'],
                    'call_empno' => $uname,
                    'call_grace' => $date->format('Y-m-d'),
                    'call_snum' => $susp->f_snum,
                    'call_sic' => $susp->f_sic,
                    'call_cnum' => $susp->f_cnum,
                    'call_fdnum' => $susp->f_num,
                );
                if(!isset($susp->call_snum))$this->getsqlmod->addcalldoc($data);
                else $this->getsqlmod->updatecalldoc($value,$data);
            }
            $data1 = array(
                    'cp_name' => $_POST['projectname1'],
                    'cp_date' => date('Y-m-d'),
                    'cp_type' => '公示',
                    'cp_empno' => $uname,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addcallProject($data1);
            redirect('Call/ProjectList/'); 
        }
        if($_POST['s_status']=='0' && $_POST['s_cnum']!=''){//加入舊案
            foreach ($s_cnum as $key => $value) {
                $query = $this->getsqlmod->getCalldetail($value)->result(); 
                $susp = $query[0]; 
                $date = new DateTime($susp->f_date); // Y-m-d
                $date->add(new DateInterval('P30D'));
                if($date->format('d')>25){
                    $date->add(new DateInterval('P5D'));
                }
                $day = $date->format('d')%5;
                //$day = 6;
                if($day < 5){
                    while($day  < 5){
                        $day++;
                        $date->add(new DateInterval('P1D'));
                    }
                }
                else if($day < 10){
                    while($day  < 10){
                        $day++;
                        $date->add(new DateInterval('P1D'));
                    }
                }
                $data = array(
                    'call_no' => $_POST['cp_name'],
                    'call_projectnum' => $_POST['cp_name'],
                    'call_empno' => $uname,
                    'call_grace' => $date->format('Y-m-d'),
                    'call_snum' => $susp->f_snum,
                    'call_sic' => $susp->f_sic,
                    'call_cnum' => $susp->f_cnum,
                    'call_fdnum' => $susp->f_num,
                );
                if(!isset($susp->call_snum))$this->getsqlmod->addcalldoc($data);
                else $this->getsqlmod->updatecalldoc($value,$data);
            }
            redirect('Call/ProjectList/'); 
        }
        else redirect('Call/index/'); 
    }

    function table_DPProject1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getcpproject($id)->result(); // 
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('','專案編號','發文日期','類型');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $table_row = NULL;
            $table_row[] = $susp->cp_name;
            $table_row[] = anchor('Call/listcp/' . $susp->cp_name, $susp->cp_name);
            $table_row[] = $this->tranfer2RCyear2($susp->cp_date);
            $table_row[] = $susp->cp_type;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    
    public function ProjectList() {//(催繳專案)
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table_DPProject1($table);
        $data['s_table'] = $test_table;
        $data['title'] = "催繳專案列表";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Call/dpprojectlist';
        else $data['include'] = 'Call_query/dpprojectlist';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    function table_CP($id) {//催繳案件一般
        $this->load->library('table');
        $query = $this->getsqlmod->getCallListfromProject($id)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 100%;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','發文日期(催繳函)','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限','發文日期(罰緩)');
        $table_row = array();
        foreach ($query as $susp)
        {
            $date = new DateTime($susp->f_date); // Y-m-d
            $date->add(new DateInterval('P30D'));
            if($date->format('d')>25){
                $date->add(new DateInterval('P5D'));
            }
            $day = $date->format('d')%5;
            //$day = 6;
            if($day < 5){
                while($day  < 5){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            else if($day < 10){
                while($day  < 10){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->call_snum;
			$table_row[] = $this->tranfer2RCyear2($susp->call_date);
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->fd_zipcode;
            $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            $table_row[] = $this->tranfer2RCyear2($susp->f_date);
            $table_row[] = $this->tranfer2RCyear2($susp->fd_date);
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    public function listcp() {//處理專案
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $dp = $this->getsqlmod->getcpprojecttype($id)->result();
        $test_table = $this->table_CP($id);
        $data['s_table'] = $test_table;
        $data['title'] = "催繳專案:".$dp[0]->cp_name;
        $data['type'] = $dp[0]->cp_type;
        $data['sp'] = $dp[0]->cp_name;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Call/dplist1';
        else $data['include'] = 'Call_query/dplist1';
        $data['nav'] = 'navbar3';
        $data['id'] = $id;
        $this->load->view('template', $data);
    }
    
    public function listcp_ed() {//專案編輯（一般）
        $this->load->library('table');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getCallListfromProject($id)->result(); 
        $dp = $this->getsqlmod->getcpprojecttype($id)->result();
        $data['sp'] = $dp[0]->cp_name;
        $tmpl = array (
            'table_open' => '<table style="width: 2400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限','發文日期(罰緩)','發文日期(催繳函)','本號(催繳函文號)','支號','寬限期限');
		$str = "";
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
			$i++;
			// if($susp->fd_address == null || $susp->fd_address == '0'){
                $fd_addr = '<input name="fd_address['.$susp->call_snum.']" data-id="'.$susp->call_snum.'" required="required" class="form-control"  value="'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress.'" onkeypress="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')"  onchange="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')">';
				$fd_addr_txt = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            // }
            // else{
                // $fd_addr = '<input name="fd_address['.$susp->call_snum.']" data-id="'.$susp->call_snum.'" required="required"  value="'.$susp->fd_address.'" class="form-control" onkeypress="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')"  onchange="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')">';
				// $fd_addr_txt = $susp->fd_address;
            // }

			if($susp->call_bno == null || $susp->call_bno == '0'){
                $bno = '<input name="call_bno['.$susp->call_snum.']"  type="number" required="required" class="form-control" class="form-control" value='.$i.' >';
            }
            else{
                $bno = '<input name="call_bno['.$susp->call_snum.']"  type="number"  required="required" class="form-control" value='.$susp->call_bno.' >';
            }
			$str .= '<div class="panel panel-default" style="'.(($i%2 == 1)?'background-color:#eee;':'background-color:#FFF;').'">
				<div class="panel-body">
					<div class="row">
						<input type="hidden" name="call_snum" value="'.$susp->call_snum.'"/>
						<div class="form-group col-md-3">
							<label>處分書編號</label>
							<p class="text-primary">'.$susp->fd_num.'</p>
						</div>
						<div class="form-group col-md-3">
							<label>受處分人</label>
							<p>'.$susp->s_name.'</p>
						</div>
						<div class="form-group col-md-3">
							<label>郵遞區號</label>
							<input name="fd_zipcode['.$susp->call_snum.']" type="number" id="fd_zipcode_'.$susp->call_snum.'" required="required" value='.$susp->fd_zipcode.' class="form-control">
						</div>	
						<div class="form-group col-md-3">
							<label><input type="radio" name="sendto['.$susp->call_snum.']" value="'.$fd_addr_txt.'" '.((strcmp($susp->fd_address,$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress) == 0)?'checked':'').' onclick="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')"/>戶籍地址</label>
							'.$fd_addr.'
							<label><input type="radio" name="sendto['.$susp->call_snum.']" value="'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress.'"  '.((strcmp($susp->fd_address,$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress) == 0)?'checked':'').' onclick="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')"/>現住地址</label>
							<input type="text" class="form-control" name="raddress['.$susp->call_snum.']" value="'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress.'" onkeypress="listzipcode($(this), \'\')"  onchange="listzipcode($(this), \'\')"/>
						</div>
						<div class="form-group col-md-3">
							<label>繳納期限</label>
							<p>'.$this->tranfer2RCyear2($susp->f_date).'</p>
						</div>
						<div class="form-group col-md-3">
							<label>發文日期(罰緩)</label>
							<p>'.$this->tranfer2RCyear2($susp->fd_date).'</p>
						</div>
						<div class="form-group col-md-3">
							<label>發文日期(催繳函)</label>
							<input name="call_date['.$susp->call_snum.']" type="text" required="required" class="rcdate form-control" value="'.((isset($susp->call_date))?((strlen($susp->call_date) > 7 && $susp->call_date != '0000-00-00')?str_pad(((int)substr($susp->call_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->call_date, 5, 2).substr($susp->call_date, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md')).'"  >
							<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
						</div>
						<div class="form-group col-md-3">
							<label>本號(催繳函文號)</label>
							<input name="call_no['.$susp->call_snum.']"  required="required" value='.$susp->call_no.' class="form-control">
						</div>
						<div class="form-group col-md-3">
							<label>支號</label>
							'.$bno.'
						</div>
						<div class="form-group col-md-3">
							<label>寬限期限</label>
							<input name="call_grace['.$susp->call_snum.']" type="text" required="required" value='.((isset($susp->call_grace))?((strlen($susp->call_grace) > 7 && $susp->call_grace != '0000-00-00')?str_pad(((int)substr($susp->call_grace, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->call_grace, 5, 2).substr($susp->call_grace, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md')).' class="rcdate form-control">
							<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>

						</div>
					</div>
				</div>
			</div>';
            // $i++;
            // $date = new DateTime($susp->f_date); // Y-m-d
            // $date->add(new DateInterval('P30D'));
            // if($date->format('d')>25){
            //     $date->add(new DateInterval('P5D'));
            // }
            // $day = $date->format('d')%5;
            // //$day = 6;
            // if($day < 5){
            //     while($day  < 5){
            //         $day++;
            //         $date->add(new DateInterval('P1D'));
            //     }
            // }
            // else if($day < 10){
            //     while($day  < 10){
            //         $day++;
            //         $date->add(new DateInterval('P1D'));
            //     }
            // }
            // $table_row = NULL;
            // $table_row[] = $susp->call_snum;
            // $table_row[] = $susp->fd_num;
            // $table_row[] = $susp->s_name;
            // $table_row[] = '<input name="fd_zipcode['.$susp->call_snum.']" type="number" required="required" value='.$susp->fd_zipcode.'>';
            // if($susp->fd_address == null || $susp->fd_address == '0'){
            //     $table_row[] = '<input name="fd_address['.$susp->call_snum.']" required="required" class="form-control"  value="'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress.'" >';
            // }
            // else{
            //     $table_row[] = '<input name="fd_address['.$susp->call_snum.']" required="required" style="width:250px" value="'.$susp->fd_address.'" >';
            // }
            // $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            // $table_row[] = $susp->f_date;
            // $table_row[] = $susp->fd_date;
            // $table_row[] = '<input name="call_date['.$susp->call_snum.']" type="date" required="required" value='.$susp->call_date.' class="form-control" >';
            // $table_row[] = '<input name="call_no['.$susp->call_snum.']"  required="required" value='.$susp->call_no.' >';
            // if($susp->call_bno == null || $susp->call_bno == '0'){
            //     $table_row[] = '<input name="call_bno['.$susp->call_snum.']" style="width:30px" type="number" required="required" value='.$i.' >';
            // }
            // else{
            //     $table_row[] = '<input name="call_bno['.$susp->call_snum.']" style="width:30px" type="number"  required="required" value='.$susp->call_bno.' >';
            // }
            // $table_row[] = '<input name="call_grace['.$susp->call_snum.']" type="date" required="required" value='.$susp->call_grace.' >';
            // $this->table->add_row($table_row);
        }   
        $data['title'] = "催繳專案:".$id;
        $data['fpid'] = $id;
        if(isset($query[0]))$data['call_date'] = $query[0]->call_date;
        else $data['call_date'] = null;
        $data['user'] = $this -> session -> userdata('uic');
        // $data['s_table'] = $this->table->generate();
		$data['s_table'] = $str;
        $data['id'] = $id;
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Call/dplist_ed';
        else $data['include'] = 'Call_query/dplist_ed';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function listcp_ed_public() {//專案編輯（公示）
        $this->load->library('table');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getCallListfromProject($id)->result(); 
        $dp = $this->getsqlmod->getcpprojecttype($id)->result();
        $data['sp'] = $dp[0]->cp_name;
        $tmpl = array (
            'table_open' => '<table style="width: 2400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限','發文日期(罰緩)','發文日期(催繳函)','本號(催繳函文號)','支號','寬限期限','公示送達');
        $table_row = array();
        $i=0;
		$str = '';
        foreach ($query as $susp)
        {
			$i++;
			if($susp->fd_address == null || $susp->fd_address == '0'){
                $fd_addr = '<input name="fd_address['.$susp->call_snum.']" required="required" class="form-control"  value="'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress.'" onkeypress="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')"  onchange="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')" >';
            }
            else{
                $fd_addr = '<input name="fd_address['.$susp->call_snum.']" required="required"  value="'.$susp->fd_address.'" class="form-control" onkeypress="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')"  onchange="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')">';
            }

			if($susp->call_bno == null || $susp->call_bno == '0'){
                $bno = '<input name="call_bno['.$susp->call_snum.']"  type="number" required="required" class="form-control" class="form-control" value='.$i.' >';
            }
            else{
                $bno = '<input name="call_bno['.$susp->call_snum.']"  type="number"  required="required" class="form-control" value='.$susp->call_bno.' >';
            }

			if($susp->call_delivery_date != null){
				$deliver_date = $susp->call_delivery_date.' '.$susp->call_delivery_date;
			}
			else{
				$deliver_date = '<strong>！未輸入送達資料</strong>';
			}
			$data['call_delivery_date'] = $susp->call_delivery_date;
			$data['call_date'] = $susp->call_date;
			$str .= '<div class="panel panel-default" style="'.(($i%2 == 1)?'background-color:#eee;':'background-color:#FFF;').'">
				<div class="panel-body">
					<div class="row">
						<input type="hidden" name="call_snum" value="'.$susp->call_snum.'"/>
						<div class="form-group col-md-3">
							<label>處分書編號</label>
							<p class="text-primary">'.$susp->fd_num.'</p>
						</div>
						<div class="form-group col-md-3">
							<label>受處分人</label>
							<p>'.$susp->s_name.'</p>
						</div>
						<div class="form-group col-md-3">
							<label>郵遞區號</label>
							<input name="fd_zipcode['.$susp->call_snum.']" type="number" id="fd_zipcode_'.$susp->call_snum.'" required="required" value='.$susp->fd_zipcode.' class="form-control">
						</div>	
						<div class="form-group col-md-3">
							<label>戶籍地址</label>
							'.$fd_addr.'
						</div>
						<div class="form-group col-md-3">
							<label>現住地址</label>
							<p>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress.'</p>
						</div>
						<div class="form-group col-md-3">
							<label>繳納期限</label>
							<p>'.$this->tranfer2RCyear2($susp->f_date).'</p>
						</div>
						<div class="form-group col-md-3">
							<label>發文日期(罰緩)</label>
							<p>'.$this->tranfer2RCyear2($susp->fd_date).'</p>
						</div>
						<div class="form-group col-md-3">
							<label>發文日期(催繳函)</label>
							<input name="call_date['.$susp->call_snum.']" type="text" required="required" class="rcdate form-control" value="'.((isset($susp->call_date))?((strlen($susp->call_date) > 7 && $susp->call_date != '0000-00-00')?str_pad(((int)substr($susp->call_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->call_date, 5, 2).substr($susp->call_date, 8, 2):((int)date('Y') - 1911).date('md')):'').'"  >
							<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
						</div>
						<div class="form-group col-md-3">
							<label>本號(催繳函文號)</label>
							<input name="call_no['.$susp->call_snum.']"  required="required" value='.$susp->call_no.' class="form-control">
						</div>
						<div class="form-group col-md-3">
							<label>支號</label>
							'.$bno.'
						</div>
						<div class="form-group col-md-3">
							<label>寬限期限</label>
							<input name="call_grace['.$susp->call_snum.']" type="text" required="required" value='.((isset($susp->call_grace))?((strlen($susp->call_grace) > 7 && $susp->call_grace != '0000-00-00')?str_pad(((int)substr($susp->call_grace, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->call_grace, 5, 2).substr($susp->call_grace, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md')).' class="rcdate form-control">
							<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>

						</div>
						<div class="form-group col-md-3">
							<label>公示送達</label>
							<p>'.$deliver_date.'</p>
						</div>
					</div>
				</div>
			</div>';
            // $i++;
            // $date = new DateTime($susp->f_date); // Y-m-d
            // $date->add(new DateInterval('P30D'));
            // if($date->format('d')>25){
            //     $date->add(new DateInterval('P5D'));
            // }
            // $day = $date->format('d')%5;
            // //$day = 6;
            // if($day < 5){
            //     while($day  < 5){
            //         $day++;
            //         $date->add(new DateInterval('P1D'));
            //     }
            // }
            // else if($day < 10){
            //     while($day  <span 10){
            //         $day++;
            //         $date->add(new DateInterval('P1D'));
            //     }
            // }
            // $table_row = NULL;
            // $table_row[] = $susp->call_snum;
            // $table_row[] = $susp->fd_num;
            // $table_row[] = $susp->s_name;
            // $table_row[] = '<input name="fd_zipcode['.$susp->call_snum.']" type="number" required="required" value='.$susp->fd_zipcode.'>';
            // if($susp->fd_address == null || $susp->fd_address == '0'){
            //     $table_row[] = '<input name="fd_address['.$susp->call_snum.']" required="required" class="form-control"  value="'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress.'" >';
            // }
            // else{
            //     $table_row[] = '<input name="fd_address['.$susp->call_snum.']" required="required" style="width:250px" value="'.$susp->fd_address.'" >';
            // }
            // $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            // $table_row[] = $susp->f_date;
            // $table_row[] = $susp->fd_date;
            // $table_row[] = '<input name="call_date['.$susp->call_snum.']" type="date" required="required" value='.$susp->call_date.' class="form-control" >';
            // $table_row[] = '<input name="call_no['.$susp->call_snum.']"  required="required" type="number" value='.$susp->call_no.' >';
            // if($susp->call_bno == null || $susp->call_bno == '0'){
            //     $table_row[] = '<input name="call_bno['.$susp->call_snum.']" style="width:30px" type="number" required="required" value='.$i.' >';
            // }
            // else{
            //     $table_row[] = '<input name="call_bno['.$susp->call_snum.']" style="width:30px" type="number" required="required" value='.$susp->call_bno.' >';
            // }
            // $table_row[] = '<input name="call_grace['.$susp->call_snum.']" type="date" required="required" value='.$susp->call_grace.' >';
            // if($susp->call_delivery_date != null){
            //     $table_row[] = $susp->call_delivery_date.' '.$susp->call_delivery_date;
            // }
            // else{
            //     $table_row[] = '<strong>！未輸入送達資料</strong>';
            // }
            // $this->table->add_row($table_row);
        }   
		
        $data['title'] = "公示催繳專案:".$id;
        $data['fpid'] = $id;
        $data['user'] = $this -> session -> userdata('uic');
        // $data['s_table'] = $this->table->generate();
		$data['s_table'] = $str;
        $data['id'] = $id;
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Call/dplist_ed_public';
        else $data['include'] = 'Call_query/dplist_ed_public';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function updateCPDate(){
        //var_dump($_POST);
        $this->getsqlmod->updateCPDate($_POST['call_no'],$this->tranfer2ADyear($_POST['call_date']));
        redirect('Call/listcp_ed/'.$_POST['call_no']); 
    }
    
    public function updateCPDatepublic(){
        //var_dump($_POST);
        $this->getsqlmod->updateCPDate($_POST['call_no'],$this->tranfer2ADyear($_POST['call_date']));
        redirect('Call/listcp_ed_public/'.$_POST['call_no']); 
    }
    
    public function updatepublicdate(){
        //var_dump($_POST);
        $this->getsqlmod->updatepublicdate($_POST['call_no'],$this->tranfer2ADyear($_POST['call_date']));
        redirect('Call/listcp_ed_public/'.$_POST['call_no']); 
    }
    
    public function updateCP(){
        // var_dump($_POST);
		// exit;
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        var_dump($_POST);
        foreach ($s_cnum1 as $key => $value) {
            $fine_doc = array(
                'fd_address' => $_POST['sendto'][$value],
                'fd_zipcode' => $_POST['fd_zipcode'][$value],
            );
            $call = array(
                'call_date' => $this->tranfer2ADyear($_POST['call_date'][$value]),
                'call_no' => $_POST['call_no'][$value],
                'call_bno' => $_POST['call_bno'][$value],
                'call_grace' => $this->tranfer2ADyear($_POST['call_grace'][$value]),
            );
            //var_dump($fine_doc);
            //echo '<br>';
            //var_dump($call);
            $this->getsqlmod->updateFD($value,$fine_doc);
            $this->getsqlmod->updatecalldoc($value,$call);
        };
        redirect('Call/listcp/'.$_POST['call_projectnum']); 
    }
    
    public function updatestatus_ready() {
        $this->load->helper('form');
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $this->getsqlmod->updateCall_Project_sp($value,'寄出');

				$updatedata2 = array(
					'call_maildate' => date('Y-m-d'),
					'call_delivery_status' => '已寄出'
				);
	
				$this->getsqlmod->updateCall_delivery($value, $updatedata2);

            }
            redirect('Call/ProjectList/'); 
        }
    }    
    
    public function listdelivery() {//催繳送達登入
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $this->load->library('table');
        $query = $this->getsqlmod->getCalldelivery()->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 100%;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        // $this->table->set_heading( '','罰緩列管編號','發文對象','發文時間', '送達情形', '送達情形修改','送達日期');
		$this->table->set_heading( '','退回/重處','處分書編號','送達日期');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $table_row = NULL;
            $table_row[] = $susp->s_num;
			$table_row[] = ($susp->call_delivery_status != '公示送達')?anchor('Call/rollback_call/'.$susp->s_num .'/'.$susp->call_maildate , '退回', array('class' => 'text-danger')):anchor('#/'.$susp->s_num .'/'.$susp->call_maildate , '移送', array('class' => 'text-danger'));
            $table_row[] = $susp->fd_num . ' (' . $susp->fd_target . ')'.(($susp->call_delivery_status == '公示送達')?'【公示送達】':'');

            // $table_row[] = $susp->fd_num;
            // $table_row[] = $susp->fd_target;
            // $table_row[] = $susp->call_date;
            // if($susp->call_delivery_date == null){
            //     $table_row[] = '<strong>未登入</strong>';
            // }
            // else{
            //     $table_row[] = $susp->call_delivery_status.'；'.$susp->call_delivery_date;
            // }
            // if(preg_match("/c/i", $this->session-> userdata('3permit'))){
            //     if($susp->call_delivery_status == '已送達'){
            //         $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //                 <option selected value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else if($susp->call_delivery_status == '現住地未送達'){
            //         $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option selected value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else if($susp->call_delivery_status == '戶籍地未送達'){
            //         $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option selected value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else if($susp->call_delivery_status == '公示送達'){
            //         $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option selected value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else if($susp->call_delivery_status == '已出監'){
            //         $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option selected value="已出監">已出監</option>
            //                 <option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else{
            //         $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            // }
            // else $table_row[] = $susp->call_delivery_status;
			$call_delivery_date = strtotime($susp->call_delivery_date);
			$today = strtotime(date('Y-m-d'));  
			$daydiff = round(($today - $call_delivery_date)/3600/24) ;
            if(preg_match("/a/i", $this->session-> userdata('3permit'))){
                if($susp->call_delivery_date == null){
                    $table_row[] = '<input id="call_delivery_date" name="call_delivery_date['.$susp->s_num.']" value="" type="text" class="rcdate form-control"/> <span class="'.(($susp->call_delivery_status == '公示送達')?'':(($daydiff >=30)?'text-danger':'text-primary')).'">寄送日：'.$this->tranfer2RCyear($susp->call_maildate).'</span>
					<br/><span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>';
					
                }
                else{
                    $table_row[] = '<input id="call_delivery_date" name="call_delivery_date['.$susp->s_num.']" value="'. $this->tranfer2RCyear($susp->call_delivery_date).'"  type="text" class="rcdate form-control"/> '.(($susp->call_delivery_status == '公示送達')?'<span class="text-primary">到期日：'.date('Y-m-d', strtotime($susp->call_delivery_date . " +30 days")).'</span>':'').'
					<br/><span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>';
                }
            }
            // if(preg_match("/c/i", $this->session-> userdata('3permit'))){
            //     if($susp->call_delivery_date == null){
            //         $table_row[] = '<input id="call_delivery_date" name="call_delivery_date['.$susp->s_num.']" value="'.$today.'"type="date"/> ';
            //     }
            //     else{
            //         $table_row[] = '<input id="call_delivery_date" name="call_delivery_date['.$susp->s_num.']" value="'.$susp->call_delivery_date.'"type="date"/> ';
            //     }
            //     $this->table->add_row($table_row);
            // }
            // else $table_row[] = $susp->call_delivery_date;
			$this->table->add_row($table_row);
        }   
        $test_table = $this->table->generate();
        $data['s_table'] = $test_table;
        $data['title'] = "所有已催繳案件";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Call/listdelivery';
        else $data['include'] = 'Call_query/listdelivery';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }   
    public function rollback_call(){
		$id = $this->uri->segment(3);
		$maildate = $this->uri->segment(4);
		$data = array(
			'call_delivery_date' => null,
			'call_delivery_status' => null,
			'call_projectnum' => null,
		);
		$this->getsqlmod->updateCalldelivery($id,$data);
		redirect('Call/listdelivery','refresh' );
	}
    public function updatedelivery(){
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        foreach ($s_cnum1 as $key => $value) {
            // if($_POST['call_delivery_status'][$value] == '戶籍地未送達'){
            //     $data = array(
            //         'call_delivery_date' => $_POST['call_delivery_date'][$value],
            //         'call_delivery_status' => $_POST['call_delivery_status'][$value],
            //         'call_projectnum' => null,
            //     );
            //     $this->getsqlmod->updateCalldelivery($value,$data);
            // }
            // else{
                $data = array(
                    'call_delivery_date' => $this->tranfer2ADyear($_POST['call_delivery_date'][$value]),
                    'call_delivery_status' => '已送達',
                );
                $this->getsqlmod->updateCalldelivery($value,$data);
            // }
            //var_dump($data);
            //echo '<br>';
        };
        redirect('Call/listdelivery/'); 
    }

    public function exportpublicCSV(){//公示清冊
        $this->load->dbutil();
        $this->load->helper('download');
        $id = $this->uri->segment(3);
        $filename = '催繳公示清冊'.date('Ymd').'.csv';
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/vnd.ms-excel; "); 
        $usersData = $this->getsqlmod->getCallListfromProject($id)->result();
        $file = fopen('php://output', 'a');
        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
        $header = array("編號","受處分人姓名","出生年份","身份證統一編號","處分書編號","備註"); 
        fputcsv($file, $header);
        $i=0;
        foreach ($usersData as $key=>$line){ 
            $i++;
            $birth = (date('Y', strtotime($line->s_birth))-1911);
            $fine = '罰緩新臺幣'.$line->f_damount/10000 . '萬元';
            fputcsv($file,array($i,$line->s_name,$birth,$line->s_ic,$line->fd_num,$fine));
        }
        fclose($file); 
        exit;
        //$query = $this->getsqlmod->getfine_list($id); 
        //$data = $this->dbutil->csv_from_result($query);
        //force_download($filename,  $data);
    }
	/** 轉民國年 */
    public function tranfer2RCyear($date)
    {
        $datestr = str_replace('-', '', $date);
        
        $rc = ((int)substr($datestr, 0, 4)) - 1911;
        return (string)$rc . substr($datestr, 4, 4) ;
    }
    /** 轉民國年 */
    public function tranfer2RCyear2($date)
    {
		
		if(empty($date))
		{
			return '<strong>無相關日期</strong>';
		}
		elseif($date == '0000-00-00')
		{
			return '<strong>無相關日期</strong>';
		}
		else{
			
			$datestr = explode('-', trim($date));
        
			$rc = ((int)$datestr[0]) - 1911;
			return (string)$rc . '-' . $datestr[1] . '-' .$datestr[2] ;
		}
        
    }
	/** 轉西元年 */
    public function tranfer2ADyear($date)
    {
        if(substr($date, 0, 1) == 0 || substr($date, 0, 1) == '0')
        {
            $date = substr($date, 1, 6);            
        }
            
        if(strlen($date) == 6)
        {
            $ad = ((int)substr($date, 0, 2)) + 1911;
            return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
        }
        elseif(strlen($date) == 7)
        {
            
            $ad = ((int)substr($date, 0, 3)) + 1911;
            return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
        }
        else
        {
            return '';
        }

        
    }
}
