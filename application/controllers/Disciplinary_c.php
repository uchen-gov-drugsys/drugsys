<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Disciplinary_c extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }

    function table_dp($id) {//
        $this->load->library('table');
        // $query = $this->getsqlmod->getdp($id)->result(); 
		$query = $this->getsqlmod->getnewdp($id)->result(); 
		
        $tmpl = array (
            'table_open' => '<table style="width: 1600px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        // $this->table->set_heading( '','','處分書編號','發文字號','受處分人','身份證號','移送分局', '依據單位（字）', '戶籍','現住地址','出生日期','查獲時間','查獲地點','查獲單位','檢驗級別成分','尿液編號','持有毒品','毒報','尿報','');
		$this->table->set_heading( '','','','處分書編號','受處分人','身份證號','移送分局', '依據單位（字）', '戶籍','現住地址','出生日期','查獲時間','查獲地點','查獲單位'/*,'檢驗級別成分'*/,'尿液編號','持有毒品');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            $drug = $this->getsqlmod->get3Drug($susp->s_ic); // 抓sic
            // $drug_doc = $this->getsqlmod->get1Drug($susp->s_cnum)->result(); // 抓scnum
            $table_row = NULL;
            $table_row[] = $susp->s_num;
			$table_row[] = '';
			$table_row[] = anchor('disciplinary_c/returnSanc/' . $susp->s_num, '退回補正');
            $table_row[] = $susp->fd_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $sc_ingredient = Null;
            $drug_ingredient = Null;
            foreach ($query1->result() as $susp1){
                if($susp1->sc_num == null){}
                else{
                    $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.'<br>';
                }
				
            }
            foreach ($drug->result() as $drug3){
                if($drug3->ddc_num == null){}
                else{
                    $drug_ingredient = $drug_ingredient . $drug3->ddc_level .'級' . $drug3->ddc_ingredient.'<br>';
                }
            }
            // if($susp->s_go_no == null){
            //     $table_row[] = '<strong>未輸入</strong>';
            // }
            // else{
            //     $table_row[] = $susp->s_go_no;
            // }
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            $table_row[] = $susp->s_ic;
            $table_row[] = $susp->s_roffice;
            $table_row[] = $susp->s_roffice1;
            if($susp->s_dpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                // $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpzipcode.$susp->s_dpaddress;
				// $table_row[] = $this->getzipcode33($susp->s_dpaddress).$susp->s_dpaddress;
				$table_row[] = $susp->s_dpaddress;
            }
            if($susp->s_rpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                // $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpzipcode.$susp->s_rpaddress;
				// $table_row[] = $this->getzipcode33($susp->s_rpaddress).$susp->s_rpaddress;
				$table_row[] = $susp->s_rpaddress;
            }
            if($susp->s_birth == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad($susp->s_birth);
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                // $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
				$table_row[] = $susp->s_place;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            // if($sc_ingredient==null){
            //     $table_row[] = '<strong>未選擇</strong>';
            // }
            // else{
            //     $table_row[] = $sc_ingredient;
            // }
            if($susp->s_utest_num==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $susp->s_utest_num;
            }

			$table_row[] = $susp->fd_drug_msg;
            // if($drug_ingredient==null){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = $drug_ingredient;
            // }
            // if(!isset($drug_doc[0]->e_doc)){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('drugdoc/' . $drug_doc[0]->e_doc, '毒報');
            // }
            // if($susp->s_utest_doc==null){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('drugdoc/' . $susp->s_utest_doc, '尿報');
            // }
            // if(preg_match("/a/i", $this->session-> userdata('3permit')))$table_row[] = anchor('disciplinary_c/returnSanc/' . $susp->s_num, '退回補正');
            // else $table_row[] = '';
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

	function table_dp_rollback($id) {//
        $this->load->library('table');
        $query = $this->getsqlmod->getrollbackdp($id)->result(); 
		// var_dump($query);
		// exit;
        $tmpl = array (
            'table_open' => '<table style="width: 100%;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table_rollback">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','','處分書編號','受處分人','身份證號','移送分局', '依據單位（字）', '戶籍','現住地址','出生日期','查獲時間','查獲地點','查獲單位','檢驗級別成分','尿液編號','持有毒品');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $query1 = $this->getsqlmod->get2Susp($susp->fd_snum); // 抓snum
            $drug = $this->getsqlmod->get3Drug($susp->	fd_sic); // 抓sic
            // $drug_doc = $this->getsqlmod->get1Drug($susp->s_cnum)->result(); // 抓scnum
            $table_row = NULL;
            $table_row[] = $susp->fd_snum;
			$table_row[] = '';
            $table_row[] = $susp->fd_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $sc_ingredient = Null;
            $drug_ingredient = Null;
            foreach ($query1->result() as $susp1){
                if($susp1->sc_num == null){}
                else{
                    $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.'<br>';
                }
            }
            foreach ($drug->result() as $drug3){
                if($drug3->ddc_num == null){}
                else{
                    $drug_ingredient = $drug_ingredient . $drug3->ddc_level .'級' . $drug3->ddc_ingredient.'<br>';
                }
            }
            
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            $table_row[] = $susp->s_ic;
            $table_row[] = $susp->s_roffice;
            $table_row[] = $susp->s_roffice1;
            if($susp->s_dpzipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpzipcode.$susp->s_dpaddress;
            }
            if($susp->s_rpzipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpzipcode.$susp->s_rpaddress;
            }
            if($susp->s_birth == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_birth;
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            if($sc_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $sc_ingredient;
            }
            if($susp->s_utest_num==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $susp->s_utest_num;
            }
            if($drug_ingredient==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }
            // if(!isset($drug_doc[0]->e_doc)){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('drugdoc/' . $drug_doc[0]->e_doc, '毒報');
            // }
            // if($susp->s_utest_doc==null){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('drugdoc/' . $susp->s_utest_doc, '尿報');
            // }
            // if(preg_match("/a/i", $this->session-> userdata('3permit')))$table_row[] = anchor('disciplinary_c/returnSanc/' . $susp->s_num, '退回補正');
            // else $table_row[] = '';
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    function table_delivery($id) {//送達登入列表
        $this->load->library('table');
        $query = $this->getsqlmod->getdeliverylist()->result(); 
		// var_dump($query);
		// exit;
        $tmpl = array (
            'table_open' => '<table style="width: 1400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        if(preg_match("/a/i", $this->session-> userdata('3permit'))) $this->table->set_heading( '','退回/重處','處分書編號'/*,'發文對象','發文地址','發文時間', '送達情形', '送達情形修改'*/,'送達日期','送達證書');
        else $this->table->set_heading( '','退回/重處','處分書編號'/*,'發文對象','發文地址','發文時間', '送達情形'*/, '送達證書');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $i++;
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            $drug = $this->getsqlmod->get3Drug($susp->s_ic); // 抓sic
            $drug_doc = $this->getsqlmod->get1Drug($susp->s_cnum)->result(); // 抓scnum
            $table_row = NULL;
            $table_row[] = $susp->s_num;
			$table_row[] = anchor('disciplinary_c/rollback_fd/'.$susp->s_num .'/'.$susp->fdd_maildate , '退回/重處', array('class' => 'text-danger'));
            $table_row[] = $susp->fd_num . ' (' . $susp->fd_target . ')';
            // $table_row[] = $susp->fd_target;
            // $table_row[] = $susp->fd_address;
            // $table_row[] = $susp->fd_date;
            // if($susp->fdd_date == null){
            //     $table_row[] = '<strong>未登入</strong>';
            // }
            // else{
            //     $table_row[] = $susp->fdd_status.'；'.$susp->fdd_date;
            // }
            // if(preg_match("/a/i", $this->session-> userdata('3permit'))){
            //     if($susp->fdd_status == '已送達'){
            //         $table_row[] = '<select id="fdd_status" name="fdd_status['.$susp->s_num.']" type="text" >
            //                 <option selected value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else if($susp->fdd_status == '現住地未送達'){
            //         $table_row[] = '<select id="fdd_status" name="fdd_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option selected value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else if($susp->fdd_status == '戶籍地未送達'){
            //         $table_row[] = '<select id="fdd_status" name="fdd_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option selected value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else if($susp->fdd_status == '已超過1月半無送達'){
            //         $table_row[] = '<select id="fdd_status" name="fdd_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option selected value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else if($susp->fdd_status == '已出監'){
            //         $table_row[] = '<select id="fdd_status" name="fdd_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="已超過1月半無送達">已超過1月半無送達</option><option selected value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else if($susp->fdd_status == '公示送達'){
            //         $table_row[] = '<select id="fdd_status" name="fdd_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            //     else{
            //         $table_row[] = '<select id="fdd_status" name="fdd_status['.$susp->s_num.']" type="text" >
            //                 <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //                 <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //                 <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
            //                 </select>';
            //     }
            // }
			$fdd_maildate = strtotime($susp->fdd_maildate);
			$today = strtotime(date('Y-m-d'));  
			$daydiff = round(($today - $fdd_maildate)/3600/24) ;
            if(preg_match("/a/i", $this->session-> userdata('3permit'))){
                if($susp->fdd_date == null){
                    $table_row[] = '<input id="fdd_date" name="fdd_date['.$susp->s_num.']" value="" type="text" class="rcdate form-control"/> <span class="'.(($daydiff >=45)?'text-danger':'text-primary').'">寄送日：'.$this->tranfer2RCyear($susp->fdd_maildate).(($daydiff >=45)?'(已超過45天未送達)':'').'</span>';
                }
                else{
                    $table_row[] = '<input id="fdd_date" name="fdd_date['.$susp->s_num.']" value="'. $this->tranfer2RCyear($susp->fdd_date).'"  type="text" class="rcdate form-control"/> ';
                }
            }
            if($susp->fdd_doc == null){
                $table_row[] = '<strong>無送達文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->fdd_doc, '送達證書');
            }

			
			$this->table->add_row($table_row);
            
        }   
        return $this->table->generate();
    }

	function table_delivery_success($id) {//送達登入列表
        $this->load->library('table');
        $query = $this->getsqlmod->getdeliverysuccesslist()->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table2">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
		$this->table->set_heading('退回/重處', '處分書編號','送達日期','送達證書');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $i++;
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            $drug = $this->getsqlmod->get3Drug($susp->s_ic); // 抓sic
            $drug_doc = $this->getsqlmod->get1Drug($susp->s_cnum)->result(); // 抓scnum
            $table_row = NULL;
			$table_row[] = anchor('disciplinary_c/rollback_fd/'.$susp->s_num .'/'.$susp->fdd_date , '退回/重處', array('class' => 'text-danger'));
            $table_row[] = $susp->fd_num . ' (' . $susp->fd_target . ')';
            
            $table_row[] = $this->tranfer2RCyear($susp->fdd_date);
            if($susp->fdd_doc == null){
                $table_row[] = '<strong>無送達文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->fdd_doc, '送達證書');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    function table_Publicity($id) {//公示送達列表
        $this->load->library('table');
        $query = $this->getsqlmod->getdeliverylist_forpublic()->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','發文對象','發文地址','發文時間', '送達情形', '送達證書');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->fd_target;
            $table_row[] = $susp->fd_address;
            $table_row[] = $susp->fd_date;
            if($susp->fdd_date == null){
                $table_row[] = '<strong>未登入</strong>';
            }
            else{
                $table_row[] = $susp->fdd_status.'；'.$susp->fdd_date;
            }
            if($susp->fdd_doc == null){
                $table_row[] = '<strong>無送達文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->fdd_doc, '送達證書');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    function table_Publicity1() {//公示送達專案列表
        $this->load->library('table');
        $query = $this->getsqlmod->getfdproject1()->result(); 
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號', '公示送達時間','公報上傳');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $table_row = NULL;
            $table_row[] = $susp->fdp_num;
            $table_row[] = anchor('Disciplinary_c/listPublicityProject/'.$susp->fpd_no, $susp->dp_name);

			if($susp->fdp_delivery_date == null){
				$table_row[] = '<input id="fdp_delivery_date" name="fdp_delivery_date['.$susp->fdp_num.']" value="" type="text" class="rcdate form-control"/> <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>';
			}
			else{
				$table_row[] = $this->tranfer2RCyear($susp->fdp_delivery_date);
			}

            // if($susp->fdp_delivery_date == null){
            //     $table_row[] = '<input name="fdp_delivery_date['.$susp->fdp_num.']" type="date" value="'.$susp->fdp_delivery_date.'"/> ';
            // }
            // else{
            //     $table_row[] = '<input name="fdp_delivery_date['.$susp->fdp_num.']" type="date" value="'.$susp->fdp_delivery_date.'"/> ';
            // }
            if($susp->fdp_an == null){
                $table_row[] = form_upload('files['.$susp->fdp_num.']','','');
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->fdp_an, '公報').form_hidden('fdp_an['.$susp->fdp_num.']', $susp->fdp_an);
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    function table_PublicityProject($id) {//公示送達列表
        $this->load->library('table');
        $query = $this->getsqlmod->getdeliverylist_forpublicProject($id)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1600px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','發文對象','發文地址','發文時間', '送達情形', '送達證書');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->fd_target;
            $table_row[] = $susp->fd_address;
            $table_row[] = $susp->fd_date;
            if($susp->fdd_date == null){
                $table_row[] = '<strong>未登入</strong>';
            }
            else{
                $table_row[] = $susp->fdd_status.'；'.$susp->fdd_date;
            }
            if($susp->fdd_doc == null){
                $table_row[] = '<strong>無送達文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->fdd_doc, '送達證書');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    function table_dp1($table,$id,$type) {//
        $this->load->library('table');
        $query = $this->getsqlmod->getfp3($id[0]->dp_name)->result(); 
        $tmpl = array (
            'table_open' => '<table border="0" style="width:2400px" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','裁處','處分書編號','發文字號'/*,'涉刑事案件司法文書'*/,'身份證', '犯嫌人姓名', '戶籍','現住地址','出生日期','查獲時間','查獲地點','查獲單位'/*,'檢驗級別成分'*/,'尿液編號','持有毒品','毒報','尿報','公文函');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            // $drug = $this->getsqlmod->get3Drug($susp->s_ic); // 抓sic 
			$drug = $this->getsqlmod->get3Drugfirst($susp->s_num); 
            // $drug_doc = $this->getsqlmod->get1Drug($susp->s_cnum)->result(); // 抓scnum
			$drug_doc = $this->getsqlmod->getCases($susp->s_cnum)->result();
            $table_row = NULL;
            //$table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $sc_ingredient = Null;
            $drug_ingredient = Null;
            $table_row[] = $susp->s_num;
			if($type == 'listdp1')
			{
				if(preg_match("/a/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('disciplinary_c/editSanc/' . $susp->s_num . '/' . $id[0]->dp_num, '編輯');
				else $table_row[] ='';
			}
			else
			{
				if(preg_match("/a/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('disciplinary_c/editSanc_ready/' . $susp->s_num . '/' . $id[0]->dp_num, '編輯');
				else $table_row[] ='';
			}
			
            $table_row[] = $susp->fd_num;
            foreach ($query1->result() as $susp1){
                if($susp1->sc_num == null){}
                else{
                    $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.'<br>';
                }
            }
            foreach ($drug->result() as $drug3){
                if($drug3->df_num == null){}
                else{
                    $drug_ingredient = $drug_ingredient . $drug3->df_level .'級' . $drug3->df_ingredient.'<br>';
                }
            }
            if($susp->fd_send_num == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->fd_send_num;
            }
            // if($susp->sp_doc == null){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('/drugdoc/' . $susp->sp_doc, '司法文書');
            //     //$table_row[] = $susp->sp_doc;
            // }
            $table_row[] = $susp->s_ic;
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_dpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpzipcode.$susp->s_dpaddress;
            }
            if($susp->s_rpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpzipcode.$susp->s_rpaddress;
            }
            if($susp->s_birth == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad($susp->s_birth);
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad2($susp->s_date);
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_place;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            // if($sc_ingredient==null){
            //     $table_row[] = '<strong>未選擇</strong>';
            // }
            // else{
            //     $table_row[] = $sc_ingredient;
            // }
            if($susp->s_utest_num==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $susp->s_utest_num;
            }
			// $table_row[] = $susp->fd_drug_msg;
            if($drug_ingredient==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }
            if(!isset($drug_doc[0]->drug_doc)){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $drug_doc[0]->drug_doc, '毒報');
            }
            if($susp->s_utest_doc==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('utest/' . $susp->s_utest_doc, '尿報');
            }
            /*if($susp->s_sac_state == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->s;
            }*/
            $table_row[] = '';
            
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

	function table_dprb1($table,$id) {//
        $this->load->library('table');
        $query = $this->getsqlmod->getfp3($id[0]->dp_name)->result(); 
        $tmpl = array (
            'table_open' => '<table border="0" style="width:2400px" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','裁處','處分書編號','發文字號'/*,'涉刑事案件司法文書'*/,'身份證', '犯嫌人姓名', '戶籍','現住地址','出生日期','查獲時間','查獲地點','查獲單位'/*,'檢驗級別成分'*/,'尿液編號','持有毒品','毒報','尿報','公文函');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            $drug = $this->getsqlmod->get3Drug($susp->s_ic); // 抓sic
            $drug_doc = $this->getsqlmod->get1Drug($susp->s_cnum)->result(); // 抓scnum
            $table_row = NULL;
            //$table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $sc_ingredient = Null;
            $drug_ingredient = Null;
            $table_row[] = $susp->s_num;
			if(preg_match("/a/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('disciplinary_c/editSanc_ready/' . $susp->s_num . '/' . $id[0]->dp_num, '編輯');
            else $table_row[] ='';
            $table_row[] = $susp->fd_num;
            foreach ($query1->result() as $susp1){
                if($susp1->sc_num == null){}
                else{
                    $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.'<br>';
                }
            }
            foreach ($drug->result() as $drug3){
                if($drug3->ddc_num == null){}
                else{
                    $drug_ingredient = $drug_ingredient . $drug3->ddc_level .'級『' . $drug3->ddc_ingredient.'』'.$drug3->e_name.$drug3->e_count.$drug3->e_type.'<br>';
                }
            }
            if($susp->fd_send_num == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->fd_send_num;
            }
            // if($susp->sp_doc == null){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('/drugdoc/' . $susp->sp_doc, '司法文書');
            //     //$table_row[] = $susp->sp_doc;
            // }
            $table_row[] = $susp->s_ic;
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_dpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpzipcode.$susp->s_dpaddress;
            }
            if($susp->s_rpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpzipcode.$susp->s_rpaddress;
            }
            if($susp->s_birth == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad($susp->s_birth);
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_place;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            // if($sc_ingredient==null){
            //     $table_row[] = '<strong>未選擇</strong>';
            // }
            // else{
            //     $table_row[] = $sc_ingredient;
            // }
            if($susp->s_utest_num==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $susp->s_utest_num;
            }
			$table_row[] = $susp->fd_drug_msg;
            // if($drug_ingredient==null){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = $drug_ingredient;
            // }
            if(!isset($drug_doc[0]->e_doc)){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $drug_doc[0]->e_doc, '毒報');
            }
            if($susp->s_utest_doc==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('utest/' . $susp->s_utest_doc, '尿報');
            }
            /*if($susp->s_sac_state == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->s;
            }*/
            $table_row[] = '';
            
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_drug2($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Drug($id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '毒品編號', '數量', '毒品名稱','檢驗成分','純質淨重(g)','各級純質淨重(g)','毒報', '');
        $table_row = array();
        foreach ($query->result() as $drug)
        {
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            $query1 = $this->getsqlmod->get1DrugCk($drug->e_id); // 初驗
            $query2 = $this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id); // 檢驗
            $table_row = NULL;
            if(preg_match("/a/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('disciplinary_c/editdrug2/' . $drug->e_id, $drug->e_id);
            else $table_row[] = $drug->e_id;
            if($drug->e_type == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug->e_count . $drug->e_type;
            }
            if($drug->e_name==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug->e_name;
            }
            $drug2val=Null;
            $drug2nw=Null;
            $drug2count=Null;
            foreach ($query2->result() as $drug2){
                if($drug2->ddc_ingredient == null){
                    $drug2val = Null;
                    $drug2nw = Null;
                }
                else{
                    $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
                    $drug2nw = $drug2nw . $drug2->ddc_nw . '<br>';
                    switch ($drug2->ddc_level) {
                        case 1:
                            $drugcount[0] = $drugcount[0] + $drug2->ddc_nw;
                            break;
                        case 2:
                            $drugcount[1] = $drugcount[1] + $drug2->ddc_nw;
                            break;
                        case 3:
                            $drugcount[2] = $drugcount[2] + $drug2->ddc_nw;
                            break;
                        case 4:
                            $drugcount[3] = $drugcount[3] + $drug2->ddc_nw;
                            break;
                        case 5:
                            $drugcount[4] = $drugcount[4] + $drug2->ddc_nw;
                            break;
                    }
                }
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2nw;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                if($drugcount[0]>0){
                    $drug2count = '1級 = ' . $drugcount[0] . '<br>';
                }
                if($drugcount[1]>0){
                    $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
                }
                if($drugcount[2]>0){
                    $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
                }
                if($drugcount[3]>0){
                    $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
                }
                if($drugcount[4]>0){
                    $drug2count = $drug2count . '5級 = ' . $drugcount[4];
                }
                $table_row[] = $drug2count;
                //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
            }
            if($drug->e_doc==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $drug->e_doc, '毒報');
            }
            if(preg_match("/a/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('Disciplinary_c/deldrug/' . $drug->e_id, '刪除');
            else $table_row[] = '';
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    function deldrug(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $query=$this->getsqlmod->get1Drug_id($id)->result();
        //echo $query[0]->e_c_num;
        $this->getsqlmod->deleteDrugID($id);
        ////var_dump ($source[0]);
        redirect('Disciplinary_c/othernew/'.$query[0]->e_c_num,'refresh');
    }

    function table_susp2($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Susp($id)->result(); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '身份證', '犯嫌人姓名', '尿液編號','檢驗級別成分','尿報檔案','處分書狀態','');
        $table_row = array();
        foreach ($query as $susp)
        {
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            $table_row = NULL;
            if($susp->s_ic == null){
                $table_row[] = anchor('disciplinary_c/editsusp2/' . $susp->s_num, '未輸入');
            }
            else{
                if(preg_match("/a/i", $this->session-> userdata('3permit')))  $table_row[] = anchor('disciplinary_c/editsusp2/' . $susp->s_num, $susp->s_ic);
                else  $table_row[] = $susp->s_ic;
            }
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;
            }
            $sc_ingredient = Null;
            foreach ($query1->result() as $susp1){
                if($susp1->sc_num == null){}
                else{
                    $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.'<br>';
                }
            }
            if($susp->s_utest_num == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_utest_num;
            }
            if($sc_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $sc_ingredient;
            }
            if($susp->s_utest_doc==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $susp->s_utest_doc, '尿報');
            }
            if($susp->s_sac_state == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->s_sac_state;
            }
            if(preg_match("/a/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('Disciplinary_c/deldrug/' . $susp->s_num, '刪除');
            else $table_row[] = '';
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_DPProject1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getdpproject($id)->result(); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('','專案編號','發文日期','');
        $table_row = array();
        $i=0;
        foreach ($query as $dp)
        {
            $i++;
            $table_row = NULL;
            //$table_row[] = $dp->dp_name;
            //$table_row[] = '<input type="checkbox" name="a">';
            $table_row[] = $dp->dp_num;
            $table_row[] = anchor('disciplinary_c/listdp1/' . $dp->dp_num, $dp->dp_name);
            if($dp->dp_send_date==null){
                $table_row[] = '未寫入';
            }
            else{
                $table_row[] = $dp->dp_send_date;
            }
			$table_row[] = $dp->dp_num;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_DPRollbackProject1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getdpproject_rollback($id)->result(); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('','專案編號','發文日期','');
        $table_row = array();
        $i=0;
        foreach ($query as $dp)
        {
            $i++;
            $table_row = NULL;
            //$table_row[] = $dp->dp_name;
            //$table_row[] = '<input type="checkbox" name="a">';
            $table_row[] = $dp->dp_num;
            $table_row[] = anchor('disciplinary_c/listdp1/' . $dp->dp_num, $dp->dp_name);
            if($dp->dp_send_date==null){
                $table_row[] = '未寫入';
            }
            else{
                $table_row[] = $dp->dp_send_date;
            }
			$table_row[] = $dp->dp_num;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_DPProject1Ready($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getdpproject_Ready($id)->result(); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('','專案編號','發文日期','專案狀態','');
        $table_row = array();
        $i=0;
        foreach ($query as $dp)
        {
            $i++;
            $table_row = NULL;
            //$table_row[] = $dp->dp_name;
            //$table_row[] = '<input type="checkbox" name="a">';
            $table_row[] = $dp->dp_num;
            $table_row[] = anchor('disciplinary_c/listdp1_ready/' . $dp->dp_num, $dp->dp_name);
            if($dp->dp_send_date==null){
                $table_row[] = '未寫入';
            }
            else{
                $table_row[] = $dp->dp_send_date;
            }
            if($dp->dp_status=='送批'){
                $table_row[] = '送批';
            }
            else if($dp->dp_status=='公示'){
                $table_row[] = '公示';
            }
            else if($dp->dp_status=='已送批'){
                $table_row[] = '已送批';
            }
            else{
                $table_row[] = '已寄出';
            }
			$table_row[] = $dp->dp_num;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

	function table_DPRBProject1Ready($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getdpproject_Ready_rollback($id)->result(); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('','專案編號','發文日期','專案狀態','');
        $table_row = array();
        $i=0;
        foreach ($query as $dp)
        {
            $i++;
            $table_row = NULL;
            //$table_row[] = $dp->dp_name;
            //$table_row[] = '<input type="checkbox" name="a">';
            $table_row[] = $dp->dp_num;
            $table_row[] = anchor('disciplinary_c/listdp1_ready/' . $dp->dp_num, $dp->dp_name);
            if($dp->dp_send_date==null){
                $table_row[] = '未寫入';
            }
            else{
                $table_row[] = $dp->dp_send_date;
            }
            if($dp->dp_status=='送批'){
                $table_row[] = '送批';
            }
            else if($dp->dp_status=='公示'){
                $table_row[] = '公示';
            }
            else if($dp->dp_status=='已送批'){
                $table_row[] = '已送批';
            }
            else{
                $table_row[] = '已寄出';
            }
			$table_row[] = $dp->dp_num;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    public function listdp1() {//每日處理專案
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $dp = $this->getsqlmod->getDP1($id)->result();
		$dp = $this->getsqlmod->getDP1bynum($id)->result();
		if(substr($id, 0, 1) != 'P')
		{
			$test_table = $this->table_dp1($table,$dp,'listdp1');
		}
		else
		{
			$test_table = $this->table_dprb1($table,$dp,'listdp1');
		}

		$courses=$this->getsqlmod->getCourseData()->result();
		$course_options = array(null => '請選擇(無)');
        foreach ($courses as $course){
            $course_options[trim($course->c_date) . '-' . trim($course->c_week) . '-' . trim($course->c_time) . '-' .preg_replace('/\r\n|\n/',"",trim($course->c_place))] = trim($course->c_date) . ' (' . trim($course->c_week) . ')時段：' . trim($course->c_time);
        }

        $data['s_table'] = $test_table;
        $data['dp_num'] = $dp[0]->dp_num;
		$data['dp_date'] = $this->tranfer2RCyear($dp[0]->dp_send_date);
		$data['dp_expdate'] = $this->tranfer2RCyear($dp[0]->dp_expirdate);
		$data['dp_course'] = $dp[0]->dp_course;
		$data['dp_empno'] = $dp[0]->dp_empno;
		
		$data['course_opt'] = $course_options;
        $data['id'] = $id;
        $data['dp_status'] = $dp[0]->dp_status;
		if(substr($dp[0]->dp_name, 0, 1) != 'P')
		{
			$data['url_1'] = "案件作業";
			$data['url'] = 'disciplinary_c/listDpProject';
		}
		else
		{
			$data['url_1'] = "重處作業";
			$data['url'] = 'disciplinary_c/listDpRollbackProject';
		}
		
        $data['title'] = "每日處理專案:".$dp[0]->dp_name;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dplist1';
        else $data['include'] = 'disciplinary_query/dplist1';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    public function listdp2() {//送批
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $dp = $this->getsqlmod->getDP1($id)->result();
        $test_table = $this->table_dp1($table,$id);
        $data['s_table'] = $test_table;
        $data['dp_num'] = $dp[0]->dp_num;
        $data['dp_status'] = $dp[0]->dp_status;
        $data['title'] = "每日處理專案:".$dp[0]->dp_name;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dplist1';
        else $data['include'] = 'disciplinary_query/dplist1';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function createcases_num() {//建立新cases
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $id = $this->session->userdata('uoffice');
        $query = $this->getsqlmod->getdata(1,1)->result();
        $countid = $query[0]->c_num;
        $lastNum = (int)mb_substr($countid, -4, 4);
        $lastNum++;
        $value = str_pad($lastNum,3,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $data['c_num']='CN' . $taiwan_date.$value;
        $_POST['c_num']=$data['c_num'];
        $data = array(
            's_cnum' => $_POST['c_num'],
        );
        $this->getsqlmod->addcases($_POST);
        $this->getsqlmod->addsuspect($data);
        /*$sp_snum = $this->getsqlmod->get1Susp_ed('s_cnum',$_POST['c_num'])->result();
        $data1 = array(
            'sp_cnum' => $_POST['c_num'],
            'sp_snum' => $sp_snum[0]->s_num,
        );
        $this->getsqlmod->addSp($data1);*/
        redirect('disciplinary_c/othernew/' . $_POST['c_num'],'refresh'); 
    }
    
    public function createdrug_num() {//建立新毒品證物
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $Susp = $this->getsqlmod->get1DrugSusp($id)->result();
        $query = $this->getsqlmod->get1DrugCount()->result(); 
        $countid = $query[0]->e_id;
        $lastNum = (int)mb_substr($countid, -5, 5);
        $lastNum++;
        $value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $_POST['e_id']='EN' . $taiwan_date . $value;
        $_POST['e_c_num']=$id;
        $_POST['e_empno']=$this -> session -> userdata('uname');
        $_POST['e_ed_empno']=$this -> session -> userdata('uname');//要修改
       // //var_dump($_POST);
        $this->getsqlmod->adddrug($_POST);
        redirect('disciplinary_c/othernew/' . $_POST['e_c_num'],'refresh'); 
    }
    
    public function listdp() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getdpproject($table)->result();
		$rp_rollback = $this->getsqlmod->getdpproject_rollback()->result();
        $test_table = $this->table_dp('文山第一分局');

		$rollback_table = $this->table_dp_rollback('文山第一分局');
        $type_options = array();
		$rb_options = array();
        foreach ($rp as $rp1){
            $type_options[$rp1->dp_name] = $rp1->dp_name.$rp1->dp_status;
        }
		foreach ($rp_rollback as $rb_opt){
            $rb_options[$rb_opt->dp_name] = $rb_opt->dp_name;
        }
        $data['s_table'] = $test_table;
		$data['roll_table'] = $rollback_table;
        $data['title'] = "處分書點收本轄案件";
        $data['opt'] = $type_options;
		$data['rb_opt'] = $rb_options;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/listdp';
        else $data['include'] = 'disciplinary_query/listdp';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }   

    public function listdelivery() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table_delivery('文山第一分局');
		$success_table = $this->table_delivery_success($table);
        $data['s_table'] = $test_table;
		$data['s_success_table'] = $success_table;
        $data['title'] = "送達情形登錄";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/listdelivery';
        else $data['include'] = 'disciplinary_query/listdelivery';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }   

    public function listPublicity1() {//公示送達專案列表
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table_Publicity1('文山第一分局');
        $data['s_table'] = $test_table;
        $data['title'] = "公示送達";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/listPublicity1';
        else $data['include'] = 'disciplinary_query/listPublicity1';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }   

    public function updatepublicproject() {//公示專案修改
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        //var_dump($_POST);
        foreach ($s_cnum1 as $key => $value) {
            if(!empty($_FILES['files']['name'][$value])){
                $_FILES['file']['name'] = $_FILES['files']['name'][$value];
                $_FILES['file']['type'] = $_FILES['files']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['files']['error'][$value];
                $_FILES['file']['size'] = $_FILES['files']['size'][$value];
                $config['upload_path'] = '送達文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                }
            }
            if(!isset($filename))$filename=$_POST['fdp_an'][$value];
            $data = array(
                'fdp_delivery_date' => $this->tranfer2ADyear($_POST['fdp_delivery_date'][$value]),
                'fdp_an' => $filename,
                'fpd_status' => '公示送達',
            );
			$fdp = $this->getsqlmod->getfdprojectbyid($value)->result_array();
			$dp = $this->getsqlmod->getDP1bynum($fdp[0]['fpd_no'])->result_array();
			$fp3 = $this->getsqlmod->getfp3($dp[0]['dp_name'])->result();   
			foreach ($fp3 as $sp) {
				$updatedata2 = array(
					'fdd_maildate' => $this->tranfer2ADyear($_POST['fdp_delivery_date'][$value]),
					'fdd_status' => '公示送達'
				);
	
				$this->getsqlmod->updatedelivery($sp->s_num, $updatedata2);
			}
			// $data1 = array(
            //     'fdd_date' => $this->tranfer2ADyear($_POST['fdp_delivery_date'][$value]),
            //     'fdd_status' => '公示送達',
            //     //'fpd_status' => '公示送達',
            // );
            // //var_dump($data);
            // echo '<br>';
            $this->getsqlmod->updatefdp1($value,$data);
			// $this->getsqlmod->updatefdp1_susp($value,$data1);
        };
        redirect('Disciplinary_c/listPublicity1/'); 
    }
  
    public function listPublicity() {//公示送達列表
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table_Publicity('文山第一分局');
        $fdp = $this->getsqlmod->getfdproject($table)->result();
        $type_options = array();
        foreach ($fdp as $fdp1){
            $type_options[$fdp1->fpd_no] = $fdp1->fpd_no;
        }
        $data['opt'] = $type_options;
        $data['s_table'] = $test_table;
        $data['title'] = "公示送達";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/listPublicity';
        else $data['include'] = 'disciplinary_query/listPublicity';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }   

    public function updatePublicityproject() {
        ////var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $taiwan_date = date('Y')-1911; 
        $now = date('mdGi'); 
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    'fdd_projectpublic_num' => $taiwan_date.$now,
                );
                $this->getsqlmod->updatedelivery($value,$data);
            }
            $data1 = array(
                    'fpd_no' => $taiwan_date.$now,
                    'fdp_empno' => $user,
                    //'fdp_status' => '未處理',
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addfdp($data1);
            redirect('disciplinary_c/listPublicity1/'.$taiwan_date.$now); 
        }
        if($_POST['s_status']=='0'){//加入舊案
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    'fdd_projectpublic_num' => $_POST['fpd_no'],
                );
                $this->getsqlmod->updatedelivery($value,$data);
            }
            redirect('disciplinary_c/listPublicity1/'.$_POST['fpd_no']); 
        }
    }
  
    public function listPublicityProject() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        //$dp = $this->getsqlmod->getDP1($id)->result();
        $test_table = $this->table_PublicityProject($id);

		// $fdp = $this->getsqlmod->getfdprojectbyid($id)->result_array();
		$dp = $this->getsqlmod->getDP1bynum($id)->result_array();

        $data['fdp_num'] = $id;
        //$data['dp_name'] = $dp[0]->dp_name;
        $data['s_table'] = $test_table;
        $data['title'] = "公示送達專案：". $dp[0]['dp_name'];
		$data['url'] = 'disciplinary_c/listPublicity1';
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/listPublicityProject';
        else $data['include'] = 'disciplinary_query/listPublicityProject';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }   
    
    public function listProjectProject() {//專案處理(處分書)
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject($table)->result();
        $test_table = $this->table_DPProject1($table);
        $data['s_table'] = $test_table;
        $data['title'] = "專案處理";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dpprojectlist';
        else $data['include'] = 'disciplinary_query/dpprojectlist';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function updatedelivery(){
        // var_dump($_POST);
        // exit;
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        foreach ($s_cnum1 as $key => $value) {
            if(isset($_POST['fdd_date'][$value])){
                $data = array(
                    'fdd_date' => $this->tranfer2ADyear($_POST['fdd_date'][$value]),
                    // 'fdd_status' => $_POST['fdd_status'][$value],
					'fdd_status' => '已送達'
                );
                $this->getsqlmod->updatedelivery($value,$data);
            }
        };
        redirect('Disciplinary_c/listdelivery/'); 
    }
    
    public function uploaddeliverydoc(){
        ////var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //$s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        ////var_dump($_FILES);
        $countfiles = count($_FILES['files']['name']);
        for($i=0;$i<$countfiles;$i++){
            if(!empty($_FILES['files']['name'][$i])){
                $fd_num = substr($_FILES['files']['name'][$i],0,-4);
                $fd=$this->getsqlmod->getFD('fd_num',$fd_num)->result();//取得送達文件所需資料
                $delivery1=$this->getsqlmod->getdelivery('fdd_fdnum',$fd_num)->result();//取得送達文件所需資料
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                $config['upload_path'] = '送達文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file') &&isset($fd[0]->fd_num)){
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    $delivery = array(
                        'fdd_fdnum'  => $fd[0]->fd_num,
                        'fdd_snum'  => $fd[0]->fd_snum,
                        'fdd_cnum'  => $fd[0]->fd_cnum,
                        'fdd_doc'  => $filename,
                    );
                    if(isset($delivery1[0]))$this->getsqlmod->updatedelivery($fd[0]->fd_snum,$delivery);
                    else {
                       $this->getsqlmod->adddelivery($delivery);
                    }
                    //echo $filename;
                        //$data['filenames'][] = $filename;
                }
            }        
        }
        redirect('Disciplinary_c/listdelivery/'); 
    }
    
    public function uploadpublicdoc(){//上傳公文函第一頁
        if(isset($_POST['upload'])){
            $query = $this->getsqlmod->getfp3($_POST['id'])->result(); 
            if($_POST['foldername'] != ""){
                $foldername='test/'.$_POST['foldername'];
                if(!is_dir($foldername)) mkdir($foldername);
                foreach($_FILES['files']['name'] as $i => $name)
                {
                    if(strlen($_FILES['files']['name'][$i]) > 1)
                    {  
                        echo $name.'<br>';
                        foreach($query as $susp){
                            $fd_num = $susp->fd_num.'.pdf'; 
                            if($fd_num == $name){
                                echo $name;
                            }
                        }
                        //move_uploaded_file($_FILES['files']['tmp_name'][$i],$foldername."/".$name);
                    }
                    //echo $_FILES['files']['tmp_name'][$i],$foldername."/".$name.'<br>';
                }
                echo "Folder is successfull11y uploaded";
            }
            else
                echo "Upload folder name is empty";
        }
    }
    
    public function updatedeliveryproject() {//公示送達加入/新增專案
        ////var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $taiwan_date = date('Y')-1911; 
        $now = date('mdGi'); 
        $rp_num = $taiwan_date.$now;
        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_dp_project' => $rp_num,
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            $data1 = array(
                    'dp_name' => $rp_num,
                    'dp_empno' => $user,
                    //'dp_status' => '送批',
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->adddp($data1);
            redirect('disciplinary/listdpProject/'); 
        }
        if($_POST['s_status']=='2'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_dp_project' => $rp_num,
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            $data1 = array(
                    'dp_name' => $rp_num,
                    'dp_empno' => $user,
                    //'dp_status' => '公示',
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->adddp($data1);
            //redirect('cases2/list3Rewardlist/'); 
        }
        if($_POST['s_status']=='0'){//加入舊案
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_dp_project ' => $_POST['dp_num'],
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            redirect('disciplinary_c/listdpProject/'); 
        }
    }
    
    public function updatedpproject() {
        ////var_dump($_POST);
        $this->load->helper('form');
        $data['title'] = "案件處理系統:待裁罰列表";
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
		$empname = $this -> session -> userdata('uname');
		$taiwan_date = date('Y')-1911; 			

		$todaypj = $this->getsqlmod->getDisPJnameToday();
		$count = $todaypj->num_rows();

        // $now = date('mdGi');
		$gotourl = '';
		// $rp_num = '';
		if(isset($_POST['front_word']) && $_POST['front_word'] == 'P')
		{
			$now = date('md') . (($count > 0)?str_pad($count+1,3,'0', STR_PAD_LEFT):str_pad(1,3,'0', STR_PAD_LEFT));
			$rp_num = 'P'.$_POST['pjname']; 
			$gotourl = base_url('disciplinary_c/listDpRollbackProject');
		}			
		else
		{
			$now = date('md') . (($count > 0)?str_pad($count+1,3,'0', STR_PAD_LEFT):str_pad(1,3,'0', STR_PAD_LEFT));
			$rp_num = $_POST['pjname'];
			$gotourl = base_url('disciplinary_c/listDpProject');
		}
        
		$rp_num = $rp_num;

        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_dp_project' => $rp_num,
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            $data1 = array(
                    'dp_name' => $rp_num,
                    'dp_empno' => $empname,
                    'dp_status' => NULL,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->adddp($data1);
            // redirect('disciplinary_c/listDpProject/'); 
			// echo  base_url('disciplinary_c/listDpProject');
			echo $gotourl;
        }
        if($_POST['s_status']=='2'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_dp_project' => $rp_num,
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            $data1 = array(
                    'dp_name' => $rp_num,
                    'dp_empno' => $empname,
                    //'dp_status' => '公示',
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->adddp($data1);
            //redirect('cases2/list3Rewardlist/'); 
        }
        if($_POST['s_status']=='0'){//加入舊案
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_dp_project ' => $_POST['dp_num'],
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            // redirect('disciplinary_c/listDpProject/'); 
			// echo base_url('disciplinary_c/listDpProject');
			echo $gotourl;
        }
    }
    
    public function listDpProject_Ready() {//送批
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject($table)->result();
        $test_table = $this->table_DPProject1Ready($table);
		
        $data['s_table'] = $test_table;
        // $data['title'] = "專案處理";
        $data['title'] = "送批列表";
		$data['rollback'] = "N";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dpprojectlist_ready';
        else $data['include'] = 'disciplinary_query/dpprojectlist_ready';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

	public function listDpRollbackProject_Ready() {//送批
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject($table)->result();
        $test_table = $this->table_DPRBProject1Ready($table);
		
        $data['s_table'] = $test_table;
        // $data['title'] = "專案處理";
        $data['title'] = "送批列表（重處）";
		$data['rollback'] = "Y";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dpprojectlist_ready';
        else $data['include'] = 'disciplinary_query/dpprojectlist_ready';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function listDpProject() {//專案處理(處分書)
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject($table)->result();
        $test_table = $this->table_DPProject1($table);
        $data['s_table'] = $test_table;
        $data['title'] = "專案處理";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dpprojectlist';
        else $data['include'] = 'disciplinary_query/dpprojectlist';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }

	public function listDpRollbackProject() {//重處 - 專案處理(處分書)
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject($table)->result();
        $test_table = $this->table_DPRollbackProject1($table);
        $data['s_table'] = $test_table;
        $data['title'] = "專案處理(重處)";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dpprojectlist';
        else $data['include'] = 'disciplinary_query/dpprojectlist';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function updatestatus() {
        $this->load->helper('form');
        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
		$redirect = '';
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $this->getsqlmod->updatedproject($value,'送批');
				$dp = $this->getsqlmod->getDP1bynum($value)->result();
				if(substr($dp[0]->dp_name, 0, 1) != 'P')
				{
					$redirect ='disciplinary_c/listDpProject_Ready/'; 
				}
				else
				{
					$redirect ='disciplinary_c/listDpRollbackProject_Ready/'; 
				}
            }
			redirect($redirect); 
            
        }
    }    

	public function delete_DC_project(){
        if(isset($_POST['dp_num']))
        {
			$this->getsqlmod->delDCProject($_POST['dp_num']);
            

            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        
    }
    
    public function updatestatus_ready() {
        $this->load->helper('form');
        // var_dump($_POST);
		// exit;
		$user = $this -> session -> userdata('uic');
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $this->getsqlmod->updatedproject($value,'已送批');
            }
            redirect('disciplinary_c/listDpProject_Ready/'); 
        }
        if($_POST['s_status']=='0'){
            foreach ($s_cnum as $key => $value) {
                $this->getsqlmod->updatedproject($value,'已寄出');

				$dp = $this->getsqlmod->getDP1bynum($value)->result();
				$fp3 = $this->getsqlmod->getfp3($dp[0]->dp_name)->result();   
				foreach ($fp3 as $sp) {
					$updatedata2 = array(
						'fdd_maildate' => date('Y-m-d'),
						'fdd_status' => '已寄出'
					);
		
					$this->getsqlmod->updatedelivery($sp->s_num, $updatedata2);
				}
            }
            redirect('disciplinary_c/listDpProject_Ready/'); 
        }
		if($_POST['s_status']=='rb'){
            foreach ($s_cnum as $key => $value) {
				$data = array(
						'dp_status' => '公示',
						'dp_projectpublic_num' => $value
						//'fdp_status' => '未處理',
				);
                $this->getsqlmod->updatedrbproject($value,$data);

				$data1 = array(
						'fpd_no' => $value,
						'fdp_empno' => $user,
						//'fdp_status' => '未處理',
				);
				if($value!=NULL);$this->getsqlmod->addfdp($data1);
            }
            redirect('disciplinary_c/listDpRollbackProject_Ready/'); 
        }
		if($_POST['s_status'] == 'rb_no')
		{
			foreach ($s_cnum as $key => $value) {
                $this->getsqlmod->updatedproject($value,'已寄出');

				$fp3 = $this->getsqlmod->getfp3($value)->result();   
				foreach ($fp3 as $sp) {
					$updatedata2 = array(
						'fdd_maildate' => date('Y-m-d'),
						'fdd_status' => '已寄出'
					);
		
					$this->getsqlmod->updatedelivery($sp->s_num, $updatedata2);
				}
            }
            redirect('disciplinary_c/listdelivery/'); 
		}
    }    
    public function othernew() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getdpproject($table)->result();
        $type_options = array();
        foreach ($rp as $rp1){
            $type_options[$rp1->dp_name] = $rp1->dp_name.$rp1->dp_status;
        }
        $susp2 = $this->getsqlmod->get2SuspCk('sc_cnum',$id)->result();
        $type_options1 = array(
            '1級海洛因' => '1級海洛因' ,
            '1級鴉片代謝物:嗎啡' => '1級鴉片代謝物:嗎啡' ,
            '1級鴉片代謝物:可待因' => '1級鴉片代謝物:可待因' ,
            '2級安非他命' => '2級安非他命',
            '2級甲基安非他命' => '2級甲基安非他命',
            '2級MDMA' => '2級MDMA',
            '2級MDA' => '2級MDA',
            '2級大麻代謝物:四氫大麻酚-9-甲酸' => '2級大麻代謝物:四氫大麻酚-9-甲酸',
            '3級愷他命代謝物:愷他命' => '3級愷他命代謝物:愷他命',
            '3級愷他命代謝物:去甲基愷他命' => '3級愷他命代謝物:去甲基愷他命',
            '無級無' => '無',
        );
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        $susp = $this->getsqlmod->get1SuspAll($id)->result();
        $suspfadai = $this->getsqlmod->get1SuspFadai($susp[0]->s_ic)->result();
        //echo $susp[0]->s_num;
        //$susp1 = $this->getsqlmod->get3SP($susp[0]->s_num)->result();
        ////var_dump($suspfadai);
        $data['cases'] = $casesrepair[0];
        $data['susp'] = $susp[0];
        if(isset($suspfadai[0])) $data['suspfadai'] = $suspfadai[0];
        //var_dump($data['suspfadai']);
        $data['susp2'] = $susp2;//尿檢
        $data['c_num']=$data['cases']->c_num;
        $data['opt']=$type_options;
        $data['opt1']=$type_options1;
        $data['drug_table'] = $this->table_drug2($id);
        $data['s_table'] = $this->table_susp2($id);
        $data['title'] = "本轄案件新增(他機關查獲)";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/d_other_new';
        else $data['include'] = 'disciplinary_query/d_other_new';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function addcasesother() {//新增他轄資料
        //$suspfadai = $this->getsqlmod->get1SuspFadai($_POST['s_ic'])->result();
        $s_utest_doc1 = $this->getsqlmod->get1Susp_ed1('s_ic',$_POST['s_ic'])->result();
        $_POST['sp_doc'] = null;
        if(!empty($_FILES['sp_doc']['name'])){
            $_FILES['sp_doc']['name']  = $_POST['s_go_no'] . $_FILES['sp_doc']['name'];
            $config['upload_path'] = 'drugdoc/'; 
            $config['allowed_types'] = 'doc|pdf|docx';
            $config['max_size'] = 100000;
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('sp_doc')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['sp_doc']=$uploadData['file_name'];
            }
        }
        if(!empty($_FILES['s_utest_doc']['name'])){
            $_FILES['s_utest_doc']['name']  = $_POST['c_num'] . $_FILES['s_utest_doc']['name'];
            $config['upload_path']          = 'utest/';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 1000000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('s_utest_doc')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                //redirect('cases2/editCases2/' . $_POST['s_cnum'],'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['s_utest_doc']=$uploadData['file_name'];
            }
        }
        else{
            if(isset($s_utest_doc1[0]))$_POST['s_utest_doc']=$s_utest_doc1[0]->s_utest_doc;
            else $_POST['s_utest_doc']= NULL;
        }
        if(!empty($_FILES['doc_file']['name'])){
            $config = array();
            $_FILES['doc_file']['name']  = $_POST['c_num'] . $_FILES['doc_file']['name'];
            $config['upload_path']          = 'uploads/';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('doc_file')){
                $error = array('error' => $this->upload->display_errors());
                //redirect('cases2/editCases2/' . $_POST['s_cnum'],'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['doc_file']=$uploadData['file_name'];
                $cases = array(
                        's_date' => $_POST['s_date'],
                        's_time' => $_POST['s_time'],
                        's_place'  => $_POST['s_place'],
                        's_office'  => $_POST['s_office'],
                        'r_office'  => $_POST['s_roffice'],
                        'r_office1'  => $_POST['s_roffice1'],
                        'doc_file'  => $_POST['doc_file'],
                );
                /*$susp_fadai = array(
                        's_fadai_name' => $_POST['s_fadai_name'],
                        's_fadai_ic'  => $_POST['s_fadai_ic'],
                        's_fadai_sic'  => $_POST['s_ic'],
                        's_fadai_county'  => $_POST['s_fadai_county'],
                        's_fadai_district'  => $_POST['s_fadai_district'],
                        's_fadai_zipcode'  => $_POST['s_fadai_zipcode'],
                        's_fadai_address'  => $_POST['s_fadai_address'],
                        's_fadai_birth'  => $_POST['s_fadai_birth'],
                        's_fadai_gender'  => $_POST['s_fadai_gender'],
                );*/
                if(!isset($_POST['sc'])) $_POST['sc']=array();
                for($i=0, $count = count($_POST['sc']);$i<$count;$i++) {
                    $data = array();
                    $sc = explode("級",$_POST['sc'][$i]);
                    if(!isset($sc[1])){
                        $data = array(
                            'sc_num' => $_POST['sc_num'][$i],
                            'sc_level' => $sc[0],
                            'sc_ingredient' => $sc[1],
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['c_num'],
                            );
                    }else{
                        $data = array(
                            'sc_num' => $_POST['sc_num'][$i],
                            'sc_level' => $sc[0],
                            'sc_ingredient' => $sc[1],
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['c_num'],
                            );
                    }
                    $this->getsqlmod->updatesc($data);
                }
                if($_POST['s_status']=='1'){
                    $susp = array(
                            's_sac_state' => '確認送出裁罰',
                            's_utest_num'  => $_POST['s_utest_num'],
                            's_utest_doc'  => $_POST['s_utest_doc'],
                            's_ic'  => $_POST['s_ic'],
                            's_name'  => $_POST['s_name'],
                            's_dpcounty'  => $_POST['s_dpcounty'],
                            's_dpdistrict'  => $_POST['s_dpdistrict'],
                            's_dpzipcode'  => $_POST['s_dpzipcode'],
                            's_dpaddress'  => $_POST['s_dpaddress'],
                            's_rpcounty'  => $_POST['s_rpcounty'],
                            's_rpdistrict'  => $_POST['s_rpdistrict'],
                            's_rpzipcode'  => $_POST['s_rpzipcode'],
                            's_rpaddress'  => $_POST['s_rpaddress'],
                            's_roffice'  => $_POST['s_roffice'],
                            's_roffice1'  => $_POST['s_roffice1'],
                            's_fno'  => $_POST['s_fno'],
                            's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
                            's_go_no'  => $_POST['s_go_no'],
                            'sp_doc'  => $_POST['sp_doc'],
                            's_dp_project'  => date('Y')-1911 . date('mdGi'),
                    );
                    $data1 = array(
                            'dp_name' => date('Y')-1911 . date('mdGi'),
                            'dp_empno' => $this -> session -> userdata('uic'),
                            //'dp_status' => '公示',
                    );
                    $this->getsqlmod->adddp($data1);
                }
                if($_POST['s_status']=='0'){
                    $susp = array(
                            's_sac_state' => '確認送出裁罰',
                            's_utest_num'  => $_POST['s_utest_num'],
                            's_utest_doc'  => $_POST['s_utest_doc'],
                            's_ic'  => $_POST['s_ic'],
                            's_name'  => $_POST['s_name'],
                            's_dpcounty'  => $_POST['s_dpcounty'],
                            's_dpdistrict'  => $_POST['s_dpdistrict'],
                            's_dpzipcode'  => $_POST['s_dpzipcode'],
                            's_dpaddress'  => $_POST['s_dpaddress'],
                            's_rpcounty'  => $_POST['s_rpcounty'],
                            's_rpdistrict'  => $_POST['s_rpdistrict'],
                            's_rpzipcode'  => $_POST['s_rpzipcode'],
                            's_rpaddress'  => $_POST['s_rpaddress'],
                            's_roffice'  => $_POST['s_roffice'],
                            's_roffice1'  => $_POST['s_roffice1'],
                            's_fno'  => $_POST['s_fno'],
                            's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
                            's_go_no'  => $_POST['s_go_no'],
                            'sp_doc'  => $_POST['sp_doc'],
                            's_dp_project'  => $_POST['dp_num'],
                    );
                }
                if($_POST["s_status"]=="99"){
                    $susp = array(
                            's_sac_state' => '確認送出裁罰',
                            's_utest_num'  => $_POST['s_utest_num'],
                            's_utest_doc'  => $_POST['s_utest_doc'],
                            's_ic'  => $_POST['s_ic'],
                            's_name'  => $_POST['s_name'],
                            's_dpcounty'  => $_POST['s_dpcounty'],
                            's_dpdistrict'  => $_POST['s_dpdistrict'],
                            's_dpzipcode'  => $_POST['s_dpzipcode'],
                            's_dpaddress'  => $_POST['s_dpaddress'],
                            's_rpcounty'  => $_POST['s_rpcounty'],
                            's_rpdistrict'  => $_POST['s_rpdistrict'],
                            's_rpzipcode'  => $_POST['s_rpzipcode'],
                            's_rpaddress'  => $_POST['s_rpaddress'],
                            's_roffice'  => $_POST['s_roffice'],
                            's_roffice1'  => $_POST['s_roffice1'],
                            's_fno'  => $_POST['s_fno'],
                            's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
                            's_go_no'  => $_POST['s_go_no'],
                            'sp_doc'  => $_POST['sp_doc'],
                            //'s_dp_project'  => $_POST['dp_num'],
                    );
                    if(count($_POST['sc'])>0){
                        for($i=0, $count = count($_POST['sc']);$i<$count;$i++) {
                            $sc = explode("級",$_POST['sc'][$i]);
                            $data = array(
                                'sc_level' => $sc[0],
                                'sc_ingredient' => $sc[1],
                                'sc_snum' => $_POST['s_num'],
                                'sc_cnum' => $_POST['c_num'],
                            );
                        }
                    }else{
                        $data = array(
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['c_num'],
                        );
                    }
                    $this->getsqlmod->addsc($data);
                    //if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['s_ic'],$susp_fadai);
                    //else $this->getsqlmod->addSuspFadai($susp_fadai);
                    $this->getsqlmod->updateSusp1($_POST['c_num'],$susp);
                    $this->getsqlmod->updateCase($_POST['c_num'],$cases);
                    //$this->getsqlmod->updateSp1($_POST['c_num'],$suspect_punishment);
                    redirect('disciplinary_c/othernew/' . $_POST['c_num'],'refresh'); 
                }
                if($_POST["s_status"]=="88"){
                    $susp = array(
                            's_sac_state' => '確認送出裁罰',
                            's_utest_num'  => $_POST['s_utest_num'],
                            's_utest_doc'  => $_POST['s_utest_doc'],
                            's_ic'  => $_POST['s_ic'],
                            's_name'  => $_POST['s_name'],
                            's_dpcounty'  => $_POST['s_dpcounty'],
                            's_dpdistrict'  => $_POST['s_dpdistrict'],
                            's_dpzipcode'  => $_POST['s_dpzipcode'],
                            's_dpaddress'  => $_POST['s_dpaddress'],
                            's_rpcounty'  => $_POST['s_rpcounty'],
                            's_rpdistrict'  => $_POST['s_rpdistrict'],
                            's_rpzipcode'  => $_POST['s_rpzipcode'],
                            's_rpaddress'  => $_POST['s_rpaddress'],
                            's_roffice'  => $_POST['s_roffice'],
                            's_roffice1'  => $_POST['s_roffice1'],
                            's_fno'  => $_POST['s_fno'],
                            's_go_no'  => $_POST['s_go_no'],
                            's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
                            'sp_doc'  => $_POST['sp_doc'],
                            //'s_dp_project'  => $_POST['dp_num'],
                    );
                    //if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['s_ic'],$susp_fadai);
                    //else $this->getsqlmod->addSuspFadai($susp_fadai);
                    $this->getsqlmod->updateSusp1($_POST['c_num'],$susp);
                    $this->getsqlmod->updateCase($_POST['c_num'],$cases);
                    //$this->getsqlmod->updateSp1($_POST['c_num'],$suspect_punishment);
                    redirect('disciplinary_c/createdrug_num/' . $_POST['c_num'],'refresh'); 
                }
                    //if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['s_ic'],$susp_fadai);
                    //else $this->getsqlmod->addSuspFadai($susp_fadai);
                    $this->getsqlmod->updateSusp1($_POST['c_num'],$susp);
                    $this->getsqlmod->updateCase($_POST['c_num'],$cases);
                    //$this->getsqlmod->updateSp1($_POST['c_num'],$suspect_punishment);
                    redirect('disciplinary_c/listDpProject/','refresh'); 
            }
        }
        else{
                $cases = array(
                        's_date' => $_POST['s_date'],
                        's_place'  => $_POST['s_place'],
                        's_office'  => $_POST['s_office'],
                        'r_office'  => $_POST['s_roffice'],
                        'r_office1'  => $_POST['s_roffice1'],
                        //'doc_file'  => ''
                );
                /*$susp_fadai = array(
                        's_fadai_name' => $_POST['s_fadai_name'],
                        's_fadai_ic'  => $_POST['s_fadai_ic'],
                        's_fadai_sic'  => $_POST['s_ic'],
                        's_fadai_county'  => $_POST['s_fadai_county'],
                        's_fadai_district'  => $_POST['s_fadai_district'],
                        's_fadai_zipcode'  => $_POST['s_fadai_zipcode'],
                        's_fadai_address'  => $_POST['s_fadai_address'],
                        's_fadai_gender'  => $_POST['s_fadai_gender'],
                        's_fadai_birth'  => $_POST['s_fadai_birth'],
                );*/
                if(!isset($_POST['sc'])) $_POST['sc']=array();
                for($i=0, $count = count($_POST['sc']);$i<$count;$i++) {
                    $data = array();
                    $sc = explode("級",$_POST['sc'][$i]);
                    if(!isset($sc[1])){
                        $data = array(
                            'sc_num' => $_POST['sc_num'][$i],
                            'sc_level' => $sc[0],
                            'sc_ingredient' => $sc[1],
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['c_num'],
                            );
                    }else{
                        $data = array(
                            'sc_num' => $_POST['sc_num'][$i],
                            'sc_level' => $sc[0],
                            'sc_ingredient' => $sc[1],
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['c_num'],
                            );
                    }
                    $this->getsqlmod->updatesc($data);
                }
                if($_POST['s_status']=='1'){
                    $susp = array(
                            's_sac_state' => '確認送出裁罰',
                            's_utest_num'  => $_POST['s_utest_num'],
                            's_utest_doc'  => $_POST['s_utest_doc'],
                            's_ic'  => $_POST['s_ic'],
                            's_name'  => $_POST['s_name'],
                            's_dpcounty'  => $_POST['s_dpcounty'],
                            's_dpdistrict'  => $_POST['s_dpdistrict'],
                            's_dpzipcode'  => $_POST['s_dpzipcode'],
                            's_dpaddress'  => $_POST['s_dpaddress'],
                            's_rpcounty'  => $_POST['s_rpcounty'],
                            's_rpdistrict'  => $_POST['s_rpdistrict'],
                            's_rpzipcode'  => $_POST['s_rpzipcode'],
                            's_rpaddress'  => $_POST['s_rpaddress'],
                            's_roffice'  => $_POST['s_roffice'],
                            's_roffice1'  => $_POST['s_roffice1'],
                            's_fno'  => $_POST['s_fno'],
                            's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
                            's_go_no'  => $_POST['s_go_no'],
                            'sp_doc'  => $_POST['sp_doc'],
                            's_dp_project'  => date('Y')-1911 . date('mdGi'),
                    );
                    $data1 = array(
                            'dp_name' => date('Y')-1911 . date('mdGi'),
                            'dp_empno' => $this -> session -> userdata('uic'),
                            //'dp_status' => '公示',
                    );
                    $this->getsqlmod->adddp($data1);
                }
                if($_POST['s_status']=='0'){
                    $susp = array(
                            's_sac_state' => '確認送出裁罰',
                            's_utest_num'  => $_POST['s_utest_num'],
                            's_utest_doc'  => $_POST['s_utest_doc'],
                            's_ic'  => $_POST['s_ic'],
                            's_name'  => $_POST['s_name'],
                            's_dpcounty'  => $_POST['s_dpcounty'],
                            's_dpdistrict'  => $_POST['s_dpdistrict'],
                            's_dpzipcode'  => $_POST['s_dpzipcode'],
                            's_dpaddress'  => $_POST['s_dpaddress'],
                            's_rpcounty'  => $_POST['s_rpcounty'],
                            's_rpdistrict'  => $_POST['s_rpdistrict'],
                            's_rpzipcode'  => $_POST['s_rpzipcode'],
                            's_rpaddress'  => $_POST['s_rpaddress'],
                            's_roffice'  => $_POST['s_roffice'],
                            's_roffice1'  => $_POST['s_roffice1'],
                            's_fno'  => $_POST['s_fno'],
                            's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
                            's_go_no'  => $_POST['s_go_no'],
                            'sp_doc'  => $_POST['sp_doc'],
                            's_dp_project'  => $_POST['dp_num'],
                    );
                }
                if($_POST["s_status"]=="99"){
                    $susp = array(
                            's_sac_state' => '確認送出裁罰',
                            's_utest_num'  => $_POST['s_utest_num'],
                            's_utest_doc'  => $_POST['s_utest_doc'],
                            's_ic'  => $_POST['s_ic'],
                            's_name'  => $_POST['s_name'],
                            's_dpcounty'  => $_POST['s_dpcounty'],
                            's_dpdistrict'  => $_POST['s_dpdistrict'],
                            's_dpzipcode'  => $_POST['s_dpzipcode'],
                            's_dpaddress'  => $_POST['s_dpaddress'],
                            's_rpcounty'  => $_POST['s_rpcounty'],
                            's_rpdistrict'  => $_POST['s_rpdistrict'],
                            's_rpzipcode'  => $_POST['s_rpzipcode'],
                            's_rpaddress'  => $_POST['s_rpaddress'],
                            's_roffice'  => $_POST['s_roffice'],
                            's_roffice1'  => $_POST['s_roffice1'],
                            's_fno'  => $_POST['s_fno'],
                            's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
                            's_go_no'  => $_POST['s_go_no'],
                            'sp_doc'  => $_POST['sp_doc'],
                            //'s_dp_project'  => $_POST['dp_num'],
                    );
                    if(count($_POST['sc'])>0){
                        for($i=0, $count = count($_POST['sc']);$i<$count;$i++) {
                            $sc = explode("級",$_POST['sc'][$i]);
                            $data = array(
                                'sc_level' => $sc[0],
                                'sc_ingredient' => $sc[1],
                                'sc_snum' => $_POST['s_num'],
                                'sc_cnum' => $_POST['c_num'],
                            );
                        }
                    }else{
                        $data = array(
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['c_num'],
                        );
                    }
                    $this->getsqlmod->addsc($data);
                    //if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['s_ic'],$susp_fadai);
                    //else $this->getsqlmod->addSuspFadai($susp_fadai);
                    $this->getsqlmod->updateSusp1($_POST['c_num'],$susp);
                    $this->getsqlmod->updateCase($_POST['c_num'],$cases);
                    //$this->getsqlmod->updateSp1($_POST['c_num'],$suspect_punishment);
                    redirect('disciplinary_c/othernew/' . $_POST['c_num'],'refresh'); 
                }
                if($_POST["s_status"]=="88"){
                    $susp = array(
                            's_sac_state' => '確認送出裁罰',
                            's_utest_num'  => $_POST['s_utest_num'],
                            's_utest_doc'  => $_POST['s_utest_doc'],
                            's_ic'  => $_POST['s_ic'],
                            's_name'  => $_POST['s_name'],
                            's_dpcounty'  => $_POST['s_dpcounty'],
                            's_dpdistrict'  => $_POST['s_dpdistrict'],
                            's_dpzipcode'  => $_POST['s_dpzipcode'],
                            's_dpaddress'  => $_POST['s_dpaddress'],
                            's_rpcounty'  => $_POST['s_rpcounty'],
                            's_rpdistrict'  => $_POST['s_rpdistrict'],
                            's_rpzipcode'  => $_POST['s_rpzipcode'],
                            's_rpaddress'  => $_POST['s_rpaddress'],
                            's_roffice'  => $_POST['s_roffice'],
                            's_roffice1'  => $_POST['s_roffice1'],
                            's_fno'  => $_POST['s_fno'],
                            's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
                            'sp_doc'  => $_POST['sp_doc'],
                            //'s_dp_project'  => $_POST['dp_num'],
                    );
                    $this->getsqlmod->updateSusp1($_POST['c_num'],$susp);
                    $this->getsqlmod->updateCase($_POST['c_num'],$cases);
                    //if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['s_ic'],$susp_fadai);
                    //else $this->getsqlmod->addSuspFadai($susp_fadai);
                    redirect('disciplinary_c/createdrug_num/' . $_POST['c_num'],'refresh'); 
                }
                //if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['s_ic'],$susp_fadai);
                //else $this->getsqlmod->addSuspFadai($susp_fadai);
                $this->getsqlmod->updateSusp1($_POST['c_num'],$susp);
                $this->getsqlmod->updateCase($_POST['c_num'],$cases);
                //$this->getsqlmod->updateSp1($_POST['c_num'],$suspect_punishment);
                redirect('disciplinary_c/listDpProject/','refresh'); 
        }
    }
    
    function editSanc_ready(){//批次裁處
        $this->load->library('table');
        $id = $this->uri->segment(3);
        $pjid = $this->uri->segment(4);
        $fd1 = $this->getsqlmod->getFD('fd_snum',$id)->result();
        $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
		$fine=$this->getsqlmod->get3SP1($id)->result();
        $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp[0]->s_dp_project)->result();
        $sp=$this->getsqlmod->get3SP1($id)->result();
        $spcount=$this->getsqlmod->getfdAll()->result();
        $cases=$this->getsqlmod->getCases($susp[0]->s_cnum)->result();
        $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp[0]->s_dp_project)->result();
        $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
        $susp1 = $this->getsqlmod->get2Susp($susp[0]->s_num); // 抓snum
        $drug=$this->getsqlmod->get1Drug_ic_ind($susp[0]->s_ic)->result();
		$urine = $this->getsqlmod->get2Susp($susp[0]->s_num)->result();
        if(isset($drug[0]))$drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug[0]->e_id)->result();
        // $prison=$this->getsqlmod->getprison()->result();
        $prison=$this->getsqlmod->getWordlibPrison()->result();
        $courses=$this->getsqlmod->getCourseData()->result();
        $suspfadai = $this->getsqlmod->get1SuspFadai($susp[0]->s_ic)->result();
		// $dp = $this->getsqlmod->getdpprojectname($pjid)->result();
		$dp = $this->getsqlmod->getDP1bynum($pjid)->result();
        ////var_dump($DpOrder);
        foreach ($DpOrder as $order){
            $arr[] = $order->s_num;
        }
        $id; 
        $i=0;
       // echo count($arr);
        //print_r($arr);
        while ($arr[$i] !== $id) {
            $i++;
            next($arr); 
        }
        if($i>0){
            $data['prev'] = $arr[$i-1];
        }
        if($i==0){
            $data['prev'] = NULL;
        }
        if($i == count($arr)-1){
            $data['next'] = NULL;
        }
        if($i < count($arr)-1){
            $data['next'] = $arr[$i+1];
        }
        $type_options = array(null => '無');
        foreach ($prison as $prison){
            $type_options[$prison->prison_name . ':' . $prison->prison_address] = $prison->prison_name . ' （' . $prison->prison_address . '）';
        }
        $course_options = array(null => '請選擇(無)');
        foreach ($courses as $course){
            $course_options[trim($course->c_date) . '-' . trim($course->c_week) . '-' . trim($course->c_time) . '-' .preg_replace('/\r\n|\n/',"",trim($course->c_place))] = trim($course->c_date) . ' (' . trim($course->c_week) . ')時段：' . trim($course->c_time);
        }
        $type_options1 = array(
            '晶體粉末、碎塊' => '晶體粉末、碎塊' ,
            '藥碇' => '藥碇',
            '梅碇' => '梅碇',
            '即溶包' => '即溶包',
            '香菸(含菸蒂)' => '香菸(含菸蒂)',
            '菸草' => '菸草',
            '濾嘴' => '濾嘴',
            '吸食器' => '吸食器',
            '吸管' => '吸管',
            '卡片' => '卡片',
            '括盤' => '括盤',
            '棉花棒' => '棉花棒',
            '殘渣袋' => '殘渣袋',
            '大麻' => '大麻',
            '手機殼' => '手機殼',
            '植株' => '植株',
            '種子' => '種子',
			'菸盒' => '菸盒',
			'紙張' => '紙張',
			'其他' => '其他'
       );   
        $type_options2 = array(
            '包' => '包' ,
            '顆' => '顆',
            '支' => '支',
            '張' => '張',
            '個' => '個',
            '組' => '組',
            '株' => '株',
        );
        $drug_ingredient = Null;
        $drug_ingredient1 = Null;
		if(isset($drug2)){
			foreach ($drug2 as $drug3){
				if(!isset($drug3->ddc_level)){}
				else{
					$drug_ingredient = $drug_ingredient . $drug3->ddc_level .'級' . $drug3->ddc_ingredient.';';
				}
			}

			if(count($drug2) == 0)
            	$drug_ingredient = $drug[0]->e_type;
		} 
        // foreach ($drug2 as $drug3){
        //     if(!isset($drug3->ddc_level)){}
        //     else{
        //         $drug_ingredient1 = $drug_ingredient1 . '第' .$drug3->ddc_level .'級' .'『'. $drug3->ddc_ingredient.'』';
        //     }
        // }   
       

        $sc_ingredient = Null;
        foreach ($susp1->result() as $susp1){
            if($susp1->sc_num == null){}
            else{
                $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.';';
            }
        }
        //echo $drug_ingredient;
        $data['title'] = "處分書:". $id;
        $data['id'] = $susp[0]->s_dp_project;
        $countid = $spcount[0]->fd_num;
        $lastNum = (int)mb_substr($countid, -4, 4);
        $lastNum++;
        $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $data['fd_num']=$taiwan_date.$value;
        if(isset($sp[0]))$data['sp'] = $sp[0];
        $data['cases'] = $cases[0];
        $data['susp'] = $susp[0];
        if(isset($phone[0]))$data['phone'] = $phone[0];
        $data['drug'] = $drug;
		$data['urine'] = $urine;
        if(isset($fine[0]))$data['fine'] = $fine[0];
        
        if(isset($drug2[0]))$data['drug2'] = $drug2[0];
        $data['drug_ingredient'] = $drug_ingredient;
        $data['drug_ingredient1'] = $drug_ingredient1;
        $data['empno'] = $this->session->userdata('uname');
        $data['sc_ingredient'] = $sc_ingredient;
        $data['opt'] = $type_options;
        $data['opt1'] = $type_options1;
        $data['opt2'] = $type_options2;
        $data['course_opt'] = $course_options;
        $data['pjid'] = $pjid;
		$data['dp'] = $dp[0];
        if(isset($suspfadai[0]))$data['suspfadai'] = $suspfadai[0];
        if(isset($fd1[0])){$data['fd'] = $fd1[0]; }else{ $data['fd'] = null; }
        $data['nav'] = "navbar3";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dpedready';
        else $data['include'] = 'disciplinary_query/dpedready';
        $this->load->view('template', $data);
    }  
    
    function updateready(){//更新送批資料
        //var_dump($_POST);
        $fd1 = $this->getsqlmod->getFD('fd_snum',$_POST['s_num'])->result();
        $fr1 = $this->getsqlmod->getFR('f_snum',$_POST['s_num'])->result();
        $phone1 = $this->getsqlmod->getPhone('p_s_num',$_POST['s_num'])->result();
        $suspfadai = $this->getsqlmod->get1SuspFadai($_POST['s_ic'])->result();

        $susp = array(
            's_name'  => $_POST['s_name'],
            's_ic'  => $_POST['s_ic'],
            's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
            's_prison'  => $_POST['s_prison'],
            's_live_state' => (((int)$_POST['ad'] == 1)?1:((isset($_POST['s_prison']) && !empty($_POST['s_prison']))?3:2)),
			's_dpzipcode' => $_POST['s_dpzipcode'],
			's_rpzipcode' => $_POST['s_rpzipcode'],
			's_dpaddress' => $_POST['s_dpaddress'],
			's_rpaddress' => $_POST['s_rpaddress']
        );
        $phone = array(
            'p_no' => $_POST['p_no'],
            'p_s_ic' => $_POST['s_ic'],
            'p_s_num' => $_POST['s_num'],
            'p_s_cnum' => $_POST['s_cnum'],
        );
        $fd = array(
            'fd_sic' => $_POST['s_ic'],
            'fd_snum' => $_POST['s_num'],
            'fd_cnum' => $_POST['s_cnum'],
            'fd_wantdis' => $_POST['fd_wantdis'],
            'fd_drug_msg' => $_POST['fd_drug_message'],
			'fd_send_num' => $_POST['fd_send_num'],
            // 'fd_dis_msg'  => $_POST['fd_dis_msg'],
            'fd_fact_text'  => trim($_POST['fd_fact_text']),
			'fd_other_lec' => (isset($_POST['special_situ'])?1:null),
            'fd_lec_time'  => (isset($_POST['fd_lec_time']) && !empty($_POST['fd_lec_time']))?$_POST['fd_lec_time']:null,
            'fd_lec_date'  => (isset($_POST['fd_lec_date']) && !empty($_POST['fd_lec_date']))?$_POST['fd_lec_date']:null,
            'fd_lec_address'  => (isset($_POST['fd_lec_address']) && !empty($_POST['fd_lec_address']))?rtrim(explode("(", $_POST['fd_lec_address'])[1], ')'):null,
            'fd_lec_place'  => (isset($_POST['fd_lec_address']) && !empty($_POST['fd_lec_address']))?explode("(", $_POST['fd_lec_address'])[0]:null,
			'fd_amount' => $_POST['f_damount'],
			'fd_merge' => (isset($_POST['fd_merge']))?$_POST['fd_merge']:null,
			'fd_times' => $_POST['seltimes'],
			'fd_zipcode' => $_POST['fd_zipcode'],
            'fd_address'  => (((int)$_POST['ad'] == 1)?$_POST['s_dpaddress']:$_POST['s_rpaddress']),
			'fd_acrrod_unit' => $_POST['fd_acrrod_unit'],
			'fd_acrrod_date' => $this->tranfer2ADyear($_POST['fd_acrrod_date']),
			'fd_accrod_no' => $_POST['fd_accrod_no'],
			'fd_accrod_num' => $_POST['fd_accrod_num']
        );
        
        if(isset($_POST['s_fadai_name']))
        {
            $susp_fadai = array(
                's_fadai_name' => $_POST['s_fadai_name'],
                's_fadai_ic'  => $_POST['s_fadai_ic'],
                's_fadai_sic'  => $_POST['s_ic'],
                's_fadai_address'  => $_POST['s_fadai_address'],
                's_fadai_birth'  => $this->tranfer2ADyear($_POST['s_fadai_birth']),
                's_fadai_phone'  => $_POST['s_fadai_phone'],
                's_fadai_gender'  => $_POST['s_fadai_gender'],
            );
            if(isset($suspfadai[0]))
            {
                $this->getsqlmod->updateSuspFadai($_POST['s_ic'],$susp_fadai);
            }
            else
            {
                $this->getsqlmod->addSuspFadai($susp_fadai);
            }
        }
        $fine_rec = array(
            'f_snum' => $_POST['s_num'],
            'f_sic' => $_POST['s_ic'],
            'f_cnum' => $_POST['s_cnum'],
            'f_damount' => $_POST['f_damount'],
        );
		
        $drug = array(
            'e_name' => $_POST['e_name'],
            'e_count' => $_POST['e_count'],
            'e_type' => $_POST['drug']
        );
        ////var_dump($_POST);
        //$this->getsqlmod->updateCase($_POST['sp_cnum'],$cases);
        $this->getsqlmod->updateSusp($_POST['s_num'],$susp);
        $this->getsqlmod->updatePhone($_POST['s_num'],$phone);
        if(isset($phone1[0]))$this->getsqlmod->updatePhone($_POST['s_num'],$phone);
        else {
           $this->getsqlmod->addphone($phone);
        }
        if(isset($fd1[0]))$this->getsqlmod->updateFD($_POST['s_num'],$fd);
        else {
           $this->getsqlmod->addFD($fd);
        }
        if(isset($fr1[0]))$this->getsqlmod->updateFR($_POST['s_num'],$fine_rec);
        else {
           $this->getsqlmod->addFR($fine_rec);
        }
        
        
        if($_POST['next']!=null) redirect('disciplinary_c/editSanc_ready/' . $_POST['next']. '/' .$_POST['s_dp_project'],'refresh'); 
        if($_POST['prev']!=null) redirect('disciplinary_c/editSanc_ready/' . $_POST['prev']. '/' .$_POST['s_dp_project'],'refresh'); 
        redirect('disciplinary_c/editSanc_ready/' . $_POST['s_num'] . '/' .$_POST['s_dp_project'] ,'refresh'); 
    }
    function listdp1_ready(){//送批列表
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $dp = $this->getsqlmod->getDP1($id)->result();
		$dp = $this->getsqlmod->getDP1bynum($id)->result();
        if(substr($dp[0]->dp_name, 0, 1) != 'P')
		{
			$test_table = $this->table_dp1($table,$dp,'listdp1_ready');
		}
		else
		{
			$test_table = $this->table_dprb1($table,$dp,'listdp1_ready');
		}
        
        $data['s_table'] = $test_table;
        $data['dp_num'] = $dp[0]->dp_num;
        $data['dp_status'] = $dp[0]->dp_status;
        $data['dp_name'] = $dp[0]->dp_name;
		$data['dp_send_date'] = $this->tranfer2RCyear($dp[0]->dp_send_date);
		$data['dp_send_no'] = $dp[0]->dp_send_no;
		$data['dp_empno'] = $dp[0]->dp_empno;
        $data['id'] = $id;
        $data['title'] = "每日處理專案:".$dp[0]->dp_name;
		if(substr($dp[0]->dp_name, 0, 1) != 'P')
		{
			$data['url_1'] = "案件作業";
			$data['url'] = 'disciplinary_c/listDpProject_Ready';
			$data['rollback'] = 'N';
		}
		else
		{
			$data['url_1'] = "重處作業";
			$data['url'] = 'disciplinary_c/listDpRollbackProject_Ready';
			$data['rollback'] = 'Y';
		}
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dplist1Ready';
        else $data['include'] = 'disciplinary_query/dplist1Ready';
        //$data['include'] = 'disciplinary/dplist1Ready';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    function updateSanc(){
    //    var_dump($_POST);
	//    exit;
        $fd1 = $this->getsqlmod->getFD('fd_snum',$_POST['s_num'])->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_num',$_POST['s_num'])->result();
        $fr1 = $this->getsqlmod->getFR('f_snum',$_POST['s_num'])->result();
        $phone1 = $this->getsqlmod->getPhone('p_s_num',$_POST['s_num'])->result();
        $sc1 = $this->getsqlmod->get2Susp($_POST['s_num'])->result();
        $delivery1 = $this->getsqlmod->getdelivery('fdd_snum',$_POST['s_num'])->result();
        $suspfadai = $this->getsqlmod->get1SuspFadai($_POST['s_ic'])->result();
        if(!empty($_FILES['s_prison_doc']['name'])){
            $_FILES['s_prison_doc']['name']  = $_POST['s_ic'] . $_FILES['s_prison_doc']['name'];
            $config['upload_path']          = 'jiansuo/';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('s_prison_doc')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                //redirect('disciplinary_c/listDpProject/','refresh');
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['s_prison_doc']=$uploadData['file_name'];
                //$this->getsqlmod->uploadgongwen($_POST['dp_num'],$uploadData['file_name']);
            }
        }else{
            if($susp1[0]->s_prison_doc != null)$_POST['s_prison_doc']=$susp1[0]->s_prison_doc;
            else $_POST['s_prison_doc']=null;
        }
        if(!empty($_FILES['sp_doc']['name'])){
            $_FILES['sp_doc']['name']  = $_POST['s_ic'] . $_FILES['sp_doc']['name'];
            $config['upload_path']          = 'drugdoc/';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('sp_doc')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                //redirect('disciplinary_c/listDpProject/','refresh');
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['sp_doc']=$uploadData['file_name'];
                //$this->getsqlmod->uploadgongwen($_POST['dp_num'],$uploadData['file_name']);
            }
        }else{
            if($susp1[0]->sp_doc != null)$_POST['sp_doc']=$susp1[0]->sp_doc;
            else $_POST['sp_doc']=null;
        }
        $susp = array(
            's_roffice' => $_POST['s_roffice'],
            's_roffice1'  => $_POST['s_roffice1'],
            's_fno'  => $_POST['s_fno'],
            's_fdate'  => $this->tranfer2ADyear($_POST['s_fdate']),
             's_name'  => $_POST['s_name'],
             's_ic'  => $_POST['s_ic'],
             's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
            's_prison'  => $_POST['s_prison'],
            's_prison_doc'  => $_POST['s_prison_doc'],
            's_utest_num'  => $_POST['s_utest_num'],
            's_live_state' => (((int)$_POST['ad'] == 1)?1:((isset($_POST['s_prison']) && !empty($_POST['s_prison']))?3:2)),
			's_rpcounty' => (isset($_POST['s_rpcounty']))?$_POST['s_rpcounty']:null,
			's_rpdistrict' => (isset($_POST['s_rpdistrict']))?$_POST['s_rpdistrict']:null,
			's_rpzipcode' => (isset($_POST['s_rpzipcode']))?$_POST['s_rpzipcode']:null,
			's_rpaddress' => (isset($_POST['s_rpaddress']))?$_POST['s_rpaddress']:null,
			's_dpcounty' => (isset($_POST['s_dpcounty']))?$_POST['s_dpcounty']:null,
			's_dpdistrict' => (isset($_POST['s_dpdistrict']))?$_POST['s_dpdistrict']:null,
			's_dpzipcode' => (isset($_POST['s_dpzipcode']))?$_POST['s_dpzipcode']:null,
			's_dpaddress' => (isset($_POST['s_dpaddress']))?$_POST['s_dpaddress']:null,
			'sp_doc' => $_POST['sp_doc']
        );
        $sc = array(
            'sc_level' => '3',
            //'sc_ingredient' => $_POST['sc_ingredient'],
            'sc_snum' => $_POST['s_num'],
            'sc_cnum' => $_POST['s_cnum'],
        );
        $phone = array(
            'p_no' => $_POST['p_no'],
            'p_s_ic' => $_POST['s_ic'],
            'p_s_num' => $_POST['s_num'],
            'p_s_cnum' => $_POST['s_cnum'],
        );
        $delivery = array(
            'fdd_fdnum'  => $_POST['fd_num'],
            'fdd_snum'  => $_POST['s_num'],
            'fdd_cnum'  => $_POST['s_cnum'],
        );
		//$BVC = '21196'.$_POST['s_go_no'].$d;
        $code =  '21719'.$_POST['fd_send_num'];
        $code1 = preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY);
        $code2=0;
        for($i=0;$i<count($code1);$i++){
            if($i==0||$i==3||$i==6||$i==9||$i==12||$i==15) $code2=$code2+$code1[$i]*3;
            if($i==1||$i==4||$i==7||$i==10||$i==13) $code2=$code2+($code1[$i]*7);
            if($i==2||$i==5||$i==8||$i==11||$i==14) $code2=$code2+($code1[$i]*1);
        }
        $code3 = $code2%10;
        $code4 = 10-$code3;
        $fin = $code.$code4;
        $fd = array(
            'fd_num' => $_POST['fd_num'],
             'fd_listed_no' => $_POST['fd_num'],
             'fd_sic' => $_POST['s_ic'],
             'fd_snum' => $_POST['s_num'],
             'fd_date' => ((isset($_POST['fd_date']))?$this->tranfer2ADyear($_POST['fd_date']):null),
             'fd_send_num' => $_POST['fd_send_num'],
             'fd_cnum' => $_POST['s_cnum'],
             'fd_wantdis' => $_POST['fd_wantdis'],
             'fd_zipcode' => (isset($_POST['ifsend2susp']))?join(",",$_POST['fd_zipcode']):$_POST['fd_zipcode'][0],
             'fd_drug_msg' => $_POST['fd_drug_message'],
             'fd_dis_msg'  => $_POST['fd_dis_msg'],
             'fd_target'  => (isset($_POST['ifsend2susp']))?join(",",$_POST['fd_target']):$_POST['fd_target'][0],
             'fd_address'  => (isset($_POST['ifsend2susp']))?join(",",$_POST['fd_address']):$_POST['fd_address'][0],
             'fd_empno'  => (isset($_POST['ifsend2susp']))?join(",",$_POST['fd_empno']):$_POST['fd_empno'][0],
			 'fd_other_lec' => 1,
             'fd_lec_time'  => $_POST['fd_lec_time'],
             'fd_lec_date'  => $_POST['fd_lec_date'],
             'fd_lec_address'  => ((isset($_POST['fd_lec_address']) && !empty($_POST['fd_lec_address']))?rtrim(explode("(", $_POST['fd_lec_address'])[1], ')'):''),
             'fd_lec_place'  => ((isset($_POST['fd_lec_address']) && !empty($_POST['fd_lec_address']))?explode("(", $_POST['fd_lec_address'])[0]:''),
			 'fd_amount' => $_POST['f_damount'],
			 'fd_merge' => (isset($_POST['fd_merge']))?$_POST['fd_merge']:null,
			 'fd_times' => $_POST['seltimes'],
			 'fd_bvc' => $fin,
			 'fd_limitdate' => ((isset($_POST['f_date']))?$this->tranfer2ADyear($_POST['f_date']):null),
        );		
		
		if(isset($_POST['s_fadai_name']))
		 {
			$susp_fadai = array(
				's_fadai_name' => $_POST['s_fadai_name'],
				's_fadai_ic'  => $_POST['s_fadai_ic'],
				's_fadai_sic'  => $_POST['s_ic'],
				's_fadai_county'  => $_POST['s_fadai_county'],
				's_fadai_district'  => $_POST['s_fadai_district'],
				's_fadai_zipcode'  => $_POST['s_fadai_zipcode'],
				's_fadai_address'  => $_POST['s_fadai_address'],
				's_fadai_birth'  => $this->tranfer2ADyear($_POST['s_fadai_birth']),
				's_fadai_phone'  => $_POST['s_fadai_phone'],
				's_fadai_gender'  => $_POST['s_fadai_gender'],
			);
			if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['s_ic'],$susp_fadai);
			else 
			{
				if(isset($_POST['s_fadai_name']))
				{
					$this->getsqlmod->addSuspFadai($susp_fadai);
				}
			}
		 }
        
        $e_id = $_POST['e_id'];
        $drug = array(
            'e_name' => $_POST['e_name'],
            'e_count' => $_POST['e_count'],
            'e_type' => $_POST['drug'],
			'e_unit' => $_POST['e_type']
        );
        
        $fine_rec = array(
            'f_snum' => $_POST['s_num'],
            'f_sic' => $_POST['s_ic'],
            'f_cnum' => $_POST['s_cnum'],
            'f_date' => $this->tranfer2ADyear($_POST['f_date']),
            'f_damount' => $_POST['f_damount'],
            'f_empno' => $_POST['fd_empno'][0],
            'f_BVC' => $fin,
        );
        ////var_dump($_POST);
        //$this->getsqlmod->updateCase($_POST['sp_cnum'],$cases);
        $this->getsqlmod->updateSusp($_POST['s_num'],$susp);
        // if(isset($sc1[0]))$this->getsqlmod->updatesc_up($_POST['s_num'],$sc);
        // else {
        //    $this->getsqlmod->addsc($sc);
        // }
        if(isset($phone1[0]))
        {
            $this->getsqlmod->updatePhone($_POST['s_num'],$phone);
        }
        else {
           $this->getsqlmod->addphone($phone);
        }
        if(isset($delivery1[0]))$this->getsqlmod->updatedelivery($_POST['s_num'],$delivery);
        else {
           $this->getsqlmod->adddelivery($delivery);
        }
        if(isset($fd1[0]))
		{
			$this->getsqlmod->updateFD($_POST['s_num'],$fd);
		}
        else {
           $this->getsqlmod->addFD($fd);
        }
        if(isset($fr1[0]))$this->getsqlmod->updateFR($_POST['s_num'],$fine_rec);
        else {
           $this->getsqlmod->addFR($fine_rec);
        }
        

        // if(isset($e_id))
        // {
        //     $this->getsqlmod->updateDrug1($e_id, $drug);
        // }
        // else
        // {
        //     $d_count = (int)mb_substr($this->getsqlmod->get1DrugCount()->result()[0]->e_id, -5, 5);
        //     $eid = array(
        //         'e_id' => 'EN' . (date('Y')-1911) . str_pad($d_count++,5,'0',STR_PAD_LEFT)
        //     );
        //     $this->getsqlmod->adddrug(array_merge($drug, $eid));
        // }
        if($_POST['next']!=null) redirect('disciplinary_c/editSanc/' . $_POST['next'] . '/' . $_POST['pjid'],'refresh'); 
        if($_POST['prev']!=null) redirect('disciplinary_c/editSanc/' . $_POST['prev'] . '/' . $_POST['pjid'],'refresh'); 
		
        redirect('disciplinary_c/listdp1/' . $_POST['pjid'],'refresh'); 
    }

    // 自動儲存
    function autoUpdateSanc(){
        
         $fd1 = $this->getsqlmod->getFD('fd_snum',$_POST['s_num'])->result();
         $susp1 = $this->getsqlmod->get1Susp_ed('s_num',$_POST['s_num'])->result();
         $fr1 = $this->getsqlmod->getFR('f_snum',$_POST['s_num'])->result();
         $phone1 = $this->getsqlmod->getPhone('p_s_num',$_POST['s_num'])->result();
         $sc1 = $this->getsqlmod->get2Susp($_POST['s_num'])->result();
         $delivery1 = $this->getsqlmod->getdelivery('fdd_snum',$_POST['s_num'])->result();
         $suspfadai = $this->getsqlmod->get1SuspFadai($_POST['s_ic'])->result();
         if(!empty($_FILES['s_prison_doc']['name'])){
             $_FILES['s_prison_doc']['name']  = $_POST['s_ic'] . $_FILES['s_prison_doc']['name'];
             $config['upload_path']          = 'jiansuo/';
             $config['allowed_types']        = 'pdf';
             $config['max_size']             = 10000;
             $this->load->library('upload', $config);
             if ( ! $this->upload->do_upload('s_prison_doc')){
                 $error = array('error' => $this->upload->display_errors());
                 //var_dump($error);
                 //redirect('disciplinary_c/listDpProject/','refresh');
             }
             else{
                 $uploadData = $this->upload->data();
                 $_POST['s_prison_doc']=$uploadData['file_name'];
                 //$this->getsqlmod->uploadgongwen($_POST['dp_num'],$uploadData['file_name']);
             }
         }
         else{
             if($susp1[0]->s_prison_doc != null)$_POST['s_prison_doc']=$susp1[0]->s_prison_doc;
             else $_POST['s_prison_doc']=null;
         }

		 if(!empty($_FILES['sp_doc']['name'])){
            $_FILES['sp_doc']['name']  = $_POST['s_ic'] . $_FILES['sp_doc']['name'];
            $config['upload_path']          = 'drugdoc/';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('sp_doc')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                //redirect('disciplinary_c/listDpProject/','refresh');
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['sp_doc']=$uploadData['file_name'];
                //$this->getsqlmod->uploadgongwen($_POST['dp_num'],$uploadData['file_name']);
            }
        }else{
            if($susp1[0]->sp_doc != null)$_POST['sp_doc']=$susp1[0]->sp_doc;
            else $_POST['sp_doc']=null;
        }
         $susp = array(
             's_roffice' => $_POST['s_roffice'],
             's_roffice1'  => $_POST['s_roffice1'],
             's_fno'  => $_POST['s_fno'],
             's_fdate'  => $this->tranfer2ADyear($_POST['s_fdate']),
             's_name'  => $_POST['s_name'],
             's_ic'  => $_POST['s_ic'],
             's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
             's_prison'  => $_POST['s_prison'],
             's_prison_doc'  => $_POST['s_prison_doc'],
             's_utest_num'  => $_POST['s_utest_num'],
             's_live_state' => (((int)$_POST['ad'] == 1)?1:((isset($_POST['s_prison']) && !empty($_POST['s_prison']))?3:2)),
			 's_rpcounty' => (isset($_POST['s_rpcounty']))?$_POST['s_rpcounty']:null,
			's_rpdistrict' => (isset($_POST['s_rpdistrict']))?$_POST['s_rpdistrict']:null,
			's_rpzipcode' => (isset($_POST['s_rpzipcode']))?$_POST['s_rpzipcode']:null,
			's_rpaddress' => (isset($_POST['s_rpaddress']))?$_POST['s_rpaddress']:null,
			's_dpcounty' => (isset($_POST['s_dpcounty']))?$_POST['s_dpcounty']:null,
			's_dpdistrict' => (isset($_POST['s_dpdistrict']))?$_POST['s_dpdistrict']:null,
			's_dpzipcode' => (isset($_POST['s_dpzipcode']))?$_POST['s_dpzipcode']:null,
			's_dpaddress' => (isset($_POST['s_dpaddress']))?$_POST['s_dpaddress']:null,
			 'sp_doc' => $_POST['sp_doc']
         );
         $sc = array(
             'sc_level' => '3',
             //'sc_ingredient' => $_POST['sc_ingredient'],
             'sc_snum' => $_POST['s_num'],
             'sc_cnum' => $_POST['s_cnum'],
         );
         $phone = array(
             'p_no' => $_POST['p_no'],
             'p_s_ic' => $_POST['s_ic'],
             'p_s_num' => $_POST['s_num'],
             'p_s_cnum' => $_POST['s_cnum'],
         );
         $delivery = array(
             'fdd_fdnum'  => $_POST['fd_num'],
             'fdd_snum'  => $_POST['s_num'],
             'fdd_cnum'  => $_POST['s_cnum'],
         );
		 //$BVC = '21196'.$_POST['s_go_no'].$d;
         $code =  '21719'.$_POST['fd_send_num'];
         $code1 = preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY);
         $code2=0;
         for($i=0;$i<count($code1);$i++){
             if($i==0||$i==3||$i==6||$i==9||$i==12||$i==15) $code2=$code2+$code1[$i]*3;
             if($i==1||$i==4||$i==7||$i==10||$i==13) $code2=$code2+($code1[$i]*7);
             if($i==2||$i==5||$i==8||$i==11||$i==14) $code2=$code2+($code1[$i]*1);
         }
         $code3 = $code2%10;
         $code4 = 10-$code3;
         $fin = $code.$code4;
         $fd = array(
             'fd_num' => $_POST['fd_num'],
             'fd_listed_no' => $_POST['fd_num'],
             'fd_sic' => $_POST['s_ic'],
             'fd_snum' => $_POST['s_num'],
             'fd_date' => ((isset($_POST['fd_date']))?$this->tranfer2ADyear($_POST['fd_date']):null),
             'fd_send_num' => $_POST['fd_send_num'],
             'fd_cnum' => $_POST['s_cnum'],
             'fd_wantdis' => $_POST['fd_wantdis'],
             'fd_zipcode' => (isset($_POST['ifsend2susp']))?join(",",$_POST['fd_zipcode']):$_POST['fd_zipcode'][0],
             'fd_drug_msg' => $_POST['fd_drug_message'],
             'fd_dis_msg'  => $_POST['fd_dis_msg'],
             'fd_target'  => (isset($_POST['ifsend2susp']))?join(",",$_POST['fd_target']):$_POST['fd_target'][0],
             'fd_address'  => (isset($_POST['ifsend2susp']))?join(",",$_POST['fd_address']):$_POST['fd_address'][0],
             'fd_empno'  => (isset($_POST['ifsend2susp']))?join(",",$_POST['fd_empno']):$_POST['fd_empno'][0],
			 'fd_other_lec' => 1,
             'fd_lec_time'  => $_POST['fd_lec_time'],
             'fd_lec_date'  => $_POST['fd_lec_date'],
             'fd_lec_address'  => ((isset($_POST['fd_lec_address']) && !empty($_POST['fd_lec_address']))?rtrim(explode("(", $_POST['fd_lec_address'])[1], ')'):''),
             'fd_lec_place'  => ((isset($_POST['fd_lec_address']) && !empty($_POST['fd_lec_address']))?explode("(", $_POST['fd_lec_address'])[0]:''),
			 'fd_amount' => $_POST['f_damount'],
			 'fd_merge' => (isset($_POST['fd_merge']))?$_POST['fd_merge']:null,
			 'fd_times' => $_POST['seltimes'],
			 'fd_bvc' => $fin
         );
		 
		 if(isset($_POST['s_fadai_name']))
		 {
			$susp_fadai = array(
				's_fadai_name' => $_POST['s_fadai_name'],
				's_fadai_ic'  => $_POST['s_fadai_ic'],
				's_fadai_sic'  => $_POST['s_ic'],
				's_fadai_county'  => $_POST['s_fadai_county'],
				's_fadai_district'  => $_POST['s_fadai_district'],
				's_fadai_zipcode'  => $_POST['s_fadai_zipcode'],
				's_fadai_address'  => $_POST['s_fadai_address'],
				's_fadai_birth'  => $this->tranfer2ADyear($_POST['s_fadai_birth']),
				's_fadai_phone'  => $_POST['s_fadai_phone'],
				's_fadai_gender'  => $_POST['s_fadai_gender'],
			);
			if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['s_ic'],$susp_fadai);
			else $this->getsqlmod->addSuspFadai($susp_fadai);
		 }
         
         $e_id = $_POST['e_id'];
        $drug = array(
            'e_name' => $_POST['e_name'],
            'e_count' => $_POST['e_count'],
            'e_type' => $_POST['drug'],
			'e_unit' => $_POST['e_type']
        );
         
         $fine_rec = array(
             'f_snum' => $_POST['s_num'],
             'f_sic' => $_POST['s_ic'],
             'f_cnum' => $_POST['s_cnum'],
             'f_date' => $this->tranfer2ADyear($_POST['f_date']),
             'f_damount' => $_POST['f_damount'],
             'f_empno' => $_POST['fd_empno'][0],
             'f_BVC' => $fin,
         );
         ////var_dump($_POST);
         //$this->getsqlmod->updateCase($_POST['sp_cnum'],$cases);
         $this->getsqlmod->updateSusp($_POST['s_num'],$susp);
        //  if(isset($sc1[0]))$this->getsqlmod->updatesc_up($_POST['s_num'],$sc);
        //  else {
        //     $this->getsqlmod->addsc($sc);
        //  }
         if(isset($phone1[0]))$this->getsqlmod->updatePhone($_POST['s_num'],$phone);
         else {
            $this->getsqlmod->addphone($phone);
         }
         if(isset($delivery1[0]))$this->getsqlmod->updatedelivery($_POST['s_num'],$delivery);
         else {
            $this->getsqlmod->adddelivery($delivery);
         }
         if(isset($fd1[0]))$this->getsqlmod->updateFD($_POST['s_num'],$fd);
        else {
           $this->getsqlmod->addFD($fd);
        }
         if(isset($fr1[0]))$this->getsqlmod->updateFR($_POST['s_num'],$fine_rec);
         else {
            $this->getsqlmod->addFR($fine_rec);
         }
        

        //  if(isset($e_id))
        // {
        //     $this->getsqlmod->updateDrug1($e_id, $drug);
        // }
        // else
        // {
        //     $d_count = (int)mb_substr($this->getsqlmod->get1DrugCount()->result()[0]->e_id, -5, 5);
        //     $eid = array(
        //         'e_id' => 'EN' . (date('Y')-1911) . str_pad($d_count++,5,'0',STR_PAD_LEFT)
        //     );
        //     $this->getsqlmod->adddrug(array_merge($drug, $eid));
        // }
         if($_POST['next']!=null) redirect('disciplinary_c/editSanc/' . $_POST['next']. '/' . $_POST['s_dp_project'],'refresh'); 
         if($_POST['prev']!=null) redirect('disciplinary_c/editSanc/' . $_POST['prev']. '/' . $_POST['s_dp_project'],'refresh'); 
         redirect('disciplinary_c/listdp1/' . $_POST['s_dp_project'],'refresh'); 
     }
    
    function editSanc(){//批次裁處
        $this->load->library('table');
        $id = $this->uri->segment(3);
        $pjid = $this->uri->segment(4);
        $fd1 = $this->getsqlmod->getFD('fd_snum',$id)->result();
        $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
        $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp[0]->s_dp_project)->result();
        $sp=$this->getsqlmod->get3SP1($id)->result();
        $spcount=$this->getsqlmod->getfdAll()->result();
        $cases=$this->getsqlmod->getCases($susp[0]->s_cnum)->result();
        $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp[0]->s_dp_project)->result();
        $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
        $susp1 = $this->getsqlmod->get2Susp($susp[0]->s_num)->result(); // 抓snum
        $drug=$this->getsqlmod->get1Drug_ic_ind($susp[0]->s_ic)->result();
		$urine = $this->getsqlmod->get2Susp($susp[0]->s_num)->result();
        $suspfadai = $this->getsqlmod->get1SuspFadai($susp[0]->s_ic)->result();
        if(isset($drug[0]))$drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug[0]->e_id)->result();
        $suspfadai = $this->getsqlmod->get1SuspFadai($susp[0]->s_ic)->result();
        $prison=$this->getsqlmod->getWordlibPrison()->result();
        $courses=$this->getsqlmod->getCourseData()->result();
		// $dp = $this->getsqlmod->getdpprojectname($pjid)->result();
		$dp = $this->getsqlmod->getDP1bynum($pjid)->result();
        ////var_dump($DpOrder);
		// var_dump($drug);
		// exit;
        foreach ($DpOrder as $order){
            $arr[] = $order->s_num;
        }
        $id; 
        $i=0;
       // echo count($arr);
        //print_r($arr);
        while ($arr[$i] !== $id) {
            $i++;
            next($arr); 
        }
        //echo $arr[$i];
        if($i>0){
            $data['prev'] = $arr[$i-1];
        }
        if($i==0){
            $data['prev'] = NULL;
        }
        if($i == count($arr)-1){
            $data['next'] = NULL;
        }
        if($i < count($arr)-1){
            $data['next'] = $arr[$i+1];
        }
        $type_options = array(null => '無');
        foreach ($prison as $prison){
            $type_options[$prison->prison_name . ':' . $prison->prison_address] = $prison->prison_name . ' （' . $prison->prison_address . '）';
        }
        $course_options = array(null => '請選擇(無)');
        foreach ($courses as $course){
            $course_options[trim($course->c_date) . '-' . trim($course->c_week) . '-' . trim($course->c_time) . '-' .preg_replace('/\r\n|\n/',"",trim($course->c_place))] = trim($course->c_date) . ' (' . trim($course->c_week) . ')時段：' . trim($course->c_time);
        }
		
        $type_options1 = array(
            '晶體粉末、碎塊' => '晶體粉末、碎塊' ,
            '藥碇' => '藥碇',
            '梅碇' => '梅碇',
            '即溶包' => '即溶包',
            '香菸(含菸蒂)' => '香菸(含菸蒂)',
            '菸草' => '菸草',
            '濾嘴' => '濾嘴',
            '吸食器' => '吸食器',
            '吸管' => '吸管',
            '卡片' => '卡片',
            '括盤' => '括盤',
            '棉花棒' => '棉花棒',
            '殘渣袋' => '殘渣袋',
            '大麻' => '大麻',
            '手機殼' => '手機殼',
            '植株' => '植株',
            '種子' => '種子',
			'菸盒' => '菸盒',
			'紙張' => '紙張',
			'其他' => '其他'
       );   
        $type_options2 = array(
            '包' => '包' ,
            '顆' => '顆',
            '支' => '支',
            '張' => '張',
            '個' => '個',
            '組' => '組',
            '株' => '株',
        );
        $drug_ingredient = Null;
        if(isset($drug2)){
        foreach ($drug2 as $drug3){
            if(!isset($drug3->ddc_level)){}
            else{
                $drug_ingredient = $drug_ingredient . $drug3->ddc_level .'級' . $drug3->ddc_ingredient.';';
            }
        }}  
        $sc_ingredient = Null;
        foreach ($susp1 as $susp1){
            if($susp1->sc_num == null){}
            else{
                $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.';';
            }
        }
        //echo $drug_ingredient;
        $data['title'] = "裁處修改:". $id;
        $countid = ((isset($spcount[0]))?$spcount[0]->fd_num:'0000');
        $lastNum = (int)mb_substr($countid, -4, 4);
        $lastNum++;
        $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $data['fd_num']=$taiwan_date.$value;
        if(isset($sp[0]))$data['sp'] = $sp[0];
        $data['cases'] = (isset($cases[0]))?$cases[0]:[];
        $data['susp'] = $susp[0];
        if(isset($phone[0]))$data['phone'] = $phone[0];
        if(isset($drug[0]))$data['drug'] = $drug;
        if(isset($drug2[0]))$data['drug2'] = $drug2[0];
        if(isset($fine[0]))$data['fine'] = $fine[0];
		if(isset($urine[0]))$data['urine'] = $urine;
        if(isset($suspfadai[0]))$data['suspfadai'] = $suspfadai[0];
        $data['drug_ingredient'] = $drug_ingredient;
        $data['empno'] = $dp[0]->dp_empno;//$this->session->userdata('uname');
        $data['sc_ingredient'] = $sc_ingredient;
        $data['opt'] = $type_options;
        $data['opt1'] = $type_options1;
        $data['opt2'] = $type_options2;
        $data['course_opt'] = $course_options;
        // $data['spcount'] = $spcount[0];
		$data['dp'] = $dp[0];
        if(isset($fd1[0])){$data['fd'] = $fd1[0]; }else{ $data['fd'] = null; }
        $data['pjid'] = $pjid;
        $data['nav'] = "navbar3";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'disciplinary/dpediteach';
        else $data['include'] = 'disciplinary_query/dpediteach';
        //$data['include'] = 'disciplinary/dpediteach';
		
        $this->load->view('template', $data);
    }  
    
    function editdrug2(){
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $drugOpt = $this->getsqlmod->getDrug_option()->result();
        $drugAll = $this->getsqlmod->get1DrugAll($id)->result();
        $Susp = $this->getsqlmod->get1DrugSusp($drugAll[0]->e_c_num)->result();
        $drugck = $this->getsqlmod->get1DrugCk($id)->result();
        $drugck2 = $this->getsqlmod->get2DrugCk('ddc_drug',$id)->result();
        //echo $casesrepair[1]->c_num;
        $type_options = array(
            '毒品粉末、碎塊' => '毒品粉末、碎塊' ,
            '藥碇' => '藥碇',
            '梅碇' => '梅碇',
            '即溶包' => '即溶包',
            '香菸(含菸蒂)' => '香菸(含菸蒂)',
            '菸草' => '菸草',
            '濾嘴' => '濾嘴',
            '吸食器' => '吸食器',
            '吸管' => '吸管',
            '卡片' => '卡片',
            '括盤' => '括盤',
            '棉花棒' => '棉花棒',
            '殘渣袋' => '殘渣袋',
            '大麻' => '大麻',
            '手機殼' => '手機殼',
            '植株' => '植株',
            '種子' => '種子',
       );   
        $type_options1 = array(
            '包' => '包' ,
            '顆' => '顆',
            '支' => '支',
            '張' => '張',
            '個' => '個',
            '組' => '組',
            '株' => '株',
        );
        // $type_options2 = array(
        //     '海洛因' => '海洛因' ,
        //     '安非他命' => '安非他命',
        //     '搖頭丸(MDMA)' => '搖頭丸(MDMA)',
        //     '愷他命(KET)' => '愷他命(KET)',
        //     '一粒眠' => '一粒眠',
        //     '大麻' => '大麻',
        //     '卡西酮類' => '卡西酮類',
        //     '古柯鹼' => '古柯鹼',
        //     '無毒品反應' => '無毒品反應',
        // );
		$type_options2 = array('' => '請選擇');
        foreach ($drugOpt as $drugOpt1){
            $type_options2[$drugOpt1->drug_level .'級' .$drugOpt1->drug_name] = $drugOpt1->drug_level . '級' . $drugOpt1->drug_name;
        }
        $type_options3 = array('' => '請選擇');
        foreach ($Susp as $Susp1){
            $type_options3[$Susp1->s_ic] = $Susp1->s_name;
        }
        $type_options4 = array('' => '請選擇');
        foreach ($drugOpt as $drugOpt1){
            $type_options4[$drugOpt1->drug_level .'級' .$drugOpt1->drug_name] = $drugOpt1->drug_level . '級' . $drugOpt1->drug_name;
        }
        $data['drugAll'] = $drugAll[0];
        $data['e_s_ic'] = $Susp1->s_ic;
        $data['drugck'] = $drugck;
        $data['drugck2'] = $drugck2;
        ////var_dump($drugck2);
        $data['nameopt'] = $type_options;
        $data['countopt'] = $type_options1;
        $data['nwopt'] = $type_options2;
        $data['test'] = $type_options3;
        $data['drug_opt'] = $type_options4;
        $data['e_id'] = $id;
        $data['e_c_num'] = $drugAll[0]->e_c_num;
        $data['title'] = "毒品修改:". $id;
        $data['nav'] = "navbar3";
        $data['user'] = $this -> session -> userdata('uic');
        $data['headline'] = "測試案件處理系統";
        $data['include'] = 'disciplinary/drug2_edit';
        $this->load->view('template', $data);
    }  
    
    public function returnSanc() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $id = $this->uri->segment(3);
        $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        $this->getsqlmod->updateSusp_sac_state($id,'退回補正');      
		redirect('disciplinary_c/listdp', 'refresh');  
        // redirect('disciplinary_c/listdp1/' . $susp[0]->s_dp_project,'refresh'); 
    }
    public function update_drug2() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        if(!empty($_FILES['e_doc']['name'])){
            $_FILES['e_doc']['name']  = $_POST['e_id'] . $_FILES['e_doc']['name'];
            $config['upload_path']          = 'drugdoc/';
            $config['allowed_types']        = 'doc|pdf|docx';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('e_doc')){
                $error = array('error' => $this->upload->display_errors());
                // print_r($error);
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['e_doc']=$uploadData['file_name'];
                
                if($_POST["link"]=="1"){
                    $df2 = array (
						"df_type" => '複驗',
						  'df_drug' => $_POST['e_id']
					 );
					$this->getsqlmod->adddrugInd($df2);
                    redirect('disciplinary_c/editdrug2/' . $_POST['e_id'],'refresh'); 
                }else{
					$df2_numAry = $_POST['df_num2'];    
					foreach ($_POST['df2'] as $df2key => $df2value) {
						if(!empty($df2value))
						{
							$df2Ary = explode('級', $df2value);
							$updateAry = array(
								"df_type" => '複驗',
								"df_drug" => $_POST['e_id'],
								'df_level' => $df2Ary[0],
								'df_ingredient' => $df2Ary[1],
								'df_PNW' => $_POST['df_PNW'][$df2key],
								'df_NW' => $_POST['df_NW'][$df2key],
								'df_PNWS' => $_POST['df_PNWS'][$df2key]
							);
			
							$this->getsqlmod->deleteDrugckc('df_num', $df2_numAry[$df2key]);
							$this->getsqlmod->adddrugInd($updateAry);
						}
						
					}
					unset($_POST['df']);
					unset($_POST['df2']);
					unset($_POST['df_num']);
					unset($_POST['df_num2']);
					unset($_POST['df_PNW']);
					unset($_POST['df_NW']);
					unset($_POST['df_PNWS']);
					unset($_POST['link']);
                    ////var_dump($_POST);
                    $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['e_s_ic'])->result();
                    $_POST['e_suspect']=$susp[0]->s_name;
                    $_POST['e_state']='待送驗';
                    $_POST['e_place']=$this->session->userdata('uoffice');
                    $this->getsqlmod->updateDrug1($_POST['e_id'],$_POST);
                    redirect('disciplinary_c/othernew/' . $_POST['e_c_num'],'refresh'); 
                }
            }
        }
        else{
            
            if($_POST["link"]=="1"){
                $df2 = array (
					"df_type" => '複驗',
					  'df_drug' => $_POST['e_id']
				 );
				$this->getsqlmod->adddrugInd($df2);
                redirect('disciplinary_c/editdrug2/' . $_POST['e_id'],'refresh'); 
            }else{
				$df2_numAry = $_POST['df_num2'];    
				foreach ($_POST['df2'] as $df2key => $df2value) {
					if(!empty($df2value))
					{
						$df2Ary = explode('級', $df2value);
						$updateAry = array(
							"df_type" => '複驗',
							"df_drug" => $_POST['e_id'],
							'df_level' => $df2Ary[0],
							'df_ingredient' => $df2Ary[1],
							'df_PNW' => $_POST['df_PNW'][$df2key],
							'df_NW' => $_POST['df_NW'][$df2key],
							'df_PNWS' => $_POST['df_PNWS'][$df2key]
						);
		
						$this->getsqlmod->deleteDrugckc('df_num', $df2_numAry[$df2key]);
						$this->getsqlmod->adddrugInd($updateAry);
					}
					
				}
				unset($_POST['df']);
				unset($_POST['df2']);
				unset($_POST['df_num']);
				unset($_POST['df_num2']);
				unset($_POST['df_PNW']);
				unset($_POST['df_NW']);
				unset($_POST['df_PNWS']);
				unset($_POST['link']);
                
				$this->getsqlmod->updateDrug1($_POST['e_id'],$_POST);
                redirect('disciplinary_c/othernew/' . $_POST['e_c_num'],'refresh'); 
            }
        }
    }
    
    function uploaddp_doc(){
        if(!empty($_FILES['s_prison_doc']['name'])){
            $_FILES['s_prison_doc']['name']  = $_POST['dp_name'] . $_FILES['s_prison_doc']['name'];
            $config['upload_path']          = 'gongwen/';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('s_prison_doc')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                //redirect('disciplinary_c/listDpProject/','refresh');
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['s_prison_doc']=$uploadData['file_name'];
                $this->getsqlmod->uploadgongwen($_POST['dp_num'],$uploadData['file_name']);
            }
        }
        redirect('disciplinary_c/listDpProject/','refresh');
    }
    function uploaddpdate(){
        //var_dump($_POST);
		$pjid = $this->uri->segment(3);
        // $dp = $this->getsqlmod->getdpproject123($_POST['dp_num'])->result();
        //var_dump($dp);
        $fp3 = $this->getsqlmod->getfp3($pjid)->result();       
		$updatedata = array();
		// if(isset($_POST['dp_send_no']))
		// {
		// 	$updatedata = array(
		// 		'dp_send_no' => $_POST['dp_send_no'],
		// 		'dp_send_date' => $this->tranfer2ADyear($_POST['dp_send_date'])
		// 	);
		// }
		// else
		// {
			$updatedata = array(
				'dp_send_date' => $this->tranfer2ADyear($_POST['dp_send_date'])
			);
		// }
		
        $this->getsqlmod->updatedpdate($_POST['dp_num'], $updatedata);
		foreach ($fp3 as $sp) {
			// if(isset($_POST['dp_send_no']))
			// {
			// 	$updatedata2 = array(
			// 		'fd_send_num' => $_POST['dp_send_no'],
			// 		'fd_date' => $this->tranfer2ADyear($_POST['dp_send_date'])
			// 	);
			// }
			// else
			// {
				$updatedata2 = array(
					'fd_date' => $this->tranfer2ADyear($_POST['dp_send_date'])
				);
			// }

			$this->getsqlmod->updatedpdateAll($sp->s_num, $updatedata2);
		}
        
        redirect('disciplinary_c/listdp1_ready/'.$pjid,'refresh');
    }
	function uploaddpexpdate(){
        //var_dump($_POST);
		$pjid = $this->uri->segment(3);
        // $dp = $this->getsqlmod->getdpproject123($_POST['dp_num'])->result();
        //var_dump($dp);
        // $fp3 = $this->getsqlmod->getfp3($dp[0]->dp_name)->result();       
		if(isset($_POST['dp_empno']) && !empty($_POST['dp_empno']))
		{
			$this->getsqlmod->updatedp_expirdate($_POST['dp_num'], $this->tranfer2ADyear($_POST['f_date']), $_POST['dp_empno']);
		}
        else
		{
			$this->getsqlmod->updatedp_expirdate_noemp($_POST['dp_num'], $this->tranfer2ADyear($_POST['f_date']));
		}
        // $this->getsqlmod->updatedpdateAll($fp3[0]->s_num,$_POST['dp_send_date']);
        // redirect('disciplinary_c/listdp1/'.$pjid,'refresh');
		redirect('disciplinary_c/listdp1/'.$pjid,'refresh');
    }
	function uploaddpcourse(){
        //var_dump($_POST);
		$pjid = $this->uri->segment(3);
        // $dp = $this->getsqlmod->getdpproject123($_POST['dp_num'])->result();
        //var_dump($dp);
        // $fp3 = $this->getsqlmod->getfp3($dp[0]->dp_name)->result();       
        $this->getsqlmod->updatedp_course($_POST['dp_num'], preg_replace('/\r\n|\n/',"",$_POST['fd_lec']));
        // $this->getsqlmod->updatedpdateAll($fp3[0]->s_num,$_POST['dp_send_date']);
        redirect('disciplinary_c/listdp1/'.$pjid,'refresh');
    }
    function deleteCk2(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $drugck = $this->getsqlmod->get2DrugCk('ddc_num',$id)->result();
        $this->getsqlmod->deleteDrugCk2($id);
        ////var_dump ($drugck);
        redirect('disciplinary_c/editdrug2/' . $drugck[0]->ddc_drug,'refresh');
    }
    function deleteSuspCk(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $sc = $this->getsqlmod->get2SuspCk('sc_num',$id)->result();
        $this->getsqlmod->deletesuspCk2($id);
        ////var_dump ($drugck);
        redirect('disciplinary_c/othernew/' . $sc[0]->sc_cnum,'refresh');
    }
    
    public function uploadPetitions() {
        //var_dump($_FILES);
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        $petition_answer='';
        $petition_doc1='';
        $petition_doc2='';
        $petition_doc3='';
        $petition_doc_ap1='';
        $petition_doc_ap2='';
        $petition_doc_ap3='';
        foreach ($s_cnum1 as $key => $value) {
            $petition = $this->getsqlmod->getpetition($value)->result();
            $susp = $this->getsqlmod->getpetitionlist($value)->result();
            $data = array(
                'petition_snum' => $susp[0]->s_num,
                'petition_cnum' => $susp[0]->s_cnum,
                'petition_fdnum' => $value,
            );
            //if(!isset($petition[0]->petition_fdnum)){$this->getsqlmod->addpetition($data);}
            if(!empty($_FILES['petition_answer']['name'][$value])){//答辯書
                echo $value;
                $_FILES['file']['name'] = $value.'petition_answer.pdf';
                $_FILES['file']['type'] = $_FILES['petition_answer']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_answer']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_answer']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_answer']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_answer = $uploadData['file_name'];
                    echo $petition_answer;
                }
                else{
                    $error = array('error' => $this->upload->display_errors());
                    //var_dump($error);
                }
            }
            else{
                if(isset($petition[0]->petition_answer))$petition_answer = $petition[0]->petition_answer;
                else $petition_answer =null;
            }
            if(!empty($_FILES['petition_doc1']['name'][$value])){//公文
                $_FILES['file']['name'] = $value.'petition_doc1.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc1']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc1']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc1']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc1']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc1 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc1))$petition_doc1 = $petition[0]->petition_doc1;
                else $petition_doc1 =null;
            }
            if(!empty($_FILES['petition_doc2']['name'][$value])){//公文
                $_FILES['file']['name'] =$value.'petition_doc2.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc2']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc2']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc2']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc2']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc2 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc2))$petition_doc2 = $petition[0]->petition_doc2;
                else $petition_doc2 =null;
            }
            if(!empty($_FILES['petition_doc3']['name'][$value])){//公文
                $_FILES['file']['name'] = $value.'petition_doc3.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc3']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc3']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc3']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc3']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc3 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc3))$petition_doc3 = $petition[0]->petition_doc3;
                else $petition_doc3 =null;
            }
            if(!empty($_FILES['petition_doc_ap1']['name'][$value])){//行政訴訟
                $_FILES['file']['name'] = $value.'petition_doc_ap1.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc_ap1']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc_ap1']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc_ap1']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc_ap1']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc_ap1 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc_ap1))$petition_doc_ap1 = $petition[0]->petition_doc_ap1;
                else $petition_doc_ap1 =null;
            }
            if(!empty($_FILES['petition_doc_ap2']['name'][$value])){//行政訴訟
                $_FILES['file']['name'] = $value.'petition_doc_ap2.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc_ap2']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc_ap2']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc_ap2']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc_ap2']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc_ap2 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc_ap2))$petition_doc_ap2 = $petition[0]->petition_doc_ap2;
                else $petition_doc_ap2 =null;
            }
            if(!empty($_FILES['petition_doc_ap3']['name'][$value])){//行政訴訟
                $_FILES['file']['name'] = $value.'petition_doc_ap3.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc_ap3']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc_ap3']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc_ap3']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc_ap3']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc_ap3 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc_ap3))$petition_doc_ap3 = $petition[0]->petition_doc_ap3;
                else $petition_doc_ap3 =null;
            }
            $data = array(
                //'value' => $value,
                'petition_date' => $_POST['petition_date'][$value],
                'petition_answer' => $petition_answer,
                'petition_doc1' => $petition_doc1,
                'petition_doc1' => $petition_doc1,
                'petition_doc2' => $petition_doc2,
                'petition_doc3' => $petition_doc3,
                'petition_doc_ap1' => $petition_doc_ap1,
                'petition_doc_ap2' => $petition_doc_ap2,
                'petition_doc_ap3' => $petition_doc_ap3,
                'petition_snum' => $susp[0]->s_num,
                'petition_cnum' => $susp[0]->s_cnum,
                'petition_fdnum' => $value,
            );
            ///var_dump($data);
            ///echo '<br>';
            if(isset($petition[0]->petition_fdnum)){
                $this->getsqlmod->updatepetition($value,$data);
            }
            else{
                $this->getsqlmod->addpetition($data);
            }
            redirect('Disciplinary_c/listPetitions/'); 
        };
    }
    
    public function listPetitions() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        //$rp = $this->getsqlmod->getRP1($id)->result();
        $this->load->library('table');
        $name="";
        $ic="";
        $fd_num="";
        $first = date("Y-m-d");
        $last = date("Y-m-d");
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
            $first = '2020-01-01';
            $last = date("Y-m-d");
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
            $first = '2020-01-01';
            $last = date("Y-m-d");
        } 
        if(!empty($_POST["fd_num"]))
        {
            $fd_num = $_POST["fd_num"];
            $first = '2020-01-01';
            $last = date("Y-m-d");
        } 
        $query = $this->getsqlmod->get3Petition_search($name,$ic,$fd_num,$first,$last)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號', '犯嫌人姓名', '身份證','答辯日期','答辯書','訴願公文1','訴願公文2','訴願公文3','行政訴訟公文1','行政訴訟公文2','行政訴訟公文3');
        $table_row = array();
        $i=0;
        foreach ($query as $susp){
            $table_row =NUll;
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->s_ic;
            if(!isset($susp->petition_date)){
                $table_row[] = '<input name="petition_date['.$susp->fd_num.']" type="date" class="form-control" value='.date('Y-m-s').'>';
            }
            else{
                $table_row[] = '<input name="petition_date['.$susp->fd_num.']" type="date" class="form-control" value='.$susp->petition_date.'>';
            }
            if(!isset($susp->petition_answer)){
                $table_row[] = form_upload('petition_answer['.$susp->fd_num.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_answer, '答辯書');
            }
            if(!isset($susp->petition_doc1)||$susp->petition_doc1==null){
                $table_row[] = form_upload('petition_doc1['.$susp->fd_num.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc1, '訴願公文1');
            }
            if(!isset($susp->petition_doc2)||$susp->petition_doc2==null){
                $table_row[] = form_upload('petition_doc2['.$susp->fd_num.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc2, '訴願公文2');
            }
            if(!isset($susp->petition_doc3)||$susp->petition_doc3==null){
                $table_row[] = form_upload('petition_doc3['.$susp->fd_num.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc3, '訴願公文3');
            }
            if(!isset($susp->petition_doc_ap1)||$susp->petition_doc_ap1==null){
                $table_row[] = form_upload('petition_doc_ap1['.$susp->fd_num.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc_ap1, '行政訴訟公文1');
            }
            if(!isset($susp->petition_doc_ap2)||$susp->petition_doc_ap2==null){
                $table_row[] = form_upload('petition_doc_ap2['.$susp->fd_num.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc_ap2, '行政訴訟公文2');
            }
            if(!isset($susp->petition_doc_ap3)||$susp->petition_doc_ap3==null){
                $table_row[] = form_upload('petition_doc_ap3['.$susp->fd_num.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc_ap3, '行政訴訟公文3');
            }
            $this->table->add_row($table_row);
        }   
        $data['title'] = "查詢已送出案件";
        $data['s_table'] = $this->table->generate();
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'disciplinary/listPetitions';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function downloadsignxml(){
        ////var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getdeliverylist_forpublicProject($id); 
        $count = $query->num_rows();
        $su = $query->result();
        $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
        $dom = new DOMDocument("1.0","utf-8"); 
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $implementation = new DOMImplementation();
        $dom->appendChild($implementation->createDocumentType('簽 SYSTEM "104_5_utf8.dtd"')); 
        // display document in browser as plain text 
        // for readability purposes 
        header("Content-Type: text/plain"); 
         
        $root = $dom->createElement("簽"); 
        $dom->appendChild($root); 
        
        $item = $dom->createElement("於"); 
        $root->appendChild($item); 
        
        $item = $dom->createElement("檔號"); 
        $root->appendChild($item); 
        
        $item2 = $dom->createElement("年月日"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(date('Y')-1911 .'年'. date('m').'月'.date('y').'日'); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("機關"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('全銜'); 
        $text = $dom->createTextNode("刑事警察大隊(毒品查緝中心)"); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        
        $item2 = $dom->createElement("主旨"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $text = $dom->createTextNode('有關受處分人'.$su[0]->s_name.' 等'. $count . '人違反「毒品危害防制條例」案件處分書公示送達一案，請核示。'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
          
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "說明：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "一、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("依據文號臚列如下："); 
        $item3->appendChild($text); 
        $i=0;
        foreach($su as $susp){
            $i++;
            $item3 = $dom->createElement('條列'); 
            $item3->setAttribute("序號", $i."、");  
            $item2->appendChild($item3); 
            $item4 = $dom->createElement('文字'); 
            $item3->appendChild($item4); 
            $text = $dom->createTextNode("本局".$susp->s_roffice."北市警". (date("Y",strtotime($susp->fd_date))- 1911) .'月'. date("m",strtotime($susp->fd_date)) .'月'.date("d",strtotime($susp->fd_date)) .'日'.$susp->s_roffice1."分刑字第".$susp->s_fno."號(列管編號：".$susp->fd_listed_no.")。"); 
            $item4->appendChild($text); 
        }
        
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "擬辦：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("奉核後，製作公告及處分書函送臺北市政府秘書處刊登市府公報。"); 
        $item2->appendChild($text); 

        $item = $dom->createElement("敬陳"); 
        $root->appendChild($item); 
        $text = $dom->createTextNode("敬陳　大隊長"); 
        $item->appendChild($text); 

        // echo $dom->saveXML();  
        $this->load->helper('download');
        force_download($datet.'.di', $dom->saveXML());
        redirect('Disciplinary_c/listPublicityProject/'.$id); 
    }
    public function downloadpublicxml(){
        ////var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getdeliverylist_forpublicProject($id); 
        $count = $query->num_rows();
        $su = $query->result();
        $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
        $dom = new DOMDocument("1.0","utf-8"); 
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $implementation = new DOMImplementation();
        $dom->appendChild($implementation->createDocumentType('簽 SYSTEM "104_5_utf8.dtd"')); 
        // display document in browser as plain text 
        // for readability purposes 
        header("Content-Type: text/plain"); 
         
        $root = $dom->createElement("公告"); 
        $dom->appendChild($root); 
        
        $item = $dom->createElement("發文機關"); 
        $root->appendChild($item); 
       
        $item2 = $dom->createElement("全銜"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("我是測試系統"); 
        $item2->appendChild($text); 
       
        $item2 = $dom->createElement("機關代碼"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("379130000C"); 
        $item2->appendChild($text); 
       
        $item = $dom->createElement("發文日期"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("年月日"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(date('Y')-1911 .'年'. date('m').'月'.date('y').'日'); 
        $item2->appendChild($text); 
        
        $item = $dom->createElement("發文字號"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("北市警刑毒緝"); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("文號"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('年度'); 
        $text = $dom->createTextNode(date('Y')-1911); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        $item3 = $dom->createElement('流水號'); 
        $text = $dom->createTextNode('3002347'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        $item3 = $dom->createElement('支號'); 
        $text = $dom->createTextNode('2'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
          
        $item = $dom->createElement("附件"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("主旨"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $text = $dom->createTextNode('公示送達應受送達人'.$su[0]->s_name.' 等'. $count . '人違反「毒品危害防制條例」案處分書'.$count.'分'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "依據：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "一、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("毒品危害防制條例第11條之1第2項。"); 
        $item3->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "二、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("行政程序法第78條第3項。"); 
        $item3->appendChild($text); 
        $suspect ='';
        foreach($su as $susp){
            $suspect = $suspect . $susp->s_name.'、';
        }

        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "公告事項：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("應受送達人".substr($suspect,0,-3)."等".$count."人違反毒品危害防制條例，應送達之處分書9份，現由本局刑事警察大隊毒品查緝中心(地址：臺北市中正區北平東路1號6樓、電話：23932397、承辦人警務員".$susp->fd_empno."保管，應受送達人得隨時前來領取。"); 
        $item2->appendChild($text); 

        // echo $dom->saveXML();  
        $this->load->helper('download');
        force_download($datet.'.di', $dom->saveXML());
        redirect('Disciplinary_c/listPublicityProject/'.$id); 
    }

    function getCourses(){
        $query = $this->getsqlmod->getCourseData()->result(); 
        
        echo json_encode(array('data' => $query));
    }
    function emptyCourses(){
        try {
            $this->getsqlmod->emptyCourseData(); 
        
            echo 'ok';
        } catch (\Throwable $th) {
            //throw $th;
            echo $th;
        }
        
    }
	function getCase_courseDup(){
		$data = array(
			'fd_lec_date' => $_POST['fd_lec_date'],
			'fd_lec_time' => $_POST['fd_lec_time'],
			'fd_lec_place' => explode("(", $_POST['fd_lec_place'])[0],
			'fd_sic' => $_POST['fd_sic']
		);
        $query = $this->getsqlmod->getcase_coursedup($data); 
        
        echo json_encode(array('data' => $query));
    }

	function rollback_fd(){
		$snum = $this->uri->segment(3);
		$fdd_maildate = $this->uri->segment(4);
		$fd = $this->getsqlmod->getFD('fd_snum', $snum)->result()[0];
		$fd_updatedate = array(
			'fd_rollback' => 1,
			'fd_backlog' => $this->tranfer2RCyear($fd->fd_date).'_'.$fd->fd_send_num. "_作廢\n"
		);
		$fdd_updatedate = array(
			'fdd_maildate ' => null,
			'fdd_status' => null
		);
		$sp_update = array(
			's_dp_project ' => null
		);
		$this->getsqlmod->updaterollback_fd($snum, $fd_updatedate);
		$this->getsqlmod->updaterollback_fdd($snum, $fdd_updatedate);
		$this->getsqlmod->updatesp_dp_project($snum, $sp_update);
		redirect('disciplinary_c/listdp','refresh' );
	}

	function getDrudInd(){
		$id = $_POST['e_id'];

        $query = $this->getsqlmod->getdrugInds_double($id)->result(); 
        
        echo json_encode(array('data' => $query));
	}

	/**
	 * 新增 毒品成分
	 */
	public function addDrugInd()
	{
		if(isset($_POST['e_name']) && !empty($_POST['e_name']) && isset($_POST['method']))
		{
			$query = $this->getsqlmod->get1DrugCount()->result(); 
			$countid = $query[0]->e_id;
			$lastNum = (int)mb_substr($countid, -5, 5);
			$lastNum++;
			$value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
			$taiwan_date = date('Y')-1911; 
			$e_id='EN' . $taiwan_date . $value;

			switch ($_POST['method']) {
				case 'insert':
					
					$ind_drug = explode(',', $_POST['df_drug']);
					$ind_level = explode(',', $_POST['df_level']);
					$ind_count = explode(',', $_POST['df_count']);
					$e_type = array();
					for ($j=0; $j < count($ind_drug); $j++) { 
						array_push($e_type, $ind_level[$j] . '級' . $ind_drug[$j]);
					}

					$insertdata = array(
						"e_id" => $e_id,
						"e_c_num" => $_POST['e_c_num'],
						"e_empno" => $this -> session -> userdata('uic'),
						"e_type" => implode("、", $e_type),
						"e_name" => $_POST['e_name'],
						"e_1_N_W" => $_POST['e_1_N_W'],
						"e_suspect" => $_POST['e_suspect'],
						"e_s_ic" => $_POST['e_s_ic'],
						"e_count" => $_POST['e_count'],
						"e_unit" => $_POST['e_unit']
					);
		
					for ($i=0; $i < count($ind_drug); $i++){ 
						// 毒品成份
						$rowData4 = array(
							"df_ingredient" => $ind_drug[$i],
							"df_drug" => $e_id,
							"df_count" => $ind_count[$i],
							"df_level" => $ind_level[$i],
							"df_type" => "複驗"
						);
		
						$this->getsqlmod->adddrugInd($rowData4);
					}
					$this->getsqlmod->adddrug($insertdata);
					break;
				case 'update':
					if(isset($_POST['e_id']) && !empty($_POST['e_id']))
					{
						
						$ind_dfid = explode(',', $_POST['df_id']);
						$ind_drug = explode(',', $_POST['df_drug']);
						$ind_level = explode(',', $_POST['df_level']);
						$ind_count = explode(',', $_POST['df_count']);
						$ind_wtype = explode(',', $_POST['df_wtype']);
						$e_type = array();
						
						if(count($ind_wtype) > 0 )
						{
							
							for ($i=0; $i < count($ind_wtype); $i++){ 
								if($ind_wtype[$i] == "update")
								{
									// 毒品成份
									$rowData4 = array(
										"df_ingredient" => $ind_drug[$i],
										"df_count" => $ind_count[$i],
										"df_level" => $ind_level[$i]
									);
									$this->getsqlmod->updatedrugInd($ind_dfid[$i], $rowData4);
								}
								elseif($ind_wtype[$i] == "insert")
								{
									// 毒品成份
									$rowData5 = array(
										"df_ingredient" => $ind_drug[$i],
										"df_drug" => $_POST['e_id'],
										"df_count" => $ind_count[$i],
										"df_level" => $ind_level[$i],
										"df_type" => "複驗"
									);
	
									$this->getsqlmod->adddrugInd($rowData5);
								}
								
							}

							$inds = $this->getsqlmod->getdrugInd($_POST['e_id'])->result();
							for ($j=0; $j < count($inds); $j++) { 
								array_push($e_type, $inds[$j]->df_level . '級' . $inds[$j]->df_ingredient);
							}

							$updatedata = array(
								"e_type" => implode("、", $e_type),
								"e_name" => $_POST['e_name'],
								"e_1_N_W" => $_POST['e_1_N_W'],
								"e_count" => $_POST['e_count'],
								"e_unit" => $_POST['e_unit']
							);
						}
						else
						{
							$updatedata = array(
								"e_name" => $_POST['e_name'],
								"e_1_N_W" => $_POST['e_1_N_W'],
								"e_count" => $_POST['e_count'],
								"e_unit" => $_POST['e_unit']
							);
						}
						
						$this->getsqlmod->updateDrug1($_POST['e_id'], $updatedata);
					}
					break;
				default:
					# code...
					break;
			}

			

			$query=$this->getsqlmod->get1Drug_ic_ind($_POST['e_s_ic'])->result();
			echo json_encode(array('data' => $query));
		}
		else
		{
			echo 'error';
		}
		
	}
	/**
	 * 刪除 毒品
	 */
	function delDrudInd(){
		$id = $_POST['e_id'];

        $this->getsqlmod->deldrugInds($id); 

		$query=$this->getsqlmod->get1Drug_ic($_POST['e_s_ic'])->result();
		echo json_encode(array('data' => $query));
	}
	/**
	 * 新增 尿檢成分
	 */
	public function addUrineTest()
	{
		if(isset($_POST['sc_ingredient']) && !empty($_POST['sc_ingredient']) && isset($_POST['method']))
		{
			$utest_id = 0;
			switch ($_POST['method']) {
				case 'insert':

					$insertdata = array(
						"sc_ingredient" => $_POST['sc_ingredient'],
						"sc_level" => $_POST['sc_level'],
						"sc_cnum" => $_POST['sc_cnum'],
						"sc_snum" => $_POST['sc_snum']
					);
					$utest_id = $this->getsqlmod->addurineInd($insertdata);
					break;
				case 'update':
					if(isset($_POST['sc_num']) && !empty($_POST['sc_num']))
					{
						$utest_id = $_POST['sc_num'];
						$updatedata = array(
							"sc_ingredient" => $_POST['sc_ingredient'],
							"sc_level" => $_POST['sc_level']
						);
						$this->getsqlmod->updateurineInd($_POST['sc_num'], $updatedata);
					}
					break;
				default:
					# code...
					break;
			}

			

			$query=$this->getsqlmod->geturineInds($utest_id)->result();
			echo json_encode(array('data' => $query));
		}
		else
		{
			echo 'error';
		}
		
	}

	function getthirdempno(){
		if(isset($_GET['search']))
		{
			$query = $this->getsqlmod->getthirdempnobysearch($_GET['search'], $_GET['unselected'])->result_array(); 
		}
		else
		{
			$query = $this->getsqlmod->getthirdempno($_GET['unselected'])->result_array(); 
		}
		
		echo json_encode( array('results' => $query));
	}

    /** 轉西元年 */
    public function tranfer2ADyear($date)
    {
        if(substr($date, 0, 1) == 0 || substr($date, 0, 1) == '0')
        {
            $date = substr($date, 1, 6);            
        }
            
        if(strlen($date) == 6)
        {
            $ad = ((int)substr($date, 0, 2)) + 1911;
            return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
        }
        elseif(strlen($date) == 7)
        {
            
            $ad = ((int)substr($date, 0, 3)) + 1911;
            return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
        }
        else
        {
            return '';
        }

        
    }
    /** 轉民國年 */
    public function tranfer2RCyear($date)
    {
        $datestr = str_replace('-', '', $date);
        
        $rc = ((int)substr($datestr, 0, 4)) - 1911;
        return ($rc > 0)?(string)$rc . substr($datestr, 4, 4):'' ;
    }
	/** 轉民國年 中文年月日 */
    function tranfer2RCyearTrad($date)
    {
        $date = str_replace('-', '', $date);
        $rc = ((int)substr($date, 0, 4)) - 1911;
        return (string)$rc . '年' . substr($date, 4, 2) . '月' . substr($date, 6, 2) . '日';
    }
	/** 轉民國年 中文年-月-日 */
	function tranfer2RCyearTrad2($date)
	{
		if($date != '0000-00-00' && !empty($date) && isset($date))
		{
			$date = str_replace('-', '', $date);
			$rc = ((int)substr($date, 0, 4)) - 1911;
			return (string)$rc . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2) ;
		}
		else
		{
			return '';
		}
		
	}

    /**
     * 讀取郵遞區號
     */
    public function getZipCode()
    {
        
        $addr = explode(':', $this->processAddr($_POST['addr']));
        $array = json_decode($this->getsqlmod->getZipCode(), true);
        $addrkey = 0;
        
        // 縣市
        foreach ($array as $key => $value) {
            if($array[$key]['name'] == $addr[0])
            {
                $addrkey = $key;
                break;
            }
        }
        $data = 0;
        // 區鄉鎮
        foreach ($array[$addrkey]['districts'] as $k => $v) {
            if($array[$addrkey]['districts'][$k]['name'] == $addr[1])
            {
                $data = $array[$addrkey]['districts'][$k]['zip'];
                break;
            }
        }
        
        echo json_encode(array('data' => $data));
          
    }

	/**
     * 讀取 成份級數
     */
    public function getDrugLevel()
    {
        
        $druglv = $_POST['druglv'];
        $data = $this->getsqlmod->getDrugLevel($druglv)->result();
        
        echo json_encode(array('data' => $data));
          
    }
	

    /**
     * 地址切割
     */
    function processAddr($addr)
    {
        $addr1 = '';
        $addr2 = '';

        $a1 = '';
        $a2 = '';

        if(strpos($addr,'縣') !== false){ 
            $a1 = explode('縣', $addr)[1];
            $addr1 = explode('縣', $addr)[0] . '縣'; 
        }elseif(strpos($addr,'市') !== false){
            $a1 = explode('市', $addr)[1];
            $addr1 = explode('市', $addr)[0] . '市';  
        }

        if(strpos($a1, '鄉') !== false)
        {
            $a2 = explode('鄉', $a1)[1];
            $addr2 = explode('鄉', $a1)[0] . '鄉';
        }elseif (strpos($a1, '鎮') !== false) {
            $a2 = explode('鎮', $a1)[1];
            $addr2 = explode('鎮', $a1)[0] . '鎮';
        }elseif (strpos($a1, '市') !== false) {
            $a2 = explode('市', $a1)[1];
            $addr2 = explode('市', $a1)[0] . '市';
        }elseif (strpos($a1, '區') !== false){
            $a2 = explode('區', $a1)[1];
            $addr2 = explode('區', $a1)[0] . '區';
        }
        else
        {
            $a2 = $a1;
            $addr2 = '';
        }

        return str_replace('台','臺', $addr1) . ':' . $addr2;
    }

	public function getzipcode33($addr){
		if(isset($_GET['addr']))
			$addr = $_GET['addr'];
		
		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
		);
		$homepage = file_get_contents('https://www.yijingtw.com/taoyuan/daxi/system/api/zipcodeSoap?postaddr='.$addr, false, stream_context_create($arrContextOptions));
		echo json_decode($homepage, true)['data'];
		exit;

		$curl = curl_init();
		$send_url = "https://www.yijingtw.com/taoyuan/daxi/system/api/zipcodeSoap?";
		$param_post = array(
			"postaddr" => $addr
        );
		curl_setopt_array($curl, array(
            CURLOPT_URL => $send_url. http_build_query($param_post) ,
            CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CONNECTTIMEOUT => 30,
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
		));
        $output = curl_exec($curl);
        // $error = curl_error($curl);
        // echo $error;

        if($output === false){
			throw new Exception('Http request message :'.curl_error($curl));
		}

        curl_close($curl);

		return json_decode($output, true)['data'];
	}
}
