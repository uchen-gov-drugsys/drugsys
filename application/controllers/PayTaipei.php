    <?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class PayTaipei extends CI_Controller {//移送罰緩
	const FH_GICID = 81;
	const DJ_GICID = 82;
	const GCD_TYPE = 1; // 以識別碼查詢
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }
	function index(){
        $data['title'] = "PayTaipei文件下載";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'paytaipei/index';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
	public function downloadcsv()
	{
		date_default_timezone_set('Asia/Taipei');
		$this->load->helper('file');
		$data = $this->getsqlmod->getpaylist()->result_array();
                // var_dump($data);
				// exit;
		$target = "files/" ;
		header('Content-Type: application/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename="payTaipei'.date('Ymd').'.csv"');
		$output = fopen('php://output','w') or die("Can't open php://output");

		$csv_header = array('列管編號','受處分人姓名','身分證字號', '應繳納金額', '繳費期限','創建日期');
		fwrite($output, chr(0xEF).chr(0xBB).chr(0xBF));
		$csv_content = array();
		// $csv_content =chr(0xEF).chr(0xBB).chr(0xBF);//注意要加這三個字元
		// $csv_content .= join(',',$csv_header) ."\n";
		fputcsv($output, $csv_header);
		$cand_nums = 1;
		for ($i=0; $i < count($data); $i++) { 
			$needdpay = ($data[$i]['type'] == 'A')?(($data[$i]['f_amount']*10000)-$data[$i]['f_paymoney']*1):(($data[$i]['f_amount']*1)-$data[$i]['f_paymoney']*1);
			$row = array(
				$data[$i]['f_caseid'],
				$data[$i]['f_username'],
				$data[$i]['f_userid'],
				$needdpay,
				$data[$i]['f_findate'],
				date('Y-m-d H:i:s')
			);
			array_push($csv_content, $row);
			// 生成CSV內容
			// $csv_content .= $data[$i]['f_caseid']."," . $data[$i]['f_username']."," . $data[$i]['f_userid']."," . $needdpay."," . '="'.$data[$i]['f_findate'].'"'."\n";//注意數字字串保留首位0的處理
		}
		foreach($csv_content as $wr) {
			fputcsv($output, $wr);
		}
		fclose($output) or die("Can't close php://output");
		exit;
		// header("Content-type:text/csv;charset=gb2312");  //設定header                

		
	}
    public function getBill()
    {
		$data = file_get_contents("php://input");
        $postdata = json_decode($data);
        $gic_id = $_GET['gic_id']; //81:罰鍰  82:怠金
		var_dump($_GET);

		$response = array(
			'GID' => 'CID',
			'gic_id' => $gic_id,
			'items' => array(
				'custom_id' => '', //繳費編號
				'amt' => 0, // 金額
				'bill_end_time' => date('Y/m/d H:i:s'), // 繳費期限
				'field1_name' => '', // 帳單資訊名稱1
				'field1_value' => '' //帳單資訊1
			),
			'statusCode' => 0, // 交易結果代碼
			'timestamp' => time(), // 時間戳
		);
		array_push($response, array('checkCode' => self::processSignature($response)));
		$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				->_display();
        
        exit;
        
    }

	private static function statusCode($code){
		switch ($code) {
			case 0:
				return '交易正常處理完成';
				break;
			case -10:
				return '平台維護中';
				break;
			case -20:
				return '此API 版本平台不再支援';
				break;
			case -30:
				return '缺少參數';
				break;
			case -31:
				return '多餘的參數';
				break;
			case -32:
				return '參數值不合法';
				break;
			case -110:
				return '建立支付資訊失敗。';
				break;
			case -210:
				return '付款失敗。';
				break;
			case -420:
				return '退款失敗。';
				break;
			case -1060:
				return '資料檢查碼有誤。';
				break;
			case -3010:
				return '輸入參數有誤。';
				break;
			case -3030:
				return '查無資料。';
				break;
			case -3050:
				return 'accessToken 不存在。';
				break;
			case -4030:
				return '帳單無法繳費，請稍後再試。';
				break;
			case -4040:
				return '取得交易序號時發生異常。';
				break;
			case -9000:
				return '交易失敗。';
				break;
			case -9010:
				return '該交易須進行退款交易。';
				break;	
			default:
				return "平台異常";
				break;
		}
	}
	private static function processSignature($data){
        $HMAC_KEY = "IjOxGvLaKuBbDfHo";
        $search_str = array('%2F','%2B','%2D');
        $replace_str = array('/','+','-');
		$requestString = "";
		// 步驟一  requestString = 合併所有電文參數值（合併順序請參考各交易的詳細說明）
		foreach ($data as $key => $value) {
			if(is_array($value))
			{
				foreach ($value as $ak => $av) {
					$requestString .= $av;
				}
			}
			else
			{
				$requestString .= $value;
			}
			
		}
		// 步驟二  checkString = 在requestString 後端附加機關局處金鑰
		$checkString = $requestString . $HMAC_KEY;
		// 步驟三  SHA256Value = 對CheckString 進行SHA256 摘要算法得到摘要值
		$SHA256Value = hash('sha256', $checkString);

		// 步驟四  checkCode = 將SHA256Value 編碼字串轉爲小寫
		$checkCode = strtolower($SHA256Value);

        $pass1 = hash_hmac('sha256', str_replace($search_str,$replace_str, utf8_encode(http_build_query($data))) , $HMAC_KEY, true);
        $pass = base64_encode($pass1);
        $sendata = utf8_encode(http_build_query($data)) . '&signature=' . $pass;
		
        return $sendata;
		
    }
	

	/** 轉民國年 */
    public function tranfer2RCyear2($date)
    {
		if(empty($date))
		{
			return '<strong>無相關日期</strong>';
		}
		elseif($date == '0000-00-00')
		{
			return '<strong>無相關日期</strong>';
		}
		else{
			
			$datestr = explode('-', trim($date));
        
			$rc = ((int)$datestr[0]) - 1911;
			return (string)$rc . '-' . $datestr[1] . '-' .$datestr[2] ;
		}
        
    }
	
	/** 轉西元年 */
    public function tranfer2ADyear($date)
    {
        if(substr($date, 0, 1) == 0 || substr($date, 0, 1) == '0')
        {
            $date = substr($date, 1, 6);            
        }
            
        if(strlen($date) == 6)
        {
            $ad = ((int)substr($date, 0, 2)) + 1911;
            return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
        }
        elseif(strlen($date) == 7)
        {
            
            $ad = ((int)substr($date, 0, 3)) + 1911;
            return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
        }
        else
        {
            return '';
        }

        
    }
}
