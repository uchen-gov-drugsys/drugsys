<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class PDFcreate extends CI_Controller {

    public function __construct()
    {
        parent::__construct(); 
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
        $this->load->helper('url_helper');               
        $this->load->helper('date'); 
    }
    
    public function fine_doc()//產生處分書pdf
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $id = $this->uri->segment(3);
		$pjid = $this->uri->segment(4);
        $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        $susp=$susp[0];
        // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
		$fine=$this->getsqlmod->get3SP1($id)->result();
        $fine=$fine[0];
        $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
        $sp1=$this->getsqlmod->get3SP1($id)->result();
        $sp=$sp1[0];
        $spcount=$this->getsqlmod->getfdAll()->result();
        $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
        $cases=$cases[0];
        $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp->s_dp_project)->result();
        $susp_dp=$susp_dp[0];
        $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
		// $dp = $this->getsqlmod->getdpprojectname($pjid)->result()[0];
		$dp = $this->getsqlmod->getDP1bynum($pjid)->result()[0];
        ////$phone=$phone[0];

        // if(!isset($phone))$phone1=null;
        // if(isset($phone))$phone1=$phone[0]->p_no;
		$phone1=(isset($fine->fd_phone)?$fine->fd_phone:'');

        $susp1 = $this->getsqlmod->get2Susp($susp->s_num)->result(); // 抓snum
        // $susp1 = $susp1[0]; // 抓snum
        $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
        $drug=$drug[0];
        $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
        $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
        $today['day']   = date('d');
        $today['month'] = date('m');
        $today['year']  = date('Y') - 20;
        $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
            $fname = NULL;
            $fgender = NULL;
            $fic = NULL;
            $fbirth = NULL;
            $fphone = NULL;
            $fzipcode = NULL;
            $faddress = NULL;
        if(isset($suspfadai[0])){
            $suspfadai = $suspfadai[0];
            $stampBirth = strtotime($susp->s_birth);
            if ($stampBirth > $stampToday) {
                $fname =  $suspfadai->s_fadai_name;
                $fgender = $suspfadai->s_fadai_gender;
                $fic = $suspfadai->s_fadai_ic;
                $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                $fphone = $suspfadai->s_fadai_phone;
                $fzipcode = $suspfadai->s_fadai_zipcode;
                $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
            }else{
                $fname = NULL;
                $fgender = NULL;
                $fic = NULL;
                $fbirth = NULL;
                $fphone = NULL;
                $fzipcode = NULL;
                $faddress = NULL;
            }
        }
        $drug2msg = Null;
        foreach ($drug2 as $drug3){
            if(!isset($drug3->ddc_level)){}
            else{
                $drug2msg = $drug2msg . '第'.$drug3->ddc_level .'級' . '『'.$drug3->ddc_ingredient.'』 ';
            }
        }  
        $drug2=((isset($drug2[0]))?$drug2[0]:null);
        $prison=$this->getsqlmod->getprison()->result();
        $prison=$prison[0];

        $has_f_damount = ($fine->fd_amount > 0)?true:false; // 是否有罰鍰
		$has_course = (isset($sp->fd_lec_date) && !empty($sp->fd_lec_date))?true:false;  // 是否有講習
		$has_merge = (isset($sp->fd_merge) && !empty($sp->fd_merge))?true:false;  //是否有單沒入
		$has_ary = array($has_f_damount, $has_course, $has_merge);
		$count_has_ch = array('一、', '二、', '三、');

		$target_str = '';
		$send_str = '';
		
        if(null != $sp->fd_address && count(explode(",", $sp->fd_address)) > 1)
        {
            $fd_name = explode(",", $sp->fd_target)[0];
            $fd_addr = explode(",", $sp->fd_address)[0];
            $fd_zip = explode(",", $sp->fd_zipcode)[0];
            // $target_str = $fd_name.' 君 <br>                '. $fd_zip . ' ' . $fd_addr;
            // $target_str .= '<br/>            ';
            $fd_name1 = explode(",", $sp->fd_target)[1];
            $fd_addr1 = explode(",", $sp->fd_address)[1];
            $fd_zip1 = $susp->s_dpzipcode;
            $target_str .=  $fd_name.'('.$fd_name1.'法定代理人) 君 <br>            '. $fd_zip1 . ' ' . $fd_addr1;
            if($susp->s_live_state == 2)
            {
                $target_str .= '（指定送達現住地）';
            }
        }
        else
        {
            $target_str = $sp->fd_target.' 君 <br>            '. $sp->fd_zipcode . ' ' . $sp->fd_address ;
            if($susp->s_live_state == 2)
            {
                $target_str .= '（指定送達現住地）';
            }
        }
		
		if($sp->fd_date != '0000-00-00' && isset($sp->fd_date))
		{
			$send_str = '發文日期字號：'.(date('Y')-1911).'年'. date('m', strtotime($sp->fd_date))*1 .'月' . date('d', strtotime($sp->fd_date))*1  .'日北市警刑毒緝字第' . $sp->fd_send_num.((isset($susp->s_prison) && !empty($susp->s_prison) && $susp->s_live_state == 3)?'1':'') .'號';
		}
		else
		{
			if($dp->dp_send_date !=  '0000-00-00' && isset($dp->dp_send_date))
			{
				$send_str = '發文日期字號：'.((int)substr($dp->dp_send_date, 0, 4)- 1911) .'年'. (int)substr($dp->dp_send_date, 5, 2)*1 . '月' . (int)substr($dp->dp_send_date, 8, 2)*1 .'日北市警刑毒緝字第' . $sp->fd_send_num .((isset($susp->s_prison) && !empty($susp->s_prison) && $susp->s_live_state == 3)?'1':'') .'號';
			}else{
				$send_str = '發文日期字號：<strong>民國'.(date('Y')-1911).'年'.date('m')*1 .'月  日</strong>北市警刑毒緝字第' . $sp->fd_send_num . ((isset($susp->s_prison) && !empty($susp->s_prison) && $susp->s_live_state == 3)?'1':'') . '號';
			}
			
		}
		
		if(mb_strlen($susp->s_roffice1) < 10)
			$s_roffice = '我是測試系統'.$susp->s_roffice;
		else
			$s_roffice = $susp->s_roffice;

        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            $pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(10, 1.5, 70.5,true);
            // set some language-dependent strings (optional)
            $pdf->setFontSubsetting(true);
            $pdf->SetFont('twkai98_1','',10);
            // $pdf->SetFont('msungstdlight', '', 10);
            $pdf->AddPage();

            

            $html='';
            $html='
                                <div style="width:680px" class="panel-body">
									<table width="680" cellspacing="0" cellpadding="0" border="0">
											<tr><td  colspan="3"><h2>列管編號：'.$sp->fd_num .'</h2> </td><td colspan="5">郵寄 以稿代簽 限制開放 第二層決行 檔號：'.(date('Y')-1911).'/07270399 保存年限：3年</td></tr>
												<tr>
												<td colspan="2"> 校對：</td>
												<td colspan="2"> 監印：</td>
												<td colspan="2">發文：</td>
												</tr>
												<tr><td colspan="8"><br/> </td></tr>
												<tr><td colspan="8">正本：'.(($has_f_damount == false && $has_course == false && $has_merge == true)?$s_roffice:$target_str).'</td></tr>
												<tr><td colspan="8">副本：'.(($has_f_damount == false && $has_course == false && $has_merge == true)?'我是測試系統刑事警察大隊':$s_roffice.'、我是測試系統刑事警察大隊、臺北市政府毒品危害防制中心').'</td></tr>
									</table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">											
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統 違反毒品危害防制條例案件處分書(稿)</h3></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">'.$send_str.'</td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">依據：'.((isset($sp->fd_acrrod_unit))?$sp->fd_acrrod_unit:$s_roffice) .((isset($sp->fd_acrrod_date))?((date('Y', strtotime($sp->fd_acrrod_date))-1911) .'年'.date('m', strtotime($sp->fd_acrrod_date))*1 .'月' . date('d', strtotime($sp->fd_acrrod_date))*1 .'日'):((isset($susp->s_fdate))?((strlen($susp->s_fdate) > 7 && $susp->s_fdate != '0000-00-00')?(date('Y', strtotime($susp->s_fdate))-1911) .'年'.date('m', strtotime($susp->s_fdate))*1 .'月' . date('d', strtotime($susp->s_fdate))*1 .'日':''):'')) . ((isset($sp->fd_accrod_no))?$sp->fd_accrod_no:((mb_strlen($susp->s_roffice1) < 4)?'北市警'.$susp->s_roffice1.((strpos($susp->s_roffice, '分局') !== false)?'分刑':'') :$susp->s_roffice1)).'字第'. ((isset($sp->fd_accrod_num))?$sp->fd_accrod_num:$susp->s_fno) .'號</td>
                                            </tr>
                                            <tr>
												<td rowspan="4" width="77" height="70" style="line-height:70px;">
													<h3 align="center" >受處分人</h3>
												</td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
												<td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
                                                <td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.((isset($susp->s_prison) && !empty($susp->s_prison))?explode(':', $susp->s_prison)[1] .'('.explode(':', $susp->s_prison)[0].')' :$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress) . (($susp->s_live_state == 2)?'（指定送達現住地）':'') .'</strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>';
            // list($year,$month,$day) = explode("-",$susp->s_birth);
            // $year_diff = date("Y") - $year;
            // $month_diff = date("m") - $month;
            // $day_diff  = date("d") - $day;
            // if ($day_diff < 0 || $month_diff < 0)
            //     $year_diff--;                                
            $html .=                            '<td rowspan="4" width="77" height="70" style="line-height:20px;"><h3 align="center"><br>法定 代理人</h3></td>
												<td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_name:'').'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_gender:'').'</strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?'民國'.(date('Y', strtotime($suspfadai->s_fadai_birth))-1911) .'年'.(date('m', strtotime($suspfadai->s_fadai_birth))) .'月' .(date('d', strtotime($suspfadai->s_fadai_birth))) .'日':'') .'</strong></td>
                                            </tr>
                                            <tr>
												<td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_ic:'').'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_phone:'').'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
												<td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="445" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address:'').'</strong></td>
                                            </tr>
                                            <tr>
												<td width="77" height="70" style="line-height:70px;"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603"  >';
												$ch_j = 0;
												$has_ch = array();
												for ($i=0; $i < count($has_ary); $i++) { 
													if($has_ary[$i])
														array_push($has_ch, $count_has_ch[$ch_j++]);
													else
														array_push($has_ch, '');
												}
												if($ch_j > 1)
													$html .= '受處分人處罰：<br/>';

												if($has_ary[0]){
													$html .=(($ch_j > 1)?$has_ch[0]:'受處分人處罰：') . '新臺幣' . ($fine->fd_amount/10000) . '萬元整。'.((isset($sp->fd_times) && $sp->fd_times > 1)?'( '.(date('Y')-1911).'年第'.$sp->fd_times.'次裁罰 )':'').'<br/>';
												}

												if($has_ary[1]){
													$html .=(($ch_j > 1)?$has_ch[1]:'受處分人處罰：') . '毒品危害6小時。(講習時間詳見下方「毒品講習」欄位)<br/>';
												}

												if($has_ary[2] ){
													$patterns = array('1級','2級','3級','4級');
													$replacements = array('一級','二級','三級','四級');

													$html .=(($ch_j > 1)?$has_ch[2]:'受處分人處罰：')  . str_replace($patterns, $replacements, $sp->fd_drug_msg) . '沒入。';
												}
												
												// 受處分人處罰：<br>
                                                //         一、新臺幣<strong>'.($fine->f_damount/10000).'</strong>萬元整<br>
                                                //         二、毒品危害 <strong>6</strong>小時。(講習時間詳見下方「毒品講習」欄位)<br>
                                                //         三、<strong> '.$sp->fd_drug_msg/*$drug2msg.$drug->e_count.$drug->e_type*/.'</strong>沒入。
			$html .=                            '</td>
                                            </tr>
                                            <tr>
												<td width="77" height="75" style="line-height:75px;"><h3 align="center">事實</h3></td>';
            $s_date = $this->tranfer2RCyear($cases->s_date);
            $s_time = $this->tranfer2RChour($cases->s_time);   
            $courser_str = '';
            $fd_fact_text = '';
            if(isset($susp->s_prison) && !empty($susp->s_prison))
            {
                $courser_str = '請受處分人出所後，主動聯繫臺北市政府毒品危害防制中心。';
            }
            else
            {
                if(isset($sp->fd_lec_date) && !empty($sp->fd_lec_date) && $sp->fd_other_lec == null){
                    $md_str = explode("(", $sp->fd_lec_date)[0];
                    $week_str = explode("(", $sp->fd_lec_date)[1];

                    $courser_str = "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日（". $week_str . '，時間：' . explode('-', $sp->fd_lec_time)[0] . "（請攜帶有相片之證件報到，逾時將無法入場），地點：".preg_replace('/\s(?=)/', '', $sp->fd_lec_place) ."（" . $sp->fd_lec_address . "）";
                }
                else
                {
                    $courser_str = '由臺北市政府毒品危害防制中心另行通知。';
                }
                
            }
            if(!isset($sp->fd_fact_text)||($sp->fd_fact_text==null)||($sp->fd_fact_text=='')){
                $fd_fact_text = $susp->s_name. $s_date . $s_time.'許，在本市'.$cases->s_place.'，為'.$cases->s_office.'員警查獲持有'.$sp->fd_drug_msg.'，經採集毒品及尿液檢體送專業單位鑑驗，均呈第'.((isset($drug2))?$drug2->ddc_level:'').'級毒品「'.((isset($drug2))?$drug2->ddc_ingredient:'').'」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg;
            }
            else
            {
                $fd_fact_text = $sp->fd_fact_text;
            }

			$reason_1= '';
			$reason_2= '';
            if($has_ary[0] || $has_ary[1])$reason_1 = '■'; else $reason_1 = '□';
			if($has_ary[2])$reason_2 = '■'; else $reason_2 = '□';
            $html .=                            '<td colspan="7" width="603">'.$fd_fact_text .'</td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:15px;"><h3 align="center"><br>理由及 法令依據</h3></td>
                                                <td colspan="7" width="603">'.$reason_1.'依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                        <br>'.$reason_2.'依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                </td>
                                            </tr>';
	if($has_ary[0]) {
	$html .=                                '<tr>
												<td width="77" height="100" style="line-height:15px;"><h3 align="center"><br><br>繳納期限 及方式</h3></td>
                                                <td colspan="7" width="603">一、罰鍰限於<strong>民國'.(($fine->fd_limitdate != '0000-00-00')?(date('Y', strtotime($fine->fd_limitdate))-1911) .'年'.(date('m', strtotime($fine->fd_limitdate))) .'月'.(date('d', strtotime($fine->fd_limitdate))):'Ｏ年Ｏ月Ｏ') .'日</strong>前選擇下列方式之一繳款：
                                                            （一）以自動化設備（ATM、網路ATM、網路銀行）匯款至「虛擬帳號」（限本處分書）。
                                                            （二）至金融機構臨櫃繳款至「臨櫃帳戶」。<br>二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong>
                                                            （三）輸入本案虛擬帳號：<strong>'.$fine->fd_bvc.'</strong>
                                                                三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<span style="text-decoration: underline;">備註(附言)欄位</span>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。
                                                </td>
                                            </tr>';
	}
	if($has_ary[1]) {
	$html .=                                '<tr>
												<td width="77" height="80" style="line-height:80px;"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">一、可選擇參與實體講習或線上課程（每年可參與4次，惟應以申請不同套裝課程）。<br>二、講習時間、地點： '.$courser_str .'如要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。<br>三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。                                                </td>
                                            </tr>';
	}
	$html .=                                '<tr>
												<td width="77" height="50" style="line-height:50px;"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。 <br>二、承辦人：'. $dp->dp_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <p>
                                        承辦單位 (2539) 核稿 決行
                                    </p>            
                                </div>';

            $pdf->writeHTML($html, true, false, false, false, '');
                        
        // $pdf->Output($sp->fd_num .'.pdf', 'D');    
        $pdf->Output($sp->fd_num .'.pdf', 'I');    
    }
    
    public function fine_doc_project()//產生處分書pdf
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
		$this->load->library('zip');
        $project = $this->uri->segment(3);
        // $query = $this->getsqlmod->getdpprojectlist($project); 
		// $dp = $this->getsqlmod->getdpprojectname($project)->result()[0];
		$dp = $this->getsqlmod->getDP1bynum($project)->result()[0];
		$query = $this->getsqlmod->getdpprojectlist($dp->dp_name);
        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            // $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            // $pdf->SetCreator(PDF_CREATOR);
            // $pdf->setPrintHeader(false); //不要頁首
            // //$pdf->setPrintFooter(false); //不要頁尾
            // // 版面配置 > 邊界
            // //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            // $pdf->SetMargins(5.5, 3.5, 10,true);
            // // set some language-dependent strings (optional)
            // // set default header data
            // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            // $pdf->setFooterData(array(0,64,0), array(0,64,128));
            // $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // $pdf->SetAutoPageBreak(FALSE, 0);
            // $pdf->SetFont('twkai98_1','',10);
            $html='';       
            foreach ($query->result() as $suspect)
            {
				$pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->setPrintHeader(false); //不要頁首
				$pdf->setPrintFooter(false); //不要頁尾
				// 版面配置 > 邊界
				//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				$pdf->SetMargins(10, 1.5, 70.5,true);
				// set some language-dependent strings (optional)
				$pdf->setFontSubsetting(true);
				$pdf->SetFont('twkai98_1','',10);
				// $pdf->SetFont('msungstdlight', '', 10);
				$pdf->AddPage();

                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
				$fine=$this->getsqlmod->get3SP1($id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp->s_dp_project)->result();
                $susp_dp=$susp_dp[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();

                // if(isset($phone[0]))//$phone=$phone[0];
                // if(!isset($phone))$phone1=null;
                // if(isset($phone))$phone1=$phone[0]->p_no;
				$phone1=(isset($fine->fd_phone)?$fine->fd_phone:'');

                $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 20;
                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                    $fname = NULL;
                    $fgender = NULL;
                    $fic = NULL;
                    $fbirth = NULL;
                    $fphone = NULL;
                    $fzipcode = NULL;
                    $faddress = NULL;
                if(isset($suspfadai[0])){
                    $suspfadai = $suspfadai[0];
                    $stampBirth = strtotime($susp->s_birth);
                    if ($stampBirth > $stampToday) {
                        $fname =  $suspfadai->s_fadai_name;
                        $fgender = $suspfadai->s_fadai_gender;
                        $fic = $suspfadai->s_fadai_ic;
                        $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                        $fphone = $suspfadai->s_fadai_phone;
                        $fzipcode = $suspfadai->s_fadai_zipcode;
                        $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                    }else{
                        $fname = NULL;
                        $fgender = NULL;
                        $fic = NULL;
                        $fbirth = NULL;
                        $fphone = NULL;
                        $fzipcode = NULL;
                        $faddress = NULL;
                    }
                }
                $susp1 = $this->getsqlmod->get2Susp($susp->s_num)->result(); // 抓snum
                $susp1 =  (isset($susp1[0]))?$susp1[0]:$susp1; // 抓snum
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2msg = Null;
                foreach ($drug2 as $drug3){
                    if(!isset($drug3->ddc_level)){}
                    else{
                        $drug2msg = $drug2msg . '第'.$drug3->ddc_level .'級' . '『'.$drug3->ddc_ingredient.'』 ';
                    }
                }  
                $drug2=((isset($drug2[0]))?$drug2[0]:null);
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];

				$has_f_damount = ($fine->fd_amount > 0)?true:false; // 是否有罰鍰
				$has_course = (isset($sp->fd_lec_date) && !empty($sp->fd_lec_date))?true:false;  // 是否有講習
				$has_merge = (isset($sp->fd_merge) && !empty($sp->fd_merge))?true:false;  //是否有單沒入
				$has_ary = array($has_f_damount, $has_course, $has_merge);
				$count_has_ch = array('一、', '二、', '三、');

                $target_str = '';
                if(null != $sp->fd_address && count(explode(",", $sp->fd_address)) > 1)
                {
                    $fd_name = explode(",", $sp->fd_target)[0];
                    $fd_addr = explode(",", $sp->fd_address)[0];
                    $fd_zip = explode(",", $sp->fd_zipcode)[0];
                    // $target_str = $fd_name.' 君 <br>            '. $fd_zip . ' ' . $fd_addr;
                    // $target_str .= '<br/>            ';
                    $fd_name1 = explode(",", $sp->fd_target)[1];
                    $fd_addr1 = explode(",", $sp->fd_address)[1];
                    $fd_zip1 = $susp->s_dpzipcode;
                    $target_str .=  $fd_name.'('.$fd_name1.'法定代理人) 君 <br>            '. $fd_zip1 . ' ' . $fd_addr1;
                    if($susp->s_live_state == 2)
                    {
                        $target_str .= '（指定送達現住地）';
                    }
                }
                else
                {
                    $target_str = $sp->fd_target.' 君 <br>            '. $sp->fd_zipcode . ' ' . $sp->fd_address ;
                    if($susp->s_live_state == 2)
                    {
                        $target_str .= '（指定送達現住地）';
                    }
                }

				if($sp->fd_date != '0000-00-00' && isset($sp->fd_date))
				{
					$send_str = '發文日期字號：'.(date('Y')-1911).'年'. date('m', strtotime($sp->fd_date))*1 .'月' . date('d', strtotime($sp->fd_date))*1  .'日北市警刑毒緝字第' . $sp->fd_send_num .'號';
				}
				else
				{
					if($dp->dp_send_date !=  '0000-00-00' && isset($dp->dp_send_date))
					{
						$send_str = '發文日期字號：'.((int)substr($dp->dp_send_date, 0, 4)- 1911) .'年'. (int)substr($dp->dp_send_date, 5, 2)*1 . '月' . (int)substr($dp->dp_send_date, 8, 2)*1 .'日北市警刑毒緝字第' . $sp->fd_send_num .'號';
					}else{
						$send_str = '發文日期字號：<strong>民國'.(date('Y')-1911).'年'.date('m')*1 .'月  日</strong>北市警刑毒緝字第' . $sp->fd_send_num . '號';
					}
					
				}
				if(mb_strlen($susp->s_roffice1) < 10)
					$s_roffice = '我是測試系統'.$susp->s_roffice;
				else
					$s_roffice = $susp->s_roffice;

                $html ='
                                <div style="width:680px" class="panel-body">
                                    <table width="680">
                                        <tr><td colspan="3"><h2>列管編號：'.$sp->fd_num .'</h2> </td><td  colspan="5">郵寄 以稿代簽 限制開放 第二層決行 '.(date('Y')-1911).'/07270399 保存年限：3年</td></tr>
                                        <tr>
                                        <td colspan="2"> 校對：</td>
                                        <td colspan="2"> 監印：</td>
                                        <td colspan="2">發文：</td>
                                        </tr>
                                        <tr><td colspan="8"><br/> </td></tr>
                                        <tr><td colspan="8">正本：'.(($has_f_damount == false && $has_course == false && $has_merge == true)?$s_roffice:$target_str).'</td></tr>
										<tr><td colspan="8">副本：'.(($has_f_damount == false && $has_course == false && $has_merge == true)?'我是測試系統刑事警察大隊':$s_roffice.'、我是測試系統刑事警察大隊、臺北市政府毒品危害防制中心').'</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統 違反毒品危害防制條例案件處分書(稿)</h3></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">'.$send_str.'</td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">依據：'.((isset($sp->fd_acrrod_unit))?$sp->fd_acrrod_unit:$s_roffice) .((isset($sp->fd_acrrod_date))?((date('Y', strtotime($sp->fd_acrrod_date))-1911) .'年'.date('m', strtotime($sp->fd_acrrod_date))*1 .'月' . date('d', strtotime($sp->fd_acrrod_date))*1 .'日'):((isset($susp->s_fdate))?((strlen($susp->s_fdate) > 7 && $susp->s_fdate != '0000-00-00')?(date('Y', strtotime($susp->s_fdate))-1911) .'年'.date('m', strtotime($susp->s_fdate))*1 .'月' . date('d', strtotime($susp->s_fdate))*1 .'日':''):'')) . ((isset($sp->fd_accrod_no))?$sp->fd_accrod_no:((mb_strlen($susp->s_roffice1) < 4)?'北市警'.$susp->s_roffice1.((strpos($susp->s_roffice, '分局') !== false)?'分刑':'') :$susp->s_roffice1)).'字第'. ((isset($sp->fd_accrod_num))?$sp->fd_accrod_num:$susp->s_fno) .'
                                                號</td>
                                            </tr>
                                            <tr>
												<td rowspan="4" width="77" height="70" style="line-height:70px;">
													<h3 align="center" >受處分人</h3>
												</td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
												<td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
                                                <td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.((isset($susp->s_prison) && !empty($susp->s_prison))?explode(':', $susp->s_prison)[1] .'('.explode(':', $susp->s_prison)[0].')' :$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress) . (($susp->s_live_state == 2)?'（指定送達現住地）':'')  .'</strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>';
                    // list($year,$month,$day) = explode("-",$susp->s_birth);
                    // $year_diff = date("Y") - $year;
                    // $month_diff = date("m") - $month;
                    // $day_diff  = date("d") - $day;
                    // if ($day_diff < 0 || $month_diff < 0)
                    //     $year_diff--; 
                    $html .=                    '<td rowspan="4" width="77" height="70" style="line-height:20px;"><h3 align="center"><br>法定 代理人</h3></td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_name:'').'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_gender:'').'</strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?'民國'.(date('Y', strtotime($suspfadai->s_fadai_birth))-1911) .'年'.(date('m', strtotime($suspfadai->s_fadai_birth))) .'月' .(date('d', strtotime($suspfadai->s_fadai_birth))) .'日':'') .'</strong></td>
                                            </tr>
                                            <tr>
												<td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_ic:'').'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_phone:'').'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
												<td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="445" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address:'').'</strong></td>
                                            </tr>
                                            <tr>
											<td width="77" height="70" style="line-height:70px;"><h3 align="center">主旨</h3></td>
											<td colspan="7" width="603" >';
											$ch_j = 0;
											$has_ch = array();
											for ($i=0; $i < count($has_ary); $i++) { 
												if($has_ary[$i])
													array_push($has_ch, $count_has_ch[$ch_j++]);
												else
													array_push($has_ch, '');
											}
											if($ch_j > 1)
												$html .= '受處分人處罰：<br/>';

											if($has_ary[0]){
												$html .=(($ch_j > 1)?$has_ch[0]:'受處分人處罰：') . '新臺幣' . ($fine->fd_amount/10000) . '萬元整。'.((isset($sp->fd_times) && $sp->fd_times > 1)?'( '.(date('Y')-1911).'年第'.$sp->fd_times.'次裁罰 )':'').'<br/>';
											}

											if($has_ary[1]){
												$html .=(($ch_j > 1)?$has_ch[1]:'受處分人處罰：') . '毒品危害6小時。(講習時間詳見下方「毒品講習」欄位)<br/>';
											}

											if($has_ary[2]){
												$patterns = array('1級','2級','3級','4級');
												$replacements = array('一級','二級','三級','四級');
												$html .=(($ch_j > 1)?$has_ch[2]:'受處分人處罰：')  . str_replace($patterns, $replacements, $sp->fd_drug_msg) . '沒入。';
											}
											
											// 受處分人處罰：<br>
											//         一、新臺幣<strong>'.($fine->f_damount/10000).'</strong>萬元整<br>
											//         二、毒品危害 <strong>6</strong>小時。(講習時間詳見下方「毒品講習」欄位)<br>
											//         三、<strong> '.$sp->fd_drug_msg/*$drug2msg.$drug->e_count.$drug->e_type*/.'</strong>沒入。
		$html .=                            '</td>
                                            </tr>
                                            <tr>
												<td width="77" height="75" style="line-height:75px;"><h3 align="center">事實</h3></td>';
                        $s_date = $this->tranfer2RCyear($cases->s_date);
                        $s_time = $this->tranfer2RChour($cases->s_time);   
                        $courser_str = '';
                        $fd_fact_text = '';
                        if(isset($susp->s_prison) && !empty($susp->s_prison))
                        {
                            $courser_str = '請受處分人出所後，主動聯繫臺北市政府毒品危害防制中心。';
                        }
                        else
                        {
                            if(isset($sp->fd_lec_date) && !empty($sp->fd_lec_date) && $sp->fd_other_lec == null){
                                $md_str = explode("(", $sp->fd_lec_date)[0];
                                $week_str = explode("(", $sp->fd_lec_date)[1];
            
                                $courser_str = "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日（". $week_str . '，時間：' . explode('-', $sp->fd_lec_time)[0] . "（請攜帶有相片之證件報到，逾時將無法入場），地點：".preg_replace('/\s(?=)/', '', $sp->fd_lec_place) ."（" . $sp->fd_lec_address . "）";
                            }
                            else
                            {
                                $courser_str = '由臺北市政府毒品危害防制中心另行通知。';
                            }
                        }
                        if(!isset($sp->fd_fact_text)||($sp->fd_fact_text==null)||($sp->fd_fact_text=='')){
                            $fd_fact_text = $susp->s_name. $s_date . $s_time.'許，在本市'.$cases->s_place.'，為'.$cases->s_office.'員警查獲持有'.$sp->fd_drug_msg.'，經採集毒品及尿液檢體送專業單位鑑驗，均呈第'.((isset($drug2))?$drug2->ddc_level:'').'級毒品「'.((isset($drug2))?$drug2->ddc_ingredient:'').'」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg;
                        }
                        else
                        {
                            $fd_fact_text = $sp->fd_fact_text;
                        }
						$reason_1= '';
						$reason_2= '';
						if($has_ary[0] || $has_ary[1])$reason_1 = '■'; else $reason_1 = '□';
						if($has_ary[2])$reason_2 = '■'; else $reason_2 = '□';
                        $html .=             '<td colspan="7" width="603" >受處分人'.$fd_fact_text .'</td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:15px;"><h3 align="center"><br>理由及 法令依據</h3></td>
                                                <td colspan="7" width="603">'.$reason_1.'依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                        <br>'.$reason_2.'依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                </td>
												</tr>';
		if($has_ary[0]) {
		$html .=                                '<tr>
												<td width="77" height="100" style="line-height:15px;"><h3 align="center"><br><br>繳納期限 及方式</h3></td>
                                                <td colspan="7" width="603">一、罰鍰限於<strong>民國'.(($fine->fd_limitdate != '0000-00-00')?(date('Y', strtotime($fine->fd_limitdate))-1911) .'年'.(date('m', strtotime($fine->fd_limitdate))) .'月'.(date('d', strtotime($fine->fd_limitdate))):'Ｏ年Ｏ月Ｏ') .'日</strong>前選擇下列方式之一繳款：
                                                        （一）以自動化設備（ATM、網路ATM、網路銀行）匯款至「虛擬帳號」（限本處分書）。
                                                        （二）至金融機構臨櫃繳款至「臨櫃帳戶」。<br>二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong>
                                                        （三）輸入本案虛擬帳號：<strong>'.$fine->fd_bvc.'</strong>
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。
                                                </td>
												</tr>';
		}
		if($has_ary[1]) {
		$html .=                                '<tr>
												<td width="77" height="80" style="line-height:80px;"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">一、可選擇參與實體講習或線上課程（每年可參與4次，惟應以申請不同套裝課程）。<br>二、講習時間、地點： '.$courser_str.' 如要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。<br>三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。</td>
												</tr>';
		}
		$html .=                                '<tr>
												<td width="77" height="50" style="line-height:50px;"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。 <br>二、承辦人：'.$dp->dp_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table>
                                    <tr>
                                    <td>承辦單位(2539)</td>   
                                    <td>核稿</td>   
                                    <td>決行</td>   
                                    </tr>            
                                    </table> 
                                    <table>
                                    <tr>
                                    <td height="210"></td>   
                                    </tr>            
                                    </table> 
                                </div>';
								$pdf->writeHTML($html, true, false, true, false, '');    
								// $pdf->Output($project.'.pdf', 'I');    //read  
								$pdf->lastPage();
								// $pdf->Output(getcwd().'\\drugdoc\\'.$sp->fd_send_num . '.pdf', 'F');
								

								// $path = getcwd().'\\drugdoc\\'.$sp->fd_send_num . '.pdf';
								// $this->zip->read_file($path);
								$pdf->Output(dirname(__FILE__) .'\\'.$sp->fd_send_num . '.pdf', 'F');
								$files = dirname(__FILE__) .'\\'.$sp->fd_send_num . '.pdf';
								// $this->zip->add_data($sp->fd_send_num . '.pdf', $files);
								$this->zip->read_file($files);
								$pdf->Close();
								unlink(dirname(__FILE__) .'\\'.$sp->fd_send_num . '.pdf');
            }
			$j = 0;
			foreach ($query->result() as $suspect)
			{
				$id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
				$fine=$this->getsqlmod->get3SP1($id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp->s_dp_project)->result();
                $susp_dp=$susp_dp[0];
				$dp = $this->getsqlmod->getdpprojectname($susp->s_dp_project)->result()[0];

				if(mb_strlen($susp->s_roffice1) < 10)
					$s_roffice = '我是測試系統'.$susp->s_roffice;
				else
					$s_roffice = $susp->s_roffice;
				
				$dom = new DOMDocument("1.0","utf-8"); 
				$implementation = new DOMImplementation();
				$dom->appendChild($implementation->createDocumentType('簽 SYSTEM "104_5_utf8.dtd" [<!ENTITY 表單 SYSTEM "簽.sw" NDATA DI><!NOTATION DI SYSTEM ""><!NOTATION _X SYSTEM "">]')); 
				// display document in browser as plain text 
				// for readability purposes 
				header("Content-Type: text/plain"); 
				// create root element 
				$root = $dom->createElement("簽"); 
				$dom->appendChild($root); 
				// create child element 
				 
				$item = $dom->createElement("檔號"); 
				$root->appendChild($item); 
				
				$item2 = $dom->createElement("年度"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode(date('Y')-1911); 
				$item2->appendChild($text); 
				$item2 = $dom->createElement("分類號"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("07270399");
				// $text = $dom->createTextNode(""); 
				$item2->appendChild($text); 
				$item2 = $dom->createElement("案次號"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("1"); 
				$item2->appendChild($text); 
				 
				$item = $dom->createElement("保存年限"); 
				$root->appendChild($item); 
				$text = $dom->createTextNode("3"); 
				$item->appendChild($text); 
			
				$item = $dom->createElement("稿面註記"); 
				$root->appendChild($item); 
				$item2 = $dom->createElement("擬辦方式"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("以稿代簽"); 
				$item2->appendChild($text); 
				$item2 = $dom->createElement("決行層級"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("二層決行"); 
				$item2->appendChild($text); 
				$item2 = $dom->createElement("應用限制"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("限制開放"); 
				$item2->appendChild($text); 
				
			
				$item = $dom->createElement("於"); 
				$root->appendChild($item);
				$text = $dom->createTextNode(""); 
				$item->appendChild($text); 

				$item = $dom->createElement("年月日"); 
				$root->appendChild($item); 
				$text = $dom->createTextNode($this->tranfer2RCyearTrad($dp->dp_send_date)); 
				$item->appendChild($text); 

				$item = $dom->createElement("機關"); 
				$root->appendChild($item);
				$item2 = $dom->createElement("全銜"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("刑事警察大隊(毒品查緝中心)"); 
				$item2->appendChild($text); 
				
				
				$item = $dom->createElement("主旨"); 
				$root->appendChild($item); 
				$item2 = $dom->createElement("文字"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("檢陳".$susp->s_name."違反毒品危害防制條例案，行政裁罰處分案。"); 
				$item2->appendChild($text); 
				// $text = $dom->createTextNode($susp->s_roffice.(date('Y', strtotime($susp->s_fdate))-1911) .'年' . date('m', strtotime($susp->s_fdate))*1 .'月' . date('d', strtotime($susp->s_fdate))*1 . '日'.$sp->fd_send_num."所報".$susp->s_name."(證號:".$susp->s_ic.")違反毒品危害防制條例行政裁罰案"); 
				// $item2->appendChild($text); 
				
				// if(!isset($sp->fd_fact_text)||($sp->fd_fact_text==null)||($sp->fd_fact_text=='')){
				// 	$fd_fact_text = '受處分人'.$susp->s_name. $s_date . $s_time.'許，在本市'.$cases->s_place.'，為'.$cases->s_office.'員警查獲持有'.$sp->fd_drug_msg.'，經採集毒品及尿液檢體送專業單位鑑驗，均呈第'.((isset($drug2))?$drug2->ddc_level:'').'級毒品「'.((isset($drug2))?$drug2->ddc_ingredient:'').'」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg;
				// }
				// else
				// {
				// 	$fd_fact_text = $sp->fd_fact_text;
				// }
				
				$item = $dom->createElement("段落"); 
				$item->setAttribute("段名", "說明：");  
				$root->appendChild($item); 
				$item2 = $dom->createElement("文字"); 
				$item->appendChild($item2);

				$item3 = $dom->createElement("條列");
				$item3->setAttribute("序號", "一、");
				$item->appendChild($item3); 
				$item2 = $dom->createElement("文字"); 
				$item3->appendChild($item2);
				// $text = $dom->createTextNode($fd_fact_text); 
				$text = $dom->createTextNode("依據".((isset($sp->fd_acrrod_date))?((date('Y', strtotime($sp->fd_acrrod_date))-1911) .'年'.date('m', strtotime($sp->fd_acrrod_date))*1 .'月' . date('d', strtotime($sp->fd_acrrod_date))*1 .'日'):((isset($susp->s_fdate))?((strlen($susp->s_fdate) > 7 && $susp->s_fdate != '0000-00-00')?(date('Y', strtotime($susp->s_fdate))-1911) .'年'.date('m', strtotime($susp->s_fdate))*1 .'月' . date('d', strtotime($susp->s_fdate))*1 .'日':''):'')). ((isset($sp->fd_accrod_no))?$sp->fd_accrod_no:((mb_strlen($susp->s_roffice1) < 4)?'北市警'.$susp->s_roffice1.((strpos($susp->s_roffice, '分局') !== false)?'分刑':'') :$susp->s_roffice1)).'字第'. ((isset($sp->fd_accrod_num))?$sp->fd_accrod_num:$susp->s_fno) .'號'); 
				$item2->appendChild($text); 

				$item3 = $dom->createElement("條列");
				$item3->setAttribute("序號", "二、");
				$item->appendChild($item3); 
				$item2 = $dom->createElement("文字"); 
				$item3->appendChild($item2);
				// $text = $dom->createTextNode($fd_fact_text); 
				$text = $dom->createTextNode("處分書業經簽核奉准在案(如附件資料)。"); 
				$item2->appendChild($text); 

				$item = $dom->createElement("段落"); 
				$item->setAttribute("段名", "擬辦：");  
				$root->appendChild($item); 
				$item2 = $dom->createElement("文字"); 
				$item->appendChild($item2);
				$text = $dom->createTextNode("陳核後，依規定發文。"); 
				$item2->appendChild($text);

				$item = $dom->createElement("敬陳"); 
				$root->appendChild($item); 
				$text = $dom->createTextNode("敬陳　局長"); 
				$item->appendChild($text);
			
				//echo $dom->saveXML();  
				// $xmls[] = $dom->saveXML();  
				$this->zip->add_data($sp->fd_send_num.'.di',  $dom->saveXML());

				// 在監另外多一份函稿
				if($susp->s_live_state == 3 || $susp->s_live_state == '3')
				{
					$dom = new DOMDocument("1.0","utf-8"); 
					$implementation = new DOMImplementation();
					$dom->appendChild($implementation->createDocumentType('函 SYSTEM "104_8_utf8.dtd" [<!ENTITY 表單 SYSTEM "函.sw" NDATA DI><!NOTATION DI SYSTEM ""><!NOTATION _X SYSTEM "">]')); 
					// display document in browser as plain text 
					// for readability purposes 
					header("Content-Type: text/plain"); 
					// create root element 
					$root = $dom->createElement("函"); 
					$dom->appendChild($root); 
					// create child element 
					
					$item = $dom->createElement("檔號"); 
					$root->appendChild($item); 
					
					$item2 = $dom->createElement("年度"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode(date('Y')-1911); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("分類號"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("07270399");
					// $text = $dom->createTextNode(""); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("案次號"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("1"); 
					$item2->appendChild($text); 
					
					$item = $dom->createElement("保存年限"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("3"); 
					$item->appendChild($text); 

					$item = $dom->createElement("稿面註記"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("擬辦方式"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("以稿代簽"); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("決行層級"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("二層決行"); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("應用限制"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("限制開放"); 
					$item2->appendChild($text); 
							
					
					$item = $dom->createElement("地址"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("103024臺北市大同區重慶北路2段219號"); 
					$item->appendChild($text); 

					$item = $dom->createElement("聯絡方式"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("承辦人：".$dp->dp_empno); 
					$item->appendChild($text); 

					$item = $dom->createElement("聯絡方式"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("電話：(02)2393-2397"); 
					$item->appendChild($text); 


					$item = $dom->createElement("聯絡方式"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("電子信箱：p26169@police.taipei"); 
					$item->appendChild($text); 

					$item = $dom->createElement("受文者"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("交換表"); 
					$item2->setAttribute("交換表單", "表單");  
					$item->appendChild($item2); 
					$text = $dom->createTextNode(explode(':', $susp->s_prison)[0]); 
					$item2->appendChild($text); 

					// $item = $dom->createElement("受文者"); 
					// $root->appendChild($item); 
					// $text = $dom->createTextNode(explode(':', $susp->s_prison)[0]); 
					// $item->appendChild($text); 

					// $item2 = $dom->createElement("交換表"); 
					// $item2->setAttribute("交換表單", "表單");  
					// $item->appendChild($item2); 

					$item = $dom->createElement("發文日期"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("年月日"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode(""); 
					$item2->appendChild($text); 
					
					$item = $dom->createElement("發文字號"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("字"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("北市警刑毒緝"); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("文號"); 
					$item->appendChild($item2); 
					$item3 = $dom->createElement("年度"); 
					$item2->appendChild($item3); 
					$text = $dom->createTextNode((date('Y')-1911)); 
					$item3->appendChild($text); 
					$item3 = $dom->createElement("流水號"); 
					$item2->appendChild($item3); 
					// $text = $dom->createTextNode($susp->f_project_num); //日後再確認
					// $text = $dom->createTextNode($susp->f_project_id);
					// $item3->appendChild($text); 
					// $item3 = $dom->createElement("支號"); 
					// $item2->appendChild($item3); 
					
					$item = $dom->createElement("速別"); 
					$item->setAttribute("代碼", "普通件");  
					$root->appendChild($item); 
					
					$item = $dom->createElement("密等及解密條件或保密期限"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("密等"); 
					$item->appendChild($item2); 
					$item2 = $dom->createElement("解密條件或保密期限"); 
					$item->appendChild($item2); 
					$item = $dom->createElement("附件"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("文字"); 
					$item->appendChild($item2); 

					$text = $dom->createTextNode("處分書1份"); 
					$item2->appendChild($text); 
					
					$item = $dom->createElement("主旨"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("文字"); 
					$item->appendChild($item2);

					$text = $dom->createTextNode("檢送貴所在所人".$susp->s_name."違反毒品危害防制條例案件處分書（文號：".$sp->fd_send_num."）1份，請查照惠復。"); 
					$item2->appendChild($text); 

					$item = $dom->createElement("段落"); 
					$item->setAttribute("段名", "說明：");  
					$root->appendChild($item); 
					$item2 = $dom->createElement("文字"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("依據行政程序法第89條：「對於在監所人為送達者，因囑託該監所長官為之」辦理。"); 
					$item2->appendChild($text);

					$item = $dom->createElement("正本"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("敬陳　大隊長"); 
					$item->appendChild($text); 
					$item2 = $dom->createElement('全銜'); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode(explode(':', $susp->s_prison)[0] . "(".explode(':', $susp->s_prison)[1].")"); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement('副本'); 
					$item->appendChild($item2); 
					$item2 = $dom->createElement('抄本'); 
					$item->appendChild($item2); 

					$this->zip->add_data($sp->fd_send_num.'inP.di',  $dom->saveXML());
				}
				$j++;
			}

			
			// $htmlString_head = '<table>
			// 			<thead>
			// 				<tr>
			// 					<td align="center">文號</td>
			// 					<td align="center">受文者</td>
			// 					<td align="center">郵遞區號</td>
			// 					<td align="center">郵遞地址</td>
			// 					<td align="center">案件類型(1.本轄要處分   2.本轄不罰、沒入者   3.他轄要處分   4.他轄不罰、沒入者)</td>
			// 					<td align="center">移送單位</td>
			// 				</tr>
			// 			</thead>
			// 			<tbody>';
			// $htmlString_body = '';
			// foreach ($query->result() as $suspect) {
			// 	$id = $suspect->s_num;
            //     $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
            //     $susp=$susp[0];
            //     // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
			// 	$fine=$this->getsqlmod->get3SP1($id)->result();
            //     $fine=$fine[0];
            //     $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
            //     $sp1=$this->getsqlmod->get3SP1($id)->result();
            //     $sp=$sp1[0];
            //     $spcount=$this->getsqlmod->getfdAll()->result();
            //     $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
            //     $cases=$cases[0];
            //     $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp->s_dp_project)->result();
            //     $susp_dp=$susp_dp[0];
			// 	$dp = $this->getsqlmod->getdpprojectname($susp->s_dp_project)->result()[0];

			// 	$casetype=1;

			// 	if(mb_strlen($susp->s_roffice1) < 10)
			// 		$s_roffice = '我是測試系統'.$susp->s_roffice;
			// 	else
			// 		$s_roffice = $susp->s_roffice;

			// 	$has_f_damount = ($fine->fd_amount > 0)?true:false; // 是否有罰鍰
			// 	$has_course = (isset($sp->fd_lec_date) && !empty($sp->fd_lec_date))?true:false;  // 是否有講習
			// 	$has_merge = (isset($sp->fd_merge) && !empty($sp->fd_merge))?true:false;  //是否有單沒入

			// 	if(!$has_f_damount && !$has_course && $has_merge)
			// 		$casetype=2;

			// 	$htmlString_body .= '<tr>
			// 		<td align="center">'.$sp->fd_send_num.'</td>
			// 		<td align="center">'.$sp->fd_target.'</td>
			// 		<td align="center">'.$sp->fd_zipcode.'</td>
			// 		<td align="center">'.$sp->fd_address.'</td>
			// 		<td align="center">'.$casetype.'</td>
			// 		<td align="center">'.$s_roffice.'</td>
			// 	</tr>';
			// }
			
			
			// $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
			// $spreadsheet = $reader->loadFromString($htmlString_head . $htmlString_body);

			// $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
			// // $writer->save('write.xlsx'); 
			// // header('Content-Type: application/vnd.ms-excel');
			// // header('Content-Disposition: attachment;filename="processdoc.xlsx"');
			// // header('Cache-Control: max-age=0');
			// // $writer->save('php://output'); // download file
			// // $this->zip->add_data('processdoc.xlsx',  $writer->save('processdoc.xlsx'));

			// $excel_file_tmp = tempnam("/tmp", 'processdoc.xlsx');
			// $writer->save($excel_file_tmp);
			// $this->zip->read_file($excel_file_tmp);

			$this->zip->download(date('YmdHis').'.zip');
			unlink(date('YmdHis') .'.zip');
                              
        // $pdf->Output($project.'.pdf', 'D');    //download
        
    }

	// 處分書-送達證書
	public function fine_doc_project_delivery(){
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
		
		$dp = $this->getsqlmod->getDP1bynum($project)->result()[0];
		$query = $this->getsqlmod->getdpprojectlist($dp->dp_name);

		$pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->setPrintHeader(false); //不要頁首
		//$pdf->setPrintFooter(false); //不要頁尾
		
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		$pdf->SetAutoPageBreak(FALSE, 0);
		$pdf->SetFont('twkai98_1','',11);
		$html='';
		foreach ($query->result() as $suspect)
		{
			$pdf->AddPage('P', 'A4');
			$id = $suspect->s_num;
			$susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
			$susp=$susp[0];
			$sp1=$this->getsqlmod->get3SP1($id)->result();
			$sp=$sp1[0];
			$suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();

			$target_str = '';
			$target_str2 = '';
			$target_str2_addr = '';
			if(null != $sp->fd_address && count(explode(",", $sp->fd_address)) > 1)
			{
				$fd_name = explode(",", $sp->fd_target)[0];
				$fd_addr = explode(",", $sp->fd_address)[0];
				$fd_zip = explode(",", $sp->fd_zipcode)[0];
				// $target_str = $fd_name.' 君 <br>            '. $fd_zip . ' ' . $fd_addr;
				// $target_str .= '<br/>            ';
				$fd_name1 = explode(",", $sp->fd_target)[1];
				$fd_addr1 = explode(",", $sp->fd_address)[1];
				$fd_zip1 = $susp->s_dpzipcode;
				$target_str .=  $fd_name.'('.$fd_name1.'法定代理人) 君 <br>'. $fd_zip1 . ' ' . $fd_addr1;
				$target_str2 =  $fd_name.'('.$fd_name1.'法定代理人) 君';
				$target_str2_addr = $fd_addr1;
			}
			else
			{
				$target_str = $sp->fd_target.' 君 <br>'. $sp->fd_zipcode . ' ' . $sp->fd_address ;
				$target_str2 = $sp->fd_target.' 君';
				$target_str2_addr = $sp->fd_address;
			}

			$send_str = '北市警刑毒緝字第' . $sp->fd_send_num .'號';

			// 在監送達證書格式是另一種
			if($susp->s_live_state != 3)
			{
				$pdf->SetMargins(20, 15, 20,true);
				$html ='
				<div style="width:600px" class="panel-body">
					<table width="600">
						<tr>
							<td align="right" style="height:30px;line-height:30px;"><h3>我是測試系統   </h3></td>
							<td align="left" style="height:30px;line-height:30px;"><h3>   送達證書-<strong>'.$sp->fd_num.'</strong></h3></td>
						</tr>
					</table>
					<table width="600" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td colspan="2" width="240" align="center" style="height:60px;line-height:75px;"><h5>受送達人名稱姓名地址</h5></td>
							<td colspan="2" width="360" style="height:60px;line-height:30px;"><h4>'.$target_str.'</h4></td>
						</tr>
						<tr>
							<td colspan="2" width="240" align="center" style="height:20px;line-height:25px;letter-spacing:108px;"><h5>文號</h5></td>
							<td colspan="2" width="360" style="height:20px;line-height:20px;"><h4>'.$send_str.'</h4></td>
						</tr>
						<tr>
							<td colspan="2" width="240" align="center" style="height:20px;line-height:25px;letter-spacing:7px;"><h5>送達文書(含案由)</h5></td>
							<td colspan="2" width="360" style="height:20px;line-height:20px;"><h4>違反毒品危害防制條例案件  <strong>處分書</strong></h4></td>
						</tr>
						<tr>
							<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"><h5>原寄郵局日戳</h5></td>
							<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"><h5>送達郵局日戳</h5></td>
							<td align="center" width="240" style="height:20px;line-height:25px;"><h5>送達處所 (由送達人填記)</h5></td>
							<td align="center" rowspan="2" width="120" style="height:60px;line-height:75px;"><h5>送達人簽章</h5></td>
						</tr>
						<tr>
							<td width="240" style="height:40px;line-height:25px;"><h5>☐同上記載地址 <br/>☐改送:</h5></td>
						</tr>
						<tr>
							<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"></td>
							<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"></td>
							<td align="center" width="240" style="height:20px;line-height:25px;"><h5>送達處所 (由送達人填記)</h5></td>
							<td align="center" rowspan="2" width="120" style="height:60px;line-height:75px;"></td>
						</tr>
						<tr>
							<td width="240" style="height:40px;line-height:25px;">
								<table>
									<tr>
										<td>中華民國</td>
										<td align="right">年</td>
										<td align="right">月</td>
										<td align="right">日</td>
									</tr>
									<tr>
										<td></td>
										<td align="right">午</td>
										<td align="right">時</td>
										<td align="right">分</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="4" width="600" style="height:20px;line-height:25px;letter-spacing:135px;">送達方式</td>
						</tr>
						<tr>
							<td align="center" colspan="4" width="600" style="height:20px;line-height:25px;letter-spacing:33px;">由送達人在☐上劃V選記</td>
						</tr>
					</table>
					<table width="600" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td colspan="2" width="280" style="height:30px;line-height:35px;"><h5>☐已將文書交與應受送達人</h5></td>
							<td colspan="2" width="320" style="height:30px;line-height:35px;">
								<table>
									<tr>
										<td width="200"><h5>☐本人</h5></td>
										<td width="120"><h5>(簽名或蓋章)</h5></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" width="280" style="height:30px;line-height:15px;"><h5>☐未獲會晤本人，已將文書交與有辨別事理能力之同居人、受僱人或應送達處所之接收郵件人員</h5></td>
							<td colspan="2" width="320" style="height:30px;line-height:15px;">
								<table>
									<tr>
										<td width="200"><h5>☐同居人<br/>☐受僱人<br/>☐應送達處所接收郵件人員</h5></td>
										<td width="120"><h5><br/>(簽名或蓋章)</h5></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" width="280" style="height:60px;line-height:20px;"><h5>☐應受送達之本人、同居人或受雇人收領後，拒絕或不能簽名或蓋章者，由送達人記明其事由</h5></td>
							<td colspan="2" width="320" style="height:60px;line-height:20px;">
								<table>
									<tr>
										<td width="200"><h5>送達人填記﹔</h5></td>
										<td width="120"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" width="280" style="height:60px;line-height:15px;"><h5>☐應受送達之本人、同居人、受僱人或應受送達處所接收郵件人員無正當理由拒絕領經送達人將文書留置於送達處所，以為送達</h5></td>
							<td colspan="2" width="320" style="height:60px;line-height:15px;">
								<table>
									<tr>
										<td width="200"><h5>☐本人<br/>☐同居人<br/>☐受僱人<br/>☐應受送達處所接收郵件人員</h5></td>
										<td width="120"><h5><br/>拒絕收領</h5></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" width="280" style="height:140px;line-height:15px;"><h5>☐未獲會晤本人亦無受領文書之同居人、受僱人或應受送達處所接收郵件人員，已將該送達文書﹔<br/><br/>☐應受送達之本人、同居人、受僱人或應受送達處所接收郵件人員無正當理由拒絕收領，並有難達留置情事，已將該送達文書﹔</h5></td>
							<td colspan="2" width="160" style="height:140px;line-height:20px;">
								<h5>☐寄存於_____________派出所<br/>☐寄存於_________鄉(鎮、市、區)__________公所<br/>☐寄存於_________鄉(鎮、市、區)__________公所__________村(里)辦公處<br/>☐寄存於_______________郵局</h5>
							</td>
							<td colspan="2" width="160" style="height:140px;line-height:15px;">
								<h5>並作送達通知書二份，一份黏貼於應受送達人住居所、事務所、營業所或其就業處所門首，一份☐交由鄰居轉交或☐置於該受送達處所信箱或其他適當位置，以為送達。</h5>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2" width="280" style="height:120px;line-height:130px;letter-spacing:20px;"><h5>送達人注意事項</h5></td>
							<td colspan="2" width="320" style="height:120px;line-height:20px;">
								<h5>一、依上述送達方法送達者，送達人應即將本送達證書，提出於交送達之行政機關附卷。</h5>
								<h5>二、不能依上述送達方法送達者，送達人應製作記載該事由之報告書，提出於交送達之行政機關附卷，並缴回應送達之文書。</h5>
							</td>
						</tr>
					</table>
					<table>
						<tr>
							<td>
								<h4>※ 請繳回 我是測試系統刑事警察大隊  地址﹔103024臺北市大同區重慶北路2段219號</h4>
							</td>
						</tr>
						<tr>
							<td>
								<h4>單位﹔毒品查緝中心('.$dp->dp_empno.')</h4>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h6>我是測試系統交郵送達格式</h6>
							</td>
						</tr>
					</table>
				</div>';
			}
			else
			{
				$pdf->SetMargins(15, 15, 15,true);
				$html ='
				<div style="width:630px" class="panel-body">
					<table width="630px" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td align="center" colspan="2" style="height:60px;line-height:40px;"><h2>我是測試系統違反毒品危害條例處分送達證書</h2></td>
						</tr>
						<tr>
							<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:5px;"><h2>應送達之文書</h2></td>
							<td width="480" style="height:60px;line-height:20px;"><h2>名稱﹔查獲違反毒品危害防制條例案件處分書<br/>件數﹔1件（案件列管編號﹔'.$sp->fd_num.'）</h2></td>
						</tr>
						<tr>
							<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:10.5px;"><h2>應受送達人</h2></td>
							<td width="480" style="height:60px;line-height:40px;"><h2>'.$target_str2.'</h2></td>
						</tr>
						<tr>
							<td align="center" colspan="4" style="height:20px;line-height:25px;letter-spacing:55px;"><h3>由送達人劃V選記</h3></td>
						</tr>
					</table>
					<table width="630px" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td width="30" style="height:20px;line-height:25px;"></td>
							<td width="30" align="center" style="height:20px;line-height:25px;"><h3>一</h3></td>
							<td width="570" style="height:20px;line-height:25px;"><h3>已將文書交與應受達本人。</h3></td>
						</tr>
						<tr>
							<td rowspan="2" width="30" style="height:20px;line-height:25px;"></td>
							<td rowspan="2" width="30" align="center" style="height:20px;line-height:50px;"><h3>二</h3></td>
							<td rowspan="2" width="230" style="height:20px;line-height:25px;"><h3>未會晤本人已將文書交與有辨別事理能力之下列</h3></td>
							<td width="100" align="center" style="height:20px;line-height:25px;letter-spacing:16px;"><h3>同居人</h3></td>
							<td width="240" align="center" style="height:20px;line-height:25px;"></td>
						</tr>
						<tr>
							<td width="100" align="center" style="height:20px;line-height:25px;letter-spacing:16px;"><h3>受僱人</h3></td>
							<td width="240" align="center" style="height:20px;line-height:25px;"></td>
						</tr>
						<tr>
							<td width="30" style="height:20px;line-height:25px;"></td>
							<td width="30" align="center" style="height:20px;line-height:55px;"><h3>三</h3></td>
							<td width="570" style="height:20px;line-height:20px;"><h3>未會晤本人亦無收領文書之同居人、受僱人已將文書寄存於_____________________，並作送達通知書兩份，一份黏貼於應受送達人門首並拍照，一份交由鄰居轉交或置放於送達處所信箱或其他適當位置。</h3></td>
						</tr>
						<tr>
							<td rowspan="3" width="30" style="height:50px;line-height:60px;"></td>
							<td rowspan="3" width="30" align="center" style="height:60px;line-height:80px;"><h3>四</h3></td>
							<td width="230" style="height:20px;line-height:25px;"><h3>本人</h3></td>
							<td rowspan="3" width="340" style="height:50px;line-height:15px;"><br/><h3>無正當理由拒絕收領，已將文書留置於該處，以為送達。</h3></td>
						</tr>
						<tr>
							<td width="230" style="height:20px;line-height:25px;"><h3>同居人</h3></td>
						</tr>
						<tr>
							<td width="230" style="height:20px;line-height:25px;"><h3>受僱人</h3></td>
						</tr>
						<tr>
							<td width="60" align="center" style="height:60px;line-height:75px;"><h3>五</h3></td>
							<td width="570" style="height:60px;line-height:65px;"><h2>收領人簽名或蓋章</h2></td>
						</tr>
						<tr>
							<td width="60" align="center" style="height:60px;line-height:75px;"><h3>六</h3></td>
							<td width="570" style="height:60px;line-height:65px;"><h2>收領人拒絕或不能簽名或蓋章</h2></td>
						</tr>
					</table>
					<table width="630px" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:5px;"><h2>送達日期時間</h2></td>
							<td width="480" style="height:60px;line-height:20px;"><h2>中華民國____年____月____日____午____時____分（由送達人填記）</h2></td>
						</tr>
						<tr>
							<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:10.5px;"><h2>送達處所</h2></td>
							<td width="480" style="height:60px;line-height:40px;"><h2>'.$target_str2_addr.'</h2></td>
						</tr>
						<tr>
							<td colspan="2" align="center" width="630" style="height:60px;line-height:20px;">
								<h2>中華民國____年____月____日製作（由送達人填記）</h2>
								<h1>送達人（簽章）</h1>
							</td>
						</tr>
						<tr>
							<td align="center" width="150" style="height:60px;line-height:70px;letter-spacing:10.5px;"><h2>注意事項</h2></td>
							<td width="480" style="height:60px;line-height:15px;">
								<table>
									<tr>
										<td width="40" style="line-height:20px;"><h2>一、</h2></td>
										<td width="440" style="line-height:20px;"><h2>「一」、「二」、「三」、「四」等欄位，係所送達方式在該欄位勾選為記。</h2></td>
									</tr>
									<tr>
										<td width="40"><h2>二、</h2></td>
										<td width="440"><h2>遇有「六」欄情形時，送達人應記明其事由。</h2></td>
									</tr>
									<tr>
										<td width="40"><h2>三、</h2></td>
										<td width="440"><h2>代領人應填註「關係」及「身分證字號」。</h2></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table>
						<tr>
							<td>
								<h5>※ 請繳回 我是測試系統刑事警察大隊  地址﹔103024臺北市大同區重慶北路2段219號</h5>
							</td>
						</tr>
						<tr>
							<td>
								<h5>單位﹔毒品查緝中心('.$dp->dp_empno.')</h5>
							</td>
						</tr>
					</table>';
			}
			
			$pdf->writeHTML($html, true, false, true, false, '');
		}
		// $pdf->Output($project.'.pdf', 'D');    //download
        $pdf->Output($dp->dp_name.'.pdf', 'I');    //read
	}
    
    public function fine_doc_project_fin(){//產生處分書pdf
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
         
		// $dp = $this->getsqlmod->getdpprojectname($project)->result()[0];
		$dp = $this->getsqlmod->getDP1bynum($project)->result()[0];
		$query = $this->getsqlmod->getdpprojectlist($dp->dp_name);
        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            $pdf->SetMargins(5.5, 3.5, 10,true);
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            
            $pdf->SetAutoPageBreak(FALSE, 0);
            $pdf->SetFont('twkai98_1','',10);
            $html='';       
            foreach ($query->result() as $suspect)
            {
				$pdf->AddPage('P', 'A4');
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
				$fine=$this->getsqlmod->get3SP1($id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp->s_dp_project)->result();
                $susp_dp=$susp_dp[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
				$dp = $this->getsqlmod->getdpprojectname($susp->s_dp_project)->result()[0];
                //$phone=$phone[0];
                // if(!isset($phone))$phone1=null;
                // if(isset($phone))$phone1=$phone[0]->p_no;
				$phone1=(isset($fine->fd_phone)?$fine->fd_phone:'');

                $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 20;
                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                    $fname = NULL;
                    $fgender = NULL;
                    $fic = NULL;
                    $fbirth = NULL;
                    $fphone = NULL;
                    $fzipcode = NULL;
                    $faddress = NULL;
                if(isset($suspfadai[0])){
                    $suspfadai = $suspfadai[0];
                    $stampBirth = strtotime($susp->s_birth);
                    if ($stampBirth > $stampToday) {
                        $fname =  $suspfadai->s_fadai_name;
                        $fgender = $suspfadai->s_fadai_gender;
                        $fic = $suspfadai->s_fadai_ic;
                        $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                        $fphone = $suspfadai->s_fadai_phone;
                        $fzipcode = $suspfadai->s_fadai_zipcode;
                        $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                    }else{
                        $fname = NULL;
                        $fgender = NULL;
                        $fic = NULL;
                        $fbirth = NULL;
                        $fphone = NULL;
                        $fzipcode = NULL;
                        $faddress = NULL;
                    }
                }
                $susp1 = $this->getsqlmod->get2Susp($susp->s_num)->result(); // 抓snum
                $susp1 = (isset($susp1[0]))?$susp1[0]:$susp1; // 抓snum
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2msg = Null;
                foreach ($drug2 as $drug3){
                    if(!isset($drug3->ddc_level)){}
                    else{
                        $drug2msg = $drug2msg . '第'.$drug3->ddc_level .'級' . '『'.$drug3->ddc_ingredient.'』 ';
                    }
                }  
                $drug2=((isset($drug2[0]))?$drug2[0]:null);
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];

				$has_f_damount = ($fine->fd_amount > 0)?true:false; // 是否有罰鍰
				$has_course = (isset($sp->fd_lec_date) && !empty($sp->fd_lec_date))?true:false;  // 是否有講習
				$has_merge = (isset($sp->fd_merge) && !empty($sp->fd_merge))?true:false;  //是否有單沒入
				$has_ary = array($has_f_damount, $has_course, $has_merge);
				$count_has_ch = array('一、', '二、', '三、');

                $target_str = '';
                if(null != $sp->fd_address && count(explode(",", $sp->fd_address)) > 1)
                {
                    $fd_name = explode(",", $sp->fd_target)[0];
                    $fd_addr = explode(",", $sp->fd_address)[0];
                    $fd_zip = explode(",", $sp->fd_zipcode)[0];
                    // $target_str = $fd_name.' 君 <br>            '. $fd_zip . ' ' . $fd_addr;
                    // $target_str .= '<br/>            ';
                    $fd_name1 = explode(",", $sp->fd_target)[1];
                    $fd_addr1 = explode(",", $sp->fd_address)[1];
                    $fd_zip1 = $susp->s_dpzipcode;
                    $target_str .=  $fd_name.'('.$fd_name1.'法定代理人) 君 <br>            '. $fd_zip1 . ' ' . $fd_addr1;
                    if($susp->s_live_state == 2)
                    {
                        $target_str .= '（指定送達現住地）';
                    }
                }
                else
                {
                    $target_str = $sp->fd_target.' 君 <br>            '. $sp->fd_zipcode . ' ' . $sp->fd_address ;
                    if($susp->s_live_state == 2)
                    {
                        $target_str .= '（指定送達現住地）';
                    }
                }

				if($sp->fd_date != '0000-00-00' && isset($sp->fd_date))
				{
					$send_str = '發文日期字號：'.(date('Y')-1911).'年'. date('m', strtotime($sp->fd_date))*1 .'月' . date('d', strtotime($sp->fd_date))*1  .'日北市警刑毒緝字第' . $sp->fd_send_num .'號';
				}
				else
				{
					if($dp->dp_send_date !=  '0000-00-00' && isset($dp->dp_send_date))
					{
						$send_str = '發文日期字號：'.((int)substr($dp->dp_send_date, 0, 4)- 1911) .'年'. (int)substr($dp->dp_send_date, 5, 2)*1 . '月' . (int)substr($dp->dp_send_date, 8, 2)*1 .'日北市警刑毒緝字第' . $sp->fd_send_num .'號';
					}else{
						$send_str = '發文日期字號：<strong>民國'.(date('Y')-1911).'年'.date('m')*1 .'月  日</strong>北市警刑毒緝字第' . $sp->fd_send_num . '號';
					}
					
				}
				if(mb_strlen($susp->s_roffice1) < 10)
					$s_roffice = '我是測試系統'.$susp->s_roffice;
				else
					$s_roffice = $susp->s_roffice;

                $html ='
                                <div style="width:680px" class="panel-body">
                                    <table width="680">
                                        <tr><td colspan="3"><h2>列管編號：'.$sp->fd_num .'</h2> </td><td colspan="5"></td></tr>
                                        <tr><td colspan="8"> </td></tr>
                                        <tr><td colspan="8">正本：'.(($has_f_damount == false && $has_course == false && $has_merge == true)?$s_roffice:$target_str).'</td></tr>
										<tr><td colspan="8">副本：'.(($has_f_damount == false && $has_course == false && $has_merge == true)?'我是測試系統刑事警察大隊':$s_roffice.'、我是測試系統刑事警察大隊、臺北市政府毒品危害防制中心').'</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統 違反毒品危害防制條例案件處分書</h3></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">'.$send_str.'</td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">依據：'.((isset($sp->fd_acrrod_unit))?$sp->fd_acrrod_unit:$s_roffice) .((isset($sp->fd_acrrod_date))?((date('Y', strtotime($sp->fd_acrrod_date))-1911) .'年'.date('m', strtotime($sp->fd_acrrod_date))*1 .'月' . date('d', strtotime($sp->fd_acrrod_date))*1 .'日'):((isset($susp->s_fdate))?((strlen($susp->s_fdate) > 7 && $susp->s_fdate != '0000-00-00')?(date('Y', strtotime($susp->s_fdate))-1911) .'年'.date('m', strtotime($susp->s_fdate))*1 .'月' . date('d', strtotime($susp->s_fdate))*1 .'日':''):'')) . ((isset($sp->fd_accrod_no))?$sp->fd_accrod_no:((mb_strlen($susp->s_roffice1) < 4)?'北市警'.$susp->s_roffice1.((strpos($susp->s_roffice, '分局') !== false)?'分刑':'') :$susp->s_roffice1)).'字第'. ((isset($sp->fd_accrod_num))?$sp->fd_accrod_num:$susp->s_fno).'
                                                號</td>
                                            </tr>
                                            <tr>
												<td rowspan="4" width="77" height="70" style="line-height:70px;">
													<h3 align="center" >受處分人</h3>
												</td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
												<td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
                                                <td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.((isset($susp->s_prison) && !empty($susp->s_prison))?explode(':', $susp->s_prison)[1] .'('.explode(':', $susp->s_prison)[0].')' :$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress) . (($susp->s_live_state == 2)?'（指定送達現住地）':'')  .'</strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>';
        // list($year,$month,$day) = explode("-",$susp->s_birth);
        // $year_diff = date("Y") - $year;
        // $month_diff = date("m") - $month;
        // $day_diff  = date("d") - $day;
        // if ($day_diff < 0 || $month_diff < 0)
        //     $year_diff--;  
        $html .=                                '<td rowspan="4" width="77" height="70" style="line-height:20px;"><h3 align="center"><br>法定 代理人</h3></td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_name:'').'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_gender:'').'</strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?'民國'.(date('Y', strtotime($suspfadai->s_fadai_birth))-1911) .'年'.(date('m', strtotime($suspfadai->s_fadai_birth))) .'月' .(date('d', strtotime($suspfadai->s_fadai_birth))) .'日':'').'</strong></td>
                                            </tr>
                                            <tr>
												<td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_ic:'').'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_phone:'').'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
												<td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="445" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !$this->isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address:'').'</strong></td>
                                            </tr>                                            
                                            <tr>
												<td width="77" height="70" style="line-height:70px;"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">';
												$ch_j = 0;
												$has_ch = array();
												for ($i=0; $i < count($has_ary); $i++) { 
													if($has_ary[$i])
														array_push($has_ch, $count_has_ch[$ch_j++]);
													else
														array_push($has_ch, '');
												}
												if($ch_j > 1)
													$html .= '受處分人處罰：<br/>';

												if($has_ary[0]){
													$html .=(($ch_j > 1)?$has_ch[0]:'受處分人處罰：') . '新臺幣' . ($fine->fd_amount/10000) . '萬元整。'.((isset($sp->fd_times) && $sp->fd_times > 1)?'( '.(date('Y')-1911).'年第'.$sp->fd_times.'次裁罰 )':'').'<br/>';
												}

												if($has_ary[1]){
													$html .=(($ch_j > 1)?$has_ch[1]:'受處分人處罰：') . '毒品危害6小時。(講習時間詳見下方「毒品講習」欄位)<br/>';
												}

												if($has_ary[2] ){
													$patterns = array('1級','2級','3級','4級');
													$replacements = array('一級','二級','三級','四級');													

													$html .=(($ch_j > 1)?$has_ch[2]:'受處分人處罰：')  . str_replace($patterns, $replacements, $sp->fd_drug_msg) . '沒入。';
												}
												
												// 受處分人處罰：<br>
												//         一、新臺幣<strong>'.($fine->f_damount/10000).'</strong>萬元整<br>
												//         二、毒品危害 <strong>6</strong>小時。(講習時間詳見下方「毒品講習」欄位)<br>
												//         三、<strong> '.$sp->fd_drug_msg/*$drug2msg.$drug->e_count.$drug->e_type*/.'</strong>沒入。
			$html .=                            '</td>
                                            </tr>
                                            <tr>
												<td width="77" height="75" style="line-height:75px;"><h3 align="center">事實</h3></td>';
        $s_date = $this->tranfer2RCyear($cases->s_date);
        $s_time = $this->tranfer2RChour($cases->s_time);   
        $courser_str = '';
        $fd_fact_text = '';
        if(isset($susp->s_prison) && !empty($susp->s_prison))
        {
            $courser_str = '請受處分人出所後，主動聯繫臺北市政府毒品危害防制中心。';
        }
        else
        {
            if(isset($sp->fd_lec_date) && !empty($sp->fd_lec_date) && $sp->fd_other_lec == null){
                $md_str = explode("(", $sp->fd_lec_date)[0];
                $week_str = explode("(", $sp->fd_lec_date)[1];

                $courser_str = "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日（". $week_str . '，時間：' . explode('-', $sp->fd_lec_time)[0] . "（請攜帶有相片之證件報到，逾時將無法入場），地點：".preg_replace('/\s(?=)/', '', $sp->fd_lec_place) ."（" . $sp->fd_lec_address . "）";
            }
            else
            {
                $courser_str = '由臺北市政府毒品危害防制中心另行通知。';
            }
        }
        if(!isset($sp->fd_fact_text)||($sp->fd_fact_text==null)||($sp->fd_fact_text=='')){
            $fd_fact_text = $susp->s_name. $s_date . $s_time.'許，在本市'.$cases->s_place.'，為'.$cases->s_office.'員警查獲持有'.$sp->fd_drug_msg.'，經採集毒品及尿液檢體送專業單位鑑驗，均呈第'.((isset($drug2))?$drug2->ddc_level:'').'級毒品「'.((isset($drug2))?$drug2->ddc_ingredient:'').'」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg;
        }
        else
        {
            $fd_fact_text = $sp->fd_fact_text;
        }
		$reason_1= '';
		$reason_2= '';
		if($has_ary[0] || $has_ary[1])$reason_1 = '■'; else $reason_1 = '□';
		if($has_ary[2])$reason_2 = '■'; else $reason_2 = '□';

        $html .=                                 '<td colspan="7" width="603" >受處分人'.$fd_fact_text .'</td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:15px;"><h3 align="center"><br>理由及 法令依據</h3></td>
                                                <td colspan="7" width="603">'.$reason_1.'依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                        <br>'.$reason_2.'依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                </td>
												</tr>';
		if($has_ary[0]) {
		$html .=                                '<tr>
												<td width="77" height="100" style="line-height:15px;"><h3 align="center"><br><br>繳納期限 及方式</h3></td>
                                                <td colspan="7" width="603">一、罰鍰限於<strong>民國'.(($fine->fd_limitdate != '0000-00-00')?(date('Y', strtotime($fine->fd_limitdate))-1911) .'年'.(date('m', strtotime($fine->fd_limitdate))) .'月'.(date('d', strtotime($fine->fd_limitdate))):'Ｏ年Ｏ月Ｏ') .'日</strong>前選擇下列方式之一繳款：
                                                            （一）以自動化設備（ATM、網路ATM、網路銀行）匯款至「虛擬帳號」（限本處分書）。
                                                            （二）至金融機構臨櫃繳款至「臨櫃帳戶」。<br>二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong>
                                                            （三）輸入本案虛擬帳號：<strong>'.$fine->fd_bvc.'</strong>
                                                            三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。
                                                </td>
												</tr>';
		}
		if($has_ary[1]) {
		$html .=                                '<tr>
												<td width="77" height="80" style="line-height:80px;"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">一、可選擇參與實體講習或線上課程(每年可參與三次，惟應以申請不同套裝課程)。<br>二、講習時間、地點：'.$courser_str .'要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。<br>三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。</td>
														</tr>';
	}
	$html .=                                '<tr>
												<td width="77" height="50" style="line-height:50px;"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。<br>二、承辦人：'.$dp->dp_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table>
                                    <tr>
                                    <td height="218"></td>   
                                    </tr>            
                                    </table> 
                                </div>';
								$pdf->writeHTML($html, true, false, true, false, ''); 
            }
                                   
        // $pdf->Output($project.'.pdf', 'D');    //download
        $pdf->Output($project.'.pdf', 'I');    //read
    }
    
    public function fine_doc_project_fin_surc(){//產生處分書pdf怠金
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            $pdf->SetMargins(5.5, 3.5, 10,true);
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp->s_dp_project)->result();
                $susp_dp=$susp_dp[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 20;
                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                    $fname = NULL;
                    $fgender = NULL;
                    $fic = NULL;
                    $fbirth = NULL;
                    $fphone = NULL;
                    $fzipcode = NULL;
                    $faddress = NULL;
                if(isset($suspfadai[0])){
                    $suspfadai = $suspfadai[0];
                    $stampBirth = strtotime($susp->s_birth);
                    if ($stampBirth > $stampToday) {
                        $fname =  $suspfadai->s_fadai_name;
                        $fgender = $suspfadai->s_fadai_gender;
                        $fic = $suspfadai->s_fadai_ic;
                        $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                        $fphone = $suspfadai->s_fadai_phone;
                        $fzipcode = $suspfadai->s_fadai_zipcode;
                        $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                    }else{
                        $fname = NULL;
                        $fgender = NULL;
                        $fic = NULL;
                        $fbirth = NULL;
                        $fphone = NULL;
                        $fzipcode = NULL;
                        $faddress = NULL;
                    }
                }
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2msg = Null;
                foreach ($drug2 as $drug3){
                    if(!isset($drug3->ddc_level)){}
                    else{
                        $drug2msg = $drug2msg . '第'.$drug3->ddc_level .'級' . '『'.$drug3->ddc_ingredient.'』 ';
                    }
                }  
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                                <div style="width:680px" class="panel-body"><p>
                                    <table>
                                        <tr><td ><h2>列管編號：'.$sp->fd_num .'</h2> </td><td colspan="2"></td></tr>
                                        <tr><td colspan="3"> </td></tr>
                                        <tr><td colspan="3">正本：'.$sp->fd_target.'君'.$sp->fd_zipcode.' '.$sp->fd_address.'</td></tr>
                                        <tr><td colspan="3">副本：我是測試系統'.$susp->s_roffice.'、我是測試系統刑事警察大隊</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統 違反毒品危害防制條例案件處分書</h3></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">發文日期字號：'.(date('Y')-1911).'年'.date('m', strtotime($sp->fd_date)).'月'.date('d', strtotime($sp->fd_date)).'日北市警刑毒緝字第號'.$sp->fd_send_num.'</td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">依據：我是測試系統'.$susp->s_roffice.(date('Y', strtotime($sp->fd_date))-1911) .'年'.date('m', strtotime($sp->fd_date)).'月'.date('d', strtotime($sp->fd_date)).'日北市警分刑字第號'.$susp->s_fno.'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77">
                                                <br>
                                                <h3 align="center">受處分人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77"><h3 align="center">法　定<br>代理人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$fname.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$fgender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>'.$fbirth.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$fic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$fphone.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>
                                                        一、新臺幣<strong>'.($fine->f_damount/10000).'</strong>萬元整<br>
                                                        二、毒品危害 <strong>6</strong>小時。(講習時間詳見下方「毒品講習」欄位)<br>
                                                        三、<strong> '.$drug2msg.$drug->e_count.$drug->e_type.'</strong>沒入。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>事實</h3></td>
                                                <td colspan="7" width="603">受處分人'.$susp->s_name.$cases->s_date.'許，在本市'.$cases->r_county.$cases->r_district.$cases->r_address.'，為'.$cases->s_office.'員警查獲持有'.$sp->fd_drug_msg.'，經採集毒品及尿液檢體送專業單位鑑驗，均呈第'.$drug2->ddc_level.'級毒品「'.$drug2->ddc_ingredient.'」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg .'</td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">理由及<br>法令依據</h3></td>
                                                <td colspan="7" width="603">
                                                        ■
                                                        依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                        <br>■
                                                        依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">繳納期限<br>及方式</h3></td>
                                                <td colspan="7" width="603"> 一、罰鍰限於<strong>民國'.(date('Y', strtotime($fine->f_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日</strong>前選擇下列方式之一繳款：
                                                            (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                            (二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>
                                                                二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong>
                                                            (三)輸入本案虛擬帳號：<strong>'.$fine->f_BVC.'</strong>
                                                                三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">
                                                        一、可選擇參與實體講習或線上課程(每年可參與三次，惟應以申請不同套裝課程)。<br>
                                                        二、講習時間： 民國'.(date('Y', strtotime($sp->fd_lec_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日09:30分（請攜帶有相片之證件報到，逾時將無法入場），地點：臺北市非政府(NGO)會館1919樓 多功能資料室 。如要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。<br>
                                                        三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：'.$sp->fd_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table>
                                    <tr>
                                    <td height="264"></td>   
                                    </tr>            
                                    </table> 
                                </div>';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function fine_doc_public()//產生處分書公示pdf
    {

        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getdeliverylist_forpublicProject($project); 
        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp->s_dp_project)->result();
                $susp_dp=$susp_dp[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 20;
                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                    $fname = NULL;
                    $fgender = NULL;
                    $fic = NULL;
                    $fbirth = NULL;
                    $fphone = NULL;
                    $fzipcode = NULL;
                    $faddress = NULL;
                if(isset($suspfadai[0])){
                    $suspfadai = $suspfadai[0];
                    $stampBirth = strtotime($susp->s_birth);
                    if ($stampBirth > $stampToday) {
                        $fname =  $suspfadai->s_fadai_name;
                        $fgender = $suspfadai->s_fadai_gender;
                        $fic = $suspfadai->s_fadai_ic;
                        $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                        $fphone = $suspfadai->s_fadai_phone;
                        $fzipcode = $suspfadai->s_fadai_zipcode;
                        $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                    }else{
                        $fname = NULL;
                        $fgender = NULL;
                        $fic = NULL;
                        $fbirth = NULL;
                        $fphone = NULL;
                        $fzipcode = NULL;
                        $faddress = NULL;
                    }
                }
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $susp1 = $this->getsqlmod->get2Susp($susp->s_num)->result(); // 抓snum
                $susp1 = $susp1[0]; // 抓snum
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=(isset($drug2[0]))?$drug2[0]:null;
                $drug2msg = Null;
                if($drug2)
                {
                    foreach ($drug2 as $drug3){
                        if(!isset($drug3->ddc_level)){}
                        else{
                            $drug2msg = $drug2msg . '第'.$drug3->ddc_level .'級' . '『'.$drug3->ddc_ingredient.'』 ';
                        }
                    }  
                }
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                                <div style="width:680px" class="panel-body"><p>
                                    <table>
                                        <tr><td ><h2>列管編號：'.$sp->fd_num .'</h2> </td><td colspan="2">郵寄 以稿代簽 限制開放 第二層決行 檔號：109/07270399 保存年限：3年</td></tr>
                                        <tr>
                                        <td width="220"> 校對：</td>
                                        <td width="220"> 監印：</td>
                                        <td width="220">發文：</td>
                                        </tr>
                                        <tr><td colspan="3"> </td></tr>
                                        <tr><td colspan="3">正本：'.$sp->fd_target.'君'.$sp->fd_zipcode.' '.$sp->fd_address.'</td></tr>
                                        <tr><td colspan="3">副本：我是測試系統'.$susp->s_roffice.'、我是測試系統刑事警察大隊</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統 違反毒品危害防制條例案件處分書(稿)</h3></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">發文日期字號：'.(date('Y')-1911).'年'.date('m', strtotime($sp->fd_date)).'月'.date('d', strtotime($sp->fd_date)).'日北市警刑毒緝字第號'.$sp->fd_send_num.'</td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">依據：我是測試系統'.$susp->s_roffice.(date('Y', strtotime($sp->fd_date))-1911) .'年'.date('m', strtotime($sp->fd_date)).'月'.date('d', strtotime($sp->fd_date)).'日北市警分刑字第號'.$susp->s_fno.'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77">
                                                <br>
                                                <h3 align="center">受處分人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77"><h3 align="center">法　定<br>代理人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$fname.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$fgender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>'.$fbirth.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$fic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$fphone.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>
                                                        一、新臺幣<strong>'.($fine->f_damount/10000).'</strong>萬元整<br>
                                                        二、毒品危害 <strong>6</strong>小時。(講習時間詳見下方「毒品講習」欄位)<br>
                                                        三、<strong> '.$drug2msg.$drug->e_count.$drug->e_type.'</strong>沒入。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>事實</h3></td>
                                                <td colspan="7" width="603">受處分人'.((isset($susp))?$susp->s_name:'').((isset($cases))?$cases->s_date:'').'許，在本市'.((isset($cases))?$cases->r_county:'').((isset($cases))?$cases->r_district:'').((isset($cases))?$cases->r_address:'').'，為'.((isset($cases))?$cases->s_office:'').'員警查獲持有'.((isset($sp))?$sp->fd_drug_msg:'').'，經採集毒品及尿液檢體送專業單位鑑驗，均呈第'.((isset($drug2))?$drug2->ddc_level:'').'級毒品「'.((isset($drug2))?$drug2->ddc_ingredient:'').'」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.((isset($sp))?$sp->fd_dis_msg:'') .'</td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">理由及<br>法令依據</h3></td>
                                                <td colspan="7" width="603">
                                                        ■
                                                        依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                        <br>■
                                                        依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">繳納期限<br>及方式</h3></td>
                                                <td colspan="7" width="603"> 一、罰鍰限於<strong>民國'.(date('Y', strtotime($fine->f_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日</strong>前選擇下列方式之一繳款：
                                                            (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                            (二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>
                                                                二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong>
                                                            (三)輸入本案虛擬帳號：<strong>'.$fine->f_BVC.'</strong>
                                                                三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">
                                                        一、可選擇參與實體講習或線上課程(每年可參與三次，惟應以申請不同套裝課程)。<br>
                                                        二、講習時間： 民國'.(date('Y', strtotime($sp->fd_lec_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日09:30分（請攜帶有相片之證件報到，逾時將無法入場），地點：臺北市非政府(NGO)會館1919樓 多功能資料室 。如要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。<br>
                                                        三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：'.$sp->fd_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table>
                                    <tr>
                                    <td>承辦單位(2539)</td>   
                                    <td>核稿</td>   
                                    <td>決行</td>   
                                    </tr>            
                                    </table> 
                                    <table>
                                    <tr>
                                    <td height="190"></td>   
                                    </tr>            
                                    </table> 
                                </div>';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    public function fine_doc_public_fin()//產生處分書公示pdf
    {
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);

        $query = $this->getsqlmod->getdeliverylist_forpublicProject($project); 
		// var_dump($query->result());
		// exit;
		$dp = $this->getsqlmod->getdpprojectname($project)->result()[0];
        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            // $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
			$pdf->SetFont('twkai98_1','',10);
            // $html='';       
            foreach ($query->result() as $suspect)
            {
				$pdf->AddPage('P', 'A4');
				$html='';  
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp->s_dp_project)->result();
                $susp_dp=$susp_dp[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 20;
                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                    $fname = NULL;
                    $fgender = NULL;
                    $fic = NULL;
                    $fbirth = NULL;
                    $fphone = NULL;
                    $fzipcode = NULL;
                    $faddress = NULL;
                if(isset($suspfadai[0])){
                    $suspfadai = $suspfadai[0];
                    $stampBirth = strtotime($susp->s_birth);
                    if ($stampBirth > $stampToday) {
                        $fname =  $suspfadai->s_fadai_name;
                        $fgender = $suspfadai->s_fadai_gender;
                        $fic = $suspfadai->s_fadai_ic;
                        $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                        $fphone = $suspfadai->s_fadai_phone;
                        $fzipcode = $suspfadai->s_fadai_zipcode;
                        $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                    }else{
                        $fname = NULL;
                        $fgender = NULL;
                        $fic = NULL;
                        $fbirth = NULL;
                        $fphone = NULL;
                        $fzipcode = NULL;
                        $faddress = NULL;
                    }
                }
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $susp1 = $this->getsqlmod->get2Susp($susp->s_num)->result(); // 抓snum
                $susp1 = (isset($susp1[0]))?$susp1[0]:$susp1; // 抓snum
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2msg = Null;
                foreach ($drug2 as $drug3){
                    if(!isset($drug3->ddc_level)){}
                    else{
                        $drug2msg = $drug2msg . '第'.$drug3->ddc_level .'級' . '『'.$drug3->ddc_ingredient.'』 ';
                    }
                }  
                $drug2=((isset($drug2[0]))?$drug2[0]:null);
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];

				$has_f_damount = ($fine->f_damount > 0)?true:false; // 是否有罰鍰
				$has_course = (isset($sp->fd_lec_date) && !empty($sp->fd_lec_date))?true:false;  // 是否有講習
				$has_merge = (isset($sp->fd_merge) && !empty($sp->fd_merge))?true:false;  //是否有單沒入
				$has_ary = array($has_f_damount, $has_course, $has_merge);
				$count_has_ch = array('一、', '二、', '三、');

                $target_str = '';
                if(null != $sp->fd_address && count(explode(",", $sp->fd_address)) > 1)
                {
                    $fd_name = explode(",", $sp->fd_target)[0];
                    $fd_addr = explode(",", $sp->fd_address)[0];
                    $fd_zip = explode(",", $sp->fd_zipcode)[0];
                    // $target_str = $fd_name.' 君 <br>            '. $fd_zip . ' ' . $fd_addr;
                    // $target_str .= '<br/>            ';
                    $fd_name1 = explode(",", $sp->fd_target)[1];
                    $fd_addr1 = explode(",", $sp->fd_address)[1];
                    $fd_zip1 = explode(",", $sp->fd_zipcode)[1];
                    $target_str .=  $fd_name.'('.$fd_name1.'法定代理人) 君 <br>            '. $fd_zip1 . ' ' . $fd_addr1;
                    if($susp->s_live_state == 2)
                    {
                        $target_str .= '（指定送達現住地）';
                    }
                }
                else
                {
                    $target_str = $sp->fd_target.' 君 <br>            '. $sp->fd_zipcode . ' ' . $sp->fd_address ;
                    if($susp->s_live_state == 2)
                    {
                        $target_str .= '（指定送達現住地）';
                    }
                }

				if($sp->fd_date != '0000-00-00' && isset($sp->fd_date))
				{
					$send_str = '發文日期字號：'.(date('Y')-1911).'年'. date('m', strtotime($sp->fd_date))*1 .'月' . date('d', strtotime($sp->fd_date))*1  .'日北市警刑毒緝字第' . $sp->fd_send_num .'號';
				}
				else
				{
					if($dp->dp_send_date !=  '0000-00-00' && isset($dp->dp_send_date))
					{
						$send_str = '發文日期字號：'.((int)substr($dp->dp_send_date, 0, 4)- 1911) .'年'. (int)substr($dp->dp_send_date, 5, 2)*1 . '月' . (int)substr($dp->dp_send_date, 8, 2)*1 .'日北市警刑毒緝字第' . $sp->fd_send_num .'號';
					}else{
						$send_str = '發文日期字號：<strong>民國'.(date('Y')-1911).'年'.date('m')*1 .'月  日</strong>北市警刑毒緝字第' . $sp->fd_send_num . '號';
					}
					
				}
				if(mb_strlen($susp->s_roffice1) < 10)
					$s_roffice = '我是測試系統'.$susp->s_roffice;
				else
					$s_roffice = $susp->s_roffice;

                $html ='
                                <div style="width:680px" class="panel-body">
                                    <table width="680">
                                        <tr><td colspan="3"><h2>列管編號：'.$sp->fd_num .'</h2> </td></tr>
                                        <tr><td colspan="3"> </td></tr>
                                        <tr><td colspan="3">正本：'.$target_str.'</td></tr>
                                        <tr><td colspan="3">副本：'.$s_roffice.'、我是測試系統刑事警察大隊、臺北市政府毒品危害防制中心</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統 違反毒品危害防制條例案件處分書</h3></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">'.$send_str.'</td>
                                            </tr>
                                            <tr>
												<td colspan="8" width="680">依據：'.$s_roffice .(date('Y', strtotime($susp->s_fdate))-1911) .'年' . date('m', strtotime($susp->s_fdate))*1 .'月' . date('d', strtotime($susp->s_fdate))*1 . '日' . ((mb_strlen($susp->s_roffice1) < 4)?'北市警'.$susp->s_roffice1.'分刑' :$susp->s_roffice1) . '字第' . $susp->s_fno .'
												號</td>
                                            </tr>
                                            <tr>
												<td rowspan="4" width="77" height="70" style="line-height:70px;">
													<h3 align="center" >受處分人</h3>
												</td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
												<td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
                                                <td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.((isset($susp->s_prison) && !empty($susp->s_prison))?explode(':', $susp->s_prison)[1] .'('.explode(':', $susp->s_prison)[0].')' :$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress) . (($susp->s_live_state == 2)?'（指定送達現住地）':'') .'</strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>';
					list($year,$month,$day) = explode("-",$susp->s_birth);
					$year_diff = date("Y") - $year;
					$month_diff = date("m") - $month;
					$day_diff  = date("d") - $day;
					if ($day_diff < 0 || $month_diff < 0)
						$year_diff--;
					$html .=                '<tr>
												<td rowspan="4" width="77" height="70" style="line-height:20px;"><h3 align="center"><br>法定 代理人</h3></td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && $year_diff < 20)?$suspfadai->s_fadai_name:'').'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && $year_diff < 20)?$suspfadai->s_fadai_gender:'').'</strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && $year_diff < 20)?'民國'.(date('Y', strtotime($suspfadai->s_fadai_birth))-1911) .'年'.(date('m', strtotime($suspfadai->s_fadai_birth))) .'月' .(date('d', strtotime($suspfadai->s_fadai_birth))) .'日':'').'</strong></td>
                                            </tr>
                                            <tr>
												<td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && $year_diff < 20)?$suspfadai->s_fadai_ic:'').'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && $year_diff < 20)?$suspfadai->s_fadai_phone:'').'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
												<td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="445" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.((isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && $year_diff < 20)?$suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address:'').'</strong></td>
                                            </tr>
                                            <tr>
												<td width="77" height="70" style="line-height:70px;"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">';
													$ch_j = 0;
													$has_ch = array();
													for ($i=0; $i < count($has_ary); $i++) { 
														if($has_ary[$i])
															array_push($has_ch, $count_has_ch[$ch_j++]);
														else
															array_push($has_ch, '');
													}
													if($ch_j > 1)
														$html .= '受處分人處罰：<br/>';
		
													if($has_ary[0]){
														$html .=(($ch_j > 1)?$has_ch[0]:'受處分人處罰：') . '新臺幣' . ($fine->f_damount/10000) . '萬元整。'.((isset($sp->fd_times) && $sp->fd_times > 1)?'( '.(date('Y')-1911).'年第'.$sp->fd_times.'次裁罰 )':'').'<br/>';
													}
		
													if($has_ary[1]){
														$html .=(($ch_j > 1)?$has_ch[1]:'受處分人處罰：') . '毒品危害6小時。(講習時間詳見下方「毒品講習」欄位)<br/>';
													}
		
													if($has_ary[2]){
														$patterns = array('1級','2級','3級','4級');
														$replacements = array('一級','二級','三級','四級');		

														$html .=(($ch_j > 1)?$has_ch[2]:'受處分人處罰：')  . str_replace($patterns, $replacements, $sp->fd_drug_msg) . '沒入。';
													}
					$html .=                    '</td>
                                            </tr>
                                            <tr>
												<td width="77" height="75" style="line-height:75px;"><h3 align="center">事實</h3></td>';
							$s_date = $this->tranfer2RCyear($cases->s_date);
							$s_time = $this->tranfer2RChour($cases->s_time);   
							$courser_str = '';
							$fd_fact_text = '';
							if(isset($susp->s_prison) && !empty($susp->s_prison))
							{
								$courser_str = '請受處分人出所後，主動聯繫臺北市政府毒品危害防制中心。';
							}
							else
							{
								if(isset($sp->fd_lec_date) && !empty($sp->fd_lec_date) && $sp->fd_other_lec == null){
									$md_str = explode("(", $sp->fd_lec_date)[0];
									$week_str = explode("(", $sp->fd_lec_date)[1];
				
									$courser_str = "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日（". $week_str . '，時間：' . explode('-', $sp->fd_lec_time)[0] . "（請攜帶有相片之證件報到，逾時將無法入場），地點：".preg_replace('/\s(?=)/', '', $sp->fd_lec_place) ."（" . $sp->fd_lec_address . "）";
								}
								else
								{
									$courser_str = '由臺北市政府毒品危害防制中心另行通知。';
								}
							}

							if(isset($sp->fd_backlog))
							{
								$fd_backlog = explode('_', $sp->fd_backlog);
								$fd_backlog_str = '本局'.$this->tranfer2ADyearCh($fd_backlog[0]) .'日北市警刑毒緝字第' . $fd_backlog[1] .'號處分書(列管編號'.$sp->fd_num.')作廢';
							}
							else
							{
								$fd_backlog_str = '';
							}
							
							
							if(!isset($sp->fd_fact_text)||($sp->fd_fact_text==null)||($sp->fd_fact_text=='')){
								$fd_fact_text = $susp->s_name. $s_date . $s_time.'許，在本市'.$cases->s_place.'，為'.$cases->s_office.'員警查獲持有'.$sp->fd_drug_msg.'，經採集毒品及尿液檢體送專業單位鑑驗，均呈第'.((isset($drug2))?$drug2->ddc_level:'').'級毒品「'.((isset($drug2))?$drug2->ddc_ingredient:'').'」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg . $fd_backlog_str;
							}
							else
							{
								$fd_fact_text = $sp->fd_fact_text . $fd_backlog_str;
							}
							$reason_1= '';
							$reason_2= '';
							if($has_ary[0] || $has_ary[1])$reason_1 = '■'; else $reason_1 = '□';
							if($has_ary[2])$reason_2 = '■'; else $reason_2 = '□';
					$html .=                          '<td colspan="7" width="603">受處分人'.$fd_fact_text .'</td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:15px;"><h3 align="center"><br>理由及 法令依據</h3></td>
                                                <td colspan="7" width="603">'.$reason_1.'依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                        <br>'.$reason_2.'依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                </td>
                                            </tr>';
					if($has_ary[0]) {
					$html .=                '<tr>
												<td width="77" height="100" style="line-height:15px;"><h3 align="center"><br><br>繳納期限 及方式</h3></td>
                                                <td colspan="7" width="603">一、罰鍰限於<strong>民國'.(($fine->f_date != '0000-00-00')?(date('Y', strtotime($fine->f_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))):'Ｏ年Ｏ月Ｏ') .'日</strong>前選擇下列方式之一繳款：
                                                            (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                            (二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong>
                                                            (三)輸入本案虛擬帳號：<strong>'.$fine->f_BVC.'</strong>
                                                            三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。
                                                </td>
                                            </tr>';
					}
					if($has_ary[1]) {
					$html .=				'<tr>
												<td width="77" height="80" style="line-height:80px;"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">一、可選擇參與實體講習或線上課程(每年可參與三次，惟應以申請不同套裝課程)。<br>二、講習時間、地點： '.$courser_str.'如要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。<br>三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。                                                </td>
                                            </tr>';
					}						
					$html .=                '<tr>
												<td width="77" height="50" style="line-height:50px;"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：'.$sp->fd_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table>
                                    <tr>
                                    <td height="264"></td>   
                                    </tr>            
                                    </table> 
                                </div>';
				$pdf->writeHTML($html, true, false, true, false, ''); 
            }
			// echo $html;
			// exit;
                                  
        $pdf->Output($project.'.pdf', 'I');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function surc_doc()//產生怠金處分書郵遞pdf
    {
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        // $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            $pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5.5, 3.5, 5.5,true);
            // set some language-dependent strings (optional)
            $pdf->setFontSubsetting(true);
            $pdf->SetFont('twkai98_1','',10);
            // $pdf->SetFont('msungstdlight', '', 10);
            $pdf->AddPage();
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // $pdf->SetAutoPageBreak(TRUE, 0);
			
                  
            // foreach ($surc->result() as $suspect)
            // {
                // $id = $suspect->s_num;
                // $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                // $susp=$susp[0];
                // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                // $fine=$fine[0];
                // $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                // $sp1=$this->getsqlmod->get3SP1($id)->result();
                // $sp=$sp1[0];
                // $spcount=$this->getsqlmod->getfdAll()->result();
                // $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                // $cases=$cases[0];
                // $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                // $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                // $today['day']   = date('d');
                // $today['month'] = date('m');
                // $today['year']  = date('Y') - 20;
                // $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                //     $fname = NULL;
                //     $fgender = NULL;
                //     $fic = NULL;
                //     $fbirth = NULL;
                //     $fphone = NULL;
                //     $fzipcode = NULL;
                //     $faddress = NULL;
                // if(isset($suspfadai[0])){
                //     $suspfadai = $suspfadai[0];
                //     $stampBirth = strtotime($susp->s_birth);
                //     if ($stampBirth > $stampToday) {
                //         $fname =  $suspfadai->s_fadai_name;
                //         $fgender = $suspfadai->s_fadai_gender;
                //         $fic = $suspfadai->s_fadai_ic;
                //         $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                //         $fphone = $suspfadai->s_fadai_phone;
                //         $fzipcode = $suspfadai->s_fadai_zipcode;
                //         $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                //     }else{
                //         $fname = NULL;
                //         $fgender = NULL;
                //         $fic = NULL;
                //         $fbirth = NULL;
                //         $fphone = NULL;
                //         $fzipcode = NULL;
                //         $faddress = NULL;
                //     }
                // }
                // if(!isset($phone))$phone1=null;
                // if(isset($phone))$phone1=$phone[0]->p_no;
                // $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                // $drug=$drug[0];
                // $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                // $drug2=$drug2[0];
                // $prison=$this->getsqlmod->getprison()->result();
                // $prison=$prison[0];
				$id = $this->uri->segment(3);
				$pjid = $this->uri->segment(4);
				// $fd1 = $this->getsqlmod->getFD('fd_snum',$id)->result();
				// $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
				$surc=$this->getsqlmod->get1Sur_ed('surc_num',$id)->result()[0];
				$DpOrder=$this->getsqlmod->get1SurOrder('surc_projectnum',$surc->surc_projectnum)->result();
				// $sp=$this->getsqlmod->get3SP1($id)->result();
				// $spcount=$this->getsqlmod->getsurcAll()->result();
				// $cases=$this->getsqlmod->getCases($susp[0]->s_cnum)->result();
				// $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp[0]->s_dp_project)->result();
				// $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
				// $susp1 = $this->getsqlmod->get2Susp($susp[0]->s_num)->result(); // 抓snum
				
				$suspfadai = $this->getsqlmod->get1SuspFadai($surc->surc_sic)->result();

				$prison=$this->getsqlmod->getWordlibPrison()->result();
				$courses=$this->getsqlmod->getCourseData()->result();

				$sp = $this->getsqlmod->getSP1($pjid)->result();
				$target_str = '';
				$send_str = '';
				if(null != $surc && count(explode(",", $surc->surc_address)) > 1)
				{
					$fd_name = explode(",", $surc->surc_target)[0];
					$fd_addr = explode(",", $surc->surc_address)[0];
					$fd_zip = explode(",", $surc->surc_zipcode)[0];
					// echo '<mark>'. $fd_name.'</mark> 君 <br> <span style="margin-left: 3em;">'. $fd_zip . ' ' . $fd_addr.'</span>';
					// echo '<br/>';
					$fd_name1 = explode(",", $surc->surc_target)[1];
					$fd_addr1 = explode(",", $$surc->surc_address)[1];
					$fd_zip1 = explode(",", $surc->surc_zipcode)[1];
					$target_str .= $fd_name.'君(受處分人'.$fd_name1.'之法定代理人)  <br> <span>'. $fd_zip1 . ' ' . $fd_addr1.'</span>';
					if($surc->surc_live_state == 2)
					{
						$target_str .= '（指定送達現住地）';
					}
				}
				else
				{
					$target_str = $surc->surc_target.' 君 <br><span>'. ((!empty($surc->surc_prison))?explode(':', $surc->surc_prison)[1] .'('.explode(':', $surc->surc_prison)[0].')' :$surc->surc_zipcode . ' ' . $surc->surc_address) .'</span>';
					if($surc->surc_live_state == 2)
					{
						$target_str .= '（指定送達現住地）';
					}
				}
				if($surc->surc_date != '0000-00-00' && isset($surc->surc_date))
				{
					$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.(date('m', strtotime($surc->surc_date))*1) .'月' . (date('d', strtotime($surc->surc_date))*1) .'日' .'北市警刑毒緝字第'.$surc->surc_send_num .'號';
				}else{
					if(isset($sp->sp_send_date))
					{ 
						if($sp->sp_send_date !=  '0000-00-00')
						{
							$send_str = '發文日期及文號：'.((int)substr($sp->sp_send_date, 0, 4)- 1911) . '年' . ((int)substr($sp->sp_send_date, 5, 2)*1) . '月' . ((int)substr($sp->sp_send_date, 8, 2)*1) . '日'.'北市警刑毒緝字第'.$surc->surc_send_num.'號';
						}
						else
						{
							$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.str_pad((date('m')*1),2,'0',STR_PAD_LEFT).'月  日北市警刑毒緝字第'.$surc->surc_send_num.'號';
						}
						
					
					}else{
						$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.str_pad((date('m')*1),2,'0',STR_PAD_LEFT).'月  日北市警刑毒緝字第'.$surc->surc_send_num.'號';
					}
				} 
				$html=''; 
                $html='
                                <div style="width:680px" class="panel-body">
                                    <table width="680">
                                        <tr>
											<td colspan="3">
												<h2><b>列管編號：'.$surc->surc_no .'</b></h2> 
											</td>
											<td colspan="5">
												<h2>原處分管制編號：'.$surc->surc_listed_no .'</h2>
											</td>
										</tr>
										<tr>
											<td colspan="8"><h2>正本：'.$target_str.'</h2></td>
										</tr>
										<tr>
											<td colspan="8"><h3>副本：我是測試系統刑事警察大隊毒品查緝中心<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;臺北市政府毒品危害防制中心</h3></td>
										</tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
											<tr>
                                                <td colspan="8" width="680">
													<h1 align="center" style="margin-bottom:0px;">我是測試系統違反毒品危害防制條例案件（怠金）處分書</h1>
													<h3 align="right">'.$send_str.'</h3>
												</td>
                                            </tr>
											<tr>
                                                <td colspan="1" width="77">
													<h3 align="center">依據</h3>
												</td>
												<td colspan="7" width="603">
												臺北市政府衛生局'.(date('Y', strtotime($surc->surc_basenumdate))-1911) .'年'. (date('m', strtotime($surc->surc_basenumdate))*1) .'月' . (date('d', strtotime($surc->surc_basenumdate))*1).'日北市衛醫傳防字第'.$surc->surc_basenum.'號
												</td>
                                            </tr>  
                                            <tr>
                                                <td rowspan="4" width="77" height="70" style="line-height:70px;">
													<h3 align="center" >受處分人</h3>
												</td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$surc->surc_name.'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="70" height="30" style="line-height:15px;">出生<br>年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;">
													<strong>民國'.((isset($susp))?((date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth)))):'' ).'日</strong>
												</td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:15px;letter-spacing:5px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$surc->surc_sic.'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及【聯絡電話】</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.((isset($phone1))?$phone1:'').'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="20" height="40" align="center" style="line-height:20px;">地址</td>
                                                <td width="50" align="center" height="20" style="line-height:20px;">現住地</td>
                                                <td colspan="5" width="533" height="20" style="line-height:20px;"><strong>'.((isset($surc->surc_prison) && !empty($surc->surc_prison))?explode(':', $surc->surc_prison)[1] .'('.explode(':', $surc->surc_prison)[0].')' : $surc->surc_address) . (($surc->surc_live_state == 2)?'（指定送達現住地）':'') .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="533" height="20" style="line-height:20px;" ><strong>'.$surc->surc_address.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77" height="70" style="line-height:20px;"><h3 align="center"><br>法定 代理人</h3></td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="70" height="30" style="line-height:15px;">出生<br>年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong></strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center"  width="88" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及【聯絡電話】</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="20" height="40" align="center" style="line-height:20px;">地址</td>
												<td width="50" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td width="50" align="center"  height="20" style="line-height:20px;">戶籍地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="77" height="60" style="line-height:60px;"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">一、受處分人處罰：怠金新台幣<strong>'.(number_format($surc->surc_amount)).'</strong>元整。<br>二、仍須參加講習，講習時地詳見詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續處以怠金。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77" height="60" style="line-height:60px;"><h3 align="center">事實</h3></td>
                                                <td colspan="7" width="603">緣受處分人'.$surc->surc_target.'涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於'.$this->tranfer2RCyear2($surc->surc_olec_date).'至ＯＯＯ參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。</td>
                                            </tr>
                                            <tr>
                                                <td width="77" height="50" style="line-height:20px;"><h3 align="center"><br>理由及 法令依據</h3></td>
                                                <td colspan="7" width="603">■一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習&nbsp;&nbsp;&nbsp;&nbsp;者，依行政執行法規定處以怠金。<br>■二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不&nbsp;&nbsp;&nbsp;&nbsp;能由他人代為履行者，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。<br>■三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續&nbsp;&nbsp;&nbsp;&nbsp;處以怠金。<br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77" height="90" style="line-height:20px;"><h3 align="center"><br><br>繳納期限 及方式</h3></td>
                                                <td colspan="7" width="603">一、怠金限於<strong>民國'.(($surc->surc_findate != '0000-00-00')?(date('Y', strtotime($surc->surc_findate))-1911) .'年'.(date('m', strtotime($surc->surc_findate))) .'月'.(date('d', strtotime($surc->surc_findate))) .'日':'Ｏ年Ｏ月Ｏ日').'</strong>前選擇下列方式之一繳款，逾期不繳納依法移送行政執行處執行：<br>&nbsp;&nbsp;&nbsp;&nbsp;(一)以自動化設備（ATM、網路ATM、網路銀行）匯款至「虛擬帳號」（限本處分書）。 <br/>&nbsp;&nbsp;&nbsp;&nbsp;(二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>二、自動化設備匯款方式：（一）選擇【繳費】服務 （二）輸入轉入銀行代號：<strong>012</strong><br>&nbsp;&nbsp;&nbsp;&nbsp;(三)輸入本案虛擬帳號：<strong>'.$surc->surc_BVC.'</strong>。（匯款手續費由繳款人負擔）<br/>三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361021</strong>，戶名:<strong>&nbsp;&nbsp;&nbsp;&nbsp;我是測試系統刑事警察大隊</strong>。<br>四、匯款繳納時，請於匯款單上之匯款人欄填寫受處分人之姓名「'.$surc->surc_name.'」，並應要求於備註(附言)欄填入案&nbsp;&nbsp;&nbsp;&nbsp;件列管編號「'.$surc->surc_no.'」及身分證統一編號「'.$surc->surc_sic.'」，俾利辦理銷案。
                                                </td>
                                            </tr>';
				$courser_str = '';
				if(isset($surc->surc_prison) && !empty($surc->surc_prison))
				{
					$courser_str = '請受處分人出監後，主動聯繫臺北市政府毒品危害防制中心。';
				} else {
					if(isset($surc->surc_lec_date) && !empty($surc->surc_lec_datee) && $surc->surc_other_lec == null){
						
						$md_str = explode("(", $surc->surc_lec_date)[0];
						$week_str = explode("(", $surc->surc_lec_date)[1];

						$lec_time = tranfer2RChour(explode('-', $surc->surc_lec_time)[0]);

						$courser_str = "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日"  . $lec_time . '（請攜帶有相片之證件報到，逾時將無法入場），地點：'.$surc->surc_lec_place .'(' . $surc->surc_lec_address . ')'.'，請配合防疫相關措施。';
					}else{
						$courser_str = '由臺北市政府毒品危害防制中心另行通知。';
					}
				}
				$html .=                   '<tr>
                                                <td width="77" height="60" style="line-height:60px;"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">一、可選擇參與實體講習或線上課程（線上課程每年可參與三次，惟應以不同套裝課程申請認定）。<br/>二、講習時間：'.$courser_str.'有正當理由無法參加或要選擇線上課程者，請至下&nbsp;&nbsp;&nbsp;&nbsp;列網址：https://nodrug.gov.taipei下載異動申請表或依指示完成線上課程。講習相關問題，請逕向臺&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;北市政府毒品危害防制中心洽詢，電話(02)2375-4068。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77" height="60" style="line-height:60px;"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">一、不服本處分者，義務人或利害關係人對執行命令、執行方法、應遵守之程序或其他侵害利益之情事，得於&nbsp;&nbsp;&nbsp;&nbsp;執行程序終結前，向執行機關聲明異議。執行機關認其聲明異議有理由者，應即停止執行，並撤銷或更正&nbsp;&nbsp;&nbsp;&nbsp;之；認其無理由者，應於十日內加具意見，送直接上級主管機關於三十日內決定之。<br/>二、本件處分書承辦人：'.$surc->surc_empno.'，聯絡電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                </div>';
            // }
			// echo $html;
            $pdf->writeHTML($html, true, false, false, false, '');                        
        // $pdf->Output($sp->fd_num .'.pdf', 'D');    
        $pdf->Output($surc->surc_no .'.pdf', 'I');    
    }
	public function surc_doc_project()//產生怠金處分書郵遞pdf
    {
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
		$this->load->library('zip');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
		// $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf->SetCreator(PDF_CREATOR);
		// $pdf->setPrintHeader(false); //不要頁首
		// // $pdf->setPrintFooter(false); //不要頁尾
		// // 版面配置 > 邊界
		// //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		// $pdf->SetMargins(5.5, 3.5, 10,true);
		// // set some language-dependent strings (optional)
		// $pdf->setFontSubsetting(true);
		// $pdf->SetFont('twkai98_1','',10);
		// // $pdf->SetFont('msungstdlight', '', 10);
		// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		// $pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		// $pdf->SetAutoPageBreak(TRUE, 0);
		// $pdf->SetFont('twkai98_1','',10);
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // $pdf->SetAutoPageBreak(TRUE, 0);
			
            $html='';       
            foreach ($query->result() as $surc)
            {
				$pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->setPrintHeader(false); //不要頁首
				$pdf->setPrintFooter(false); //不要頁尾
				// 版面配置 > 邊界
				//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				$pdf->SetMargins(10, 1.5, 70.5,true);
				// set some language-dependent strings (optional)
				$pdf->setFontSubsetting(true);
				$pdf->SetFont('twkai98_1','',10);
				// $pdf->SetFont('msungstdlight', '', 10);
				$pdf->AddPage();
				// $pdf->AddPage('P', 'A4');
                // $id = $suspect->s_num;
                // $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                // $susp=$susp[0];
                // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                // $fine=$fine[0];
                // $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                // $sp1=$this->getsqlmod->get3SP1($id)->result();
                // $sp=$sp1[0];
                // $spcount=$this->getsqlmod->getfdAll()->result();
                // $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                // $cases=$cases[0];
                // $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                // $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                // $today['day']   = date('d');
                // $today['month'] = date('m');
                // $today['year']  = date('Y') - 20;
                // $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                //     $fname = NULL;
                //     $fgender = NULL;
                //     $fic = NULL;
                //     $fbirth = NULL;
                //     $fphone = NULL;
                //     $fzipcode = NULL;
                //     $faddress = NULL;
                // if(isset($suspfadai[0])){
                //     $suspfadai = $suspfadai[0];
                //     $stampBirth = strtotime($susp->s_birth);
                //     if ($stampBirth > $stampToday) {
                //         $fname =  $suspfadai->s_fadai_name;
                //         $fgender = $suspfadai->s_fadai_gender;
                //         $fic = $suspfadai->s_fadai_ic;
                //         $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                //         $fphone = $suspfadai->s_fadai_phone;
                //         $fzipcode = $suspfadai->s_fadai_zipcode;
                //         $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                //     }else{
                //         $fname = NULL;
                //         $fgender = NULL;
                //         $fic = NULL;
                //         $fbirth = NULL;
                //         $fphone = NULL;
                //         $fzipcode = NULL;
                //         $faddress = NULL;
                //     }
                // }
                // if(!isset($phone))$phone1=null;
                // if(isset($phone))$phone1=$phone[0]->p_no;
                // $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                // $drug=$drug[0];
                // $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                // $drug2=$drug2[0];
                // $prison=$this->getsqlmod->getprison()->result();
                // $prison=$prison[0];
				$id = $this->uri->segment(3);
				$pjid = $this->uri->segment(3);
				// $fd1 = $this->getsqlmod->getFD('fd_snum',$id)->result();
				// $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
				// $surc=$this->getsqlmod->get1Sur_ed('surc_num',$id)->result();
				$DpOrder=$this->getsqlmod->get1SurOrder('surc_projectnum',$surc->surc_projectnum)->result();
				// $sp=$this->getsqlmod->get3SP1($id)->result();
				// $spcount=$this->getsqlmod->getsurcAll()->result();
				// $cases=$this->getsqlmod->getCases($susp[0]->s_cnum)->result();
				// $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp[0]->s_dp_project)->result();
				// $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
				// $susp1 = $this->getsqlmod->get2Susp($susp[0]->s_num)->result(); // 抓snum
				
				$suspfadai = $this->getsqlmod->get1SuspFadai($surc->surc_sic)->result();

				$prison=$this->getsqlmod->getWordlibPrison()->result();
				$courses=$this->getsqlmod->getCourseData()->result();

				$sp = $this->getsqlmod->getSP1($pjid)->result();
				$target_str = '';
				$send_str = '';
				if(null != $surc && count(explode(",", $surc->surc_address)) > 1)
				{
					$fd_name = explode(",", $surc->surc_target)[0];
					$fd_addr = explode(",", $surc->surc_address)[0];
					$fd_zip = explode(",", $surc->surc_zipcode)[0];
					// echo '<mark>'. $fd_name.'</mark> 君 <br> <span style="margin-left: 3em;">'. $fd_zip . ' ' . $fd_addr.'</span>';
					// echo '<br/>';
					$fd_name1 = explode(",", $surc->surc_target)[1];
					$fd_addr1 = explode(",", $$surc->surc_address)[1];
					$fd_zip1 = explode(",", $surc->surc_zipcode)[1];
					$target_str .= $fd_name.'君(受處分人'.$fd_name1.'之法定代理人)  <span>'. $fd_zip1 . ' ' . $fd_addr1.'</span>';
					if($surc->surc_live_state == 2)
					{
						$target_str .= '（指定送達現住地）';
					}
				}
				else
				{
					$target_str = $surc->surc_target.' 君 <span>'. ((!empty($surc->surc_prison))?explode(':', $surc->surc_prison)[0] .'('.explode(':', $surc->surc_prison)[1].')' :$surc->surc_zipcode . ' ' . $surc->surc_address) .'</span>';
					if($surc->surc_live_state == 2)
					{
						$target_str .= '（指定送達現住地）';
					}
				}
				if($surc->surc_date != '0000-00-00' && isset($surc->surc_date))
				{
					$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.(date('m', strtotime($surc->surc_date))*1) .'月' . (date('d', strtotime($surc->surc_date))*1) .'日' .'北市警刑毒緝字第'.$surc->surc_send_num .'號';
				}else{
					if(isset($sp->sp_send_date))
					{ 
						if($sp->sp_send_date !=  '0000-00-00')
						{
							$send_str = '發文日期及文號：'.((int)substr($sp->sp_send_date, 0, 4)- 1911) . '年' . ((int)substr($sp->sp_send_date, 5, 2)*1) . '月' . ((int)substr($sp->sp_send_date, 8, 2)*1) . '日'.'北市警刑毒緝字第'.$surc->surc_send_num.'號';
						}
						else
						{
							$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.str_pad((date('m')*1),2,'0',STR_PAD_LEFT).'月  日北市警刑毒緝字第'.$surc->surc_send_num.'號';
						}
						
					
					}else{
						$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.str_pad((date('m')*1),2,'0',STR_PAD_LEFT).'月  日北市警刑毒緝字第'.$surc->surc_send_num.'號';
					}
				} 
				
                $html='
                                <div style="width:680px" class="panel-body">
                                    <table width="680">
										<tr>
											<td>以稿代簽</td>
											<td>第二層決行</td>
											<td>限制開放</td>
											<td colspan="5">檔號：1198/07270399/1 保存年限：3年</td>
										</tr>
                                        <tr>
											<td colspan="3">
												<h3>案件列管編號：'.$surc->surc_no .'</h3> 
											</td>
											<td colspan="5">
												<h3>原處分管制編號：'.$surc->surc_listed_no .'</h3>
											</td>
										</tr>
										<tr>
											<td colspan="8"><h3>正本：'.$target_str.'</h3></td>
										</tr>
										<tr>
											<td colspan="8"><h3>副本：我是測試系統刑事警察大隊毒品查緝中心、臺北市政府毒品危害防制中心</h3></td>
										</tr>
										<tr>
											<td colspan="3"><h3>校對：</h3></td>
											<td colspan="3"><h3>監印：</h3></td>
											<td colspan="2"><h3>發文：</h3></td>
                                        </tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
											<tr>
                                                <td colspan="8" width="680">
													<h1 align="center">我是測試系統違反毒品危害防制條例案件（怠金）處分書(稿)</h1>
													<h3 align="right">'.$send_str.'</h3>
												</td>
                                            </tr>
											<tr>
                                                <td colspan="1" width="77">
													<h3 align="center">依據</h3>
												</td>
												<td colspan="7" width="603">
												臺北市政府衛生局'.(date('Y', strtotime($surc->surc_basenumdate))-1911) .'年'. (date('m', strtotime($surc->surc_basenumdate))*1) .'月' . (date('d', strtotime($surc->surc_basenumdate))*1).'日北市衛醫傳防字第'.$surc->surc_basenum.'號
												</td>
                                            </tr>  
                                            <tr>
                                                <td rowspan="4" width="77" height="70" style="line-height:70px;">
													<h3 align="center">受處分人</h3>
												</td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$surc->surc_name.'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生<br>年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;">
													<strong>民國'.((isset($susp))?((date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth)))):'' ).'日</strong>
												</td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" align="center" width="88" height="30" style="line-height:30px;"><strong>'.$surc->surc_sic.'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及【聯絡電話】</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.((isset($phone1))?$phone1:'').'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="20" height="40" align="center" style="line-height:20px;">地址</td>
												<td width="50" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong>'.((isset($surc->surc_prison) && !empty($surc->surc_prison))?explode(':', $surc->surc_prison)[1] .'('.explode(':', $surc->surc_prison)[0].')' : $surc->surc_address) . (($surc->surc_live_state == 2)?'（指定送達現住地）':'') .'</strong></td>
                                            </tr>
                                            <tr>
												<td width="50" align="center"  height="20" style="line-height:20px;">戶籍地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong>'.$surc->surc_address.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77" height="70" style="line-height:20px;"><h3 align="center"><br>法定 代理人</h3></td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生<br>年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong></strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及【聯絡電話】</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="20" height="40" align="center" style="line-height:20px;">地址</td>
												<td width="50" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td width="50" align="center"  height="20" style="line-height:20px;">戶籍地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="77" height="60" style="line-height:60px;"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>一、怠金新台幣<strong>'.(number_format($surc->surc_amount)).'</strong>元整。<br>二、仍須參加講習，講習時地詳見詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續處以怠金。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77" height="60" style="line-height:60px;"><h3 align="center">事實</h3></td>
                                                <td colspan="7" width="603">緣受處分人'.$surc->surc_target.'涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於'.$this->tranfer2RCyear2($surc->surc_olec_date).'至ＯＯＯ參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。</td>
                                            </tr>
                                            <tr>
												<td width="77" height="50" style="line-height:20px;"><h3 align="center"><br>理由及 法令依據</h3></td>
                                                <td colspan="7" width="603">■一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習&nbsp;&nbsp;&nbsp;&nbsp;者，依行政執行法規定處以怠金。<br>■二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不&nbsp;&nbsp;&nbsp;&nbsp;能由他人代為履行者，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。<br>■三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續&nbsp;&nbsp;&nbsp;&nbsp;處以怠金。<br>
                                                </td>
                                            </tr>
                                            <tr>
												<td width="77" height="90" style="line-height:20px;"><h3 align="center"><br><br>繳納期限 及方式</h3></td>
                                                <td colspan="7" width="603">一、怠金限於<strong>民國'.(($surc->surc_findate != '0000-00-00')?(date('Y', strtotime($surc->surc_findate))-1911) .'年'.(date('m', strtotime($surc->surc_findate))) .'月'.(date('d', strtotime($surc->surc_findate))) .'日':'Ｏ年Ｏ月Ｏ日').'</strong>前選擇下列方式之一繳款，逾期不繳納依法移送行政執行處執行：<br>&nbsp;&nbsp;&nbsp;&nbsp;(一)以自動化設備（ATM、網路ATM、網路銀行）匯款至「虛擬帳號」（限本處分書）。 <br/>&nbsp;&nbsp;&nbsp;&nbsp;(二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>二、自動化設備匯款方式：（一）選擇【繳費】服務 （二）輸入轉入銀行代號：<strong>012</strong><br>&nbsp;&nbsp;&nbsp;&nbsp;(三)輸入本案虛擬帳號：<strong>'.$surc->surc_BVC.'</strong>。（匯款手續費由繳款人負擔）<br/>三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361021</strong>，戶名:<strong>&nbsp;&nbsp;&nbsp;&nbsp;我是測試系統刑事警察大隊</strong>。<br>四、匯款繳納時，請於匯款單上之匯款人欄填寫受處分人之姓名「'.$surc->surc_name.'」，並應要求於備註(附言)欄填入案&nbsp;&nbsp;&nbsp;&nbsp;件列管編號「'.$surc->surc_no.'」及身分證統一編號「'.$surc->surc_sic.'」，俾利辦理銷案。
                                                </td>
                                            </tr>';
				$courser_str = '';
				if(isset($surc->surc_prison) && !empty($surc->surc_prison))
				{
					$courser_str = '請受處分人出監後，主動聯繫臺北市政府毒品危害防制中心。';
				} else {
					if(isset($surc->surc_lec_date) && !empty($surc->surc_lec_datee) && $surc->surc_other_lec == null){
						
						$md_str = explode("(", $surc->surc_lec_date)[0];
						$week_str = explode("(", $surc->surc_lec_date)[1];

						$lec_time = tranfer2RChour(explode('-', $surc->surc_lec_time)[0]);

						$courser_str = "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日"  . $lec_time . '（請攜帶有相片之證件報到，逾時將無法入場），地點：'.$surc->surc_lec_place .'(' . $surc->surc_lec_address . ')'.'，請配合防疫相關措施。';
					}else{
						$courser_str = '由臺北市政府毒品危害防制中心另行通知。';
					}
				}
				$html .=                   '<tr>
												<td width="77" height="60" style="line-height:60px;"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">一、可選擇參與實體講習或線上課程（線上課程每年可參與三次，惟應以不同套裝課程申請認定）。<br/>二、講習時間：'.$courser_str.'有正當理由無法參加或要選擇線上課程者，請至下&nbsp;&nbsp;&nbsp;&nbsp;列網址：https://nodrug.gov.taipei下載異動申請表或依指示完成線上課程。講習相關問題，請逕向臺&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;北市政府毒品危害防制中心洽詢，電話(02)2375-4068。
                                                </td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:60px;"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">一、不服本處分者，義務人或利害關係人對執行命令、執行方法、應遵守之程序或其他侵害利益之情事，得於&nbsp;&nbsp;&nbsp;&nbsp;執行程序終結前，向執行機關聲明異議。執行機關認其聲明異議有理由者，應即停止執行，並撤銷或更正&nbsp;&nbsp;&nbsp;&nbsp;之；認其無理由者，應於十日內加具意見，送直接上級主管機關於三十日內決定之。<br/>二、本件處分書承辦人：'.$surc->surc_empno.'，聯絡電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
										<table width="680">
											<tr>
												<th>承辦單位(2539)</th>
												<th>審核</th>
												<th>決行</th>
											</tr>
										</table>
                                </div>';
								$pdf->writeHTML($html, true, false, true, false, '');  
								//$surc->surc_send_num
								$pdf->lastPage();
								// $pdf->Output(getcwd().'\\drugdoc\\'.$sp->fd_send_num . '.pdf', 'F');
								

								// $path = getcwd().'\\drugdoc\\'.$sp->fd_send_num . '.pdf';
								// $this->zip->read_file($path);
								$pdf->Output(dirname(__FILE__) .'\\'.$surc->surc_send_num . '.pdf', 'F');
								$files = dirname(__FILE__) .'\\'.$surc->surc_send_num . '.pdf';
								// $this->zip->add_data($sp->fd_send_num . '.pdf', $files);
								$this->zip->read_file($files);
								$pdf->Close();
								unlink(dirname(__FILE__) .'\\'.$surc->surc_send_num . '.pdf');
            }
			$j = 0;
			foreach ($query->result() as $surc)
            {
				$id = $this->uri->segment(3);
				$pjid = $this->uri->segment(3);
				// $fd1 = $this->getsqlmod->getFD('fd_snum',$id)->result();
				// $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
				// $surc=$this->getsqlmod->get1Sur_ed('surc_num',$id)->result();
				$DpOrder=$this->getsqlmod->get1SurOrder('surc_projectnum',$surc->surc_projectnum)->result();
				// $sp=$this->getsqlmod->get3SP1($id)->result();
				// $spcount=$this->getsqlmod->getsurcAll()->result();
				// $cases=$this->getsqlmod->getCases($susp[0]->s_cnum)->result();
				// $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp[0]->s_dp_project)->result();
				// $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
				// $susp1 = $this->getsqlmod->get2Susp($susp[0]->s_num)->result(); // 抓snum
				
				$suspfadai = $this->getsqlmod->get1SuspFadai($surc->surc_sic)->result();

				$prison=$this->getsqlmod->getWordlibPrison()->result();
				$courses=$this->getsqlmod->getCourseData()->result();

				$sp = $this->getsqlmod->getSP1($pjid)->result();
				$dom = new DOMDocument("1.0","utf-8"); 
				$implementation = new DOMImplementation();
				$dom->appendChild($implementation->createDocumentType('簽 SYSTEM "104_5_utf8.dtd" [<!ENTITY 表單 SYSTEM "簽.sw" NDATA DI><!NOTATION DI SYSTEM ""><!NOTATION _X SYSTEM "">]')); 
				// display document in browser as plain text 
				// for readability purposes 
				header("Content-Type: text/plain"); 
				// create root element 
				$root = $dom->createElement("簽"); 
				$dom->appendChild($root); 
				// create child element 
				 
				$item = $dom->createElement("檔號"); 
				$root->appendChild($item); 
				
				$item2 = $dom->createElement("年度"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode(date('Y')-1911); 
				$item2->appendChild($text); 
				$item2 = $dom->createElement("分類號"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("07270399");
				// $text = $dom->createTextNode(""); 
				$item2->appendChild($text); 
				$item2 = $dom->createElement("案次號"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("1"); 
				$item2->appendChild($text); 
				 
				$item = $dom->createElement("保存年限"); 
				$root->appendChild($item); 
				$text = $dom->createTextNode("3"); 
				$item->appendChild($text); 
			
				$item = $dom->createElement("稿面註記"); 
				$root->appendChild($item); 
				$item2 = $dom->createElement("擬辦方式"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("以稿代簽"); 
				$item2->appendChild($text); 
				$item2 = $dom->createElement("決行層級"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("二層決行"); 
				$item2->appendChild($text); 
				$item2 = $dom->createElement("應用限制"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("限制開放"); 
				$item2->appendChild($text); 
				
			
				$item = $dom->createElement("於"); 
				$root->appendChild($item);
				$text = $dom->createTextNode(""); 
				$item->appendChild($text); 

				$item = $dom->createElement("年月日"); 
				$root->appendChild($item); 
				$text = $dom->createTextNode($this->tranfer2RCyearTrad($sp[0]->sp_send_date)); 
				$item->appendChild($text); 

				$item = $dom->createElement("機關"); 
				$root->appendChild($item);
				$item2 = $dom->createElement("全銜"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("刑事警察大隊(毒品查緝中心)"); 
				$item2->appendChild($text); 
				
				
				$item = $dom->createElement("主旨"); 
				$root->appendChild($item); 
				$item2 = $dom->createElement("文字"); 
				$item->appendChild($item2); 
				$text = $dom->createTextNode("檢陳".$surc->surc_name."違反毒品危害防制條例案，行政怠金處分案。"); 
				$item2->appendChild($text); 
				// $text = $dom->createTextNode($susp->s_roffice.(date('Y', strtotime($susp->s_fdate))-1911) .'年' . date('m', strtotime($susp->s_fdate))*1 .'月' . date('d', strtotime($susp->s_fdate))*1 . '日'.$sp->fd_send_num."所報".$susp->s_name."(證號:".$susp->s_ic.")違反毒品危害防制條例行政裁罰案"); 
				// $item2->appendChild($text); 
				
				// if(!isset($sp->fd_fact_text)||($sp->fd_fact_text==null)||($sp->fd_fact_text=='')){
				// 	$fd_fact_text = '受處分人'.$susp->s_name. $s_date . $s_time.'許，在本市'.$cases->s_place.'，為'.$cases->s_office.'員警查獲持有'.$sp->fd_drug_msg.'，經採集毒品及尿液檢體送專業單位鑑驗，均呈第'.((isset($drug2))?$drug2->ddc_level:'').'級毒品「'.((isset($drug2))?$drug2->ddc_ingredient:'').'」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg;
				// }
				// else
				// {
				// 	$fd_fact_text = $sp->fd_fact_text;
				// }
				
				$item = $dom->createElement("段落"); 
				$item->setAttribute("段名", "說明：");  
				$root->appendChild($item); 
				$item2 = $dom->createElement("文字"); 
				$item->appendChild($item2);

				$item3 = $dom->createElement("條列");
				$item3->setAttribute("序號", "一、");
				$item->appendChild($item3); 
				$item2 = $dom->createElement("文字"); 
				$item3->appendChild($item2);
				// $text = $dom->createTextNode($fd_fact_text); 
				$text = $dom->createTextNode('依據臺北市政府衛生局'.(date('Y', strtotime($surc->surc_basenumdate))-1911) .'年'. (date('m', strtotime($surc->surc_basenumdate))*1) .'月' . (date('d', strtotime($surc->surc_basenumdate))*1).'日北市衛醫傳防字第'.$surc->surc_basenum.'號'); 
				$item2->appendChild($text); 

				$item3 = $dom->createElement("條列");
				$item3->setAttribute("序號", "二、");
				$item->appendChild($item3); 
				$item2 = $dom->createElement("文字"); 
				$item3->appendChild($item2);
				// $text = $dom->createTextNode($fd_fact_text); 
				$text = $dom->createTextNode("處分書業經簽核奉准在案(如附件資料)。"); 
				$item2->appendChild($text); 

				$item = $dom->createElement("段落"); 
				$item->setAttribute("段名", "擬辦：");  
				$root->appendChild($item); 
				$item2 = $dom->createElement("文字"); 
				$item->appendChild($item2);
				$text = $dom->createTextNode("陳核後，依規定發文。"); 
				$item2->appendChild($text);

				$item = $dom->createElement("敬陳"); 
				$root->appendChild($item); 
				$text = $dom->createTextNode("敬陳　局長"); 
				$item->appendChild($text);
			
				//echo $dom->saveXML();  
				// $xmls[] = $dom->saveXML();  
				$this->zip->add_data($surc->surc_send_num.'.di',  $dom->saveXML());

				// 在監另外多一份函稿
				if($surc->surc_live_state == 3 || $surc->surc_live_state == '3')
				{
					$dom = new DOMDocument("1.0","utf-8"); 
					$implementation = new DOMImplementation();
					$dom->appendChild($implementation->createDocumentType('函 SYSTEM "104_8_utf8.dtd" [<!ENTITY 表單 SYSTEM "函.sw" NDATA DI><!NOTATION DI SYSTEM ""><!NOTATION _X SYSTEM "">]')); 
					// display document in browser as plain text 
					// for readability purposes 
					header("Content-Type: text/plain"); 
					// create root element 
					$root = $dom->createElement("函"); 
					$dom->appendChild($root); 
					// create child element 
					
					$item = $dom->createElement("檔號"); 
					$root->appendChild($item); 
					
					$item2 = $dom->createElement("年度"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode(date('Y')-1911); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("分類號"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("07270399");
					// $text = $dom->createTextNode(""); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("案次號"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("1"); 
					$item2->appendChild($text); 
					
					$item = $dom->createElement("保存年限"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("3"); 
					$item->appendChild($text); 

					$item = $dom->createElement("稿面註記"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("擬辦方式"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("以稿代簽"); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("決行層級"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("二層決行"); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("應用限制"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("限制開放"); 
					$item2->appendChild($text); 
							
					
					$item = $dom->createElement("地址"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("103024臺北市大同區重慶北路2段219號"); 
					$item->appendChild($text); 

					$item = $dom->createElement("聯絡方式"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("承辦人：".$surc->surc_empno); 
					$item->appendChild($text); 

					$item = $dom->createElement("聯絡方式"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("電話：(02)2393-2397"); 
					$item->appendChild($text); 


					$item = $dom->createElement("聯絡方式"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("電子信箱：p26169@police.taipei"); 
					$item->appendChild($text); 

					$item = $dom->createElement("受文者"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("交換表"); 
					$item2->setAttribute("交換表單", "表單");  
					$item->appendChild($item2); 
					$text = $dom->createTextNode(explode(':', $surc->surc_prison)[0]); 
					$item2->appendChild($text); 

					// $item = $dom->createElement("受文者"); 
					// $root->appendChild($item); 
					// $text = $dom->createTextNode(explode(':', $susp->s_prison)[0]); 
					// $item->appendChild($text); 

					// $item2 = $dom->createElement("交換表"); 
					// $item2->setAttribute("交換表單", "表單");  
					// $item->appendChild($item2); 

					$item = $dom->createElement("發文日期"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("年月日"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode(""); 
					$item2->appendChild($text); 
					
					$item = $dom->createElement("發文字號"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("字"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("北市警刑毒緝"); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement("文號"); 
					$item->appendChild($item2); 
					$item3 = $dom->createElement("年度"); 
					$item2->appendChild($item3); 
					$text = $dom->createTextNode((date('Y')-1911)); 
					$item3->appendChild($text); 
					$item3 = $dom->createElement("流水號"); 
					$item2->appendChild($item3); 
					// $text = $dom->createTextNode($susp->f_project_num); //日後再確認
					// $text = $dom->createTextNode($susp->f_project_id);
					// $item3->appendChild($text); 
					// $item3 = $dom->createElement("支號"); 
					// $item2->appendChild($item3); 
					
					$item = $dom->createElement("速別"); 
					$item->setAttribute("代碼", "普通件");  
					$root->appendChild($item); 
					
					$item = $dom->createElement("密等及解密條件或保密期限"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("密等"); 
					$item->appendChild($item2); 
					$item2 = $dom->createElement("解密條件或保密期限"); 
					$item->appendChild($item2); 
					$item = $dom->createElement("附件"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("文字"); 
					$item->appendChild($item2); 

					$text = $dom->createTextNode("處分書1份"); 
					$item2->appendChild($text); 
					
					$item = $dom->createElement("主旨"); 
					$root->appendChild($item); 
					$item2 = $dom->createElement("文字"); 
					$item->appendChild($item2);

					$text = $dom->createTextNode("檢送貴所在所人".$surc->surc_name."違反毒品危害防制條例案件處分書（文號：".$surc->surc_send_num."）1份，請查照惠復。"); 
					$item2->appendChild($text); 

					$item = $dom->createElement("段落"); 
					$item->setAttribute("段名", "說明：");  
					$root->appendChild($item); 
					$item2 = $dom->createElement("文字"); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode("依據行政程序法第89條：「對於在監所人為送達者，因囑託該監所長官為之」辦理。"); 
					$item2->appendChild($text);

					$item = $dom->createElement("正本"); 
					$root->appendChild($item); 
					$text = $dom->createTextNode("敬陳　大隊長"); 
					$item->appendChild($text); 
					$item2 = $dom->createElement('全銜'); 
					$item->appendChild($item2); 
					$text = $dom->createTextNode(explode(':', $surc->surc_prison)[0] . "(".explode(':', $surc->surc_prison)[1].")"); 
					$item2->appendChild($text); 
					$item2 = $dom->createElement('副本'); 
					$item->appendChild($item2); 
					$item2 = $dom->createElement('抄本'); 
					$item->appendChild($item2); 

					$this->zip->add_data($surc->surc_send_num.'inP.di',  $dom->saveXML());
				}
				$j++;
			}
			$this->zip->download(date('YmdHis').'.zip');
			unlink(date('YmdHis') .'.zip');
			// echo $html;
                                  
        // $pdf->Output($sp->fd_num .'.pdf', 'D');    
        // $pdf->Output($project .'.pdf', 'I');    
    }
	public function surc_doc_project_delivery(){
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($project); 

		$pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->setPrintHeader(false); //不要頁首
		//$pdf->setPrintFooter(false); //不要頁尾
		
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		$pdf->SetAutoPageBreak(FALSE, 0);
		$pdf->SetFont('twkai98_1','',11);
		$html='';
		foreach ($query->result() as $surc)
		{
			$pdf->AddPage('P', 'A4');
			$id = $this->uri->segment(3);
			$pjid = $this->uri->segment(4);
			

			$sp = $this->getsqlmod->getSP1($pjid)->result();
			$target_str = '';
			$send_str = '';

			$target_str = '';
			$target_str2 = '';
			$target_str2_addr = '';
			if(null != $surc && count(explode(",", $surc->surc_address)) > 1)
			{
				$fd_name = explode(",", $surc->surc_target)[0];
				$fd_addr = explode(",", $surc->surc_address)[0];
				$fd_zip = explode(",", $surc->surc_zipcode)[0];
				// echo '<mark>'. $fd_name.'</mark> 君 <br> <span style="margin-left: 3em;">'. $fd_zip . ' ' . $fd_addr.'</span>';
				// echo '<br/>';
				$fd_name1 = explode(",", $surc->surc_target)[1];
				$fd_addr1 = explode(",", $surc->surc_address)[1];
				$fd_zip1 = explode(",", $surc->surc_zipcode)[1];
				$target_str .=  $fd_name.'('.$fd_name1.'法定代理人) 君 <br>'. $fd_zip1 . ' ' . $fd_addr1;
				$target_str2 =  $fd_name.'('.$fd_name1.'法定代理人) 君';
				$target_str2_addr = $fd_addr1;
			}
			else
			{
				$target_str = $surc->surc_target.' 君 <br>'. $surc->surc_zipcode . ' ' . $surc->surc_address ;
				$target_str2 = $surc->surc_target.' 君';
				$target_str2_addr = $surc->surc_address;
			}
			
			$send_str = '北市警刑毒緝字第'.$surc->surc_send_num .'號';

			// 在監送達證書格式是另一種
			if($surc->surc_live_state != 3)
			{
				$pdf->SetMargins(20, 15, 20,true);
				$html ='
				<div style="width:600px" class="panel-body">
					<table width="600">
						<tr>
							<td align="right" style="height:30px;line-height:30px;"><h3>我是測試系統   </h3></td>
							<td align="left" style="height:30px;line-height:30px;"><h3>   送達證書-<strong>'.$surc->surc_no.'</strong></h3></td>
						</tr>
					</table>
					<table width="600" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td colspan="2" width="240" align="center" style="height:60px;line-height:75px;"><h5>受送達人名稱姓名地址</h5></td>
							<td colspan="2" width="360" style="height:60px;line-height:30px;"><h4>'.$target_str.'</h4></td>
						</tr>
						<tr>
							<td colspan="2" width="240" align="center" style="height:20px;line-height:25px;letter-spacing:108px;"><h5>文號</h5></td>
							<td colspan="2" width="360" style="height:20px;line-height:20px;"><h4>'.$send_str.'</h4></td>
						</tr>
						<tr>
							<td colspan="2" width="240" align="center" style="height:20px;line-height:25px;letter-spacing:7px;"><h5>送達文書(含案由)</h5></td>
							<td colspan="2" width="360" style="height:20px;line-height:20px;"><h4>違反毒品危害防制條例案件  <strong>處分書</strong></h4></td>
						</tr>
						<tr>
							<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"><h5>原寄郵局日戳</h5></td>
							<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"><h5>送達郵局日戳</h5></td>
							<td align="center" width="240" style="height:20px;line-height:25px;"><h5>送達處所 (由送達人填記)</h5></td>
							<td align="center" rowspan="2" width="120" style="height:60px;line-height:75px;"><h5>送達人簽章</h5></td>
						</tr>
						<tr>
							<td width="240" style="height:40px;line-height:25px;"><h5>☐同上記載地址 <br/>☐改送:</h5></td>
						</tr>
						<tr>
							<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"></td>
							<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"></td>
							<td align="center" width="240" style="height:20px;line-height:25px;"><h5>送達處所 (由送達人填記)</h5></td>
							<td align="center" rowspan="2" width="120" style="height:60px;line-height:75px;"></td>
						</tr>
						<tr>
							<td width="240" style="height:40px;line-height:25px;">
								<table>
									<tr>
										<td>中華民國</td>
										<td align="right">年</td>
										<td align="right">月</td>
										<td align="right">日</td>
									</tr>
									<tr>
										<td></td>
										<td align="right">午</td>
										<td align="right">時</td>
										<td align="right">分</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="4" width="600" style="height:20px;line-height:25px;letter-spacing:135px;">送達方式</td>
						</tr>
						<tr>
							<td align="center" colspan="4" width="600" style="height:20px;line-height:25px;letter-spacing:33px;">由送達人在☐上劃V選記</td>
						</tr>
					</table>
					<table width="600" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td colspan="2" width="280" style="height:30px;line-height:35px;"><h5>☐已將文書交與應受送達人</h5></td>
							<td colspan="2" width="320" style="height:30px;line-height:35px;">
								<table>
									<tr>
										<td width="200"><h5>☐本人</h5></td>
										<td width="120"><h5>(簽名或蓋章)</h5></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" width="280" style="height:30px;line-height:15px;"><h5>☐未獲會晤本人，已將文書交與有辨別事理能力之同居人、受僱人或應送達處所之接收郵件人員</h5></td>
							<td colspan="2" width="320" style="height:30px;line-height:15px;">
								<table>
									<tr>
										<td width="200"><h5>☐同居人<br/>☐受僱人<br/>☐應送達處所接收郵件人員</h5></td>
										<td width="120"><h5><br/>(簽名或蓋章)</h5></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" width="280" style="height:60px;line-height:20px;"><h5>☐應受送達之本人、同居人或受雇人收領後，拒絕或不能簽名或蓋章者，由送達人記明其事由</h5></td>
							<td colspan="2" width="320" style="height:60px;line-height:20px;">
								<table>
									<tr>
										<td width="200"><h5>送達人填記﹔</h5></td>
										<td width="120"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" width="280" style="height:60px;line-height:15px;"><h5>☐應受送達之本人、同居人、受僱人或應受送達處所接收郵件人員無正當理由拒絕領經送達人將文書留置於送達處所，以為送達</h5></td>
							<td colspan="2" width="320" style="height:60px;line-height:15px;">
								<table>
									<tr>
										<td width="200"><h5>☐本人<br/>☐同居人<br/>☐受僱人<br/>☐應受送達處所接收郵件人員</h5></td>
										<td width="120"><h5><br/>拒絕收領</h5></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" width="280" style="height:140px;line-height:15px;"><h5>☐未獲會晤本人亦無受領文書之同居人、受僱人或應受送達處所接收郵件人員，已將該送達文書﹔<br/><br/>☐應受送達之本人、同居人、受僱人或應受送達處所接收郵件人員無正當理由拒絕收領，並有難達留置情事，已將該送達文書﹔</h5></td>
							<td colspan="2" width="160" style="height:140px;line-height:20px;">
								<h5>☐寄存於_____________派出所<br/>☐寄存於_________鄉(鎮、市、區)__________公所<br/>☐寄存於_________鄉(鎮、市、區)__________公所__________村(里)辦公處<br/>☐寄存於_______________郵局</h5>
							</td>
							<td colspan="2" width="160" style="height:140px;line-height:15px;">
								<h5>並作送達通知書二份，一份黏貼於應受送達人住居所、事務所、營業所或其就業處所門首，一份☐交由鄰居轉交或☐置於該受送達處所信箱或其他適當位置，以為送達。</h5>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2" width="280" style="height:120px;line-height:130px;letter-spacing:20px;"><h5>送達人注意事項</h5></td>
							<td colspan="2" width="320" style="height:120px;line-height:20px;">
								<h5>一、依上述送達方法送達者，送達人應即將本送達證書，提出於交送達之行政機關附卷。</h5>
								<h5>二、不能依上述送達方法送達者，送達人應製作記載該事由之報告書，提出於交送達之行政機關附卷，並缴回應送達之文書。</h5>
							</td>
						</tr>
					</table>
					<table>
						<tr>
							<td>
								<h4>※ 請繳回 我是測試系統刑事警察大隊  地址﹔103024臺北市大同區重慶北路2段219號</h4>
							</td>
						</tr>
						<tr>
							<td>
								<h4>單位﹔毒品查緝中心('.$surc->surc_empno.')</h4>
							</td>
						</tr>
						<tr>
							<td align="right">
								<h6>我是測試系統交郵送達格式</h6>
							</td>
						</tr>
					</table>
				</div>';
			}
			else
			{
				$pdf->SetMargins(15, 15, 15,true);
				$html ='
				<div style="width:630px" class="panel-body">
					<table width="630px" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td align="center" colspan="2" style="height:60px;line-height:40px;"><h2>我是測試系統送達證書</h2></td>
						</tr>
						<tr>
							<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:5px;"><h2>案由</h2></td>
							<td width="480" style="height:60px;line-height:40px;"><h2>'.$target_str2.'怠金處分書送達</h2></td>
						</tr>
						<tr>
							<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:5px;"><h2>應送達之文書</h2></td>
							<td width="480" style="height:60px;line-height:20px;"><h2>我是測試系統行政文書一件（案件列管編號：'.$surc->surc_no.'）</h2></td>
						</tr>
						<tr>
							<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:10.5px;"><h2>應受送達人</h2></td>
							<td width="480" style="height:60px;line-height:40px;"><h2>'.$target_str2.'</h2></td>
						</tr>
					</table>
					<table width="630px" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td width="30" align="center" style="height:20px;line-height:25px;"><h3>一</h3></td>
							<td width="30" style="height:20px;line-height:25px;"></td>
							<td width="570" style="height:20px;line-height:25px;"><h3>已將文書交與應受送達人本人。</h3></td>
						</tr>
						<tr>
							<td rowspan="3" width="30" align="center" style="height:60px;line-height:50px;"><h3>二</h3></td>
							<td rowspan="3" width="30" style="height:60px;line-height:25px;"></td>
							<td width="570" style="height:20px;line-height:25px;"><h3>未會晤本人，已將文書交予下列有辨別事理能力之人。</h3></td>
							
						</tr>
						<tr>
							<td width="570">
								<table width="570" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td width="100" align="center" style="height:20px;line-height:25px;"><h3>同居人</h3></td>
										<td width="185" align="center" style="height:20px;line-height:25px;"></td>
										<td width="130" align="center" style="height:20px;line-height:25px;"><h3>與本人關係</h3></td>
										<td width="155" align="center" style="height:20px;line-height:25px;"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="570">
								<table width="570" cellspacing="0" cellpadding="0" border="1">
									<tr>
										<td width="100" align="center" style="height:20px;line-height:25px;"><h3>受僱人</h3></td>
										<td width="185" align="center" style="height:20px;line-height:25px;"></td>
										<td width="130" align="center" style="height:20px;line-height:25px;"><h3>與本人關係</h3></td>
										<td width="155" align="center" style="height:20px;line-height:25px;"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="30" align="center" style="height:20px;line-height:55px;"><h3>三</h3></td>
							<td width="30" style="height:20px;line-height:25px;"></td>
							<td width="570" style="height:20px;line-height:20px;"><h3>未會晤本人亦無收領文書之同居人、受僱人，已將文書寄存於_____________________，並作送達通知書兩份，一份粘貼於應受送達人住居所、事務所、營業所或其就業處所門首，另一份置於該送達處所信箱或其他位置。</h3></td>
						</tr>
						<tr>
							<td rowspan="2" width="30" align="center" style="height:40px;line-height:40px;"><h3>四</h3></td>
							<td rowspan="2" width="30" style="height:40px;"></td>
							<td width="570" style="height:20px;line-height:20px;">
								<table width="570">
									<tr>
										<td align="center" width="190"><h3>☐本人</h3></td>
										<td align="center" width="190"><h3>☐同居人</h3></td>
										<td align="center" width="190"><h3>☐受僱人</h3></td>
									</tr>
								</table>
							</td>
							
						</tr>
						<tr>
							<td width="570" style="height:20px;line-height:20px;"><h3>無正當理由拒絕收領，已將文書留置於該處。</h3></td>
						</tr>
						<tr>
							<td width="30" align="center" style="height:40px;line-height:40px;"><h3>五</h3></td>
							<td width="30" style="height:40px;line-height:40px;"></td>
							<td width="570" style="height:40px;line-height:40px;"><h3>收領人拒絕或不能簽名蓋章及捺指印。</h3></td>
						</tr>
						<tr>
							<td colspan="3" width="630" style="height:60px;line-height:65px;"><h3>收領人簽名蓋章或捺指印：</h3></td>
						</tr>
					</table>
					<table width="630px" cellspacing="0" cellpadding="0" border="1">
						<tr>
							<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:5px;"><h2>送達之時日</h2></td>
							<td width="480" style="height:60px;line-height:40px;"><h2>中華民國____年____月____日____午____時____分</h2></td>
						</tr>
						<tr>
							<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:10.5px;"><h2>送達之處所</h2></td>
							<td width="480" style="height:60px;line-height:40px;"><h2>'.explode(':', $surc->surc_prison)[1] .'('.explode(':', $surc->surc_prison)[0].')'.'</h2></td>
						</tr>
						<tr>
							<td colspan="2" align="center" width="630" style="height:60px;line-height:20px;">
								<h2>中華民國____年____月____日製作</h2>
								<h1>送達單位﹔____________________送達人(職稱、姓名)﹔____________________</h1>
							</td>
						</tr>
						<tr>
							<td align="center" width="150" style="height:60px;line-height:70px;letter-spacing:10.5px;"><h2>注意</h2></td>
							<td width="480" style="height:60px;line-height:15px;">
								<table>
									<tr>
										<td width="40" style="line-height:20px;"><h2>一、</h2></td>
										<td width="440" style="line-height:20px;"><h2>「一」、「二」、「三」、「四」、「五」等欄所載各方法中，係依其何種方法將文書送達，應在該欄數字右方之空格內打勾為記。</h2></td>
									</tr>
									<tr>
										<td width="40"><h2>二、</h2></td>
										<td width="440"><h2>如係同居人、受僱人應載其姓名及與本人之關係。</h2></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<table>
						<tr>
							<td>
								<h5>※ 請繳回 我是測試系統刑事警察大隊  地址﹔103024臺北市大同區重慶北路2段219號</h5>
							</td>
						</tr>
						<tr>
							<td>
								<h5>單位﹔毒品查緝中心('.$surc->surc_empno.')</h5>
							</td>
						</tr>
					</table>';
			}
			
			$pdf->writeHTML($html, true, false, true, false, '');
		}
		// $pdf->Output($project.'.pdf', 'D');    //download
        $pdf->Output($pjid.'.pdf', 'I');    //read
	}
	public function surc_doc_project_fin()//產生怠金處分書郵遞pdf
    {
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
		$pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->setPrintHeader(false); //不要頁首
		// $pdf->setPrintFooter(false); //不要頁尾
		// 版面配置 > 邊界
		//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetMargins(5.5, 3.5, 10,true);
		// set some language-dependent strings (optional)
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('twkai98_1','',10);
		// $pdf->SetFont('msungstdlight', '', 10);
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetFont('twkai98_1','',10);
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // $pdf->SetAutoPageBreak(TRUE, 0);
			
            $html='';       
            foreach ($query->result() as $surc)
            {
				$pdf->AddPage('P', 'A4');
                // $id = $suspect->s_num;
                // $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                // $susp=$susp[0];
                // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                // $fine=$fine[0];
                // $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                // $sp1=$this->getsqlmod->get3SP1($id)->result();
                // $sp=$sp1[0];
                // $spcount=$this->getsqlmod->getfdAll()->result();
                // $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                // $cases=$cases[0];
                // $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                // $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                // $today['day']   = date('d');
                // $today['month'] = date('m');
                // $today['year']  = date('Y') - 20;
                // $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                //     $fname = NULL;
                //     $fgender = NULL;
                //     $fic = NULL;
                //     $fbirth = NULL;
                //     $fphone = NULL;
                //     $fzipcode = NULL;
                //     $faddress = NULL;
                // if(isset($suspfadai[0])){
                //     $suspfadai = $suspfadai[0];
                //     $stampBirth = strtotime($susp->s_birth);
                //     if ($stampBirth > $stampToday) {
                //         $fname =  $suspfadai->s_fadai_name;
                //         $fgender = $suspfadai->s_fadai_gender;
                //         $fic = $suspfadai->s_fadai_ic;
                //         $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                //         $fphone = $suspfadai->s_fadai_phone;
                //         $fzipcode = $suspfadai->s_fadai_zipcode;
                //         $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                //     }else{
                //         $fname = NULL;
                //         $fgender = NULL;
                //         $fic = NULL;
                //         $fbirth = NULL;
                //         $fphone = NULL;
                //         $fzipcode = NULL;
                //         $faddress = NULL;
                //     }
                // }
                // if(!isset($phone))$phone1=null;
                // if(isset($phone))$phone1=$phone[0]->p_no;
                // $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                // $drug=$drug[0];
                // $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                // $drug2=$drug2[0];
                // $prison=$this->getsqlmod->getprison()->result();
                // $prison=$prison[0];
				$id = $this->uri->segment(3);
				$pjid = $this->uri->segment(4);
				// $fd1 = $this->getsqlmod->getFD('fd_snum',$id)->result();
				// $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
				// $surc=$this->getsqlmod->get1Sur_ed('surc_num',$id)->result();
				$DpOrder=$this->getsqlmod->get1SurOrder('surc_projectnum',$surc->surc_projectnum)->result();
				// $sp=$this->getsqlmod->get3SP1($id)->result();
				// $spcount=$this->getsqlmod->getsurcAll()->result();
				// $cases=$this->getsqlmod->getCases($susp[0]->s_cnum)->result();
				// $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp[0]->s_dp_project)->result();
				// $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
				// $susp1 = $this->getsqlmod->get2Susp($susp[0]->s_num)->result(); // 抓snum
				
				$suspfadai = $this->getsqlmod->get1SuspFadai($surc->surc_sic)->result();

				$prison=$this->getsqlmod->getWordlibPrison()->result();
				$courses=$this->getsqlmod->getCourseData()->result();

				$sp = $this->getsqlmod->getSP1($pjid)->result();
				$target_str = '';
				$send_str = '';
				if(null != $surc && count(explode(",", $surc->surc_address)) > 1)
				{
					$fd_name = explode(",", $surc->surc_target)[0];
					$fd_addr = explode(",", $surc->surc_address)[0];
					$fd_zip = explode(",", $surc->surc_zipcode)[0];
					// echo '<mark>'. $fd_name.'</mark> 君 <br> <span style="margin-left: 3em;">'. $fd_zip . ' ' . $fd_addr.'</span>';
					// echo '<br/>';
					$fd_name1 = explode(",", $surc->surc_target)[1];
					$fd_addr1 = explode(",", $$surc->surc_address)[1];
					$fd_zip1 = explode(",", $surc->surc_zipcode)[1];
					$target_str .= $fd_name.'君(受處分人'.$fd_name1.'之法定代理人)  <br> <span>'. $fd_zip1 . ' ' . $fd_addr1.'</span>';
					if($surc->surc_live_state == 2)
					{
						$target_str .= '（指定送達現住地）';
					}
				}
				else
				{
					$target_str = $surc->surc_target.' 君 <br> <span style="margin-left: 3em;">'. ((!empty($surc->surc_prison))?explode(':', $surc->surc_prison)[1] .'('.explode(':', $surc->surc_prison)[0].')' :$surc->surc_zipcode . ' ' . $surc->surc_address) .'</span>';
					if($surc->surc_live_state == 2)
					{
						$target_str .= '（指定送達現住地）';
					}
				}
				if($surc->surc_date != '0000-00-00' && isset($surc->surc_date))
				{
					$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.(date('m', strtotime($surc->surc_date))*1) .'月' . (date('d', strtotime($surc->surc_date))*1) .'日' .'北市警刑毒緝字第'.$surc->surc_send_num .'號';
				}else{
					if(isset($sp->sp_send_date))
					{ 
						if($sp->sp_send_date !=  '0000-00-00')
						{
							$send_str = '發文日期及文號：'.((int)substr($sp->sp_send_date, 0, 4)- 1911) . '年' . ((int)substr($sp->sp_send_date, 5, 2)*1) . '月' . ((int)substr($sp->sp_send_date, 8, 2)*1) . '日'.'北市警刑毒緝字第'.$surc->surc_send_num.'號';
						}
						else
						{
							$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.(date('m')*1).'月  日北市警刑毒緝字第'.$surc->surc_send_num.'號';
						}
						
					
					}else{
						$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.(date('m')*1).'月  日北市警刑毒緝字第'.$surc->surc_send_num.'號';
					}
				} 
				
                $html='
                                <div style="width:680px" class="panel-body">
                                    <table width="680">
                                        <tr>
											<td colspan="3">
												<h2>列管編號：'.$surc->surc_no .'</h2> 
											</td>
											<td colspan="5">
												<h2>原處分管制編號：'.$surc->surc_listed_no .'</h2>
											</td>
										</tr>
										<tr>
											<td colspan="8"><h2>正本：'.$target_str.'</h2></td>
										</tr>
										<tr>
											<td colspan="8"><h3>副本：我是測試系統刑事警察大隊毒品查緝中心<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;臺北市政府毒品危害防制中心</h3></td>
										</tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
											<tr>
                                                <td colspan="8" width="680">
													<h1 align="center">我是測試系統違反毒品危害防制條例案件（怠金）處分書</h1>
													<h3 align="right">'.$send_str.'</h3>
												</td>
                                            </tr>
											<tr>
                                                <td colspan="1" width="77">
													<h3 align="center">依據</h3>
												</td>
												<td colspan="7" width="603">
												臺北市政府衛生局'.(date('Y', strtotime($surc->surc_basenumdate))-1911) .'年'. (date('m', strtotime($surc->surc_basenumdate))*1) .'月' . (date('d', strtotime($surc->surc_basenumdate))*1).'日北市衛醫傳防字第'.$surc->surc_basenum.'號
												</td>
                                            </tr>  
                                            <tr>
                                                <td rowspan="4" width="77" height="70" style="line-height:70px;">
													<h3 align="center">受處分人</h3>
												</td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$surc->surc_name.'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="70" height="30" style="line-height:15px;">出生<br>年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;">
													<strong>民國'.((isset($susp))?((date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth)))):'' ).'日</strong>
												</td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" align="center" width="88" height="30" style="line-height:30px;"><strong>'.$surc->surc_sic.'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及【聯絡電話】</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.((isset($phone1))?$phone1:'').'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="20" height="40" align="center" style="line-height:20px;">地址</td>
												<td width="50" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong>'.((isset($surc->surc_prison) && !empty($surc->surc_prison))?explode(':', $surc->surc_prison)[1] .'('.explode(':', $surc->surc_prison)[0].')' : $surc->surc_address) . (($surc->surc_live_state == 2)?'（指定送達現住地）':'') .'</strong></td>
                                            </tr>
                                            <tr>
												<td width="50" align="center"  height="20" style="line-height:20px;">戶籍地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong>'.$surc->surc_address.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77" height="70" style="line-height:20px;"><h3 align="center"><br>法定 代理人</h3></td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生<br>年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;"><strong></strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及【聯絡電話】</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="20" height="40" align="center" style="line-height:20px;">地址</td>
												<td width="50" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td width="50" align="center"  height="20" style="line-height:20px;">戶籍地</td>
												<td colspan="5" width="533" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="77" height="60" style="line-height:60px;"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">一、受處分人處罰：怠金新台幣<strong>'.(number_format($surc->surc_amount)).'</strong>元整。<br>二、仍須參加講習，講習時地詳見詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續處以怠金。
                                                </td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:60px;"><h3 align="center">事實</h3></td>
                                                <td colspan="7" width="603">緣受處分人'.$surc->surc_target.'涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於'.$this->tranfer2RCyear2($surc->surc_olec_date).'至ＯＯＯ參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。</td>
                                            </tr>
                                            <tr>
												<td width="77" height="50" style="line-height:20px;"><h3 align="center"><br>理由及 法令依據</h3></td>
                                                <td colspan="7" width="603">■一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習&nbsp;&nbsp;&nbsp;&nbsp;者，依行政執行法規定處以怠金。<br>■二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不&nbsp;&nbsp;&nbsp;&nbsp;能由他人代為履行者，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。<br>■三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續&nbsp;&nbsp;&nbsp;&nbsp;處以怠金。<br>
                                                </td>
                                            </tr>
                                            <tr>
												<td width="77" height="90" style="line-height:20px;"><h3 align="center"><br><br>繳納期限 及方式</h3></td>
                                                <td colspan="7" width="603">一、怠金限於<strong>民國'.(($surc->surc_findate != '0000-00-00')?(date('Y', strtotime($surc->surc_findate))-1911) .'年'.(date('m', strtotime($surc->surc_findate))) .'月'.(date('d', strtotime($surc->surc_findate))) .'日':'Ｏ年Ｏ月Ｏ日').'</strong>前選擇下列方式之一繳款，逾期不繳納依法移送行政執行處執行：<br>&nbsp;&nbsp;&nbsp;&nbsp;(一)以自動化設備（ATM、網路ATM、網路銀行）匯款至「虛擬帳號」（限本處分書）。 <br/>&nbsp;&nbsp;&nbsp;&nbsp;(二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>二、自動化設備匯款方式：（一）選擇【繳費】服務 （二）輸入轉入銀行代號：<strong>012</strong><br>&nbsp;&nbsp;&nbsp;&nbsp;(三)輸入本案虛擬帳號：<strong>'.$surc->surc_BVC.'</strong>。（匯款手續費由繳款人負擔）<br/>三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361021</strong>，戶名:<strong>&nbsp;&nbsp;&nbsp;&nbsp;我是測試系統刑事警察大隊</strong>。<br>四、匯款繳納時，請於匯款單上之匯款人欄填寫受處分人之姓名「'.$surc->surc_name.'」，並應要求於備註(附言)欄填入案&nbsp;&nbsp;&nbsp;&nbsp;件列管編號「'.$surc->surc_no.'」及身分證統一編號「'.$surc->surc_sic.'」，俾利辦理銷案。
                                                </td>
                                            </tr>';
				$courser_str = '';
				if(isset($surc->surc_prison) && !empty($surc->surc_prison))
				{
					$courser_str = '請受處分人出監後，主動聯繫臺北市政府毒品危害防制中心。';
				} else {
					if(isset($surc->surc_lec_date) && !empty($surc->surc_lec_datee) && $surc->surc_other_lec == null){
						
						$md_str = explode("(", $surc->surc_lec_date)[0];
						$week_str = explode("(", $surc->surc_lec_date)[1];

						$lec_time = tranfer2RChour(explode('-', $surc->surc_lec_time)[0]);

						$courser_str = "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日"  . $lec_time . '（請攜帶有相片之證件報到，逾時將無法入場），地點：'.$surc->surc_lec_place .'(' . $surc->surc_lec_address . ')'.'，請配合防疫相關措施。';
					}else{
						$courser_str = '由臺北市政府毒品危害防制中心另行通知。';
					}
				}
				$html .=                   '<tr>
												<td width="77" height="60" style="line-height:60px;"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">一、可選擇參與實體講習或線上課程（線上課程每年可參與三次，惟應以不同套裝課程申請認定）。<br/>二、講習時間：'.$courser_str.'有正當理由無法參加或要選擇線&nbsp;&nbsp;&nbsp;&nbsp;上課程者，請至下列網址：https://nodrug.gov.taipei下載異動申請表或依指示完成線上課程。講習相&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;關問題，請逕向臺北市政府毒品危害防制中心洽詢，電話(02)2375-4068。
                                                </td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:60px;"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">一、不服本處分者，義務人或利害關係人對執行命令、執行方法、應遵守之程序或其他侵害利益之情事，得於&nbsp;&nbsp;&nbsp;&nbsp;執行程序終結前，向執行機關聲明異議。執行機關認其聲明異議有理由者，應即停止執行，並撤銷或更正&nbsp;&nbsp;&nbsp;&nbsp;之；認其無理由者，應於十日內加具意見，送直接上級主管機關於三十日內決定之。<br/>二、本件處分書承辦人：'.$surc->surc_empno.'，聯絡電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                </div>';
								$pdf->writeHTML($html, true, false, true, false, '');  
            }
			// echo $html;
                                  
        // $pdf->Output($sp->fd_num .'.pdf', 'D');    
        $pdf->Output($project .'.pdf', 'I');    
    }
	public function surc_doc_public_fin()
	{
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
		$pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->setPrintHeader(false); //不要頁首
		// $pdf->setPrintFooter(false); //不要頁尾
		// 版面配置 > 邊界
		//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetMargins(5.5, 3.5, 10,true);
		// set some language-dependent strings (optional)
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('twkai98_1','',10);
		// $pdf->SetFont('msungstdlight', '', 10);
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetFont('twkai98_1','',10);
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // $pdf->SetAutoPageBreak(TRUE, 0);
			
            $html='';       
            foreach ($query->result() as $surc)
            {
				$pdf->AddPage('P', 'A4');
                // $id = $suspect->s_num;
                // $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                // $susp=$susp[0];
                // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                // $fine=$fine[0];
                // $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                // $sp1=$this->getsqlmod->get3SP1($id)->result();
                // $sp=$sp1[0];
                // $spcount=$this->getsqlmod->getfdAll()->result();
                // $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                // $cases=$cases[0];
                // $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                // $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                // $today['day']   = date('d');
                // $today['month'] = date('m');
                // $today['year']  = date('Y') - 20;
                // $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                //     $fname = NULL;
                //     $fgender = NULL;
                //     $fic = NULL;
                //     $fbirth = NULL;
                //     $fphone = NULL;
                //     $fzipcode = NULL;
                //     $faddress = NULL;
                // if(isset($suspfadai[0])){
                //     $suspfadai = $suspfadai[0];
                //     $stampBirth = strtotime($susp->s_birth);
                //     if ($stampBirth > $stampToday) {
                //         $fname =  $suspfadai->s_fadai_name;
                //         $fgender = $suspfadai->s_fadai_gender;
                //         $fic = $suspfadai->s_fadai_ic;
                //         $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                //         $fphone = $suspfadai->s_fadai_phone;
                //         $fzipcode = $suspfadai->s_fadai_zipcode;
                //         $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                //     }else{
                //         $fname = NULL;
                //         $fgender = NULL;
                //         $fic = NULL;
                //         $fbirth = NULL;
                //         $fphone = NULL;
                //         $fzipcode = NULL;
                //         $faddress = NULL;
                //     }
                // }
                // if(!isset($phone))$phone1=null;
                // if(isset($phone))$phone1=$phone[0]->p_no;
                // $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                // $drug=$drug[0];
                // $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                // $drug2=$drug2[0];
                // $prison=$this->getsqlmod->getprison()->result();
                // $prison=$prison[0];
				$id = $this->uri->segment(3);
				$pjid = $this->uri->segment(4);
				// $fd1 = $this->getsqlmod->getFD('fd_snum',$id)->result();
				// $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
				// $surc=$this->getsqlmod->get1Sur_ed('surc_num',$id)->result();
				$DpOrder=$this->getsqlmod->get1SurOrder('surc_projectnum',$surc->surc_projectnum)->result();
				// $sp=$this->getsqlmod->get3SP1($id)->result();
				// $spcount=$this->getsqlmod->getsurcAll()->result();
				// $cases=$this->getsqlmod->getCases($susp[0]->s_cnum)->result();
				// $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp[0]->s_dp_project)->result();
				// $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
				// $susp1 = $this->getsqlmod->get2Susp($susp[0]->s_num)->result(); // 抓snum
				
				$suspfadai = $this->getsqlmod->get1SuspFadai($surc->surc_sic)->result();

				$prison=$this->getsqlmod->getWordlibPrison()->result();
				$courses=$this->getsqlmod->getCourseData()->result();

				$sp = $this->getsqlmod->getSP1($pjid)->result();
				$target_str = '';
				$send_str = '';
				if(null != $surc && count(explode(",", $surc->surc_address)) > 1)
				{
					$fd_name = explode(",", $surc->surc_target)[0];
					$fd_addr = explode(",", $surc->surc_address)[0];
					$fd_zip = explode(",", $surc->surc_zipcode)[0];
					// echo '<mark>'. $fd_name.'</mark> 君 <br> <span style="margin-left: 3em;">'. $fd_zip . ' ' . $fd_addr.'</span>';
					// echo '<br/>';
					$fd_name1 = explode(",", $surc->surc_target)[1];
					$fd_addr1 = explode(",", $$surc->surc_address)[1];
					$fd_zip1 = explode(",", $surc->surc_zipcode)[1];
					$target_str .= $fd_name.'('.$fd_name1.'法定代理人) 君 <br> <span style="margin-left: 3em;">'. $fd_zip1 . ' ' . $fd_addr1.'</span>';
					if($surc->surc_live_state == 2)
					{
						$target_str .= '（指定送達現住地）';
					}
				}
				else
				{
					$target_str = $surc->surc_target.' 君 <br> <span style="margin-left: 3em;">'. ((!empty($surc->surc_prison))?explode(':', $surc->surc_prison)[0] .'('.explode(':', $surc->surc_prison)[1].')' :$surc->surc_zipcode . ' ' . $surc->surc_address) .'</span>';
					if($surc->surc_live_state == 2)
					{
						$target_str .= '（指定送達現住地）';
					}
				}
				if($surc->surc_date != '0000-00-00' && isset($surc->surc_date))
				{
					$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.(date('m', strtotime($surc->surc_date))*1) .'月' . (date('d', strtotime($surc->surc_date))*1) .'日' .'北市警刑毒緝字第'.$surc->surc_send_num .'號';
				}else{
					if($sp->sp_send_date !=  '0000-00-00' && isset($sp->sp_send_date))
					{ 
					
						$send_str = '發文日期及文號：'.((int)substr($sp->sp_send_date, 0, 4)- 1911) . '年' . ((int)substr($sp->sp_send_date, 5, 2)*1) . '月' . ((int)substr($sp->sp_send_date, 8, 2)*1) . '日'.'北市警刑毒緝字第'.$surc->surc_send_num.'號';
					
					}else{
						$send_str = '發文日期及文號：'.(date('Y')-1911).'年'.(date('m')*1).'月  日北市警刑毒緝字第'.$surc->surc_send_num.'號';
					}
				} 
				
                $html='
                                <div style="width:680px" class="panel-body">
                                    <table width="680">
                                        <tr>
											<td colspan="3">
												<h2>列管編號：'.$surc->surc_no .'</h2> 
											</td>
											<td colspan="5">
												<h2>原處分管制編號：'.$surc->surc_listed_no .'</h2>
											</td>
										</tr>
										<tr>
											<td colspan="8">正本：'.$target_str.'</td>
										</tr>
										<tr>
											<td colspan="8">副本：我是測試系統刑事警察大隊毒品查緝中心、臺北市政府毒品危害防治中心</td>
										</tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
											<tr>
                                                <td colspan="8" width="680">
													<h3 align="center">我是測試系統違反毒品危害防制條例案件（怠金）處分書</h3>
													<h4 align="right">'.$send_str.'</h4>
												</td>
                                            </tr>
											<tr>
                                                <td colspan="1" width="77">
													<h3 align="center">依據</h3>
												</td>
												<td colspan="7" width="603">
												臺北市政府衛生局'.(date('Y', strtotime($surc->surc_basenumdate))-1911) .'年'. (date('m', strtotime($surc->surc_basenumdate))*1) .'月' . (date('d', strtotime($surc->surc_basenumdate))*1).'日北市衛醫傳防字第'.$surc->surc_basenum.'號
												</td>
                                            </tr>  
                                            <tr>
                                                <td rowspan="4" width="77" height="70" style="line-height:70px;">
													<h3 align="center">受處分人</h3>
												</td>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
                                                <td align="center" width="88" height="30" style="line-height:30px;"><strong>'.$surc->surc_name.'</strong></td>
                                                <td align="center" width="49" height="30" style="line-height:30px;">性別</td>
                                                <td align="center" width="50" height="30" style="line-height:30px;"><strong></strong></td>
                                                <td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
                                                <td align="center" width="276" height="30" style="line-height:30px;">
													<strong>民國'.((isset($susp))?((date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth)))):'' ).'日</strong>
												</td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
                                                <td align="center" align="center" width="88" height="30" style="line-height:30px;"><strong>'.$surc->surc_sic.'</strong></td>
                                                <td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276" height="30" style="line-height:30px;"><strong>'.((isset($phone1))?$phone1:'').'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
                                                <td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.((isset($surc->surc_prison) && !empty($surc->surc_prison))?explode(':', $surc->surc_prison)[1] .'('.explode(':', $surc->surc_prison)[0].')' : $surc->surc_address) . (($surc->surc_live_state == 2)?'（指定送達現住地）':'') .'</strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
                                                <td colspan="5" width="445" height="20" style="line-height:20px;"><strong>'.$surc->surc_address.'</strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="4" width="77" height="70" style="line-height:20px;"><h3 align="center"><br>法定 代理人</h3></td>
												<td align="center" colspan="2" width="70" height="30" style="line-height:30px;">姓名</td>
												<td align="center" width="88" height="30" style="line-height:30px;"><strong></strong></td>
												<td align="center" width="49" height="30" style="line-height:30px;">性別</td>
												<td align="center" width="50" height="30" style="line-height:30px;"><strong></strong></td>
												<td align="center" width="70" height="30" style="line-height:30px;">出生年月日</td>
												<td align="center" width="276" height="30" style="line-height:30px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td align="center" colspan="2" width="70" height="30" style="line-height:15px;">身分證<br>統一編號</td>
												<td align="center"  width="88" height="30" style="line-height:30px;"><strong></strong></td>
												<td align="center" colspan="3" width="169" height="30" style="line-height:15px;">其他足資辨別之特徵<br>及聯絡電話</td>
												<td width="276" height="30" style="line-height:30px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td rowspan="2" width="70" height="40" align="center" style="line-height:40px;">地址</td>
												<td width="88" align="center" height="20" style="line-height:20px;">現住地</td>
												<td colspan="5" width="445" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td width="88" align="center"  height="20" style="line-height:20px;">戶籍地</td>
												<td colspan="5" width="445" height="20" style="line-height:20px;"><strong></strong></td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:60px;"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>一、怠金新台幣<strong>'.(number_format($surc->surc_amount)).'</strong>元整。<br>二、仍須參加講習，講習時地詳見詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續處以怠金。
                                                </td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:60px;"><h3 align="center">事實</h3></td>
                                                <td colspan="7" width="603">緣受處分人'.$surc->surc_target.'涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於'.$this->tranfer2RCyear2($surc->surc_olec_date).'至ＯＯＯ參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。</td>
                                            </tr>
                                            <tr>
												<td width="77" height="50" style="line-height:20px;"><h3 align="center"><br>理由及 法令依據</h3></td>
                                                <td colspan="7" width="603">■一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習者，依行政執行法規定處以怠金。<br>■二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不能由他人代為履行者，依其情節輕重處新台幣五千元以上三十萬元以下怠金。<br>■三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續處以怠金。<br>
                                                </td>
                                            </tr>
                                            <tr>
												<td width="77" height="90" style="line-height:20px;"><h3 align="center"><br><br>繳納期限 及方式</h3></td>
                                                <td colspan="7" width="603">一、怠金限於<strong>民國'.(($surc->surc_findate != '0000-00-00')?(date('Y', strtotime($surc->surc_findate))-1911) .'年'.(date('m', strtotime($surc->surc_findate))) .'月'.(date('d', strtotime($surc->surc_findate))) .'日':'Ｏ年Ｏ月Ｏ日').'</strong>前選擇下列方式之一繳款，逾期不繳納依法移送行政執行處執行：<br>(一)以自動化設備（ATM、網路ATM、網路銀行）匯款至「虛擬帳號」（限本處分書）。 <br/>(二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>二、自動化設備匯款方式：（一）選擇【繳費】服務 （二）輸入轉入銀行代號：<strong>012</strong><br>(三)輸入本案虛擬帳號：<strong>'.$surc->surc_BVC.'</strong>。（匯款手續費由繳款人負擔）<br/>三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361021</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>。<br>四、匯款繳納時，請於匯款單上之匯款人欄填寫受處分人之姓名「'.$surc->surc_name.'」，並應要求於備註(附言)欄填入案件列管編號「'.$surc->surc_no.'」及身分證統一編號「'.$surc->surc_sic.'」，俾利辦理銷案。
                                                </td>
                                            </tr>';
				$courser_str = '';
				if(isset($surc->surc_prison) && !empty($surc->surc_prison))
				{
					$courser_str = '請受處分人出監後，主動聯繫臺北市政府毒品危害防制中心。';
				} else {
					if(isset($surc->surc_lec_date) && !empty($surc->surc_lec_datee) && $surc->surc_other_lec == null){
						
						$md_str = explode("(", $surc->surc_lec_date)[0];
						$week_str = explode("(", $surc->surc_lec_date)[1];

						$lec_time = tranfer2RChour(explode('-', $surc->surc_lec_time)[0]);

						$courser_str = "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日"  . $lec_time . '（請攜帶有相片之證件報到，逾時將無法入場），地點：'.$surc->surc_lec_place .'(' . $surc->surc_lec_address . ')'.'，請配合防疫相關措施。';
					}else{
						$courser_str = '由臺北市政府毒品危害防制中心另行通知。';
					}
				}
				$html .=                   '<tr>
												<td width="77" height="60" style="line-height:60px;"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">一、可選擇參與實體講習或線上課程（線上課程每年可參與三次，惟應以不同套裝課程申請認定）。<br/>二、講習時間：'.$courser_str.'有正當理由無法參加或要選擇線上課程者，請至下列網址：https://nodrug.gov.taipei下載異動申請表或依指示完成線上課程。講習相關問題，請逕向臺北市政府毒品危害防制中心洽詢，電話(02)2375-4068。
                                                </td>
                                            </tr>
                                            <tr>
												<td width="77" height="60" style="line-height:60px;"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">一、不服本處分者，義務人或利害關係人對執行命令、執行方法、應遵守之程序或其他侵害利益之情事，得於執行程序終結前，向執行機關聲明異議。執行機關認其聲明異議有理由者，應即停止執行，並撤銷或更正之；認其無理由者，應於十日內加具意見，送直接上級主管機關於三十日內決定之。<br/>二、本件處分書承辦人：'.$surc->surc_empno.'，聯絡電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
									<p>
                                        承辦單位 (2539) 核稿 決行
                                    </p>  
                                </div>';
								$pdf->writeHTML($html, true, false, true, false, '');  
            }
			// echo $html;
                                  
        // $pdf->Output($sp->fd_num .'.pdf', 'D');    
        $pdf->Output($project .'.pdf', 'I');   
	}
    public function surc_prison_fin()//產生怠金處分書郵遞pdf(正本)
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
			$pdf->SetFont('twkai98_1','',10);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 20;
                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                    $fname = NULL;
                    $fgender = NULL;
                    $fic = NULL;
                    $fbirth = NULL;
                    $fphone = NULL;
                    $fzipcode = NULL;
                    $faddress = NULL;
                if(isset($suspfadai[0])){
                    $suspfadai = $suspfadai[0];
                    $stampBirth = strtotime($susp->s_birth);
                    if ($stampBirth > $stampToday) {
                        $fname =  $suspfadai->s_fadai_name;
                        $fgender = $suspfadai->s_fadai_gender;
                        $fic = $suspfadai->s_fadai_ic;
                        $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                        $fphone = $suspfadai->s_fadai_phone;
                        $fzipcode = $suspfadai->s_fadai_zipcode;
                        $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                    }else{
                        $fname = NULL;
                        $fgender = NULL;
                        $fic = NULL;
                        $fbirth = NULL;
                        $fphone = NULL;
                        $fzipcode = NULL;
                        $faddress = NULL;
                    }
                }
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                                <div style="width:680px" class="panel-body"><p>
                                    <table>
                                        <tr><td ><h2>列管編號：'.$suspect->surc_no .'</h2> </td><td colspan="2"><h2>原處分管制編號：'.$sp->fd_num .'</h2></td></tr>
                                        <tr><td colspan="3">正本：'.$sp->fd_target.'君'.$sp->fd_zipcode.' '.$sp->fd_address.'</td></tr>
                                        <tr><td colspan="3">副本：我是測試系統刑事警察大隊毒品查緝中心<br>臺北市政府毒品危害防制中心</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680">
                                                <table>
                                                <tr>
                                                    <td colspan="8" width="680"><h3 align="center">我是測試系統違反毒品危害防制條例案件（怠金）處分書</h3></td>
                                                </tr>
                                                <tr>
                                                <td colspan="1" width="280"></td>
                                                <td colspan="7" width="400">發文日期及文號：'.(date('Y', strtotime($suspect->surc_date))-1911).'年'.date('m', strtotime($suspect->surc_date)).'月'.date('d', strtotime($suspect->surc_date)).'日北市警刑毒緝字第'.$suspect->surc_send_num.'號</td>
                                                </tr>
                                                </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77">依據</td><td colspan="7" width="603"><strong>臺北市政府衛生局'.(date('Y', strtotime($suspect->surc_basenumdate))-1911).'年'.date('m', strtotime($suspect->surc_basenumdate)).'月'.date('d', strtotime($suspect->surc_basenumdate)).'日北市衛醫傳防字第'.$suspect->surc_basenum.'號</strong></td>
                                            </tr>
                                                <td rowspan="4" width="77">
                                                <br>
                                                <h3 align="center">受處分人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77"><h3 align="center">法　定<br>代理人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$fname.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$fgender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>'.$fbirth.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$fic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$fphone.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>
                                                        一、新臺幣<strong>'.($fine->f_samount).'</strong>元整<br>
                                                            二、仍須參加講習，講習時地詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續<br>
                                                                      處以怠金。。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>事實</h3></td>
                                                <td colspan="7" width="603">緣受處分人'.$susp->s_name.'涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於民國'.(date('Y', strtotime($sp->fd_lec_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日至'.$sp->fd_lec_place.'參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。</td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">理由及<br>法令依據</h3></td>
                                                <td colspan="7" width="603">
                                                        ■一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習者，依行政執行法規定處以怠金。<br>
                                                        ■二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不能由他人代為履行者，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。<br>
                                                        ■三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續處以怠金。<br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">繳納期限<br>及方式</h3></td>
                                                <td colspan="7" width="603"> 一、怠金限於<strong>民國'.(date('Y', strtotime($fine->f_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日'.$sp->fd_lec_date.'</strong>前選擇下列方式之一繳款：
                                                            (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                            (二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>
                                                        二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong><br>
                                                            (三)輸入本案虛擬帳號：<strong>'.$fine->f_BVC.'</strong>
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。<br>
                                                        四、	匯款繳納時，請於匯款單上之匯款人欄填寫受處分人之姓名「«姓名»」，並應要求於備註(附言)欄填入案件列管編號「«怠金編號»」及身分證統一編號「«身分證號»」，俾利辦理銷案。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">
                                                    一、講習時間：請受處分人出監後，主動連線臺北市政府毒品危害防治中心，另行通知辦理毒品講習。講習相關問題，請逕向臺北市政府毒品危害防制中心洽詢，電話(02)2375-4068。<br>
                                                    二、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：'.$sp->fd_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table> 
                                    <tr>
                                    <td height="180"></td>   
                                    </tr>            
                                    </table> 
                                </div>';				 
            }
			$pdf->writeHTML($html, true, false, true, false, '');       
        $pdf->Output($project.'.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    public function surc_doc_fin()//產生怠金處分書監所pdf(正本)
    {   
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 20;
                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                    $fname = NULL;
                    $fgender = NULL;
                    $fic = NULL;
                    $fbirth = NULL;
                    $fphone = NULL;
                    $fzipcode = NULL;
                    $faddress = NULL;
                if(isset($suspfadai[0])){
                    $suspfadai = $suspfadai[0];
                    $stampBirth = strtotime($susp->s_birth);
                    if ($stampBirth > $stampToday) {
                        $fname =  $suspfadai->s_fadai_name;
                        $fgender = $suspfadai->s_fadai_gender;
                        $fic = $suspfadai->s_fadai_ic;
                        $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                        $fphone = $suspfadai->s_fadai_phone;
                        $fzipcode = $suspfadai->s_fadai_zipcode;
                        $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                    }else{
                        $fname = NULL;
                        $fgender = NULL;
                        $fic = NULL;
                        $fbirth = NULL;
                        $fphone = NULL;
                        $fzipcode = NULL;
                        $faddress = NULL;
                    }
                }
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                                <div style="width:680px" class="panel-body"><p>
                                    <table>
                                        <tr><td ><h2>列管編號：'.$suspect->surc_no .'</h2> </td><td colspan="2"><h2>原處分管制編號：'.$sp->fd_num .'</h2></td></tr>
                                        <tr><td colspan="3">正本：'.$sp->fd_target.'君'.$sp->fd_zipcode.' '.$sp->fd_address.'</td></tr>
                                        <tr><td colspan="3">副本：我是測試系統刑事警察大隊毒品查緝中心<br>臺北市政府毒品危害防制中心</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680">
                                                <table>
                                                <tr>
                                                    <td colspan="8" width="680"><h3 align="center">我是測試系統違反毒品危害防制條例案件（怠金）處分書</h3></td>
                                                </tr>
                                                <tr>
                                                <td colspan="1" width="280"></td>
                                                <td colspan="7" width="400">發文日期及文號：'.(date('Y', strtotime($suspect->surc_date))-1911).'年'.date('m', strtotime($suspect->surc_date)).'月'.date('d', strtotime($suspect->surc_date)).'日北市警刑毒緝字第'.$suspect->surc_send_num.'號</td>
                                                </tr>
                                                </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77">依據</td><td colspan="7" width="603"><strong>臺北市政府衛生局'.(date('Y', strtotime($suspect->surc_basenumdate))-1911).'年'.date('m', strtotime($suspect->surc_basenumdate)).'月'.date('d', strtotime($suspect->surc_basenumdate)).'日北市衛醫傳防字第'.$suspect->surc_basenum.'號</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77">
                                                <br>
                                                <h3 align="center">受處分人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77"><h3 align="center">法　定<br>代理人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$fname.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$fgender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>'.$fbirth.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$fic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$fphone.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>
                                                        一、新臺幣<strong>'.($suspect->surc_amount).'</strong>元整<br>
                                                            二、仍須參加講習，講習時地詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續<br>
                                                                      處以怠金。。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>事實</h3></td>
                                                <td colspan="7" width="603">緣受處分人'.$susp->s_name.'涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於民國'.(date('Y', strtotime($sp->fd_lec_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日至'.$sp->fd_lec_place.'參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。</td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">理由及<br>法令依據</h3></td>
                                                <td colspan="7" width="603">
                                                        ■一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習者，依行政執行法規定處以怠金。<br>
                                                        ■二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不能由他人代為履行者，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。<br>
                                                        ■三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續處以怠金。<br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">繳納期限<br>及方式</h3></td>
                                                <td colspan="7" width="603"> 一、怠金限於<strong>民國'.(date('Y', strtotime($suspect->surc_findate))-1911) .'年'.(date('m', strtotime($suspect->surc_findate))) .'月'.(date('d', strtotime($suspect->surc_findate))) .'日</strong>前選擇下列方式之一繳款：
                                                            (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                            (二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>
                                                        二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong><br>
                                                            (三)輸入本案虛擬帳號：<strong>'.$suspect->surc_BVC.'</strong>
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。<br>
                                                        四、	匯款繳納時，請於匯款單上之匯款人欄填寫受處分人之姓名「'.$suspect->s_name.'」，並應要求於備註(附言)欄填入案件列管編號「'.$suspect->surc_no.'」及身分證統一編號「'.$suspect->s_ic.'」，俾利辦理銷案。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">
                                                    一、可選擇參與實體講習或線上課程（線上課程每年可參與三次，惟應以不同套裝課程申請認定）。
                                                    二、講習時間：民國'.(date('Y', strtotime($suspect->surc_lec_date))-1911) .'年'.(date('m', strtotime($suspect->surc_lec_date))) .'月'.(date('d', strtotime($suspect->surc_lec_date))) .'日（請攜帶有相片之證件報到，逾時將無法入場），地點：'.$suspect->surc_lec_place.'。有正當理由無法參加或要選擇線上課程者，請至下列網址：https://nodrug.gov.taipei下載異動申請表或依指示完成線上課程。講習相關問題，請逕向臺北市政府毒品危害防制中心洽詢，電話(02)2375-4068。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：'.$suspect->surc_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table> 
                                    <tr>
                                    <td height="233"></td>   
                                    </tr>            
                                    </table> 
                                </div>';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function surc_prison()//產生怠金處分書監所pdf（稿）
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 20;
                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                    $fname = NULL;
                    $fgender = NULL;
                    $fic = NULL;
                    $fbirth = NULL;
                    $fphone = NULL;
                    $fzipcode = NULL;
                    $faddress = NULL;
                if(isset($suspfadai[0])){
                    $suspfadai = $suspfadai[0];
                    $stampBirth = strtotime($susp->s_birth);
                    if ($stampBirth > $stampToday) {
                        $fname =  $suspfadai->s_fadai_name;
                        $fgender = $suspfadai->s_fadai_gender;
                        $fic = $suspfadai->s_fadai_ic;
                        $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                        $fphone = $suspfadai->s_fadai_phone;
                        $fzipcode = $suspfadai->s_fadai_zipcode;
                        $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                    }else{
                        $fname = NULL;
                        $fgender = NULL;
                        $fic = NULL;
                        $fbirth = NULL;
                        $fphone = NULL;
                        $fzipcode = NULL;
                        $faddress = NULL;
                    }
                }
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                                <div style="width:680px" class="panel-body"><p>
                                    <table>
                                        <tr><td colspan="3">郵寄 以稿代簽 限制開放 第二層決行 檔號：109/07270399 保存年限：3年</td></tr>
                                        <tr><td ><h2>列管編號：'.$suspect->surc_no .'</h2> </td><td colspan="2"><h2>原處分管制編號：'.$sp->fd_num .'</h2></td></tr>
                                        <tr><td colspan="3">正本：'.$sp->fd_target.'君'.$sp->fd_zipcode.' '.$sp->fd_address.'</td></tr>
                                        <tr><td colspan="3">副本：我是測試系統刑事警察大隊毒品查緝中心、臺北市政府毒品危害防制中心</td></tr>
                                        <tr>
                                        <td width="220"> 校對：</td>
                                        <td width="220"> 監印：</td>
                                        <td width="220">發文：</td>
                                        </tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統違反毒品危害防制條例案件（怠金）處分書(稿)</h3></td>
                                            </tr>
                                            <tr>
                                                <td width="77">依據</td><td colspan="7" width="603"><strong>臺北市政府衛生局'.(date('Y')-1911).'年'.date('m', strtotime($suspect->surc_basenumdate)).'月'.date('d', strtotime($suspect->surc_basenumdate)).'日北市警刑毒緝字第號'.$sp->fd_send_num.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77">
                                                <br>
                                                <h3 align="center">受處分人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77"><h3 align="center">法　定<br>代理人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$fname.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$fgender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>'.$fbirth.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$fic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$fphone.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>
                                                        一、新臺幣<strong>'.($fine->f_samount).'</strong>元整<br>
                                                            二、仍須參加講習，講習時地詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續<br>
                                                                      處以怠金。。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>事實</h3></td>
                                                <td colspan="7" width="603" >緣受處分人'.$susp->s_name.'涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於民國'.(date('Y', strtotime($sp->fd_lec_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日至'.$sp->fd_lec_place.'參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。</td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">理由及<br>法令依據</h3></td>
                                                <td colspan="7" width="603">
                                                        ■一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習者，依行政執行法規定處以怠金。<br>
                                                        ■二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不能由他人代為履行者，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。<br>
                                                        ■三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續處以怠金。<br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">繳納期限<br>及方式</h3></td>
                                                <td colspan="7" width="603"> 一、怠金限於<strong>民國'.(date('Y', strtotime($fine->f_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日'.$sp->fd_lec_date.'</strong>前選擇下列方式之一繳款：
                                                            (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                            (二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>
                                                        二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong><br>
                                                            (三)輸入本案虛擬帳號：<strong>'.$fine->f_BVC.'</strong>
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。<br>
                                                        四、	匯款繳納時，請於匯款單上之匯款人欄填寫受處分人之姓名「«姓名»」，並應要求於備註(附言)欄填入案件列管編號「«怠金編號»」及身分證統一編號「«身分證號»」，俾利辦理銷案。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">
                                                    一、講習時間：請受處分人出監後，主動連線臺北市政府毒品危害防治中心，另行通知辦理毒品講習。講習相關問題，請逕向臺北市政府毒品危害防制中心洽詢，電話(02)2375-4068。<br>
                                                    二、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：'.$sp->fd_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table>
                                    <tr>
                                    <td>承辦單位(2539)</td>   
                                    <td>核稿</td>   
                                    <td>決行</td>   
                                    </tr>            
                                    </table> 
                                    <table>
                                    <tr>
                                    <td height="140"></td>   
                                    </tr>            
                                    </table> 
                                </div>';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    public function surc_delivery()//產生送達文件
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(18.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                //$phone=$phone[0];
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                    <p>我是測試系統 送達證書-<strong></strong><strong>'.$suspect->surc_no.'</strong><strong></strong></p>
                    <table border="1" cellspacing="0" cellpadding="0" width="600">
                        <tbody>
                            <tr>
                                <td width="200" colspan="2" align="center">
                                        受送達人名稱姓名地址
                                </td>
                                <td width="400" colspan="4" valign="top"><h2>'.$susp->s_name.'君</h2>'.$susp->s_dpzipcode.' '.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress.'
                                </td>
                            </tr>
                            <tr> 
                                <td width="200" colspan="2" valign="top" align="center">文       號</td>
                                <td width="400" colspan="4" valign="top" align="center">北市警刑毒緝字第'.$suspect->surc_send_num.'號</td>
                            </tr>
                            <tr>
                                <td width="200" colspan="2" align="center">送 逹 文 書 （含 案 由）</td>
                                <td width="400" colspan="4" align="center"><strong>怠金處分書</strong>
                                </td>
                            </tr>
                            <tr>
                                <td width="100" rowspan="2" align="center">原寄郵局日戳</td>
                                <td width="100" rowspan="2" align="center">送達郵局日戳</td>
                                <td width="300" colspan="3" align="center">送逹處所（由送逹人填記）</td>
                                <td width="100" rowspan="2" align="center">送逹人簽章</td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="left">
                                        □同上記載地址<br>
                                        □改送：
                                </td>
                            </tr>
                            <tr>
                                <td width="100" rowspan="2" valign="top">
                                </td>
                                <td width="100" rowspan="2" valign="top">
                                </td>
                                <td width="300" colspan="3" align="center">送逹時間（由送逹人填記）</td>
                                <td width="100" rowspan="2" align="center">
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="center">
                                        中華民國   年____月____日<br>
                                        午____時___分
                                </td>
                            </tr>
                            <tr>
                                <td width="600" colspan="6" valign="top">
                                        送達方式
                                </td>
                            </tr>
                            <tr>
                                <td width="600" colspan="6" valign="top">
                                        由 送 逹 人 在 □ 上 劃 ˇ 選 記
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top">
                                        □已將文書交與應受送逹人
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        □本人 （簽名或蓋章）
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top">
                                        □未獲會晤本人，已將文書交與有辨別事理能力之同居人、受雇人或應送逹處所之接收郵件人員
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        □同居人<br>
                                        □受雇人 （簽名或蓋章）<br>
                                        □應送逹處所接收郵件人員<br>
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top"><p>
                                        □應受送逹之本人、同居人或受雇人收領後，拒絕或不能簽名或蓋章者，由送逹人記明其事由<br>
                                    </p>
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        送逹人填記：
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top">
                                        □應受送逹之本人、同居人、受雇人或應受送逹處所接收郵件人員無正當理由拒絕領經送逹人將文書留置於送逹處所，以為送逹
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        □本人<br>
                                        □同居人 拒絕收領<br>
                                        □受雇人<br>
                                        □應受送逹處所接收郵件人員<br>
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="center">
                                        □未獲會晤本人亦無受領文書之同居人、受雇人或應受送逹處所接收郵件人員，已將該送逹文書：<br>
                                        □應受送逹之本人、同居人、受雇人或應受送逹處所接收郵件人員無正當理由拒絕收領，並有難逹留置情事，已將該送逹文書：
                                </td>
                                <td width="180" colspan="3" align="left">
                                        □寄存於________派出所<br>
                                        □寄存於_鄉（鎮、市、區）<br>
                                        公所<br>
                                        □寄存於 鄉（鎮、市、區）<br>
                                        公所<br>
                                        村（里）辦公處<br>
                                        □寄存於 郵局<br>
                                </td>
                                <td width="120" valign="top">
                                        並作送達通知書二份，一份黏貼於應受送逹人住居所、事務所、營業所或其就業處所門首，一份□交由鄰居轉交或□置於該受送逹處所信箱或其他適當位置，以為送逹。
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="center">
                                        送 達 人 注 意 事 項
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        一、依上述送達方法送達者，送達人應即將本送達證書，提出於交送達之行政機關附卷。<br>
                                        二、不能依上述送達方法送達者，送達人應製作記載該事由之報告書，提出於交送達之行政機關附卷，並繳回應送達之文書。
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p>
                        ※ <strong>請繳回 我是測試系統刑事警察大隊 地址：10042臺北市中正區武昌街一段69號</strong>
                    </p>
                    <p>
                        <strong>單位：毒品查緝中心(</strong>
                        <strong>'.$suspect->surc_empno.'</strong>
                        <strong>)</strong>
                    </p>
                    <p>
                        我是測試系統交郵送達格式
                    </p>
                    <table>
                    <tr>
                    <td height="270"></td>   
                    </tr>            
                    </table> '
                    ;
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function surc_delivery_prison()//產生送達文件
    {
        $this->load->library('PHP_TCPDF16');
        $project = $this->uri->segment(3);
        $uname = $this->session-> userdata('uname');
        $query = $this->getsqlmod->getsurchangelist1($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF16 (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
	        //$this->SetFont('msungstdlight', '', 16);
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(12.5, 13.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                //$phone=$phone[0];
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                    <div align="center">
                        <table border="1" cellspacing="0" cellpadding="0" width="640">
                            <tbody>
                                <tr>
                                    <td width="640" colspan="8">
                                            <strong>我是測試系統違反毒品危害條例處分送達證書</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="160" colspan="2">
                                            應送達之文書
                                    </td>
                                    <td width="480" colspan="6">
                                            名稱：查獲違反毒品危害防制條例案件處分書
                                    </td>
                                </tr>
                                <tr>
                                    <td width="160" colspan="4">
                                            應受送達人
                                    </td>
                                    <td width="480" colspan="4">
                                            '.$suspect->s_name.' 君<strong></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="640" colspan="8">
                                            由送達人劃v選記
                                    </td>
                                </tr>
                                <tr>
                                    <td width="28">
                                    </td>
                                    <td width="32">
                                        <p align="center">
                                            一
                                        </p>
                                    </td>
                                    <td width="580" colspan="6">
                                        <p>
                                            已將文書交與應受達本人。
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="28" rowspan="2">
                                    </td>
                                    <td width="32" rowspan="2">
                                            二
                                    </td>
                                    <td width="240" colspan="3" rowspan="2">未會晤本人已將文書交與有辨別事理能力之下列
                                    </td>
                                    <td width="100" colspan="2">
                                            同居人
                                    </td>
                                    <td width="240">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100" colspan="2">
                                            受雇人
                                    </td>
                                    <td width="240">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="28">
                                    </td>
                                    <td width="32">
                                            三
                                    </td>
                                    <td width="580" colspan="6" align="left"><p>未會晤本人亦無收領文書之同居人、受雇人已將文書寄存於，並作送達通知書兩份，一份黏貼於應受送達人門首<strong>並拍照
                                    </strong>，一份交由鄰居轉交或置放於送達處所信箱或其他適當位置。</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="28" rowspan="3">
                                    </td>
                                    <td width="32" rowspan="3">
                                            四
                                    </td>
                                    <td width="240" colspan="3">
                                            本人
                                    </td>
                                    <td width="340" colspan="3" rowspan="3">
                                            無正當理由拒絕收領，已將文書留置於該處，以為送達。
                                    </td>
                                </tr>
                                <tr>
                                    <td width="240" colspan="4">
                                            同居人
                                    </td>
                                </tr>
                                <tr>
                                    <td width="240" colspan="4">
                                            受雇人
                                    </td>
                                </tr>
                                <tr>
                                    <td width="60" colspan="2">
                                            五
                                    </td>
                                    <td width="580" colspan="6">
                                            <strong>收領人簽名或蓋章</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="60" colspan="2">
                                            六
                                    </td>
                                    <td width="580" colspan="6">
                                            <strong>收領人拒絕或不能簽名或蓋章</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150" colspan="3">
                                            送達日期時間
                                    </td>
                                    <td width="490" colspan="5">
                                            中華民國　 年 月　 日 午　 時 分（由送達人填記）
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150" colspan="3">
                                            送達處所
                                    </td>
                                    <td width="490" colspan="5">
                                            <strong>宜蘭縣三星鄉三星路</strong>
                                            <strong>3</strong>
                                            <strong>段</strong>
                                            <strong>365</strong>
                                            <strong>巷安農新村</strong>
                                            <strong>1</strong>
                                            <strong>號（法務部矯正署宜蘭監獄）</strong>
                                            <strong></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="640" colspan="8">
                                            中華民國　　年　　月　　日製作（由送達人填記）
                                            　　<strong>送達人（簽章）</strong><strong></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150" colspan="3">
                                            注意事項
                                    </td>
                                    <td width="490" colspan="5">
                                        一、「一」、「二」、「三」、「四」等欄位，係所送達方式在該欄位勾選為記。
                                        <p>二、遇有「六」欄情形時，送達人應記明其事由。</p>
                                        <p><strong>三、代領人應填註「關係」及「身分證字號」。</strong></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <p>
                        ※ <strong>請繳回 我是測試系統刑事警察大隊 地址：10042臺北市中正區武昌街一段69號</strong>
                    </p>
                    <p>
                        <strong>單位：毒品查緝中心('.$uname.')</strong>
                    </p>
                    <table >
                    <tbody>
                    <tr>
                    <td height="70"></td>   
                    </tr>
                    </tbody>
                    </table>
                    
                    ';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'_delivery.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function surc_callDelivery()//產生送達文件
    {
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $uname = $this->session-> userdata('uname');
        $query = $this->getsqlmod->getCallListfromProject($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
	        //$this->SetFont('msungstdlight', '', 16);
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(12.5, 13.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
			$pdf->SetFont('twkai98_1','',10);
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                ////$phone=$phone[0];
                if(!isset($phone[0]))$phone1=null;
                if(isset($phone[0]))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                // $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                if($suspect->fd_address == null || $suspect->fd_address == '0'){$address= $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;}
                else { $address=$suspect->fd_address;}

				$target_str = '';
				$target_str2 = '';
				$target_str2_addr = '';
				if(null != $sp->fd_address && count(explode(",", $sp->fd_address)) > 1)
				{
					$fd_name = explode(",", $sp->fd_target)[0];
					$fd_addr = explode(",", $sp->fd_address)[0];
					$fd_zip = explode(",", $sp->fd_zipcode)[0];
					// $target_str = $fd_name.' 君 <br>            '. $fd_zip . ' ' . $fd_addr;
					// $target_str .= '<br/>            ';
					$fd_name1 = explode(",", $sp->fd_target)[1];
					$fd_addr1 = explode(",", $sp->fd_address)[1];
					$fd_zip1 = $susp->s_dpzipcode;
					$target_str .=  $fd_name.'('.$fd_name1.'法定代理人) 君 <br>'. $fd_zip1 . ' ' . $fd_addr1;
					$target_str2 =  $fd_name.'('.$fd_name1.'法定代理人) 君';
					$target_str2_addr = $fd_addr1;
				}
				else
				{
					$target_str = $sp->fd_target.' 君 <br>'. $sp->fd_zipcode . ' ' . $sp->fd_address ;
					$target_str2 = $sp->fd_target.' 君';
					$target_str2_addr = $sp->fd_address;
				}
                
				// 在監送達證書格式是另一種
				if($susp->s_live_state != 3)
				{
					$pdf->SetMargins(20, 15, 20,true);
					$html ='
					<div style="width:600px" class="panel-body">
						<table width="600">
							<tr>
								<td align="right" style="height:30px;line-height:30px;"><h3>我是測試系統   </h3></td>
								<td align="left" style="height:30px;line-height:30px;"><h3>   送達證書-<strong>'.$suspect->fd_num.'</strong></h3></td>
							</tr>
						</table>
						<table width="600" cellspacing="0" cellpadding="0" border="1">
							<tr>
								<td colspan="2" width="240" align="center" style="height:60px;line-height:75px;"><h5>受送達人名稱姓名地址</h5></td>
								<td colspan="2" width="360" style="height:50px;line-height:20px;"><h2>'.$susp->s_name.'君</h2>'.$suspect->fd_zipcode.$address.'</td>
							</tr>
							<tr>
								<td colspan="2" width="240" align="center" style="height:20px;line-height:25px;letter-spacing:108px;"><h5>文號</h5></td>
								<td colspan="2" width="360" style="height:20px;line-height:20px;">北市警刑毒緝字第'. $suspect->call_no . $suspect->call_bno.'號</td>
							</tr>
							<tr>
								<td colspan="2" width="240" align="center" style="height:20px;line-height:25px;letter-spacing:7px;"><h5>送達文書(含案由)</h5></td>
								<td colspan="2" width="360" style="height:20px;line-height:20px;"><h4>違反毒品危害防制條例案件  <strong>處分書</strong></h4></td>
							</tr>
							<tr>
								<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"><h5>原寄郵局日戳</h5></td>
								<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"><h5>送達郵局日戳</h5></td>
								<td align="center" width="240" style="height:20px;line-height:25px;"><h5>送達處所 (由送達人填記)</h5></td>
								<td align="center" rowspan="2" width="120" style="height:60px;line-height:75px;"><h5>送達人簽章</h5></td>
							</tr>
							<tr>
								<td width="240" style="height:40px;line-height:25px;"><h5>☐同上記載地址 <br/>☐改送:</h5></td>
							</tr>
							<tr>
								<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"></td>
								<td rowspan="2" width="120" align="center" style="height:60px;line-height:75px;"></td>
								<td align="center" width="240" style="height:20px;line-height:25px;"><h5>送達處所 (由送達人填記)</h5></td>
								<td align="center" rowspan="2" width="120" style="height:60px;line-height:75px;"></td>
							</tr>
							<tr>
								<td width="240" style="height:40px;line-height:25px;">
									<table>
										<tr>
											<td>中華民國</td>
											<td align="right">年</td>
											<td align="right">月</td>
											<td align="right">日</td>
										</tr>
										<tr>
											<td></td>
											<td align="right">午</td>
											<td align="right">時</td>
											<td align="right">分</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center" colspan="4" width="600" style="height:20px;line-height:25px;letter-spacing:135px;">送達方式</td>
							</tr>
							<tr>
								<td align="center" colspan="4" width="600" style="height:20px;line-height:25px;letter-spacing:33px;">由送達人在☐上劃V選記</td>
							</tr>
						</table>
						<table width="600" cellspacing="0" cellpadding="0" border="1">
							<tr>
								<td colspan="2" width="280" style="height:30px;line-height:35px;"><h5>☐已將文書交與應受送達人</h5></td>
								<td colspan="2" width="320" style="height:30px;line-height:35px;">
									<table>
										<tr>
											<td width="200"><h5>☐本人</h5></td>
											<td width="120"><h5>(簽名或蓋章)</h5></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" width="280" style="height:30px;line-height:15px;"><h5>☐未獲會晤本人，已將文書交與有辨別事理能力之同居人、受僱人或應送達處所之接收郵件人員</h5></td>
								<td colspan="2" width="320" style="height:30px;line-height:15px;">
									<table>
										<tr>
											<td width="200"><h5>☐同居人<br/>☐受僱人<br/>☐應送達處所接收郵件人員</h5></td>
											<td width="120"><h5><br/>(簽名或蓋章)</h5></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" width="280" style="height:60px;line-height:20px;"><h5>☐應受送達之本人、同居人或受雇人收領後，拒絕或不能簽名或蓋章者，由送達人記明其事由</h5></td>
								<td colspan="2" width="320" style="height:60px;line-height:20px;">
									<table>
										<tr>
											<td width="200"><h5>送達人填記﹔</h5></td>
											<td width="120"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" width="280" style="height:60px;line-height:15px;"><h5>☐應受送達之本人、同居人、受僱人或應受送達處所接收郵件人員無正當理由拒絕領經送達人將文書留置於送達處所，以為送達</h5></td>
								<td colspan="2" width="320" style="height:60px;line-height:15px;">
									<table>
										<tr>
											<td width="200"><h5>☐本人<br/>☐同居人<br/>☐受僱人<br/>☐應受送達處所接收郵件人員</h5></td>
											<td width="120"><h5><br/>拒絕收領</h5></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" width="280" style="height:140px;line-height:15px;"><h5>☐未獲會晤本人亦無受領文書之同居人、受僱人或應受送達處所接收郵件人員，已將該送達文書﹔<br/><br/>☐應受送達之本人、同居人、受僱人或應受送達處所接收郵件人員無正當理由拒絕收領，並有難達留置情事，已將該送達文書﹔</h5></td>
								<td colspan="2" width="160" style="height:140px;line-height:20px;">
									<h5>☐寄存於_____________派出所<br/>☐寄存於_________鄉(鎮、市、區)__________公所<br/>☐寄存於_________鄉(鎮、市、區)__________公所__________村(里)辦公處<br/>☐寄存於_______________郵局</h5>
								</td>
								<td colspan="2" width="160" style="height:140px;line-height:15px;">
									<h5>並作送達通知書二份，一份黏貼於應受送達人住居所、事務所、營業所或其就業處所門首，一份☐交由鄰居轉交或☐置於該受送達處所信箱或其他適當位置，以為送達。</h5>
								</td>
							</tr>
							<tr>
								<td align="center" colspan="2" width="280" style="height:120px;line-height:130px;letter-spacing:20px;"><h5>送達人注意事項</h5></td>
								<td colspan="2" width="320" style="height:120px;line-height:20px;">
									<h5>一、依上述送達方法送達者，送達人應即將本送達證書，提出於交送達之行政機關附卷。</h5>
									<h5>二、不能依上述送達方法送達者，送達人應製作記載該事由之報告書，提出於交送達之行政機關附卷，並缴回應送達之文書。</h5>
								</td>
							</tr>
						</table>
						<table>
							<tr>
								<td>
									<h4>※ 請繳回 我是測試系統刑事警察大隊  地址﹔103024臺北市大同區重慶北路2段219號</h4>
								</td>
							</tr>
							<tr>
								<td>
									<h4>單位﹔毒品查緝中心('.$uname.')</h4>
								</td>
							</tr>
							<tr>
								<td align="right">
									<h6>我是測試系統交郵送達格式</h6>
								</td>
							</tr>
						</table>
					</div>';
				}
				else
				{
					$pdf->SetMargins(15, 15, 15,true);
					$html ='
					<div style="width:630px" class="panel-body">
						<table width="630px" cellspacing="0" cellpadding="0" border="1">
							<tr>
								<td align="center" colspan="2" style="height:60px;line-height:40px;"><h2>我是測試系統違反毒品危害條例處分送達證書</h2></td>
							</tr>
							<tr>
								<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:5px;"><h2>應送達之文書</h2></td>
								<td width="480" style="height:60px;line-height:20px;"><h2>名稱﹔查獲違反毒品危害防制條例案件處分書<br/>件數﹔1件（案件列管編號﹔'.$suspect->fd_num.'）</h2></td>
							</tr>
							<tr>
								<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:10.5px;"><h2>應受送達人</h2></td>
								<td width="480" style="height:60px;line-height:40px;"><h2>'.$susp->s_name.'君</h2>'.$suspect->fd_zipcode.$address.'</td>
							</tr>
							<tr>
								<td align="center" colspan="4" style="height:20px;line-height:25px;letter-spacing:55px;"><h3>由送達人劃V選記</h3></td>
							</tr>
						</table>
						<table width="630px" cellspacing="0" cellpadding="0" border="1">
							<tr>
								<td width="30" style="height:20px;line-height:25px;"></td>
								<td width="30" align="center" style="height:20px;line-height:25px;"><h3>一</h3></td>
								<td width="570" style="height:20px;line-height:25px;"><h3>已將文書交與應受達本人。</h3></td>
							</tr>
							<tr>
								<td rowspan="2" width="30" style="height:20px;line-height:25px;"></td>
								<td rowspan="2" width="30" align="center" style="height:20px;line-height:50px;"><h3>二</h3></td>
								<td rowspan="2" width="230" style="height:20px;line-height:25px;"><h3>未會晤本人已將文書交與有辨別事理能力之下列</h3></td>
								<td width="100" align="center" style="height:20px;line-height:25px;letter-spacing:16px;"><h3>同居人</h3></td>
								<td width="240" align="center" style="height:20px;line-height:25px;"></td>
							</tr>
							<tr>
								<td width="100" align="center" style="height:20px;line-height:25px;letter-spacing:16px;"><h3>受僱人</h3></td>
								<td width="240" align="center" style="height:20px;line-height:25px;"></td>
							</tr>
							<tr>
								<td width="30" style="height:20px;line-height:25px;"></td>
								<td width="30" align="center" style="height:20px;line-height:55px;"><h3>三</h3></td>
								<td width="570" style="height:20px;line-height:20px;"><h3>未會晤本人亦無收領文書之同居人、受僱人已將文書寄存於_____________________，並作送達通知書兩份，一份黏貼於應受送達人門首並拍照，一份交由鄰居轉交或置放於送達處所信箱或其他適當位置。</h3></td>
							</tr>
							<tr>
								<td rowspan="3" width="30" style="height:50px;line-height:60px;"></td>
								<td rowspan="3" width="30" align="center" style="height:60px;line-height:80px;"><h3>四</h3></td>
								<td width="230" style="height:20px;line-height:25px;"><h3>本人</h3></td>
								<td rowspan="3" width="340" style="height:50px;line-height:15px;"><br/><h3>無正當理由拒絕收領，已將文書留置於該處，以為送達。</h3></td>
							</tr>
							<tr>
								<td width="230" style="height:20px;line-height:25px;"><h3>同居人</h3></td>
							</tr>
							<tr>
								<td width="230" style="height:20px;line-height:25px;"><h3>受僱人</h3></td>
							</tr>
							<tr>
								<td width="60" align="center" style="height:60px;line-height:75px;"><h3>五</h3></td>
								<td width="570" style="height:60px;line-height:65px;"><h2>收領人簽名或蓋章</h2></td>
							</tr>
							<tr>
								<td width="60" align="center" style="height:60px;line-height:75px;"><h3>六</h3></td>
								<td width="570" style="height:60px;line-height:65px;"><h2>收領人拒絕或不能簽名或蓋章</h2></td>
							</tr>
						</table>
						<table width="630px" cellspacing="0" cellpadding="0" border="1">
							<tr>
								<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:5px;"><h2>送達日期時間</h2></td>
								<td width="480" style="height:60px;line-height:20px;"><h2>中華民國____年____月____日____午____時____分（由送達人填記）</h2></td>
							</tr>
							<tr>
								<td align="center" width="150" style="height:60px;line-height:40px;letter-spacing:10.5px;"><h2>送達處所</h2></td>
								<td width="480" style="height:60px;line-height:40px;"><h2>'.$target_str2_addr.'</h2></td>
							</tr>
							<tr>
								<td colspan="2" align="center" width="630" style="height:60px;line-height:20px;">
									<h2>中華民國____年____月____日製作（由送達人填記）</h2>
									<h1>送達人（簽章）</h1>
								</td>
							</tr>
							<tr>
								<td align="center" width="150" style="height:60px;line-height:70px;letter-spacing:10.5px;"><h2>注意事項</h2></td>
								<td width="480" style="height:60px;line-height:15px;">
									<table>
										<tr>
											<td width="40" style="line-height:20px;"><h2>一、</h2></td>
											<td width="440" style="line-height:20px;"><h2>「一」、「二」、「三」、「四」等欄位，係所送達方式在該欄位勾選為記。</h2></td>
										</tr>
										<tr>
											<td width="40"><h2>二、</h2></td>
											<td width="440"><h2>遇有「六」欄情形時，送達人應記明其事由。</h2></td>
										</tr>
										<tr>
											<td width="40"><h2>三、</h2></td>
											<td width="440"><h2>代領人應填註「關係」及「身分證字號」。</h2></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table>
							<tr>
								<td>
									<h5>※ 請繳回 我是測試系統刑事警察大隊  地址﹔103024臺北市大同區重慶北路2段219號</h5>
								</td>
							</tr>
							<tr>
								<td>
									<h5>單位﹔毒品查緝中心('.$uname.')</h5>
								</td>
							</tr>
						</table>';
				}
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'_delivery.pdf', 'I');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function surc_Call()//產生催繳函
    {
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $uname = $this->session-> userdata('uname');
        $query = $this->getsqlmod->getCallListfromProject($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
	        //$this->SetFont('msungstdlight', '', 16);
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(20.5, 13.5, 20.5,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
			$pdf->SetFont('twkai98_1','',10);
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                ////$phone=$phone[0];
                //if(!isset($phone[0]))$phone1=null;
                //if(isset($phone[0]))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                // $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                    <div>正 本<h2 align="center">我是測試系統刑事警察大隊   函</h2>
                        <table cellpadding="0" cellspacing="0" >
                            <tbody>
                                <tr>
                                    <td width="300"></td><td width="70"></td>
                                    <td rowspan=3 width="220">地址：10042臺北市中正區武昌街1段69號
                                    </td>
                                </tr>
                                <tr>
                                    <td width="300">'.$suspect->fd_zipcode.'<br>'.$suspect->fd_address.'</td><td width="70"></td><td>承辦人：'.$uname.'<br>電話：02-23932397</td>
                                </tr>
                                <tr>
                                    <td width="300"><h2>受文者：'.$suspect->s_name.' 君</h2></td><td width="70"></td><td>傳真：02-23932311<br>電子信箱：andy33@ police.taipei</td>
                                </tr>
                            </tbody>
                        </table>
                        
                            發文日期：<strong>中華民國</strong><strong>'.(date('Y', strtotime($suspect->call_date))-1911) .'年'.(date('m', strtotime($suspect->call_date))) .'月'.(date('d', strtotime($suspect->call_date))) .'日'.'</strong><strong></strong>
                            <br>發文字號：北市警刑大毒緝字第<strong>10930546347</strong>號<br>速別：普通件<br>密等及解密條件或保密期限：<br>附件：<br>
                        <br ><table cellpadding="0" cellspacing="0" >
                            <tbody>
                                <tr>
                                    <td width="60"><h2>主旨：</h2></td>
                                    <td width="510"><h2>臺端違反毒品危害防制條例一案，經我是測試系統依法處分罰鍰新臺幣'.$fine->f_damount/10000 .'萬元整(案件列管編號：'.$suspect->fd_num.')，已逾繳納期限('.$this->tranfer2RCyearTrad($fine->f_date).')迄未清償，請儘速於'.$this->tranfer2RCyearTrad($suspect->call_grace).'前完納，逾期不完納將依法移送法務部行政執行署所屬分署強制執行，請查照。
                                    </h2></td>
                                </tr>
                                <tr>
                                    <td width="60"><h2>說明：</h2></td>
                                    <td width="510"><h2>繳納罰鍰請至各金融機構填具匯款單匯款，注意事項如下：<br>一、行庫：台北富邦銀行公庫處<br>二、帳號：16112470361019。<br>三、戶名：我是測試系統刑事警察大隊。<br>四、非受處分人本人匯款時，應於備註欄輸入<u>受處分人之姓名</u>
                                    <br>及<u>身分證統一編號</u>，俾利辦理銷案。
                                    </h2></td>
                                </tr>
                            </tbody>
                        </table>
                        <br><br><br>正本：'.$suspect->s_name.' 君
                        <br>副本：
                        <br>
                        <table >
                        <tbody>
                        <tr>
                        <td height="430"></td>   
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    <br clear="all"/>
                    ';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'_delivery.pdf', 'I');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }

	public function surc_call_di(){
		$project = $this->uri->segment(3);
        $uname = $this->session-> userdata('uname');
        $query = $this->getsqlmod->getCallListfromProject($project)->result(); 
		foreach ($query as $suspect)
		{
			$id = $suspect->s_num;
			$susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
			$susp=$susp[0];
			$fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
			$fine=$fine[0];
			$DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
			$sp1=$this->getsqlmod->get3SP1($id)->result();
			$sp=$sp1[0];
			$spcount=$this->getsqlmod->getfdAll()->result();
			$cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
			$cases=$cases[0];
			$phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
			////$phone=$phone[0];
			//if(!isset($phone[0]))$phone1=null;
			//if(isset($phone[0]))$phone1=$phone[0]->p_no;
			$drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
			$drug=$drug[0];
			$drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
			// $drug2=$drug2[0];
			$prison=$this->getsqlmod->getprison()->result();
			$prison=$prison[0];

			$datet = '中華民國'.$this->tranfer2RCyearTrad($suspect->call_date);

			$dom = new DOMDocument("1.0","utf-8"); 
			$implementation = new DOMImplementation();
			$dom->appendChild($implementation->createDocumentType('函 SYSTEM "104_2_utf8.dtd" [<!ENTITY 表單 SYSTEM "函.sw" NDATA DI><!NOTATION DI SYSTEM ""><!NOTATION _X SYSTEM "">]')); 
			// display document in browser as plain text 
			// for readability purposes 
			header("Content-Type: text/plain"); 
			// create root element 
			$root = $dom->createElement("函"); 
			$dom->appendChild($root); 
			// create child element 
			
			$item = $dom->createElement("發文機關"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("全銜"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("我是測試系統刑事警察大隊"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("機關代碼"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("379130200C"); 
            $item2->appendChild($text); 
            
            $item = $dom->createElement("函類別"); 
            $item->setAttribute("代碼", "函");  
			$root->appendChild($item); 
            
            $item = $dom->createElement("地址"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("10042臺北市中正區武昌街一段69號"); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("承辦人：".$uname); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("電話：(02)2393-2397"); 
            $item->appendChild($text); 

            $item = $dom->createElement("聯絡方式"); 
            $root->appendChild($item); 
            $text = $dom->createTextNode("電子信箱：andy33@tcpd.gov.tw"); 
            $item->appendChild($text); 

            $item = $dom->createElement("受文者"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("交換表"); 
            $item2->setAttribute("交換表單", "表單");  
            $item->appendChild($item2); 

            $item = $dom->createElement("發文日期"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("年月日"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode($datet); 
            $item2->appendChild($text); 
            
            $item = $dom->createElement("發文字號"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("字"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("北市警刑大毒緝"); 
            $item2->appendChild($text); 
            $item2 = $dom->createElement("文號"); 
            $item->appendChild($item2); 
            $item3 = $dom->createElement("年度"); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode((date('Y')-1911)); 
            $item3->appendChild($text); 
            $item3 = $dom->createElement("流水號"); 
            $item2->appendChild($item3);
			$item3 = $dom->createElement("支號"); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode($suspect->	call_bno); 
            $item3->appendChild($text);
            // $text = $dom->createTextNode($susp->f_project_num); //日後再確認
            // $text = $dom->createTextNode($susp->f_project_id);
            // $item3->appendChild($text); 
            // $item3 = $dom->createElement("支號"); 
            // $item2->appendChild($item3); 
            
            $item = $dom->createElement("速別"); 
            $item->setAttribute("代碼", "普通件");  
            $root->appendChild($item); 
            
            $item = $dom->createElement("密等及解密條件或保密期限"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("密等"); 
            $item->appendChild($item2); 
            $item2 = $dom->createElement("解密條件或保密期限"); 
            $item->appendChild($item2); 
            $item = $dom->createElement("附件"); 
            $root->appendChild($item); 
            $item2 = $dom->createElement("文字"); 
            $item->appendChild($item2); 

			$text = $dom->createTextNode(""); 
			$item2->appendChild($text); 
			
			$item = $dom->createElement("主旨"); 
			$root->appendChild($item); 
			$item2 = $dom->createElement("文字"); 
			$item->appendChild($item2);

			$text = $dom->createTextNode('臺端違反毒品危害防制條例一案，經我是測試系統依法處分罰鍰新臺幣'.$fine->f_damount/10000 .'萬元整(案件列管編號：'.$suspect->fd_num.')，已逾繳納期限('.$this->tranfer2RCyearTrad($fine->f_date).')迄未清償，請儘速於'.$this->tranfer2RCyearTrad($suspect->call_grace).'前完納，逾期不完納將依法移送法務部行政執行署所屬分署強制執行，請查照。'); 
			$item2->appendChild($text); 

			$item = $dom->createElement("段落"); 
            $item->setAttribute("段名", "說明：");  
            $root->appendChild($item); 
            $item2 = $dom->createElement("文字"); 
            $item->appendChild($item2); 
            $text = $dom->createTextNode("繳納罰鍰請至各金融機構填具匯款單匯款，注意事項如下："); 
            $item2->appendChild($text); 
            
            $item2 = $dom->createElement('條列'); 
            $item2->setAttribute("序號", "一、");  
            $item->appendChild($item2); 
            $item3 = $dom->createElement('文字'); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode("行庫：台北富邦銀行公庫處。");
            $item3->appendChild($text); 

			$item2 = $dom->createElement('條列'); 
            $item2->setAttribute("序號", "二、");  
            $item->appendChild($item2); 
            $item3 = $dom->createElement('文字'); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode("帳號：16112470361019。");
			$item3->appendChild($text); 

			$item2 = $dom->createElement('條列'); 
            $item2->setAttribute("序號", "三、");  
            $item->appendChild($item2); 
            $item3 = $dom->createElement('文字'); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode("戶名：我是測試系統刑事警察大隊。");
            $item3->appendChild($text); 

			$item2 = $dom->createElement('條列'); 
            $item2->setAttribute("序號", "四、");  
            $item->appendChild($item2); 
            $item3 = $dom->createElement('文字'); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode("非受處分人本人匯款時，應於備註欄輸入受處分人之姓名及身分證統一編號，俾利辦理銷案。");
            $item3->appendChild($text); 

			$item = $dom->createElement("正本"); 
			$root->appendChild($item); 
			$item2 = $dom->createElement('全銜'); 
			$item2->setAttribute("發文方式", "人工郵寄");  
			$item->appendChild($item2); 
			$text = $dom->createTextNode($suspect->s_name); 
			$item2->appendChild($text); 
			$item3 = $dom->createElement('郵遞區號'); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode($suspect->fd_zipcode);
            $item3->appendChild($text);
			$item3 = $dom->createElement('地址'); 
            $item2->appendChild($item3); 
            $text = $dom->createTextNode($suspect->fd_address);
            $item3->appendChild($text);
			$item3 = $dom->createElement('通訊錄名稱'); 
			$item3->setAttribute("單位代碼", "");
			$item3->setAttribute("機關代碼", "");  
            $item2->appendChild($item3); 
            $text = $dom->createTextNode($suspect->s_name);
            $item3->appendChild($text);

			$item = $dom->createElement("署名"); 
			$root->appendChild($item); 

			$xmls[] = $dom->saveXML();  

		}
		$this->load->library('zip');
        //var_dump($xmls);
        $i=0;
        foreach ($xmls as $key) {
            $i++;
            $name = $i.'.di';
            $data = $key;
            $this->zip->add_data($name, $data);
        }
        //$this->zip->archive('temp/'.$datet.'.zip');
        $this->zip->download($query[0]->call_no.'.zip');
        unlink($query[0]->call_no .'.zip');

		
	}
    
    public function surc_Traf()//產生移送執行函
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $uname = $this->session-> userdata('uname');
        $query = $this->getsqlmod->getFTListfromProject($project); 
        $count = $query->num_rows();
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
	        //$this->SetFont('msungstdlight', '', 16);
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(20.5, 13.5, 20.5,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                //if(isset($phone[0]))//$phone=$phone[0];
                if(!isset($phone))$phone1=null;
                //if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                // $drug2=$drug2[0];
				$drug2 = null;
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                    <div><table cellpadding="0" cellspacing="0" >
                            <tbody>
                                <tr>
                                    <td width="100">以稿代簽</td><td width="100"> 一層決行</td><td width="100">限制開放 </td><td width="170"></td>
                                    <td rowspan=3 width="120">檔 號:109/07270399/1<br>保存年限：3年
                                    </td>
                                </tr>
                            </tbody>
                        </table><h2 align="center">我是測試系統刑事警察大隊   函(稿)</h2>
                        <table cellpadding="0" cellspacing="0" >
                            <tbody>
                                <tr>
                                    <td width="300">校對<br><br>監印<br><br>發文</td><td width="70"></td>
                                    <td rowspan=3 width="220"><br><br><br>地址：10042臺北市中正區武昌街1段69號<br>承辦人：'.$uname.'<br>電話：02-23932397<br>傳真：02-23932311
                                    </td>
                                </tr>
                                <tr>
                                    <td width="300">'.$suspect->fd_zipcode.'<br>'.$suspect->fd_address.'</td><td width="70"></td><td>電子信箱：andy33@ police.taipei</td>
                                </tr>
                                <tr>
                                    <td width="300"><h2>受文者：'.$suspect->s_name.' 君</h2></td><td width="70"></td><td></td>
                                </tr>
                            </tbody>
                        </table>
                        
                            發文日期：<strong>中華民國</strong><strong>'.(date('Y', strtotime($suspect->ft_send_date))-1911) .'年'.(date('m', strtotime($suspect->ft_send_date))) .'月'.(date('d', strtotime($suspect->ft_send_date))) .'日'.'</strong><strong></strong>
                            <br>發文字號：北市警刑大毒緝字第<strong>10930546347</strong>號<br>速別：普通件<br>密等及解密條件或保密期限：<br>附件：移送卷資及清冊(附件隨文發出，移送書電子檔另以電子郵件傳送)<br>
                        <br ><table cellpadding="0" cellspacing="0" >
                            <tbody>
                                <tr>
                                    <td width="60"><h2>主旨：</h2></td>
                                    <td width="510"><h2>檢送本大隊辦理違反毒品危害防治條例罰緩義務人'.$suspect->s_name.'等'.$count.'案移送書正本暨相關卷資各1份移請執行，請依權責捉出，請查照。</h2></td>
                                </tr>
                                <tr>
                                    <td width="60"><h2>說明：</h2></td>
                                    <td width="510"><h2><br>一、依據行政執行第11條辦理。<br>二、旨揭卷資內含義務人'.$suspect->s_name.'等'.$count.'案（詳如清冊）移送書正本及違反毒品危害防制條列處分書等卷資影本各1份，為行政作業需要，請以匯款方式至本大隊保管進專戶（行庫：臺北富邦銀行公庫出、戶名：我是測試系統刑事警察大隊。、帳號：16112470361019）。<br>三、移送案件所需郵資，請貴分署一本大隊申辦之中華郵政（股）公司'.$suspect->ft_target_post.'郵局郵件特約用戶（特約編號：241057000064）核實使用。
                                    </h2></td>
                                </tr>
                            </tbody>
                        </table>
                        <br><br><br>正本：法務部行政執行署'.$suspect->ft_target.'分署（'.$suspect->ft_target_address.'）
                        <br>副本：
                        <br>抄本：
                        <br>
                        <br><h2>大隊長陳OO</h2>
                        <table >
                        <tbody>
                        <tr>
                        <td height="217"></td>   
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    <br clear="all"/>
                    ';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        //$pdf->Output($project.'_delivery.pdf', 'D');    //download
        $pdf->Output($project.'.pdf', 'D');    //read
    }
    
    
    public function surc_Trafdoc()//產生移送書
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $uname = $this->session-> userdata('uname');
        $query = $this->getsqlmod->getFTListfromProject($project); 
        $count = $query->num_rows();
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
	        //$this->SetFont('msungstdlight', '', 16);
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(20.5, 13.5, 20.5,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                ////$phone=$phone[0];
                if(!isset($phone))$phone1=null;
                //if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                // $drug2=$drug2[0];
				$drug2 = null;
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                    <table cellpadding="0" cellspacing="0" align="left">
                        <tbody>
                            <tr>
                                <td width="309">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td width="200" rowspan="2"><strong>原案件編號：</strong><u><font size="18" >'.$suspect->fd_num.'</font></u></td>
                                                <td colspan="3" width="400">承辦人：陳昌澂 聯絡電話：02-2393-2397傳真電話:2393-2311</td>
                                                
                                            </tr>
                                            <tr>
                                                <td width="250"></td>
                                                <td Border="1" Cellspacing="0" align="right" width="50">移送案號</td>
                                                <td Border="1" Cellspacing="0" align="center" width="90">'.$suspect->ft_no.'</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="1" cellspacing="0" cellpadding="0" align="left" width="606">
                        <tbody>
                            <tr border="0">
                                <td  width="606" colspan="7" valign="top">
                                    <table border="0" cellspacing="0" cellpadding="0" align="left" width="606">
                                        <tbody>
                                        <tr border="0">
                                        <td><strong><font size="20">我是測試系統刑事警察大隊行政執行案件移送書</font></strong><br>
                                        </td>
                                        </tr>
                            <tr>
                                <td width="303" colspan="6" valign="top">
                                </td>
                                <td width="303">發文日期：中華民國'.(date('Y', strtotime($suspect->ft_send_date))-1911) .'年'.(date('m', strtotime($suspect->ft_send_date))) .'月'.(date('d', strtotime($suspect->ft_send_date))) .'日'.'<br>發文字號：北市警刑大毒緝字第'.$suspect->ft_send_no.'號</td>
                            </tr>
                                        <tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="358" colspan="3">義務人</td>
                                <td width="248" colspan="4">法定代理人或代表人</td>
                            </tr>
                            <tr>
                                <td width="107">姓名或名稱
                                </td>
                                <td width="251" colspan="2" valign="top">'.$suspect->s_ic.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">出生年月日
                                </td>
                                <td width="251" colspan="2" valign="top">'.(date('Y', strtotime($suspect->s_birth))-1911) .'年'.(date('m', strtotime($suspect->s_birth))) .'月'.(date('d', strtotime($suspect->s_birth))) .'日'.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">性別
                                </td>
                                <td width="251" colspan="2" valign="top">'.$suspect->s_gender.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">職 業
                                </td>
                                <td width="251" colspan="2" valign="top">
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">身分證統一號碼或營利事業統一編號
                                </td>
                                <td width="251" colspan="2">'.$suspect->s_ic.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">住居所或事務所、營業所地址及郵遞區號
                                </td>
                                <td width="251" colspan="2" valign="top">
                                        戶籍地：'.$suspect->fd_zipcode.'<br>'
                                                .$suspect->fd_address.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">執行標的物所在地
                                </td>
                                <td width="251" colspan="2">
                                        如附件所得及財產資料清單所載
                                </td>
                                <td width="68" align="center" colspan="2">
                                    分署<br>收案日期</td>
                                <td width="180" colspan="2" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">義務發生之原因與日期</td>
                                <td width="251" colspan="2">義務人違反毒品危害防制條例經裁處講習6小時而不履行該義務、本局109年07月09日北市警刑毒緝字第'.$suspect->fd_num.'號怠金行政處分書。
                                </td>
                                <td width="68" align="center" colspan="2">行政處分或裁定確定日</td>
                                <td width="180" colspan="2" valign="top">▓'.(date('Y', strtotime($suspect->ft_send_date))-1911) .'年'.(date('m', strtotime($suspect->ft_send_date))) .'月'.(date('d', strtotime($suspect->ft_send_date))) .'日'.'<br>□尚未確定</td>
                            </tr>
                            <tr>
                                <td width="107">移送法條
                                </td>
                                <td width="251" colspan="2" valign="top">▓依據行政執行法第11條<br>▓依據毒品危害防制條例第11條之1
                                </td>
                                <td width="68" align="center" colspan="2">繳納期間<br>屆滿日</td>
                                <td width="180" colspan="2">'.(date('Y', strtotime($suspect->call_grace))-1911) .'年'.(date('m', strtotime($suspect->call_grace))) .'月'.(date('d', strtotime($suspect->call_grace))) .'日'.'
                                </td>
                            </tr>
                            <tr>
                                <td width="107" rowspan="2">執行必要費用核銷機關（單位）名稱及統一編號
                                </td>
                                <td width="56">核銷機關(單位)名稱
                                </td>
                                <td align="center"  width="195"><strong>我是測試系統刑事警察大隊</strong></td>
                                <td width="68" align="center" colspan="2">徵收期間<br>屆滿日
                                </td>
                                <td width="180" colspan="2">
                                    ___年___月___日
                                </td>
                            </tr>
                            <tr>
                                <td width="56">統一編號
                                </td>
                                <td width="195"><strong>04135000</strong>
                                </td>
                                <td width="68" align="center" colspan="2">應納金額</td>
                                <td width="180" colspan="2" valign="top">新臺幣5000元(詳如附件)</td>
                            </tr>
                            <tr>
                                <td width="107" rowspan="4">承辦移送業務機關（單位）名稱與受款金融機構帳戶及帳號
                                </td>
                                <td width="56">承辦機關(單位)名稱
                                </td>
                                <td width="195"><strong>我是測試系統刑事警察大隊</strong>
                                </td>
                                <td width="248" colspan="4" rowspan="2">□執行(債權)憑證再移送<br>□執行憑證編號：
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="56">立帳金融機構名稱
                                </td>
                                <td width="195"><strong>臺北富邦銀行公庫處</strong>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="56">受款帳戶
                                </td>
                                <td width="195"><strong>我是測試系統刑事警察大隊</strong>
                                        <strong>(</strong>
                                        <strong>支</strong>
                                        <strong>(</strong>
                                        <strong>匯</strong>
                                        <strong>)</strong>
                                        <strong>票受款人請開具此抬頭</strong>
                                        <strong>)</strong>
                                        <strong></strong>
                                </td>
                                <td width="68" align="center" colspan="2">催繳情形</td>
                                <td width="180" colspan="2" valign="top">▓業經催繳<br>□未經催繳
                                </td>
                            </tr>
                            <tr>
                                <td align="center"  width="56">帳號</td>
                                <td width="195"><strong>16112470361021</strong>
                                </td>
                                <td width="68" align="center" colspan="2">催繳方式</td>
                                <td width="180" colspan="2" valign="top">□電話催繳<br>▓明信片或信函方式催繳<br>□其他方式(方式為 )
                                </td>
                            </tr>
                            <tr>
                                <td width="107">附件
                                </td>
                                <td width="251" colspan="2" valign="top">□附表<br>▓處分文書、裁定書或義務人依法令負有義務之證明文件及送達證明文件<br>▓義務人經限期履行而逾期仍不履行之證明文件及送達證明文件
                                </td>
                                <td width="248" colspan="4" valign="top">▓義務人之財產目錄<br>□土地登記簿謄本<br>▓義務人之戶籍資料<br>□保全措施之資料<br>□執行（債權）憑證<br>□
                                </td>
                            </tr>
                            <tr>
                                <td width="107" valign="top">保全措施
                                </td>
                                <td width="499" colspan="6" valign="top">□已限制出境（日期：　年　月　日）□已禁止處分□已提供擔保□已假扣押□已勒令停業
                                </td>
                            </tr>
                            <tr>
                                <td width="606" colspan="7" valign="bottom">此 致<br><strong><font size="20">法務部行政執行署臺北分署</font></strong><br><br><br>副本：我是測試系統刑事警察大隊
                                </td>
                            </tr>
                        </tbody>
                    </table>
                        <table ><tbody><tr><td height="255"></td></tr></tbody></table>';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        //$pdf->Output($project.'_delivery.pdf', 'D');    //download
        $pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function surc_Traf_surc()//產生移送執行函（怠金）
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $uname = $this->session-> userdata('uname');
        $query = $this->getsqlmod->getFTListfromProjectSurc($project); 
        $count = $query->num_rows();
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
	        //$this->SetFont('msungstdlight', '', 16);
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(20.5, 13.5, 20.5,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                ////$phone=$phone[0];
                //if(!isset($phone))$phone1=null;
                //if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                    <div><table cellpadding="0" cellspacing="0" >
                            <tbody>
                                <tr>
                                    <td width="100">以稿代簽</td><td width="100"> 一層決行</td><td width="100">限制開放 </td><td width="170"></td>
                                    <td rowspan=3 width="120">檔 號:109/07270399/1<br>保存年限：3年
                                    </td>
                                </tr>
                            </tbody>
                        </table><h2 align="center">我是測試系統刑事警察大隊   函(稿)</h2>
                        <table cellpadding="0" cellspacing="0" >
                            <tbody>
                                <tr>
                                    <td width="300">校對<br><br>監印<br><br>發文</td><td width="70"></td>
                                    <td rowspan=3 width="220"><br><br><br>地址：10042臺北市中正區武昌街1段69號<br>承辦人：'.$uname.'<br>電話：02-23932397<br>傳真：02-23932311
                                    </td>
                                </tr>
                                <tr>
                                    <td width="300">'.$suspect->surc_zipcode.'<br>'.$suspect->surc_address.'</td><td width="70"></td><td>電子信箱：andy33@ police.taipei</td>
                                </tr>
                                <tr>
                                    <td width="300"><h2>受文者：'.$suspect->s_name.' 君</h2></td><td width="70"></td><td></td>
                                </tr>
                            </tbody>
                        </table>
                        
                            發文日期：<strong>中華民國</strong><strong>'.(date('Y', strtotime($suspect->ft_send_date))-1911) .'年'.(date('m', strtotime($suspect->ft_send_date))) .'月'.(date('d', strtotime($suspect->ft_send_date))) .'日'.'</strong><strong></strong>
                            <br>發文字號：北市警刑大毒緝字第<strong>10930546347</strong>號<br>速別：普通件<br>密等及解密條件或保密期限：<br>附件：移送卷資及清冊(附件隨文發出，移送書電子檔另以電子郵件傳送)<br>
                        <br ><table cellpadding="0" cellspacing="0" >
                            <tbody>
                                <tr>
                                    <td width="60"><h2>主旨：</h2></td>
                                    <td width="510"><h2>檢送本大隊辦理違反毒品危害防治條例怠金義務人'.$suspect->s_name.'等'.$count.'案移送書正本暨相關卷資各1份移請執行，請依權責捉出，請查照。</h2></td>
                                </tr>
                                <tr>
                                    <td width="60"><h2>說明：</h2></td>
                                    <td width="510"><h2><br>一、依據行政執行第11條辦理。<br>二、旨揭卷資內含義務人'.$suspect->s_name.'等'.$count.'案（詳如清冊）移送書正本及違反毒品危害防制條列處分書(怠金)等卷資影本各1份，為行政作業需要，請以匯款方式至本大隊保管進專戶（行庫：臺北富邦銀行公庫出、戶名：我是測試系統刑事警察大隊。、帳號：16112470361019）。<br>三、移送案件所需郵資，請貴分署一本大隊申辦之中華郵政（股）公司'.$suspect->ft_target_post.'郵局郵件特約用戶（特約編號：241057000064）核實使用。
                                    </h2></td>
                                </tr>
                            </tbody>
                        </table>
                        <br><br><br>正本：法務部行政執行署'.$suspect->ft_target.'分署（'.$suspect->ft_target_address.'）
                        <br>副本：
                        <br>抄本：
                        <br>
                        <br><h2>大隊長陳OO</h2>
                        <table >
                        <tbody>
                        <tr>
                        <td height="217"></td>   
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    <br clear="all"/>
                    ';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        //$pdf->Output($project.'_delivery.pdf', 'D');    //download
        $pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function surc_TrafSurc()//產生怠金移送書
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $uname = $this->session-> userdata('uname');
        $query = $this->getsqlmod->getFTListfromProjectSurc($project); 
        $count = $query->num_rows();
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
	        //$this->SetFont('msungstdlight', '', 16);
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(20.5, 13.5, 20.5,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                ////$phone=$phone[0];
                //if(!isset($phone))$phone1=null;
                ///if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                    <table cellpadding="0" cellspacing="0" align="left">
                        <tbody>
                            <tr>
                                <td width="309">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td width="200" rowspan="2"><strong>原案件編號：</strong><u><font size="18" >'.$suspect->surc_no.'</font></u></td>
                                                <td colspan="3" width="400">承辦人：陳昌澂 聯絡電話：02-2393-2397傳真電話:2393-2311</td>
                                                
                                            </tr>
                                            <tr>
                                                <td width="250"></td>
                                                <td Border="1" Cellspacing="0" align="right" width="50">移送案號</td>
                                                <td Border="1" Cellspacing="0" align="center" width="90">'.$suspect->ft_no.'</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="1" cellspacing="0" cellpadding="0" align="left" width="606">
                        <tbody>
                            <tr border="0">
                                <td  width="606" colspan="7" valign="top">
                                    <table border="0" cellspacing="0" cellpadding="0" align="left" width="606">
                                        <tbody>
                                        <tr border="0">
                                        <td><strong><font size="20">我是測試系統刑事警察大隊行政執行案件移送書</font></strong><br>
                                        </td>
                                        </tr>
                            <tr>
                                <td width="303" colspan="6" valign="top">
                                </td>
                                <td width="303">發文日期：中華民國'.(date('Y', strtotime($suspect->ft_send_date))-1911) .'年'.(date('m', strtotime($suspect->ft_send_date))) .'月'.(date('d', strtotime($suspect->ft_send_date))) .'日'.'<br>發文字號：北市警刑大毒緝字第'.$suspect->ft_send_no.'號</td>
                            </tr>
                                        <tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="358" colspan="3">義務人</td>
                                <td width="248" colspan="4">法定代理人或代表人</td>
                            </tr>
                            <tr>
                                <td width="107">姓名或名稱
                                </td>
                                <td width="251" colspan="2" valign="top">'.$suspect->s_name.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">出生年月日
                                </td>
                                <td width="251" colspan="2" valign="top">'.(date('Y', strtotime($suspect->s_birth))-1911) .'年'.(date('m', strtotime($suspect->s_birth))) .'月'.(date('d', strtotime($suspect->s_birth))) .'日'.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">性別
                                </td>
                                <td width="251" colspan="2" valign="top">'.$suspect->s_gender.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">職 業
                                </td>
                                <td width="251" colspan="2" valign="top">
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">身分證統一號碼或營利事業統一編號
                                </td>
                                <td width="251" colspan="2">'.$suspect->s_ic.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">住居所或事務所、營業所地址及郵遞區號
                                </td>
                                <td width="251" colspan="2" valign="top">
                                        戶籍地：'.$suspect->surc_zipcode.'<br>'
                                                .$suspect->surc_address.'
                                </td>
                                <td width="248" colspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">執行標的物所在地
                                </td>
                                <td width="251" colspan="2">
                                        如附件所得及財產資料清單所載
                                </td>
                                <td width="48" colspan="2">
                                        分署收案日期
                                </td>
                                <td width="200" colspan="2" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td width="107">義務發生之原因與日期
                                </td>
                                <td width="251" colspan="2">義務人違反毒品危害防制條例經裁處講習6小時而不履行該義務、本局109年07月09日北市警刑毒緝字第'.$suspect->fd_num.'號怠金行政處分書。
                                </td>
                                <td width="48" colspan="2">行政處分或裁定確定日
                                </td>
                                <td width="200" colspan="2" valign="top">▓'.(date('Y', strtotime($suspect->ft_send_date))-1911) .'年'.(date('m', strtotime($suspect->ft_send_date))) .'月'.(date('d', strtotime($suspect->ft_send_date))) .'日'.'<br>□尚未確定</td>
                            </tr>
                            <tr>
                                <td width="107">移送法條
                                </td>
                                <td width="251" colspan="2" valign="top">▓依據行政執行法第11條<br>▓依據毒品危害防制條例第11條之1
                                </td>
                                <td width="48" colspan="2">繳納期間屆滿日
                                </td>
                                <td width="200" colspan="2">'.(date('Y', strtotime($suspect->surc_findate))-1911) .'年'.(date('m', strtotime($suspect->surc_findate))) .'月'.(date('d', strtotime($suspect->surc_findate))) .'日'.'
                                </td>
                            </tr>
                            <tr>
                                <td width="107" rowspan="2">執行必要費用核銷機關（單位）名稱及統一編號
                                </td>
                                <td width="51">核銷機關(單位)名稱
                                </td>
                                <td width="200"><strong>我是測試系統刑事警察大隊</strong>
                                </td>
                                <td width="48" colspan="2">徵收期間屆滿日
                                </td>
                                <td width="200" colspan="2">年 月 日
                                </td>
                            </tr>
                            <tr>
                                <td width="51">統一編號
                                </td>
                                <td width="200"><strong>04135000</strong>
                                </td>
                                <td width="48" colspan="2">應納金額
                                </td>
                                <td width="200" colspan="2" valign="top">新臺幣5000元(詳如附件)
                                </td>
                            </tr>
                            <tr>
                                <td width="107" rowspan="4">承辦移送業務機關（單位）名稱與受款金融機構帳戶及帳號
                                </td>
                                <td width="51">承辦機關(單位)名稱
                                </td>
                                <td width="200"><strong>我是測試系統刑事警察大隊</strong>
                                </td>
                                <td width="248" colspan="4" rowspan="2">□執行(債權)憑證再移送<br>□執行憑證編號：
                                </td>
                            </tr>
                            <tr>
                                <td width="51">立帳金融機構名稱
                                </td>
                                <td width="200"><strong>臺北富邦銀行公庫處</strong>
                                </td>
                            </tr>
                            <tr>
                                <td width="51">受款帳戶
                                </td>
                                <td width="200"><strong>我是測試系統刑事警察大隊</strong>
                                        <strong>(</strong>
                                        <strong>支</strong>
                                        <strong>(</strong>
                                        <strong>匯</strong>
                                        <strong>)</strong>
                                        <strong>票受款人請開具此抬頭</strong>
                                        <strong>)</strong>
                                        <strong></strong>
                                </td>
                                <td width="48" colspan="2">催繳情形
                                </td>
                                <td width="200" colspan="2" valign="top">▓業經催繳<br>□未經催繳
                                </td>
                            </tr>
                            <tr>
                                <td width="51">帳號
                                </td>
                                <td width="200"><strong>16112470361021</strong>
                                </td>
                                <td width="48" colspan="2">催繳方式
                                </td>
                                <td width="200" colspan="2" valign="top">□電話催繳<br>▓明信片或信函方式催繳<br>□其他方式(方式為 )
                                </td>
                            </tr>
                            <tr>
                                <td width="107">附件
                                </td>
                                <td width="251" colspan="2" valign="top">□附表<br>▓處分文書、裁定書或義務人依法令負有義務之證明文件及送達證明文件<br>▓義務人經限期履行而逾期仍不履行之證明文件及送達證明文件
                                </td>
                                <td width="248" colspan="4" valign="top">▓義務人之財產目錄<br>□土地登記簿謄本<br>▓義務人之戶籍資料<br>□保全措施之資料<br>□執行（債權）憑證<br>□
                                </td>
                            </tr>
                            <tr>
                                <td width="107" valign="top">保全措施
                                </td>
                                <td width="499" colspan="6" valign="top">□已限制出境（日期：　年　月　日）□已禁止處分□已提供擔保□已假扣押□已勒令停業
                                </td>
                            </tr>
                            <tr>
                                <td width="606" colspan="7" valign="bottom">此 致<br><strong><font size="20">法務部行政執行署臺北分署</font></strong><br><br><br>副本：我是測試系統刑事警察大隊
                                </td>
                            </tr>
                        </tbody>
                    </table>
                        <table >
                        <tbody>
                        <tr>
                        <td height="170"></td>   
                        </tr>
                        </tbody>
                        </table>
                    <br clear="all"/>
                    ';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        //$pdf->Output($project.'_delivery.pdf', 'D');    //download
        $pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function fine_doc_project_fin_no(){//產生處分書pdf 移送產生處分書
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getFTListfromProject($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            $pdf->SetMargins(5.5, 3.5, 10,true);
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp->s_dp_project)->result();
                $susp_dp=$susp_dp[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                //var_dump($phone[0]);    
                if(!isset($phone[0]))$phone1=null;
                if(isset($phone[0]))$phone1=$phone[0]->p_no;
                $susp1 = $this->getsqlmod->get2Susp($susp->s_num)->result(); // 抓snum
                //$susp1 = $susp1[0]; // 抓snum
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2msg = Null;
                foreach ($drug2 as $drug3){
                    if(!isset($drug3->ddc_level)){}
                    else{
                        $drug2msg = $drug2msg . '第'.$drug3->ddc_level .'級' . '『'.$drug3->ddc_ingredient.'』 ';
                    }
                }  
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                                <div style="width:680px" class="panel-body"><p>
                                    <table>
                                        <tr><td ><h2>列管編號：'.$sp->fd_num .'</h2> </td><td colspan="2"></td></tr>
                                        <tr><td colspan="3"> </td></tr>
                                        <tr><td colspan="3">正本：'.$sp->fd_target.'君'.$sp->fd_zipcode.' '.$sp->fd_address.'</td></tr>
                                        <tr><td colspan="3">副本：我是測試系統'.$susp->s_roffice.'、我是測試系統刑事警察大隊</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統 違反毒品危害防制條例案件處分書</h3></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">發文日期字號：'.(date('Y')-1911).'年'.date('m', strtotime($sp->fd_date)).'月'.date('d', strtotime($sp->fd_date)).'日北市警刑毒緝字第號'.$sp->fd_send_num.'</td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">依據：我是測試系統'.$susp->s_roffice.(date('Y', strtotime($sp->fd_date))-1911) .'年'.date('m', strtotime($sp->fd_date)).'月'.date('d', strtotime($sp->fd_date)).'日北市警分刑字第號'.$susp->s_fno.'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77">
                                                <br>
                                                <h3 align="center">受處分人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77"><h3 align="center">法　定<br>代理人</h3></td>
                                                <td colspan="2" width="70">姓名</td>
                                                <td width="88"></td>
                                                <td width="49">性別</td>
                                                <td width="50"></td>
                                                <td width="70" valign="top">出生年月日</td>
                                                <td width="276"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="70" valign="top">身分證統一編號</td>
                                                <td width="88"></td>
                                                <td colspan="3" width="169" valign="top">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"></td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>
                                                        一、新臺幣<strong>'.($suspect->f_damount/10000).'</strong>萬元整
                                                            二、毒品危害 <strong>6</strong>小時。(講習時間詳見下方「毒品講習」欄位)
                                                        三、<strong> '.$drug2msg.$drug->e_count.$drug->e_type.'</strong>沒入。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>事實</h3></td>
                                                <td colspan="7" width="603">受處分人'.$susp->s_name.$cases->s_date.'許，在本市'.$cases->r_county.$cases->r_district.$cases->r_address.'，為'.$cases->s_office.'員警查獲持有'.$sp->fd_drug_msg.'，經採集毒品及尿液檢體送專業單位鑑驗，均呈第'.$drug2->ddc_level.'級毒品「'.$drug2->ddc_ingredient.'」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg .'</td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">理由及<br>法令依據</h3></td>
                                                <td colspan="7" width="603">
                                                        ■
                                                        依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                        <br>■
                                                        依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">繳納期限<br>及方式</h3></td>
                                                <td colspan="7" width="603"> 一、罰鍰限於<strong>民國'.(date('Y', strtotime($suspect->f_date))-1911) .'年'.(date('m', strtotime($suspect->f_date))) .'月'.(date('d', strtotime($suspect->f_date))) .'日</strong>前選擇下列方式之一繳款：
                                                            (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                            (二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>
                                                                二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong>
                                                            (三)輸入本案虛擬帳號：<strong>'.$suspect->f_BVC.'</strong>
                                                                三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">
                                                        一、可選擇參與實體講習或線上課程(每年可參與三次，惟應以申請不同套裝課程)。<br>
                                                        二、講習時間： 民國'.(date('Y', strtotime($sp->fd_lec_date))-1911) .'年'.(date('m', strtotime($sp->fd_lec_date))) .'月'.(date('d', strtotime($sp->fd_lec_date))) .'日09:30分（請攜帶有相片之證件報到，逾時將無法入場），地點：臺北市非政府(NGO)會館1919樓 多功能資料室 。如要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。<br>
                                                        三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：'.$sp->fd_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                </div>';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function traf_delivery()//產生移送送達文件
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getFTListfromProject($project); 
        $uname = $this->session-> userdata('uname');
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(18.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                //if(!isset($phone[0]))$phone1=null;
                //if(isset($phone[0]))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                // $drug2=$drug2[0];
				$drug2=null;
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                    <p>我是測試系統 送達證書-<strong></strong><strong><font size="18">'.$suspect->fd_num.'</font></strong><strong></strong></p>
                    <table border="1" cellspacing="0" cellpadding="0" width="600">
                        <tbody>
                            <tr>
                                <td width="200" colspan="2" align="center">
                                        受送達人名稱姓名地址
                                </td>
                                <td width="400" colspan="4" valign="top"><h2>'.$susp->s_name.'君</h2>'.$susp->s_dpzipcode.' '.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress.'
                                </td>
                            </tr>
                            <tr> 
                                <td width="200" colspan="2" valign="top" align="center">文       號</td>
                                <td width="400" colspan="4" valign="top" align="center">北市警刑毒緝字第'.$suspect->ft_send_no.'號</td>
                            </tr>
                            <tr>
                                <td width="200" colspan="2" align="center">送 逹 文 書 （含 案 由）</td>
                                <td width="400" colspan="4" align="center"><strong>違反毒品危害防制條例案件處分書</strong>
                                </td>
                            </tr>
                            <tr>
                                <td width="100" rowspan="2" align="center">原寄郵局日戳</td>
                                <td width="100" rowspan="2" align="center">送達郵局日戳</td>
                                <td width="300" colspan="3" align="center">送逹處所（由送逹人填記）</td>
                                <td width="100" rowspan="2" align="center">送逹人簽章</td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="left">
                                        □同上記載地址<br>
                                        □改送：
                                </td>
                            </tr>
                            <tr>
                                <td width="100" rowspan="2" valign="top">
                                </td>
                                <td width="100" rowspan="2" valign="top">
                                </td>
                                <td width="300" colspan="3" align="center">送逹時間（由送逹人填記）</td>
                                <td width="100" rowspan="2" align="center">
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="center">
                                        中華民國   年____月____日<br>
                                        午____時___分
                                </td>
                            </tr>
                            <tr>
                                <td width="600" colspan="6" valign="top">
                                        送達方式
                                </td>
                            </tr>
                            <tr>
                                <td width="600" colspan="6" valign="top">
                                        由 送 逹 人 在 □ 上 劃 ˇ 選 記
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top">
                                        □已將文書交與應受送逹人
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        □本人 （簽名或蓋章）
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top">
                                        □未獲會晤本人，已將文書交與有辨別事理能力之同居人、受雇人或應送逹處所之接收郵件人員
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        □同居人<br>
                                        □受雇人 （簽名或蓋章）<br>
                                        □應送逹處所接收郵件人員<br>
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top"><p>
                                        □應受送逹之本人、同居人或受雇人收領後，拒絕或不能簽名或蓋章者，由送逹人記明其事由<br>
                                    </p>
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        送逹人填記：
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top">
                                        □應受送逹之本人、同居人、受雇人或應受送逹處所接收郵件人員無正當理由拒絕領經送逹人將文書留置於送逹處所，以為送逹
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        □本人<br>
                                        □同居人 拒絕收領<br>
                                        □受雇人<br>
                                        □應受送逹處所接收郵件人員<br>
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="center">
                                        □未獲會晤本人亦無受領文書之同居人、受雇人或應受送逹處所接收郵件人員，已將該送逹文書：<br>
                                        □應受送逹之本人、同居人、受雇人或應受送逹處所接收郵件人員無正當理由拒絕收領，並有難逹留置情事，已將該送逹文書：
                                </td>
                                <td width="180" colspan="3" align="left">
                                        □寄存於________派出所<br>
                                        □寄存於_鄉（鎮、市、區）<br>
                                        公所<br>
                                        □寄存於 鄉（鎮、市、區）<br>
                                        公所<br>
                                        村（里）辦公處<br>
                                        □寄存於 郵局<br>
                                </td>
                                <td width="120" valign="top">
                                        並作送達通知書二份，一份黏貼於應受送逹人住居所、事務所、營業所或其就業處所門首，一份□交由鄰居轉交或□置於該受送逹處所信箱或其他適當位置，以為送逹。
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="center">
                                        送 達 人 注 意 事 項
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        一、依上述送達方法送達者，送達人應即將本送達證書，提出於交送達之行政機關附卷。<br>
                                        二、不能依上述送達方法送達者，送達人應製作記載該事由之報告書，提出於交送達之行政機關附卷，並繳回應送達之文書。
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p>
                        ※ <strong>請繳回 我是測試系統刑事警察大隊 地址：10042臺北市中正區武昌街一段69號</strong>
                    </p>
                    <p>
                        <strong>單位：毒品查緝中心(</strong>
                        <strong>'.$uname.'</strong>
                        <strong>)</strong>
                    </p>
                    <p>
                        我是測試系統交郵送達格式
                    </p>
                    <table>
                    <tr>
                    <td height="258"></td>   
                    </tr>            
                    </table> '
                    ;
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        //$pdf->Output($project.'.pdf', 'D');    //download
        $pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function traf_delivery_surc()//產生移送送達文件（怠金）
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getFTListfromProjectSurc($project); 
        $uname = $this->session-> userdata('uname');
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(18.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                ////$phone=$phone[0];
                //if(!isset($phone))$phone1=null;
                //if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                    <p>我是測試系統 送達證書-<strong></strong><strong><font size="18">'.$suspect->surc_no.'</font></strong><strong></strong></p>
                    <table border="1" cellspacing="0" cellpadding="0" width="600">
                        <tbody>
                            <tr>
                                <td width="200" colspan="2" align="center">
                                        受送達人名稱姓名地址
                                </td>
                                <td width="400" colspan="4" valign="top"><h2>'.$susp->s_name.'君</h2>'.$susp->s_dpzipcode.' '.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress.'
                                </td>
                            </tr>
                            <tr> 
                                <td width="200" colspan="2" valign="top" align="center">文       號</td>
                                <td width="400" colspan="4" valign="top" align="center">北市警刑毒緝字第'.$suspect->ft_send_no.'號</td>
                            </tr>
                            <tr>
                                <td width="200" colspan="2" align="center">送 逹 文 書 （含 案 由）</td>
                                <td width="400" colspan="4" align="center"><strong>怠金處分書</strong>
                                </td>
                            </tr>
                            <tr>
                                <td width="100" rowspan="2" align="center">原寄郵局日戳</td>
                                <td width="100" rowspan="2" align="center">送達郵局日戳</td>
                                <td width="300" colspan="3" align="center">送逹處所（由送逹人填記）</td>
                                <td width="100" rowspan="2" align="center">送逹人簽章</td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="left">
                                        □同上記載地址<br>
                                        □改送：
                                </td>
                            </tr>
                            <tr>
                                <td width="100" rowspan="2" valign="top">
                                </td>
                                <td width="100" rowspan="2" valign="top">
                                </td>
                                <td width="300" colspan="3" align="center">送逹時間（由送逹人填記）</td>
                                <td width="100" rowspan="2" align="center">
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="center">
                                        中華民國   年____月____日<br>
                                        午____時___分
                                </td>
                            </tr>
                            <tr>
                                <td width="600" colspan="6" valign="top">
                                        送達方式
                                </td>
                            </tr>
                            <tr>
                                <td width="600" colspan="6" valign="top">
                                        由 送 逹 人 在 □ 上 劃 ˇ 選 記
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top">
                                        □已將文書交與應受送逹人
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        □本人 （簽名或蓋章）
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top">
                                        □未獲會晤本人，已將文書交與有辨別事理能力之同居人、受雇人或應送逹處所之接收郵件人員
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        □同居人<br>
                                        □受雇人 （簽名或蓋章）<br>
                                        □應送逹處所接收郵件人員<br>
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top"><p>
                                        □應受送逹之本人、同居人或受雇人收領後，拒絕或不能簽名或蓋章者，由送逹人記明其事由<br>
                                    </p>
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        送逹人填記：
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" valign="top">
                                        □應受送逹之本人、同居人、受雇人或應受送逹處所接收郵件人員無正當理由拒絕領經送逹人將文書留置於送逹處所，以為送逹
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        □本人<br>
                                        □同居人 拒絕收領<br>
                                        □受雇人<br>
                                        □應受送逹處所接收郵件人員<br>
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="center">
                                        □未獲會晤本人亦無受領文書之同居人、受雇人或應受送逹處所接收郵件人員，已將該送逹文書：<br>
                                        □應受送逹之本人、同居人、受雇人或應受送逹處所接收郵件人員無正當理由拒絕收領，並有難逹留置情事，已將該送逹文書：
                                </td>
                                <td width="180" colspan="3" align="left">
                                        □寄存於________派出所<br>
                                        □寄存於_鄉（鎮、市、區）<br>
                                        公所<br>
                                        □寄存於 鄉（鎮、市、區）<br>
                                        公所<br>
                                        村（里）辦公處<br>
                                        □寄存於 郵局<br>
                                </td>
                                <td width="120" valign="top">
                                        並作送達通知書二份，一份黏貼於應受送逹人住居所、事務所、營業所或其就業處所門首，一份□交由鄰居轉交或□置於該受送逹處所信箱或其他適當位置，以為送逹。
                                </td>
                            </tr>
                            <tr>
                                <td width="300" colspan="3" align="center">
                                        送 達 人 注 意 事 項
                                </td>
                                <td width="300" colspan="3" valign="top">
                                        一、依上述送達方法送達者，送達人應即將本送達證書，提出於交送達之行政機關附卷。<br>
                                        二、不能依上述送達方法送達者，送達人應製作記載該事由之報告書，提出於交送達之行政機關附卷，並繳回應送達之文書。
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p>
                        ※ <strong>請繳回 我是測試系統刑事警察大隊 地址：10042臺北市中正區武昌街一段69號</strong>
                    </p>
                    <p>
                        <strong>單位：毒品查緝中心(</strong>
                        <strong>'.$uname.'</strong>
                        <strong>)</strong>
                    </p>
                    <p>
                        我是測試系統交郵送達格式
                    </p>
                    <table>
                    <tr>
                    <td height="258"></td>   
                    </tr>            
                    </table> '
                    ;
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        //$pdf->Output($project.'.pdf', 'D');    //download
        $pdf->Output($project.'.pdf', 'D');    //read
    }
    
    public function surc_doc_fin_traf()//產生怠金處分書監所pdf(正本)
    {
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getFTListfromProjectSurc($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                $suspfadai = $this->getsqlmod->get1SuspFadai($susp->s_ic)->result();
                $today['day']   = date('d');
                $today['month'] = date('m');
                $today['year']  = date('Y') - 20;
                $stampToday = mktime(0, 0, 0, $today['month'], $today['day'], $today['year']);
                    $fname = NULL;
                    $fgender = NULL;
                    $fic = NULL;
                    $fbirth = NULL;
                    $fphone = NULL;
                    $fzipcode = NULL;
                    $faddress = NULL;
                if(isset($suspfadai[0])){
                    $suspfadai = $suspfadai[0];
                    $stampBirth = strtotime($susp->s_birth);
                    if ($stampBirth > $stampToday) {
                        $fname =  $suspfadai->s_fadai_name;
                        $fgender = $suspfadai->s_fadai_gender;
                        $fic = $suspfadai->s_fadai_ic;
                        $fbirth = '民國'.(date('Y', strtotime($fbirth))-1911) .'年'.(date('m', strtotime($fbirth))) .'月'.(date('d', strtotime($fbirth))).'日' ;
                        $fphone = $suspfadai->s_fadai_phone;
                        $fzipcode = $suspfadai->s_fadai_zipcode;
                        $faddress = $suspfadai->s_fadai_county.$suspfadai->s_fadai_district.$suspfadai->s_fadai_address;
                    }else{
                        $fname = NULL;
                        $fgender = NULL;
                        $fic = NULL;
                        $fbirth = NULL;
                        $fphone = NULL;
                        $fzipcode = NULL;
                        $faddress = NULL;
                    }
                }
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                                <div style="width:680px" class="panel-body"><p>
                                    <table>
                                        <tr><td ><h2>列管編號：'.$suspect->surc_no .'</h2> </td><td colspan="2"><h2>原處分管制編號：'.$sp->fd_num .'</h2></td></tr>
                                        <tr><td colspan="3">正本：'.$sp->fd_target.'君'.$sp->fd_zipcode.' '.$sp->fd_address.'</td></tr>
                                        <tr><td colspan="3">副本：我是測試系統刑事警察大隊毒品查緝中心<br>臺北市政府毒品危害防制中心</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統違反毒品危害防制條例案件（怠金）處分書</h3></td>
                                            </tr>
                                            <tr>
                                                <td width="77">依據</td><td colspan="7" width="603"><strong>臺北市政府衛生局'.(date('Y')-1911).'年'.date('m', strtotime($suspect->surc_basenumdate)).'月'.date('d', strtotime($suspect->surc_basenumdate)).'日北市警刑毒緝字第號'.$sp->fd_send_num.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77">
                                                <br>
                                                <h3 align="center">受處分人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77"><h3 align="center">法　定<br>代理人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$fname.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$fgender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>'.$fbirth.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$fic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$fphone.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$faddress.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>
                                                        一、新臺幣<strong>'.($fine->f_samount).'</strong>元整<br>
                                                            二、仍須參加講習，講習時地詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續<br>
                                                                      處以怠金。。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br>事實</h3></td>
                                                <td colspan="7" width="603">緣受處分人'.$susp->s_name.'涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於民國'.(date('Y', strtotime($sp->fd_lec_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日至'.$sp->fd_lec_place.'參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。</td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">理由及<br>法令依據</h3></td>
                                                <td colspan="7" width="603">
                                                        ■一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習者，依行政執行法規定處以怠金。<br>
                                                        ■二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不能由他人代為履行者，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。<br>
                                                        ■三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續處以怠金。<br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">繳納期限<br>及方式</h3></td>
                                                <td colspan="7" width="603"> 一、怠金限於<strong>民國'.(date('Y', strtotime($fine->f_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日'.$sp->fd_lec_date.'</strong>前選擇下列方式之一繳款：
                                                            (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                            (二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>
                                                        二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong><br>
                                                            (三)輸入本案虛擬帳號：<strong>'.$fine->f_BVC.'</strong>
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。<br>
                                                        四、	匯款繳納時，請於匯款單上之匯款人欄填寫受處分人之姓名「«姓名»」，並應要求於備註(附言)欄填入案件列管編號「«怠金編號»」及身分證統一編號「«身分證號»」，俾利辦理銷案。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">
                                                    一、可選擇參與實體講習或線上課程（線上課程每年可參與三次，惟應以不同套裝課程申請認定）。
                                                    二、講習時間：民國'.(date('Y', strtotime($suspect->surc_lec_date))-1911) .'年'.(date('m', strtotime($suspect->surc_lec_date))) .'月'.(date('d', strtotime($suspect->surc_lec_date))) .'日'.$sp->fd_lec_date.'（請攜帶有相片之證件報到，逾時將無法入場），地點：'.$suspect->surc_lec_place.'。有正當理由無法參加或要選擇線上課程者，請至下列網址：https://nodrug.gov.taipei下載異動申請表或依指示完成線上課程。講習相關問題，請逕向臺北市政府毒品危害防制中心洽詢，電話(02)2375-4068。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：'.$sp->fd_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table> 
                                    <tr>
                                    <td height="180"></td>   
                                    </tr>            
                                    </table> 
                                </div>';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }

    // 金融專案憑證用紙
    public function fineproject_card_print()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        
        $this->load->library('PHP_TCPDF');
        $id = $this->uri->segment(3);
        $data = $this->getsqlmod->export_finePJ_lists($id)->result();
        $pay_tamount_sum = 0;
        $first_username = '';
        $name = array();
        for ($i=0; $i < count($data); $i++) { 
            $pay_tamount_sum = ($pay_tamount_sum + $data[$i]->pay_tamount);
            $first_username = $data[0]->f_username;
            array_push($name, $data[$i]->f_username);
        }
        $monet_str = str_split($pay_tamount_sum,1);
        $uname = $this->session-> userdata('uname');
        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(15.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='
            <h1 style="text-align:center;letter-spacing: 5px; ">我是測試系統刑事警察大隊</h1>
            <h1 style="text-align:center;letter-spacing: 20px;text-decoration: underline;">黏貼憑證用紙</h1>';
            $pdf->SetFont('twkai98_1','',10);
            $html .='
            <table style="width:120px;" border="1">
                <tr>
                    <td width="100px" height="50px" style="line-height:50px;">傳票編號</td>
                    <td style="line-height:50px;">H/I</td>
                </tr>
                <tr>
                    <td>付款憑單編號</td>
                    <td></td>
                </tr>
            </table>
            <table style="width:940px;text-align:center;line-height:30px;" border="1" valign="center">
                    <tr>
                        <td width="35px"  rowspan="3"><span>憑證編號</span></td>
                        <td width="65px" height="30px" align="center">預算年度</td>
                        <td width="60px" align="center">'. (date('Y')-1911) .'</td>
                        <td width="180px" colspan="9" align="center"  style="letter-spacing: 20px;line-height:30px;">金額</td>
                        <td width="150px"  rowspan="3" style="letter-spacing: 20px;line-height:90px;">用途說明</td>
                        <td width="150px" align="left">附件</td>
                    </tr>
                    <tr>
                        <td colspan="2" height="30px" align="center"  style="letter-spacing: 10px;line-height:30px;">預算科目</td>
                        <td width="20px" rowspan="2"><font size="8">億</font></td>
                        <td width="20px" rowspan="2"><font size="8">千萬</font></td>
                        <td width="20px" rowspan="2"><font size="8">百萬</font></td>
                        <td width="20px" rowspan="2"><font size="8">十萬</font></td>
                        <td width="20px" rowspan="2"><font size="8">萬</font></td>
                        <td width="20px" rowspan="2"><font size="8">千</font></td>
                        <td width="20px" rowspan="2"><font size="8">百</font></td>
                        <td width="20px" rowspan="2"><font size="8">十</font></td>
                        <td width="20px" rowspan="2"><font size="8">元</font></td>
                        <td  rowspan="3">
                            <table width="120px" cellpadding="0" style="line-height:20px;">
                                <tr>
                                    <td width="60px" height="10px" align="left">發票</td>
                                    <td width="60px" align="right">張</td>
                                </tr>
                                <tr>
                                    <td align="left" height="10px">收據</td>
                                    <td align="right">張</td>
                                </tr>
                                <tr>
                                    <td align="left" height="10px">請修單</td>
                                    <td align="right">張</td>
                                </tr>
                                <tr>
                                    <td align="left" height="10px">驗收報告</td>
                                    <td align="right">張</td>
                                </tr>
                                <tr>
                                    <td align="left" height="10px">合約書</td>
                                    <td align="right">份</td>
                                </tr>
                                <tr>
                                    <td align="left" height="10px">其他文件</td>
                                    <td align="right">張</td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2"><font size="8">(需註明文件名稱)</font></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="30px" align="center">工作計畫</td>
                        <td align="center">用途別</td>
                    </tr>
                    <tr>
                        <td width="35px" height="60px"></td>
                        <td colspan="2">繳付公庫數 <br/> 罰金罰鍰</td>
                        <td width="20px" style="line-height:60px;"><font size="10">'.(((count($monet_str) - 9) == -1)?"$":"").(((count($monet_str) - 9) >= 0)?$monet_str[count($monet_str) - 9]:"").'</font></td>
                        <td width="20px" style="line-height:60px;"><font size="10">'.(((count($monet_str) - 8) == -1)?"$":"").(((count($monet_str) - 8) >= 0)?$monet_str[count($monet_str) - 8]:"").'</font></td>
                        <td width="20px" style="line-height:60px;"><font size="10">'.(((count($monet_str) - 7) == -1)?"$":"").(((count($monet_str) - 7) >= 0)?$monet_str[count($monet_str) - 7]:"").'</font></td>
                        <td width="20px" style="line-height:60px;"><font size="10">'.(((count($monet_str) - 6) == -1)?"$":"").(((count($monet_str) - 6) >= 0)?$monet_str[count($monet_str) - 6]:"").'</font></td>
                        <td width="20px" style="line-height:60px;"><font size="10">'.(((count($monet_str) - 5) == -1)?"$":"").(((count($monet_str) - 5) >= 0)?$monet_str[count($monet_str) - 5]:"").'</font></td>
                        <td width="20px" style="line-height:60px;"><font size="10">'.(((count($monet_str) - 4) == -1)?"$":"").(((count($monet_str) - 4) >= 0)?$monet_str[count($monet_str) - 4]:"").'</font></td>
                        <td width="20px" style="line-height:60px;"><font size="10">'.(((count($monet_str) - 3) == -1)?"$":"").(((count($monet_str) - 3) >= 0)?$monet_str[count($monet_str) - 3]:"").'</font></td>
                        <td width="20px" style="line-height:60px;"><font size="10">'.(((count($monet_str) - 2) == -1)?"$":"").(((count($monet_str) - 2) >= 0)?$monet_str[count($monet_str) - 2]:"").'</font></td>
                        <td width="20px" style="line-height:60px;"><font size="10">'.(((count($monet_str) - 1) == -1)?"$":"").(((count($monet_str) - 1) >= 0)?$monet_str[count($monet_str) - 1]:"").'</font></td>
                        <td width="150px" >'.$first_username.'等'. count(array_unique($name)).'人違反「毒品危害防制條例」罰鍰</td>
                    </tr>
                    <tr>
                        <td colspan="3" height="60px" style="letter-spacing: 20px;line-height:60px;">經辦單位</td>
                        <td width="180px">申請、使用單位 <br/> (驗收或證明、保管)</td>
                        <td width="150px" style="letter-spacing: 20px;line-height:60px;">會計單位</td>
                        <td width="150px" style="letter-spacing: 8px;">機關長官 <br/> 或授權代簽人</td>
                    </tr>
                    <tr >
                        <td colspan="3" height="120px" ></td>
                        <td width="180px"></td>
                        <td width="150px"></td>
                        <td width="150px"></td>
                    </tr>
                </table>
                <br/>
                <div><table style="width:850px;border: 1px dashed black;"></table>
                <p align="center" style="letter-spacing: 10px;">(黏貼憑證線)</p>
                <p style="text-indent: 4em;"><b>說明:</b></p>
                <p>
                    <ol style="width:850px;">
                        <li>不同工作計畫或用途別之原始憑證及發票請勿混合黏貼。</li>
                        <li>本用紙除「憑證編號」及「預算科目」兩欄由會計部門填列外，其餘各欄由經辦核銷工作之事務人員填列。</li>
                        <li>本用紙憑證黏貼線上端有關人員核章欄，得視各機關經理財務工作之實際分工程序自行增列。</li>
                        <li>凡提供參考之附件，如不能同時黏貼,則記明某號憑證之附件，按號另裝成冊一併附送，並於憑證簿封面註明上開另裝附件若干件。</li>
                        <li>本用紙由有關人員順序核章後，送會計部門辦理經費核銷手續，月終由會計部門彙總裝訂成冊，依規定程序辦理。</li>
                        <li>各單位主管請於騎縫處核章。</li>
                    </ol>
                </p>
                </div>';
            $pdf->writeHTML($html, true, false, true, false, '');                        
        //$pdf->Output($project.'.pdf', 'D');    //download
        $pdf->Output($data[0]->fp_no.$data[0]->fp_type .'.pdf', 'I');    //read
    }

    // 清冊
    public function export_finePJ_lists()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        
        $this->load->library('PHP_TCPDF');
        $id = $this->uri->segment(3);
        $data = $this->getsqlmod->export_finePJ_lists($id)->result();
        
        
        $uname = $this->session-> userdata('uname');
        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(15.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('L', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $pdf->SetFont('twkai98_1','',8);

            $html = '<table border="1">
                    <thead>
                        <tr>
                            <td colspan="12" align="center" style="font-size:16px;">'.$data[0]->fp_type.'繳納清冊</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="right">列印日期：'.(date('Y')-1911).'年'.date('m').'月'.date('d').'日</td>
                        </tr>
                        <tr>
                            <td align="center">編號</td>
                            <td align="center">移送</td>
                            <td align="center">年度</td>
                            <td align="center">列管編號</td>
                            <td align="center">受處分人</td>
                            <td align="center">繳款日期</td>
                            <td align="center">繳款金額</td>
                            <td align="center">(分期/完納)</td>
                            <td align="center">核銷'.$data[0]->fp_type.'</td>
                            <td align="center">執行費</td>
                            <td align="center">轉正'.(($data[0]->fp_type == "罰鍰")?"怠金":"罰鍰").'</td>
                            <td align="center">退費</td>
                        </tr>
                    </thead>
                    <tbody>';
            $pay_tamount_sum = 0;
            $fpart_amount_sum = 0;
            $f_fee_sum = 0;
            $f_turnsub = 0;
            $f_refund = 0;
            for ($i=0; $i < count($data); $i++) { 

				$calcmovelog = explode("\n", $data[$i]->calc_movelog);
				$temp_moveday = 0;
				$new_movelog = '';
				foreach ($calcmovelog as $key => $value) {
					$movedate = (int)explode('_', $value)[0];

					if(($movedate * 1) > $temp_moveday)
					{
						$temp_moveday = ($movedate * 1);
						$new_movelog = explode('_', $value)[1];
					}
						
				}

                $html .= '<tr>
                    <td align="center">'.($i + 1).'</td>
                    <td align="center">'.$new_movelog.'</td>
                    <td align="center">'.$data[$i]->f_year.'</td>
                    <td align="center">'.$data[$i]->f_caseid.'</td>
                    <td align="center">'.$data[$i]->f_username.'</td>
                    <td align="right">'.$this->tranfer2RCyearSticky($data[$i]->fp_createdate).'</td>
                    <td align="right">$'.(($data[$i]->pay_tamount < 0)?number_format(0 - (int)$data[$i]->pay_tamount):number_format((int)$data[$i]->pay_tamount)).'</td>
                    <td align="center">'.$data[$i]->payway.'</td>
                    <td align="right">$'.number_format($data[$i]->fpart_amount).'</td>
                    <td align="right">$'.number_format($data[$i]->f_fee).'</td>
                    <td align="right">$'.(($data[$i]->f_turnsub < 0)?number_format(0 - (int)$data[$i]->f_turnsub):number_format((int)$data[$i]->f_turnsub)).'</td>
                    <td align="right">$'.number_format($data[$i]->f_refund).'</td>
                </tr>';
                $pay_tamount_sum = ($pay_tamount_sum + (($data[$i]->pay_tamount < 0)?(0 - (int)$data[$i]->pay_tamount):(int)$data[$i]->pay_tamount));
                $fpart_amount_sum = ($fpart_amount_sum + $data[$i]->fpart_amount);
                $f_fee_sum = ($f_fee_sum + $data[$i]->f_fee);
                $f_turnsub = ($f_turnsub + (($data[$i]->f_turnsub < 0)?(0-(int)$data[$i]->f_turnsub):(int)$data[$i]->f_turnsub));
                $f_refund = ($f_refund + $data[$i]->f_refund);
            }

            $html .= '</tbody>
                <tfoot>
                    <tr>
                        <td>合計</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="right">$'. number_format($pay_tamount_sum) .'</td>
                        <td></td>
                        <td align="right">$'. number_format($fpart_amount_sum) .'</td>
                        <td align="right">$'. number_format($f_fee_sum) .'</td>
                        <td align="right">$'. number_format($f_turnsub) .'</td>
                        <td align="right">$'. number_format($f_refund) .'</td>
                    </tr>
                </tfoot>
                </table>'; 
            
            
            $pdf->writeHTML($html, true, false, true, false, '');                        
        //$pdf->Output($project.'.pdf', 'D');    //download
        $pdf->Output($data[0]->fp_no.'_Lists.pdf', 'D');    //read
    }

    // 收繳登記
    public function export_finePJ_lists_by_indiv(){
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        
        $this->load->library('PHP_TCPDF');
        $id = $this->uri->segment(3);
        $data = $this->getsqlmod->get_finePJ_lists_user_in_distinct($id)->result();
        
        
        $uname = $this->session-> userdata('uname');
        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(15.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));

            for ($i=0; $i < count($data) ; $i++) { 
                $pdf->AddPage('L', 'A4');
                $pdf->SetAutoPageBreak(TRUE, 0);
                $pdf->SetFont('twkai98_1','',8);
                $html = '';

                $html = '<table border="1">
                            <thead>
                                <tr style="height:1.5em;">
                                    <td colspan="14" align="center" style="font-size:14px;">我是測試系統<br/>'. (date('Y')-1911) . '年毒品' .(($data[$i]->type == 'A')?'罰鍰':'怠金') . $data[$i]->listtype.'收繳登記簿</td>
                                </tr>
                                <tr>
                                    <td colspan="14" align="right">列印日期：'.(date('Y')-1911).'年'.date('m').'月'.date('d').'日</td>
                                </tr>
                                <tr>
                                    <td align="center">年度</td>
                                    <td align="center">月份</td>
                                    <td align="center">流水號</td>
                                    <td align="center">處分年度</td>
                                    <td align="center">案件</td>
                                    <td align="center">受處分人</td>
                                    <td align="center">繳款日期</td>
                                    <td align="center">繳款方式</td>
                                    <td align="center">'.(($data[$i]->listtype == "完納")?"繳款金額":"本期核銷金額").'</td>
                                    <td align="center">'.(($data[$i]->type == 'A')?'罰鍰':'怠金').'金額</td>
                                    <td align="center">'.(($data[$i]->listtype == "完納")?"本期核銷金額":((($data[$i]->type == 'A')?'罰鍰':'怠金') . "餘額")).'</td>
                                    <td align="center">執行費</td>
                                    <td align="center">狀態</td>
                                    <td align="center">備註</td>
                                </tr>
                            </thead>
                            <tbody>';
                
                $detail = $this->getsqlmod->get_finePJ_list_user_pay_detail($data[$i]->f_no)->result();
    
                $fpart_amount_sum = 0;
                $f_fee_sum = 0;
                $done_amount = 0;

                for ($j=0; $j < count($detail); $j++) { 
                    if($data[$i]->listtype == "完納")
                    {
                        $fpart_amount_sum = ($fpart_amount_sum + (int)$detail[$j]->fpart_amount + (int)$detail[$j]->f_fee);
                        $done_amount = $detail[$j]->f_doneamount;
                    }
                    else
                    {
                        $fpart_amount_sum = ($fpart_amount_sum + $detail[$j]->fpart_amount);
                    }
                    
                    $f_fee_sum = ($f_fee_sum + $detail[$j]->f_fee);

                    $html .= '<tr>
                        <td align="center">'.(($data[$i]->listtype == "完納")?$detail[$j]->d_year:$detail[$j]->year).'</td>
                        <td align="center">'.(($data[$i]->listtype == "完納")?$detail[$j]->d_month:$detail[$j]->month).'</td>
                        <td align="center">'.(($j == 0)?$detail[$j]->f_no:"").'</td>
                        <td align="center">'.$detail[$j]->f_year.'</td>
                        <td align="center">'.$detail[$j]->f_caseid.'</td>
                        <td align="center">'.$detail[$j]->f_username.'</td>
                        <td align="center">'.(($data[$i]->listtype == "完納")?$detail[$j]->donedate:$detail[$j]->paydate).'</td>
                        <td align="center">'.$detail[$j]->fpart_type.'</td>
                        <td align="center">$'.(($data[$i]->listtype == "完納")?number_format((int)$detail[$j]->fpart_amount + (int)$detail[$j]->f_fee + (int)$detail[$j]->f_refund):number_format($detail[$j]->fpart_amount)).'</td>
                        <td align="center">$'.number_format((($detail[$j]->type == "A")?((int)$detail[$j]->f_amount*10000):$detail[$j]->f_amount)).'</td>
                        <td align="center">$'.(($data[$i]->listtype == "完納")?number_format($detail[$j]->fpart_amount):number_format((($detail[$j]->type == "A")?((int)$detail[$j]->f_amount*10000):$detail[$j]->f_amount) - $fpart_amount_sum)).'</td>
                        <td align="center">'.(((int)$detail[$j]->f_fee == 0 )?"":"$".number_format($detail[$j]->f_fee)).'</td>
                        <td align="center">'.(((int)$detail[$j]->f_refund > 0)?'退款':($detail[$j]->f_status . '/' .$detail[$j]->f_cstatus)).'</td>
                        <td align="center">'.$detail[$j]->f_comment.'</td>
                    </tr>';
                    
                }

                $html .= '</tbody>
                <tfoot>
                    <tr>
                        <td>合計</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>$'. number_format($fpart_amount_sum) .'</td>
                        <td></td>
                        <td>'.(($data[$i]->listtype == "完納")?("$".number_format($done_amount)):"").'</td>
                        <td>$'. number_format($f_fee_sum) .'</td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
                </table>';

                $pdf->writeHTML($html, true, false, true, false, '');    
            }
                                
        //$pdf->Output($project.'.pdf', 'D');    //download
        $pdf->Output($data[0]->fp_no .'_Individual.pdf', 'D');    //read
    }

	// 收據號碼
    public function export_finePJ_lists_receipt(){
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        
        $this->load->library('PHP_TCPDF');
        $id = $this->uri->segment(3);
        $data = $this->getsqlmod->export_finePJ_lists_ticket_receipt_report($id)->result();
        $array = json_decode(json_encode($data),true);

		$vaild_receipt_count = 0;
        $invaild_receipt_count = 0;
        $vaild_receipt = array();
        $invaild_receipt = array();
        for ($n=0; $n < count($array) ; $n++) { 
            if($array[$n]['f_comment'] != '作廢')
            {
                $vaild_receipt_count = ($vaild_receipt_count +1);
                array_push($vaild_receipt, $array[$n]['f_receipt_no']);
            }                
            else
            {
                $invaild_receipt_count = ($invaild_receipt_count +1);
                array_push($invaild_receipt, $array[$n]['f_receipt_no']);
            }  
        }
        
        $uname = $this->session-> userdata('uname');
        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(15.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));

            
                $pdf->AddPage('L', 'A4');
                $pdf->SetAutoPageBreak(TRUE, 0);
                $pdf->SetFont('twkai98_1','',8);
                $html = '';

                $html = '<table border="1">
				<thead>
					<tr>
						<td colspan="17" align="center" style="font-size:16px;text-decoration: underline;">我是測試系統刑事警察大隊收入憑證報告單</td>
					</tr>
					<tr>
						<td colspan="3" align="left" style="font-size:12px;">編號：</td>
						<td colspan="8"></td>
						<td colspan="6" align="right" style="font-size:12px;">'.$this->tranfer2RCyearTrad($data[0]->fp_createdate).'填報</td>
					</tr>
					<tr>
						<td rowspan="3" align="center">收入憑證名稱</td>
						<td rowspan="2" colspan="2" align="center">上日結存數</td>
						<td rowspan="2" colspan="2" align="center">本日新領數</td>
						<td colspan="4" align="center">本日使用數</td>
						<td rowspan="3" align="center">開立金額</td>
						<td rowspan="3" align="center">本日實收金額</td>
						<td rowspan="2" colspan="2" align="center">截至本日止結存數</td>
						<td rowspan="2" colspan="4" align="center">備註<br/><span style="font-size:6px;">「本日實收金額」使用數據</span></td>
					</tr>
					<tr>
						<td colspan="2" align="center">繳核</td>
						<td colspan="2" align="center">作廢</td>
					</tr>
					<tr>
						<td align="center">張數</td>
						<td align="center">起迄號碼</td>
						<td align="center">張數</td>
						<td align="center">起迄號碼</td>
						<td align="center">張數</td>
						<td align="center">起迄號碼</td>
						<td align="center">張數</td>
						<td align="center">起迄號碼</td>
						<td align="center">張數</td>
						<td align="center">起迄號碼</td>
						<td align="center">收據號碼</td>
						<td align="center">金額</td>
						<td align="center">收據號碼</td>
						<td align="center">金額</td>
					</tr>
				</thead>
				<tbody>';
				$html .=  '<tr>';
				$html .=  '
				<td rowspan="9">罰金罰鍰怠金（毒品）</td>
				<td rowspan="9"></td>
				<td rowspan="9"></td>
				<td rowspan="9"></td>
				<td rowspan="9"></td>
				<td rowspan="9" align="center">'.$vaild_receipt_count.'</td>
				<td rowspan="9">'.implode("、\n",$vaild_receipt).'</td>
				<td rowspan="9" align="center">'.$invaild_receipt_count.'</td>
				<td rowspan="9">'.implode("、",$invaild_receipt).'</td>
				<td rowspan="9">$'.number_format($data[0]->fpart_amount) .'</td>
				<td rowspan="9">'.number_format($data[0]->fpart_amount) .'</td>
				<td rowspan="9"></td>
				<td rowspan="9"></td>';

				if(count($data) > 0)
				{
					$html .='
					<td>'.$data[0]->f_receipt_no.'</td>
					<td>$'.(($data[0]->f_comment != '作廢')?number_format($data[0]->paymoney):$data[0]->f_comment).'</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>';
				}
				$html .= '</tr>';
				for ($i=0; $i < 8; $i++) { 
					if(($i+1) < count($data))
					{
						$html .= '<tr>
						<td>'.(($data[$i+1]->f_receipt_no)?$data[$i+1]->f_receipt_no:'').'</td>
						<td>'.(($data[$i+1]->f_comment != '作廢')?(($data[$i+1]->paymoney)?('$'.number_format($data[$i+1]->paymoney)):''):$data[$i+1]->f_comment).'</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>';
					}
					else
					{
						$html .= '<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>';
					}
					

					$html .= '</tr>';
				}
                        // for ($i=0; $i < 5; $i++) { 
                        //     $htmlString_body .= '<tr>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td></tr>';
                        //     }
				for ($i=0; $i < 10; $i++) { 
					if(($i+8) <= count($data))
					{
						$html .= '<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>'.(($data[$i + 8]->f_receipt_no)?$data[$i]->f_receipt_no:'').'</td>
						<td>'.(($data[$i+8]->f_comment != '作廢')?(($data[$i + 8]->paymoney)?('$'.$data[$i]->paymoney):''):$data[$i+8]->f_comment).'</td>
						<td></td>
						<td></td>';
					}
					else
					{
						$html .= '<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>';
					}
					

					$html .= '</tr>';
				}
				$html .= '</tbody>
				<tfoot>
					<tr>
						<td>合計</td>
						<td colspan="2"></td>
						<td colspan="2"></td>
						<td colspan="2"  align="center">'.$vaild_receipt_count.'</td>
						<td colspan="2" align="center">'.$invaild_receipt_count.'</td>
						<td>'.number_format($data[0]->fpart_amount) .'</td>
						<td>'.number_format($data[0]->fpart_amount) .'</td>
						<td colspan="2"></td>
					</tr>
				</tfoot>
				</table>';   
				$html .= '<table>
					<tr>
						<td colspan="3">製表人</td>
						<td colspan="3">業務主管</td>
						<td colspan="3">出納</td>
						<td colspan="2">主辦會計</td>
						<td colspan="2">機關首長</td>
					</tr>
				</table>';  
                $pdf->writeHTML($html, true, false, true, false, '');    
                                
        // $pdf->Output($data[0]->f_project_id .'_ReceiptNO.pdf', 'D');    //download
        $pdf->Output($data[0]->f_project_id .'_ReceiptNO.pdf', 'I');    //read
    }

	

    // 帳務-報表查詢-罰金罰鍰怠金明細表
    public function finereport_print()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        
        $this->load->library('PHP_TCPDF');
        // $dateparam = $this->uri->segment(3);
        $dateparam = $_GET['param'];
        $data = $this->getsqlmod->get_finereport_month_case(explode(',',$dateparam)[0], explode(',',$dateparam)[1], $_GET['rtype'])->result();
        $fee_data = $this->getsqlmod->get_finereport_month_case_fee(explode(',',$dateparam)[0], explode(',',$dateparam)[1], $_GET['rtype'])->result();
        // $pay_tamount_sum = 0;
        // $first_username = '';
        // $name = array();
        // for ($i=0; $i < count($data); $i++) { 
        //     $pay_tamount_sum = ($pay_tamount_sum + $data[$i]->pay_tamount);
        //     $first_username = $data[0]->f_username;
        //     array_push($name, $data[$i]->f_username);
        // }
        // $monet_str = str_split($pay_tamount_sum,1);
        // $uname = $this->session-> userdata('uname');
        // ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(8, 3.5, 8,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('L', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $pdf->SetTitle((explode('-',explode(',',$dateparam)[1])[0]-1911) .'年'.explode('-',explode(',',$dateparam)[1])[1].'月毒品'.(($_GET['rtype'] == 'A')?'罰金罰鍰':'怠金').'明細表');
            $html='
            <h3 style="text-align:center;letter-spacing: 5px; ">'.(explode('-',explode(',',$dateparam)[1])[0]-1911) .'年'.explode('-',explode(',',$dateparam)[1])[1].'月毒品'.(($_GET['rtype'] == 'A')?'罰金罰鍰':'怠金').'明細表</h3>';
            $pdf->SetFont('twkai98_1','',8);
            $html .='
            <table style="width:980px;" border="1">
                <tr>
                    <td width="100px"></td>
                    <td colspan="2" align="center">本月收繳</td>
                    <td colspan="2" align="center">已註銷應收款歸屬'.(date('Y')-1911).'年度本月實收</td>
                    <td rowspan="2" align="center">對帳單金額</td>
                    <td colspan="2" align="center">累計收繳</td>
                    <td colspan="2" align="center">已註銷應收款歸屬'.(date('Y')-1911).'年度累計實收</td>
                    <td rowspan="2" align="center">對帳單金額</td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center">件數</td>
                    <td align="center">金額</td>
                    <td align="center">件數</td>
                    <td align="center">金額</td>
                    <td align="center">件數</td>
                    <td align="center">金額</td>
                    <td align="center">件數</td>
                    <td align="center">金額</td>
                </tr>';
            $t_casenum = 0;
            $t_receiveamount = 0;
            $t_del_casenum = 0;
            $t_del_receivemoney = 0;
            $t_grandcasenum = 0;
            $t_grandreceiveamount = 0;
            $t_del_grandcasenum = 0;
            $t_del_grandreceivemoney = 0;
            for ($i=0; $i < count($data) ; $i++) {
                $t_casenum = $t_casenum + $data[$i]->casenum;   
                $t_receiveamount = $t_receiveamount + $data[$i]->receivemoney; 
                $t_del_casenum = $t_del_casenum + $data[$i]->del_casenum;   
                $t_del_receivemoney = $t_del_receivemoney + $data[$i]->del_receivemoney;
                
                $t_grandcasenum = $t_grandcasenum + $data[$i]->grandcasenum;   
                $t_grandreceiveamount = $t_grandreceiveamount + $data[$i]->grandreceivemoney; 
                $t_del_grandcasenum = $t_del_grandcasenum + $data[$i]->del_grandcasenum;   
                $t_del_grandreceivemoney = $t_del_grandreceivemoney + $data[$i]->del_grandreceivemoney;
            }
            $html .='<tr>
                    <td align="center">合計</td>
                    <td align="center">'.$t_casenum.'</td>
                    <td align="center">'.number_format($t_receiveamount).'</td>
                    <td align="center">'.$t_del_casenum.'</td>
                    <td align="center">'.number_format($t_del_receivemoney).'</td>
                    <td></td>
                    <td align="center">'.$t_grandcasenum.'</td>
                    <td align="center">'.number_format($t_grandreceiveamount).'</td>
                    <td align="center">'.$t_del_grandcasenum.'</td>
                    <td align="center">'.number_format($t_del_grandreceivemoney).'</td>
                    <td></td>
                </tr>
                
                <tr>
                    <td align="center">'.$data[count($data) - 1]->f_year.'</td>
                    <td align="center">'.$data[count($data) - 1]->casenum.'</td>
                    <td align="center">'.number_format($data[count($data) - 1]->receivemoney).'</td>
                    <td align="center">'.$data[count($data) - 1]->del_casenum.'</td>
                    <td align="center">'.number_format($data[count($data) - 1]->del_receivemoney).'</td>
                    <td></td>

                    <td align="center">'.$data[count($data) - 1]->grandcasenum.'</td>
                    <td align="center">'.number_format($data[count($data) - 1]->grandreceivemoney).'</td>
                    <td align="center">'.$data[count($data) - 1]->del_grandcasenum.'</td>
                    <td align="center">'.number_format($data[count($data) - 1]->del_grandreceivemoney).'</td>
                    <td></td>
                </tr>
                <tr>
                    <td align="center">以前年度 <br/> (99-109年)</td>
                    <td align="center">'.($t_casenum - $data[count($data) - 1]->casenum).'</td>
                    <td align="center">'.number_format($t_receiveamount - $data[count($data) - 1]->receivemoney).'</td>
                    <td align="center">'.($t_del_casenum - $data[count($data) - 1]->del_casenum).'</td>
                    <td align="center">'.number_format($t_del_receivemoney - $data[count($data) - 1]->del_receivemoney).'</td>
                    <td></td>

                    <td align="center">'.($t_grandcasenum - $data[count($data) - 1]->grandcasenum).'</td>
                    <td align="center">'.number_format($t_grandreceiveamount - $data[count($data) - 1]->grandreceivemoney).'</td>
                    <td align="center">'.($t_del_grandcasenum - $data[count($data) - 1]->del_grandcasenum).'</td>
                    <td align="center">'.number_format($t_del_grandreceivemoney - $data[count($data) - 1]->del_grandreceivemoney).'</td>
                    <td></td>
                </tr>';
            
            for ($i=0; $i < count($data) - 1 ; $i++) { 
                $html .='<tr>
                    <td align="center">'.$data[$i]->f_year.'</td>
                    <td align="center">'. $data[$i]->casenum.'</td>
                    <td align="center">'.number_format($data[$i]->receivemoney).'</td>
                    <td align="center">'. $data[$i]->del_casenum.'</td>
                    <td align="center">'.number_format($data[$i]->del_receivemoney).'</td>';
                    if($i == 0)
                    {
                        $html .= '<td rowspan="'.(count($data) - 1).'"></td>';
                    }
                    $html .= '<td align="center">'. $data[$i]->grandcasenum.'</td>
                    <td align="center">'.number_format($data[$i]->grandreceivemoney).'</td>
                    <td align="center">'. $data[$i]->del_grandcasenum.'</td>
                    <td align="center">'.number_format($data[$i]->del_grandreceivemoney).'</td>';
                $html .='</tr>';
            } 

            $t_receivefee = 0;
            $t_del_receivefee = 0;
            $t_grandreceivefee = 0;
            $t_del_grandreceivefee = 0;
            for ($i=0; $i < count($fee_data) ; $i++) {
                $t_receivefee = $t_receivefee + $fee_data[$i]->receivefee; 
                $t_del_receivefee = $t_del_receivefee + $fee_data[$i]->del_receivefee;
                $t_grandreceivefee = $t_grandreceivefee + $fee_data[$i]->grandreceivefee; 
                $t_del_grandreceivefee = $t_del_grandreceivefee + $fee_data[$i]->del_grandreceivefee;
            }
            $html .='<tr>
                    <td align="center">執行費</td>
                    <td align="center" rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center">'.number_format($t_receivefee).'</td>
                    <td align="center" rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center">'.number_format($t_del_receivefee).'</td>
                    <td rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center" rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center">'.number_format($t_grandreceivefee).'</td>
                    <td align="center" rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center">'.number_format($t_del_grandreceivefee).'</td>
                    <td></td>
                </tr>
                
                <tr>
                    <td align="center">'.$fee_data[count($fee_data) - 1]->f_year.'</td>
                    <td align="center">'.number_format($fee_data[count($fee_data) - 1]->receivefee).'</td>
                    <td align="center">'.number_format($fee_data[count($fee_data) - 1]->del_receivefee).'</td>
                    

                    <td align="center">'.number_format($fee_data[count($fee_data) - 1]->grandreceivefee).'</td>
                    <td align="center">'.number_format($fee_data[count($fee_data) - 1]->del_grandreceivefee).'</td>
                    
                </tr>
                <tr>
                    <td align="center">以前年度 <br/> (99-109年)</td>
                    <td align="center">'.number_format($t_receivefee - $fee_data[count($fee_data) - 1]->receivefee).'</td>
                    <td align="center">'.number_format($t_del_receivefee - $fee_data[count($fee_data) - 1]->del_receivefee).'</td>
                   

                    <td align="center">'.number_format($t_grandreceivefee - $fee_data[count($fee_data) - 1]->grandreceivefee).'</td>
                    <td align="center">'.number_format($t_del_grandreceivefee - $fee_data[count($fee_data) - 1]->del_grandreceivefee).'</td>
                    
                </tr>';
                for ($i=0; $i < count($fee_data) - 1 ; $i++) { 
                    $html .='<tr>
                        <td align="center">'.$fee_data[$i]->f_year.'</td>
                        <td align="center">'.number_format($fee_data[$i]->receivefee).'</td>
                        <td align="center">'.number_format($fee_data[$i]->del_receivefee).'</td>';
                    $html .= '<td align="center">'.number_format($fee_data[$i]->grandreceivefee).'</td>
                        <td align="center">'.number_format($fee_data[$i]->del_grandreceivefee).'</td>';
                    $html .='</tr>';
                } 
            
            $html .='</table>';
            $pdf->writeHTML($html, true, false, true, false, '');                        
        //$pdf->Output($project.'.pdf', 'D');    //download
        $pdf->Output('報表測試.pdf', 'I');    //read
    }

    // 帳務-報表查詢-收入憑證月報表
    public function finereport_print_receipt()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        
        $this->load->library('PHP_TCPDF');
        $dateparam = $_GET['param'];
        $data = $this->getsqlmod->get_finereport_ticket_receipt(explode(',',$dateparam)[0], explode(',',$dateparam)[1])->result();
        if(count($data) <= 0)
            exit;
        $array = json_decode(json_encode($data),true);
        $vaild_receipt_count = 0;
        $invaild_receipt_count = 0;
        $vaild_receipt = array();
        $invaild_receipt = array();
        $t_paymoney = 0;
        for ($n=0; $n < count($array) ; $n++) { 
            if($array[$n]['f_comment'] != '作廢')
            {
                $vaild_receipt_count = ($vaild_receipt_count +1);
                array_push($vaild_receipt, $array[$n]['f_receipt_no']);
            }                
            else
            {
                $invaild_receipt_count = ($invaild_receipt_count +1);
                array_push($invaild_receipt, $array[$n]['f_receipt_no']);
            }  
            $t_paymoney = ($t_paymoney + (int)$array[$n]['paymoney']);
        }
        $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->setPrintHeader(false); //不要頁首
        $pdf->SetMargins(8, 3.5, 8,true);
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        $pdf->setFooterData(array(0,64,0), array(0,64,128));
        $pdf->AddPage('L', 'A4');
        $pdf->SetAutoPageBreak(TRUE, 0);
        $pdf->SetTitle((explode('-',explode(',',$dateparam)[1])[0]-1911) .'年'.explode('-',explode(',',$dateparam)[1])[1].'月收入憑證報告單');
        $html='
        <h3 style="font-size:16px;text-decoration: underline;text-align:center;">我是測試系統刑事警察大隊收入憑證報告單</h3>';
        $pdf->SetFont('twkai98_1','',8);
        $html.='<p style="text-align:center;">中華民國'.(explode('-',explode(',',$dateparam)[1])[0]-1911) .'年'.explode('-',explode(',',$dateparam)[1])[1].'月份</p>';
        $html .= '<table border="1">
                    <thead>
                        <tr>
                            <td colspan="9">編號：</td>
                            <td colspan="8" align="right">'.(date('Y')-1911) .'年'.date('m').'月'.date('d').'日填報</td>
                        </tr>
                        <tr>
                            <td rowspan="3" align="center">收入憑證名稱</td>
                            <td colspan="5" align="center">領用數</td>
                            <td colspan="5" align="center">本月使用數</td>
                            <td colspan="2" rowspan="2" align="center">截至本月止 <br/> 結存數</td>
                            <td rowspan="3" align="center">開立金額</td>
                            <td colspan="2" align="center">收入分析</td>
                            <td rowspan="3" align="center">備註</td>
                        </tr>
                        <tr>
                            
                            <td colspan="2" align="center">上日結存數</td>
                            <td colspan="2" align="center">本日新領數</td>
                            <td rowspan="2"  align="center">小計份數</td>
                            <td colspan="2" align="center">繳核</td>
                            <td colspan="2" align="center">作廢</td>
                            <td rowspan="2"  align="center">小計份數</td>
                            <td rowspan="2" align="center">本日實收金額</td>
                            <td rowspan="2" align="center">本日繳庫金額</td>
                            
                        </tr>
                        <tr>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                        </tr>
                    </thead>
                    <tbody>';
                    $html .= '<tr>';
                    $html .= '
                        <td  align="center">一般收款收據</td>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td ></td>                        
                        <td  align="center">'.$vaild_receipt_count.'</td>
                        <td  align="center">'.implode("、",$vaild_receipt).'</td>
                        <td  align="center">'.$invaild_receipt_count.'</td>
                        <td  align="center">'.implode("、",$invaild_receipt).'</td>
                        <td  align="center">'.count($data).'</td>
                        <td ></td>
                        <td ></td>                        
                        <td  align="center">'.number_format($t_paymoney) .'</td>
                        <td  align="center">'.number_format($t_paymoney) .'</td>
                        <td ></td>
                        <td rowspan="8"></td>';
                        $html .= '</tr>';

                        for ($i=0; $i < 6; $i++) { 
                            $html .= '<tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>';
                            

                            $html .= '</tr>';
                        }
                        
                    
                    
            $html .= '</tbody>
            <tfoot>
            <tr>
                <td>合計</td>
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td></td>
                <td colspan="2"  align="center">'.$vaild_receipt_count.'</td>
                <td colspan="2" align="center">'.$invaild_receipt_count.'</td>
                <td  align="center">'.count($data).'</td>
                <td colspan="2"></td>
                <td align="center">'.number_format($t_paymoney) .'</td>
                <td align="center">'.number_format($t_paymoney) .'</td>
                <td ></td>
            </tr>
            </tfoot>
            </table>';   
            $html.='<table>
                <tr>
                    <td>製表人</td>
                    <td>業務主管</td>
                    <td>出納</td>
                    <td>主辦會計</td>
                    <td>機關首長</td>
                </tr>
            </table>';            
            $pdf->writeHTML($html, true, false, true, false, '');                        
            //$pdf->Output($project.'.pdf', 'D');    //download
            $pdf->Output('我是測試系統刑事警察大隊收入憑證報告單.pdf', 'I');    //read
    }

    /** 轉民國年 */
    function tranfer2RCyear($date)
    {
        $date = str_replace('-', '', $date);
        $rc = ((int)substr($date, 0, 4)) - 1911;
        return (string)$rc . '年' . (int)substr($date, 4, 2)*1 . '月' . (int)substr($date, 6, 2)*1 . '日';
    }
	function tranfer2RCyear2($date)
    {
        $date = str_replace('-', '', $date);
		$rc = ((int)substr($date, 0, 4)) - 1911;
		return (string)$rc . '年' . substr($date, 4, 2) . '月' . substr($date, 6, 2) . '日';
    }
    /** 轉時分 */
    function tranfer2RChour($time)
    {
        if(null != $time)
        {
            $hour = explode(':', $time)[0];
			$min = explode(':', $time)[1];
			return $hour*1 . '時'.  $min*1 . '分';
        }
        else
        {
            return '';
        }
        
    }

    /** 轉民國年 - 連號 */
    public function tranfer2RCyearSticky($date)
    {
        $date = str_replace('-', '', $date);
        $rc = ((int)substr($date, 0, 4)) - 1911;
        return (string)$rc . substr($date, 4, 2) . substr($date, 6, 2);
    }
	/** 轉民國年 中文年月日 */
    function tranfer2RCyearTrad($date)
    {
        $date = str_replace('-', '', $date);
        $rc = ((int)substr($date, 0, 4)) - 1911;
        return (string)$rc . '年' . substr($date, 4, 2) . '月' . substr($date, 6, 2) . '日';
    }
	/** 轉民國年 中文年月日 */
    public function tranfer2ADyearCh($date)
    {
        if(strlen($date) == 6)
        {
            $ad = (substr($date, 0, 2));
            return (string)$ad .'年' . substr($date, 2, 2)*1 . '月' . substr($date, 4, 2)*1;
        }
        elseif(strlen($date) == 7)
        {
            $ad = (substr($date, 0, 3));
            return (string)$ad . '年' . substr($date, 3, 2)*1 . '月' . substr($date, 5, 2)*1;
        }
        else
        {
            return '';
        }
    }
	// 判斷是否成年
	function isAudlt($birth, $catchdate){
		list($year,$month,$day) = explode("-",$birth);
		$nowyear = date("Y", strtotime($catchdate));
		$nowmonth = date("m", strtotime($catchdate));
		$nowday = date("d", strtotime($catchdate));
		$audlt = 1; // 成年

		if(($year+20) <= $nowyear)
		{
			if(($year+20) == $nowyear)
			{
				if($month*1 <= $nowmonth*1)
				{
					if($month*1 == $nowmonth*1)
					{
						$audlt = ($day > $nowday)?0:1;
					}
				}							
				else
					$audlt = 0;
			}
		}
		else
		{
			$audlt = 0;
		}

		return (($audlt==0)?false:true);
	}
}
