<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Cases2 extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }    
    //function index() {
   //     redirect("cases2/listCases");
   // }
    function table_susp_sou($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getSourcelist($id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '身份證', '案件編號', '犯嫌人姓名', '犯罪手法','照片');
        $table_row = array();
        foreach ($query->result() as $susp)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases2/editsusp1/' . $susp->s_num .'#mark1', $susp->s_ic);
            if($susp->s_cnum == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_cnum;
            }
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;
            }
            if($susp->s_CMethods==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_CMethods;
            }
            if($susp->s_pic == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = anchor_popup('1susppic/' . $susp->s_pic, '犯嫌人照片');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    function table_cases() {
        $this->load->library('table');
        $this->load->model ( 'getsqlmod' ); // 載入model
        // if($this -> session -> userdata('uoffice') == '刑事警察大隊') $query = $this->getsqlmod->getCaseslist($this -> session -> userdata('uofficeAll')); // 改成session  分局
        // else $query = $this->getsqlmod->getCaseslist($this -> session -> userdata('uoffice')); // 改成session  分局
		$query = $this->getsqlmod->getCaseslist($this -> session -> userdata('uoffice'));
        $tmpl = array (
            'table_open' => '<table border="0"  class="table table-striped table-bordered table-hover" id="table1" style="width:100%;">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','','案件編號', '身份證', '犯嫌人', '查獲時間', '查獲地點','查獲證物','初驗成分', '查獲單位','毒報' );
        $table_row = array();
        foreach ($query->result() as $cases)
        {
            $taiwan_date = new DateTime($cases->s_date);
            $taiwan_date->modify("-1911 year");
            $table_row = NULL;
			$table_row[] = "";
			$table_row[] = $cases->c_num;
            $table_row[] = anchor('cases2/editCases2/' . $cases->c_num, $cases->c_num);
			if($cases->s_name==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $cases->s_name;
            }
			if($cases->s_ic==null){
                $table_row[] = '<strong>未選選</strong>';
            }
            else{
                $table_row[] = $cases->s_ic;
            }
            if($cases->s_date==null){
                $table_row[] = '<strong>未輸入日期</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad($cases->s_date)." ".$cases->s_time;
				//ltrim($taiwan_date->format("Y"),"0").'-'.$taiwan_date->format("m").'-'.$taiwan_date->format("d");  
            }
            if($cases->s_place==null){
                $table_row[] = '<strong>未輸入地點</strong>';
            }
            else{
                $table_row[] = $cases->s_place;
            }

			if($cases->e_name==null){
                $table_row[] = '<strong>無</strong>';
            }
            else{
                $table_row[] = $cases->e_name;
            }

			if($cases->e_type==null){
                $table_row[] = '<strong>無</strong>';
            }
            else{
                $table_row[] = $cases->e_type;
            }

			
            $table_row[] = $cases->s_office;
            
			if($cases->drug_doc==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $cases->drug_doc, '毒報');
            }
            
            // if($cases->doc_file==null){
            //     $table_row[] = '<strong>未上傳文件</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('uploads/' . $cases->doc_file, '戶役政/筆錄/搜扣');
            // }         
            // if($cases->other_doc==null){
            //     $table_row[] = '<strong>未上傳文件</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('uploads/' . $cases->other_doc, '其他檔案(逮捕通知書/拘票/同意書)');
            // }
            // $table_row[] = anchor('cases2/delcases/' . $cases->c_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_susps() {
        $this->load->library('table');
        $this->load->model ( 'getsqlmod' ); // 載入model
        // if($this -> session -> userdata('uoffice') == '刑事警察大隊') $query = $this->getsqlmod->getCaseslist($this -> session -> userdata('uofficeAll')); // 改成session  分局
        // else $query = $this->getsqlmod->getCaseslist($this -> session -> userdata('uoffice')); // 改成session  分局
		$query = $this->getsqlmod->getSuspslist($this -> session -> userdata('uoffice'));
        $tmpl = array (
            'table_open' => '<table border="0" width="100%"  class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','', '查獲單位', '身份證', '犯嫌人' ,'匯入時間','');
        $table_row = array();
        foreach ($query->result() as $susp)
        {
			$table_row = null;
			$table_row[] = $susp->s_num;
			$table_row[] = '';
            $table_row[] = $susp->s_roffice;
            if($susp->s_ic==null){
                $table_row[] = '<strong>未選選</strong>';
            }
            else{
                $table_row[] = $susp->s_ic;
            }
            if($susp->s_name==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->s_name;
            }
			$table_row[] = $this->tranfer2RCyear($susp->s_edtime);
            $table_row[] = '<button type="button" class="btn btn-link" data-toggle="modal" data-target="#trandferModal" data-href="cases2/trans_s_office/'.$susp->s_num.'">移轉</button>';
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	
    
    function table_cases3($id) {//裁罰 列表
        $this->load->library('table');
        $query = $this->getsqlmod->get3Susp($id)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table style="width: 100%;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','','','分局發文字號','涉刑事案件司法文書','身份證', '犯嫌人姓名', '戶籍','現住地址','出生日期','查獲時間','查獲地點','查獲單位','檢驗級別成分','尿液編號','持有毒品','毒報','尿報','處分書狀態');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            $drug = $this->getsqlmod->get3Drugfirst($susp->s_num); // 抓sic
            $drug_doc = $this->getsqlmod->get1Drug($susp->s_cnum)->result(); // 抓scnum
			$query2 = $this->getsqlmod->get1DrugCksec($drug_doc[0]->e_id); // 檢驗
			$cases = $this->getsqlmod->getCases($susp->s_cnum)->result_array();
            $table_row = NULL;
			$table_row[] = "";
            $table_row[] = $susp->s_num;
			$table_row[] = anchor('cases2/editsp/' . $susp->s_num, '編輯');
            //$table_row[] = '<input type="checkbox" name="a">';
            $sc_ingredient = Null;
            $drug_ingredient = Null;
            foreach ($query1->result() as $susp1){
                if($susp1->sc_num == null){}
                else{
                    $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.'<br>';
                }
            }
            foreach ($drug->result() as $drug3){
                if($drug3->df_num == null){}
                else{
                    $drug_ingredient = $drug_ingredient . $drug3->df_level .'級' . $drug3->df_ingredient.'<br>';
                }
            }
            if($susp->s_go_no == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->s_go_no;
            }
            if($susp->sp_doc == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $susp->sp_doc, '司法文書');
                //$table_row[] = $susp->sp_doc;
            }
            $table_row[] = $susp->s_ic;//anchor('cases2/editCases2/' . $susp->s_cnum, $susp->s_ic);
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;//form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_dpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpzipcode.$susp->s_dpaddress;
            }
            if($susp->s_rpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpzipcode.$susp->s_rpaddress;
            }
            if($susp->s_birth == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad3($susp->s_birth);
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad($susp->s_date);
            }
            if($susp->s_place == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_place;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            if($sc_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = str_replace(array(1,2,3,4,),array('一','二','三','四'),$sc_ingredient);
            }
            if($susp->s_utest_num==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $susp->s_utest_num;
            }
            if($drug_ingredient==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }
            if($cases[0]['drug_doc'] == null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $cases[0]['drug_doc'], '毒報');
            }
            if($susp->s_utest_doc==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('utest/' . $susp->s_utest_doc, '尿報');
            }
            if($susp->s_sac_state == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = ($susp->s_sac_state == '退回補正')?'<span class="text-danger">'.$susp->s_sac_state.'</span>':$susp->s_sac_state;
            }
            
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_cases3Other($id) {//裁罰 列表
        $this->load->library('table');
        $query = $this->getsqlmod->get3Susp_other($id)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table style="width: 100%;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','','發文字號','涉刑事案件司法文書','身份證', '犯嫌人姓名', '戶籍','現住地址','出生日期','查獲時間','查獲地點','查獲單位','檢驗級別成分','尿液編號','持有毒品','毒報','尿報','處分書狀態','');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            $drug = $this->getsqlmod->get3Drugfirst($susp->s_num); // 抓sic
            $drug_doc = $this->getsqlmod->get1Drug($susp->s_cnum)->result(); // 抓scnum
			$cases = $this->getsqlmod->getCases($susp->s_cnum)->result_array();
            $table_row = NULL;
			$table_row[] = '';
            $table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $sc_ingredient = Null;
            $drug_ingredient = Null;
            foreach ($query1->result() as $susp1){
                if($susp1->sc_num == null){}
                else{
                    $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.'<br>';
                }
            }
            foreach ($drug->result() as $drug3){
				if($drug3->df_num == null){}
                else{
					$drug_ingredient = $drug_ingredient . $drug3->df_level .'級' . $drug3->df_ingredient.'<br>';
                }
            }
            if($susp->s_go_no == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->s_go_no;
            }
            if($susp->sp_doc == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->sp_doc;
            }
            $table_row[] = anchor('cases2/editCases2/' . $susp->s_cnum, $susp->s_ic);
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_dpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpzipcode.$susp->s_dpaddress;
            }
            if($susp->s_rpaddress == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpzipcode.$susp->s_rpaddress;
            }
            if($susp->s_birth == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad($susp->s_birth);
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $this->tranfer2RCyearTrad($susp->s_date);
            }
            if($susp->s_place == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_place;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            if($sc_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = str_replace(array(1,2,3,4,),array('一','二','三','四'),$sc_ingredient);
            }
            if($susp->s_utest_num==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $susp->s_utest_num;
            }
            if($drug_ingredient==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }
            if($cases[0]['drug_doc'] == null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $cases[0]['drug_doc'], '毒報');
            }
            if($susp->s_utest_doc==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('utest/' . $susp->s_utest_doc, '尿報');
            }
            if($susp->s_sac_state == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->s_sac_state;
            }
            $table_row[] = anchor('cases2/editsp/' . $susp->s_num, '編輯');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_reward1($id) {//獎金 列表
        $this->load->library('table');
        $query = $this->getsqlmod->getSuspReward_normal($id)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','案件編號','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','淨重','驗後淨重','純質淨重','犯罪手法','退回次數');
        $table_row = array();
        $i=0;
        $query2='';
        return $this->table->generate();
    }

    function rewardApplication() { //獎金申請json
        include 'config.php';

        $query = $this -> session -> userdata('uoffice');
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Date search value
        $searchByFromdate = mysqli_real_escape_string($con,$_POST['searchByFromdate']);
        $searchByTodate = mysqli_real_escape_string($con,$_POST['searchByTodate']);

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (s_ic like '%".$searchValue."%' or 
                s_name like '%".$searchValue."%' or 
                s_reward_status like '%".$searchValue."%' or 
                s_CMethods like '%".$searchValue."%' or 
                s_reward_fine like '%".$searchValue."%' or 
                s_reward_rej like '%".$searchValue."%' or 
                s_cnum like'%".$searchValue."%' ) ";
        }

        ## Date filter
        if($searchByFromdate != '' && $searchByTodate != ''){
            $searchQuery .= " and (s_date between '".$searchByFromdate."' and '".$searchByTodate."' ) ";
        }
        
        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE 
                                 cases.c_num = suspect.s_cnum AND suspect.s_reward_project IS
                                 NULL AND suspect.s_reward_project2 IS NULL  AND 
                                 suspect.s_reward_project3 IS NULL AND r_office = '".$query."'".$searchQuery);  
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum AND suspect.s_reward_project IS NULL AND suspect.s_reward_project2 IS NULL  AND suspect.s_reward_project3 IS NULL AND r_office = '".$query."'".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            $drug = "select * from drug_evidence INNER JOIN drug_double_check WHERE drug_evidence.e_id = drug_double_check.ddc_drug AND drug_evidence.e_s_ic = '".$row['s_ic']."'";//." AND e_c_num ='". $row['s_cnum']."'"; 
            $drugRecords = mysqli_query($con, $drug);
            $drug2val=NULL;
            $drug2nw=Null;
            $drugcount=NULL;
            $drugcount[0]=$drugcount[1]=$drugcount[2]=$drugcount[3]=NULL;
            $drugrw[0]=$drugrw[1]=$drugrw[2]=$drugrw[3]=NULL;
            $drugpnw[0]=$drugpnw[1]=$drugpnw[2]=$drugpnw[3]=NULL;
            while ($row1 = mysqli_fetch_assoc($drugRecords)) {
                //$drug2val  = $row1['ddc_ingredient']; 
                    if(!$row1['ddc_ingredient']){
                        $drug2val = '<strong>未輸入</strong>';
                        //$drug2nw = Null;
                    }
                    else{
                        $drug2val = $drug2val .$row1['ddc_level'].'級' . $row1['ddc_ingredient'].'<br>';
                        //$drug2nw = $drug2nw . $row1['ddc_NW'] . '<br>';
                        switch ($row1['ddc_level']) {
                            case 1:
                                $drugcount[0] = ($drugcount[0] + $row1['ddc_NW']);
                                $drugrw[0] = ($drugrw[0] + $row1['ddc_RW']);
                                $drugpnw[0] = ($drugpnw[0] + $row1['ddc_PNW']);
                                break;
                            case 2:
                                $drugcount[1] = ($drugcount[1] + $row1['ddc_NW']);
                                $drugrw[1] = ($drugrw[1] + $row1['ddc_RW']);
                                $drugpnw[1] = ($drugpnw[1] + $row1['ddc_PNW']);
                                break;
                            case 3:
                                $drugcount[2] = ($drugcount[2] + $row1['ddc_NW']);
                                $drugrw[2] = ($drugrw[2] + $row1['ddc_RW']);
                                $drugpnw[2] = ($drugpnw[2] + $row1['ddc_PNW']);
                                break;
                            case 4:
                                $drugcount[3] = ($drugcount[3] + $row1['ddc_NW']);
                                $drugrw[3] = ($drugrw[3] + $row1['ddc_RW']);
                                $drugpnw[3] = ($drugpnw[3] + $row1['ddc_PNW']);
                                break;
                        }
                    }
            }
            if($drugcount[0]!=NULL) $drugcount[0] = '第一級:'.$drugcount[0]; 
            if($drugcount[1]!=NULL) $drugcount[1] = '<br>第二級:'.$drugcount[1]; 
            if($drugcount[2]!=NULL) $drugcount[2] = '<br>第三級:'.$drugcount[2]; 
            if($drugcount[3]!=NULL) $drugcount[3] = '<br>第四級:'.$drugcount[3]; 
            if($drugrw[0]!=NULL) $drugrw[0] = '第一級:'.$drugrw[0]; 
            if($drugrw[1]!=NULL) $drugrw[1] = '<br>第二級:'.$drugrw[1]; 
            if($drugrw[2]!=NULL) $drugrw[2] = '<br>第三級:'.$drugrw[2]; 
            if($drugrw[3]!=NULL) $drugrw[3] = '<br>第四級:'.$drugrw[3]; 
            if($drugpnw[0]!=NULL) $drugpnw[0] = '第一級:'.$drugpnw[0]; 
            if($drugpnw[1]!=NULL) $drugpnw[1] = '<br>第二級:'.$drugpnw[1]; 
            if($drugpnw[2]!=NULL) $drugpnw[2] = '<br>第三級:'.$drugpnw[2]; 
            if($drugpnw[3]!=NULL) $drugpnw[3] = '<br>第四級:'.$drugpnw[3]; 
            $address = $row['r_county'].$row['r_district'].$row['r_zipcode'].$row['r_address'];
            $data[] = array(
                    "s_num"=>$row['s_num'],
                    "s_ic"=>$row['s_ic'],
                    "s_name"=>$row['s_name'],
                    "s_reward_status"=>$row['s_reward_status'],
                    "s_reward_fine"=>$row['s_reward_fine'],
                    "s_date"=> $this->tranfer2RCyearTrad($row['s_date']),
                    "s_cnum"=>$row['s_cnum'],
                    "s_CMethods"=>$row['s_CMethods'],
                    "s_reward_rej"=>$row['s_reward_rej'],
                    "address"=>$address,
                    "s_office"=>$row['s_office'],
                    "ddc_ingredient"=>$drug2val,
                    "drug2nw"=>$drugcount[0].$drugcount[1].$drugcount[2].$drugcount[3],
                    "drug2rw"=>$drugrw[0].$drugrw[1].$drugrw[2].$drugrw[3],
                    "drug2pnw"=>$drugpnw[0].$drugpnw[1].$drugpnw[2].$drugpnw[3],

                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }
    
    function table_evi_rec($id) {//裁罰 列表
        $this->load->library('table');
        $query = $this->getsqlmod->getdrugRec($id)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '證物編號','證物內容','證物狀態','證物地點', '最後修改者','異動時間');
        $table_row = array();
        $i=0;
        foreach ($query as $drug_rec)
        {
            $table_row = NULL;
            $table_row[] = $drug_rec->drug_rec_eid;
			$table_row[] = $drug_rec->e_name;
            $table_row[] = $drug_rec->drug_rec_estatus;
            $table_row[] = $drug_rec->drug_rec_eplace;
            $table_row[] = $drug_rec->drug_rec_empno;
            $table_row[] = $this->tranfer2RCyearTrad2($drug_rec->drug_rec_time);
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_evi($id, $type) {//證物 列表
        $this->load->library('table');
        $type_options = array("" => '請選擇');
		switch ($type) {
			case 'keep':
				$query = $this->getsqlmod->getSuspinCases2($id, '單位保管中')->result(); // 使用getsqlmod裡的getdata功能
				break;
			case 'process':
				$query = $this->getsqlmod->getSuspinCases2($id, '送驗中')->result(); // 使用getsqlmod裡的getdata功能
				break;
			case 'check':
				$query = $this->getsqlmod->getSuspinCases2($id, '送地檢')->result();
				break;
			case 'inlocal':
				$query = $this->getsqlmod->getSuspinCases2($id, '領回')->result();
				break;
			case 'indrug':
				$query = $this->getsqlmod->getSuspinCases2($id, '送警察局')->result();
				break;
			case 'receive':
				$query = $this->getsqlmod->getSuspinCases2($id, '沒入物室')->result();
				break;
		}
        $office = $this -> session -> userdata('uoffice');
        // $query = $this->getsqlmod->getSusp($id)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover drug-table" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','案件編號','證物編號','姓名','身份證字號','證物內容','檢驗級別成分','重量','證物狀態','證物地點','毒報');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $drug = $this->getsqlmod->get3Drug2Double($susp->e_id); // 抓sic
            //$drug_doc = $this->getsqlmod->get1Drug($susp->e_id)->result(); // 抓scnum
            $table_row = NULL;
			$cnum = $this->getsqlmod->getsuspect('s_num', $susp->e_c_num)->result_array();
			$table_row[] = '';
            $table_row[] = anchor('cases2/editCases2/' . $cnum[0]['s_cnum'], $cnum[0]['s_cnum']);
            $table_row[] = $susp->e_id;
			$table_row[] = $susp->s_name;
			$table_row[] = $susp->s_ic;
			$table_row[] = $susp->e_name;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drug_ingredient = Null;
            foreach ($drug->result() as $drug3){
                if($drug3->df_num == null){}
                else{
                    $drug_ingredient = $drug_ingredient . $drug3->df_level .'級' . $drug3->df_ingredient.'<br>';
                }
            }
            if($drug_ingredient==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }
			$table_row[] = $susp->e_1_N_W;
            if($susp->e_state == '單位保管中'){
                $table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" type="text" class="form-control" onchange="updateStatus(\''.$susp->e_id.'\', $(this))">
                <option value="單位保管中">單位保管中【未驗】</option><option value="送驗中">送驗中</option>
                </select>';
            }
            else{
                if($susp->e_state == "送驗中"){
                    $table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" type="text" class="form-control" onchange="updateStatus(\''.$susp->e_id.'\', $(this))">
                    <option value="單位保管中">單位保管中【未驗】</option><option value="送驗中" selected="selected">送驗中</option><option value="領回">領回，單位保管中【已驗】</option>
                    </select>';
                }
				else if($susp->e_state == "領回"){
                    $table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" type="text" class="form-control" >
                    <option value="送驗中" >送驗中</option><option value="領回" selected="selected">領回，單位保管中【已驗】</option><option value="送地檢" >送地檢署(法院)</option>
                    </select>';
                }
                else if($susp->e_state == "送地檢"){
                    $table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" type="text" class="form-control" >
                    <option value="送驗中" >送驗中</option><option value="領回">領回，單位保管中【已驗】</option><option value="送地檢" selected="selected">送地檢署(法院)</option>
                    </select>'.((isset($susp->e_state_doc))?anchor_popup('drugdoc/' . $susp->e_state_doc, '地檢入庫清單'):'');
                }
                else if($susp->e_state == "送警察局"){
                    $table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" type="text" class="form-control" onchange="updateStatus(\''.$susp->e_id.'\', $(this))">
					<option value="送驗中" >送驗中</option><option value="領回">領回，單位保管中【已驗】</option><option value="送地檢" selected="selected">送地檢署(法院)</option>
                    </select>';
                }
                else{
                    $table_row[] =  '已沒入';
                }
            }
            if($susp->e_place == null){
                if($susp->e_state == "待送驗"){
                    $table_row[] =  $susp->r_office;
                }
                else if($susp->e_state == "送驗中"){
                    if($susp->e_place =="航醫中心"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option selected="selected">航醫中心</option><option>刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    if($susp->e_place =="刑事局"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option selected="selected">刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    if($susp->e_place =="調查局"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option selected="selected">調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    if($susp->e_place == null){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option>調查局</option><option  selected="selected">鑑識中心</option>
                        </select>';
                    }
                    else{
                        $table_row[] =  '<select id="e_state class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option>調查局</option><option  selected="selected">鑑識中心</option>
                        </select>';
                    }
                }
                else if($susp->e_state == "待裁罰"){
                    $table_row[] =  $susp->r_office;
                }
                else if($susp->e_state == "送地檢"){
                    // if($susp->e_place ==null){
                        // $table_row[] =  '<select id="e_place" class="e_place form-control" name="e_place['.$susp->e_id.']" type="text" data-value="'. $susp->e_place .'"><option>臺灣臺北地方檢察署</option><option>臺灣新北地方檢察署</option><option>臺灣士林地方檢察署</option><option>臺灣宜蘭地方檢察署</option><option>臺灣桃園地方檢察署</option><option>臺灣基隆地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣彰化地方法院檢察署</option><option>臺灣雲林地方檢察署</option><option>臺灣嘉義地方檢察署</option><option>臺灣花蓮地方檢察署</option><option>臺灣苗栗地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣臺南地方法院檢察署</option><option>臺灣南投地方法院埔里簡易庭</option></select>';
						$table_row[] = '<input type="text" class=" form-control" name="e_place['.$susp->e_id.']" value="'.$susp->e_place.'"/>';
                    // }else{
                    //      $table_row[] = $susp->e_place;
                    // }
                }
                else if($susp->e_state == "待銷毀"){
                    if($susp->e_place !="市警局"){
                        $table_row[] =  $susp->r_office;
                    }
                    else if($susp->e_place !="市警局"){
                        $table_row[] =  '市警局';
                    }
                }
                else{
                $table_row[] =  '刑案證物室';
                }
            }
            else{
                if($susp->e_state == "待送驗"){
                    $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="'.$office.'"/> ';
                }
                else if($susp->e_state == "送驗中"){
                    if($susp->e_place =="航醫中心"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option selected="selected">航醫中心</option><option>刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    else if($susp->e_place =="刑事局"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option selected="selected">刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    else if($susp->e_place =="調查局"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option selected="selected">調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    else if($susp->e_place =="鑑識中心"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option>調查局</option><option selected="selected">鑑識中心</option>
                        </select>';
                    }
                    else {
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                }
                else if($susp->e_state == "待裁罰"){
                    $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="'.$susp->r_office.'"/> ';
                }
                else if($susp->e_state == "送地檢"){
                    // if(mb_substr($susp->e_place, -1) !='署'){
                        // $table_row[] =  '<select id="e_place" class="e_place form-control" name="e_place['.$susp->e_id.']" type="text" data-value="'.$susp->e_place.'">
                        // <option>臺灣臺北地方檢察署</option><option>臺灣新北地方檢察署</option><option>臺灣士林地方檢察署</option><option>臺灣宜蘭地方檢察署</option><option>臺灣桃園地方檢察署</option>
                        // <option>臺灣基隆地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣彰化地方法院檢察署</option><option>臺灣雲林地方檢察署</option><option>臺灣嘉義地方檢察署</option>
                        // <option>臺灣花蓮地方檢察署</option><option>臺灣苗栗地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣臺南地方法院檢察署</option><option>臺灣南投地方法院埔里簡易庭</option>
                        // </select>';
						$table_row[] = '<input type="text" class=" form-control" name="e_place['.$susp->e_id.']" value="'.$susp->e_place.'"/>';
                    // }else if(mb_substr($susp->e_place, -1) =='署'){
                        //  $table_row[] = $susp->e_place;
                    // }
                }
                else if($susp->e_state == "待銷毀"){
                    if($susp->e_place !="市警局"){
                    $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="'.$susp->r_office.'"/> ';
                    }
                    else if($susp->e_place =="市警局"){
                    $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="市警局"/> ';
                    }
                    else{
                        $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="'.$susp->r_office.'"/> ';
                    }
                }
                else{
                    $table_row[] =  '刑案證物室';
                }
            }
            if($susp->drug_doc == null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $susp->drug_doc, '毒報');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_project_reward($id,$id2) {//裁罰 列表
        $this->load->library('table');
        $query = $this->getsqlmod->get3RewardProject($id,$id2)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','初驗淨重','淨重','建議獎金','退回次數');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $drug = $this->getsqlmod->get3Drugtest($susp->s_ic,$susp->s_cnum)->result() ; // 抓sic
            $drug_doc = $this->getsqlmod->get1DrugWithIC($susp->s_ic)->result(); // 抓scnum
            //$query2 = $this->getsqlmod->get2DrugCk('ddc_drug',$drug_doc[0]->e_id); 
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            $table_row[] = anchor('cases2/editCases2_Reward/' . $susp->s_cnum, $susp->s_ic);//
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            /*if($drug_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }*/
            $drug2val=Null;
            $drug2nw=Null;
            $drug2count=NULL;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $drug2val = Null;
                    $drug2nw = Null;
                }
                else{
                    $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
                    $drug2nw = $drug2nw . $drug2->ddc_NW . '<br>';
                    switch ($drug2->ddc_level) {
                        case 1:
                            $drugcount[0] = $drugcount[0] + $drug2->ddc_NW;
                            break;
                        case 2:
                            $drugcount[1] = $drugcount[1] + $drug2->ddc_NW;
                            break;
                        case 3:
                            $drugcount[2] = $drugcount[2] + $drug2->ddc_NW;
                            break;
                        case 4:
                            $drugcount[3] = $drugcount[3] + $drug2->ddc_NW;
                            break;
                        case 5:
                            $drugcount[4] = $drugcount[4] + $drug2->ddc_NW;
                            break;
                    }
                }
            }
            $drugreward = 0;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $$drugreward = 0;
                }
                else{
                    switch ($drug2->ddc_level) {
                        case 1:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 50) $drugreward = $$drugreward + 2500;
                            else if($drug2->ddc_NW >= 50) $drugreward = $drugreward + 5000;
                            break;
                        case 2:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 100) $drugreward = $drugreward + 2000;
                            else if($drug2->ddc_NW >= 100 && $drug2->ddc_NW < 250) $drugreward = $drugreward + 2500;
                            else if($drug2->ddc_NW >= 250 ) $drugreward = $drugreward + 5000;
                            break;
                        case 3:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                        case 4:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                    }
                }
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2nw;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                if($drugcount[0]>0){
                    $drug2count = '1級 = ' . $drugcount[0] . '<br>';
                }
                if($drugcount[1]>0){
                    $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
                }
                if($drugcount[2]>0){
                    $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
                }
                if($drugcount[3]>0){
                    $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
                }
                if($drugcount[4]>0){
                    $drug2count = $drug2count . '5級 = ' . $drugcount[4];
                }
                $table_row[] = $drug2count;
                //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
            }
            if($susp->s_reward_fine == null || $susp->s_reward_fine == 0){
                $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$drugreward.'"></input>';
            }
            else{
                $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$susp->s_reward_fine.'"></input>';
            }
            $table_row[] = $susp->s_reward_rej;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_project_reward2($id,$id2) {//裁罰 列表
        $this->load->library('table');
        $query = $this->getsqlmod->get3RewardProject2($id,$id2)->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','身份證', '犯嫌人姓名', '查獲時間','查獲地點','查獲單位','持有毒品','初驗淨重','淨重','建議獎金','退回次數');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $drug = $this->getsqlmod->get3Drugtest($susp->s_ic,$susp->s_cnum)->result() ; // 抓sic
            $drug_doc = $this->getsqlmod->get1DrugWithIC($susp->s_ic)->result(); // 抓scnum
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            $table_row[] = anchor('cases2/editCases2_Reward/' . $susp->s_cnum, $susp->s_ic);//
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;form_input('s_name',$susp->s_name, 'class="form-control" readonly');
            }
            if($susp->s_date == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_date;
            }
            if($susp->r_zipcode == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address;
            }
            if($susp->s_office == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_office;
            }
            /*if($drug_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug_ingredient;
            }*/
            $drug2val=Null;
            $drug2nw=Null;
            $drug2count=NULL;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $drug2val = Null;
                    $drug2nw = Null;
                }
                else{
                    $drug2val = $drug2val . $drug2->ddc_level .'級' . $drug2->ddc_ingredient.'<br>';
                    $drug2nw = $drug2nw . $drug2->ddc_NW . '<br>';
                    switch ($drug2->ddc_level) {
                        case 1:
                            $drugcount[0] = $drugcount[0] + $drug2->ddc_NW;
                            break;
                        case 2:
                            $drugcount[1] = $drugcount[1] + $drug2->ddc_NW;
                            break;
                        case 3:
                            $drugcount[2] = $drugcount[2] + $drug2->ddc_NW;
                            break;
                        case 4:
                            $drugcount[3] = $drugcount[3] + $drug2->ddc_NW;
                            break;
                        case 5:
                            $drugcount[4] = $drugcount[4] + $drug2->ddc_NW;
                            break;
                    }
                }
            }
            $drugreward = 0;
            foreach ($drug as $drug2){
                if($drug2->ddc_ingredient == null){
                    $$drugreward = 0;
                }
                else{
                    switch ($drug2->ddc_level) {
                        case 1:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 50) $drugreward = $$drugreward + 2500;
                            else if($drug2->ddc_NW >= 50) $drugreward = $drugreward + 5000;
                            break;
                        case 2:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 10) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 10 && $drug2->ddc_NW < 100) $drugreward = $drugreward + 2000;
                            else if($drug2->ddc_NW >= 100 && $drug2->ddc_NW < 250) $drugreward = $drugreward + 2500;
                            else if($drug2->ddc_NW >= 250 ) $drugreward = $drugreward + 5000;
                            break;
                        case 3:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                        case 4:
                            if($drug2->ddc_NW > 0 && $drug2->ddc_NW < 1) $drugreward = $drugreward + 500;
                            else if($drug2->ddc_NW >= 1 && $drug2->ddc_NW < 150) $drugreward = $drugreward + 1500;
                            else if($drug2->ddc_NW >= 150 ) $drugreward = $drugreward + 2000;
                            break;
                    }
                }
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2nw;
            }
            if($drug2nw == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                if($drugcount[0]>0){
                    $drug2count = '1級 = ' . $drugcount[0] . '<br>';
                }
                if($drugcount[1]>0){
                    $drug2count = $drug2count . '2級 = ' . $drugcount[1].'<br>';
                }
                if($drugcount[2]>0){
                    $drug2count = $drug2count . '3級 = ' . $drugcount[2].'<br>';
                }
                if($drugcount[3]>0){
                    $drug2count = $drug2count . '4級 = ' . $drugcount[3].'<br>';
                }
                if($drugcount[4]>0){
                    $drug2count = $drug2count . '5級 = ' . $drugcount[4];
                }
                $table_row[] = $drug2count;
                //$table_row[] = '1級 = ' . $drugcount[0].'<br>2級 = ' . $drugcount[1].'<br>3級 = ' . $drugcount[2].'<br>4級 = ' . $drugcount[3].'<br>5級 = ' . $drugcount[4];
            }
            if($susp->s_reward_fine == null || $susp->s_reward_fine == 0){
                $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$drugreward.'"></input>';
            }
            else{
                $table_row[] = '</input><input id="reward" name="reward['.$susp->s_num.']" type="number" value="'.$susp->s_reward_fine.'"></input>';
            }
            $table_row[] = $susp->s_reward_rej;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    
    function table_rewardProject1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getrewardproject($id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '專案編號', '申請狀態');
        $table_row = array();
        foreach ($query->result() as $rp)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases2/listrp2/' . $rp->rp_name, $rp->rp_name);
            if($rp->rp_status==null){
                $table_row[] = '申請中';
            }
            else{
                $table_row[] = $rp->rp_status;
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_drug1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Drug($id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '毒品編號', '數量', '毒品名稱','初驗淨重(g)', '毒品級別', '毒品成分', '擁有人', '');
        $table_row = array();
        foreach ($query->result() as $drug)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases2/editdrug1/' . $drug->e_id, $drug->e_id);
            if($drug->e_type == null){
                $table_row[] = '<strong>未輸入日期</strong>';
            }
            else{
                $table_row[] = $drug->e_count . $drug->e_type;
            }
            if($drug->e_name==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug->e_name;
            }
            $table_row[] = $drug->e_1_N_W;
            if($drug->df_level == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug->df_level;
            }
            if($drug->df_ingredient == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug->df_ingredient;
            }
            if($drug->e_suspect == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug->e_suspect;
            }
            $table_row[] = anchor('cases2/deldrug/' . $drug->e_id.'/'.$drug->e_c_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_susp1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Susp($id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '身份證', '犯嫌人姓名', '犯罪手法','照片','');
       
		$data = array();
        foreach ($query->result() as $susp)
        {
			$table_row = array();
            $table_row = NULL;
			$table_row[] = $susp->s_num;
            $table_row[] = anchor('cases2/editsusp1/' . $susp->s_num, $susp->s_ic);
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;
            }
            if($susp->s_CMethods==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_CMethods;
            }
            if($susp->s_pic == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = anchor_popup('1susppic/' . $susp->s_pic, '<img src= '.base_url("/1susppic/").''.$susp->s_pic.' alt="" width="50" height="50">');
            }
            $table_row[] = anchor('cases2/delsusp/' . $susp->s_num, '刪除');
            // $this->table->add_row($table_row);
			array_push($data, $table_row);
        }   
        // return $this->table->generate();
		return $data;
    }
    
    function table_drug2($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Drug($id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '毒品編號', '證物名稱', '成分','初驗淨重(g)', '初驗成分', '擁有人','檢驗成分','淨重(g)','驗後餘重(g)','純質淨重(g)', '');
        $data = array();
        foreach ($query->result() as $drug)
        {
			$table_row = array();
            $drugcount[0]=0;
            $drugcount[1]=0;
            $drugcount[2]=0;
            $drugcount[3]=0;
            $drugcount[4]=0;
            $drugcount[5]=0;
            $query1 = $this->getsqlmod->get1DrugCkfirst($drug->e_id); // 初驗
            $query2 = $this->getsqlmod->get1DrugCksec($drug->e_id); // 檢驗
            $table_row = NULL;
            $table_row[] = anchor('cases2/editdrug2/' . $drug->e_id.'/'.$id, $drug->e_id.'<small class="text-muted"> (填寫檢驗成分結果)</small>');
			if($drug->e_name==null){
                $table_row[] = '<strong>無</strong>';
            }
            else{
                $table_row[] = $drug->e_name;
            }
            if($drug->e_type == null){
                $table_row[] = '<strong>無</strong>';
            }
            else{
                $table_row[] = $drug->e_type;
            }
            
			if($drug->e_1_N_W == null){
                $table_row[] = '0';
            }
            else{
                $table_row[] = $drug->e_1_N_W;
            }
            $drug1val=Null;
            foreach ($query1->result() as $drug1){
                if($drug1->df_ingredient == null){
                    $drug1val = Null;
                }
                else{
                    $drug1val = $drug1val . ((isset($drug1->df_level) || !empty($drug1->df_level))?(str_replace(array(1,2,3,4,),array('一','二','三','四'),$drug1->df_level) .'級'):'') . $drug1->df_ingredient.'<br>';
                }
            }
            if($drug1val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug1val;
            }
			$table_row[] = $drug->e_c_num;
            if($drug->e_c_num == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $this->getsqlmod->get1DrugSusp($drug->e_c_num)->result_array()[0]['s_name'];
            }
            $drug2val=Null;
            $drug2nw=Null;
            $drug2rw=Null;
            $drug2pnw=Null;
            $drug2count=Null;
            foreach ($query2->result() as $drug2){
                if($drug2->df_ingredient == null){
                    $drug2val = Null;
                    $drug2nw = Null;//淨重
                    $drug2rw = Null;//驗後餘重
                    $drug2pnw = Null;//純質淨重
                }
                else{
                    $drug2val = $drug2val . $drug2->df_level .'級' . $drug2->df_ingredient.'<br>';
                    $drug2nw = $drug2nw . $drug2->df_PNW . '<br>';
                    $drug2rw = $drug2rw . $drug2->df_NW . '<br>';
                    $drug2pnw = $drug2pnw . $drug2->df_PNWS . '<br>';
                    switch ($drug2->df_level) {
                        case 1:
                            $drugcount[0] = $drugcount[0] + $drug2->df_PNW;
                            break;
                        case 2:
                            $drugcount[1] = $drugcount[1] + $drug2->df_PNW;
                            break;
                        case 3:
                            $drugcount[2] = $drugcount[2] + $drug2->df_PNW;
                            break;
                        case 4:
                            $drugcount[3] = $drugcount[3] + $drug2->df_PNW;
                            break;
                        case 5:
                            $drugcount[4] = $drugcount[4] + $drug2->df_PNW;
                            break;
                    }
                }
            }
            if($drug2val == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug2val;
            }
			$table_row[] = $drug->e_PNW;
			$table_row[] = $drug->e_NW;
			$table_row[] = $drug->e_PNWS;
            
            // if($drug->e_doc==null){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('drugdoc/' . $drug->e_doc, '毒報');
            // }
            $table_row[] = anchor('cases2/deldrug/' . $drug->e_id.'/'.$drug->e_c_num, '刪除');
			$table_row[] = $drug->e_togethe;
			$togeAry = array();
			if(isset($drug->e_togethe))
			{
				foreach (explode(',', $drug->e_togethe) as $toge) {
					array_push($togeAry, $this->getsqlmod->get1DrugSusp($toge)->result_array()[0]['s_name']);
				}
				$table_row[] = implode("、", $togeAry);
			}
			else
			{
				$table_row[] = NULL;
			}
			
            // $this->table->add_row($table_row);
			array_push($data, $table_row);
        }   
        // return $this->table->generate();
		return $data;
    }

    function table_susp2($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Susp($id)->result(); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '身份證', '犯嫌人姓名', '尿液編號','檢驗級別成分','尿報檔案','處分書狀態','');
        $data = array();
        foreach ($query as $susp)
        {
			$table_row = array();
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            $table_row = NULL;
			$table_row[] = $susp->s_num;
            $table_row[] = anchor('cases2/editsusp2/' . $susp->s_num, $susp->s_ic.'<small class="text-muted"> (尿報編輯/上傳)</small>');
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;
            }
            $sc_ingredient = Null;
            foreach ($query1->result() as $susp1){
                if($susp1->sc_num == null){}
                else{
                    $sc_ingredient = $sc_ingredient . $susp1->sc_level .'級' . $susp1->sc_ingredient.'<br>';
                }
            }
            if($susp->s_utest_num == null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_utest_num;
            }
            if($sc_ingredient==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $sc_ingredient;
            }
            if($susp->s_utest_doc==null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('utest/' . $susp->s_utest_doc, '尿報');
            }
            if($susp->s_sac_state == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $susp->s_sac_state;
            }
            $table_row[] = anchor('cases2/delsusp/' . $susp->s_num, '刪除','onclick="alert(\'重新加入案件請到新增誤合併頁面加入之。\')"');
            // $this->table->add_row($table_row);
			array_push($data, $table_row);
        }   
        // return $this->table->generate();
		return $data;
    }
    
    function table_car1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Car('v_s_num',$id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '車牌號碼', '交通工具種類', '顏色','');
        $table_row = array();
        foreach ($query->result() as $car)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases2/editcar/' . $car->v_num, $car->v_license_no);
            if($car->v_type == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $car->v_type;
            }
            if($car->v_color==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $car->v_color;
            }
            $table_row[] = anchor('cases2/delcar/' . $car->v_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    function table_social($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getSocial('s_num',$id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '社群軟體名稱', '社群帳號1', '社群帳號2','社群帳號3','');
        $table_row = array();
        foreach ($query->result() as $social)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases2/editsocialmedia/' . $social->social_media_num, $social->social_media_name);
            if($social->social_media_ac1 == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $social->social_media_ac1;
            }
            if($social->social_media_ac2==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $social->social_media_ac2;
            }
            if($social->social_media_ac3==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $social->social_media_ac3;
            }
            $table_row[] = anchor('cases2/delsocial/' . $social->social_media_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_phone($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getPhone('p_s_num',$id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '手機號碼', '手機序號(IMEI)', '查扣手機','');
        $table_row = array();
        foreach ($query->result() as $phone)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases2/editphone/' . $phone->p_num, $phone->p_no);
            if($phone->p_imei == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $phone->p_imei;
            }
            if($phone->p_conf==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $phone->p_conf;
            }
            $table_row[] = anchor('cases2/delphone/' . $phone->p_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_source($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getSource1('sou_s_num',$id); // 使用getsqlmod裡的getdata功能
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '姓名', '手機號碼', '交易時間', '交易地點', '交易金額', '交易數量','');
        $table_row = array();
        foreach ($query->result() as $source)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases2/editsource/' . $source->sou_num, $source->sou_name);
            if($source->sou_phone == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_phone;
            }
            if($source->sou_time == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_time;
            }
            if($source->sou_place == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_place;
            }
            if($source->sou_amount == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_amount;
            }
            if($source->sou_count == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_count;
            }
            $table_row[] = anchor('cases2/delsource/' . $source->sou_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    function logout() {
        $this -> load -> library('session');
        $this -> session -> unset_userdata('uic');
        redirect('login/','refresh'); 
    }
    public function listSouceCases() {
       // generate HTML table from query results
        $this->load->helper('form');
        if($this -> session -> userdata('uoffice') == '刑事警察大隊') $data['data_table'] = $this->table_susp_sou($this -> session -> userdata('uofficeAll'));  
        else $data['data_table'] = $this->table_susp_sou($this -> session -> userdata('uoffice')); 
        $data['title'] = "案件處理系統:溯源列表";
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/cases_list_Sou';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2_list';
        $this->load->view('template1', $data);
    }
    public function listCases() {
       // generate HTML table from query results
        $this->load->helper('form');
        $test_table = $this->table_cases();
        $data['data_table'] = $test_table;
        $data['title'] = "案件處理系統:列表";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['include'] = 'cases2/cases_list';
		// $data['include'] = 'cases2/import_case';
        $data['nav'] = 'navbar2_list';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $this->load->view('template1', $data);
    }

	public function addNewCases() {
		// generate HTML table from query results
		$this->load->helper('form');
		$query = $this->getsqlmod->getdata(1,1)->result();
        $countid = $query[0]->c_num;
        $lastNum = (int)mb_substr($countid, -4, 4);
        $lastNum++;
        $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
       
        $type_options = array(
            '中正第一分局' => '中正第一分局' ,
            '中正第二分局' => '中正第二分局',
            '文山第一分局' => '文山第一分局',
            '文山第二分局' => '文山第二分局',
            '信義分局' => '信義分局',
            '大安分局' => '大安分局',
            '中山分局' => '中山分局',
            '松山分局' => '松山分局',
            '大同分局' => '大同分局',
            '萬華分局' => '萬華分局',
            '南港分局' => '南港分局',
            '內湖分局' => '內湖分局',
            '士林分局' => '士林分局',
            '北投分局' => '北投分局',
            '刑事警察大隊偵查第一隊' => '刑事警察大隊偵查第一隊',
            '刑事警察大隊偵查第二隊' => '刑事警察大隊偵查第二隊',
            '刑事警察大隊偵查第三隊' => '刑事警察大隊偵查第三隊',
            '刑事警察大隊偵查第四隊' => '刑事警察大隊偵查第四隊',
            '刑事警察大隊偵查第五隊' => '刑事警察大隊偵查第五隊',
            '刑事警察大隊偵查第七隊' => '刑事警察大隊偵查第七隊',
            '刑事警察大隊偵查第八隊' => '刑事警察大隊偵查第八隊',
            '刑事警察大隊偵查第六隊' => '毒緝中心',
            '刑事警察大隊科技犯罪偵查隊' => '科偵隊',
            '刑事警察大隊肅竊組' => '肅竊組',
        );
		$opt_cases = array();
		$opt_cases[''] = '請選擇現有案件';
		foreach ($this->getsqlmod->getAllCases()->result_array() as $opt_case_row) {
			$opt_cases[$opt_case_row['c_num']] = '【'.$opt_case_row['c_num'].'】 '. $opt_case_row['s_date'].' '.$opt_case_row['s_time']. ' '. $opt_case_row['s_place'];
		}
        $data['c_num']='CN' . $taiwan_date.$value;
        $data['s_office']=$this -> session -> userdata('uroffice');
        $data['opt'] = $type_options;
		if($this -> session -> userdata('uoffice') == '刑事警察大隊') $data['r_office']=$this -> session -> userdata('uoffice').$this -> session -> userdata('uroffice');
        else $data['r_office']=$this -> session -> userdata('uoffice');

		 $test_table = $this->table_susps();
		 $data['opt_cases'] = $opt_cases;
		 $data['s_table'] = $test_table;
		 $data['title'] = "案件處理系統:列表";
		 $data['user'] = $this -> session -> userdata('uic');
		 // $data['include'] = 'cases2/cases_list';
		 $data['include'] = 'cases2/import_case';
		 $data['nav'] = 'navbar2_list';
		 $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
		 $this->load->view('template1', $data);
	 }
    
    public function admin() {//管理權限
        $this->load->helper('form');
        $this->load->library('table');
        $this->load->model('getsqlmod');
        $dep = $this->getsqlmod->getdep($this -> session -> userdata('uoffice'))->result();
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $type_options = array("" => '請選擇');
        foreach ($dep as $depnm){
            $type_options[$depnm->dep_nm] = $depnm->dep_nm;
        }
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '賬號', '姓名', '分局','職稱','單位','權限','驗證碼','異動');
        $data['s_table'] = $this->table->generate();
        $data['title'] = "管理權限獲取驗證碼";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['include'] = 'cases2/IsAdmin';
        $data['nav'] = 'navbar2';
        $data['options'] = $type_options;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $this->load->view('template', $data);
    }
    
    public function change() {//管理權限
        $this->load->helper('form');
        $this->load->library('table');
        $this->load->model('getsqlmod');
        $dep = $this->getsqlmod->getdep($this -> session -> userdata('uoffice'))->result();
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $type_options = array("" => '請選擇');
        foreach ($dep as $depnm){
            $type_options[$depnm->dep_nm] = $depnm->dep_nm;
        }
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '賬號', '姓名', '原分局','職稱','原單位','加入分局');
        $data['s_table'] = $this->table->generate();
        $data['title'] = "異動人員列表";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['include'] = 'cases2/PChange';
        $data['options'] = $type_options;
        $data['nav'] = 'navbar2';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $this->load->view('template', $data);
    }

    function testjson() {
        include 'config.php';

        $query = $this -> session -> userdata('uoffice');
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (em_mailId like '%".$searchValue."%' or 
                em_lastname like '%".$searchValue."%' or 
                em_centername like '%".$searchValue."%' or 
                em_firstname like '%".$searchValue."%' or 
                em_officeAll like '%".$searchValue."%' or 
                em_office like '%".$searchValue."%' or 
                em_roffice like'%".$searchValue."%' ) ";
        }

        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees WHERE em_priority != '99' and em_office = '".$query."'".$searchQuery);
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from employees WHERE em_priority != '99' and em_office = '".$query."'".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            $edit = '<a href="#change" data-toggle="modal" data-job-id="'.$row['em_job'].'" data-e-id="'.$row['em_ic'].'" data-email-id="'.$row['em_mailId'].'" data-ln-id="'.$row['em_lastname'].'" data-fn-id="'.$row['em_firstname'].'" class="callModal">異動人員</a>';
            if($row['em_IsAdmin'] == '1') $link = anchor('login/changead/1/' . $row['em_mailId'], '更改為一般使用者');
            if($row['em_IsAdmin'] == '0') $link = anchor('login/changead/0/' . $row['em_mailId'], '更改為管理員');
            if($row['em_IsAdmin'] == '2') $link = '特殊管理者(僅警局承辦人可更改)';
            $data[] = array(
                    "em_lastname"=>$row['em_lastname'].$row['em_centername'].$row['em_firstname'],
                    "em_officeAll"=>$row['em_officeAll'],
                    "em_office"=>$row['em_office'],
                    "em_job"=>$row['em_job'],
                    "em_IsAdmin"=> $link,
                    //"change"=> anchor('login/changead/99/' . $row['em_mailId'], '異動人員'),
                    "change"=> $edit,
                    "em_change_code"=>$row['em_change_code'],
                    "em_mailId"=>$row['em_mailId'],
                    "em_roffice"=>$row['em_roffice']
                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }
    function changejson() {
        include 'config.php';

        $query = '99';
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (em_mailId like '%".$searchValue."%' or 
                em_lastname like '%".$searchValue."%' or 
                em_centername like '%".$searchValue."%' or 
                em_firstname like '%".$searchValue."%' or 
                em_officeAll like '%".$searchValue."%' or 
                em_office like '%".$searchValue."%' or 
                em_priority like '%".$searchValue."%' or 
                em_roffice like'%".$searchValue."%' ) ";
        }

        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees WHERE em_priority = '".$query."'".$searchQuery);
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from employees WHERE em_priority = '".$query."'".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            $edit = '<a href="#change" data-toggle="modal" data-job-id="'.$row['em_job'].'" data-e-id="'.$row['em_ic'].'" data-email-id="'.$row['em_mailId'].'" data-ln-id="'.$row['em_lastname'].'" data-fn-id="'.$row['em_firstname'].'" class="callModal">加入分局</a>';
            $data[] = array(
                    "em_lastname"=>$row['em_lastname'].$row['em_centername'].$row['em_firstname'],
                    "em_officeAll"=>$row['em_officeAll'],
                    "em_office"=>$row['em_office'],
                    "em_job"=>$row['em_job'],
                    //"em_IsAdmin"=>  anchor('login/changeoffice/' . $row['em_mailId'], '加入分局'),
                    "em_IsAdmin"=>  $edit,
                    "em_change_code"=>$row['em_change_code'],
                    "em_mailId"=>$row['em_mailId'],
                    "em_roffice"=>$row['em_roffice']
                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }

    public function change_dep() {//異動人員
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('getsqlmod','',TRUE);
        if($_POST['em_roffice'] == '偵查隊')$_POST['em_priority']=2;
        else $_POST['em_priority']=1;
        if(Isset($_POST['inout']) && $_POST['inout'] == '0')  
        {
            $_POST['em_priority']=99;
            unset($_POST['em_roffice']);
        }else{
            $_POST['em_officeAll']=$this -> session -> userdata('uoffice').$_POST['em_roffice'];
            $_POST['em_office']=$this -> session -> userdata('uoffice');
        }
        //var_dump($_POST);
        unset($_POST['inout']);
        $this->getsqlmod->changedep($_POST['em_mailId'],$_POST);
        redirect('cases2/admin/','refresh'); 
    }
    
    public function createcases_num() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $query = $this->getsqlmod->getdata(1,1)->result();
        $countid = $query[0]->c_num;
        $lastNum = (int)mb_substr($countid, -4, 4);
        $lastNum++;
        $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $data['c_num']='CN' . $taiwan_date.$value;
        $_POST['s_office']=$this -> session -> userdata('uroffice');
        if($this -> session -> userdata('uoffice') == '刑事警察大隊') $_POST['r_office']=$this -> session -> userdata('uoffice').$this -> session -> userdata('uroffice');
        else $_POST['r_office']=$this -> session -> userdata('uoffice');
        $_POST['c_num']=$data['c_num'];
        $_POST['r_office1']=$this -> getsqlmod -> r_office1($_POST['r_office']);
        $_POST['e_empno']=$this -> session -> userdata('email');
        $_POST['e_ed_empno']=$this -> session -> userdata('email');
        $this->getsqlmod->addcases($_POST);
        redirect('cases2/newcases/' . $data['c_num'],'refresh'); 
    }
    
    public function newcases() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        $data['row'] = $casesrepair[0];
        $type_options = array(
            '中正第一分局' => '中正第一分局' ,
            '中正第二分局' => '中正第二分局',
            '文山第一分局' => '文山第一分局',
            '文山第二分局' => '文山第二分局',
            '信義分局' => '信義分局',
            '大安分局' => '大安分局',
            '中山分局' => '中山分局',
            '松山分局' => '松山分局',
            '大同分局' => '大同分局',
            '萬華分局' => '萬華分局',
            '南港分局' => '南港分局',
            '內湖分局' => '內湖分局',
            '士林分局' => '士林分局',
            '北投分局' => '北投分局',
            '刑事警察大隊偵查第一隊' => '刑事警察大隊偵查第一隊',
            '刑事警察大隊偵查第二隊' => '刑事警察大隊偵查第二隊',
            '刑事警察大隊偵查第三隊' => '刑事警察大隊偵查第三隊',
            '刑事警察大隊偵查第四隊' => '刑事警察大隊偵查第四隊',
            '刑事警察大隊偵查第五隊' => '刑事警察大隊偵查第五隊',
            '刑事警察大隊偵查第七隊' => '刑事警察大隊偵查第七隊',
            '刑事警察大隊偵查第八隊' => '刑事警察大隊偵查第八隊',
            '刑事警察大隊偵查第六隊' => '毒緝中心',
            '刑事警察大隊科技犯罪偵查隊' => '科偵隊',
            '刑事警察大隊肅竊組' => '肅竊組',
        );
        $data['c_num']=$data['row']->c_num;
        $data['s_office']=$data['row']->s_office;
        $data['title'] = "案件處理系統:新增";
        $data['user'] = $this -> session -> userdata('uic');
        $data['drug_table'] = $this->table_drug1($id);
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['s_table'] = $this->table_susp1($id);
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['opt'] = $type_options;
        $data['include'] = 'cases2/new_cases';
        $this->load->view('template', $data);
    }
    
    public function createcases() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('table');
        $this->load->library('form_validation'); 
        // $this->load->model('getsqlmod','',TRUE);
		if(empty($_POST['s_ic']))
		{
			redirect('cases2/addNewCases', 'refresh'); 
			exit;
		}
		$s_ic = json_decode($_POST['s_ic'], true);
		
		// $this->getsqlmod->addcases(array('c_num' => $_POST['c_num']));
        // $casesrepair = $this->getsqlmod->getCases($_POST['c_num'])->result();
        // $data['drug_table'] = $this->table_drug1($_POST['c_num']);
        // $data['s_table'] = $this->table_susp1($_POST['c_num']);
        // $data['row'] = $casesrepair[0];
        // $data['c_num']=$data['row']->c_num;
        // $data['s_office']=$data['row']->s_office;
        // $link = $_POST['link'];
        // $data['title'] = "案件處理系統:新增案件";
        // $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        // $data['nav'] = 'navbar2';
        // $data['user'] = $this -> session -> userdata('uic');
        // $data['include'] = 'cases/new_cases';
        $_POST['drug_pic'] = null;
        $_POST['doc_file'] = null;
        $_POST['other_doc'] = null;
        if(!empty($_FILES['drugpic']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['drugpic']['name'];
            $_FILES['file']['type'] = $_FILES['drugpic']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['drugpic']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['drugpic']['error'];
            $_FILES['file']['size'] = $_FILES['drugpic']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['drug_pic']=$uploadData['file_name'];
            }
        }
        if(!empty($_FILES['doc_file']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['doc_file']['name'];
            $_FILES['file']['type'] = $_FILES['doc_file']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['doc_file']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['doc_file']['error'];
            $_FILES['file']['size'] = $_FILES['doc_file']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['doc_file']=$uploadData['file_name'];
            }
        }
        if(!empty($_FILES['other_doc']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['other_doc']['name'];
            $_FILES['file']['type'] = $_FILES['other_doc']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['other_doc']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['other_doc']['error'];
            $_FILES['file']['size'] = $_FILES['other_doc']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['other_doc']=$uploadData['file_name'];
            }
        }
        $_POST['r_address'] = strtoupper($_POST['r_address']);
        $_POST['s_place'] = strtoupper($_POST['s_place']);
		$_POST['s_date'] = $this->tranfer2ADyear($_POST['s_date']);
        // unset($_POST['link']);
		$update_case = array(
			'r_address' => $_POST['r_address'],
			's_office' => $_POST['s_office'],
			'r_office' => $_POST['r_office'],
			's_place' => $_POST['s_place'],
			'detection_process' => $_POST['detection_process'],
			's_date' => $_POST['s_date'],
			'other_doc' => $_POST['other_doc'],
			'doc_file' => $_POST['doc_file'],
			'drug_pic' => $_POST['drug_pic']
		);
        // $this->getsqlmod->updateCase($_POST['c_num'],$update_case);
		foreach ($s_ic as $key => $value) {
			$snum = $this->getsqlmod->checkSuspexist($value['userid'])->result()[0]->s_num;
			$susp_update = array(
				's_cnum' => $_POST['c_num']
			);
			$this->getsqlmod->updateSusp($snum, $susp_update);
			// $phone_update = array(
			// 	'p_s_cnum' => $_POST['c_num']
			// );
			// $this->getsqlmod->updatePhone($snum, $phone_update);
			// $phonerec_update = array(
			// 	'pr_s_cnum' => $_POST['c_num']
			// );
			// $this->getsqlmod->updatePhoneRec2($snum, $phonerec_update);
			// $drug_update = array(
			// 	'e_c_num' => $_POST['c_num']
			// );
			// $this->getsqlmod->updateDrug1byS_ic($value['userid'], $drug_update);
		}
        redirect('cases2/listCases'); 
    }
    
    public function newsuspect() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        //$casesrepair = $this->getsqlmod->getCases($id)->result();
        $data['s_cnum']=$id;
        $data['title'] = "案件處理系統:新增犯嫌人";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['include'] = 'cases2/new_suspect';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases2/new_cases' ); 
    }
    
    public function create_suspect1() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        /*$data['title'] = "案件處理系統";
        $data['user'] = "分局承辦人";
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/new_drug';*/
        $link=$_POST['link'];
        unset($_POST['link']);
        ////var_dump($_POST);
        $_POST['s_CMethods'] = implode(',',$_POST['s_CMethods']); 
		$_POST['s_birth'] = $this->tranfer2ADyear($_POST['s_birth']);
        if(!empty($_FILES['susppic']['name'])){
            $_FILES['susppic']['name']  = $_POST['s_cnum'] . $_FILES['susppic']['name'];
            $config['upload_path']          = '1susppic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('susppic')){
                $error = array('error' => $this->upload->display_errors());
                ////var_dump($error);
                redirect($link,'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['s_pic']=$uploadData['file_name'];
                $this->getsqlmod->addsuspect($_POST);
                redirect($link,'refresh'); 
                //redirect('cases2/edit/' . $_POST['s_cnum'],'refresh'); 
            }
        }
        else{
            $_POST['s_pic']=Null;
            ////var_dump($_POST);
            $this->getsqlmod->addsuspect($_POST);
            redirect($link,'refresh'); 
        }
    }

    function edit(){//修改案件 案件修改 casesedit
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        ////echo $casesrepair[1]->c_num;
        $type_options = array(
            '中正第一分局' => '中正第一分局' ,
            '中正第二分局' => '中正第二分局',
            '文山第一分局' => '文山第一分局',
            '文山第二分局' => '文山第二分局',
            '信義分局' => '信義分局',
            '大安分局' => '大安分局',
            '中山分局' => '中山分局',
            '松山分局' => '松山分局',
            '大同分局' => '大同分局',
            '萬華分局' => '萬華分局',
            '南港分局' => '南港分局',
            '內湖分局' => '內湖分局',
            '士林分局' => '士林分局',
            '北投分局' => '北投分局',
            '刑事警察大隊偵查第一隊' => '刑事警察大隊偵查第一隊',
            '刑事警察大隊偵查第二隊' => '刑事警察大隊偵查第二隊',
            '刑事警察大隊偵查第三隊' => '刑事警察大隊偵查第三隊',
            '刑事警察大隊偵查第四隊' => '刑事警察大隊偵查第四隊',
            '刑事警察大隊偵查第五隊' => '刑事警察大隊偵查第五隊',
            '刑事警察大隊偵查第七隊' => '刑事警察大隊偵查第七隊',
            '刑事警察大隊偵查第八隊' => '刑事警察大隊偵查第八隊',
            '刑事警察大隊偵查第六隊' => '毒緝中心',
            '刑事警察大隊科技犯罪偵查隊' => '科偵隊',
            '刑事警察大隊肅竊組' => '肅竊組',
        );
        $type_options1 = array(
            '路檢' => '路檢' ,
            '臨檢' => '臨檢',
            '專案勤務(無令狀)' => '專案勤務(無令狀)',
            '毒品調驗人口' => '毒品調驗人口',
            '拘提/搜索' => '拘提/搜索',
            '通知到案' => '通知到案',
            '借訊(提)' => '借訊(提)',
            '報案(110)' => '報案(110)',
            '99' => '其他',
        );
        //$test = array($casesrepair[0]->r_office, $casesrepair[0]->r_office);
        $data['row'] = $casesrepair[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['c_num'] = $id;
        $data['title'] = "案件修改:". $id;
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = "navbar2";
        $data['drug_table'] = $this->table_drug2($id);//$this->table_drug1($id);
        $data['s_table'] = $this->table_susp1($id);
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/cases_edit';
        $this->load->view('template', $data);
    }  
    
    public function new_drug() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $Susp = $this->getsqlmod->get1DrugSusp($id)->result();
        $c_value = substr($id,-3);
        $type_options = array();
        foreach ($Susp as $Susp1){
            $type_options[$Susp1->s_ic] = $Susp1->s_name;
        }
        $taiwan_date = date('Y')-1911; 
        $query = $this->getsqlmod->get1DrugCount()->result(); 
        $countid = $query[0]->e_id;
        $lastNum = (int)mb_substr($countid, -5, 5);
        $lastNum++;
        $value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
        $data['e_id']='EN' . $taiwan_date. $value;
        $data['e_c_num']=$id;
        $data['opt']=$type_options;
        $data['e_empno']=$this -> session -> userdata('uname');
        $data['e_ed_empno']=$this -> session -> userdata('uname');
        $data['title'] = "案件處理系統:新增毒品證物";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['include'] = 'cases2/new_drug';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases2/new_cases' ); 
    }
    public function create_drug1() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        // var_dump($_POST);
		// exit;
        //echo '<br>';
		$df = $_POST['df'];
		unset($_POST['df']);
		// $df_num = $_POST['df_num'];
		// unset($_POST['df_num']);
		$e_s_ic = $_POST['e_s_ic'];
		unset($_POST['e_s_ic']);
		foreach ($e_s_ic as $ic_key => $ic_value) {
			if($ic_key != 0)
			{
				$query = $this->getsqlmod->get1DrugCount()->result(); 
				$countid = $query[0]->e_id;
				$lastNum = (int)mb_substr($countid, -5, 5);
				$lastNum++;
				$value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
				$taiwan_date = date('Y')-1911; 
				$_POST['e_id']='EN' . $taiwan_date . $value;
			}
			foreach ($df as $key => $value) {
				$dataSet1 = array();
				switch ($value) {
					case '海洛因':
						$dataSet1[] = array (
							  'df_level' => '1',
							  'df_ingredient' => $value,
							  'df_drug' => $_POST['e_id'],
							//   'df_count' => $_POST['df_num'][$key]
						 );
						break;
					case '安非他命':
						$dataSet1[] = array (
							  'df_level' => '2',
							  'df_ingredient' => $value,
							  'df_drug' => $_POST['e_id'],
							//   'df_count' => $_POST['df_num'][$key]
						 );
						break;
					case '搖頭丸(MDMA)':
						$dataSet1[] = array (
							  'df_level' => '2',
							  'df_ingredient' => $value,
							  'df_drug' => $_POST['e_id'],
							//   'df_count' => $_POST['df_num'][$key]
						 );
						break;
					case '愷他命(KET)':
						$dataSet1[] = array (
							  'df_level' => '3',
							  'df_ingredient' => $value,
							  'df_drug' => $_POST['e_id'],
							//   'df_count' =>$_POST['df_num'][$key]
						 );
						break;
					case '一粒眠':
						$dataSet1[] = array (
							  'df_level' => '3',
							  'df_ingredient' => $value,
							  'df_drug' => $_POST['e_id'],
							//   'df_count' => $_POST['df_num'][$key]
						 );
						break;
					case '大麻':
						$dataSet1[] = array (
							  'df_level' => '2',
							  'df_ingredient' => $value,
							  'df_drug' => $_POST['e_id'],
							//   'df_count' => $_POST['df_num'][$key]
						 );
						break;
					case '卡西酮類':
						$dataSet1[] = array (
							  'df_level' => '3',
							  'df_ingredient' => $value,
							  'df_drug' => $_POST['e_id'],
							//   'df_count' => $_POST['df_num'][$key]
						 );
						break;
					case '古柯鹼':
						$dataSet1[] = array (
							  'df_level' => '2',
							  'df_ingredient' => $value,
							  'df_drug' => $_POST['e_id'],
							//   'df_count' =>$_POST['df_num'][$key]
						 );
						break;
					default:
						//echo " ";
						break;
				}
				//echo '<br>';
				
				$this->getsqlmod->adddrug1($dataSet1);
				////var_dump($dataSet1);
			}
			$susp = $this->getsqlmod->get1Susp_ed('s_ic',$ic_value)->result();
			$_POST['e_suspect']=$susp[0]->s_name;
			$_POST['e_s_ic'] = $value;
			$this->getsqlmod->adddrug($_POST);
		}
        
        ////echo '<br>';
        
        
        ////var_dump($_POST);
        redirect('cases2/edit/' . $_POST['e_c_num'],'refresh'); 
    }
    
    function editdrug1(){ //修改一階毒品
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $drugAll = $this->getsqlmod->get1DrugAll($id)->result();
        $Susp = $this->getsqlmod->get1DrugSusp($drugAll[0]->e_c_num)->result();
        $drugck = $this->getsqlmod->get1DrugCk($id)->result();
        ////echo $casesrepair[1]->c_num;
        $type_options = array(
            '毒品粉末、碎塊' => '毒品粉末、碎塊' ,
            '藥碇' => '藥碇',
            '梅碇' => '梅碇',
            '即溶包' => '即溶包',
            '香菸(含菸蒂)' => '香菸(含菸蒂)',
            '菸草' => '菸草',
            '濾嘴' => '濾嘴',
            '吸食器' => '吸食器',
            '吸管' => '吸管',
            '卡片' => '卡片',
            '括盤' => '括盤',
            '棉花棒' => '棉花棒',
            '殘渣袋' => '殘渣袋',
            '大麻' => '大麻',
            '手機殼' => '手機殼',
            '植株' => '植株',
            '種子' => '種子',
       );   
        $type_options1 = array(
            '包' => '包' ,
            '顆' => '顆',
            '支' => '支',
            '張' => '張',
            '個' => '個',
            '組' => '組',
            '株' => '株',
        );
        $type_options2 = array(
            '海洛因' => '海洛因' ,
            '安非他命' => '安非他命',
            '搖頭丸(MDMA)' => '搖頭丸(MDMA)',
            '愷他命(KET)' => '愷他命(KET)',
            '一粒眠' => '一粒眠',
            '大麻' => '大麻',
            '卡西酮類' => '卡西酮類',
            '古柯鹼' => '古柯鹼',
            '無毒品反應' => '無毒品反應',
        );
        $type_options3 = array('' => '請選擇');
        foreach ($Susp as $Susp1){
            $type_options3[$Susp1->s_ic] = $Susp1->s_name;
        }
        $data['drugAll'] = $drugAll[0];
        $data['drugck'] = $drugck;
        $data['nameopt'] = $type_options;
        $data['countopt'] = $type_options1;
        $data['nwopt'] = $type_options2;
        $data['test'] = $type_options3;
        $data['e_id'] = $id;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['e_c_num'] = $drugAll[0]->e_c_num;
        $data['title'] = "毒品修改:". $id;
        $data['user'] = $this -> session -> userdata('uic');
        $data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/drug1_edit';
        $this->load->view('template', $data);
    }  
    
    public function update_drug1() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        ////var_dump($_POST);
        //echo '<br>';
        foreach ($this->input->post('df') as $key => $value) {
            $dataSet1 = array();
            switch ($value) {
                case '海洛因':
                    $dataSet1[] = array (
                          'df_level' => '1',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '安非他命':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '搖頭丸(MDMA)':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '愷他命(KET)':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '一粒眠':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '大麻':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '卡西酮類':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '古柯鹼':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                default:
                    //echo " ";
                    break;
            }
            //echo '<br>';
            $this->getsqlmod->deleteDrugCkfor_Ed($_POST['e_id'],$value);
            $this->getsqlmod->adddrug1($dataSet1);
            ////var_dump($dataSet1);
        }
        ////echo '<br>';
        unset($_POST['df']);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['e_s_ic'])->result();
        $_POST['e_suspect']=$susp[0]->s_name;
        $this->getsqlmod->updateDrug1($_POST['e_id'],$_POST);
        ////var_dump($_POST);
        redirect('cases2/edit/' . $_POST['e_c_num'],'refresh'); 
    }
    
    function deleteCk(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $drugck = $this->getsqlmod->get1DrugCkdel($id)->result();
        $this->getsqlmod->deleteDrugCk($id);
        ////var_dump ($drugck);
        redirect('cases2/editdrug2/' . $drugck[0]->df_drug.'/'.$this->uri->segment(4),'refresh');
    }
    function editsusp1(){ //修改犯嫌
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        ////var_dump ($susp[0]);
        $image_properties = array(
                  'src' => 'img/logo.gif',
                  'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                  'class' => 'post_images',
                  'width' => '50',
                  'height' => '50',
        );     
        $type_options = array(
            '未受教育' => '未受教育' ,
            '國小' => '國小',
            '國中' => '國中',
            '高中' => '高中',
            '大學' => '大學',
            '碩士' => '碩士',
            '博士' => '博士',
        );
        $type_options1 = array(
            ' ' => '請選擇' ,
            '忘記來源' => '忘記來源',
            '否認施用' => '否認施用',
            '國外購入' => '國外購入',
            '6' => '其他',
        );
        $methods = mb_split(",",$susp[0]->s_CMethods);
        $data['持有'] = '';
        $data['意圖販售而持有'] = '';
        $data['販賣'] = '';
        $data['運輸'] = '';
        $data['栽種'] = '';
        $data['施用'] = '';
        $data['製造'] = '';
        $data['轉讓'] = '';
        foreach($methods as $value){
            $data[$value] = $value;
            ////echo $data[$value];
        }
        ////var_dump($data);
        $data['row'] = $susp[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['img'] = $image_properties;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['s_cnum'] = $id;
        $data['nav'] = 'navbar2';
        $data['sourcetable'] = $this->table_source($susp[0]->s_num);
        $data['phonetable'] = $this->table_phone($susp[0]->s_num);
        $data['socialtable'] = $this->table_social($susp[0]->s_num);
        $data['cartable'] = $this->table_car1($susp[0]->s_num);
        $data['title'] = "犯嫌人修改:" .$susp[0]->s_name;
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/edit_suspect';
        $this->load->view('template', $data);
    }  
    
    public function createcar() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['s_ic'])->result();
        ////echo $susp[0]->s_cnum;
        for($i=0, $count = count($_POST['v_color']);$i<$count;$i++) {
            $dataSet1 = array();
            $dataSet1[] = array (
                'v_type' => $_POST['v_type'][$i],
                'v_license_no' => $_POST['v_license_no'][$i],
                'v_owner ' => $_POST['s_ic'],
                'v_s_num ' => $_POST['s_num'],
                'v_color' => $_POST['v_color'][$i],
            );
            ////var_dump($dataSet1);
            $this->getsqlmod->addcar($dataSet1);
        }
        redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    public function newcar() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_num']=$susp[0]->s_num;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['title'] = "案件處理系統:新增車輛";
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/new_car';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases2/new_cases' ); 
    }
    
    function editcar(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $car = $this->getsqlmod->get1Car('v_num',$id)->result();
        ////var_dump ($susp[0]);
        $type_options = array(
            '機車' => '機車' ,
            '汽車' => '汽車',
            '貨車' => '貨車',
        );
        $data['row'] = $car[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['s_cnum'] = $id;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['title'] = "車輛修改:".$car[0]->v_license_no;
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/car_edit';
        $this->load->view('template', $data);
    }  
    
    public function update_car() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $this->getsqlmod->updateCar($_POST['v_num'],$_POST);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['v_owner'])->result();
        ////var_dump($susp[0]);
        redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }

    public function casesUpdate(){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $now = date('Y-m-d H:i:s');
        $link = $_POST['link'];
        //$_POST['drug_pic'] = null;
        //$_POST['doc_file'] = null;
        //$_POST['other_doc'] = null;
        if(!empty($_FILES['drug_pic']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['drug_pic']['name'];
            $_FILES['file']['type'] = $_FILES['drug_pic']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['drug_pic']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['drug_pic']['error'];
            $_FILES['file']['size'] = $_FILES['drug_pic']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['drug_pic']=$uploadData['file_name'];
            }
        }
        if(!empty($_FILES['doc_file']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['doc_file']['name'];
            $_FILES['file']['type'] = $_FILES['doc_file']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['doc_file']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['doc_file']['error'];
            $_FILES['file']['size'] = $_FILES['doc_file']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['doc_file']=$uploadData['file_name'];
            }
        }
        if(!empty($_FILES['other_doc']['name'])){
            $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['other_doc']['name'];
            $_FILES['file']['type'] = $_FILES['other_doc']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['other_doc']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['other_doc']['error'];
            $_FILES['file']['size'] = $_FILES['other_doc']['size'];
            $config['upload_path'] = 'uploads/'; 
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '5000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $_POST['other_doc']=$uploadData['file_name'];
            }
        }
            $_POST['e_ed_date']=$now;
            $_POST['e_ed_empno']=$this -> session -> userdata('uid');
			$_POST['s_date'] = $this->tranfer2ADyear($_POST['s_date']);
            unset($_POST['link']);
            $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
            redirect($link,'refresh'); 
    }
    public function suspect1Update(){//修改犯嫌
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $link = $_POST['link'];
        $_POST['s_CMethods'] = implode(',',$_POST['s_CMethods']); 
		$_POST['s_birth'] = $this->tranfer2ADyear($_POST['s_birth']);
        if(!empty($_FILES['susppic']['name'])){
            $_FILES['susppic']['name']  = $_POST['s_cnum'] . $_FILES['susppic']['name'];
            $config['upload_path']          = '1susppic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('susppic')){
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
                //$this->load->view('upload_form', $error);
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['s_pic']=$uploadData['file_name'];
                //$this->getsqlmod->addsuspect($_POST);
                ////var_dump($_POST);
                //redirect($link,'refresh'); 
                //redirect('cases2/edit/' . $_POST['s_cnum'],'refresh'); 
            }
        }
            ////var_dump($_POST);
            unset($_POST['link']);
            $this->getsqlmod->updateSusp($_POST['s_num'],$_POST);
            redirect($link,'refresh'); 
        //$now = date('Y-m-d H:i:s');
    }
    
    public function createsocialmedia() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['s_ic'])->result();
        ////echo $susp[0]->s_cnum;
        for($i=0, $count = count($_POST['social_media_name']);$i<$count;$i++) {
            $dataSet1 = array();
            $dataSet1[] = array (
                'social_media_name' => $_POST['social_media_name'][$i],
                'social_media_ac1' => $_POST['social_media_ac1'][$i],
                'social_media_ac2 ' => $_POST['social_media_ac2'][$i],
                'social_media_ac3' => $_POST['social_media_ac3'][$i],
                's_ic' => $_POST['s_ic'],
                's_cnum' => $_POST['s_cnum'],
                's_num' => $_POST['s_num'],
            );
            //var_dump($dataSet1);
            $this->getsqlmod->addsocialmedia($dataSet1);
        }
        redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    public function newsocialmedia() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_cnum']= $susp[0]->s_cnum;
        $data['s_num']= $susp[0]->s_num;
        $data['title'] = "案件處理系統:新增社交媒體";
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/new_socialmedia';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases2/new_cases' ); 
    }
    
    function editsocialmedia(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $social = $this->getsqlmod->getSocial('social_media_num',$id)->result();
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['row'] = $social[0];
        $data['nav'] = 'navbar1';
        //$data['test'] = $test;
        $data['title'] = "修改社交媒體";
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/socialmedia_edit';
        $this->load->view('template', $data);
    }  
    
    public function updatesocialmedia() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $this->getsqlmod->updateSocial($_POST['social_media_num'],$_POST);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['s_ic'])->result();
        ////var_dump($susp[0]);
        redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }

    public function createphone() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['p_s_ic'])->result();
        for($i=0, $count = count($_POST['pr_phone']);$i<$count;$i++) {
            $dataSet1[] = array();
            $dataSet1[$i] = array (
                'pr_path' => $_POST['pr_path'][$i],
                'pr_phone' => $_POST['pr_phone'][$i],
                'pr_name ' => $_POST['pr_name'][$i],
                'pr_time' => $_POST['pr_time'][$i],
                'pr_relationship' => $_POST['pr_relationship'][$i],
                'pr_has_drug' => $_POST['pr_has_drug'][$i],
                'pr_user' => $_POST['pr_user'][$i],
                'pr_seller' => $_POST['pr_seller'][$i],
                'pr_s_ic ' => $_POST['p_s_ic'],
                'pr_s_cnum' => $_POST['p_s_cnum'],
                'pr_p_no' => $_POST['p_no'],
            );
        }
        unset($_POST['pr_path']);
        unset($_POST['pr_phone']);
        unset($_POST['pr_name']);
        unset($_POST['pr_time']);
        unset($_POST['pr_relationship']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_user']);
        unset($_POST['pr_seller']);
        ////var_dump($_POST);
        $this->getsqlmod->addphone($_POST);
        $phone = $this->getsqlmod->getPhone('p_s_ic',$_POST['p_s_ic'])->result();
        for($i=0, $count1 = count($dataSet1);$i<$count1;$i++) {
            $dataSet1[$i]['pr_p_num'] = $phone[0]->p_num;
        }
        $this->getsqlmod->addphone_rec($dataSet1);
        redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    public function newphone() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_cnum']= $susp[0]->s_cnum;
        $data['s_num']= $susp[0]->s_num;
        $data['title'] = "案件處理系統:新增手機";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['include'] = 'cases2/new_phone';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases2/new_cases' ); 
    }
    
    function editphone(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $phone = $this->getsqlmod->getPhone('p_num',$id)->result();
        $phonerec = $this->getsqlmod->getPhoneRec('pr_p_num',$phone[0]->p_num)->result();
        $type_options = array(
            '是' => '是' ,
            '否' => '否',
        );
        $type_options1 = array(
            '撥入' => '撥入' ,
            '撥出' => '撥出',
        );
        ////var_dump($phonerec);
        ////echo count($phonerec);
        $data['row'] = $phone[0];
        $data['phonerec'] = $phonerec;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['title'] = "手機修改";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['include'] = 'cases2/phone_edit';
        $this->load->view('template', $data);
    }  
    
    public function updatephone() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['p_s_ic'])->result();
        if(isset($_POST['pr_path'])){
            for($i=0, $count = count($_POST['pr_path']);$i<$count;$i++) {
                $dataSet1 = array();
                $dataSet1[] = array (
                    'pr_num' => $_POST['pr_num'][$i],
                    'pr_path' => $_POST['pr_path'][$i],
                    'pr_phone' => $_POST['pr_phone'][$i],
                    'pr_name ' => $_POST['pr_name'][$i],
                    'pr_time' => $_POST['pr_time'][$i],
                    'pr_relationship' => $_POST['pr_relationship'][$i],
                    'pr_has_drug' => $_POST['pr_has_drug'][$i],
                    'pr_user' => $_POST['pr_user'][$i],
                    'pr_seller' => $_POST['pr_seller'][$i],
                    'pr_s_ic ' => $_POST['p_s_ic'],
                    'pr_s_cnum' => $_POST['p_s_cnum'],
                    'pr_p_no' => $_POST['p_no'],
                    'pr_p_num' => $_POST['p_num'],
                );
                if(!empty($_POST['pr_num'][$i])) $this->getsqlmod->deletePhonerec($_POST['pr_num'][$i]);
                $this->getsqlmod->addphone_rec($dataSet1);
            }
        }
        //echo "<br>";
        unset($_POST['pr_num']);
        unset($_POST['pr_path']);
        unset($_POST['pr_phone']);
        unset($_POST['pr_name']);
        unset($_POST['pr_time']);
        unset($_POST['pr_relationship']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_user']);
        unset($_POST['pr_seller']);
        //var_dump($_POST);
        $this->getsqlmod->updatePhone($_POST['p_num'],$_POST);
        redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    function delphonerec(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $phonerec = $this->getsqlmod->getPhoneRec('pr_num',$id)->result();
        $this->getsqlmod->deletePhonerec($id);
        redirect('cases2/editphone/' . $phonerec[0]->pr_p_num,'refresh');
    }

    public function createsource() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['sou_s_ic'])->result();
        for($i=0, $count = count($_POST['sou_sm_name']);$i<$count;$i++) {
            $dataSet1[] = array();
            $dataSet1[$i] = array (
                'sou_sm_name' => $_POST['sou_sm_name'][$i],
                'sou_sm_ac1' => $_POST['sou_sm_ac1'][$i],
                'sou_sm_ac2 ' => $_POST['sou_sm_ac2'][$i],
                'sou_sm_ac3' => $_POST['sou_sm_ac3'][$i],
                'sou_sm_s_ic ' => $_POST['sou_s_ic'],
                'sou_sm_s_cnum' => $_POST['sou_cnum'],
                'sou_sm_phone' => $_POST['sou_phone'],
            );
        }
        //$this->getsqlmod->addsource_sm($dataSet1);
        unset($_POST['sou_sm_name']);
        unset($_POST['sou_sm_ac1']);
        unset($_POST['sou_sm_ac2']);
        unset($_POST['sou_sm_ac3']);
        if(!empty($_FILES['sourcepic']['name'])){
            $_FILES['sourcepic']['name']  = $_POST['sou_s_ic'] . $_FILES['sourcepic']['name'];
            $config['upload_path']          = 'sourcedrugpic/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('sourcepic')){
                $error = array('error' => $this->upload->display_errors());
                ////var_dump($error);
                redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['sou_pic']=$uploadData['file_name'];
                $this->getsqlmod->adddrugSou($_POST);
                $source = $this->getsqlmod->getSource('sou_s_ic',$_POST['sou_s_ic'])->result();
                for($i=0, $count1 = count($dataSet1);$i<$count1;$i++) {
                    $dataSet1[$i]['sou_sounum'] = $source[0]->sou_num;
                }
                $this->getsqlmod->addsource_sm($dataSet1);
                redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
        }
        else{
            $_POST['sou_pic']=null;
            $this->getsqlmod->adddrugSou($_POST);
            $source = $this->getsqlmod->getSource('sou_s_ic',$_POST['sou_s_ic'])->result();
            for($i=0, $count1 = count($dataSet1);$i<$count1;$i++) {
                $dataSet1[$i]['sou_sounum'] = $source[0]->sou_num;
            }
            $this->getsqlmod->addsource_sm($dataSet1);
            redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
        }
    }
    
    public function newsource() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_cnum']= $susp[0]->s_cnum;
        $data['s_num']= $susp[0]->s_num;
        $data['s_name']= $susp[0]->s_name;
        $data['title'] = "案件處理系統:溯源";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['include'] = 'cases2/new_source';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases2/new_cases' ); 
    }
    
    function editsource(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $source = $this->getsqlmod->getSource('sou_num',$id)->result();
        $sourcesm = $this->getsqlmod->getSourceSM('sou_sounum',$source[0]->sou_num)->result();
        $data['row'] = $source[0];
        $data['sourcesm'] = $sourcesm;
        $data['title'] = "修改溯源";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['include'] = 'cases2/source_edit';
        $this->load->view('template', $data);
    }  
    
    public function updatesource() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['sou_s_ic'])->result();
        if(isset($_POST['sou_sm_name'])){
            for($i=0, $count = count($_POST['sou_sm_name']);$i<$count;$i++) {
                $dataSet1 = array();
                $dataSet1[] = array (
                    'sou_sm_name' => $_POST['sou_sm_name'][$i],
                    'sou_sm_ac1' => $_POST['sou_sm_ac1'][$i],
                    'sou_sm_ac2 ' => $_POST['sou_sm_ac2'][$i],
                    'sou_sm_ac3' => $_POST['sou_sm_ac3'][$i],
                    'sou_sm_s_ic ' => $_POST['sou_s_ic'],
                    'sou_sm_s_cnum' => $_POST['sou_cnum'],
                    'sou_sm_phone' => $_POST['sou_phone'],
                    'sou_sounum' => $_POST['sou_num'],
                );
                if(!empty($_POST['sou_sm_num'][$i])) $this->getsqlmod->deleteSource($_POST['sou_sm_num'][$i]);
                $this->getsqlmod->addsource_sm($dataSet1);
            }
        }
        unset($_POST['sou_sm_num']);
        unset($_POST['sou_sm_name']);
        unset($_POST['sou_sm_ac1']);
        unset($_POST['sou_sm_ac2']);
        unset($_POST['sou_sm_ac3']);
        if(!empty($_FILES['sourcepic']['name'])){
            $_FILES['sourcepic']['name']  = $_POST['sou_s_ic'] . $_FILES['sourcepic']['name'];
            $config['upload_path']          = 'sourcedrugpic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('sourcepic')){
                $error = array('error' => $this->upload->display_errors());
                ////var_dump($error);
                redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
            else{
                //$this->getsqlmod->addsuspect($_POST);
                $uploadData = $this->upload->data();
                $_POST['sou_pic']=$uploadData['file_name'];
                $this->getsqlmod->updateSource($_POST['sou_num'],$_POST);
                redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
        }
        else{
            $_POST['sou_pic']=null;
            $this->getsqlmod->updateSource($_POST['sou_num'],$_POST);
            redirect('cases2/editsusp1/' . $susp[0]->s_num,'refresh'); 
        }
    }
    
    function delsourcesm(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $source = $this->getsqlmod->getSourceSM('sou_sm_num',$id)->result();
        $this->getsqlmod->deleteSource($id);
        ////var_dump ($source[0]);
        redirect('cases2/editsource/' . $source[0]->sou_sounum,'refresh');
    }
    function delcases(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $this->getsqlmod->deleteCaseS($id);
        ////var_dump ($source[0]);
        redirect('cases2/listCases','refresh');
    }
    function delsusp(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        // $this->getsqlmod->deleteSusp($id);
		$data = array(
			's_cnum' => NULL
		);
		$this->getsqlmod->updateSuspCnum($id, $data);
        ////var_dump ($source[0]);
        redirect('cases2/editCases2/'. $susp[0]->s_cnum ,'refresh');
    }
    function delsource(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->getSource('sou_num',$id)->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_ic',$susp[0]->sou_s_ic)->result();
        $this->getsqlmod->deleteSou($id);
        redirect('cases2/editsusp1/'. $susp1[0]->s_num ,'refresh');
    }
    function delphone(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->getPhone('p_num',$id)->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_ic',$susp[0]->p_s_ic)->result();
        $this->getsqlmod->deletePho($id,$susp[0]->p_no);
        redirect('cases2/editsusp1/'. $susp1[0]->s_num ,'refresh');
    }
    function delsocial(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->getSocial('social_media_num',$id)->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_ic',$susp[0]->s_ic)->result();
        $this->getsqlmod->deleteSoc($id);
        redirect('cases2/editsusp1/'. $susp1[0]->s_num ,'refresh');
    }
    function delcar(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->get1Car('v_num',$id)->result();
        $this->getsqlmod->deleteVec($id);
        redirect('cases2/editsusp1/'. $susp[0]->v_s_num ,'refresh');
    }
    function deldrug(){
        $id = $this->uri->segment(3);
        $id2 = $this->uri->segment(4);
        $this->load->model('getsqlmod','',TRUE);
        $this->getsqlmod->deleteDrugID($id);
		$s_cnum = $this->getsqlmod->get1Susp_ed('s_num', $id2)->result_array();
        ////var_dump ($source[0]);
        redirect('cases2/editCases2/'.$s_cnum[0]['s_cnum'],'refresh');
    }
    //二階案件
    function editCases2(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        $data['row'] = $casesrepair[0];
        //$data['sourcesm'] = $sourcesm;
        $data['title'] = "案件查詢/編輯";
        $data['cnum'] = $id;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['user'] = $this -> session -> userdata('uic');
        $data['drug_table'] = $this->table_drug2($id);
        $data['s_table'] = $this->table_susp2($id);
        $data['include'] = 'cases2/2-new_cases';
        $this->load->view('template', $data);

    }
    function updateCases2(){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
		if(!empty($_FILES['drug_doc']['name'])){
            $_FILES['drug_doc']['name']  = $_POST['c_num'] . $_FILES['drug_doc']['name'];
            $config['upload_path']          = 'drugdoc/';
            $config['allowed_types']        = 'doc|pdf|docx';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('drug_doc')){
                $error = array('error' => $this->upload->display_errors());
                //redirect('cases2/editCases2/' . $_POST['s_cnum'],'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['drug_doc']=$uploadData['file_name'];

            }
        }
		else
		{
			$_POST['drug_doc'] = null;
		}		
        $now = date('Y-m-d H:i:s');
        $_POST['e_ed_date']=$now;
        $_POST['e_ed_empno']=$this -> session -> userdata('uid');
        $link= $_POST['link'];
        unset($_POST['link']);
        $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
        redirect($link,'refresh'); 
    }

    function editdrug2(){
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
		$c_num = $this->uri->segment(4);
        $this->load->model ( 'getsqlmod' ); 
        $drugOpt = $this->getsqlmod->getDrug_option()->result();
        $drugAll = $this->getsqlmod->get1DrugAll($id)->result();
        $Susp = $this->getsqlmod->get1DrugSuspbyCnum($c_num)->result();
        $drugck = $this->getsqlmod->get1DrugCk($id)->result();
        $drugck2 = $this->getsqlmod->get2DrugCk('ddc_drug',$id)->result();
        ////echo $casesrepair[1]->c_num;
        $type_options = array(
            '毒品粉末、碎塊' => '毒品粉末、碎塊' ,
            '藥碇' => '藥碇',
            '梅碇' => '梅碇',
            '即溶包' => '即溶包',
            '香菸(含菸蒂)' => '香菸(含菸蒂)',
            '菸草' => '菸草',
            '濾嘴' => '濾嘴',
            '吸食器' => '吸食器',
            '吸管' => '吸管',
            '卡片' => '卡片',
            '括盤' => '括盤',
            '棉花棒' => '棉花棒',
            '殘渣袋' => '殘渣袋',
            '大麻' => '大麻',
            '手機殼' => '手機殼',
            '植株' => '植株',
            '種子' => '種子',
       );   
        $type_options1 = array(
            '包' => '包' ,
            '顆' => '顆',
            '支' => '支',
            '張' => '張',
            '個' => '個',
            '組' => '組',
            '株' => '株',
        );
        // $type_options2 = array(
        //     '海洛因' => '海洛因' ,
        //     '安非他命' => '安非他命',
        //     '搖頭丸(MDMA)' => '搖頭丸(MDMA)',
        //     '愷他命(KET)' => '愷他命(KET)',
        //     '一粒眠' => '一粒眠',
        //     '大麻' => '大麻',
        //     '卡西酮類' => '卡西酮類',
        //     '古柯鹼' => '古柯鹼',
        //     '無毒品反應' => '無毒品反應',
        // );
		$type_options2 = array('' => '請選擇');
        foreach ($drugOpt as $drugOpt1){
            $type_options2[$drugOpt1->drug_level .'級' .$drugOpt1->drug_name] = $drugOpt1->drug_level . '級' . $drugOpt1->drug_name;
        }

        $type_options3 = array('' => '請選擇');
        foreach ($Susp as $Susp1){
            $type_options3[$Susp1->s_num] = $Susp1->s_name;
        }
		// $type_options3['共同持有'] = '共同持有';

        $type_options4 = array('' => '請選擇');
		$type_options4['0級尿液(純吸食)'] = '尿液(純吸食)';
        foreach ($drugOpt as $drugOpt1){
            $type_options4[$drugOpt1->drug_level .'級' .$drugOpt1->drug_name] = $drugOpt1->drug_level . '級' . $drugOpt1->drug_name;
        }
        $data['drugAll'] = $drugAll[0];
        $data['drugck'] = $drugck;
        $data['drugck2'] = $drugck2;
        // var_dump($drugck);
		// exit;
        $data['nameopt'] = $type_options;
        $data['countopt'] = $type_options1;
        $data['nwopt'] = $type_options2;
        $data['test'] = $type_options3;
        $data['drug_opt'] = $type_options4;
        $data['e_id'] = $id;
        $data['e_c_num'] = $drugAll[0]->e_c_num;
        $data['title'] = "毒品修改:". $id;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = "navbar2";
        $data['user'] = $this -> session -> userdata('uic');
        $data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/drug2_edit';
        $this->load->view('template', $data);
    }  

    public function update_drug2() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
		$df_numAry = $this->input->post('df_num');
		if(null != $this->input->post('df'))
		{
			foreach ($this->input->post('df') as $key => $value) {
				$dataSet1 = array();
				// switch ($value) {
				//     case '海洛因':
				//         $dataSet1[] = array (
				// 				"df_type" => '初驗',
				//               'df_level' => '1',
				//               'df_ingredient' => $value,
				//               'df_drug' => $_POST['e_id']
				//          );
				//         break;
				//     case '安非他命':
				//         $dataSet1[] = array (
				// 				"df_type" => '初驗',
				//               'df_level' => '2',
				//               'df_ingredient' => $value,
				//               'df_drug' => $_POST['e_id']
				//          );
				//         break;
				//     case '搖頭丸(MDMA)':
				//         $dataSet1[] = array (
				// 			"df_type" => '初驗',
				//               'df_level' => '2',
				//               'df_ingredient' => $value,
				//               'df_drug' => $_POST['e_id']
				//          );
				//         break;
				//     case '愷他命(KET)':
				//         $dataSet1[] = array (
				// 			"df_type" => '初驗',
				//               'df_level' => '3',
				//               'df_ingredient' => $value,
				//               'df_drug' => $_POST['e_id']
				//          );
				//         break;
				//     case '一粒眠':
				//         $dataSet1[] = array (
				// 			"df_type" => '初驗',
				//               'df_level' => '3',
				//               'df_ingredient' => $value,
				//               'df_drug' => $_POST['e_id']
				//          );
				//         break;
				//     case '大麻':
				//         $dataSet1[] = array (
				// 			"df_type" => '初驗',
				//               'df_level' => '2',
				//               'df_ingredient' => $value,
				//               'df_drug' => $_POST['e_id']
				//          );
				//         break;
				//     case '卡西酮類':
				//         $dataSet1[] = array (
				// 			"df_type" => '初驗',
				//               'df_level' => '3',
				//               'df_ingredient' => $value,
				//               'df_drug' => $_POST['e_id']
				//          );
				//         break;
				//     case '古柯鹼':
				//         $dataSet1[] = array (
				// 			"df_type" => '初驗',
				//               'df_level' => '2',
				//               'df_ingredient' => $value,
				//               'df_drug' => $_POST['e_id']
				//          );
				//         break;
				//     default:
				//         //echo " ";
				//         break;
				// }
				$dfAry = explode('級', $value);
				$dataSet1[] = array (
					"df_type" => '初驗',
					  'df_level' => $dfAry[0],
					  'df_ingredient' => $dfAry[1],
					  'df_drug' => $_POST['e_id']
				 );
				//echo '<br>';
				$this->getsqlmod->deleteDrugckc('df_num', $df_numAry[$key]);
				// $this->getsqlmod->deleteDrugCkfor_Ed($_POST['e_id'],$value);
				$this->getsqlmod->adddrug1($dataSet1);
				////var_dump($dataSet1);
			}
		}
        
        if(!empty($_FILES['e_doc']['name'])){
            $_FILES['e_doc']['name']  = $_POST['e_id'] . $_FILES['e_doc']['name'];
            $config['upload_path']          = 'drugdoc/';
            $config['allowed_types']        = 'doc|pdf|docx';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('e_doc')){
                $error = array('error' => $this->upload->display_errors());
                //redirect('cases2/editCases2/' . $_POST['s_cnum'],'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['e_doc']=$uploadData['file_name'];
				$susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['e_s_ic'])->result();
				$_POST['e_suspect']=$susp[0]->s_name;

            }
        }
		if($_POST["link"]=="2"){
			$df1 = array (
				"df_type" => '初驗',
				  'df_drug' => $_POST['e_id']
			 );
			$this->getsqlmod->adddrugInd($df1);
		}
        if($_POST["link"]=="1"){
			$df2 = array (
				"df_type" => '複驗',
				'df_drug' => $_POST['e_id']
			 );
			$this->getsqlmod->adddrugInd($df2);
		}else{       
			if(isset($_POST['df_num2'])) 
			{
				$df2_numAry = $_POST['df_num2'];    
				foreach ($_POST['df2'] as $df2key => $df2value) {
					if(!empty($df2value))
					{
						$df2Ary = explode('級', $df2value);
						$updateAry = array(
							"df_type" => '複驗',
							"df_drug" => $_POST['e_id'],
							'df_level' => $df2Ary[0],
							  'df_ingredient' => $df2Ary[1],
							// 'df_PNW' => $_POST['df_PNW'][$df2key],
							// 'df_NW' => $_POST['df_NW'][$df2key],
							// 'df_PNWS' => $_POST['df_PNWS'][$df2key]
						);
		
						$this->getsqlmod->deleteDrugckc('df_num', $df2_numAry[$df2key]);
						$this->getsqlmod->adddrugInd($updateAry);
					}
					
				}
			}
			
		}
		$_POST['e_togethe'] = ((isset($_POST['together']))?implode(',', $_POST['together']):null);
		$link = $_POST['link'];
		unset($_POST['df']);
		unset($_POST['df2']);
		unset($_POST['df_num']);
		unset($_POST['df_num2']);
		unset($_POST['df_PNW']);
		unset($_POST['df_NW']);
		unset($_POST['df_PNWS']);
		unset($_POST['link']);
		unset($_POST['together']);
		$this->getsqlmod->updateDrug1($_POST['e_id'],$_POST);

		$searchdata = $this->getsqlmod->get1Drug_id($_POST['e_id'])->result_array();
		$searchdata1 = $this->getsqlmod->get1Susp_ed('s_num', $searchdata[0]['e_c_num'])->result_array();
		if($link == 3 || $link == '3')
		{
			
			redirect('cases2/editdrug2/'.$_POST['e_id'].'/'.$searchdata1[0]['s_cnum'],'refresh');
		}
		else
		{
			redirect('cases2/editdrug2/' . $_POST['e_id'].'/'.$searchdata1[0]['s_cnum'],'refresh');
		}
		 
    }
    
    function deleteCk2(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $drugck = $this->getsqlmod->get2DrugCk('ddc_num',$id)->result();
        $this->getsqlmod->deleteDrugCk2($id);
        ////var_dump ($drugck);
        redirect('cases2/editdrug2/' . $drugck[0]->ddc_drug,'refresh');
    }
    function deleteSuspCk(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $sc = $this->getsqlmod->get2SuspCk('sc_num',$id)->result();
        $this->getsqlmod->deletesuspCk2($id);
        ////var_dump ($drugck);
        redirect('cases2/editsusp2/' . $sc[0]->sc_snum,'refresh');
    }
    function editsusp2(){
        $this->load->library('table');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        $susp2 = $this->getsqlmod->get2SuspCk('sc_snum',$id)->result();
        $drugOpt = $this->getsqlmod->getDrug_option()->result();
        $type_options1 = array('' => '請選擇');
        foreach ($drugOpt as $drugOpt1){
            $type_options1[$drugOpt1->drug_level .'級' .$drugOpt1->drug_name] = $drugOpt1->drug_level . '級' . $drugOpt1->drug_name;
        }
        $type_options2 = array(
            '待裁罰' => '待裁罰' ,
            '移送地檢' => '移送地檢',
        );
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['row'] = $susp[0];
        $data['opt'] = $type_options1;
        $data['opt2'] = $type_options2;
        $data['susp2'] = $susp2;
        $data['s_cnum'] = $id;
        $data['title'] = "犯嫌人修改:" .$susp[0]->s_name;
        $data['user'] = $this -> session -> userdata('uic');
		$data['error'] = (isset($_GET['error']))?"需上傳尿報以及填寫檢驗成分，否則將不予儲存。":'';
        $data['include'] = 'cases2/edit_suspect2';
        $this->load->view('template', $data);
    }  
    function testpostarray(){
        var_dump($_POST);
    }
    function updateSusp2(){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
		// var_dump($_POST);
		// exit;
        if(!empty($_FILES['s_utest_doc']['name'])){
            $_FILES['s_utest_doc']['name']  = $_POST['s_ic'] . $_FILES['s_utest_doc']['name'];
            $config['upload_path']          = 'utest/';
            $config['allowed_types']        = 'doc|pdf|docx';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('s_utest_doc')){
                $error = array('error' => $this->upload->display_errors());
                //redirect('cases2/editCases2/' . $_POST['s_cnum'],'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['s_utest_doc']=$uploadData['file_name'];
                if(!isset($_POST['sc'])) $_POST['sc']=array();
                for($i=0, $count = count($_POST['sc']);$i<$count;$i++) {
                    $data = array();
                    $sc = explode("級",$_POST['sc'][$i]);
                    if(!isset($sc[1])){
                        $data = array(
                            'sc_num' => $_POST['sc_num'][$i],
                            'sc_level' => $sc[0],
                            'sc_ingredient' => $sc[1],
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['s_cnum'],
                            );
                    }else{
                        $data = array(
                            'sc_num' => $_POST['sc_num'][$i],
                            'sc_level' => $sc[0],
                            'sc_ingredient' => $sc[1],
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['s_cnum'],
                            );
                    }
                    $this->getsqlmod->updatesc($data);
                }
                if($_POST["link"]=="1"){
                    if(count($_POST['sc'])>0){
                        for($i=0, $count = count($_POST['sc']);$i<$count;$i++) {
                            $sc = explode("級",$_POST['sc'][$i]);
                            $data = array(
                                'sc_level' => $sc[0],
                                'sc_ingredient' => $sc[1],
                                'sc_snum' => $_POST['s_num'],
                                'sc_cnum' => $_POST['s_cnum'],
                            );
                        }
                    }else{
                        $data = array(
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['s_cnum'],
                        );
                    }
                    $this->getsqlmod->addsc($data);
					unset($_POST['s_sac_state']);
                    unset($_POST['sc']);
                    unset($_POST['sc_num']);
                    unset($_POST['link']);
                    ////var_dump($_POST);
                    $this->getsqlmod->updateSusp($_POST['s_num'],$_POST);
                    redirect('cases2/editsusp2/' . $_POST['s_num'],'refresh'); 
                }else if(isset($_POST['s_utest_doc']) && !empty($_POST['s_utest_doc']) && count($_POST['sc'])>0){
					unset($_POST['s_sac_state']);
                    unset($_POST['sc']);
                    unset($_POST['sc_num']);
                    unset($_POST['link']);
                    ////var_dump($_POST);
                    $this->getsqlmod->updateSusp($_POST['s_num'],$_POST);
					$caseid = $this->getsqlmod->get1Susp_ed('s_num', $_POST['s_num'])->result_array()[0]['s_cnum'];
                    redirect('cases2/editsusp2/' . $_POST['s_num'],'refresh'); 
                }else{
					redirect('cases2/editsusp2/' . $_POST['s_num'].'?error=true','refresh'); 
				}
            }
        }
        else{
            if(!isset($_POST['sc'])) $_POST['sc']=array();
            for($i=0, $count = count($_POST['sc']);$i<$count;$i++) {
                $data = array();
                $sc = explode("級",$_POST['sc'][$i]);
                if(!isset($sc[1])){
                    $data = array(
                        'sc_num' => $_POST['sc_num'][$i],
                        'sc_level' => $sc[0],
                        'sc_ingredient' => $sc[1],
                        'sc_snum' => $_POST['s_num'],
                        'sc_cnum' => $_POST['s_cnum'],
                        );
                }else{
                    $data = array(
                        'sc_num' => $_POST['sc_num'][$i],
                        'sc_level' => $sc[0],
                        'sc_ingredient' => $sc[1],
                        'sc_snum' => $_POST['s_num'],
                        'sc_cnum' => $_POST['s_cnum'],
                        );
                }
                $this->getsqlmod->updatesc($data);
            }
			
            if($_POST["link"]=="1"){
                if(count($_POST['sc'])>0){
                    for($i=0, $count = count($_POST['sc']);$i<$count;$i++) {
                        $sc = explode("級",$_POST['sc'][$i]);
                        $data = array(
                            'sc_level' => $sc[0],
                            'sc_ingredient' => $sc[1],
                            'sc_snum' => $_POST['s_num'],
                            'sc_cnum' => $_POST['s_cnum'],
                        );
                    }
                }else{
                    $data = array(
                        'sc_snum' => $_POST['s_num'],
                        'sc_cnum' => $_POST['s_cnum'],
                    );
                }
                $this->getsqlmod->addsc($data);
				unset($_POST['s_sac_state']);
                unset($_POST['sc']);
                unset($_POST['sc_num']);
                unset($_POST['link']);
                ////var_dump($_POST);
                $this->getsqlmod->updateSusp($_POST['s_num'],$_POST);
				
                redirect('cases2/editsusp2/' . $_POST['s_num'],'refresh'); 
            }
			else if(isset($_POST['s_utest_doc']) && !empty($_POST['s_utest_doc']) && count($_POST['sc'])>0){
				unset($_POST['s_sac_state']);
                unset($_POST['sc']);
                unset($_POST['sc_num']);
                unset($_POST['link']);
                ////var_dump($_POST);
                $this->getsqlmod->updateSusp($_POST['s_num'],$_POST);
				$caseid = $this->getsqlmod->get1Susp_ed('s_num', $_POST['s_num'])->result_array()[0]['s_cnum'];
                redirect('cases2/editsusp2/' .  $_POST['s_num'],'refresh'); 
            }else{
				redirect('cases2/editsusp2/' . $_POST['s_num'].'?error=true','refresh'); 
			}
        }
    }
    public function createdrug2_num() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $Susp = $this->getsqlmod->get1DrugSusp($id)->result();
        $query = $this->getsqlmod->get1DrugCount()->result(); 
        $countid = $query[0]->e_id;
        $lastNum = (int)mb_substr($countid, -5, 5);
        $lastNum++;
        $value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $_POST['e_id']='EN' . $taiwan_date . $value;
        // $_POST['e_c_num']=$id;
        $_POST['e_empno']=$this->session->userdata('uname');
        $_POST['e_ed_empno']=$this->session->userdata('uname');
       // //var_dump($_POST);
        $this->getsqlmod->adddrug($_POST);
        redirect('cases2/editdrug2/' . $_POST['e_id'].'/'.$id,'refresh'); 
    }

    public function createsusp2_num() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $c_value = substr($id,-3);
        $_POST['s_cnum']=$id;
        $this->getsqlmod->addsuspect($_POST);
        $num=$this->getsqlmod->get1Susp_ed('s_cnum',$id)->result();
        //echo $num[0]->s_num;
        redirect('cases2/editsusp2/' . $num[0]->s_num,'refresh'); 
    }

    //3階裁罰
    public function list3Cases() {
        $this->load->helper('form');
        if($this -> session -> userdata('uoffice')== '刑事警察大隊') $test_table = $this->table_cases3($this->session->userdata('uoffice').$this->session->userdata('uroffice'));
        else $test_table = $this->table_cases3($this->session->userdata('uoffice'));
        $data['s_table'] = $test_table;
        $data['title'] = "案件處理系統:待裁罰列表";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases2/3caseslist';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    public function editsp() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        $susp2 = $this->getsqlmod->get2SuspCk('sc_snum',$id)->result();
        ////var_dump($susp2[0]);
        $drug = $this->getsqlmod->get1Drug($susp[0]->s_cnum)->result();
        $drug1 = $this->getsqlmod->get3Drugfirst($id)->result(); // 抓sic
        $cases = $this->getsqlmod->getCases($susp[0]->s_cnum)->result();
        $type_options = array(
            '確認送出裁罰' => '確認送出裁罰' ,
            '免裁罰' => '免裁罰',
			'移送法院' => '移送法院',
            '待裁罰' => '待裁罰',
        );
        $data['susp'] = $susp[0];
        $data['susp2'] = $susp2;
        $data['drug'] = $drug[0];
        $data['drug1'] = $drug1;
        $data['cases'] = $cases[0];
        if(isset($sp[0]))$data['sp'] = $sp[0];
        $data['opt'] = $type_options;
        ////var_dump($data['sp']);
        $data['title'] = "三階裁罰修改模式";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases2/3casesedit';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    
    function updateSusp3(){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' );
        $cases = array(
            's_date' => $_POST['s_date'],
        );        
        if(!empty($_FILES['sp_doc']['name'])){
            $_FILES['sp_doc']['name']  = $_POST['s_go_no'] . $_FILES['sp_doc']['name'];
            $config['upload_path']          = 'drugdoc/';
            $config['allowed_types']        = 'doc|pdf|docx';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('sp_doc')){
                $error = array('error' => $this->upload->display_errors());
                //redirect('cases2/editCases2/' . $_POST['s_cnum'],'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['sp_doc']=$uploadData['file_name'];
                $susp = array(
                    's_name' => $_POST['s_name'],
                    's_ic'  => $_POST['s_ic'],
                    // 's_dpcounty'  => $_POST['s_dpcounty'],
                    // 's_dpdistrict'  => $_POST['s_dpdistrict'],
                    // 's_dpzipcode'  => $_POST['s_dpzipcode'],
                    's_dpaddress'  => $_POST['s_dpaddress'],
                    // 's_rpcounty'  => $_POST['s_rpcounty'],
                    // 's_rpdistrict'  => $_POST['s_rpdistrict'],
                    // 's_rpzipcode'  => $_POST['s_rpzipcode'],
                    's_rpaddress'  => $_POST['s_rpaddress'],
                    's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
                    's_utest_num'  => $_POST['s_utest_num'],
                    's_go_no'  => $_POST['s_go_no'],
                    'sp_doc' => $_POST['sp_doc'],
                    's_sac_state'  => $_POST['s_sac_state']
                );                    
                ////var_dump($sp['s_num']);        
                $this->getsqlmod->updateSusp($_POST['s_num'],$susp);        
                $this->getsqlmod->updateCase($_POST['c_num'],$cases);       
				// exit; 
                redirect($_POST['link'],'refresh'); 
            }
        }
        else{
                $susp = array(
                    's_name' => $_POST['s_name'],
                    's_ic'  => $_POST['s_ic'],
                    // 's_dpcounty'  => $_POST['s_dpcounty'],
                    // 's_dpdistrict'  => $_POST['s_dpdistrict'],
                    // 's_dpzipcode'  => $_POST['s_dpzipcode'],
                    's_dpaddress'  => $_POST['s_dpaddress'],
                    // 's_rpcounty'  => $_POST['s_rpcounty'],
                    // 's_rpdistrict'  => $_POST['s_rpdistrict'],
                    // 's_rpzipcode'  => $_POST['s_rpzipcode'],
                    's_rpaddress'  => $_POST['s_rpaddress'],
                    's_birth'  =>  $this->tranfer2ADyear($_POST['s_birth']),
                    's_utest_num'  => $_POST['s_utest_num'],
                    's_go_no'  => $_POST['s_go_no'],
                    // 'sp_doc' => $_POST['sp_doc'],
                    's_sac_state'  => $_POST['s_sac_state']
                );                    
            $this->getsqlmod->updateSusp($_POST['s_num'],$susp);        
            $this->getsqlmod->updateCase($_POST['c_num'],$cases);  
			// exit;
            redirect($_POST['link'],'refresh'); 
        }
    }

    public function update3cases() {
        $this->load->helper('form');
        $data['title'] = "案件處理系統:待裁罰列表";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['include'] = 'cases2/3caseslist';
        $data['nav'] = 'navbar2';
        ////var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        ////var_dump($s_cnum);
        if($_POST['s_status']=='1'){ // 送出裁罰
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_sac_state' => '確認送出裁罰',
                );     
                // $data1 = array(//確認送裁罰的狀態
                //        'e_state' => '',
                //        'e_place' => $office,
                // );
                //$this->getsqlmod->updateDrug1($value,$data);
                $this->getsqlmod->updateSusp($value,$data);
            }
            redirect('cases2/list3Cases/'); 

        }
        else if($_POST['s_status']=='0'){ // 免罰
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_sac_state' => '免裁罰',
                );
                $this->getsqlmod->updateSusp($value,$data);
                //redirect('cases2/list3Cases/'); 
            }
            redirect('cases2/list3Cases/'); 
        }
		else if($_POST['s_status']=='3'){ // 移送(法院)裁處
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_sac_state' => '移送法院',
                );
                $this->getsqlmod->updateSusp($value,$data);
                //redirect('cases2/list3Cases/'); 
            }
            redirect('cases2/list3Cases/'); 
        }
		else if($_POST['s_status']=='2'){ // 退回
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_sac_state' => null,
                );
                $this->getsqlmod->updateSusp($value,$data);
				$caseid = $this->getsqlmod->getsuspect('s_num', $value)->result_array()[0]['s_cnum'];
				$updatecase = array(
					'c_status' => null
				);
				$this->getsqlmod->updateCase($caseid, $updatecase);
				$updatedrug = array(
					'e_state' => '單位保管中'
				);
				$this->getsqlmod->updateDrug('e_c_num',$value, $updatedrug);
                //redirect('cases2/list3Cases/'); 
            }
            redirect('cases2/list3Cases/'); 
        }
        else{
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_sac_state' => '待裁罰',
                );
                $this->getsqlmod->updateSusp($value,$data);
                //redirect('cases2/list3Cases/'); 
            }
            redirect('cases2/list3Cases/'); 
        }
    }
    public function list3CasesCX() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table_cases3Other($table);
        $data['s_table'] = $test_table;
        $data['title'] = "案件處理系統:查詢免罰與法院案件";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases2/3caseslist_other';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    
    public function list3Rewardlist() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        if($this -> session -> userdata('IsAdmin')=="1") $rp=$this->getsqlmod->getrewardproject($table)->result();
        else $rp=$this->getsqlmod->getrewardproject_normal2($table)->result();
        $test_table = $this->table_reward1($table);
        $type_options = array();
        foreach ($rp as $rp1){
            $type_options[$rp1->rp_name] = $rp1->rp_name;
        }
        $data['s_table'] = $test_table;
        $data['title'] = "獎金申請";
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        if($this -> session -> userdata('IsAdmin')=="1") {
            $data['include'] = 'cases2/3rewardlist';
        } 
        else{
            $data['include'] = 'cases2/3rewardlist_na';
        }
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    
    public function list3RewardProject() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject($table)->result();
        $test_table = $this->table_rewardProject1($table);
        $data['s_table'] = $test_table;
        $data['title'] = "獎金專案";
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases2/3rewardproject';
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    
    public function listrp1() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $rp = $this->getsqlmod->getRP1($id)->result();
        $test_table = $this->table_project_reward($table,$id);
        $data['s_table'] = $test_table;
        $data['title'] = "獎金專案:".$rp[0]->rp_name;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases2/3RPlist';
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    
    public function listrp2() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $rp = $this->getsqlmod->getRP2($id)->result();
        $test_table = $this->table_project_reward2($table,$id);
        $data['s_table'] = $test_table;
        $data['title'] = "獎金專案:".$rp[0]->rp_name;
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases2/3RPlist';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    
    public function addRewardProject_na(){
        ////var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $soffice = $this -> session -> userdata('uroffice');
        $roffice = $this -> session -> userdata('uoffice');
        $data['include'] = 'cases/3caseslist';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        if(isset($IsAdmin)&&$IsAdmin=="2") $data['nav'] = 'navbar1_ad2';
        else if(isset($IsAdmin)&&$IsAdmin=="1") $data['nav'] = 'navbar1_ad';
        else $data['nav'] = 'navbar1';
        $taiwan_date = date('Y')-1911; 
         date_default_timezone_set("Asia/Taipei");
        $now = date('mdHi'); 
        $rp_num = $taiwan_date.$now;
        ////var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_reward_project' => $rp_num,
                    's_reward_fine' => $_POST['reward'][$value],
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            $data1 = array(
                    'rp_name' => $rp_num,
                    'rp_roffice' => $roffice,
                    'rp_soffice' => $soffice,
                    'rp_empno' => $user,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addrp1($data1);
            redirect('cases2/list3Rewardlist/'); 
        }
        else if($_POST['s_status']=='0'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_reward_project' => $_POST['rp_num'],
                    's_reward_fine' => $_POST['reward'][$value],
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            redirect('cases2/list3Rewardlist/'); 
        }
        
    }
    
    public function addRewardProject(){
        ////var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uname');
        $office = $this -> session -> userdata('uoffice');
        $taiwan_date = date('Y')-1911; 
         date_default_timezone_set("Asia/Taipei");
        $now = date('mdHi'); 
        $rp_num = $taiwan_date.$now;
        ////var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_reward_project2' => $rp_num,
                    's_reward_fine' => $_POST['reward'][$value],
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            $data1 = array(
                    'rp_name' => $rp_num,
                    'rp_roffice' => $office,
                    'rp_empno' => $user,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addrp2($data1);
            redirect('cases2/list3Rewardlist/'); 
        }
        else if($_POST['s_status']=='0'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    's_reward_project2' => $_POST['rp_num'],
                    's_reward_fine' => $_POST['reward'][$value],
                );
                $this->getsqlmod->updateSusp($value,$data);
            }
            redirect('cases2/list3Rewardlist/'); 
        }
    }
    
    public function addRewardProject2(){
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $query = $this->getsqlmod->getrewardproject3ALl();
        $countid = $query->num_rows();
        $countid++;
        $value = str_pad($countid,2,'0',STR_PAD_LEFT);
        $now = date('md'); 
        $now2 = date('Y-m-d');
        $taiwan_date = date('Y')-1911; 
        $rp_num = $taiwan_date.$now.$value;
        //echo $rp_num;
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value1) {
                $rp_project=$this->getsqlmod->getRP1_num($value1)->result();
                $data = array(
                    's_reward_project2' => $rp_num,
                );
                $rp1 = array(
                        'rp_status' => '警局待接收',
                        'rp_project_num' => $rp_num,
                );
                $this->getsqlmod->updateRP1($rp_project[0]->rp_name,$rp1);
                $this->getsqlmod->updateSuspReward($rp_project[0]->rp_name,$data);
            }
            $data1 = array(
                    'rp_name' => $rp_num,
                    'rp_roffice' => $office,
                    'rp_empno' => $user,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addrp2($data1);
            //var_dump($_POST);
            redirect('cases2/list3RewardProject/'); 
        }
        else if($_POST['s_status']=='0'){
            foreach ($s_cnum as $key => $value) {
                $rp_project=$this->getsqlmod->getRP1_num($value)->result();
                $data = array(
                    's_reward_project2' => $_POST['rp_num'],
                );
                $rp1 = array(
                        'rp_status' => '警局待接收',
                        'rp_project_num' =>$_POST['rp_num'],
                );
                $this->getsqlmod->updateSuspReward($rp_project[0]->rp_name,$data);
                $this->getsqlmod->updateRP1($rp_project[0]->rp_name,$rp1);
            }
                //var_dump($_POST);
            redirect('cases2/list3RewardProject/'); 
        }
    }

    public function editRewardProject(){
        ////var_dump($_POST);
        $this->load->helper('form');
        $data['title'] = "案件處理系統:待裁罰列表";
        $user = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $office = $this -> session -> userdata('uoffice');
        $data['include'] = 'cases2/3caseslist';
        $data['nav'] = 'navbar2';
        $taiwan_date = date('Y')-1911; 
        $now = date('mdGi'); 
        $rp_num = $taiwan_date.$now;
        ////var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        foreach ($s_cnum1 as $key => $value) {
            $data = array(
                's_reward_fine' => $_POST['reward'][$value],
            );
            $this->getsqlmod->updateSusp($value,$data);
        };
        redirect('cases2/list3RewardProject/'); 
    }

    public function editevistatus(){
        ////var_dump($_POST);
        $this->load->helper('form');
        $data['title'] = "案件處理系統:待裁罰列表";
        $user = $this -> session -> userdata('uic');
        $username = $this -> session -> userdata('uname');
        $office = $this -> session -> userdata('uoffice');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['include'] = 'cases2/3caseslist';
        $data['nav'] = 'navbar2';
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $enum =mb_split(",",$_POST['enum']);
        ////var_dump($enum);
        foreach ($enum as $key => $value) {
        if(isset($_POST['e_state'][$value])){
            if(isset($_POST['e_place'][$value])){
                if($_POST['e_state'][$value]=="待銷毀"){
                    $data = array(
                       'e_state' => $_POST['e_state'][$value],
                       'e_place' => $office,
                        );
                    $data2 = array(
                       'drug_rec_eid' => $value,
                       'drug_rec_empno' => $username,
                       'drug_rec_estatus' => $_POST['e_state'][$value],
                       'drug_rec_eplace' => $office,
                       'drug_rec_office' => $office,
                    );
                }
                else{
                    $data = array(
                       'e_state' => $_POST['e_state'][$value],
                       'e_place' => $_POST['e_place'][$value],
                        );
                    $data2 = array(
                       'drug_rec_eid' => $value,
                       'drug_rec_empno' => $username,
                       'drug_rec_estatus' => $_POST['e_state'][$value],
                       'drug_rec_eplace' => $_POST['e_place'][$value],
                        'drug_rec_office' => $office,
                        );
                }
            }
            else{
                $data = array(
                   'e_state' => $_POST['e_state'][$value],
                   'e_place' => $office,
                    );
                $data2 = array(
                   'drug_rec_eid' => $value,
                   'drug_rec_empno' => $username,
                   'drug_rec_estatus' => $_POST['e_state'][$value],
                   'drug_rec_eplace' => $office,
                   'drug_rec_office' => $office,
                    );
            }
            ////var_dump($_POST);
            $this->getsqlmod->updateDrug1($value,$data);
            $this->getsqlmod->adddrugrec($data2);
        }
        };
        redirect('cases2/listevi/'); 
    }

    public function listevi() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        // $test_table = $this->table_evi($table);
		// 單位保管中 送驗中 送地檢 送警局 警局已沒入
		$keep_table = $this->table_evi($table, 'keep');
		$process_table = $this->table_evi($table, 'process');
		$check_table = $this->table_evi($table, 'check');
		$inlocal_table = $this->table_evi($table, 'inlocal');
		$indrug_table = $this->table_evi($table, 'indrug');
		$receive_table = $this->table_evi($table, 'receive');
        // $data['s_table'] = $test_table;
		$data['keep_table'] = $keep_table;
		$data['process_table'] = $process_table;
		$data['check_table'] = $check_table;
		$data['inlocal_table'] = $inlocal_table;
		$data['indrug_table'] = $indrug_table;
		$data['receive_table'] = $receive_table;
        $data['title'] = "證物出入庫";
        $data['user'] = $this -> session -> userdata('uic');
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['include'] = 'cases2/3evilist';
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    public function listevirec() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $test_table = $this->table_evi_rec($table);
        $data['s_table'] = $test_table;
        $data['title'] = "出入庫日誌記錄";
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'cases2/3evilistrec';
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    
    function editCases2_Reward(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        $data['row'] = $casesrepair[0];
        //$data['sourcesm'] = $sourcesm;
        $data['title'] = "案件查詢/編輯";
        $data['cnum'] = $id;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['user'] = $this -> session -> userdata('uic');
        $data['drug_table'] = $this->table_drug2($id);
        $data['s_table'] = $this->table_susp2($id);
        $data['nav'] = 'navbar2';
        $data['include'] = 'cases2/edit_reward_cases';
        $this->load->view('template', $data);
    }
   
    function table_rewardProject3($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getrewardproject_table2($id); 
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號', '申請派出所','申請時間','申請狀態');
        $table_row = array();
        foreach ($query->result() as $rp)
        {
            $table_row = NULL;
            $table_row[] = $rp->rp_num;
            $table_row[] = anchor('cases2/listrp1/' . $rp->rp_name, $rp->rp_name);
            $table_row[] = $rp->rp_soffice;
            $table_row[] = $this->tranfer2RCyearTrad2($rp->rp_datetime);
            if($rp->rp_status==null){
                $table_row[] = '待審核';
            }
            else{
                $table_row[] = $rp->rp_status;
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
   
    public function listRewardProject() { // 一階員警整合的專案
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $rp=$this->getsqlmod->getrewardproject($table)->result();
        $test_table = $this->table_rewardProject3($table);
        $data['s_table'] = $test_table;
        $type_options = array();
        foreach ($rp as $rp1){
            $type_options[$rp1->rp_name] = $rp1->rp_name;
        }
        if($this -> session -> userdata('IsAdmin')=="1") {
            $data['include'] = 'cases2/2rewardproject';
        } 
        else{
            $data['include'] = 'cases2/2rewardproject_na';
        }
        $data['title'] = "一階員警上傳專案";
        $data['opt'] = $type_options;
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $data['user'] = $this -> session -> userdata('uic');
        $data['nav'] = 'navbar2';
        $this->load->view('template', $data);
    }
    
    public function exportDrugCSV(){
        $this->load->dbutil();
        $id = $this->session-> userdata('uoffice');
        $this->load->helper('download');
        $filename = '毒品證物'.date('Ymd').'.csv';
        $query = $this->getsqlmod->getdrugRecdownload($id); 
        $data = $this->dbutil->csv_from_result($query);
        force_download($filename, "\xEF\xBB\xBF" . $data);
    }
    
    public function rewardstate() {//查詢獎金狀態
        $this->load->helper('form');
        $this->load->library('table');
        $this->load->model('getsqlmod');
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '案件編號', '犯嫌人姓名', '身份證','查獲時間','獎金金額','申請狀態');
        $data['s_table'] = $this->table->generate();
        $data['title'] = "查詢獎金狀態";
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases2/rewardstate';
        $IsAdmin = $this -> session -> userdata('IsAdmin');
        $data['nav'] = 'navbar2';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $this->load->view('template', $data);
    }
    
    function rewardjson() { //獎金狀態json
        include 'config.php';

        $query = $this -> session -> userdata('uoffice');
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (s_ic like '%".$searchValue."%' or 
                s_name like '%".$searchValue."%' or 
                s_reward_status like '%".$searchValue."%' or 
                s_reward_fine like '%".$searchValue."%' or 
                s_date like '%".$searchValue."%' or 
                s_cnum like'%".$searchValue."%' ) ";
        }

        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        $sel = mysqli_query($con,"select count(*) as allcount from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum AND r_office = '".$query."'");//.$searchQuery);
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from cases JOIN suspect WHERE cases.c_num = suspect.s_cnum AND r_office = '".$query."'".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            $data[] = array(
                    "s_ic"=>$row['s_ic'],
                    "s_name"=>$row['s_name'],
                    "s_reward_status"=>$row['s_reward_status'],
                    "s_reward_fine"=>$row['s_reward_fine'],
                    "s_date"=> $this->tranfer2RCyearTrad($row['s_date']),
                    "s_cnum"=>$row['s_cnum'],
                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }
	public function importdata(){
        $array = json_decode($this->getsqlmod->getSuspJson(), true);
		$_source = $array['hits']['hits'];
		// echo json_encode($_source);
		// exit;
		try {
			for ($index=0; $index < count($_source); $index++) { 
				// $query = $this->getsqlmod->getdata(1,1)->result();
				// $countid = $query[0]->c_num;
				// $lastNum = (int)mb_substr($countid, -4, 4);
				// $lastNum++;
				// $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
				// $taiwan_date = date('Y')-1911; 
				// $CID = 'CN' . $taiwan_date.$value;
				if($this->getsqlmod->checkSuspexist($_source[$index]['_source']['upid'])->num_rows() == 0)
				{
					// $case = array(
					// 	'c_num' => $CID,
					// 	's_office' => $_source[$index]['_source']['sendDept']
					// );
					// $this->getsqlmod->addcases($case);
					$data = array(
						's_ic' => $_source[$index]['_source']['upid'],
						's_gender' => ((substr($_source[$index]['_source']['upid'], 1, 1) == '1')?'男':'女'),
						's_name' => $_source[$index]['_source']['uname'],
						's_nname' => $_source[$index]['_source']['nname'],
						's_feature' => $_source[$index]['_source']['feature'],
						's_birth' => (($_source[$index]['_source']['birthdayText'])?str_replace('/', '-', $_source[$index]['_source']['birthdayText']):null),
						// 's_birth' => (($_source[$index]['_source']['birthday'])?str_replace('/', '-', $_source[$index]['_source']['birthday']):null),
						's_edu' => $_source[$index]['_source']['education'],
						's_dpcounty' => $_source[$index]['_source']['houseCityShip'],
						's_dpdistrict' => $_source[$index]['_source']['houseZoneShip'],
						's_dpaddress' => $_source[$index]['_source']['houseAddrShip'],
						's_dpzipcode' => $_source[$index]['_source']['houseZipShip'],
						's_rpcounty' => $_source[$index]['_source']['presentCityShip'],
						's_rpdistrict' => $_source[$index]['_source']['presentZoneShip'],
						's_rpaddress' => $_source[$index]['_source']['presentAddrShip'],
						's_rpzipcode' => $_source[$index]['_source']['presentZipShip'],
						's_rpzipcode' => $_source[$index]['_source']['presentZipShip'],
						// 's_gang' => $_source[$index]['_source']['partyName'],
						's_CMethods' => $_source[$index]['_source']['crimeType'],
						// 's_resource' => $_source[$index]['_source']['notraceability'],
						// 's_noresource' => $_source[$index]['_source']['notraceability']['notraceability'],
						// 's_cnum' => $CID,
						's_roffice' => $_source[$index]['_source']['sendDept']
						// 's_U_Col' => (($_source[$index]['_source']['focus'] == '0')?'否':'是'),
					);
					$insert_id = $this->getsqlmod->addsuspect_lastid($data);
					for ($pindex=0; $pindex < count($_source[$index]['_source']['phones']); $pindex++) { 
						$phone = array(
							'p_no' => $_source[$index]['_source']['phones'][$pindex]['phone'],
							'p_oppw' => $_source[$index]['_source']['phones'][$pindex]['phonePw'],
							'p_imei' => $_source[$index]['_source']['phones'][$pindex]['phoneNo'],
							'p_conf' => (($_source[$index]['_source']['phones'][$pindex]['isImport'] == '0')?'否':'是'),
							'p_s_ic' => $_source[$index]['_source']['upid'],
							'p_s_num' => $insert_id,
							// 'p_s_cnum' => $CID
						);
						$this->getsqlmod->addphone($phone);
					}
					for ($prindex=0; $prindex < count($_source[$index]['_source']['phoneRecords']); $prindex++) { 
						$phone_rec = array(
							'pr_path' => (($_source[$index]['_source']['phoneRecords'][$prindex]['direction'] == '1')?'撥入':'撥出'),
							'pr_phone' => $_source[$index]['_source']['phoneRecords'][$prindex]['phone'],
							'pr_name' => $_source[$index]['_source']['phoneRecords'][$prindex]['uname'],
							'pr_time' => $_source[$index]['_source']['phoneRecords'][$prindex]['pdate'],
							'pr_relationship' => $_source[$index]['_source']['phoneRecords'][$prindex]['relationship'],
							'pr_has_drug' => (($_source[$index]['_source']['phoneRecords'][$prindex]['drugmanA'] == 0)?'否':'是'),
							'pr_user' => (($_source[$index]['_source']['phoneRecords'][$prindex]['drugmanB'] == 0)?'否':'是'),
							'pr_seller' => (($_source[$index]['_source']['phoneRecords'][$prindex]['drugmanC'] == 0)?'否':'是'),
							// 'pr_p_no' => $_source[$index]['_source']['phoneRecords'][$prindex]['target'],
							'pr_p_no' => $_source[$index]['_source']['phones'][0]['phone'],
							'pr_s_ic' => $_source[$index]['_source']['upid'],
							'pr_p_num' => $insert_id
						);
						$this->getsqlmod->addphone_rec($phone_rec);
					}
					for ($vindex=0; $vindex < count($_source[$index]['_source']['vehicles']); $vindex++) { 
						$vehicle = array(
							'v_type' => $_source[$index]['_source']['vehicles'][$vindex]['type'],
							'v_license_no' => $_source[$index]['_source']['vehicles'][$vindex]['licensePlate'],
							'v_color' => $_source[$index]['_source']['vehicles'][$vindex]['color'],
							'v_s_num' => $insert_id
						);
						$this->getsqlmod->addcar($vehicle);
					}
	
					for ($dindex=0; $dindex < count($_source[$index]['_source']['drugs']); $dindex++) { 
						$taiwan_date = date('Y')-1911; 
						$query = $this->getsqlmod->get1DrugCount()->result(); 
						$countid = $query[0]->e_id;
						$lastNum = (int)mb_substr($countid, -5, 5);
						$lastNum++;
						$value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
						$EID = 'EN' . $taiwan_date. $value;
						$drugs = array(
							'e_id' => $EID,
							// 'e_c_num' => $CID,
							'e_s_ic' => $_source[$index]['_source']['upid'],
							'e_suspect' => $_source[$index]['_source']['uname'],
							'e_1_N_W' => $_source[$index]['_source']['drugs'][$dindex]['holdWeight'],
							'e_name' => $_source[$index]['_source']['drugs'][$dindex]['drugName'],
							'e_type' => $_source[$index]['_source']['drugs'][$dindex]['drugLevel'] . '級'
						);
						$this->getsqlmod->adddrug($drugs);
					}
	
					// for ($sindex=0; $sindex < count($_source[$index]['_source']['socialMedia']); $sindex++) { 
					// 	$social = array(
					// 		'social_media_name' => $_source[$index]['_source']['socialMedia'][$sindex]['type'],
					// 		'social_media_ac1' => $_source[$index]['_source']['socialMedia'][$sindex]['account'],
					// 		'social_media_ac2' => $_source[$index]['_source']['socialMedia'][$sindex]['phoneNumber'],
					// 		's_ic' => $_source[$index]['_source']['upid'],
					// 		's_num' => $insert_id
					// 	);
					// 	$this->getsqlmod->addsocialmedia($social);
					// }
				}
				
				
			}
        echo 'ok';
        } catch (\Throwable $th) {
            echo $th;
        }
	}
	// 個案移轉其他單位
	public function trans_s_office()
    {
		$snum = $this->uri->segment(3);
		if(isset($_POST['roffice']))
		{
			$updatedata= array(
				's_roffice' => $_POST['roffice'] . ((isset($_POST['rofficeunit']))?$_POST['rofficeunit']:'')
			);
			$this->getsqlmod->updateSuspRoffice($snum, $updatedata);
		}
        redirect('cases2/addNewCases','refresh');
    }
	function getotherdep(){
		if(isset($_GET['search']))
		{
			$query = $this->getsqlmod->getotherdep1bysearch($_GET['search'], $_GET['unselected'])->result_array(); 
		}
		else
		{
			$query = $this->getsqlmod->getotherdep1($_GET['unselected'])->result_array(); 
		}
		
		echo json_encode( array('results' => $query));
	}
	function getotherdepunit(){
		if(isset($_GET['search']))
		{
			$query = $this->getsqlmod->getotherdep2bysearch($_GET['parent'], $_GET['search'], $_GET['unselected'])->result_array(); 
		}
		else
		{
			$query = $this->getsqlmod->getotherdep2($_GET['parent'], $_GET['unselected'])->result_array(); 
		}
		
		echo json_encode( array('results' => $query));
	}
	function mergrCases(){
		$caseid_selected = json_decode($_POST['caseid_selected'], true);
		$merge_caseid = $caseid_selected[0];

		foreach ($caseid_selected as $key => $value) {
			$snum = $this->getsqlmod->searchSnumbyCnum($value)[0]['s_num'];

			$updatedata = array(
				's_cnum' => $merge_caseid
			);
			$this->getsqlmod->updateSuspCnum($snum, $updatedata);
		}
		echo 'ok';
		
	}
	function getCaseData(){
		$data = $this->getsqlmod->getCase($_POST['caseid'])->result_array();
		if(is_array($data))
		{
			$resp = array(
				'status' => 200,
				'data' => $data
			);
		}
		else
		{
			$resp = array(
				'status' => 404,
				'msg' => "資料載入錯誤"
			);
		}
		echo json_encode($resp);
	}
	function checkCasesInput(){
		$caseid = $_POST['caseid'];
		$data = $this->getsqlmod->checkCaseInput($_POST['caseid'])->result_array();
		if(is_array($data))
		{
			if(!isset($data[0]['nourine']) && $data[0]['doublecheck'] == 0)
			{
				$resp = array(
					'status' => 404,
					'msg' => "案件資料輸入不完整，請確認是否上傳檔案以及輸入檢驗資料。"
				);
			}
			else
			{
				if($data[0]['nourine'] != $data[0]['needurine'] && $data[0]['doublecheck'] != $data[0]['casehaddrug'])
				{
					$resp = array(
						'status' => 404,
						'msg' => "案件資料輸入不完整，請確認是否上傳檔案以及輸入檢驗資料。"
					);
				}
				else
				{
					$updatecase = array(
						'c_status' => '送裁罰'
					);
					$this->getsqlmod->updateCase($caseid, $updatecase);
	
					$case_susps = $this->getsqlmod->getsuspect('s_cnum',$caseid)->result_array();
					foreach ($case_susps as $cs_row) {
						$updatesusp = array(
							's_sac_state' => '待裁罰'
						);
						$this->getsqlmod->updateSuspSacNull($cs_row['s_num'], $updatesusp);
						$updatedrug = array(
							'e_state' => '單位保管中'
						);
						$this->getsqlmod->updateDrug('e_c_num',$cs_row['s_num'], $updatedrug);
	
					}
					$resp = array(
						'status' => 200,
						'data' => base_url('cases2/list3Cases')
					);
				}
			}
		}
		else
		{
			$resp = array(
				'status' => 404,
				'msg' => "資料載入錯誤"
			);
		}
		echo json_encode($resp);
	}
	function checkCasesInput_batch(){
		$caseid_selected = json_decode($_POST['caseid_selected'], true);

		foreach ($caseid_selected as $caseid) {
			$data = $this->getsqlmod->checkCaseInput($caseid)->result_array();
			if(is_array($data))
			{
				if(!isset($data[0]['nourine']) && $data[0]['doublecheck'] == 0)
				{
					$resp = array(
						'status' => 404,
						'msg' => "案件資料輸入不完整，請確認是否上傳檔案以及輸入檢驗資料。"
					);
				}
				else
				{
					if($data[0]['nourine'] != $data[0]['needurine'] && $data[0]['doublecheck'] != $data[0]['casehaddrug'])
					{
						$resp = array(
							'status' => 404,
							'msg' => "案件資料輸入不完整，請確認是否上傳檔案以及輸入檢驗資料。"
						);
						break;
					}
					else
					{
						$updatecase = array(
							'c_status' => '送裁罰'
						);
						$this->getsqlmod->updateCase($caseid, $updatecase);
		
						$case_susps = $this->getsqlmod->getsuspect('s_cnum',$caseid)->result_array();
						foreach ($case_susps as $cs_row) {
							$updatesusp = array(
								's_sac_state' => '待裁罰'
							);
							$this->getsqlmod->updateSuspSacNull($cs_row['s_num'], $updatesusp);
							$updatedrug = array(
								'e_state' => '待送驗'
							);
							$this->getsqlmod->updateDrug('e_c_num',$cs_row['s_num'], $updatedrug);
		
						}
						$resp = array(
							'status' => 200,
							'data' => base_url('cases2/list3Cases')
						);
					}
				}
			}
			else
			{
				$resp = array(
					'status' => 404,
					'msg' => "資料載入錯誤"
				);
				break;
			}
		}
		
		
		echo json_encode($resp);
	}
	function deletedrugpaper(){
		$caseid = $this->uri->segment(3);
		$updatedata= array(
			'drug_doc' =>null
		);
		$this->getsqlmod->updateCase($caseid, $updatedata);

		redirect('cases2/editCases2/'.$caseid,'refresh');
	}
	function deleteurinepaper(){
		$snum = $this->uri->segment(3);
		$updatedata= array(
			's_utest_doc' =>null
		);
		$this->getsqlmod->updateSusp($snum, $updatedata);

		redirect('cases2/editsusp2/'.$snum,'refresh');
	}
	public function updateDrugStatus(){		
		$e_id = $_POST['eid'];
		$e_state = $_POST['estatus'];

		if(!empty($_FILES['estatusdoc']['name'])){
			$_FILES['estatusdoc']['name']  = $e_id . $_FILES['estatusdoc']['name'];
			$config['upload_path']          = 'drugdoc/';
			$config['allowed_types']        = 'pdf';
			$config['max_size']             = 100000;
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('estatusdoc')){
				$error = array('error' => $this->upload->display_errors());
				//redirect('cases2/editCases2/' . $_POST['s_cnum'],'refresh'); 
			}
			else{
				$uploadData = $this->upload->data();
				$_POST['estatusdoc']=$uploadData['file_name'];

			}
			$updatedata = array(
				'e_state' => $e_state,
				'e_state_doc' => $_POST['estatusdoc'],
				'e_place' => (isset($_POST['eplace']))?$_POST['eplace']:null
			);
		}
		else
		{
			$updatedata = array(
				'e_state' => $e_state,
				'e_place' => (isset($_POST['eplace']))?$_POST['eplace']:null
			);
		}
		$this->getsqlmod->updateDrug1($e_id, $updatedata);

		$adddruglog = array(
			'drug_rec_eid' => $e_id,
			'drug_rec_estatus' => $e_state,
			'drug_rec_eplace' => ((isset($_POST['eplace']))?$_POST['eplace']:'刑案證物室'),
			'drug_rec_empno' => $this -> session -> userdata('uname'),
			'drug_rec_office' => $this -> session -> userdata('uoffice')
		);
		$this->getsqlmod->adddrugrec($adddruglog);
		echo 'ok';
	}
	// 證物入庫清冊
    public function export_drugsreceive_list(){
        $id = $this->uri->segment(3);
		$search = array(
			'startdate' => $_GET['startdate'],
			'enddate' => $_GET['enddate'],
			'e_state' => '送警察局'
		);
        // var_dump($data);
		// exit;
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$i = 0;
		$htmlString = '';
		$detail = $this->getsqlmod->getDrugRoffice_detail_byCases($search, $this -> session -> userdata('uoffice'))->result_array(); // 抓sic
			
		$htmlString = '<table border="1">
					<thead>
						<tr style="height:1.5em;">
							<td colspan="9" align="center" style="font-size:14px;">'. (date('Y')-1911) . '年' .str_pad(date('m'),2,'0',STR_PAD_LEFT) .'月'. str_pad(date('d'),2,'0',STR_PAD_LEFT).'日'.$this -> session -> userdata('uroffice').'入庫清冊</td>
						</tr>
						<tr>
							<td colspan="9" align="right">列印日期：'.(date('Y')-1911).'年'.date('m').'月'.date('d').'日</td>
						</tr>
						<tr>
							<td align="center">地檢署或地方法院<br/>
							(保號或囑託銷燬文號)</td>
							<td align="center">CA系統編號</td>
							<td align="center">列管編號</td>
							<td align="center">查獲單位處分書文號</td>
							<td align="center">受處分人姓名</td>
							<td align="center">身分證號</td>
							<td align="center">毒品種類</td>
							<td align="center">查扣物件<br/>
							(包、個、瓶、張、支、粒)</td>
							<td>重量(g)<br/>
							(或微量無法磅秤)</td>
						</tr>
					</thead>
					<tbody>';

		foreach ($detail as $detail_row) {                     

			$htmlString .= '<tr>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center"></td>
				<td align="center">'.$detail_row['s_fno'].'</td>
				<td align="center">'.$detail_row['s_name'].'</td>
				<td align="center">'.$detail_row['s_ic'].'</td>
				<td align="center">'.$detail_row['e_name'].'</td>
				<td align="center">'.$detail_row['e_type'].'</td>
				<td align="center">$'.$detail_row['e_1_N_W'].'</td>
			</tr>';
			
		}
		
				
		$htmlString .= '</tbody>
		<tfoot>
			<tr>
				<td colspan="9"></td>
			</tr>
			<tr>
				<td colspan="9"></td>
			</tr>
			<tr>
				<td>交件人：</td>
				<td colspan="8"></td>
			</tr>
			<tr>
				<td colspan="9"></td>
			</tr>
			<tr>
				<td colspan="9"></td>
			</tr>
			<tr>
				<td>收件人：</td>
				<td colspan="8"></td>
			</tr>
		</tfoot>
		</table>';

	// if($i == 0)
	// {
		
	//     $spreadsheet = $reader->loadFromString($htmlString);
		
	// }
	// else
	// {                
		$spreadsheet = $reader->loadFromString($htmlString, $spreadsheet);
		
	// }
	$spreadsheet->getSheet($i)->setTitle($this -> session -> userdata('uroffice'));
            
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        // $writer->save('write.xlsx'); 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.(date('Y')-1911) . '年' .str_pad(date('m'),2,'0',STR_PAD_LEFT) .'月'. str_pad(date('d'),2,'0',STR_PAD_LEFT).'日入庫清冊.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file

    }
	/** 轉西元年 */
    public function tranfer2ADyear($date)
    {
        if(strlen($date) == 6)
        {
            $ad = ((int)substr($date, 0, 2)) + 1911;
            return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
        }
        elseif(strlen($date) == 7)
        {
            $ad = ((int)substr($date, 0, 3)) + 1911;
            return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
        }
        else
        {
            return '';
        }
    }
    /** 轉民國年 */
    public function tranfer2RCyear($date)
    {
        $datestr = str_replace('-', '', $date);
        
        $rc = ((int)substr($datestr, 0, 4)) - 1911;
        return (string)$rc . substr($datestr, 4, 4) ;
    }
	/** 轉民國年 中文年-月-日 */
	function tranfer2RCyearTrad($date)
	{
		if($date != '0000-00-00' && !empty($date) && isset($date))
		{
			$date = str_replace('-', '', $date);
			$rc = ((int)substr($date, 0, 4)) - 1911;
			return (string)$rc . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2) ;
		}
		else
		{
			return '';
		}
		
	}
	/** 轉民國年 中文年-月-日 時:分:秒 */
	function tranfer2RCyearTrad2($datetime)
	{
		$date = substr($datetime, 0, 10);
		$date = str_replace('-', '', $date);
		$rc = ((int)substr($date, 0, 4)) - 1911;
		return (string)$rc . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2)  . ' ' . substr($datetime, 11, 19) ;
	}
	/** 轉民國年 中文年月日 */
    function tranfer2RCyearTrad3($date)
    {
        $date = str_replace('-', '', $date);
        $rc = ((int)substr($date, 0, 4)) - 1911;
        return (string)$rc . '年' . substr($date, 4, 2) . '月' . substr($date, 6, 2) . '日';
    }
}
