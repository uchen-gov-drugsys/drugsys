<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Test_BVC extends CI_Controller {//帳務憑證
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }

    function index(){
        $data['title'] = "換算虛擬碼";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = '3RPlistALL';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    function testBVC(){
        //echo (int)$_POST['s_go'];   
        $code =  '21196'.$_POST['cnum'];
        $code1 = preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY);
        $code2=0;
        for($i=0;$i<count($code1);$i++){
            if($i==0||$i==3||$i==6||$i==9||$i==12||$i==15) $code2=$code2+$code1[$i]*3;
            if($i==1||$i==4||$i==7||$i==10||$i==13) $code2=$code2+($code1[$i]*7);
            if($i==2||$i==5||$i==8||$i==11||$i==14) $code2=$code2+($code1[$i]*1);
        }
        $code3 = $code2%10;
        $code4 = 10-$code3;
        $fin = $code.$code4;
        echo $fin;
    }
}
