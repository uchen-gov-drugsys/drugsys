    <?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Fine_traf extends CI_Controller {//移送罰緩
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }

    function table_dp($id) {//
        $this->load->library('table');
        $query = $this->getsqlmod->getFTListFine($id)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 100%" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限','寬限期限','催繳送達情形');
        $table_row = array();
        foreach ($query as $susp)
        {
            $date = new DateTime($susp->f_date); // Y-m-d
            $date->add(new DateInterval('P30D'));
            if($date->format('d')>25){
                $date->add(new DateInterval('P5D'));
            }
            $day = $date->format('d')%5;
            //$day = 6;
            if($day < 5){
                while($day  < 5){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            else if($day < 10){
                while($day  < 10){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->s_num;
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->fd_zipcode;
            $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            $table_row[] = $this->tranfer2RCyear2($susp->f_date);
            $table_row[] = $this->tranfer2RCyear2($susp->call_grace);
            $table_row[] = $susp->call_delivery_status.'  '.$susp->call_delivery_date;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    public function index() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $cp=$this->getsqlmod->getftproject($table)->result();
        $test_table = $this->table_dp($table);
        $type_options = array();
        foreach ($cp as $rp1){
            $type_options[$rp1->ftp_name] = $rp1->ftp_name;
        }
        $data['s_table'] = $test_table;
        $data['title'] = "待移送罰緩案件";
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Fine_traf/listdp';
        else $data['include'] = 'Fine_traf_query/listdp';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function updateProject() {//加入專案
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $uname = $this -> session -> userdata('uname');
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        //var_dump($_POST['s_cnum']);
        if($_POST['s_status']=='1' && $_POST['s_cnum']!=''){//
            foreach ($s_cnum as $key => $value) {
                //echo '<br>';
                $query = $this->getsqlmod->getCalldetail($value)->result(); 
                $fine_traf = $this->getsqlmod->getfine_trafwithsnum($value)->result(); 
                $fine_t = $this->getsqlmod->getfine_traf()->result(); 
                $fine_tnum = $fine_t[0]->ft_no; 
                $c_value = substr($fine_tnum,-4);
                $c_value++;
                $c_value = str_pad($c_value,4,'0',STR_PAD_LEFT);
                $taiwan_date = date('Y')-1911; 
                $ft_no=$taiwan_date .'-'.$c_value ;
                //echo  $ft_no.'<br>';
                $susp = $query[0]; 
                $data = array(
                    'ft_no' => $ft_no,
                    'ft_send_no' => $_POST['projectname'],
                    'ft_projectnum' => $_POST['projectname'],
                    'ft_empno' => $uname,
                    'ft_snum' => $susp->f_snum,
                    'ft_sic' => $susp->f_sic,
                    'ft_cnum' => $susp->f_cnum,
                    'ft_dnum' => $susp->f_num,
                    'ft_callnum' => $susp->call_num,//確認可以嗎？
                );
                if(!isset($fine_traf[0]->ft_snum))$this->getsqlmod->addft($data);
                else $this->getsqlmod->updateft($value,$data);
            }
            $data1 = array(
                    'ftp_name' => $_POST['projectname'],
                    'ftp_date' => date('Y-m-d'),
                    'ftp_empno' => $uname,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addftp($data1);
            redirect('Fine_traf/ProjectList/'); 
        }
        else if($_POST['s_status']=='0' && $_POST['s_cnum']!=''){//加入舊案
            foreach ($s_cnum as $key => $value) {
                $query = $this->getsqlmod->getCalldetail($value)->result(); 
                $fine_traf = $this->getsqlmod->getfine_trafwithsnum($value)->result(); 
                $fine_t = $this->getsqlmod->getfine_traf()->result(); 
                $fine_tnum = $fine_t[0]->ft_no; 
                $c_value = substr($fine_tnum,-4);
                $c_value++;
                $c_value = str_pad($c_value,4,'0',STR_PAD_LEFT);
                $taiwan_date = date('Y')-1911; 
                $ft_no=$taiwan_date .'-'.$c_value ;
                //echo  $ft_no.'<br>';
                $susp = $query[0]; 
                $data = array(
                    'ft_no' => $ft_no,
                    'ft_send_no' => $_POST['ftp_name'],
                    'ft_projectnum' => $_POST['ftp_name'],
                    'ftp_empno' => $uname,
                    'ft_snum' => $susp->f_snum,
                    'ft_sic' => $susp->f_sic,
                    'ft_cnum' => $susp->f_cnum,
                    'ft_dnum' => $susp->f_num,
                    'ft_callnum' => $susp->call_num,//確認可以嗎？
                );
                if(!isset($fine_traf[0]->ft_snum))$this->getsqlmod->addft($data);
                else $this->getsqlmod->updateft($value,$data);
            }
            redirect('Fine_traf/ProjectList/'); 
        }
        else redirect('Fine_traf/index/'); 
    }

    function table_DPProject1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getftproject($id)->result(); // 
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('','專案編號','發文日期');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $table_row = NULL;
            $table_row[] = $susp->ftp_name;
            $table_row[] = anchor('Fine_traf/listtraf/' . $susp->ftp_name, $susp->ftp_name);
            $table_row[] = $this->tranfer2RCyear2($susp->ftp_date);
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    
    public function ProjectList() {//(催繳專案)
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table_DPProject1($table);
        $data['s_table'] = $test_table;
        $data['title'] = "催繳專案列表";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Fine_traf/dpprojectlist';
        else $data['include'] = 'Fine_traf_query/dpprojectlist';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    function table_CP($id) {//催繳案件一般
        $this->load->library('table');
        $query = $this->getsqlmod->getFTListfromProject($id)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 100%" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限','發文日期(罰緩)','寬限期限','催繳送達情形');
        $table_row = array();
        foreach ($query as $susp)
        {
            $date = new DateTime($susp->f_date); // Y-m-d
            $date->add(new DateInterval('P30D'));
            if($date->format('d')>25){
                $date->add(new DateInterval('P5D'));
            }
            $day = $date->format('d')%5;
            //$day = 6;
            if($day < 5){
                while($day  < 5){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            else if($day < 10){
                while($day  < 10){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->call_snum;
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->fd_zipcode;
            $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            $table_row[] = $susp->f_date;
            $table_row[] = $susp->fd_date;
            $table_row[] = $susp->call_grace;
            $table_row[] = $susp->call_delivery_status.'  '.$susp->call_delivery_date;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    public function listtraf() {//處理專案
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $dp = $this->getsqlmod->getftprojecttype($id)->result();
        $susp = $this->getsqlmod->getFTListfromProject($id)->result();
        $test_table = $this->table_CP($id);
        $data['s_table'] = $test_table;
        $data['title'] = "待移送罰緩專案:".$id;
        //$data['type'] = $dp[0]->cp_type;
        $data['sp'] = $dp[0]->ftp_name;
        $data['fd_num'] = $susp[0]->s_dp_project;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Fine_traf/dplist1';
        else $data['include'] = 'Fine_traf_query/dplist1';
        $data['nav'] = 'navbar3';
        $data['id'] = $id;
        $this->load->view('template', $data);
    }
    
    public function listtraf_ed() {//專案編輯（一般）
        $this->load->library('table');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getFTListfromProject($id)->result(); 
        $dp = $this->getsqlmod->getftprojecttype($id)->result();
        $data['sp'] = $dp[0]->ftp_name;
        $tmpl = array (
            'table_open' => '<table style="width: 2400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限','發文日期(罰緩)','發文日期','本號(移送發文文號)','支號','寬限期限','送達情形','送達日期');
        $table_row = array();
        $i=0;
		$str = "";
        foreach ($query as $susp)
        {
			$i++;
			if($susp->fd_address == null || $susp->fd_address == '0'){
			    $fd_addr = '<input name="fd_address['.$susp->ft_snum.']" required="required" class="form-control"  value="'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress.'" onkeypress="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')"  onchange="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')">';
			}
			else{
			    $fd_addr = '<input name="fd_address['.$susp->ft_snum.']" required="required"  value="'.$susp->fd_address.'" class="form-control" onkeypress="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')"  onchange="listzipcode($(this), \'#fd_zipcode_'.$susp->call_snum.'\')">';
			}

			if($susp->ft_send_bno == null || $susp->ft_send_bno == '0'){
				$bno = '<input name="ft_send_bno['.$susp->ft_snum.']" required="required" value='.$i.' type="number" class="form-control">';
			}
			else{
				$bno = '<input name="ft_send_bno['.$susp->ft_snum.']"  required="required" value='.$susp->ft_send_bno.' type="number" class="form-control">';
			}

			if($susp->call_delivery_status == '已送達'){
				$delivery_status = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" class="form-control">
						<option selected value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
						<option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
						<option value="公示送達">公示送達</option>
						</select>';
			}
			else if($susp->call_delivery_status == '現住地未送達'){
				$delivery_status = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" class="form-control">
						<option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
						<option selected value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
						<option value="公示送達">公示送達</option>
						</select>';
			}
			else if($susp->call_delivery_status == '戶籍地未送達'){
				$delivery_status = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" class="form-control">
						<option value="已送達">已送達</option><option selected value="戶籍地未送達">戶籍地未送達</option>
						<option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
						<option value="公示送達">公示送達</option>
						</select>';
			}
			else if($susp->call_delivery_status == '公示送達'){
				$delivery_status = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" class="form-control">
						<option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
						<option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
						<option selected value="公示送達">公示送達</option>
						</select>';
			}
			else if($susp->call_delivery_status == '已出監'){
				$delivery_status = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" class="form-control">
						<option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
						<option value="現住地未送達">現住地未送達</option><option selected value="已出監">已出監</option>
						<option value="公示送達">公示送達</option>
						</select>';
			}
			else{
				$delivery_status = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" class="form-control">
						<option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
						<option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
						<option value="公示送達">公示送達</option>
						</select>';
			}

			if($susp->call_delivery_date == null){
				$delivery_date = '<input id="call_delivery_date" name="call_delivery_date['.$susp->s_num.']" value="'.$this->tranfer2RCyear2(date('Y-m-d')).'"type="text" class="form-control rcdate"/> 
				<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>';
			}
			else{
				$delivery_date = '<input id="call_delivery_date" name="call_delivery_date['.$susp->s_num.']" value="'.((isset($susp->call_delivery_date))?((strlen($susp->call_delivery_date) > 7 && $susp->call_delivery_date != '0000-00-00')?str_pad(((int)substr($susp->call_delivery_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->call_delivery_date, 5, 2).substr($susp->call_delivery_date, 8, 2):''):'').'"type="text" class="form-control rcdate"/> 
				<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>';
			}
			$str .= '<div class="panel panel-default" style="'.(($i%2 == 1)?'background-color:#eee;':'background-color:#FFF;').'">
				<div class="panel-body">
					<div class="row">
						<input type="hidden" name="ft_snum" value="'.$susp->ft_snum.'"/>
						<div class="form-group col-md-3">
							<label>處分書編號</label>
							<p class="text-primary">'.$susp->fd_num.'</p>
						</div>
						<div class="form-group col-md-3">
							<label>受處分人</label>
							<p>'.$susp->s_name.'</p>
						</div>
						<div class="form-group col-md-3">
							<label>郵遞區號</label>
							<input name="fd_zipcode['.$susp->ft_snum.']" type="number" required="required" value='.$susp->fd_zipcode.' class="form-control">
						</div>	
						<div class="form-group col-md-3">
							<label>戶籍地址</label>
							'.$fd_addr.'
						</div>
						<div class="form-group col-md-3">
							<label>現住地址</label>
							<p>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress.'</p>
						</div>
						<div class="form-group col-md-3">
							<label>繳納期限</label>
							<p>'.$this->tranfer2RCyear2($susp->f_date).'</p>
						</div>
						<div class="form-group col-md-3">
							<label>發文日期(罰緩)</label>
							<p>'.$this->tranfer2RCyear2($susp->fd_date).'</p>
						</div>
						<div class="form-group col-md-3">
							<label>發文日期</label>
							<input name="ft_send_date['.$susp->ft_snum.']" type="text" required="required" value='.((isset($susp->ft_send_date))?((strlen($susp->ft_send_date) > 7 && $susp->ft_send_date != '0000-00-00')?str_pad(((int)substr($susp->ft_send_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->ft_send_date, 5, 2).substr($susp->ft_send_date, 8, 2):((int)date('Y') - 1911).date('md')):'').' class="form-control rcdate" >
							<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
						</div>
						<div class="form-group col-md-3">
							<label>本號(移送發文文號)</label>
							<input name="ft_send_no['.$susp->ft_snum.']"  required="required" value='.$susp->ft_send_no.' class="form-control">
						</div>
						<div class="form-group col-md-3">
							<label>支號</label>
							'.$bno.'
						</div>
						<div class="form-group col-md-3">
							<label>寬限期限</label>
							<p>'.$this->tranfer2RCyear2($susp->call_grace).'</p>
						</div>
						<div class="form-group col-md-3">
							<label>送達情形</label>
							'.$delivery_status.'
						</div>
						<div class="form-group col-md-3">
							<label>送達日期</label>
							'.$delivery_date.'
						</div>
					</div>
				</div>
			</div>';
            // $i++;
            // $date = new DateTime($susp->f_date); // Y-m-d
            // $date->add(new DateInterval('P30D'));
            // if($date->format('d')>25){
            //     $date->add(new DateInterval('P5D'));
            // }
            // $day = $date->format('d')%5;
            // //$day = 6;
            // if($day < 5){
            //     while($day  < 5){
            //         $day++;
            //         $date->add(new DateInterval('P1D'));
            //     }
            // }
            // else if($day < 10){
            //     while($day  < 10){
            //         $day++;
            //         $date->add(new DateInterval('P1D'));
            //     }
            // }
            // $table_row = NULL;
            // $table_row[] = $susp->ft_snum;
            // $table_row[] = $susp->fd_num;
            // $table_row[] = $susp->s_name;
            // $table_row[] = '<input name="fd_zipcode['.$susp->ft_snum.']" type="number" required="required" value='.$susp->fd_zipcode.'>';
            // if($susp->fd_address == null || $susp->fd_address == '0'){
            //     $table_row[] = '<input name="fd_address['.$susp->ft_snum.']" required="required" class="form-control"  value="'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress.'" >';
            // }
            // else{
            //     $table_row[] = '<input name="fd_address['.$susp->ft_snum.']" required="required" style="width:250px" value="'.$susp->fd_address.'" >';
            // }
            // $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            // $table_row[] = $susp->f_date;
            // $table_row[] = $susp->fd_date;
            // $table_row[] = '<input name="ft_send_date['.$susp->ft_snum.']" type="date" required="required" value='.$susp->ft_send_date.' class="form-control" >';
            // $table_row[] = '<input name="ft_send_no['.$susp->ft_snum.']"  required="required" value='.$susp->ft_send_no.' >';
            // if($susp->ft_send_bno == null || $susp->ft_send_bno == '0'){
            //     $table_row[] = '<input name="ft_send_bno['.$susp->ft_snum.']" style="width:20px" required="required" value='.$i.' >';
            // }
            // else{
            //     $table_row[] = '<input name="ft_send_bno['.$susp->ft_snum.']" style="width:20px" required="required" value='.$susp->ft_send_bno.' >';
            // }
            // $table_row[] =  $susp->call_grace;
            // if($susp->call_delivery_status == '已送達'){
            //     $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //             <option selected value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //             <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //             <option value="公示送達">公示送達</option>
            //             </select>';
            // }
            // else if($susp->call_delivery_status == '現住地未送達'){
            //     $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //             <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //             <option selected value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //             <option value="公示送達">公示送達</option>
            //             </select>';
            // }
            // else if($susp->call_delivery_status == '戶籍地未送達'){
            //     $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //             <option value="已送達">已送達</option><option selected value="戶籍地未送達">戶籍地未送達</option>
            //             <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //             <option value="公示送達">公示送達</option>
            //             </select>';
            // }
            // else if($susp->call_delivery_status == '公示送達'){
            //     $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //             <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //             <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //             <option selected value="公示送達">公示送達</option>
            //             </select>';
            // }
            // else if($susp->call_delivery_status == '已出監'){
            //     $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //             <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //             <option value="現住地未送達">現住地未送達</option><option selected value="已出監">已出監</option>
            //             <option value="公示送達">公示送達</option>
            //             </select>';
            // }
            // else{
            //     $table_row[] = '<select id="call_delivery_status" name="call_delivery_status['.$susp->s_num.']" type="text" >
            //             <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
            //             <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
            //             <option value="公示送達">公示送達</option>
            //             </select>';
            // }
            // if($susp->call_delivery_date == null){
            //     $table_row[] = '<input id="call_delivery_date" name="call_delivery_date['.$susp->s_num.']" value="'.date('Y-m-d').'"type="date"/> ';
            // }
            // else{
            //     $table_row[] = '<input id="call_delivery_date" name="call_delivery_date['.$susp->s_num.']" value="'.$susp->call_delivery_date.'"type="date"/> ';
            // }
            $this->table->add_row($table_row);
        }   
        $data['title'] = "待移送罰緩專案:".$id;
        $data['fpid'] = $id;
        $data['ft_send_date'] = $query[0]->ft_send_date;
        $data['user'] = $this -> session -> userdata('uic');
        // $data['s_table'] = $this->table->generate();
		$data['s_table'] = $str;
        $data['id'] = $id;
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Fine_traf/dplist_ed';
        else $data['include'] = 'Fine_traf_query/dplist_ed';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function updateTPDate(){
        //var_dump($_POST);
        $this->getsqlmod->updateFTdate($_POST['ft_projectnum'],$this->tranfer2ADyear($_POST['ft_send_date']));
        redirect('Fine_traf/listtraf_ed/'.$_POST['ft_projectnum']); 
    }
    
    public function updatepublicdate(){
        //var_dump($_POST);
        $this->getsqlmod->updatepublicdate($_POST['call_no'],$this->tranfer2ADyear($_POST['call_date']));
        redirect('Fine_traf/listtraf_ed_public/'.$_POST['call_no']); 
    }
    
    public function updateCP(){
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        var_dump($_POST);
        foreach ($s_cnum1 as $key => $value) {
            $fine_doc = array(
                'fd_address' => $_POST['fd_address'][$value],
                'fd_zipcode' => $_POST['fd_zipcode'][$value],
            );
            $call = array(
                'call_grace' => $this->tranfer2ADyear($_POST['call_grace'][$value]),
                'call_delivery_status' => $_POST['call_delivery_status'][$value],
                'call_delivery_date' => $this->tranfer2ADyear($_POST['call_delivery_date'][$value]),
            );
            $ft = array(
                'ft_send_date' => $this->tranfer2ADyear($_POST['ft_send_date'][$value]),
                'ft_send_no' => $_POST['ft_send_no'][$value],
                'ft_send_bno' => $_POST['ft_send_bno'][$value],
            );
            //var_dump($fine_doc);
            //echo '<br>';
            //var_dump($call);
            $this->getsqlmod->updateft($value,$ft);
            $this->getsqlmod->updateFD($value,$fine_doc);
            $this->getsqlmod->updatecalldoc($value,$call);
        };
        redirect('Fine_traf/listtraf/'.$_POST['call_projectnum']); 
    }
    
    public function updatestatus_ready() {
        $this->load->helper('form');
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        var_dump($s_cnum);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $this->getsqlmod->updateCall_Project_sp($value,'寄出');
            }
            redirect('Fine_traf/ProjectList/'); 
        }
    }    
    
    public function exportCSV(){//公示清冊
        $this->load->dbutil();
        $this->load->helper('download');
        $id = $this->uri->segment(3);
        $filename = '發文簿'.date('Ymd').'.csv';
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/vnd.ms-excel; "); 
        $usersData = $this->getsqlmod->getFTListfromProject($id)->result();
        $file = fopen('php://output', 'a');
        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
        $header = array("編號","總收發文字","收(發)文日期","移送案號","事由","辦理情形","備考"); 
        fputcsv($file, $header);
        $i=0;
        foreach ($usersData as $key=>$line){ 
            $i++;
            fputcsv($file,array($i.'.',$line->ft_send_no,$line->ft_send_date,$line->ft_no,$line->s_name.'移送書','移'.$line->ft_target.'分署辦理',''));
        }
        fclose($file); 
        exit;
        //$query = $this->getsqlmod->getfine_list($id); 
        //$data = $this->dbutil->csv_from_result($query);
        //force_download($filename,  $data);
    }

	public function txtUploadIndex() {
		
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $cp=$this->getsqlmod->getftproject($table)->result();

        $data['title'] = "執行署文件更新";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'Fine_traf/txt_receive';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }  
	public function execCommandIndex() {
		
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $cp=$this->getsqlmod->getftproject($table)->result();

        $data['title'] = "執行命令登記";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'Fine_traf/execCommand';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
	public function updateTxt(){
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200); 
		
		header( "Content-Type: text/html; charset=big5");
		$fh = fopen($_FILES['upload_file']['tmp_name'],'r');
		$print = array();
		while ($line = fgets($fh)) {
			if(is_bool(strstr($line, 'EOF')))
			{
				$line = preg_replace('/\s\s+|\x20/', ':', $line);
				$ary = explode(":", $line);
				$ary[0] = str_replace('TPY001', '', $ary[0]);
				$ary[4] = mb_substr($ary[1], 13, 1 , 'big5');
				$ary[5] = mb_substr($ary[1], 0, 13 );
				array_push($print, $ary);
			}
		}                
		fclose($fh);
		// header( "Content-Type: text/html; charset=utf-8");
		$data = array();
		foreach ($print as $pk => $pv) {
			$movedate = '';
			$searchdata = array(
				'moveno' => iconv("BIG5", "UTF-8//TRANSLIT", $pv[0]),//mb_substr($pv[0], -8),
				'userid' => $pv[2]
			);
			// var_dump($searchdata);
			$susp = $this->getsqlmod->getFineimportBymovelog($searchdata)->result_array();
			// var_dump($susp);
			// exit;
			$depmove_log = $this->getsqlmod->getFineimport_data($susp[0]['f_no'])->result_array();

			$calcmovelog = explode("\n", $susp[0]['f_movelog']);
			foreach ($calcmovelog as $key => $value) {
				if(!is_bool(strstr( explode('_',$value)[1], mb_substr($pv[0], -8))))
				{
					$movedate = explode('_', $value)[0];
				}
			}
			usort($calcmovelog, function($a, $b){ 
				
				$movedateA = explode('_', $a)[0];
				$movedateB = explode('_', $b)[0];
				return ($movedateA < $movedateB)? 1 : -1; 
			}); 

			if(isset($depmove_log[0]['f_depmovelog']) && !empty($depmove_log[0]['f_depmovelog']))
			{
				$str = "\n";
			}
			else
			{
				$str = "";
			}
			// echo $depmove_log[0]['f_depmovelog'] . $str . $movedate .'_'.$pv[0].'_'.$ary[5].'_'.$ary[4];
			// var_dump($pv);
			// echo $movedate .'_'.$pv[0].'_'.mb_substr($pv[5], 0, NULL , 'big5').'_'.mb_substr($pv[4], 0, NULL , 'big5');
			$deplog = $depmove_log[0]['f_depmovelog'] . $str . $movedate . '_' . iconv("BIG5", "UTF-8//TRANSLIT", $pv[0]) .'_'.$pv[5].'_'.iconv("BIG5", "UTF-8//TRANSLIT", $pv[4]);
			$updatedata = array(
				'f_depmovelog' =>  $deplog
			);
			// var_dump($updatedata);
			$this->getsqlmod->addMoveDepLog($susp[0]['f_no'], $deplog);

			$susp_new = $this->getsqlmod->getFineimport_data($susp[0]['f_no'])->result_array();
			
			$calcdepmovelog = explode("\n", $susp_new[0]['f_depmovelog']);
			usort($calcdepmovelog, function($a, $b){ 
				$movedateA = explode('_', $a)[0];
				$movedateB = explode('_', $b)[0];
				return ($movedateA < $movedateB)? 1 : -1; 
			}); 
			array_push($data, array(
				'type' => (($susp_new[0]['type'] == 'A')?'<span class="label label-danger">罰鍰</span>':'<span class="label label-warning">怠金</span>'),
				'f_caseid' => $susp_new[0]['f_caseid'],
				'f_username' => $susp_new[0]['f_username'],
				'f_userid' => $susp_new[0]['f_userid'],
				'f_movelog' => implode("<br/>", $calcmovelog) ,
				'f_depmovelog' => implode("<br/>", $calcdepmovelog)
			));
		}
		echo json_encode($data);
	}

	public function loadExecTxt(){
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200); 
		
		header( "Content-Type: text/html; charset=utf-8");
		$fh = fopen($_FILES['upload_file']['tmp_name'],'r');
		$print = array();
		while ($line = fgets($fh)) {
			if(is_bool(strstr($line, 'EOF')))
			{
				$line = preg_replace('/\s\s+|\x20/', ':', $line);
				$ary = explode(":", $line);
				// $ary[0] = str_replace('TPY001', '', $ary[0]);
				// $ary[4] = mb_substr($ary[1], 13, 1 , 'big5');
				// $ary[5] = mb_substr($ary[1], 0, 13 );
				if($ary[1] == '204A16')
				{
					$searchdata = array(
						'moveno' => $ary[2],
					);
					$user = $this->getsqlmod->getFineimportBymoveno($searchdata)->result_array();
					$line = array(
						'getdocdate' => $_POST['getdocdate'],
						'moveno' => $ary[2],
						'selfofficeno' => $_POST['selfofficeno'],
						'caseno' => $user[0]['f_caseid'],
						'casename' => $user[0]['f_username'],
						'action' => '禁止'
					);
					array_push($print, $line);
				}
				
			}
		}                
		fclose($fh);
		echo json_encode($print);
		// var_dump($print);
		exit;
		// header( "Content-Type: text/html; charset=utf-8");
		$data = array();
		foreach ($print as $pk => $pv) {
			$movedate = '';
			$searchdata = array(
				'moveno' => iconv("BIG5", "UTF-8//TRANSLIT", $pv[0]),//mb_substr($pv[0], -8),
				'userid' => $pv[2]
			);
			// var_dump($searchdata);
			$susp = $this->getsqlmod->getFineimportBymovelog($searchdata)->result_array();
			// var_dump($susp);
			// exit;
			$depmove_log = $this->getsqlmod->getFineimport_data($susp[0]['f_no'])->result_array();

			$calcmovelog = explode("\n", $susp[0]['f_movelog']);
			foreach ($calcmovelog as $key => $value) {
				if(!is_bool(strstr( explode('_',$value)[1], mb_substr($pv[0], -8))))
				{
					$movedate = explode('_', $value)[0];
				}
			}
			usort($calcmovelog, function($a, $b){ 
				
				$movedateA = explode('_', $a)[0];
				$movedateB = explode('_', $b)[0];
				return ($movedateA < $movedateB)? 1 : -1; 
			}); 

			if(isset($depmove_log[0]['f_depmovelog']) && !empty($depmove_log[0]['f_depmovelog']))
			{
				$str = "\n";
			}
			else
			{
				$str = "";
			}
			// echo $depmove_log[0]['f_depmovelog'] . $str . $movedate .'_'.$pv[0].'_'.$ary[5].'_'.$ary[4];
			// var_dump($pv);
			// echo $movedate .'_'.$pv[0].'_'.mb_substr($pv[5], 0, NULL , 'big5').'_'.mb_substr($pv[4], 0, NULL , 'big5');
			$deplog = $depmove_log[0]['f_depmovelog'] . $str . $movedate . '_' . iconv("BIG5", "UTF-8//TRANSLIT", $pv[0]) .'_'.$pv[5].'_'.iconv("BIG5", "UTF-8//TRANSLIT", $pv[4]);
			$updatedata = array(
				'f_depmovelog' =>  $deplog
			);
			// var_dump($updatedata);
			$this->getsqlmod->addMoveDepLog($susp[0]['f_no'], $deplog);

			$susp_new = $this->getsqlmod->getFineimport_data($susp[0]['f_no'])->result_array();
			
			$calcdepmovelog = explode("\n", $susp_new[0]['f_depmovelog']);
			usort($calcdepmovelog, function($a, $b){ 
				$movedateA = explode('_', $a)[0];
				$movedateB = explode('_', $b)[0];
				return ($movedateA < $movedateB)? 1 : -1; 
			}); 
			array_push($data, array(
				'type' => (($susp_new[0]['type'] == 'A')?'<span class="label label-danger">罰鍰</span>':'<span class="label label-warning">怠金</span>'),
				'f_caseid' => $susp_new[0]['f_caseid'],
				'f_username' => $susp_new[0]['f_username'],
				'f_userid' => $susp_new[0]['f_userid'],
				'f_movelog' => implode("<br/>", $calcmovelog) ,
				'f_depmovelog' => implode("<br/>", $calcdepmovelog)
			));
		}
		echo json_encode($data);
	}

	public function loadSuspbymoveno(){
		$moveno = $_POST['moveno'];
		$searchdata = array(
			'moveno' => $moveno
		);
		$user = $this->getsqlmod->getFineimportBymoveno($searchdata)->result_array();
		$data = array(
			'caseno' => $user[0]['f_caseid'],
			'casename' => $user[0]['f_username']
		);
		echo json_encode($data);
	}

	public function updateExec(){
		// print_r($_POST);
		// exit;
		foreach ($_POST['caseno'] as $key => $value) {
			$exec_log = $this->getsqlmod->getFineimport_databycaseid($value)->result_array();
			if(isset($exec_log[0]['f_execlog']) && !empty($exec_log[0]['f_execlog']))
			{
				$str = "\n";
			}
			else
			{
				$str = "";
			}
			$execlog = $exec_log[0]['f_execlog'] . $str . $_POST['getdocdate'][$key] . '_' . $_POST['action'][$key] . '_' . $_POST['selfofficeno'][$key];
			$updatedata = array(
				'f_execlog' =>  $execlog
			);
			// var_dump($updatedata);
			$this->getsqlmod->addMoveExecLog($value, $execlog);
		}
		
		redirect('Fine_traf/execCommandIndex', 'refresh'); 
	}
	

	/** 轉民國年 */
    public function tranfer2RCyear2($date)
    {
		if(empty($date))
		{
			return '<strong>無相關日期</strong>';
		}
		elseif($date == '0000-00-00')
		{
			return '<strong>無相關日期</strong>';
		}
		else{
			
			$datestr = explode('-', trim($date));
        
			$rc = ((int)$datestr[0]) - 1911;
			return (string)$rc . '-' . $datestr[1] . '-' .$datestr[2] ;
		}
        
    }
	/** 轉西元年 */
    public function tranfer2ADyear($date)
    {
        if(substr($date, 0, 1) == 0 || substr($date, 0, 1) == '0')
        {
            $date = substr($date, 1, 6);            
        }
            
        if(strlen($date) == 6)
        {
            $ad = ((int)substr($date, 0, 2)) + 1911;
            return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
        }
        elseif(strlen($date) == 7)
        {
            
            $ad = ((int)substr($date, 0, 3)) + 1911;
            return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
        }
        else
        {
            return '';
        }

        
    }
}
