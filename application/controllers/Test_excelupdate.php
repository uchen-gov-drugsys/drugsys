<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Test_excelupdate extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }
    
    public function index() {
       // generate HTML table from query results
        $this->load->helper('form');
        $data['title'] = "沒有權限";
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'login';
        $data['error'] = '沒有權限';
        //$data['nav'] = 'navbar2_list';
        $this->load->view('template', $data);
        redirect('Test_excelupdate/test_update/'); 
    }
    
    public function test_update() { //測試人事excel更新界面
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $data['title'] = "測試excel更新";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'test/test_receive';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function receiveupdate() {//excel人事測試
        $this->load->helper('form');
         $taiwan_date = date('Y')-1911 . date('m') . date('d'); 
        if(!empty($_FILES['excel']['name'])){
            $_FILES['file']['name'] = $_FILES['excel']['name'];
            $_FILES['file']['type'] = $_FILES['excel']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['excel']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['excel']['error'];
            $_FILES['file']['size'] = $_FILES['excel']['size'];
            $config['upload_path'] = 'test/'; 
            $config['allowed_types'] = 'csv|xlsx|CSV';
            $config['max_size'] = '50000'; // max_size in kb
            $handle  = fopen($_FILES['excel']['tmp_name'],'r'); 
             
            while (($data = fgetcsv($handle, 10000, ",")) != FALSE) { 
                if($data[74] != null && $data[74] != '帳戶'){
                    $user = $this->getsqlmod->getempno_mail($data[74])->result(); 
                    $dui = strstr($data[90], "隊",true);
                    if(strlen($dui)<=15 && strlen($dui)>0){
                        $dui=$dui."隊";
                    }else{
                        $dui = strstr($data[90], "局",true);
                        $dui=$dui."局";
                    }
                    if(preg_match("/\隊長/i", $data[7]) || preg_match("/\所長/i", $data[7]) || preg_match("/\組長/i", $data[7]) || preg_match("/\局長/i", $data[7])) {
                        $admin = '1';
                    }
                    else $admin ='0';
                    if(!preg_match("/\偵查/i", $data[90]) && !preg_match("/\警備/i", $data[90]) && !preg_match("/\少年/i", $data[90])){
                        $permit = '1';
                    }
                    else $permit ='2';
                    //echo $data[3].$data[2].$data[1].$data[74].$data[90].$dui.$data[6].'職稱:'.$data[7].',permit:'.$permit.',admin：'.$admin.'<br>';
                    $dataSet1 = array (
                          'em_mailId' => $data[74],
                          'em_firstname' => $data[1],
                          'em_centername' => $data[2],
                          'em_lastname' => $data[3],
                          'em_job' => $data[7],
                          'em_officeAll' => $data[90],
                          'em_office' => $dui,
                          'em_priority' => $permit,
                          'em_IsAdmin' => $admin,
                    );
                    if(isset($user[0]->em_mailId)){
                        $this->getsqlmod->updateemail($data[74],$dataSet1);
                    }
                    else $this->getsqlmod->addemail($dataSet1);
                    //print_r($dataSet1);
                }
            }
            redirect('Test_excelupdate/test_update/','refresh'); 
        }
        
    }
}