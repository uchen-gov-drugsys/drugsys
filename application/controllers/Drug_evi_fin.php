<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Drug_evi_fin extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }

    function table_evi($id, $type) {//證物 列表
        $this->load->library('table');
        $type_options = array("" => '請選擇');
		switch ($type) {
			case 'keep':
				$query = $this->getsqlmod->getSuspDrug($id, '單位保管中')->result(); // 使用getsqlmod裡的getdata功能
				break;
			case 'process':
				$query = $this->getsqlmod->getSuspDrug($id, '送驗中')->result(); // 使用getsqlmod裡的getdata功能
				break;
			case 'check':
				$query = $this->getsqlmod->getSuspDrug($id, '送地檢')->result();
				break;
			case 'inlocal':
				$query = $this->getsqlmod->getSuspDrug($id, '領回')->result();
				break;
			case 'indrug':
				$query = $this->getsqlmod->getSuspDrug($id, '送警察局')->result();
				break;
			case 'receive':
				$query = $this->getsqlmod->getSuspDrug($id, '沒入物室')->result();
				break;
			case 'clean':
				$query = $this->getsqlmod->getSuspDrug($id, '已銷毀')->result();
				break;
		}
        
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover drug-table" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','列管編號','證物編號','受處分人','身份證字號','證物內容','檢驗級別成分','重量','證物狀態','證物地點','收案單位','毒報');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
			$drug = $this->getsqlmod->get3Drug2($susp->e_id); // 抓sic
            
            //$drug_doc = $this->getsqlmod->get1Drug($susp->e_id)->result(); // 抓scnum
            $table_row = NULL;
			$table_row[] = '';
            $table_row[] = $susp->fd_num;
            $table_row[] = $susp->e_id;
			$table_row[] = $susp->s_name;
			$table_row[] = $susp->s_ic;
            //$table_row[] = '<input type="checkbox" name="a">';
            $drug_ingredient = Null;
            foreach ($drug->result() as $drug3){
                if($drug3->ddc_num == null){}
                else{
                    $drug_ingredient = $drug_ingredient . $drug3->ddc_level .'級' . $drug3->ddc_ingredient.'<br>';
                }
            }
            // if($drug_ingredient==null){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = $drug_ingredient;
            // }
			$table_row[] = $susp->e_name;
			$table_row[] = $susp->e_type;
			$table_row[] = $susp->e_1_N_W;
            // if(preg_match("/f/i", $this->session-> userdata('3permit'))) {
            //     if($susp->e_state == null){
            //         $table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" type="text" >
            //         <option value="待送驗">待送驗</option><option value="送驗中">送驗中</option><option value="待裁罰">待裁罰</option><option value="送地檢">送地檢</option><option value="待銷毀">待銷毀</option>
            //         </select>';
            //     }
            //     else{
            //         if($susp->e_state == "送地檢"){
            //             $table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" type="text" >
            //             <option selected="selected" value="送地檢">送地檢</option><option value="待銷毀">待銷毀</option><option value="已銷毀">已銷毀</option>
            //             </select>';
            //         }
            //         if($susp->e_state == "待銷毀"){
            //             $table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" type="text" >
            //             <option value="送地檢">送地檢</option><option selected="selected" value="待銷毀">待銷毀</option><option value="已銷毀">已銷毀</option>
            //             </select>';
            //         }
            //         if($susp->e_state == "已銷毀"){
            //             $table_row[] = '<input id="e_state" readonly name="e_state['.$susp->e_id.']" type="text" value="已銷毀"/> ';
            //         }
            //     }
            // }
            // else $table_row[] = $susp->e_state;
			switch ($type) {
				case 'keep':
					$table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" class="form-control" onchange="updateStatus(\''.$susp->e_id.'\', $(this))">
					<option value="單位保管中" selected="selected">單位保管中【未驗】</option><option value="送驗中">送驗中</option><option value="領回">領回，單位保管中【已驗】</option></select>';
					break;
				case 'process':
					$table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" class="form-control" onchange="updateStatus(\''.$susp->e_id.'\', $(this))">
					<option value="單位保管中">單位保管中【未驗】</option><option value="送驗中"  selected="selected">送驗中</option><option value="領回">領回，單位保管中【已驗】</option><option value="送地檢">送地檢署(法院)</option></select>';
					break;				
				case 'inlocal':
					$table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" class="form-control" onchange="updateStatus(\''.$susp->e_id.'\', $(this))"><option value="單位保管中">單位保管中【未驗】</option><option value="送驗中"  >送驗中</option><option value="送地檢">送地檢署(法院)</option><option value="領回" selected="selected">領回，單位保管中【已驗】</option><option value="沒入物室">沒入物室</option></select>';
					break;
				case 'check':
					$table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" class="form-control" onchange="updateStatus(\''.$susp->e_id.'\', $(this))">
					<option value="單位保管中">單位保管中</option><option value="送驗中" >送驗中</option><option  value="送地檢" selected="selected">送地檢</option><option value="沒入物室">沒入物室</option></select>'.((isset($susp->e_state_doc))?anchor_popup('drugdoc/' . $susp->e_state_doc, '地檢入庫清單'):'');
					break;
				case 'indrug':
					$table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" class="form-control" onchange="updateStatus(\''.$susp->e_id.'\', $(this))">
					<option value="單位保管中">單位保管中</option><option value="送驗中" >送驗中</option><option  value="送地檢" selected="selected">送地檢</option><option value="送警察局" selected="selected">送警察局</option><option value="沒入物室">沒入物室</option><option value="已銷毀">已銷毀</option></select> ';
					break;
				case 'receive':
					$table_row[] =  '<select id="e_state" name="e_state['.$susp->e_id.']" class="form-control" onchange="updateStatus(\''.$susp->e_id.'\', $(this))">
					<option value="單位保管中">單位保管中</option><option value="送驗中">送驗中</option><option  value="送地檢">送地檢</option><option value="沒入物室"  selected="selected">沒入物室</option><option value="已銷毀">已銷毀</option></select> <span class="text-primary">警局已沒入</span>';
					break;
				case 'clean':
					$table_row[] =  '<span class="text-danger">已銷毀</span>';
					break;
			}
			
            // if(preg_match("/f/i", $this->session-> userdata('3permit'))) {
            //     if($susp->e_place == null){
            //         if($susp->e_state == "送地檢"){
            //             if($susp->e_place ==null){
            //                 $table_row[] =  '<select id="e_place" name="e_place['.$susp->e_id.']" type="text" >
            //                 <option>臺灣臺北地方檢察署</option><option>臺灣新北地方檢察署</option><option>臺灣士林地方檢察署</option><option>臺灣宜蘭地方檢察署</option><option>臺灣桃園地方檢察署</option>
            //                 <option>臺灣基隆地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣彰化地方法院檢察署</option><option>臺灣雲林地方檢察署</option><option>臺灣嘉義地方檢察署</option>
            //                 <option>臺灣花蓮地方檢察署</option><option>臺灣苗栗地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣臺南地方法院檢察署</option><option>臺灣南投地方法院埔里簡易庭</option>
            //                 </select>';
            //             }else{
            //                  $table_row[] = $susp->e_place;
            //             }
            //         }
            //         if($susp->e_state == "待銷毀"){
            //             if($susp->e_place !="市警局"){
            //                 $table_row[] =  $susp->e_place;
            //             }
            //             else{
            //                 $table_row[] =  '市警局';
            //             }
            //         }
            //         if($susp->e_state == "已銷毀"){
            //             $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="已銷毀"/> ';
            //         }
            //         else{
            //         $table_row[] =  '確認修改狀態後可選擇';
            //         }
            //     }
            //     else{
            //         if($susp->e_state == "送地檢"){
            //             if(mb_substr($susp->e_place, -1) !='署'){
            //                 $table_row[] =  '<select id="e_place" name="e_place['.$susp->e_id.']" type="text" >
            //                 <option>臺灣臺北地方檢察署</option><option>臺灣新北地方檢察署</option><option>臺灣士林地方檢察署</option><option>臺灣宜蘭地方檢察署</option><option>臺灣桃園地方檢察署</option>
            //                 <option>臺灣基隆地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣彰化地方法院檢察署</option><option>臺灣雲林地方檢察署</option><option>臺灣嘉義地方檢察署</option>
            //                 <option>臺灣花蓮地方檢察署</option><option>臺灣苗栗地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣臺南地方法院檢察署</option><option>臺灣南投地方法院埔里簡易庭</option>
            //                 </select>';
            //             }else if(mb_substr($susp->e_place, -1) =='署'){
            //                  $table_row[] = $susp->e_place;
            //             }
            //         }
            //         if($susp->e_state == "待銷毀"){
            //             if($susp->e_place !="市警局"){
            //             $table_row[] = '<select id="e_place" name="e_place['.$susp->e_id.']" type="text" >
            //                 <option value='.$susp->e_place.'>'.$susp->e_place.'</option><option value="市警局">警局入庫</option>
            //                 </select>';
            //             }
            //             else{
            //             $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="市警局"/> ';
            //             }
            //         }
            //         if($susp->e_state == "已銷毀"){
            //             $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="已銷毀"/> ';
            //         }
            //     }
            // }
            // else $table_row[] = $susp->e_place;            
            // if($susp->e_doc == null){
            //     $table_row[] = '<strong>未上傳</strong>';
            // }
            // else{
            //     $table_row[] = anchor_popup('drugdoc/' . $susp->e_doc, '毒報');
            // }
			if($susp->e_place == null){
                if($susp->e_state == "待送驗"){
                    $table_row[] =  $susp->r_office;
                }
                else if($susp->e_state == "送驗中"){
                    if($susp->e_place =="航醫中心"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option selected="selected">航醫中心</option><option>刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    if($susp->e_place =="刑事局"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option selected="selected">刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    if($susp->e_place =="調查局"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option selected="selected">調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    if($susp->e_place == null){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option>調查局</option><option  selected="selected">鑑識中心</option>
                        </select>';
                    }
                    else{
                        $table_row[] =  '<select id="e_state class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option>調查局</option><option  selected="selected">鑑識中心</option>
                        </select>';
                    }
                }
                else if($susp->e_state == "待裁罰"){
                    $table_row[] =  $susp->r_office;
                }
                else if($susp->e_state == "送地檢"){
                    // if($susp->e_place ==null){
                        // $table_row[] =  '<select id="e_place" class="e_place form-control" name="e_place['.$susp->e_id.']" type="text" data-value="'. $susp->e_place .'"><option>臺灣臺北地方檢察署</option><option>臺灣新北地方檢察署</option><option>臺灣士林地方檢察署</option><option>臺灣宜蘭地方檢察署</option><option>臺灣桃園地方檢察署</option><option>臺灣基隆地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣彰化地方法院檢察署</option><option>臺灣雲林地方檢察署</option><option>臺灣嘉義地方檢察署</option><option>臺灣花蓮地方檢察署</option><option>臺灣苗栗地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣臺南地方法院檢察署</option><option>臺灣南投地方法院埔里簡易庭</option></select>';
						$table_row[] = '<input type="text" class=" form-control" name="e_place['.$susp->e_id.']" value="'.$susp->e_place.'"/>';
                    // }else{
                    //      $table_row[] = $susp->e_place;
                    // }
                }
                else if($susp->e_state == "待銷毀"){
                    if($susp->e_place !="市警局"){
                        $table_row[] =  $susp->r_office;
                    }
                    else if($susp->e_place !="市警局"){
                        $table_row[] =  '市警局';
                    }
                }
                else{
                $table_row[] =  '刑案證物室';
                }
            }
            else{
                if($susp->e_state == "待送驗"){
                    $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="'.$office.'"/> ';
                }
                else if($susp->e_state == "送驗中"){
                    if($susp->e_place =="航醫中心"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option selected="selected">航醫中心</option><option>刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    else if($susp->e_place =="刑事局"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option selected="selected">刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    else if($susp->e_place =="調查局"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option selected="selected">調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                    else if($susp->e_place =="鑑識中心"){
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option>調查局</option><option selected="selected">鑑識中心</option>
                        </select>';
                    }
                    else {
                        $table_row[] =  '<select id="e_state" class="form-control" name="e_place['.$susp->e_id.']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option>調查局</option><option>鑑識中心</option>
                        </select>';
                    }
                }
                else if($susp->e_state == "待裁罰"){
                    $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="'.$susp->r_office.'"/> ';
                }
                else if($susp->e_state == "送地檢"){
                    // if(mb_substr($susp->e_place, -1) !='署'){
                        // $table_row[] =  '<select id="e_place" class="e_place form-control" name="e_place['.$susp->e_id.']" type="text" data-value="'.$susp->e_place.'">
                        // <option>臺灣臺北地方檢察署</option><option>臺灣新北地方檢察署</option><option>臺灣士林地方檢察署</option><option>臺灣宜蘭地方檢察署</option><option>臺灣桃園地方檢察署</option>
                        // <option>臺灣基隆地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣彰化地方法院檢察署</option><option>臺灣雲林地方檢察署</option><option>臺灣嘉義地方檢察署</option>
                        // <option>臺灣花蓮地方檢察署</option><option>臺灣苗栗地方檢察署</option><option>臺灣新竹地方檢察署</option><option>臺灣臺南地方法院檢察署</option><option>臺灣南投地方法院埔里簡易庭</option>
                        // </select>';
						$table_row[] = '<input type="text" class=" form-control" name="e_place['.$susp->e_id.']" value="'.$susp->e_place.'"/>';
                    // }else if(mb_substr($susp->e_place, -1) =='署'){
                        //  $table_row[] = $susp->e_place;
                    // }
                }
                else if($susp->e_state == "待銷毀"){
                    if($susp->e_place !="市警局"){
                    $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="'.$susp->r_office.'"/> ';
                    }
                    else if($susp->e_place =="市警局"){
                    $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="市警局"/> ';
                    }
                    else{
                        $table_row[] = '<input id="e_state" readonly name="e_place['.$susp->e_id.']" type="text" value="'.$susp->r_office.'"/> ';
                    }
                }
                else{
                    $table_row[] =  '刑案證物室';
                }
            }
			$table_row[] = $susp->r_office;
			if($susp->drug_doc == null){
                $table_row[] = '<strong>未上傳</strong>';
            }
            else{
                $table_row[] = anchor_popup('drugdoc/' . $susp->drug_doc, '毒報');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_evi_rec($id) {//裁罰 列表
        $this->load->library('table');
        $query = $this->getsqlmod->getdrugRecAll()->result(); // 使用getsqlmod裡的getdata功能
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '證物編號','證物內容','證物狀態','證物地點','異動單位', '最後修改者','異動時間');
        $table_row = array();
        $i=0;
        foreach ($query as $drug_rec)
        {
            $table_row = NULL;
            $table_row[] = $drug_rec->drug_rec_eid;
			$table_row[] = $drug_rec->e_name;
            $table_row[] = $drug_rec->drug_rec_estatus;
            $table_row[] = $drug_rec->drug_rec_eplace;
			$table_row[] = $drug_rec->drug_rec_office;
            $table_row[] = $drug_rec->drug_rec_empno;
            $table_row[] = $this->tranfer2RCyearTrad2($drug_rec->drug_rec_time);
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    public function editevistatus(){
        //var_dump($_POST);
        $this->load->helper('form');
        $data['title'] = "案件處理系統:待裁罰列表";
        $user = $this -> session -> userdata('uic');
        $username = $this -> session -> userdata('uname');
        $office = $this -> session -> userdata('uoffice');
        $data['include'] = 'cases2/3caseslist';
        $data['nav'] = 'navbar2';
        $taiwan_date = date('Y')-1911; 
        $now = date('mdGi'); 
        $rp_num = $taiwan_date.$now;
        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $enum =mb_split(",",$_POST['enum']);
        //var_dump($enum);
        foreach ($enum as $key => $value) {
        if(isset($_POST['e_state'][$value])){
            if(isset($_POST['e_place'][$value])){
                if($_POST['e_state'][$value]=="待銷毀"){
                    if($_POST['e_place'][$value]=="市警局"){
                        $data = array(
                           'e_state' => $_POST['e_state'][$value],
                           'e_place' => "市警局",
                        );
                        $data2 = array(
                           'drug_rec_eid' => $value,
                           'drug_rec_empno' => $username,
                           'drug_rec_estatus' => $_POST['e_state'][$value],
                           'drug_rec_eplace' => "市警局",
                           'drug_rec_office' => $office,
                        );
                    }
                    else{
                        $data = array(
                           'e_state' => $_POST['e_state'][$value],
                           'e_place' => $office,
                        );
                        $data2 = array(
                           'drug_rec_eid' => $value,
                           'drug_rec_empno' => $username,
                           'drug_rec_estatus' => $_POST['e_state'][$value],
                           'drug_rec_eplace' => $office,
                           'drug_rec_office' => $office,
                        );
                    }
                }
                if(!isset($_POST['e_state'][$value])){
                    $data = array(
                       'e_place' => $_POST['e_place'][$value],
                        );
                    $data2 = array(
                       'drug_rec_eid' => $value,
                       'drug_rec_empno' => $username,
                       'drug_rec_eplace' => $_POST['e_place'][$value],
                       'drug_rec_office' => $office,
                        );
                }
                else{
                    $data = array(
                       'e_state' => $_POST['e_state'][$value],
                       'e_place' => $_POST['e_place'][$value],
                        );
                    $data2 = array(
                       'drug_rec_eid' => $value,
                       'drug_rec_empno' => $username,
                       'drug_rec_estatus' => $_POST['e_state'][$value],
                       'drug_rec_eplace' => $_POST['e_place'][$value],
                        'drug_rec_office' => $office,
                        );
                }
            }
            else{
                $data = array(
                   'e_state' => $_POST['e_state'][$value],
                   'e_place' => $office,
                    );
                $data2 = array(
                   'drug_rec_eid' => $value,
                   'drug_rec_empno' => $username,
                   'drug_rec_estatus' => $_POST['e_state'][$value],
                   'drug_rec_eplace' => $office,
                   'drug_rec_office' => $office,
                    );
            }
            //var_dump($data);
            $this->getsqlmod->updateDrug1($value,$data);
            $this->getsqlmod->adddrugrec($data2);
        }
        };
        redirect('drug_evi_fin/'); 
    }

    public function index() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
		$keep_table = $this->table_evi($table, 'keep');
        $process_table = $this->table_evi($table, 'process');
		$check_table = $this->table_evi($table, 'check');
		$inlocal_table = $this->table_evi($table, 'inlocal');
		$indrug_table = $this->table_evi($table, 'indrug');
		$receive_table = $this->table_evi($table, 'receive');
		$clean_table = $this->table_evi($table, 'clean');
		$data['keep_table'] = $keep_table;
        $data['process_table'] = $process_table;
		$data['check_table'] = $check_table;
		$data['inlocal_table'] = $inlocal_table;
		$data['indrug_table'] = $indrug_table;
		$data['receive_table'] = $receive_table;
		$data['clean_table'] = $clean_table;
        $data['title'] = "證物出入庫";
        if(preg_match("/f/i", $this->session-> userdata('3permit'))) $data['include'] = 'evi/evilist';
        else $data['include'] = 'evi_query/evilist';
        $data['user'] = $this -> session -> userdata('uic');
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    public function listevirec() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $test_table = $this->table_evi_rec($table);
        $data['s_table'] = $test_table;
        $data['title'] = "出入庫日誌記錄";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/f/i", $this->session-> userdata('3permit'))) $data['include'] = 'evi/evilistrec';
        else $data['include'] = 'evi_query/evilistrec';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function exportDrugCSV(){
        $this->load->dbutil();
        $this->load->helper('download');
        $filename = '毒品證物'.date('Ymd').'.csv';
        $query = $this->getsqlmod->getdrugRecAll_Dowload(); 
        $data = $this->dbutil->csv_from_result($query);
        force_download($filename, "\xEF\xBB\xBF" . $data);
    }

	public function updateDrugStatus(){
		$e_id = $_POST['eid'];
		$e_state = $_POST['estatus'];

		if(!empty($_FILES['estatusdoc']['name'])){
			$_FILES['estatusdoc']['name']  = $e_id . $_FILES['estatusdoc']['name'];
			$config['upload_path']          = 'drugdoc/';
			$config['allowed_types']        = 'pdf';
			$config['max_size']             = 100000;
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('estatusdoc')){
				$error = array('error' => $this->upload->display_errors());
				//redirect('cases2/editCases2/' . $_POST['s_cnum'],'refresh'); 
			}
			else{
				$uploadData = $this->upload->data();
				$_POST['estatusdoc']=$uploadData['file_name'];

			}
			$updatedata = array(
				'e_state' => $e_state,
				'e_state_doc' => $_POST['estatusdoc'],
				'e_place' => (isset($_POST['eplace']))?$_POST['eplace']:null
			);
		}
		else
		{
			$updatedata = array(
				'e_state' => $e_state,
				'e_place' => (isset($_POST['eplace']))?$_POST['eplace']:null
			);
		}
		$this->getsqlmod->updateDrug1($e_id, $updatedata);
		echo 'ok';
	}
	/** 轉民國年 中文年-月-日 時:分:秒 */
	function tranfer2RCyearTrad2($datetime)
	{
		$date = substr($datetime, 0, 10);
		$date = str_replace('-', '', $date);
		$rc = ((int)substr($date, 0, 4)) - 1911;
		return (string)$rc . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2)  . ' ' . substr($datetime, 11, 19) ;
	}
}
