<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Cases extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this -> load -> library('Session/session');
        $this->load->helper('url');
        $this->load->helper('html');
    }    
    function table_cases() {
        $this->load->library('table');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $query = $this->getsqlmod->getCaseslist1($this -> session -> userdata('uroffice')); // 改成session 派出所/分局
        $tmpl = array (
            'table_open' => '<table border="0"  class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '案件編號', '查獲時間', '查獲地點',
            '偵破過程', '查獲單位', '收案單位', '犯嫌人' , ' 戶役政/筆錄/搜扣','其他檔案(逮捕通知書/拘票/同意書)','');
        $table_row = array();
        foreach ($query->result() as $cases)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases/edit/' . $cases->c_num, $cases->c_num);
            if($cases->s_date==null){
                $table_row[] = '<strong>未輸入日期</strong>';
            }
            else{
                $table_row[] = $cases->s_date;
            }
            if($cases->r_county==null ||$cases->r_district==null ||$cases->r_zipcode==null ||$cases->r_address==null){
                $table_row[] = '<strong>未輸入地點</strong>';
            }
            else{
                $table_row[] = ($cases->r_county . $cases->r_district  . $cases->r_zipcode  . $cases->r_address);
            }
            if($cases->detection_process==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $cases->detection_process;
            }
            $table_row[] = $cases->s_office;
            if($cases->r_office==null){
                $table_row[] = '<strong>未選選</strong>';
            }
            else{
                $table_row[] = $cases->r_office;
            }
            if($cases->s_name==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $cases->s_name;
            }
            if($cases->doc_file==null){
                $table_row[] = '<strong>未上傳文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('uploads/' . $cases->doc_file, '戶役政/筆錄/搜扣');
            }         
            if($cases->other_doc==null){
                $table_row[] = '<strong>未上傳文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('uploads/' . $cases->other_doc, '其他檔案(逮捕通知書/拘票/同意書)');
            }
            $table_row[] = anchor('cases/delcases/' . $cases->c_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_drug1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Drug($id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '毒品編號', '數量', '毒品名稱','初驗淨重(g)', '毒品級別', '毒品成分', '擁有人', '');
        $table_row = array();
        foreach ($query->result() as $drug)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases/editdrug1/' . $drug->e_id, $drug->e_id);
            if($drug->e_type == null){
                $table_row[] = '<strong>未輸入日期</strong>';
            }
            else{
                $table_row[] = $drug->e_type;
            }
            if($drug->e_name==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $drug->e_name;
            }
            $table_row[] = $drug->e_1_N_W;
            if($drug->df_level == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug->df_level;
            }
            if($drug->df_ingredient == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug->df_ingredient;
            }
            if($drug->e_suspect == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $drug->e_suspect;
            }
            $table_row[] = anchor('cases/deldrug/' . $drug->e_id, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_susp1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Susp($id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '身份證', '犯嫌人姓名', '犯罪手法','照片','');
        $table_row = array();
        foreach ($query->result() as $susp)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases/editsusp1/' . $susp->s_num, $susp->s_ic);
            if($susp->s_name == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $susp->s_name;
            }
            if($susp->s_CMethods==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $susp->s_CMethods;
            }
            if($susp->s_pic == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = anchor_popup('1susppic/' . $susp->s_pic, '犯嫌人照片');
            }
            $table_row[] = anchor('cases/delsusp/' . $susp->s_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_car1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->get1Car('v_s_num',$id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '車牌號碼', '交通工具種類', '顏色','');
        $table_row = array();
        foreach ($query->result() as $car)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases/editcar/' . $car->v_num, $car->v_license_no);
            if($car->v_type == null){
                $table_row[] = '<strong>未輸入姓名</strong>';
            }
            else{
                $table_row[] = $car->v_type;
            }
            if($car->v_color==null){
                $table_row[] = '<strong>未選擇</strong>';
            }
            else{
                $table_row[] = $car->v_color;
            }
            $table_row[] = anchor('cases/delcar/' . $car->v_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    function table_social($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getSocial('s_num',$id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '社群軟體名稱', '社群帳號1', '社群帳號2','社群帳號3','');
        $table_row = array();
        foreach ($query->result() as $social)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases/editsocialmedia/' . $social->social_media_num, $social->social_media_name);
            if($social->social_media_ac1 == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $social->social_media_ac1;
            }
            if($social->social_media_ac2==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $social->social_media_ac2;
            }
            if($social->social_media_ac3==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $social->social_media_ac3;
            }
            $table_row[] = anchor('cases/delsocial/' . $social->social_media_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_phone($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getPhone('p_s_num',$id); // 使用getsqlmod裡的getdata功能
        //var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '手機號碼', '手機序號(IMEI)', '查扣手機','');
        $table_row = array();
        foreach ($query->result() as $phone)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases/editphone/' . $phone->p_num, $phone->p_no);
            if($phone->p_imei == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $phone->p_imei;
            }
            if($phone->p_conf==null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $phone->p_conf;
            }
            $table_row[] = anchor('cases/delphone/' . $phone->p_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_source($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getSource1('sou_s_num',$id);
        //var_dump($query->result());
        //echo $id;
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '姓名', '手機號碼', '交易時間', '交易地點', '交易金額', '交易數量','');
        $table_row = array();
        foreach ($query->result() as $source)
        {
            $table_row = NULL;
            $table_row[] = anchor('cases/editsource/' . $source->sou_num, $source->sou_name);
            if($source->sou_phone == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_phone;
            }
            if($source->sou_time == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_time;
            }
            if($source->sou_place == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_place;
            }
            if($source->sou_amount == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_amount;
            }
            if($source->sou_count == null){
                $table_row[] = '<strong>未輸入</strong>';
            }
            else{
                $table_row[] = $source->sou_count;
            }
            $table_row[] = anchor('cases/delsource/' . $source->sou_num, '刪除');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    public function listCases() {
       // generate HTML table from query results
        $this->load->helper('form');
        $test_table = $this->table_cases();
        $data['data_table'] = $test_table;
        $data['title'] = "案件處理系統:列表";
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/cases_list';
        $data['nav'] = 'navbar1_list';
        $this->load->view('template1', $data);
    }

    public function createcases_num() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $query = $this->getsqlmod->getdata(1,1)->result();
        $countid = $query[0]->c_num;
        $lastNum = (int)mb_substr($countid, -4, 4);
        $lastNum++;
        $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $data['c_num']='CN' . $taiwan_date.$value;
        $_POST['s_office']=$this -> session -> userdata('uroffice');
        $_POST['r_office']=$this -> session -> userdata('uoffice');
        $_POST['r_office1']=$this -> getsqlmod -> r_office1($_POST['r_office']);
        $_POST['c_num']=$data['c_num'];
        $_POST['e_ed_empno']=$data['c_num'];
        //var_dump($_POST);
        //$_POST['rm_num']="12312342";
        $this->getsqlmod->addcases($_POST);
        redirect('cases/newcases/' . $data['c_num'],'refresh'); 
    }
    public function newcases() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        $data['row'] = $casesrepair[0];
        $data['c_num']=$data['row']->c_num;
        $data['s_office']=$data['row']->s_office;
        $data['title'] = "案件處理系統:新增";
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        $data['drug_table'] = $this->table_drug1($id);
        $data['s_table'] = $this->table_susp1($id);
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_cases';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    public function createcases() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('table');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $casesrepair = $this->getsqlmod->getCases($_POST['c_num'])->result();
        $data['drug_table'] = $this->table_drug1($_POST['c_num']);
        $data['s_table'] = $this->table_susp1($_POST['c_num']);
        $data['row'] = $casesrepair[0];
        $data['c_num']=$data['row']->c_num;
        $data['s_office']=$data['row']->s_office;
        $link = $_POST['link'];
        $config = array(
            array(
                'field'  => 'c_num',
                'label'  => '案件編號',
                'rules'  => 'required'
            ),
            array(
                'field'  => 'r_office',
                'label'  => '收案單位',
                'rules'  => 'required'
            ),
            array(
                'field'  => 's_date',
                'label'  => '查獲日期',
                'rules'  => 'required'
            ),
            array(
                'field'  => 'r_zipcode',
                'label'  => 'zip',
                'rules'  => 'required'
            ),
            array(
                'field'  => 'r_address',
                'label'  => '地址',
                'rules'  => 'required'
            ),
        );        
        $this->form_validation->set_rules($config);
        /*if (empty($_FILES['files']['name'][0])){
            $this->form_validation->set_rules('files', 'Document', 'required');
        }   
        if (empty($_FILES['files']['name'][1])){
            $this->form_validation->set_rules('files', 'Document', 'required');
        } */  
        $this->form_validation->set_message('required', '!必須填寫');   
        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');   

        if ($this->form_validation->run() == FALSE){
        //提交失敗 重新載入表單部分
            $data['title'] = "案件處理系統:新增案件";
            $data['nav'] = 'navbar1';
            $data['user'] = $this -> session -> userdata('uic');
            $data['include'] = 'cases/new_cases';
            $this->load->view('template', $data);
            redirect('cases/newcases/' . $data['row']->c_num,'refresh'); 
        }
        else
        {
            $countfiles = count($_FILES['files']['name']);
            if(!empty($_FILES['files']['name'][0])&&!empty($_FILES['files']['name'][1])){
                // Looping all files
                for($i=0;$i<$countfiles;$i++){
                    if(!empty($_FILES['files']['name'][$i])){
                        // Define new $_FILES array - $_FILES['file']
                        $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                        $config['upload_path'] = 'uploads/'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx';
                        $config['max_size'] = '5000'; // max_size in kb
                        //$config['file_name'] = $_FILES['file']['name'];
                        //Load upload library
                        $this->load->library('upload',$config); 
                        // File upload
                        if($this->upload->do_upload('file')){
                            $uploadData = $this->upload->data();
                            //$filename = $uploadData['full_path'];
                            $filename = $uploadData['file_name'];
                            echo $filename;
                            $data['filenames'][] = $filename;
                        }
                    }
                }
                if(!empty($_FILES['drugpic']['name'])){
                    $this->load->library('upload', $config);
                    if ( ! $this->upload->do_upload('drugpic')){
                        $error = array('error' => $this->upload->display_errors());
                        //var_dump($error);
                        redirect($link,'refresh'); 
                    }
                    else{
                        $_FILES['drugpic']['name'] = $_POST['c_num'] . $_FILES['drugpic']['name'][$i];
                        $uploadData = $this->upload->data();
                        $_POST['r_address'] = strtoupper($_POST['r_address']);
                        $_POST['s_place'] = strtoupper($_POST['s_place']);
                        $_POST['rm_num']="12312342";
                        $_POST['doc_file']=$data['filenames']['0'];
                        $_POST['other_doc']=$data['filenames']['1'];
                        $_POST['drug_pic']=$uploadData['file_name'];
                        unset($_POST['link']);
                        //var_dump($_POST);
                        $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                        redirect($link,'refresh'); 
                    }
                }
                else{
                    $uploadData = $this->upload->data();
                    $_POST['r_address'] = strtoupper($_POST['r_address']);
                    $_POST['s_place'] = strtoupper($_POST['s_place']);
                    $_POST['rm_num']="12312342";
                    $_POST['doc_file']=$data['filenames']['0'];
                    $_POST['other_doc']=$data['filenames']['1'];
                    $_POST['drug_pic']=null;
                    unset($_POST['link']);
                    $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                    redirect($link,'refresh'); 
                }
            }
            else if(empty($_FILES['files']['name'][0])&&!empty($_FILES['files']['name'][1])){
                // Looping all files
                for($i=0;$i<$countfiles;$i++){
                    if(!empty($_FILES['files']['name'][$i])){
                        // Define new $_FILES array - $_FILES['file']
                        $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                        $config['upload_path'] = 'uploads/'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|txt';
                        $config['max_size'] = '5000'; // max_size in kb
                        //$config['file_name'] = $_FILES['file']['name'];
                        //Load upload library
                        $this->load->library('upload',$config); 
                        // File upload
                        if($this->upload->do_upload('file')){
                            $uploadData = $this->upload->data();
                            //$filename = $uploadData['full_path'];
                            $filename = $uploadData['file_name'];
                            $data['filenames'][] = $filename;
                        }
                    }
                }
                if(!empty($_FILES['drugpic'])){
                    $_FILES['drugpic']['name'] = $_POST['c_num'] . $_FILES['drugpic']['name'];
                    $config['upload_path']          = 'uploads/';
                    $config['allowed_types']        = 'gif|jpg|png|pdf';
                    $config['max_size']             = 10000;
                    $this->load->library('upload', $config);
                    if ( ! $this->upload->do_upload('drugpic')){
                        $error = array('error' => $this->upload->display_errors());
                        var_dump($error);
                        redirect($link,'refresh'); 
                    }
                    else{
                        $uploadData = $this->upload->data();
                        $_POST['r_address'] = strtoupper($_POST['r_address']);
                        $_POST['s_place'] = strtoupper($_POST['s_place']);
                        $_POST['rm_num']="12312342";
                        $_POST['doc_file']=$data['filenames']['0'];
                        $_POST['other_doc']=$data['filenames']['1'];
                        $_POST['drug_pic']=$uploadData['file_name'];
                        unset($_POST['link']);
                        $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                        redirect($link,'refresh'); 
                    }
                }
                else{
                    $uploadData = $this->upload->data();
                    $_POST['r_address'] = strtoupper($_POST['r_address']);
                    $_POST['s_place'] = strtoupper($_POST['s_place']);
                    $_POST['rm_num']="12312342";
                    $_POST['doc_file']=$data['filenames']['0'];
                    $_POST['other_doc']=$data['filenames']['1'];
                    $_POST['drug_pic']=null;
                    unset($_POST['link']);
                    $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                    redirect($link,'refresh'); 
                }
            }
            else if(!empty($_FILES['files']['name'][0]) && empty($_FILES['files']['name'][1])){
                // Looping all files
                for($i=0;$i<$countfiles;$i++){
                    if(!empty($_FILES['files']['name'][$i])){
                        // Define new $_FILES array - $_FILES['file']
                        $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                        $config['upload_path'] = 'uploads/'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|txt';
                        $config['max_size'] = '5000'; // max_size in kb
                        //$config['file_name'] = $_FILES['file']['name'];
                        //Load upload library
                        $this->load->library('upload',$config); 
                        // File upload
                        if($this->upload->do_upload('file')){
                            $uploadData = $this->upload->data();
                            //$filename = $uploadData['full_path'];
                            $filename = $uploadData['file_name'];
                            $data['filenames'][] = $filename;
                        }
                    }
                }
                if(!empty($_FILES['drugpic'])){
                    $_FILES['drugpic']['name'] = $_POST['c_num'] . $_FILES['drugpic']['name'];
                    $config['upload_path']          = 'uploads/';
                    $config['allowed_types']        = 'gif|jpg|png';
                    $config['max_size']             = 10000;
                    $this->load->library('upload', $config);
                    if ( ! $this->upload->do_upload('drugpic')){
                        $error = array('error' => $this->upload->display_errors());
                        var_dump($error);
                        //redirect($link,'refresh'); 
                    }
                    else{
                        $uploadData = $this->upload->data();
                        $_POST['r_address'] = strtoupper($_POST['r_address']);
                        $_POST['s_place'] = strtoupper($_POST['s_place']);
                        $_POST['rm_num']="12312342";
                        $_POST['doc_file']=$data['filenames']['0'];
                        $_POST['other_doc']=$data['filenames']['1'];
                        $_POST['drug_pic']=$uploadData['file_name'];
                        unset($_POST['link']);
                        $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                        redirect($link,'refresh'); 
                    }
                }
                else{
                        $uploadData = $this->upload->data();
                        $_POST['r_address'] = strtoupper($_POST['r_address']);
                        $_POST['s_place'] = strtoupper($_POST['s_place']);
                        $_POST['rm_num']="12312342";
                        $_POST['doc_file']=$data['filenames']['0'];
                        $_POST['other_doc']=$data['filenames']['1'];
                        $_POST['drug_pic']=null;
                        unset($_POST['link']);
                        $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                        redirect($link,'refresh'); 
                }
            }
            else{
                if(!empty($_FILES['drugpic'])){
                    $_FILES['drugpic']['name'] = $_POST['c_num'] . $_FILES['drugpic']['name'];
                    $config['upload_path']          = 'uploads/';
                    $config['allowed_types']        = 'gif|jpg|png';
                    $config['max_size']             = 10000;
                    $this->load->library('upload', $config);
                    if ( ! $this->upload->do_upload('drugpic')){
                        $error = array('error' => $this->upload->display_errors());
                        var_dump($error);
                        redirect($link,'refresh'); 
                    }
                    else{
                        $uploadData = $this->upload->data();
                        $_POST['r_address'] = strtoupper($_POST['r_address']);
                        $_POST['s_place'] = strtoupper($_POST['s_place']);
                        $_POST['rm_num']="12312342";
                        $_POST['doc_file']=null;
                        $_POST['other_doc']=null;
                        $_POST['drug_pic']=$uploadData['file_name'];
                        unset($_POST['link']);
                        $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                        redirect($link,'refresh'); 
                    }
                }
                else{
                        $uploadData = $this->upload->data();
                        $_POST['r_address'] = strtoupper($_POST['r_address']);
                        $_POST['s_place'] = strtoupper($_POST['s_place']);
                        $_POST['rm_num']="12312342";
                        $_POST['doc_file']=null;
                        $_POST['other_doc']=null;
                        $_POST['drug_pic']=null;
                        unset($_POST['link']);
                        $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                        redirect($link,'refresh'); 
                }
            }
        }
    }
    
    public function newsuspect() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        //$casesrepair = $this->getsqlmod->getCases($id)->result();
        $data['s_cnum']=$id;
        $data['title'] = "案件處理系統:新增犯嫌人";
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_suspect';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    public function create_suspect1() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        /*$data['title'] = "案件處理系統";
        $data['user'] = $this -> session -> userdata('uic');
        $data['nav'] = 'navbar1';
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_drug';*/
        $link=$_POST['link'];
        $_POST['s_roffice'] = $this->session-> userdata('uroffice');;
        unset($_POST['link']);
        //var_dump($_POST);
            //redirect($link,'refresh'); 
        $_POST['s_CMethods'] = implode(',',$_POST['s_CMethods']); 
        if(!empty($_FILES['susppic']['name'])){
            $_FILES['susppic']['name'] = $_POST['s_cnum'] . $_FILES['susppic']['name'][$i];
            $config['upload_path']          = '1susppic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('susppic')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                redirect($link,'refresh'); 
            }
            else{
                //$this->getsqlmod->addsuspect($_POST);
                $uploadData = $this->upload->data();
                $_POST['s_pic']=$uploadData['file_name'];
                $this->getsqlmod->addsuspect($_POST);
                redirect($link,'refresh'); 
                //redirect('cases/edit/' . $_POST['s_cnum'],'refresh'); 
            }
        }
        else{
            $_POST['s_pic']=Null;
            //var_dump($_POST);
            $this->getsqlmod->addsuspect($_POST);
            redirect($link,'refresh'); 
        }
    }

    function edit(){
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $casesrepair = $this->getsqlmod->getCases($id)->result();
        //echo $casesrepair[1]->c_num;
        $type_options = array(
            '中正第一分局' => '中正第一分局' ,
            '中正第二分局' => '中正第二分局',
            '文山第一分局' => '文山第一分局',
            '文山第二分局' => '文山第二分局',
            '信義分局' => '信義分局',
            '大安分局' => '大安分局',
            '中山分局' => '中山分局',
            '松山分局' => '松山分局',
            '大同分局' => '大同分局',
            '萬華分局' => '萬華分局',
            '南港分局' => '南港分局',
            '內湖分局' => '內湖分局',
            '士林分局' => '士林分局',
            '北投分局' => '北投分局',
            '刑事警察大隊偵一隊' => '刑事警察大隊偵一隊',
            '刑事警察大隊偵二隊' => '刑事警察大隊偵二隊',
            '刑事警察大隊偵三隊' => '刑事警察大隊偵三隊',
            '刑事警察大隊偵四隊' => '刑事警察大隊偵四隊',
            '刑事警察大隊偵五隊' => '刑事警察大隊偵五隊',
            '刑事警察大隊偵七隊' => '刑事警察大隊偵七隊',
            '刑事警察大隊偵八隊' => '刑事警察大隊偵八隊',
            '毒緝中心' => '毒緝中心',
            '科偵隊' => '科偵隊',
            '肅竊組' => '肅竊組',
        );
        $type_options1 = array(
            '路檢攔查' => '路檢攔查' ,
            '臨檢' => '臨檢',
            '專案勤務(無令狀)' => '專案勤務(無令狀)',
            '毒品調驗人口' => '毒品調驗人口',
            '拘提/搜索' => '拘提/搜索',
            '通知到案' => '通知到案',
            '借訊(提)' => '借訊(提)',
            '報案(110)' => '報案(110)',
            '99' => '其他',
        );
        //$test = array($casesrepair[0]->r_office, $casesrepair[0]->r_office);
        $data['row'] = $casesrepair[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['c_num'] = $id;
        $data['title'] = "案件修改:". $id;
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        $data['drug_table'] = $this->table_drug1($id);
        $data['s_table'] = $this->table_susp1($id);
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/cases_edit';
        $this->load->view('template', $data);
    }  
    
    public function new_drug() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $Susp = $this->getsqlmod->get1DrugSusp($id)->result();
        $c_value = substr($id,-3);
        $type_options = array("" => '請選擇');
        foreach ($Susp as $Susp1){
            $type_options[$Susp1->s_ic] = $Susp1->s_name;
        }
        $query = $this->getsqlmod->get1DrugCount()->result(); 
        $countid = $query[0]->e_id;
        $lastNum = (int)mb_substr($countid, -5, 5);
        $lastNum++;
        $value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
        $taiwan_date = date('Y')-1911; 
        $data['e_id']='EN' . $taiwan_date . $value;
        $data['e_c_num']=$id;
        $data['opt']=$type_options;
        $data['e_empno']="鍾中中";
        $data['e_ed_empno']="鍾中中";
        $data['title'] = "案件處理系統:新增毒品證物";
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_drug';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    public function create_drug1() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        //var_dump($_POST);
        echo '<br>';
        foreach ($this->input->post('df') as $key => $value) {
            $dataSet1 = array();
            switch ($value) {
                case '海洛因':
                    $dataSet1[] = array (
                          'df_level' => '1',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '安非他命':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '搖頭丸(MDMA)':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '愷他命(KET)':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '一粒眠':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '大麻':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '卡西酮類':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '古柯鹼':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                default:
                    echo " ";
                    break;
            }
            echo '<br>';
            $this->getsqlmod->adddrug1($dataSet1);
            //var_dump($dataSet1);
        }
        //echo '<br>';
        unset($_POST['df']);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['e_s_ic'])->result();
        $_POST['e_suspect']=$susp[0]->s_name;
        $this->getsqlmod->adddrug($_POST);
        //var_dump($_POST);
        redirect('cases/edit/' . $_POST['e_c_num'],'refresh'); 
    }
    
    function editdrug1(){
        $this->load->library('table');
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $drugAll = $this->getsqlmod->get1DrugAll($id)->result();
        $Susp = $this->getsqlmod->get1DrugSusp($drugAll[0]->e_c_num)->result();
        $drugck = $this->getsqlmod->get1DrugCk($id)->result();
        //echo $casesrepair[1]->c_num;
        $type_options = array(
            '毒品粉末、碎塊' => '毒品粉末、碎塊' ,
            '藥碇' => '藥碇',
            '梅碇' => '梅碇',
            '即溶包' => '即溶包',
            '香菸(含菸蒂)' => '香菸(含菸蒂)',
            '菸草' => '菸草',
            '濾嘴' => '濾嘴',
            '吸食器' => '吸食器',
            '吸管' => '吸管',
            '卡片' => '卡片',
            '括盤' => '括盤',
            '棉花棒' => '棉花棒',
            '殘渣袋' => '殘渣袋',
            '大麻' => '大麻',
            '手機殼' => '手機殼',
            '植株' => '植株',
            '種子' => '種子',
       );   
        $type_options1 = array(
            '包' => '包' ,
            '顆' => '顆',
            '支' => '支',
            '張' => '張',
            '個' => '個',
            '組' => '組',
            '株' => '株',
        );
        $type_options2 = array(
            '海洛因' => '海洛因' ,
            '安非他命' => '安非他命',
            '搖頭丸(MDMA)' => '搖頭丸(MDMA)',
            '愷他命(KET)' => '愷他命(KET)',
            '一粒眠' => '一粒眠',
            '大麻' => '大麻',
            '卡西酮類' => '卡西酮類',
            '古柯鹼' => '古柯鹼',
            '無毒品反應' => '無毒品反應',
        );
        $type_options3 = array('' => '請選擇');
        foreach ($Susp as $Susp1){
            $type_options3[$Susp1->s_ic] = $Susp1->s_name;
        }
        $data['drugAll'] = $drugAll[0];
        $data['drugck'] = $drugck;
        $data['nameopt'] = $type_options;
        $data['countopt'] = $type_options1;
        $data['nwopt'] = $type_options2;
        $data['test'] = $type_options3;
        $data['e_id'] = $id;
        $data['e_c_num'] = $drugAll[0]->e_c_num;
        $data['title'] = "毒品修改:". $id;
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        $data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/drug1_edit';
        $this->load->view('template', $data);
    }  
    
    public function update_drug1() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        //var_dump($_POST);
        echo '<br>';
        foreach ($this->input->post('df') as $key => $value) {
            $dataSet1 = array();
            switch ($value) {
                case '海洛因':
                    $dataSet1[] = array (
                          'df_level' => '1',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '安非他命':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '搖頭丸(MDMA)':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '愷他命(KET)':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '一粒眠':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '大麻':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '卡西酮類':
                    $dataSet1[] = array (
                          'df_level' => '3',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                case '古柯鹼':
                    $dataSet1[] = array (
                          'df_level' => '2',
                          'df_ingredient' => $value,
                          'df_drug' => $_POST['e_id']
                     );
                    break;
                default:
                    echo " ";
                    break;
            }
            echo '<br>';
            $this->getsqlmod->deleteDrugCkfor_Ed($_POST['e_id'],$value);
            $this->getsqlmod->adddrug1($dataSet1);
            //var_dump($dataSet1);
        }
        //echo '<br>';
        unset($_POST['df']);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['e_s_ic'])->result();
        $_POST['e_suspect']=$susp[0]->s_name;
        $this->getsqlmod->updateDrug1($_POST['e_id'],$_POST);
        //var_dump($_POST);
        redirect('cases/edit/' . $_POST['e_c_num'],'refresh'); 
    }
    
    function deleteCk(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $drugck = $this->getsqlmod->get1DrugCkdel($id)->result();
        $this->getsqlmod->deleteDrugCk($id);
        //var_dump ($drugck);
        redirect('cases/editdrug1/' . $drugck[0]->df_drug,'refresh');
    }
    function editsusp1(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        //var_dump ($susp[0]);
        $image_properties = array(
                  'src' => 'img/logo.gif',
                  'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                  'class' => 'post_images',
                  'width' => '50',
                  'height' => '50',
        );     
        $type_options = array(
            '未受教育' => '未受教育' ,
            '國小' => '國小',
            '國中' => '國中',
            '高中' => '高中',
            '大學' => '大學',
            '碩士' => '碩士',
            '博士' => '博士',
        );
        $type_options1 = array(
            ' ' => '請選擇' ,
            '忘記來源' => '忘記來源',
            '否認施用' => '否認施用',
            '國外購入' => '國外購入',
            '6' => '其他',
        );
        $methods = mb_split(",",$susp[0]->s_CMethods);
        $data['持有'] = '';
        $data['意圖販售而持有'] = '';
        $data['販賣'] = '';
        $data['運輸'] = '';
        $data['栽種'] = '';
        $data['施用'] = '';
        $data['製造'] = '';
        $data['轉讓'] = '';
        foreach($methods as $value){
            $data[$value] = $value;
            //echo $data[$value];
        }
        //var_dump($data);
        $data['row'] = $susp[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['img'] = $image_properties;
        $data['s_cnum'] = $id;
        $data['sourcetable'] = $this->table_source($susp[0]->s_num);
        $data['phonetable'] = $this->table_phone($susp[0]->s_num);
        $data['socialtable'] = $this->table_social($susp[0]->s_num);
        $data['cartable'] = $this->table_car1($susp[0]->s_num);
        $data['title'] = "犯嫌人修改:" .$susp[0]->s_name;
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/edit_suspect';
        $this->load->view('template', $data);
    }  
    
    public function createcar() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['s_ic'])->result();
        //echo $susp[0]->s_cnum;
        for($i=0, $count = count($_POST['v_color']);$i<$count;$i++) {
            $dataSet1 = array();
            $dataSet1[] = array (
                'v_type' => $_POST['v_type'][$i],
                'v_license_no' => $_POST['v_license_no'][$i],
                'v_owner ' => $_POST['s_ic'],
                'v_s_num ' => $_POST['s_num'],
                'v_color' => $_POST['v_color'][$i],
            );
            //var_dump($dataSet1);
            $this->getsqlmod->addcar($dataSet1);
        }
        redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    public function newcar() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_num']=$susp[0]->s_num;
        $data['title'] = "案件處理系統:新增車輛";
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_car';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    function editcar(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $car = $this->getsqlmod->get1Car('v_num',$id)->result();
        //var_dump ($susp[0]);
        $type_options = array(
            '機車' => '機車' ,
            '客車' => '客車',
            '貨車' => '貨車',
        );
        $data['row'] = $car[0];
        //$data['test'] = $test;
        $data['options'] = $type_options;
        $data['s_cnum'] = $id;
        $data['title'] = "車輛修改:".$row->v_license_no;
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/car_edit';
        $this->load->view('template', $data);
    }  
    
    public function update_car() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $this->getsqlmod->updateCar($_POST['v_num'],$_POST);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['v_owner'])->result();
        //var_dump($susp[0]);
        redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }

    public function casesUpdate(){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $now = date('Y-m-d H:i:s');
        $link = $_POST['link'];
        $config = array(
            array(
                'field'  => 'c_num',
                'label'  => '案件編號',
                'rules'  => 'required'
            ),
            array(
                'field'  => 'r_office',
                'label'  => '收案單位',
                'rules'  => 'required'
            ),
            array(
                'field'  => 's_date',
                'label'  => '查獲日期',
                'rules'  => 'required'
            ),
            array(
                'field'  => 'r_zipcode',
                'label'  => 'zip',
                'rules'  => 'required'
            ),
            array(
                'field'  => 'r_address',
                'label'  => '地址',
                'rules'  => 'required'
            ),
        );        
        $this->form_validation->set_rules($config);
        $this->form_validation->set_message('required', '!必須填寫');   
        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');   
        if ($this->form_validation->run() == FALSE){
        //提交失敗 重新載入表單部分
            $data['title'] = "案件處理系統";
            $data['nav'] = 'navbar1';
            $data['user'] = $this -> session -> userdata('uic');
            //$data['include'] = 'cases/cases_edit';
            $data['include'] = 'cases/new_cases';
            $this->load->view('template', $data);
            redirect('cases/edit/'.$_POST['c_num'],'refresh'); 
        }
        else
        {
            if(!empty($_FILES['files']['name'][0]) && !empty($_FILES['files']['name'][1])){
                $countfiles = count($_FILES['files']['name']);
                // Looping all files
                for($i=0;$i<$countfiles;$i++){
                    if(!empty($_FILES['files']['name'][$i])){
                        // Define new $_FILES array - $_FILES['file']
                        $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                        $config['upload_path'] = 'uploads/'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
                        $config['max_size'] = '5000'; // max_size in kb
                        //$config['file_name'] = $_FILES['file']['name'];
                        //Load upload library
                        $this->load->library('upload',$config); 
                        // File upload
                        if($this->upload->do_upload('file')){
                            $uploadData = $this->upload->data();
                            //$filename = $uploadData['full_path'];
                            $filename = $uploadData['file_name'];
                            $data['filenames'][] = $filename;
                        }
                    }
                }
                $_POST['e_ed_date']=$now;
                //$_POST['e_ed_empno']=;
                $_POST['doc_file']=$data['filenames']['0'];
                $_POST['other_doc']=$data['filenames']['1'];
                //echo $_POST['c_num'];
                //var_dump($_POST);
                unset($_POST['link']);
                $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                redirect($link,'refresh'); 
            }
            else if(!empty($_FILES['files']['name'][0])&&empty($_FILES['files']['name'][1])){
                $countfiles = count($_FILES['files']['name']);
                // Looping all files
                for($i=0;$i<$countfiles;$i++){
                    if(!empty($_FILES['files']['name'][$i])){
                        // Define new $_FILES array - $_FILES['file']
                        $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                        $config['upload_path'] = 'uploads/'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
                        $config['max_size'] = '5000'; // max_size in kb
                        //$config['file_name'] = $_FILES['file']['name'];
                        //Load upload library
                        $this->load->library('upload',$config); 
                        // File upload
                        if($this->upload->do_upload('file')){
                            $uploadData = $this->upload->data();
                            //$filename = $uploadData['full_path'];
                            $filename = $uploadData['file_name'];
                            $data['filenames'][] = $filename;
                        }
                    }
                }
                //echo $_FILES['files']['name'][0];
                $_POST['e_ed_date']=$now;
                //$_POST['e_ed_empno']=;
                $_POST['doc_file']=$data['filenames']['0'];
                //$_POST['other_doc']=$data['filenames']['1'];
                //echo $_POST['c_num'];
                //var_dump($_POST);
                unset($_POST['link']);
                $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                redirect($link,'refresh'); 
            }
            else if(empty($_FILES['files']['name'][0])&&!empty($_FILES['files']['name'][1])){
                //$countfiles = count($_FILES['files']['name']);
                // Looping all files
                    if(!empty($_FILES['files']['name'][1])){
                        // Define new $_FILES array - $_FILES['file']
                        $_FILES['file']['name'] = $_POST['c_num'] . $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][1];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][1];
                        $_FILES['file']['error'] = $_FILES['files']['error'][1];
                        $_FILES['file']['size'] = $_FILES['files']['size'][1];
                        $config['upload_path'] = 'uploads/'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
                        $config['max_size'] = '5000'; // max_size in kb
                        //$config['file_name'] = $_FILES['file']['name'];
                        //Load upload library
                        $this->load->library('upload',$config); 
                        // File upload
                        if($this->upload->do_upload('file')){
                            $uploadData = $this->upload->data();
                            //$filename = $uploadData['full_path'];
                            $filename = $uploadData['file_name'];
                            $data['filenames'][1] = $filename;
                        }
                    }
                //echo $_FILES['files']['name'][0];
                $_POST['e_ed_date']=$now;
                //$_POST['e_ed_empno']=;
                //$_POST['doc_file']=$data['filenames']['0'];
                $_POST['other_doc']=$data['filenames']['1'];
                //echo $_POST['c_num'];
                //var_dump($_POST);
                unset($_POST['link']);
                $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                redirect($link,'refresh'); 
            }
            else{
                $_POST['e_ed_date']=$now;
                //$_POST['e_ed_empno']=;
                unset($_POST['link']);
                $this->getsqlmod->updateCase($_POST['c_num'],$_POST);
                redirect($link,'refresh'); 
            }
        }
    }
    public function suspect1Update(){
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model('getsqlmod','',TRUE);
        $link = $_POST['link'];
        $_POST['s_CMethods'] = implode(',',$_POST['s_CMethods']); 
        if(!empty($_FILES['susppic']['name'])){
            $_FILES['susppic']['name'] = $_POST['s_cnum'] . $_FILES['susppic']['name'][$i];
            $config['upload_path']          = '1susppic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('susppic')){
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
                //$this->load->view('upload_form', $error);
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['s_pic']=$uploadData['file_name'];
                //$this->getsqlmod->addsuspect($_POST);
                //var_dump($_POST);
                //redirect($link,'refresh'); 
                //redirect('cases/edit/' . $_POST['s_cnum'],'refresh'); 
            }
        }
        else{
            //var_dump($_POST);
            unset($_POST['link']);
            $this->getsqlmod->updateSusp($_POST['s_num'],$_POST);
            redirect($link,'refresh'); 
        }
        //$now = date('Y-m-d H:i:s');
    }
    
    public function createsocialmedia() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['s_ic'])->result();
        //echo $susp[0]->s_cnum;
        for($i=0, $count = count($_POST['social_media_name']);$i<$count;$i++) {
            $dataSet1 = array();
            $dataSet1[] = array (
                'social_media_name' => $_POST['social_media_name'][$i],
                'social_media_ac1' => $_POST['social_media_ac1'][$i],
                'social_media_ac2 ' => $_POST['social_media_ac2'][$i],
                'social_media_ac3' => $_POST['social_media_ac3'][$i],
                's_ic' => $_POST['s_ic'],
                's_cnum' => $_POST['s_cnum'],
                's_num' => $_POST['s_num'],
            );
            var_dump($dataSet1);
            $this->getsqlmod->addsocialmedia($dataSet1);
        }
        redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    public function newsocialmedia() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_cnum']= $susp[0]->s_cnum;
        $data['s_num']= $susp[0]->s_num;
        $data['title'] = "案件處理系統:新增社交媒體";
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_socialmedia';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    function editsocialmedia(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $social = $this->getsqlmod->getSocial('social_media_num',$id)->result();
        //var_dump ($susp[0]);
        $data['row'] = $social[0];
        //$data['test'] = $test;
        $data['title'] = "修改社交媒體";
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/socialmedia_edit';
        $this->load->view('template', $data);
    }  
    
    public function updatesocialmedia() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $this->getsqlmod->updateSocial($_POST['social_media_num'],$_POST);
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['s_ic'])->result();
        //var_dump($susp[0]);
        redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }

    public function createphone() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['p_s_ic'])->result();
        for($i=0, $count = count($_POST['pr_phone']);$i<$count;$i++) {
            $dataSet1[] = array();
            $dataSet1[$i] = array (
                'pr_path' => $_POST['pr_path'][$i],
                'pr_phone' => $_POST['pr_phone'][$i],
                'pr_name ' => $_POST['pr_name'][$i],
                'pr_time' => $_POST['pr_time'][$i],
                'pr_relationship' => $_POST['pr_relationship'][$i],
                'pr_has_drug' => $_POST['pr_has_drug'][$i],
                'pr_user' => $_POST['pr_user'][$i],
                'pr_seller' => $_POST['pr_seller'][$i],
                'pr_s_ic ' => $_POST['p_s_ic'],
                'pr_s_cnum' => $_POST['p_s_cnum'],
                'pr_p_no' => $_POST['p_no'],
            );
        }
        unset($_POST['pr_path']);
        unset($_POST['pr_phone']);
        unset($_POST['pr_name']);
        unset($_POST['pr_time']);
        unset($_POST['pr_relationship']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_user']);
        unset($_POST['pr_seller']);
        //var_dump($_POST);
        $this->getsqlmod->addphone($_POST);
        $phone = $this->getsqlmod->getPhone('p_s_ic',$_POST['p_s_ic'])->result();
        for($i=0, $count1 = count($dataSet1);$i<$count1;$i++) {
            $dataSet1[$i]['pr_p_num'] = $phone[0]->p_num;
        }
        $this->getsqlmod->addphone_rec($dataSet1);
        redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    public function newphone() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        $data['s_ic']=$id;
        $data['s_cnum']= $susp[0]->s_cnum;
        $data['s_num']= $susp[0]->s_num;
        $data['title'] = "案件處理系統:新增手機";
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_phone';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    function editphone(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $phone = $this->getsqlmod->getPhone('p_num',$id)->result();
        $phonerec = $this->getsqlmod->getPhoneRec('pr_p_num',$phone[0]->p_num)->result();
        $type_options = array(
            '是' => '是' ,
            '否' => '否',
        );
        $type_options1 = array(
            '撥入' => '撥入' ,
            '撥出' => '撥出',
        );
        //var_dump($phonerec);
        //echo count($phonerec);
        $data['row'] = $phone[0];
        $data['phonerec'] = $phonerec;
        $data['options'] = $type_options;
        $data['options1'] = $type_options1;
        $data['title'] = "手機修改";
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/phone_edit';
        $this->load->view('template', $data);
    }  
    
    public function updatephone() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['p_s_ic'])->result();
        if(isset($_POST['pr_path'])){
            for($i=0, $count = count($_POST['pr_path']);$i<$count;$i++) {
                $dataSet1 = array();
                $dataSet1[] = array (
                    'pr_num' => $_POST['pr_num'][$i],
                    'pr_path' => $_POST['pr_path'][$i],
                    'pr_phone' => $_POST['pr_phone'][$i],
                    'pr_name ' => $_POST['pr_name'][$i],
                    'pr_time' => $_POST['pr_time'][$i],
                    'pr_relationship' => $_POST['pr_relationship'][$i],
                    'pr_has_drug' => $_POST['pr_has_drug'][$i],
                    'pr_user' => $_POST['pr_user'][$i],
                    'pr_seller' => $_POST['pr_seller'][$i],
                    'pr_s_ic ' => $_POST['p_s_ic'],
                    'pr_s_cnum' => $_POST['p_s_cnum'],
                    'pr_p_no' => $_POST['p_no'],
                    'pr_p_num' => $_POST['p_num'],
                );
                if(!empty($_POST['pr_num'][$i])) $this->getsqlmod->deletePhonerec($_POST['pr_num'][$i]);
                $this->getsqlmod->addphone_rec($dataSet1);
            }
        }
        echo "<br>";
        unset($_POST['pr_num']);
        unset($_POST['pr_path']);
        unset($_POST['pr_phone']);
        unset($_POST['pr_name']);
        unset($_POST['pr_time']);
        unset($_POST['pr_relationship']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_has_drug']);
        unset($_POST['pr_user']);
        unset($_POST['pr_seller']);
        var_dump($_POST);
        $this->getsqlmod->updatePhone($_POST['p_num'],$_POST);
        redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
    }
    
    function delphonerec(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $phonerec = $this->getsqlmod->getPhoneRec('pr_num',$id)->result();
        $this->getsqlmod->deletePhonerec($id);
        redirect('cases/editphone/' . $phonerec[0]->pr_p_num,'refresh');
    }

    public function createsource() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['sou_s_ic'])->result();
        for($i=0, $count = count($_POST['sou_sm_name']);$i<$count;$i++) {
            $dataSet1[] = array();
            $dataSet1[$i] = array (
                'sou_sm_name' => $_POST['sou_sm_name'][$i],
                'sou_sm_ac1' => $_POST['sou_sm_ac1'][$i],
                'sou_sm_ac2 ' => $_POST['sou_sm_ac2'][$i],
                'sou_sm_ac3' => $_POST['sou_sm_ac3'][$i],
                'sou_sm_s_ic ' => $_POST['sou_s_ic'],
                'sou_sm_s_cnum' => $_POST['sou_cnum'],
                'sou_sm_phone' => $_POST['sou_phone'],
                'sou_s_num' => $_POST['sou_s_num'],
            );
        }
        //$this->getsqlmod->addsource_sm($dataSet1);
        unset($_POST['sou_sm_name']);
        unset($_POST['sou_sm_ac1']);
        unset($_POST['sou_sm_ac2']);
        unset($_POST['sou_sm_ac3']);
        if(!empty($_FILES['sourcepic']['name'])){
            $_FILES['sourcepic']['name'] = $_POST['sou_s_ic'] . $_FILES['sourcepic']['name'][$i];
            $config['upload_path']          = 'sourcedrugpic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('sourcepic')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
            else{
                $uploadData = $this->upload->data();
                $_POST['sou_pic']=$uploadData['file_name'];
                $this->getsqlmod->adddrugSou($_POST);
                $source = $this->getsqlmod->getSource('sou_s_ic',$_POST['sou_s_ic'])->result();
                for($i=0, $count1 = count($dataSet1);$i<$count1;$i++) {
                    $dataSet1[$i]['sou_sounum'] = $source[0]->sou_num;
                }
                var_dump($dataSet1);
                $this->getsqlmod->addsource_sm($dataSet1);
                //redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
        }
        else{
            $_POST['sou_pic']=null;
            $this->getsqlmod->adddrugSou($_POST);
            $source = $this->getsqlmod->getSource('sou_s_ic',$_POST['sou_s_ic'])->result();
            for($i=0, $count1 = count($dataSet1);$i<$count1;$i++) {
                $dataSet1[$i]['sou_sounum'] = $source[0]->sou_num;
            }
            $this->getsqlmod->addsource_sm($dataSet1);
            redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
        }
    }
    
    public function newsource() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$id)->result();
        //var_dump($susp);
        if(isset($susp[0]->s_cnum)){
            $data['s_ic']=$id;
            $data['s_cnum']= $susp[0]->s_cnum;
            $data['s_num']= $susp[0]->s_num;
            $data['s_num']= $susp[0]->s_num;
            $data['s_name']= $susp[0]->s_name;
        }else{
            $data['s_ic']=$id;
            $data['s_cnum']= '';
            $data['s_num']='';
            $data['s_name']= '';
        }
        $data['title'] = "案件處理系統:溯源";
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/new_source';
        $this->load->view('template', $data);
        //$this->load->view ( 'cases/new_cases' ); 
    }
    
    function editsource(){
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('html');
        $id = $this->uri->segment(3);
        $this->load->model ( 'getsqlmod' ); 
        $source = $this->getsqlmod->getSource('sou_num',$id)->result();
        $sourcesm = $this->getsqlmod->getSourceSM('sou_sounum',$source[0]->sou_num)->result();
        $data['row'] = $source[0];
        $data['sourcesm'] = $sourcesm;
        $data['title'] = "修改溯源";
        $data['nav'] = 'navbar1';
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'cases/source_edit';
        $this->load->view('template', $data);
    }  
    
    public function updatesource() {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation'); 
        $this->load->model ( 'getsqlmod' ); 
        $susp = $this->getsqlmod->get1Susp_ed('s_ic',$_POST['sou_s_ic'])->result();
        if(isset($_POST['sou_sm_name'])){
            for($i=0, $count = count($_POST['sou_sm_name']);$i<$count;$i++) {
                $dataSet1 = array();
                $dataSet1[] = array (
                    'sou_sm_name' => $_POST['sou_sm_name'][$i],
                    'sou_sm_ac1' => $_POST['sou_sm_ac1'][$i],
                    'sou_sm_ac2 ' => $_POST['sou_sm_ac2'][$i],
                    'sou_sm_ac3' => $_POST['sou_sm_ac3'][$i],
                    'sou_sm_s_ic ' => $_POST['sou_s_ic'],
                    'sou_sm_s_cnum' => $_POST['sou_cnum'],
                    'sou_sm_phone' => $_POST['sou_phone'],
                    'sou_sounum' => $_POST['sou_num'],
                );
                if(!empty($_POST['sou_sm_num'][$i])) $this->getsqlmod->deleteSource($_POST['sou_sm_num'][$i]);
                $this->getsqlmod->addsource_sm($dataSet1);
            }
        }
        unset($_POST['sou_sm_num']);
        unset($_POST['sou_sm_name']);
        unset($_POST['sou_sm_ac1']);
        unset($_POST['sou_sm_ac2']);
        unset($_POST['sou_sm_ac3']);
        if(!empty($_FILES['sourcepic']['name'])){
            $_FILES['sourcepic']['name'] = $_POST['sou_s_ic'] . $_FILES['sourcepic']['name'][$i];
            $config['upload_path']          = 'sourcedrugpic/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('sourcepic')){
                $error = array('error' => $this->upload->display_errors());
                //var_dump($error);
                redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
            else{
                //$this->getsqlmod->addsuspect($_POST);
                $uploadData = $this->upload->data();
                $_POST['sou_pic']=$uploadData['file_name'];
                $this->getsqlmod->updateSource($_POST['sou_num'],$_POST);
                redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
            }
        }
        else{
            $_POST['sou_pic']=null;
            $this->getsqlmod->updateSource($_POST['sou_num'],$_POST);
            redirect('cases/editsusp1/' . $susp[0]->s_num,'refresh'); 
        }
    }
    
    function delsourcesm(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $source = $this->getsqlmod->getSourceSM('sou_sm_num',$id)->result();
        $this->getsqlmod->deleteSource($id);
        //var_dump ($source[0]);
        redirect('cases/editsource/' . $source[0]->sou_sounum,'refresh');
    }
    function delcases(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $this->getsqlmod->deleteCaseS($id);
        //var_dump ($source[0]);
        redirect('cases/listCases','refresh');
    }
    function delsusp(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        $this->getsqlmod->deleteSusp($id);
        //var_dump ($source[0]);
        redirect('cases/edit/'. $susp[0]->s_cnum ,'refresh');
    }
    function delsource(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->getSource('sou_num',$id)->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_ic',$susp[0]->sou_s_ic)->result();
        $this->getsqlmod->deleteSou($id);
        redirect('cases/editsusp1/'. $susp1[0]->s_num ,'refresh');
    }
    function delphone(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->getPhone('p_num',$id)->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_ic',$susp[0]->p_s_ic)->result();
        $this->getsqlmod->deletePho($id,$susp[0]->p_no);
        redirect('cases/editsusp1/'. $susp1[0]->s_num ,'refresh');
    }
    function delsocial(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->getSocial('social_media_num',$id)->result();
        $susp1 = $this->getsqlmod->get1Susp_ed('s_ic',$susp[0]->s_ic)->result();
        $this->getsqlmod->deleteSoc($id);
        redirect('cases/editsusp1/'. $susp1[0]->s_num ,'refresh');
    }
    function delcar(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $susp = $this->getsqlmod->get1Car('v_num',$id)->result();
        $this->getsqlmod->deleteVec($id);
        redirect('cases/editsusp1/'. $susp[0]->v_s_num ,'refresh');
    }
    function deldrug(){
        $id = $this->uri->segment(3);
        $this->load->model('getsqlmod','',TRUE);
        $this->getsqlmod->deleteCaseS($id);
        //var_dump ($source[0]);
        redirect('cases/listCases','refresh');
    }

}