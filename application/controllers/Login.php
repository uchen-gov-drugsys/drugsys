<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Login extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }

    public function index() {
       // generate HTML table from query results
        $this->load->helper('form');
        $data['title'] = "登入";
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'login';
        $data['error'] = '';
        //$data['nav'] = 'navbar2_list';
        $this->load->view('template', $data);
    }
    
    public function forgetpw() {
       // generate HTML table from query results
        $this->load->helper('form');
        $data['title'] = "忘記密碼";
        $data['include'] = 'login_forgetpw';
        $data['error'] = '';
        //$data['nav'] = 'navbar2_list';
        $this->load->view('template', $data);
    }
    
    public function getcode() {//獲取驗證碼
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); 
        $data['title'] = "取得驗證碼";
        $uid='';
        $code= NULL;//顯示一階有權限的人
        $com= NULL;//顯示二階有權限的人
        $baoda = $this->getsqlmod->getempno_baoda()->result();//保大特殊權限
        if(isset($_POST['login_type'])&&$_POST['login_type']=='1'){
            $uid = $_POST['uid'];
            $user = $this->getsqlmod->getempno(strtoupper($_POST['uid']))->result(); 
            $this->getsqlmod->updatechangecode_ic(strtoupper($_POST['uid'])); 
            $code = $this->getsqlmod->getempno_code($user[0]->em_roffice,$user[0]->em_priority)->result(); 
            if($user[0]->em_priority =='1') $com = $this->getsqlmod->getempno_com($user[0]->em_office)->result(); 
            if($user[0]->em_office =='保安警察大隊') $baoda = $this->getsqlmod->getempno_baoda()->result(); 
        }else if(isset($_POST['login_type'])&&$_POST['login_type']=='0'){
            $uid = $_POST['uid'];
            $user = $this->getsqlmod->getempno_mail($_POST['uid'])->result(); 
            $this->getsqlmod->updatechangecode_mail($_POST['uid']); 
            $code = $this->getsqlmod->getempno_code($user[0]->em_roffice,$user[0]->em_priority,$user[0]->em_office)->result(); 
            if($user[0]->em_priority =='1') $com = $this->getsqlmod->getempno_com($user[0]->em_office)->result(); 
            if($user[0]->em_office =='保安警察大隊') $baoda = $this->getsqlmod->getempno_baoda()->result(); 
        }else {
            $code = NULL; 
            $com = NULL; 
            $baoda = NULL; 
        }
        $data['include'] = 'login_getcode';
        $data['code'] = $code;
        $data['com'] = $com;
        $data['baoda'] = $baoda;
        $data['uid'] = $uid;
        //$data['nav'] = 'navbar2_list';
        $this->load->view('template', $data);
    }
    public function changepw_forget() {
        $this->load->helper('url');
        $this->load->model ( 'getsqlmod' ); 
        if($_POST['login_type']=='1'&&isset($_POST['uid'])){
            $user = $this->getsqlmod->getempno(strtoupper($_POST['uid']))->result(); 
            if ($user[0] -> em_change_code == $_POST['u_changecode']) {
                $this->getsqlmod->updatepw($_POST['uid'],$_POST['u_pw']);   
                redirect('login/index/','refresh'); 
            }
            else{
                $data['title'] = "驗證碼錯誤";
                $data['user'] = $this -> session -> userdata('uic');
                //$data['headline'] = "測試案件處理系統";
                $data['include'] = 'login_forgetpw';
                $data['error'] = '驗證碼錯誤';
                $this->load->view('template', $data);
            }
        }
        else if($_POST['login_type']=='0'&& isset($_POST['uid'])){
            $user = $this->getsqlmod->getempno_mail($_POST['uid'])->result(); 
            if (isset($user[0]->em_change_code) && $user[0] -> em_change_code == $_POST['u_changecode']) {
                $this->getsqlmod->updatepw_mail($_POST['uid'],$_POST['u_pw']);        
                redirect('login/index/','refresh'); 
            }
            else{
                $data['title'] = "驗證碼錯誤";
                $data['user'] = $this -> session -> userdata('uic');
                //$data['headline'] = "測試案件處理系統";
                $data['include'] = 'login_forgetpw';
                $data['error'] = '驗證碼錯誤';
                $this->load->view('template', $data);
            }
        }
        else{
            $data['title'] = "帳號不存在或類型錯誤";
            $data['user'] = $this -> session -> userdata('uic');
            $data['include'] = 'login_forgetpw';
            $data['error'] = '帳號不存在或類型錯誤';
            $this->load->view('template', $data);
        }
    }
    
    public function changead() {//更改權限(1/2階)
            //echo $this -> session -> userdata('IsAdmin');
        if($this -> session -> userdata('IsAdmin')== 1 ){
            $IsAdmin = $this->uri->segment(3);
            $id = $this->uri->segment(4);
            if($IsAdmin == '1')$this->getsqlmod->changead($id,'0');  
            if($IsAdmin == '0')$this->getsqlmod->changead($id,'1');  
            if($IsAdmin == '2')$this->getsqlmod->changead($id,'2');  
            if($IsAdmin == '99')$this->getsqlmod->changepriority($id,'99');  
            
            if($this -> session -> userdata('em_priority') == '1') redirect('cases/admin/','refresh'); 
            if($this -> session -> userdata('em_priority') == '2') redirect('cases2/admin/','refresh'); 
            if($this -> session -> userdata('em_priority') == '3') redirect('record/permit/','refresh'); 
        }
        else echo'沒有權限';
    }
    
    public function changeoffice() {//更改權限(1/2階)
            //echo $this -> session -> userdata('IsAdmin');
        if($this -> session -> userdata('IsAdmin')== 1 ){
            $id = $this->uri->segment(3);
            $this->getsqlmod->changeoffice($id);  
            if($this -> session -> userdata('em_priority') == '1') redirect('cases/admin/','refresh'); 
            if($this -> session -> userdata('em_priority') == '2') redirect('cases2/admin/','refresh'); 
            if($this -> session -> userdata('em_priority') == '3') redirect('record/permit/','refresh'); 
        }
        else echo'沒有權限';
    }
    
    public function changepw() {
       // 修改密碼
        $this->load->helper('form');
        $data['title'] = "更改密碼";
        $data['include'] = 'login_changepw';
        $data['error'] = '';
        //$data['nav'] = 'navbar2_list';
        $this->load->view('template', $data);
    }
    public function firstpw() {
       // 首次登入修改密碼
        $this->load->helper('form');
        $data['title'] = "更改密碼";
        $data['include'] = 'login_changepw1';
        $data['error'] = '';
        //$data['nav'] = 'navbar2_list';
        $this->load->view('template', $data);
    }
    
    public function change_pw() {//修改密碼更新資料庫
        $this->load->helper('url');
        $this->load->model ( 'getsqlmod' ); 
        //echo $this -> session -> userdata('email').$_POST['u_pw'];
        $this->getsqlmod->updatepw_mail($this -> session -> userdata('email'),$_POST['u_pw']);        
        $this->session->sess_destroy();
        redirect('login/','refresh'); 
    }
    public function first_pw() {//首次修改密碼更新資料庫
        $this->load->helper('url');
        $this->load->model ( 'getsqlmod' ); 
        $this->getsqlmod->updatepw_first($this -> session -> userdata('email'),$_POST['u_pw'],$_POST['u_id']);        
        // echo $_POST['u_id'];
        $this->session->sess_destroy();
        redirect('login/','refresh'); 
    }
    
    function table_cases() {
        $this->load->library('table');
        $this->load->model ( 'getsqlmod' ); 
        $tmpl = array (
            'table_open' => '<table border="0"  class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '帳號', '姓名', '分局','職稱','單位','權限','驗證碼');
        $table_row = array();
        return $this->table->generate();
    }
    function testjson() {
        include 'config.php';

        ## Read value
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
            $searchQuery = " and (em_mailId like '%".$searchValue."%' or 
                em_lastname like '%".$searchValue."%' or 
                em_centername like '%".$searchValue."%' or 
                em_firstname like '%".$searchValue."%' or 
                em_officeAll like '%".$searchValue."%' or 
                em_office like '%".$searchValue."%' or 
                em_roffice like'%".$searchValue."%' ) ";
        }

        ## Total number of records without filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of records with filtering
        $sel = mysqli_query($con,"select count(*) as allcount from employees WHERE 1 ".$searchQuery);
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "select * from employees WHERE 1 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
        $empRecords = mysqli_query($con, $empQuery);
        $data = array();

        while ($row = mysqli_fetch_assoc($empRecords)) {
            $data[] = array(
                    "em_lastname"=>$row['em_lastname'].$row['em_centername'].$row['em_firstname'],
                    "em_officeAll"=>$row['em_officeAll'],
                    "em_office"=>$row['em_office'],
                    "em_job"=>$row['em_job'],
                    "em_IsAdmin"=>$row['em_IsAdmin'],
                    "em_change_code"=>$row['em_change_code'],
                    "em_mailId"=>$row['em_mailId'],
                    "em_roffice"=>$row['em_roffice']
                );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }
     
    function showalluser() {
       // generate HTML table from query results
        $this->load->helper('form');
        $test_table = $this->table_cases();
        $data['data_table'] = $test_table;
        $data['title'] = "案件處理系統:列表";
        $data['user'] = $this -> session -> userdata('uic');
        //$data['headline'] = "測試案件處理系統";
        $data['include'] = 'user_list2';
        $data['nav'] = 'navbar1_list';
        $data['IsAdmin'] = $this -> session -> userdata('IsAdmin');
        $this->load->view('template1', $data);
    }
    function checklogin() { //設定session/
        // 分為mailid 跟 身份證
        if($_POST['login_type']=='1'){
            $user = $this->getsqlmod->getempno(strtoupper($_POST['uid']))->result(); 
        }else{
            $user = $this->getsqlmod->getempno_mail($_POST['uid'])->result(); 
        }
        if ($user) {
            if ($user[0] -> em_pw == $_POST['u_pw']) {
                $this -> load -> library('session');
                $dui = strstr($user[0] -> em_office, "隊",true);
                //echo strlen($dui);
                if(strlen($dui)<=15 && strlen($dui)>0){
                    $dui=$dui."隊";
                }else{
                    $dui = strstr($user[0] -> em_office, "局",true);
                    $dui=$dui."局";
                }
                $name = $user[0] -> em_lastname.$user[0] -> em_centername.$user[0] -> em_firstname;
                $arr =array(
                        'uic' => $user[0] -> em_ic.$name.';'.$user[0] -> em_officeAll.$user[0] -> em_job,
                        'uid' => $user[0] -> em_ic,
                        'email' => $user[0] -> em_mailId,
                        'uoffice' => $user[0] -> em_office,
                        'uroffice' => $user[0] -> em_roffice,
                        'uofficeAll' => $user[0] -> em_officeAll,
                        'uname' => $name,
                        'ujob' => $user[0] -> em_job,
                        '3permit' => $user[0] -> em_3permit,//三階權限分配
                        'IsAdmin' => $user[0] -> em_IsAdmin,
                        'em_priority' => $user[0] -> em_priority
                    );
                $this -> session -> set_userdata($arr);
                if($user[0]-> em_mailId == $user[0]-> em_pw ){
                    //if($user[0]->em_office == )
                   redirect('login/firstpw/','refresh'); 
                }
                else if($user[0]-> em_priority == '1'){
                   redirect('cases/listCases/','refresh'); 
                }
                else if($user[0]-> em_priority == '2'){
                   redirect('cases2/listCases/','refresh'); 
                }
                else if($user[0]-> em_priority == '3'){
                   redirect('disciplinary_c/listdp/','refresh'); 
                }
            }
            else {
                $data['title'] = "密碼錯誤";
                $data['user'] = $this -> session -> userdata('uic');
                //$data['headline'] = "測試案件處理系統";
                $data['include'] = 'login';
                $data['error'] = '密碼錯誤';
                $this->load->view('template', $data);
            }
        }   
        else {
            $data['title'] = "帳號不存在或類型錯誤";
            $data['user'] = $this -> session -> userdata('uic');
            //$data['headline'] = "測試案件處理系統";
            $data['include'] = 'login';
            $data['error'] = '帳號不存在或類型錯誤';
            $this->load->view('template', $data);
        }       
    }
    function is_login() {
        $this -> load -> library('session');
        if ($this -> session -> userdata('uic')) {
            echo "logined";
        } 
        else {
            echo "no login";
        }
    }
    function logout() {
        $this -> load -> library('session');
        $this->session->sess_destroy();
        $this -> session -> unset_userdata('uic');
        redirect('login/','refresh'); 
    }
}
