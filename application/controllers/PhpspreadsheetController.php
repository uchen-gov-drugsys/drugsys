<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    class PhpspreadsheetController extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this -> load -> library('Session/session');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
    }
    public function index(){
        $this->load->view('spreadsheet');
    }
    public function export(){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');
        $writer = new Xlsx($spreadsheet);
        $filename = 'name-of-the-generated-file';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file
        }
    public function import(){
        /*$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        if(isset($_FILES['upload_file']['name']) && in_array($_FILES['upload_file']['type'], $file_mimes)) {
            $arr_file = explode('.', $_FILES['upload_file']['name']);
            $extension = end($arr_file);
            if('csv' == $extension){
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } else {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            echo "<pre>";
            echo 'test';
            print_r($sheetData);
        }
        else{echo 'no';}*/
        //require 'vendor/autoload.php';

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        try {
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }

        $sheet = $spreadsheet->getActiveSheet();

        $res = array();
        foreach ($sheet->getRowIterator(2) as $row) {
            $tmp = array();
            foreach ($row->getCellIterator() as $cell) {
                //$tmp[] = $cell->getFormattedValue();
                $tmp[] = $cell->getFormattedValue();
            }
            $res[] = $tmp;
        }
        
        foreach($res as $value){
            //print_r($value);
            echo $value[1];
            echo '<br>';
            $surcharge = array(
                'surc_no' => $value[1],
                'surc_listed_no' => $value[2],
                'surc_psend_num' => $value[3],
                'surc_sic' => $value[5],
                'surc_olec_date' => $value[6],
            );
            //$this->getsqlmod->adddelivery($surcharge);
            $this->getsqlmod->addsurc_receive($surcharge);
        }
        //var_dump($res);
        //echo json_encode($res);    
    } 
    
    public function importdata(){
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        try {
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }

        $sheet = $spreadsheet->getActiveSheet();
        // $sheet = $spreadsheet->getSheet(0);
        $row = $sheet->getHighestRow();
        
		
        try {
            for ($i = 2; $i <= $row; $i++) {
				$f_year = $sheet->getCellByColumnAndRow(1, $i)->getValue();
				$f_caseid = $sheet->getCellByColumnAndRow(2, $i)->getValue();
				$f_username =  $sheet->getCellByColumnAndRow(3, $i)->getValue();
				$f_userid = $sheet->getCellByColumnAndRow(4, $i)->getValue();
				$f_amount = $sheet->getCellByColumnAndRow(5, $i)->getValue();
				$f_donedate = $this->tranfer2ADyear($sheet->getCellByColumnAndRow(6, $i)->getValue());
				$f_doneamount = ((!empty($sheet->getCellByColumnAndRow(7, $i)->getValue()))?$sheet->getCellByColumnAndRow(7, $i)->getValue():0);
				$f_paydate = (($_POST['types'] === 'A')?$this->tranfer2ADyear($sheet->getCellByColumnAndRow(9, $i)->getValue()):$this->tranfer2ADyear($sheet->getCellByColumnAndRow(8, $i)->getValue()));
				$f_paymoney = (($_POST['types'] === 'A')?(!empty($sheet->getCellByColumnAndRow(10, $i)->getValue())?$sheet->getCellByColumnAndRow(10, $i)->getValue():0):(!empty($sheet->getCellByColumnAndRow(9, $i)->getValue())?$sheet->getCellByColumnAndRow(9, $i)->getValue():0));
				$f_paylog = (($_POST['types'] === 'A')?((empty($sheet->getCellByColumnAndRow(11, $i)->getValue()))?$sheet->getCellByColumnAndRow(8, $i)->getValue():$sheet->getCellByColumnAndRow(11, $i)->getValue()):$sheet->getCellByColumnAndRow(10, $i)->getValue());
				$f_execlog = (($_POST['types'] === 'A')?$sheet->getCellByColumnAndRow(12, $i)->getValue():$sheet->getCellByColumnAndRow(11, $i)->getValue());
				$f_movelog = (($_POST['types'] === 'A')?$sheet->getCellByColumnAndRow(13, $i)->getValue():$sheet->getCellByColumnAndRow(12, $i)->getValue());
				$f_cardlog = (($_POST['types'] === 'A')?$sheet->getCellByColumnAndRow(14, $i)->getValue():$sheet->getCellByColumnAndRow(13, $i)->getValue());
				$f_dellog = (($_POST['types'] === 'A')?$sheet->getCellByColumnAndRow(15, $i)->getValue():$sheet->getCellByColumnAndRow(14, $i)->getValue());
				$f_comment = str_replace(array('0','1','2','3','4','5','6','7','8','9'), "", $sheet->getCellByColumnAndRow(5, $i)->getValue());
                // 獲取行數的數據
                $rowData = array(
                    "type" => $_POST['types'],
                    "f_oldno" => null,              
                    "f_year" => $f_year,
                    "f_caseid" => trim($f_caseid),
                    "f_username" =>$f_username,
                    "f_userid" => trim($f_userid),
                    "f_amount" => $f_amount,
                    "f_donedate" => $f_donedate,
                    "f_doneamount" => $f_doneamount,
                    "f_paydate" => $f_paydate,
                    "f_paymoney" => $f_paymoney,
                    "f_virtualcode" => '',
                    "f_paylog" => $f_paylog,
                    "f_execlog" => $f_execlog,
                    "f_movelog" => $f_movelog,
                    "f_cardlog" => $f_cardlog,
                    "f_dellog" => $f_dellog,
                    "f_comment" => $f_comment
                );
				
				if($this->getsqlmod->checkFineImport($rowData))
				{
					if(!$this->getsqlmod->checkUpdateFineImport($rowData))
					{
						$this->getsqlmod->updateFineImport($rowData);
					}
					
				}
				else
				{
					// var_dump($rowData);
					// var_dump($this->getsqlmod->checkFineImport($rowData));
					// exit;
					$insert_id = $this->getsqlmod->addFineImport($rowData);
				
					$temp = $f_paylog;
					// var_dump($temp);
					if(isset($temp) && !empty($temp))
					{
						$paylogAry = mb_split("\n",$f_paylog);                    
						
						foreach ($paylogAry as $value) {
							$temp_v = explode('_', $value);
							$paylogAry_date = $temp_v[0];
							$paylogAry_money = ((isset($temp_v[1]))?$temp_v[1]:$value);
							
							if($paylogAry_money != 0)
							{
								$temp_data = array(
									"fpart_fpnum" => (int)$insert_id,
									"fpart_amount" => $paylogAry_money,
									"fpart_date" => $this->tranfer2ADyear($paylogAry_date)
								);
			
								$this->getsqlmod->import_check_finepayport_data($temp_data);
							}
							
						}
						// var_dump($paylogAry);
						// exit;
					}
				}
                
            }
            echo 'ok';
        } catch (\Throwable $th) {
            echo $th;
        }
        // $res = array();
        // foreach ($sheet->getRowIterator(3) as $row) {
        //     $tmp = array();
        //     foreach ($row->getCellIterator() as $cell) {
        //         //$tmp[] = $cell->getFormattedValue();
        //         $tmp[] = $cell->getFormattedValue();
        //     }
        //     $res[] = $tmp;
        // }
        // echo json_encode($data);
        // var_dump(json($data));
    }

    // 特定帳務列印
    public function export_choose_fines(){
        
        $datarows = json_decode($_POST['datarows'], true );
        $htmlString_head = '<table border="1">
                    <thead>
                        <tr>
                            <td align="center"></td>
                            <td align="center">類型</td>
                            <td align="center">案件編號</td>
                            <td align="center">受處分人姓名</td>
                            <td align="center">身份證編號</td>
                            <td align="center">罰鍰/怠金</td>
                            <td align="center">完納日期</td>
                            <td align="center">完納金額</td>
                            <td align="center">繳款日期</td>
                            <td align="center">繳款金額</td>
                            <td align="center">分期Log</td>
                            <td align="center">移送Log</td>
                            <td align="center">憑證Log</td>
                            <td align="center">撤銷註銷Log</td>
                            <td align="center">執行命令Log</td>
                            <td align="center">備註</td>
                        </tr>
                    </thead>
                    <tbody>';
        $htmlString_body = '';
        for ($i=0; $i < count($datarows); $i++) { 
            $htmlString_body .= '<tr>
                <td align="center">'.($i + 1).'</td>
                <td align="center">'.$datarows[$i]['type'].'</td>
                <td align="center">'.$datarows[$i]['caseid'].'</td>
                <td align="center">'.$datarows[$i]['username'].'</td>
                <td align="center">'.$datarows[$i]['userid'].'</td>
                <td align="center">'.$datarows[$i]['amount'].'</td>
                <td align="center">'.$datarows[$i]['donedate'].'</td>
                <td align="center">'.$datarows[$i]['doneamount'].'</td>
                <td align="center">'.$datarows[$i]['paydate'].'</td>
                <td align="center">'.$datarows[$i]['payamount'].'</td>
                <td align="center">'.$datarows[$i]['paylog'].'</td>
                <td align="center">'.$datarows[$i]['movelog'].'</td>
                <td align="center">'.$datarows[$i]['cardlog'].'</td>
                <td align="center">'.$datarows[$i]['dellog'].'</td>
                <td align="center">'.$datarows[$i]['execlog'].'</td>
                <td align="center">'.$datarows[$i]['comment'].'</td>
            </tr>';
        }
           
        
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($htmlString_head . $htmlString_body);

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        // $writer->save('write.xlsx'); 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.date('Ymd').'帳務資料.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file

    }

    // 清冊
    public function export_finePJ_lists(){
        $id = $this->uri->segment(3);
        $data = $this->getsqlmod->export_finePJ_lists($id)->result();
        // var_dump($data[0]->fp_type);
        // echo count($data);
        // exit;
        $htmlString_head = '<table border="1">
                    <thead>
                        <tr>
                            <td colspan="12" align="center" style="font-size:16px;">'.$data[0]->fp_type.'繳納清冊</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="right">列印日期：'.(date('Y')-1911).'年'.date('m').'月'.date('d').'日</td>
                        </tr>
                        <tr>
                            <td align="center">編號</td>
                            <td align="center">移送</td>
                            <td align="center">年度</td>
                            <td align="center">列管編號</td>
                            <td align="center">受處分人</td>
                            <td align="center">繳款日期</td>
                            <td align="center">繳款金額</td>
                            <td align="center">(分期/完納)</td>
                            <td align="center">核銷'.$data[0]->fp_type.'</td>
                            <td align="center">執行費</td>
                            <td align="center">轉正'.(($data[0]->fp_type == "罰鍰")?"怠金":"罰鍰").'</td>
                            <td align="center">退費</td>
                        </tr>
                    </thead>
                    <tbody>';
        $pay_tamount_sum = 0;
        $fpart_amount_sum = 0;
        $f_fee_sum = 0;
        $f_turnsub = 0;
        $f_refund = 0;
		
        $htmlString_body = '';
        for ($i=0; $i < count($data); $i++) { 
			$calcmovelog = explode("\n", $data[$i]->calc_movelog);
			$temp_moveday = 0;
			$new_movelog = '';
			foreach ($calcmovelog as $key => $value) {
				$movedate = explode('_', $value)[0];

				if(((int)$movedate * 1) > $temp_moveday)
				{
					$temp_moveday = ($movedate * 1);
					$new_movelog = explode('_', $value)[1];
				}
					
			}

            $htmlString_body .= '<tr>
                <td align="center">'.($i + 1).'</td>
                <td align="center">'.$new_movelog.'</td>
                <td align="center">'.$data[$i]->f_year.'</td>
                <td align="center">'.$data[$i]->f_caseid.'</td>
                <td align="center">'.$data[$i]->f_username.'</td>
                <td align="right">'.$this->tranfer2RCyear($data[$i]->fp_createdate).'</td>
                <td align="right">$'.(($data[$i]->pay_tamount < 0)?number_format(0 - (int)$data[$i]->pay_tamount):number_format((int)$data[$i]->pay_tamount)).'</td>
                <td align="center">'.$data[$i]->payway.'</td>
                <td align="right">$'.number_format($data[$i]->fpart_amount).'</td>
                <td align="right">$'.number_format($data[$i]->f_fee).'</td>
                <td align="right">$'.(($data[$i]->f_turnsub < 0)?number_format(0 - (int)$data[$i]->f_turnsub):number_format((int)$data[$i]->f_turnsub)).'</td>
                <td align="right">$'.number_format($data[$i]->f_refund).'</td>
            </tr>';
            $pay_tamount_sum = ($pay_tamount_sum + (($data[$i]->pay_tamount < 0)?(0 - (int)$data[$i]->pay_tamount):(int)$data[$i]->pay_tamount));
            $fpart_amount_sum = ($fpart_amount_sum + $data[$i]->fpart_amount);
            $f_fee_sum = ($f_fee_sum + $data[$i]->f_fee);
            $f_turnsub = ($f_turnsub + (($data[$i]->f_turnsub < 0)?(0-(int)$data[$i]->f_turnsub):(int)$data[$i]->f_turnsub));
            $f_refund = ($f_refund + $data[$i]->f_refund);
        }
        
                  
        $htmlString_foot = '</tbody>
        <tfoot>
            <tr>
                <td>合計</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right">$'. number_format($pay_tamount_sum) .'</td>
                <td></td>
                <td align="right">$'. number_format($fpart_amount_sum) .'</td>
                <td align="right">$'. number_format($f_fee_sum) .'</td>
                <td align="right">$'. number_format($f_turnsub) .'</td>
                <td align="right">$'. number_format($f_refund) .'</td>
            </tr>
        </tfoot>
        </table>';        
        
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($htmlString_head . $htmlString_body . $htmlString_foot);

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        // $writer->save('write.xlsx'); 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data[0]->fp_no.$data[0]->fp_type .'繳納清冊.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file
    }

    // 收繳登記
    public function export_finePJ_lists_by_indiv(){
        $id = $this->uri->segment(3);
        $data = $this->getsqlmod->get_finePJ_lists_user_in_distinct($id)->result();
        
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        for ($i=0; $i < count($data) ; $i++) { 
			$detail = $this->getsqlmod->get_finePJ_list_user_pay_detail($data[$i]->f_no)->result();
			if($data[$i]->f_donedate <> '0000-00-00' && count($detail) == 1)
			{
				$data[$i]->listtype = '完納';
			}
			elseif($data[$i]->f_donedate <> '0000-00-00' && count($detail) > 1)
			{
				$data[$i]->listtype = '分期完納';
			}
			else
			{
				$data[$i]->listtype = '分期';
			}
             $htmlString = '<table border="1">
                            <thead>
                                <tr style="height:1.5em;">
                                    <td colspan="14" align="center" style="font-size:14px;">我是測試系統<br/>'. (date('Y')-1911) . '年毒品' .(($data[$i]->type == 'A')?'罰鍰':'怠金') . $data[$i]->listtype.'收繳登記簿</td>
                                </tr>
                                <tr>
                                    <td colspan="14" align="right">列印日期：'.(date('Y')-1911).'年'.date('m').'月'.date('d').'日</td>
                                </tr>
                                <tr>
                                    <td align="center">年度</td>
                                    <td align="center">月份</td>
                                    <td align="center">流水號</td>
                                    <td align="center">處分年度</td>
                                    <td align="center">案件</td>
                                    <td align="center">受處分人</td>
                                    <td align="center">繳款日期</td>
                                    <td align="center">繳款方式</td>
                                    <td align="center">'.(($data[$i]->listtype == "完納")?"繳款金額":"本期核銷金額").'</td>
                                    <td align="center">'.(($data[$i]->type == 'A')?'罰鍰':'怠金').'金額</td>
                                    <td align="center">'.(($data[$i]->listtype == "完納")?"本期核銷金額":((($data[$i]->type == 'A')?'罰鍰':'怠金') . "餘額")).'</td>
                                    <td align="center">執行費</td>
                                    <td align="center">狀態</td>
                                    <td align="center">備註</td>
                                </tr>
                            </thead>
                            <tbody>';
                
                
                $fpart_amount_sum = 0;
                $f_fee_sum = 0;
                $done_amount = 0;
                
                for ($j=0; $j < count($detail); $j++) { 
                    if($data[$i]->listtype == "完納")
                    {
                        $fpart_amount_sum = ($fpart_amount_sum + (int)$detail[$j]->fpart_amount + (int)$detail[$j]->f_fee);
                        $done_amount = $detail[$j]->f_doneamount;
                    }
                    else
                    {
                        $fpart_amount_sum = ($fpart_amount_sum + $detail[$j]->fpart_amount);
                    }
                    
                    $f_fee_sum = ($f_fee_sum + $detail[$j]->f_fee);

                    $htmlString .= '<tr>
                        <td align="center">'.(($data[$i]->listtype == "完納")?$detail[$j]->d_year:$detail[$j]->year).'</td>
                        <td align="center">'.(($data[$i]->listtype == "完納")?$detail[$j]->d_month:$detail[$j]->month).'</td>
                        <td align="center">'.(($j == 0)?$detail[$j]->f_no:"").'</td>
                        <td align="center">'.$detail[$j]->f_year.'</td>
                        <td align="center">'.$detail[$j]->f_caseid.'</td>
                        <td align="center">'.$detail[$j]->f_username.'</td>
                        <td align="center">'.(($data[$i]->listtype == "完納")?$detail[$j]->donedate:$detail[$j]->paydate).'</td>
                        <td align="center">'.$detail[$j]->fpart_type.'</td>
                        <td align="center">$'.(($data[$i]->listtype == "完納")?number_format((int)$detail[$j]->fpart_amount + (int)$detail[$j]->f_fee + (int)$detail[$j]->f_refund):number_format($detail[$j]->fpart_amount)).'</td>
                        <td align="center">$'.number_format((($detail[$j]->type == "A")?((int)$detail[$j]->f_amount*10000):$detail[$j]->f_amount)).'</td>
                        <td align="center">$'.(($data[$i]->listtype == "完納")?number_format($detail[$j]->fpart_amount):number_format((($detail[$j]->type == "A")?((int)$detail[$j]->f_amount*10000):$detail[$j]->f_amount) - $fpart_amount_sum)).'</td>
                        <td align="center">'.(((int)$detail[$j]->f_fee == 0 )?"":"$".number_format($detail[$j]->f_fee)).'</td>
                        <td align="center">'.(((int)$detail[$j]->f_refund > 0)?'退款':($detail[$j]->f_status . '/' .$detail[$j]->f_cstatus)).'</td>
                        <td align="center">'.$detail[$j]->f_comment.'</td>
                    </tr>';
                    
                }
                
                        
                $htmlString .= '</tbody>
                <tfoot>
                    <tr>
                        <td>合計</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>$'. number_format($fpart_amount_sum) .'</td>
                        <td></td>
                        <td>'.(($data[$i]->listtype == "完納")?("$".number_format($done_amount)):"").'</td>
                        <td>$'. number_format($f_fee_sum) .'</td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
                </table>';

            if($i == 0)
            {
                
                $spreadsheet = $reader->loadFromString($htmlString);
                
            }
            else
            {                
                $spreadhseet = $reader->loadFromString($htmlString, $spreadsheet);
                
            }
            $spreadsheet->getSheet($i)->setTitle($data[$i]->f_caseid);
            $reader->setSheetIndex(($i+1));
         }
            
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        // $writer->save('write.xlsx'); 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data[0]->fp_no.(($data[0]->fp_type == "罰鍰/怠金")?"支匯票":$data[0]->fp_type) .'收繳登記.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file
    }

    // 收據號碼
    public function export_finePJ_lists_receipt(){
        $id = $this->uri->segment(3);
        $data = $this->getsqlmod->export_finePJ_lists_ticket_receipt_report($id)->result();
        $array = json_decode(json_encode($data),true); 
        // var_dump(implode("、",array_map(function($a) {return $a['f_receipt_no'];},$array)));
        // var_dump($array);
        // echo count($array);
        // exit;
        $vaild_receipt_count = 0;
        $invaild_receipt_count = 0;
        $vaild_receipt = array();
        $invaild_receipt = array();
        for ($n=0; $n < count($array) ; $n++) { 
            if($array[$n]['f_comment'] != '作廢')
            {
                $vaild_receipt_count = ($vaild_receipt_count +1);
                array_push($vaild_receipt, $array[$n]['f_receipt_no']);
            }                
            else
            {
                $invaild_receipt_count = ($invaild_receipt_count +1);
                array_push($invaild_receipt, $array[$n]['f_receipt_no']);
            }  
        }
        
        $htmlString_head = '<table border="1">
                    <thead>
                        <tr>
                            <td colspan="17" align="center" style="font-size:16px;text-decoration: underline;">我是測試系統刑事警察大隊收入憑證報告單</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="font-size:12px;">編號：</td>
                            <td colspan="8"></td>
                            <td colspan="6" align="right" style="font-size:12px;">'.$this->tranfer2RCyearTrad($data[0]->fp_createdate).'填報</td>
                        </tr>
                        <tr>
                            <td rowspan="3" align="center">收入憑證名稱</td>
                            <td rowspan="2" colspan="2" align="center">上日結存數</td>
                            <td rowspan="2" colspan="2" align="center">本日新領數</td>
                            <td colspan="4" align="center">本日使用數</td>
                            <td rowspan="3" align="center">開立金額</td>
                            <td rowspan="3" align="center">本日實收金額</td>
                            <td rowspan="2" colspan="2" align="center">截至本日止結存數</td>
                            <td rowspan="2" colspan="4" align="center">備註<br/><span style="font-size:6px;">「本日實收金額」使用數據</span></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">繳核</td>
                            <td colspan="2" align="center">作廢</td>
                        </tr>
                        <tr>
                            <td align="center">張數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">張數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">張數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">張數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">張數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">收據號碼</td>
                            <td align="center">金額</td>
                            <td align="center">收據號碼</td>
                            <td align="center">金額</td>
                        </tr>
                    </thead>
                    <tbody>';
                    $htmlString_body = '<tr>';
                    $htmlString_body .= '
                        <td rowspan="9">罰金罰鍰怠金（毒品）</td>
                        <td rowspan="9"></td>
                        <td rowspan="9"></td>
                        <td rowspan="9"></td>
                        <td rowspan="9"></td>
                        <td rowspan="9">'.$vaild_receipt_count.'</td>
                        <td rowspan="9">'.implode("、",$vaild_receipt).'</td>
                        <td rowspan="9">'.$invaild_receipt_count.'</td>
                        <td rowspan="9">'.implode("、",$invaild_receipt).'</td>
                        <td rowspan="9">$'.number_format($data[0]->fpart_amount) .'</td>
                        <td rowspan="9">'.number_format($data[0]->fpart_amount) .'</td>
                        <td rowspan="9"></td>
                        <td rowspan="9"></td>';
                        if(count($data) > 0)
                        {
                            $htmlString_body .='
                            <td>'.$data[0]->f_receipt_no.'</td>
                            <td>$'.(($data[0]->f_comment != '作廢')?number_format($data[0]->paymoney):$data[0]->f_comment).'</td>
                            <td></td>
                            <td></td>';
                        }
                        $htmlString_body .= '</tr>';
                        for ($i=0; $i < 8; $i++) { 
                            if(($i+1) < count($data))
                            {
                                $htmlString_body .= '<tr>
                                <td>'.(($data[$i+1]->f_receipt_no)?$data[$i+1]->f_receipt_no:'').'</td>
                                <td>'.(($data[$i+1]->f_comment != '作廢')?(($data[$i+1]->paymoney)?('$'.number_format($data[$i+1]->paymoney)):''):$data[$i+1]->f_comment).'</td>
                                <td></td>
                                <td></td>';
                            }
                            else
                            {
                                $htmlString_body .= '<tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>';
                            }
                            
    
                            $htmlString_body .= '</tr>';
                        }
                        // for ($i=0; $i < 5; $i++) { 
                        //     $htmlString_body .= '<tr>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td>
                        //         <td rowspan="2"></td></tr>';
                        //     }
                    for ($i=0; $i < 10; $i++) { 
                        if(($i+8) <= count($data))
                        {
                            $htmlString_body .= '<tr>
                            <td>'.(($data[$i + 8]->f_receipt_no)?$data[$i]->f_receipt_no:'').'</td>
                            <td>'.(($data[$i+8]->f_comment != '作廢')?(($data[$i + 8]->paymoney)?('$'.$data[$i]->paymoney):''):$data[$i+8]->f_comment).'</td>
                            <td></td>
                            <td></td>';
                        }
                        else
                        {
                            $htmlString_body .= '<tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>';
                        }
                        

                        $htmlString_body .= '</tr>';
                    }
                    
        $htmlString_foot = '</tbody>
        <tfoot>
            <tr>
                <td>合計</td>
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td colspan="2"  align="center">'.$vaild_receipt_count.'</td>
                <td colspan="2">'.$invaild_receipt_count.'</td>
                <td>'.number_format($data[0]->fpart_amount) .'</td>
                <td>'.number_format($data[0]->fpart_amount) .'</td>
                <td colspan="2"></td>
            </tr>
        </tfoot>
        </table>';   
        $htmlString_foot.='<table>
                <tr>
                    <td colspan="3">製表人</td>
                    <td colspan="3">業務主管</td>
                    <td colspan="3">出納</td>
                    <td colspan="2">主辦會計</td>
                    <td colspan="2">機關首長</td>
                </tr>
            </table>';     
        
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($htmlString_head. $htmlString_body. $htmlString_foot);

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        // $writer->save('write.xlsx'); 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data[0]->f_project_id .'收據號碼.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file
    }

    /**
     * 報表查詢-罰金罰鍰怠金明細表
     */
    public function export_fineReport_r1()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');

        $dateparam = $_GET['param'];
        $data = $this->getsqlmod->get_finereport_month_case(explode(',',$dateparam)[0], explode(',',$dateparam)[1], $_GET['rtype'])->result();
        $fee_data = $this->getsqlmod->get_finereport_month_case_fee(explode(',',$dateparam)[0], explode(',',$dateparam)[1], $_GET['rtype'])->result();

        $html='';
            $html .='
            <table width="2500px" border="1">
                <thead>
                    <tr>
                        <td colspan="11" align="center">'.(explode('-',explode(',',$dateparam)[1])[0]-1911) .'年'.explode('-',explode(',',$dateparam)[1])[1].'月毒品'.(($_GET['rtype'] == 'A')?'罰金罰鍰':'怠金').'明細表</td>
                    </tr>
                    <tr>
                        <td colspan="11" align="right">列印日期：'.(date('Y')-1911).'年'.date('m').'月'.date('d').'日</td>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td> </td>
                    <td colspan="2" align="center">本月收繳</td>
                    <td colspan="2" align="center">已註銷應收款歸屬'.(date('Y')-1911).'年度本月實收</td>
                    <td rowspan="2" align="center">對帳單金額</td>
                    <td colspan="2" align="center">累計收繳</td>
                    <td colspan="2" align="center">已註銷應收款歸屬'.(date('Y')-1911).'年度累計實收</td>
                    <td rowspan="2" align="center">對帳單金額</td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center">件數</td>
                    <td align="center">金額</td>
                    <td align="center">件數</td>
                    <td align="center">金額</td>
                    <td align="center">件數</td>
                    <td align="center">金額</td>
                    <td align="center">件數</td>
                    <td align="center">金額</td>
                </tr>';
            $t_casenum = 0;
            $t_receiveamount = 0;
            $t_del_casenum = 0;
            $t_del_receivemoney = 0;
            $t_grandcasenum = 0;
            $t_grandreceiveamount = 0;
            $t_del_grandcasenum = 0;
            $t_del_grandreceivemoney = 0;
            for ($i=0; $i < count($data) ; $i++) {
                $t_casenum = $t_casenum + $data[$i]->casenum;   
                $t_receiveamount = $t_receiveamount + $data[$i]->receivemoney; 
                $t_del_casenum = $t_del_casenum + $data[$i]->del_casenum;   
                $t_del_receivemoney = $t_del_receivemoney + $data[$i]->del_receivemoney;
                
                $t_grandcasenum = $t_grandcasenum + $data[$i]->grandcasenum;   
                $t_grandreceiveamount = $t_grandreceiveamount + $data[$i]->grandreceivemoney; 
                $t_del_grandcasenum = $t_del_grandcasenum + $data[$i]->del_grandcasenum;   
                $t_del_grandreceivemoney = $t_del_grandreceivemoney + $data[$i]->del_grandreceivemoney;
            }
            $html .='<tr>
                    <td align="center">合計</td>
                    <td align="center">'.$t_casenum.'</td>
                    <td align="center">'.number_format($t_receiveamount).'</td>
                    <td align="center">'.$t_del_casenum.'</td>
                    <td align="center">'.number_format($t_del_receivemoney).'</td>
                    <td></td>
                    <td align="center">'.$t_grandcasenum.'</td>
                    <td align="center">'.number_format($t_grandreceiveamount).'</td>
                    <td align="center">'.$t_del_grandcasenum.'</td>
                    <td align="center">'.number_format($t_del_grandreceivemoney).'</td>
                    <td></td>
                </tr>
                
                <tr>
                    <td align="center">'.$data[count($data) - 1]->f_year.'</td>
                    <td align="center">'.$data[count($data) - 1]->casenum.'</td>
                    <td align="center">'.number_format($data[count($data) - 1]->receivemoney).'</td>
                    <td align="center">'.$data[count($data) - 1]->del_casenum.'</td>
                    <td align="center">'.number_format($data[count($data) - 1]->del_receivemoney).'</td>
                    <td></td>

                    <td align="center">'.$data[count($data) - 1]->grandcasenum.'</td>
                    <td align="center">'.number_format($data[count($data) - 1]->grandreceivemoney).'</td>
                    <td align="center">'.$data[count($data) - 1]->del_grandcasenum.'</td>
                    <td align="center">'.number_format($data[count($data) - 1]->del_grandreceivemoney).'</td>
                    <td></td>
                </tr>
                <tr>
                    <td align="center">以前年度 <br/> (99-109年)</td>
                    <td align="center">'.($t_casenum - $data[count($data) - 1]->casenum).'</td>
                    <td align="center">'.number_format($t_receiveamount - $data[count($data) - 1]->receivemoney).'</td>
                    <td align="center">'.($t_del_casenum - $data[count($data) - 1]->del_casenum).'</td>
                    <td align="center">'.number_format($t_del_receivemoney - $data[count($data) - 1]->del_receivemoney).'</td>
                    <td></td>

                    <td align="center">'.($t_grandcasenum - $data[count($data) - 1]->grandcasenum).'</td>
                    <td align="center">'.number_format($t_grandreceiveamount - $data[count($data) - 1]->grandreceivemoney).'</td>
                    <td align="center">'.($t_del_grandcasenum - $data[count($data) - 1]->del_grandcasenum).'</td>
                    <td align="center">'.number_format($t_del_grandreceivemoney - $data[count($data) - 1]->del_grandreceivemoney).'</td>
                    <td></td>
                </tr>';
            
            for ($i=0; $i < count($data) - 1 ; $i++) { 
                $html .='<tr>
                    <td align="center">'.$data[$i]->f_year.'</td>
                    <td align="center">'. $data[$i]->casenum.'</td>
                    <td align="center">'.number_format($data[$i]->receivemoney).'</td>
                    <td align="center">'. $data[$i]->del_casenum.'</td>
                    <td align="center">'.number_format($data[$i]->del_receivemoney).'</td>';
                    if($i == 0)
                    {
                        $html .= '<td rowspan="'.(count($data) - 1).'"></td>';
                    }
                    $html .= '<td align="center">'. $data[$i]->grandcasenum.'</td>
                    <td align="center">'.number_format($data[$i]->grandreceivemoney).'</td>
                    <td align="center">'. $data[$i]->del_grandcasenum.'</td>
                    <td align="center">'.number_format($data[$i]->del_grandreceivemoney).'</td>';
                $html .='</tr>';
            } 

            $t_receivefee = 0;
            $t_del_receivefee = 0;
            $t_grandreceivefee = 0;
            $t_del_grandreceivefee = 0;
            for ($i=0; $i < count($fee_data) ; $i++) {
                $t_receivefee = $t_receivefee + $fee_data[$i]->receivefee; 
                $t_del_receivefee = $t_del_receivefee + $fee_data[$i]->del_receivefee;
                $t_grandreceivefee = $t_grandreceivefee + $fee_data[$i]->grandreceivefee; 
                $t_del_grandreceivefee = $t_del_grandreceivefee + $fee_data[$i]->del_grandreceivefee;
            }
            $html .='<tr>
                    <td align="center">執行費</td>
                    <td align="center" rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center">'.number_format($t_receivefee).'</td>
                    <td align="center" rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center">'.number_format($t_del_receivefee).'</td>
                    <td rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center" rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center">'.number_format($t_grandreceivefee).'</td>
                    <td align="center" rowspan="'.(count($fee_data)+2).'"></td>
                    <td align="center">'.number_format($t_del_grandreceivefee).'</td>
                    <td></td>
                </tr>
                
                <tr>
                    <td align="center">'.$fee_data[count($fee_data) - 1]->f_year.'</td>
                    <td align="center">'.number_format($fee_data[count($fee_data) - 1]->receivefee).'</td>
                    <td align="center">'.number_format($fee_data[count($fee_data) - 1]->del_receivefee).'</td>
                    

                    <td align="center">'.number_format($fee_data[count($fee_data) - 1]->grandreceivefee).'</td>
                    <td align="center">'.number_format($fee_data[count($fee_data) - 1]->del_grandreceivefee).'</td>
                    
                </tr>
                <tr>
                    <td align="center">以前年度 <br/> (99-109年)</td>
                    <td align="center">'.number_format($t_receivefee - $fee_data[count($fee_data) - 1]->receivefee).'</td>
                    <td align="center">'.number_format($t_del_receivefee - $fee_data[count($fee_data) - 1]->del_receivefee).'</td>
                   

                    <td align="center">'.number_format($t_grandreceivefee - $fee_data[count($fee_data) - 1]->grandreceivefee).'</td>
                    <td align="center">'.number_format($t_del_grandreceivefee - $fee_data[count($fee_data) - 1]->del_grandreceivefee).'</td>
                    
                </tr>';
                for ($i=0; $i < count($fee_data) - 1 ; $i++) { 
                    $html .='<tr>
                        <td align="center">'.$fee_data[$i]->f_year.'</td>
                        <td align="center">'.number_format($fee_data[$i]->receivefee).'</td>
                        <td align="center">'.number_format($fee_data[$i]->del_receivefee).'</td>';
                    $html .= '<td align="center">'.number_format($fee_data[$i]->grandreceivefee).'</td>
                        <td align="center">'.number_format($fee_data[$i]->del_grandreceivefee).'</td>';
                    $html .='</tr>';
                } 
            
            $html .='</tbody></table>';

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
            $spreadsheet = $reader->loadFromString($html);

            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            // $writer->save('write.xlsx'); 
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.(explode('-',explode(',',$dateparam)[1])[0]-1911) .'年'.explode('-',explode(',',$dateparam)[1])[1].'月毒品'.(($_GET['rtype'] == 'A')?'罰金罰鍰':'怠金').'明細表.xlsx"');
            header('Cache-Control: max-age=0');
            $writer->save('php://output'); // download file
    }
    /**
     * 報表查詢-罰金罰鍰怠金明細內容
     *  */ 
    public function export_fineReport_r1_s2(){
        
        $dateparam = $_GET['param'];
        $data = $this->getsqlmod->get_finereport_month_case_detail(explode(',',$dateparam)[0], explode(',',$dateparam)[1], $_GET['rtype']);
		// var_dump($data);
		// exit;
        $htmlString_head = '<table border="1">
                    <thead>
                        <tr>
                            <td align="center"></td>
                            <td align="center">類型</td>
                            <td align="center">案件編號</td>
                            <td align="center">受處分人姓名</td>
                            <td align="center">身份證編號</td>
                            <td align="center">罰鍰/怠金</td>
                            <td align="center">完納日期</td>
                            <td align="center">完納金額</td>
                            <td align="center">繳款日期</td>
                            <td align="center">繳款金額</td>
                            <td align="center">繳納方式</td>
							<td align="center">繳納狀態</td>
                            <td align="center">移送Log</td>
                            <td align="center">憑證Log</td>
                            <td align="center">撤銷註銷Log</td>
                            <td align="center">執行命令Log</td>
                            <td align="center">備註</td>
                        </tr>
                    </thead>
                    <tbody>';
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $stylesheet_name = array('本月收繳', '已註銷本月實收', '累計收繳', '已註銷累計實收');
        $htmlString_body = '';
        $i = 0;
        foreach ($data as $key => $value) {

            for ($j=0; $j < count($value) ; $j++) { 
                $htmlString_body .= '<tr>
                    <td align="center">'.($j + 1).'</td>
                    <td align="center">'.(($value[$j]->type == 'A')?"罰鍰":(($value[$j]->type == 'B')?"怠金":"")).'</td>
                    <td align="center">'.$value[$j]->f_caseid.'</td>
                    <td align="center">'.$value[$j]->f_username.'</td>
                    <td align="center">'.$value[$j]->f_userid.'</td>
                    <td align="center">'.$value[$j]->f_amount.'</td>
                    <td align="center">'.$value[$j]->f_donedate.'</td>
                    <td align="center">'.$value[$j]->f_doneamount.'</td>
                    <td align="center">'.$value[$j]->fpart_date.'</td>
                    <td align="center">'.$value[$j]->fpart_amount.'</td>
					<td align="center">'.$value[$j]->fpart_type.'</td>
                    <td align="center">'.$value[$j]->f_status.'</td>
                    <td align="center">'.(($value[$j]->f_movelog)?str_replace("\n", "<br/>", $value[$j]->f_movelog):'').'</td>
                    <td align="center">'.(($value[$j]->f_cardlog)?str_replace("\n", "<br/>", $value[$j]->f_cardlog):'').'</td>
                    <td align="center">'.(($value[$j]->f_dellog)?str_replace("\n", "<br/>", $value[$j]->f_dellog):'').'</td>
                    <td align="center">'.(($value[$j]->f_execlog)?str_replace("\n", "<br/>", $value[$j]->f_execlog):'').'</td>
                    <td align="center">'.$value[$j]->f_comment.'</td>
                </tr>';
            }
            
            if($i == 0)
            {
                
                $spreadsheet = $reader->loadFromString($htmlString_head. $htmlString_body);
                
            }
            else
            {                
                $spreadhseet = $reader->loadFromString($htmlString_head. $htmlString_body, $spreadsheet);
                
            }
            $spreadsheet->getSheet($i)->setTitle($stylesheet_name[$i]);
            $reader->setSheetIndex(($i+1));

            $i++;
            $htmlString_body = "";
        }

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        // $writer->save('write.xlsx'); 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.(explode('-',explode(',',$dateparam)[1])[0]-1911) .'年'.explode('-',explode(',',$dateparam)[1])[1].'月毒品罰金罰鍰帳務明細.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file

    }

    /**
     * 報表查詢-憑證明細表
     */
    public function export_fineReport_r2()
    {
        $dateparam = $_GET['param'];
        $data = $this->getsqlmod->get_finereport_ticket_receipt(explode(',',$dateparam)[0], explode(',',$dateparam)[1])->result();
        $array = json_decode(json_encode($data),true); 

        $vaild_receipt_count = 0;
        $invaild_receipt_count = 0;
        $vaild_receipt = array();
        $invaild_receipt = array();
        $t_paymoney = 0;
        for ($n=0; $n < count($array) ; $n++) { 
            if($array[$n]['f_comment'] != '作廢')
            {
                $vaild_receipt_count = ($vaild_receipt_count +1);
                array_push($vaild_receipt, $array[$n]['f_receipt_no']);
            }                
            else
            {
                $invaild_receipt_count = ($invaild_receipt_count +1);
                array_push($invaild_receipt, $array[$n]['f_receipt_no']);
            }  
            $t_paymoney = ($t_paymoney + (int)$array[$n]['paymoney']);
        }
        
        $html='';
        
        $html .= '<table border="1">
                    <thead>
                        <tr>
                            <td colspan="17" align="center">
                                我是測試系統刑事警察大隊收入憑證報告單
                            </td>
                        </tr>
                        <tr>
                            <td colspan="17" align="center">
                            中華民國'.(explode('-',explode(',',$dateparam)[1])[0]-1911) .'年'.explode('-',explode(',',$dateparam)[1])[1].'月份
                            </td>
                        </tr>
                        <tr>
                            <td colspan="9">編號：</td>
                            <td colspan="8" align="right">'.(date('Y')-1911) .'年'.date('m').'月'.date('d').'日填報</td>
                        </tr>
                        <tr>
                            <td rowspan="3" align="center">收入憑證名稱</td>
                            <td colspan="5" align="center">領用數</td>
                            <td colspan="5" align="center">本月使用數</td>
                            <td colspan="2" rowspan="2" align="center">截至本月止 <br/> 結存數</td>
                            <td rowspan="3" align="center">開立金額</td>
                            <td colspan="2" align="center">收入分析</td>
                            <td rowspan="3" align="center">備註</td>
                        </tr>
                        <tr>
                            
                            <td colspan="2" align="center">上日結存數</td>
                            <td colspan="2" align="center">本日新領數</td>
                            <td rowspan="2"  align="center">小計份數</td>
                            <td colspan="2" align="center">繳核</td>
                            <td colspan="2" align="center">作廢</td>
                            <td rowspan="2"  align="center">小計份數</td>
                            <td rowspan="2" align="center">本日實收金額</td>
                            <td rowspan="2" align="center">本日繳庫金額</td>
                            
                        </tr>
                        <tr>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                            <td align="center">份數</td>
                            <td align="center">起迄號碼</td>
                        </tr>
                    </thead>
                    <tbody>';
                    $html .= '<tr>';
                    $html .= '
                        <td  align="center">一般收款收據</td>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td ></td>                        
                        <td  align="center">'.$vaild_receipt_count.'</td>
                        <td  align="center">'.implode("、",$vaild_receipt).'</td>
                        <td  align="center">'.$invaild_receipt_count.'</td>
                        <td  align="center">'.implode("、",$invaild_receipt).'</td>
                        <td  align="center">'.count($data).'</td>
                        <td ></td>
                        <td ></td>                        
                        <td  align="center">'.number_format($t_paymoney) .'</td>
                        <td  align="center">'.number_format($t_paymoney) .'</td>
                        <td ></td>
                        <td rowspan="8"></td>';
                        $html .= '</tr>';

                        for ($i=0; $i < 6; $i++) { 
                            $html .= '<tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>';
                            

                            $html .= '</tr>';
                        }
                        
                    
                    
            $html .= '</tbody>
            <tfoot>
            <tr>
                <td>合計</td>
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td></td>
                <td colspan="2"  align="center">'.$vaild_receipt_count.'</td>
                <td colspan="2" align="center">'.$invaild_receipt_count.'</td>
                <td  align="center">'.count($data).'</td>
                <td colspan="2"></td>
                <td align="center">'.number_format($t_paymoney) .'</td>
                <td align="center">'.number_format($t_paymoney) .'</td>
                <td ></td>
            </tr>
            </tfoot>
            </table>';   
            $html.='<table>
                <tr>
                    <td colspan="3">製表人</td>
                    <td colspan="3">業務主管</td>
                    <td colspan="3">出納</td>
                    <td colspan="3">主辦會計</td>
                    <td colspan="3">機關首長</td>
                </tr>
            </table>';    

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
            $spreadsheet = $reader->loadFromString($html);

            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            // $writer->save('write.xlsx'); 
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.(explode('-',explode(',',$dateparam)[1])[0]-1911) .'年'.explode('-',explode(',',$dateparam)[1])[1].'月憑證明細表.xlsx"');
            header('Cache-Control: max-age=0');
            $writer->save('php://output'); // download file
    }

    // 收繳登記 - 轉正專案
    public function export_fineCPPJ_lists_by_indiv(){
        $id = $this->uri->segment(3);
        $data = $this->getsqlmod->get_fineCPPJ_lists_user_in_distinct($id)->result();
        
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        for ($i=0; $i < count($data) ; $i++) { 
             $htmlString = '<table border="1">
                            <thead>
                                <tr style="height:1.5em;">
                                    <td colspan="14" align="center" style="font-size:14px;">我是測試系統<br/>'. (date('Y')-1911) . '年毒品轉正' . $data[$i]->listtype.'收繳登記簿</td>
                                </tr>
                                <tr>
                                    <td colspan="14" align="right">列印日期：'.(date('Y')-1911).'年'.date('m').'月'.date('d').'日</td>
                                </tr>
                                <tr>
                                    <td align="center">年度</td>
                                    <td align="center">月份</td>
                                    <td align="center">流水號</td>
                                    <td align="center">處分年度</td>
                                    <td align="center">案件</td>
                                    <td align="center">受處分人</td>
                                    <td align="center">繳款日期</td>
                                    <td align="center">繳款方式</td>
                                    <td align="center">'.(($data[$i]->listtype == "完納")?"繳款金額":"本期核銷金額").'</td>
                                    <td align="center">金額</td>
                                    <td align="center">'.(($data[$i]->listtype == "完納")?"本期核銷金額":"餘額").'</td>
                                    <td align="center">執行費</td>
                                    <td align="center">狀態</td>
                                    <td align="center">備註</td>
                                </tr>
                            </thead>
                            <tbody>';
                $detail = $this->getsqlmod->get_fineCPPJ_list_user_pay_detail($data[$i]->f_no)->result();
                
                $fpart_amount_sum = 0;
                $f_fee_sum = 0;
                $done_amount = 0;
                
                for ($j=0; $j < count($detail); $j++) { 
                    if($data[$i]->listtype == "完納")
                    {
                        $fpart_amount_sum = ($fpart_amount_sum + (int)$detail[$j]->f_doneamount + (int)$detail[$j]->f_fee);
                        $done_amount = $detail[$j]->f_doneamount;
                    }
                    else
                    {
                        $fpart_amount_sum = ($fpart_amount_sum + $detail[$j]->fpart_amount);
                    }
                    
                    $f_fee_sum = ($f_fee_sum + $detail[$j]->f_fee);

                    $htmlString .= '<tr>
                        <td align="center">'.(($data[$i]->listtype == "完納")?$detail[$j]->d_year:$detail[$j]->year).'</td>
                        <td align="center">'.(($data[$i]->listtype == "完納")?$detail[$j]->d_month:$detail[$j]->month).'</td>
                        <td align="center">'.(($j == 0)?$detail[$j]->f_no:"").'</td>
                        <td align="center">'.$detail[$j]->f_year.'</td>
                        <td align="center">'.$detail[$j]->f_caseid.'</td>
                        <td align="center">'.$detail[$j]->f_username.'</td>
                        <td align="center">'.(($data[$i]->listtype == "完納")?$detail[$j]->donedate:$detail[$j]->paydate).'</td>
                        <td align="center">'.$detail[$j]->fpart_type.'</td>
                        <td align="center">$'.(($data[$i]->listtype == "完納")?number_format((int)$detail[$j]->f_doneamount + (int)$detail[$j]->f_fee):number_format($detail[$j]->fpart_amount)).'</td>
                        <td align="center">$'.number_format((($detail[$j]->type == "A")?((int)$detail[$j]->f_amount*10000):$detail[$j]->f_amount)).'</td>
                        <td align="center">$'.(($data[$i]->listtype == "完納")?number_format($detail[$j]->f_doneamount):number_format((($detail[$j]->type == "A")?((int)$detail[$j]->f_amount*10000):$detail[$j]->f_amount) - (($fpart_amount_sum < 0)?($detail[$j]->f_amount + $fpart_amount_sum):$fpart_amount_sum))).'</td>
                        <td align="center">'.(((int)$detail[$j]->f_fee == 0 )?"":"$".number_format($detail[$j]->f_fee)).'</td>
                        <td align="center">'.$detail[$j]->f_status.'</td>
                        <td align="center">'.$detail[$j]->f_comment.'</td>
                    </tr>';
                    
                }
                
                        
                $htmlString .= '</tbody>
                <tfoot>
                    <tr>
                        <td>合計</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>$'. number_format($fpart_amount_sum) .'</td>
                        <td></td>
                        <td>'.(($data[$i]->listtype == "完納")?("$".number_format($done_amount)):"").'</td>
                        <td>$'. number_format($f_fee_sum) .'</td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
                </table>';

            if($i == 0)
            {
                
                $spreadsheet = $reader->loadFromString($htmlString);
                
            }
            else
            {                
                $spreadhseet = $reader->loadFromString($htmlString, $spreadsheet);
                
            }
            $spreadsheet->getSheet($i)->setTitle($data[$i]->f_caseid);
            $reader->setSheetIndex(($i+1));
         }
            
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        // $writer->save('write.xlsx'); 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data[0]->fcp_no.'收繳登記.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file
    }

    public function finecard_importFinedata()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        
		$ext = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);
        // 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }

        try {
            $spreadsheet = $reader->load($_FILES['upload']['tmp_name']);
			
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }
		

        $sheet = $spreadsheet->getActiveSheet();
        $row = $sheet->getHighestRow();
        try {
            for ($i = 2; $i <= $row; $i++) {
                // 獲取行數的數據
                $rowData = array(
                    "fi_no" => 1,
                    "f_type" => 'A',
                    "f_certno" => $sheet->getCellByColumnAndRow(1, $i)->getValue(), 
                    "f_date" => $sheet->getCellByColumnAndRow(9, $i)->getValue(),   
                    "f_debtno" => $sheet->getCellByColumnAndRow(10, $i)->getValue(), 
                    "f_amount" => $sheet->getCellByColumnAndRow(11, $i)->getValue(), 
                    "f_cardmount" => $sheet->getCellByColumnAndRow(12, $i)->getValue(),
                    "f_moveno" => $sheet->getCellByColumnAndRow(13, $i)->getValue()
                );
                $this->getsqlmod->addFinecardData($rowData);
                
                
            }
            $response = array(
                'status' => 'OK',
                'message' => 'Successfully upload.'
            );
            $this->output
                    ->set_status_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
            exit;
        } catch (\Throwable $th) {
            $response = array(
                'status' => 'ERROR',
                'message' => $th
            );
            $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
            exit;
        }
    }

    public function finecard_importLazydata()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
		
        $ext = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);
        // 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }

        try {
            $spreadsheet = $reader->load($_FILES['upload']['tmp_name']);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }

        $sheet = $spreadsheet->getActiveSheet();
        $row = $sheet->getHighestRow();
        try {
            for ($i = 2; $i <= $row; $i++) {
                // 獲取行數的數據
                $rowData = array(
                    "fi_no" => 2,
                    "f_type" => 'B',
                    "f_certno" => $sheet->getCellByColumnAndRow(1, $i)->getValue(), 
                    "f_date" => $sheet->getCellByColumnAndRow(9, $i)->getValue(),   
                    "f_debtno" => $sheet->getCellByColumnAndRow(10, $i)->getValue(), 
                    "f_amount" => $sheet->getCellByColumnAndRow(11, $i)->getValue(), 
                    "f_cardmount" => $sheet->getCellByColumnAndRow(12, $i)->getValue(),
                    "f_moveno" => $sheet->getCellByColumnAndRow(13, $i)->getValue()
                );
                $this->getsqlmod->addFinecardData($rowData);
            }
            $response = array(
                'status' => 'OK',
                'message' => 'Successfully upload.'
            );
            $this->output
                    ->set_status_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
            exit;
        } catch (\Throwable $th) {
            $response = array(
                'status' => 'ERROR',
                'message' => $th
            );
            $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
            exit;
        }
    }

    public function surcharge_import()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        
        $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }
        

        try {
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }

        $sheet = $spreadsheet->getActiveSheet();
        $row = $sheet->getHighestRow();
        try {
            // 獲取行數的數據
            for ($i = 2; $i <= $row; $i++) {
                if(null == $sheet->getCellByColumnAndRow(18, $i)->getValue())
                {
                    break;
                }
                    
                $query = $this->getsqlmod->getdata(1,1)->result();
                $countid = ((isset($query[0]))?$query[0]->c_num:'0000');
                $lastNum = (int)mb_substr($countid, -4, 4);
                $lastNum++;
                $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
                $taiwan_date = date('Y')-1911; 
                $c_num ='CN' . $taiwan_date.$value;
                
                //case 案件
                $rowData = array(
                    "c_num" => $c_num,
                    "e_empno" => $sheet->getCellByColumnAndRow(18, $i)->getValue(),
                    "r_office" => $sheet->getCellByColumnAndRow(6, $i)->getValue(),
                    "r_office1" => $sheet->getCellByColumnAndRow(7, $i)->getValue(),
                    "s_office" => $sheet->getCellByColumnAndRow(28, $i)->getValue(),
                    "s_date" => $this->tranfer2ADyear($sheet->getCellByColumnAndRow(22, $i)->getValue() . str_pad($sheet->getCellByColumnAndRow(23, $i)->getValue(),2,'0',STR_PAD_LEFT) . str_pad($sheet->getCellByColumnAndRow(24, $i)->getValue(),2,'0',STR_PAD_LEFT)),
                    "s_time" => ((strlen($sheet->getCellByColumnAndRow(25, $i)->getValue()) > 1 )?$sheet->getCellByColumnAndRow(25, $i)->getValue():('0'. $sheet->getCellByColumnAndRow(25, $i)->getValue())) . ':' . ((strlen($sheet->getCellByColumnAndRow(26, $i)->getValue()) > 1 )?$sheet->getCellByColumnAndRow(26, $i)->getValue():('0'. $sheet->getCellByColumnAndRow(26, $i)->getValue())),
                    "s_place" => $sheet->getCellByColumnAndRow(27, $i)->getValue()
                );
				

                // 嫌疑人
                $rowData2 = array(
                    "s_ic" => $sheet->getCellByColumnAndRow(18, $i)->getValue(),
                    "s_gender" => $sheet->getCellByColumnAndRow(14, $i)->getValue(),
                    "s_name" => $sheet->getCellByColumnAndRow(13, $i)->getValue(), 
                    "s_nation" => '中華民國',   
                    "s_birth" => $this->tranfer2ADyear($sheet->getCellByColumnAndRow(15, $i)->getValue() . str_pad($sheet->getCellByColumnAndRow(16, $i)->getValue(),2,'0',STR_PAD_LEFT) . str_pad($sheet->getCellByColumnAndRow(17, $i)->getValue(),2,'0',STR_PAD_LEFT)),
                    "s_U_Col" => ((null !== $sheet->getCellByColumnAndRow(56, $i)->getValue())?'是':'否'),
					"s_dpzipcode" => null,
                    "s_dpaddress" => $sheet->getCellByColumnAndRow(21, $i)->getValue(),
					"s_rpzipcode" => null,
                    "s_rpaddress" => $sheet->getCellByColumnAndRow(20, $i)->getValue(),
                    "s_CMethods" => '持有',
                    "s_cnum" => $c_num,
                    "s_roffice" => $sheet->getCellByColumnAndRow(6, $i)->getValue(),
                    "s_roffice1" => $sheet->getCellByColumnAndRow(7, $i)->getValue(),
                    "s_fno" => $sheet->getCellByColumnAndRow(11, $i)->getValue(),
                    "s_fdate" => $this->tranfer2ADyear($sheet->getCellByColumnAndRow(8, $i)->getValue() . str_pad($sheet->getCellByColumnAndRow(9, $i)->getValue(),2,'0',STR_PAD_LEFT) . str_pad($sheet->getCellByColumnAndRow(10, $i)->getValue(),2,'0',STR_PAD_LEFT)),
                    "s_utest_num" => $sheet->getCellByColumnAndRow(56, $i)->getValue(),
                    "s_sac_state" => '確認送出裁罰'
                );
				if(!empty(trim($sheet->getCellByColumnAndRow(40, $i)->getValue())))
				{
					// 法定代理人
					$rowData2_fadi = array(
						"s_fadai_sic"=>$sheet->getCellByColumnAndRow(18, $i)->getValue(),
						"s_fadai_name"=>$sheet->getCellByColumnAndRow(40, $i)->getValue(),
						"s_fadai_gender"=>$sheet->getCellByColumnAndRow(41, $i)->getValue(),
						"s_fadai_ic"=>$sheet->getCellByColumnAndRow(42, $i)->getValue(),
						"s_fadai_birth"=>$this->tranfer2ADyear($sheet->getCellByColumnAndRow(44, $i)->getValue() . str_pad($sheet->getCellByColumnAndRow(45, $i)->getValue(),2,'0',STR_PAD_LEFT) . str_pad($sheet->getCellByColumnAndRow(46, $i)->getValue(),2,'0',STR_PAD_LEFT)),
						"s_fadai_address"=>$sheet->getCellByColumnAndRow(47, $i)->getValue()
					);
					$this->getsqlmod->addSuspFadai($rowData2_fadi);
				}
				
                $this->getsqlmod->addcases($rowData);
                $s_num = $this->getsqlmod->addsuspect_lastid($rowData2);

				$code =  '21719'.$sheet->getCellByColumnAndRow(5, $i)->getValue();
				$code1 = preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY);
				$code2=0;
				for($ci=0;$ci<count($code1);$ci++){
					if($ci==0||$ci==3||$ci==6||$ci==9||$ci==12||$ci==15) $code2=$code2+$code1[$ci]*3;
					if($ci==1||$ci==4||$ci==7||$ci==10||$ci==13) $code2=$code2+($code1[$ci]*7);
					if($ci==2||$ci==5||$ci==8||$ci==11||$ci==14) $code2=$code2+($code1[$ci]*1);
				}
				$code3 = $code2%10;
				$code4 = 10-$code3;
				$fin_bvc = $code.$code4;
				
				$sbirth = $this->tranfer2ADyear($sheet->getCellByColumnAndRow(15, $i)->getValue() . str_pad($sheet->getCellByColumnAndRow(16, $i)->getValue(),2,'0',STR_PAD_LEFT) . str_pad($sheet->getCellByColumnAndRow(17, $i)->getValue(),2,'0',STR_PAD_LEFT));

				$sfdate = $this->tranfer2ADyear($sheet->getCellByColumnAndRow(22, $i)->getValue() . str_pad($sheet->getCellByColumnAndRow(23, $i)->getValue(),2,'0',STR_PAD_LEFT) . str_pad($sheet->getCellByColumnAndRow(24, $i)->getValue(),2,'0',STR_PAD_LEFT));
				// 處分書
				$finedoc = array(
					"fd_num" => $sheet->getCellByColumnAndRow(1, $i)->getValue(),
					"fd_send_num" => $sheet->getCellByColumnAndRow(5, $i)->getValue(),
					"fd_cnum" => $c_num,
					"fd_snum" => $s_num,
					"fd_sic" => $sheet->getCellByColumnAndRow(18, $i)->getValue(),
					"fd_drug_msg" => $sheet->getCellByColumnAndRow(33, $i)->getValue(),
					"fd_psystem" => $sheet->getCellByColumnAndRow(48, $i)->getValue(),
					"fd_bvc" => $sheet->getCellByColumnAndRow(65, $i)->getValue(),
					"fd_target" => $sheet->getCellByColumnAndRow(13, $i)->getValue().((!$this->isAudlt($sbirth, $sfdate))?','.$sheet->getCellByColumnAndRow(40, $i)->getValue():''), 
					"fd_address" => $sheet->getCellByColumnAndRow(21, $i)->getValue().((!$this->isAudlt($sbirth, $sfdate))?','.$sheet->getCellByColumnAndRow(21, $i)->getValue():''),
					"fd_zipcode" => $sheet->getCellByColumnAndRow(38, $i)->getValue(),
					"fd_limitdate" => $this->tranfer2ADyear($sheet->getCellByColumnAndRow(35, $i)->getValue() . str_pad($sheet->getCellByColumnAndRow(36, $i)->getValue(),2,'0',STR_PAD_LEFT) . str_pad($sheet->getCellByColumnAndRow(37, $i)->getValue(),2,'0',STR_PAD_LEFT)),
					"fd_phone" => $sheet->getCellByColumnAndRow(19, $i)->getValue(),
					"fd_summary"=>$sheet->getCellByColumnAndRow(63, $i)->getValue()
				);
				$this->getsqlmod->addFD($finedoc);

				$delivery = array(
					'fdd_fdnum'  => $sheet->getCellByColumnAndRow(1, $i)->getValue(),
					'fdd_snum'  => $s_num,
					'fdd_cnum'  => $c_num,
				);
				$this->getsqlmod->adddelivery($delivery);

                if($sheet->getCellByColumnAndRow(32, $i)->getValue())
                {
                    $drupary = explode('、', $sheet->getCellByColumnAndRow(32, $i)->getValue());
                }
				$query = $this->getsqlmod->get1DrugCount()->result(); 
				$countid = ((isset($query[0]))?$query[0]->e_id:'00000');
				$lastNum = (int)mb_substr($countid, -5, 5);
				$lastNum++;
				$value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
				$taiwan_date = date('Y')-1911; 
				$e_id='EN' . $taiwan_date . $value;
				
				// 持有毒品
				$rowData3 = array(
					"e_id" => $e_id,
					"e_c_num" => $c_num,
					"e_empno" => $this -> session -> userdata('uic'),
					"e_type" => $sheet->getCellByColumnAndRow(32, $i)->getValue(),
					"e_name" => $sheet->getCellByColumnAndRow(33, $i)->getValue(),
					"e_1_N_W" => $sheet->getCellByColumnAndRow(34, $i)->getValue(),
					"e_suspect" => $sheet->getCellByColumnAndRow(13, $i)->getValue(),
					"e_s_ic" => $sheet->getCellByColumnAndRow(18, $i)->getValue()
				);
				$patterns = array('一','二','三','四');
				$replacements = array(1,2,3,4);
				foreach ($drupary as $ind) { 
					// 毒品成份
                    $rowData4 = array(
                        "df_ingredient" => $ind,
						"df_level" => str_replace($patterns, $replacements, $sheet->getCellByColumnAndRow(31, $i)->getValue()),
                        "df_drug" => $e_id,
                        "df_count" => 0
                    );

					$this->getsqlmod->adddrugInd($rowData4);
                }

				$this->getsqlmod->adddrug($rowData3);
            }
            echo 'ok';
        } catch (\Throwable $th) {
            echo $th;
        }
    }

	public function drugbug_import()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        
		$ext = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);
        // 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Ods();
                break;
        }

        try {
            $spreadsheet = $reader->load($_FILES['upload']['tmp_name']);
			
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }
		

        $sheet = $spreadsheet->getActiveSheet();
        $row = $sheet->getHighestRow();
        try {
            for ($i = 2; $i <= $row; $i++) {
				if(!$this->getsqlmod->checkDrugbugCases(preg_replace('/\s+/', '', $sheet->getCellByColumnAndRow(2, $i)->getValue()), $this->tranfer2ADyear($this->tranfer2RCyear($sheet->getCellByColumnAndRow(28, $i)->getValue())), substr($sheet->getCellByColumnAndRow(31, $i)->getValue(),11,5), $sheet->getCellByColumnAndRow(32, $i)->getValue()) > 0)
				{
					$query = $this->getsqlmod->getdata(1,1)->result();
					$countid = ((isset($query[0]))?$query[0]->c_num:'0000');
					$lastNum = (int)mb_substr($countid, -4, 4);
					$lastNum++;
					$value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
					$taiwan_date = date('Y')-1911; 
					$c_num ='CN' . $taiwan_date.$value;
					
					//case 案件
					$rowData = array(
						"c_num" => $c_num,
						"r_office" => $sheet->getCellByColumnAndRow(30, $i)->getValue(),
						"s_office" => $sheet->getCellByColumnAndRow(29, $i)->getValue(),
						"s_date" => $this->tranfer2ADyear($this->tranfer2RCyear($sheet->getCellByColumnAndRow(28, $i)->getValue())),
						"s_time" => substr($sheet->getCellByColumnAndRow(31, $i)->getValue(),11,5),
						"s_place" => $sheet->getCellByColumnAndRow(32, $i)->getValue(),
						"detection_process" => $sheet->getCellByColumnAndRow(34, $i)->getValue(),
						"e_empno" => $sheet->getCellByColumnAndRow(39, $i)->getValue(),
					);
					

					// 嫌疑人
					$rowData2 = array(
						"s_ic" => strtoupper($sheet->getCellByColumnAndRow(4, $i)->getValue()),
						"s_gender" => ((substr(trim($sheet->getCellByColumnAndRow(4, $i)->getValue()),1,1) == '1')?'男':'女'),
						"s_name" => preg_replace('/\s+/', '', $sheet->getCellByColumnAndRow(2, $i)->getValue()), 
						"s_nation" => ((empty($sheet->getCellByColumnAndRow(5, $i)->getValue()) || null == $sheet->getCellByColumnAndRow(5, $i)->getValue())?'中華民國':$sheet->getCellByColumnAndRow(5, $i)->getValue()),   
						"s_birth" => substr($sheet->getCellByColumnAndRow(7, $i)->getValue(),0,10),
						"s_U_Col" => $sheet->getCellByColumnAndRow(10, $i)->getValue(),
						"s_job" => $sheet->getCellByColumnAndRow(8, $i)->getValue(),
						"s_edu" => $sheet->getCellByColumnAndRow(9, $i)->getValue(),
						"s_nname" => $sheet->getCellByColumnAndRow(3, $i)->getValue(),
						"s_feature"=> $sheet->getCellByColumnAndRow(6, $i)->getValue(),
						"s_gang" => $sheet->getCellByColumnAndRow(16, $i)->getValue(),
						"s_CMethods" => $sheet->getCellByColumnAndRow(17, $i)->getValue(),
						"s_dpzipcode" => null,
						"s_dpaddress" => $sheet->getCellByColumnAndRow(12, $i)->getValue(),
						"s_rpzipcode" => null,
						"s_rpaddress" => $sheet->getCellByColumnAndRow(14, $i)->getValue(),
						"s_cnum" => $c_num,
						"s_roffice" => $sheet->getCellByColumnAndRow(30, $i)->getValue(),
						's_noresource' => $sheet->getCellByColumnAndRow(75, $i)->getValue()
					);

					$this->getsqlmod->addcases($rowData);
					$s_num = $this->getsqlmod->addsuspect_lastid($rowData2);

					$phone = array(
						'p_no' =>((!empty($sheet->getCellByColumnAndRow(43, $i)->getValue()))?$sheet->getCellByColumnAndRow(43, $i)->getValue():explode(',', $sheet->getCellByColumnAndRow(47, $i)->getValue())[0]),
						'p_oppw' => $sheet->getCellByColumnAndRow(44, $i)->getValue(),
						'p_imei' => $sheet->getCellByColumnAndRow(45, $i)->getValue(),
						'p_conf' => (($sheet->getCellByColumnAndRow(46, $i)->getValue() == '0')?'否':'是'),
						'p_s_ic' => $sheet->getCellByColumnAndRow(4, $i)->getValue(),
						'p_s_num' => $s_num,
						// 'p_s_cnum' => $CID
					);
					$p_num = $this->getsqlmod->addphone($phone);

					if($sheet->getCellByColumnAndRow(48, $i)->getValue())
					{
						$phonerecAry = explode(',', $sheet->getCellByColumnAndRow(48, $i)->getValue());					

						if(count($phonerecAry) > 1)
						{
							$phonerecorinoAry = explode(',', $sheet->getCellByColumnAndRow(47, $i)->getValue());
							$phonerecnoAry = explode(',', $sheet->getCellByColumnAndRow(49, $i)->getValue());
							$phonerecnameAry = explode(',', $sheet->getCellByColumnAndRow(50, $i)->getValue());
							$phonerecrelAry = explode(',', $sheet->getCellByColumnAndRow(50, $i)->getValue());
							for ($prindex=0; $prindex < count($phonerecAry); $prindex++) { 
								$phone_rec = array(
									'pr_path' => (($phonerecAry[$prindex] == '1')?'撥入':'撥出'),
									'pr_phone' => $phonerecnoAry[$prindex],
									'pr_name' => $phonerecnameAry[$prindex],
									'pr_time' => null,
									'pr_relationship' => $phonerecrelAry[$prindex],
									'pr_p_no' => $phonerecorinoAry[$prindex],
									'pr_s_ic' => $sheet->getCellByColumnAndRow(4, $i)->getValue(),
									'pr_p_num' => $p_num
								);
								$this->getsqlmod->addphone_rec($phone_rec);
							}
						}
						else
						{
							$phone_rec = array(
								'pr_path' => (($sheet->getCellByColumnAndRow(48, $i)->getValue() == '1')?'撥入':'撥出'),
								'pr_phone' => $sheet->getCellByColumnAndRow(49, $i)->getValue(),
								'pr_name' => $sheet->getCellByColumnAndRow(50, $i)->getValue(),
								'pr_time' => null,
								'pr_relationship' => $sheet->getCellByColumnAndRow(50, $i)->getValue(),
								'pr_p_no' => $sheet->getCellByColumnAndRow(43, $i)->getValue(),
								'pr_s_ic' => $sheet->getCellByColumnAndRow(4, $i)->getValue(),
								'pr_p_num' => $s_num
							);
							$this->getsqlmod->addphone_rec($phone_rec);
						}					
					}

					// 車輛
					if(!empty($sheet->getCellByColumnAndRow(40, $i)->getValue()))
					{
						$vehicleAry = explode(',', $sheet->getCellByColumnAndRow(40, $i)->getValue());					

						if(count($vehicleAry) > 1)
						{
							$v_typeAry = explode(',', $sheet->getCellByColumnAndRow(40, $i)->getValue());
							$v_carno = explode(',', $sheet->getCellByColumnAndRow(41, $i)->getValue());
							$v_color = explode(',', $sheet->getCellByColumnAndRow(42, $i)->getValue());
							for ($vindex=0; $vindex < count($vehicleAry); $vindex++) { 
								$vehicle = array(
									'v_type' => $v_typeAry[$vindex],
									'v_license_no' => $v_carno[$vindex],
									'v_color' => $v_color[$vindex],
									'v_owner' => $sheet->getCellByColumnAndRow(4, $i)->getValue(),
									'v_s_num' => $s_num
								);
								$this->getsqlmod->addcar($vehicle);
							}
						}
						else
						{
							$vehicle = array(
								'v_type' => $sheet->getCellByColumnAndRow(40, $i)->getValue(),
								'v_license_no' => $sheet->getCellByColumnAndRow(41, $i)->getValue(),
								'v_color' => $sheet->getCellByColumnAndRow(42, $i)->getValue(),
								'v_owner' => $sheet->getCellByColumnAndRow(4, $i)->getValue(),
								'v_s_num' => $s_num
							);
							$this->getsqlmod->addcar($vehicle);
						}
						
					}

					
					$patterns = array('第一級','第二級','第三級','第四級');
					$replacements = array(1,2,3,4);

					

					// 初驗
					if(!empty($sheet->getCellByColumnAndRow(80, $i)->getValue()))
					{
						$ename = explode(',', $sheet->getCellByColumnAndRow(78, $i)->getValue());
						$drupary = explode(',', $sheet->getCellByColumnAndRow(80, $i)->getValue());
						$druplevelary = explode(',', $sheet->getCellByColumnAndRow(79, $i)->getValue());
						$drupweightary = explode(',', $sheet->getCellByColumnAndRow(81, $i)->getValue());

						if(count($drupary) > 0)
						{
							$index = 0;
							foreach ($drupary as $ind) { 							
								$query = $this->getsqlmod->get1DrugCount()->result(); 
								$countid = ((isset($query[0]))?$query[0]->e_id:'00000');
								$lastNum = (int)mb_substr($countid, -5, 5);
								$lastNum++;
								$value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
								$taiwan_date = date('Y')-1911; 
								$e_id='EN' . $taiwan_date . $value;
								
								// 持有毒品
								$rowData3 = array(
									"e_id" => $e_id,
									"e_c_num" => $s_num,
									"e_type" => $drupary[$index],
									"e_name" => $ename[$index],
									"e_1_N_W" => $drupweightary[$index],
									"e_suspect" => preg_replace('/\s+/', '', $sheet->getCellByColumnAndRow(2, $i)->getValue()),
									"e_s_ic" => $sheet->getCellByColumnAndRow(4, $i)->getValue(),
									'e_state' => '單位保管中'
								);
								$this->getsqlmod->adddrug($rowData3);
								// 毒品成份
								$rowData4 = array(
									"df_type" => '初驗',
									"df_ingredient" => $ind,
									"df_level" => str_replace($patterns, $replacements, $druplevelary[$index]),
									"df_drug" => $e_id,
									"df_count" => 0,
									"df_PNW" => $drupweightary[$index]
								);
			
								$this->getsqlmod->adddrugInd($rowData4);
								$index++;
							}
						}
						else
						{
							if(!empty($sheet->getCellByColumnAndRow(80, $i)->getValue()))
							{
								$query = $this->getsqlmod->get1DrugCount()->result(); 
								$countid = ((isset($query[0]))?$query[0]->e_id:'00000');
								$lastNum = (int)mb_substr($countid, -5, 5);
								$lastNum++;
								$value = str_pad($lastNum,5,'0',STR_PAD_LEFT);
								$taiwan_date = date('Y')-1911; 
								$e_id='EN' . $taiwan_date . $value;
								$rowData4 = array(
									"df_type" => '初驗',
									"df_ingredient" => $sheet->getCellByColumnAndRow(80, $i)->getValue(),
									"df_level" => str_replace($patterns, $replacements, $sheet->getCellByColumnAndRow(79, $i)->getValue()),
									"df_drug" => $e_id,
									"df_count" => 0,
									"df_PNW" => $sheet->getCellByColumnAndRow(81, $i)->getValue()
								);
			
								$this->getsqlmod->adddrugInd($rowData4);
							}
						
						}
					}
				}

                

                // // 初驗                
                // if(!empty($sheet->getCellByColumnAndRow(19, $i)->getValue()))
                // {
                //     $firstdrupary = explode(',', $sheet->getCellByColumnAndRow(19, $i)->getValue());

				// 	if(count($firstdrupary) > 0)
				// 	{
				// 		$indexj = 0;
				// 		foreach ($firstdrupary as $firstind) { 
				// 			// 毒品成份
				// 			$rowData4 = array(
				// 				"df_type" => '初驗',
				// 				"df_ingredient" => $firstind,
				// 				"df_drug" => $e_id
				// 			);
		
				// 			$this->getsqlmod->adddrugInd($rowData4);
				// 			$indexj++;
				// 		}
				// 	}
				// 	else
				// 	{
				// 		if(!empty($sheet->getCellByColumnAndRow(19, $i)->getValue()))
				// 		{
				// 			$rowData4 = array(
				// 				"df_type" => '初驗',
				// 				"df_ingredient" => $sheet->getCellByColumnAndRow(19, $i)->getValue(),
				// 				"df_drug" => $e_id
				// 			);
		
				// 			$this->getsqlmod->adddrugInd($rowData4);
				// 		}
						
				// 	}
                // }
            }
            $response = array(
                'status' => 'OK',
                'message' => 'Successfully upload.'
            );
            $this->output
                    ->set_status_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
            exit;
        } catch (\Throwable $th) {
            $response = array(
                'status' => 'ERROR',
                'message' => $th
            );
            $this->output
                    ->set_status_header(400)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
            exit;
        }
    }

    public function courses_import()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);

		if(empty($_FILES['upload_file']['name']))
		{
			echo 'error';
			exit;
		}
        
        $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }
        

        try {
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }

        $sheet = $spreadsheet->getActiveSheet();
        $highestRow = $sheet->getHighestRow(); // e.g. 10
        $rowData = array();
        try {
            for ($row = 3; $row <= $highestRow; ++$row) {
            	if (!empty($sheet -> getCellByColumnAndRow(3, $row)->getFormattedValue())) {
            		array_push($rowData, array(
            			"c_date" => $sheet -> getCellByColumnAndRow(2, $row) -> getFormattedValue().PHP_EOL,
            			"c_week" => $sheet -> getCellByColumnAndRow(3, $row) -> getFormattedValue().PHP_EOL,
            			"c_time" => $sheet -> getCellByColumnAndRow(4, $row) -> getFormattedValue().PHP_EOL,
            			"c_place" => $sheet -> getCellByColumnAndRow(5, $row) -> getFormattedValue().PHP_EOL,
            			"c_addr" => $sheet -> getCellByColumnAndRow(5, $row) -> getFormattedValue().PHP_EOL
            		));

            	}

            }
			
            foreach($rowData as $data) {
            	$this -> getsqlmod -> addCourseData($data);
            }
            echo 'ok';
        } catch (\Throwable $th) {
            echo $th;
        }
    }
	// 證物入庫清冊
    public function export_drugsreceive_list(){
        $id = $this->uri->segment(3);
		$search = array(
			'startdate' => $_GET['startdate'],
			'enddate' => $_GET['enddate'],
			'e_state' => '送警察局'
		);
		$data = $this->getsqlmod->getDrugRoffice($search)->result_array(); 
        // var_dump($data);
		// exit;
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$i = 0;
		$htmlString = '';
		foreach ($data as $row) {
			
			$detail = $this->getsqlmod->getDrugRoffice_detail($search, $row['r_office'])->result_array(); // 抓sic
			
             $htmlString = '<table border="1">
                            <thead>
                                <tr style="height:1.5em;">
                                    <td colspan="9" align="center" style="font-size:14px;">'. (date('Y')-1911) . '年' .str_pad(date('m'),2,'0',STR_PAD_LEFT) .'月'. str_pad(date('d'),2,'0',STR_PAD_LEFT).'日'.$row['r_office'].'入庫清冊</td>
                                </tr>
                                <tr>
                                    <td colspan="9" align="right">列印日期：'.(date('Y')-1911).'年'.date('m').'月'.date('d').'日</td>
                                </tr>
                                <tr>
                                    <td align="center">地檢署或地方法院<br/>
									(保號或囑託銷燬文號)</td>
                                    <td align="center">CA系統編號</td>
                                    <td align="center">列管編號</td>
                                    <td align="center">查獲單位處分書文號</td>
                                    <td align="center">受處分人姓名</td>
                                    <td align="center">身分證號</td>
                                    <td align="center">毒品種類</td>
                                    <td align="center">查扣物件<br/>
									(包、個、瓶、張、支、粒)</td>
									<td>重量(g)<br/>
									(或微量無法磅秤)</td>
                                </tr>
                            </thead>
                            <tbody>';

				foreach ($detail as $detail_row) {                     

                    $htmlString .= '<tr>
                        <td align="center"></td>
                        <td align="center">'.$detail_row['fd_psystem'].'</td>
                        <td align="center">'.$detail_row['fd_num'].'</td>
                        <td align="center">'.$detail_row['s_fno'].'</td>
                        <td align="center">'.$detail_row['fd_target'].'</td>
                        <td align="center">'.$detail_row['fd_sic'].'</td>
                        <td align="center">'.$detail_row['e_name'].'</td>
                        <td align="center">'.$detail_row['e_type'].'</td>
                        <td align="center">$'.$detail_row['e_1_N_W'].'</td>
                    </tr>';
                    
                }
                
                        
                $htmlString .= '</tbody>
                <tfoot>
                    <tr>
                        <td colspan="9"></td>
                    </tr>
					<tr>
                        <td colspan="9"></td>
                    </tr>
					<tr>
						<td>交件人：</td>
                        <td colspan="8"></td>
                    </tr>
					<tr>
                        <td colspan="9"></td>
                    </tr>
					<tr>
                        <td colspan="9"></td>
                    </tr>
					<tr>
						<td>收件人：</td>
                        <td colspan="8"></td>
                    </tr>
                </tfoot>
                </table>';

            // if($i == 0)
            // {
                
            //     $spreadsheet = $reader->loadFromString($htmlString);
                
            // }
            // else
            // {                
                $spreadsheet = $reader->loadFromString($htmlString, $spreadsheet);
                
            // }
            $spreadsheet->getSheet($i)->setTitle($row['r_office']);
            $reader->setSheetIndex(($i+1));

			$i++;
         }
            
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        // $writer->save('write.xlsx'); 
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.(date('Y')-1911) . '年' .str_pad(date('m'),2,'0',STR_PAD_LEFT) .'月'. str_pad(date('d'),2,'0',STR_PAD_LEFT).'日入庫清冊.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file

    }
    /** 轉西元年 */
    public function tranfer2ADyear($date)
    {
        if(strlen($date) == 6)
        {
            $ad = ((int)substr($date, 0, 2)) + 1911;
            return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
        }
        elseif(strlen($date) == 7)
        {
            $ad = ((int)substr($date, 0, 3)) + 1911;
            return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
        }
        else
        {
            return '';
        }
    }

    /** 轉民國年 */
    public function tranfer2RCyear($date)
    {
        $date = str_replace('-', '', $date);
        $rc = ((int)substr($date, 0, 4)) - 1911;
        return (string)$rc . substr($date, 4, 2) . substr($date, 6, 2);
    }

    /** 取西元年度 */
    public function getyear($date)
    {
        if(strlen($date) == 7)
        {
            $ad = ((int)substr($date, 1, 2));
            return (string)$ad;
        }
        elseif(strlen($date) == 8)
        {
            $ad = ((int)substr($date, 1, 3));
            return (string)$ad;
        }
        else
        {
            return '0000';
        }
    }
	/** 轉民國年 中文年月日 */
    function tranfer2RCyearTrad($date)
    {
        $date = str_replace('-', '', $date);
        $rc = ((int)substr($date, 0, 4)) - 1911;
        return (string)$rc . '年' . substr($date, 4, 2) . '月' . substr($date, 6, 2) . '日';
    }
	/** 轉時分 */
	function tranfer2RChour($time)
	{
		if(null != $time)
		{
			$hour = explode(':', $time)[0];
			$min = explode(':', $time)[1];
			return $hour*1 . '時'.  $min*1 . '分';
		}
		else
		{
			return '';
		}
		
	}
	// 是否成年
	function isAudlt($birth, $catchdate){
		list($year,$month,$day) = explode("-",$birth);
		$nowyear = date("Y", strtotime($catchdate));
		$nowmonth = date("m", strtotime($catchdate));
		$nowday = date("d", strtotime($catchdate));
		$audlt = 1; // 成年

		if(($year+20) <= $nowyear)
		{
			if(($year+20) == $nowyear)
			{
				if($month*1 <= $nowmonth*1)
				{
					if($month*1 == $nowmonth*1)
					{
						$audlt = ($day > $nowday)?0:1;
					}
				}							
				else
					$audlt = 0;
			}
		}
		else
		{
			$audlt = 0;
		}

		return (($audlt==0)?false:true);
	}
	public function getzipcode33($addr){
		
		if(isset($_GET['addr']))
			$addr = $_GET['addr'];
		
		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
		);
		$homepage = file_get_contents('https://www.yijingtw.com/taoyuan/daxi/system/api/zipcodeSoap?postaddr='.$addr, false, stream_context_create($arrContextOptions));
		echo json_decode($homepage, true)['data'];
		exit;

		$curl = curl_init();
		$send_url = "https://www.yijingtw.com/taoyuan/daxi/system/api/zipcodeSoap?";
		$param_post = array(
			"postaddr" => $addr
        );
		curl_setopt_array($curl, array(
            CURLOPT_URL => $send_url. http_build_query($param_post) ,
            CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CONNECTTIMEOUT => 30,
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
		));
        $output = curl_exec($curl);
        // $error = curl_error($curl);
        // echo $error;

        if($output === false){
			throw new Exception('Http request message :'.curl_error($curl));
		}

        curl_close($curl);

		return json_decode($output, true)['data'];
	}
}
