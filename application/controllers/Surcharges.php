<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Surcharges extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }
    
    function table_surcharge() {//怠金點選
        $this->load->library('table');
		/** 跟裁罰處分對接好 要記得取消註解 */
        $query = $this->getsqlmod->getsurchangelist()->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1800px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','','罰鍰列管編號', '舊怠金編號','怠金依據文號', '受處分人姓名', '身分證號','應講習時間','應講習地點', '聯絡電話', '戶籍地址', '發文日期', '發文字號', '移送分局','依據單位', '依據日期', '依據字號', '講習時數');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $table_row = NULL;
            $table_row[] = $susp->surc_num;
			$table_row[] = '';

			// if(preg_match("/b/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('Surcharges/surc_prisoned/' . $susp->surc_no,'編輯在監資料');
            // else $table_row[] = '';
            $table_row[] = $susp->surc_listed_no;
			$table_row[] = '<span class="text-info">'.$susp->surc_lastnum.'</span>';
            $table_row[] = $susp->surc_basenum;
			$table_row[] = $susp->surc_name;
			$table_row[] = $susp->surc_sic;
            $table_row[] = $susp->surc_olec_date;
			$table_row[] = [];
			$table_row[] = [];

            // $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            // $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            // $table_row[] = $susp->s_date;
            // $table_row[] = $susp->r_county.$susp->r_district.$susp->r_address;
            // $table_row[] = $susp->s_office;
			$table_row[] = [];
            // $table_row[] = $susp->fd_lec_place;
            // $table_row[] = $susp->fd_date;
            // $table_row[] = $susp->fd_send_num;
            // $table_row[] = $susp->s_roffice;
            // $table_row[] = $susp->s_roffice1;
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];

            $table_row[] = $susp->surc_basenumdate;
            $table_row[] = $susp->surc_basenum;

            // $table_row[] = $susp->s_name;
            // $table_row[] = $susp->s_ic;
            // $table_row[] = $susp->p_no;
			
			

            $table_row[] = '6';
            // $table_row[] = $susp->fdd_date;//過後要改成送達時間 資料未填入
            
            
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_dp_rollback() {//怠金點選
        $this->load->library('table');
		/** 跟裁罰處分對接好 要記得取消註解 */
        $query = $this->getsqlmod->getsurchangelist_rb()->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1800px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table_rollback">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','','怠金列管編號','罰鍰列管編號', '舊怠金編號','怠金依據文號', '受處分人姓名', '身分證號','應講習時間','應講習地點', '聯絡電話', '戶籍地址', '發文日期', '發文字號', '移送分局','依據單位', '依據日期', '依據字號', '講習時數');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $table_row = NULL;
            $table_row[] = $susp->surc_num;
			$table_row[] = '';
			// if(preg_match("/b/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('Surcharges/surc_prisoned/' . $susp->surc_no,'編輯在監資料');
            // else $table_row[] = '';
			$table_row[] = '<span class="text-danger">'.$susp->surc_no.'</span>';
            $table_row[] = $susp->surc_listed_no;
			$table_row[] = '<span class="text-info">'.$susp->surc_lastnum.'</span>';
            $table_row[] = $susp->surc_basenum;
			$table_row[] = $susp->surc_name;
			$table_row[] = $susp->surc_sic;
            $table_row[] = $susp->surc_olec_date;
			$table_row[] = [];
			$table_row[] = [];

            // $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            // $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            // $table_row[] = $susp->s_date;
            // $table_row[] = $susp->r_county.$susp->r_district.$susp->r_address;
            // $table_row[] = $susp->s_office;
			$table_row[] = [];
            // $table_row[] = $susp->fd_lec_place;
            // $table_row[] = $susp->fd_date;
            // $table_row[] = $susp->fd_send_num;
            // $table_row[] = $susp->s_roffice;
            // $table_row[] = $susp->s_roffice1;
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];

            $table_row[] = $susp->surc_basenumdate;
            $table_row[] = $susp->surc_basenum;

            // $table_row[] = $susp->s_name;
            // $table_row[] = $susp->s_ic;
            // $table_row[] = $susp->p_no;
			
			

            $table_row[] = '6';
            // $table_row[] = $susp->fdd_date;//過後要改成送達時間 資料未填入
            
            
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_list($id) {//怠金專案處理
        $this->load->library('table');
        $query = $this->getsqlmod->getsurchangelist1($id)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1800px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        // $this->table->set_heading( '','怠金列管編號','裁處日期','裁處文號','虛擬帳戶','怠金金額','繳納期限','重講習日期','重講習地點',
        // '罰鍰列管編號','怠金依據文號','應講習時間','應講習地點', '發文日期', '發文字號', '移送分局', '依據單位', '依據日期', '依據字號', 
        // '受處分人姓名', '身分證號', '聯絡電話', '戶籍地址', '現住地址', '查獲時間', '查獲地點', '查獲單位',  '講習時數', 
        // '罰鍰送達時間', '講習當時在監', '現在在監情形', '在監文件');
		$this->table->set_heading( '','罰鍰列管編號', '舊怠金編號','怠金依據文號', '受處分人姓名', '身分證號','應講習時間','應講習地點', '聯絡電話', '戶籍地址', '發文日期', '發文字號', '移送分局','依據單位', '依據日期', '依據字號', '講習時數');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            // $table_row = NULL;
			$table_row = NULL;
            $table_row[] = $susp->surc_num;
            $table_row[] = $susp->surc_listed_no . ((isset($susp->surc_comment) && !empty($susp->surc_comment))?'<br/><small>原因：'.$susp->surc_comment.' <a href="#" data-toggle="modal" data-target="#documentModal" onclick="modalEvent('. $susp->surc_num .',\''.$susp->surc_comment.'\')"><span class="fa fa-pencil"></span></a></small>':'<br/><span class="label label-success" data-toggle="modal" data-target="#documentModal" onclick="modalEvent('. $susp->surc_num .')">新增備註</span>');
			$table_row[] = '<span class="text-info">'.$susp->surc_lastnum.'</span>';
            $table_row[] = $susp->surc_basenum;
			$table_row[] = $susp->surc_name;
			$table_row[] = $susp->surc_sic;
            $table_row[] = $this->tranfer2RCyearTrad($susp->surc_olec_date);
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];

            $table_row[] = $this->tranfer2RCyearTrad($susp->surc_basenumdate);
            $table_row[] = $susp->surc_basenum;
            $table_row[] = '6';
            // $table_row[] = $susp->surc_no;
            // $table_row[] = $susp->surc_no;
            // $table_row[] = $susp->surc_date;
            // $table_row[] = $susp->surc_send_bnum;
            // $table_row[] = $susp->surc_BVC;
            // $table_row[] = $susp->surc_amount;
            // $table_row[] = $susp->surc_findate;
            // $table_row[] = $susp->surc_lec_date;
            // $table_row[] = $susp->surc_lec_place;
            // $table_row[] = $susp->surc_listed_no;
            // $table_row[] = $susp->surc_no;
            // $table_row[] = $susp->surc_olec_date;
            // $table_row[] = $susp->fd_lec_place;
            // $table_row[] = $susp->fd_date;
            // $table_row[] = $susp->fd_send_num;
            // $table_row[] = $susp->s_roffice;
            // $table_row[] = $susp->s_roffice1;
            // $table_row[] = $susp->surc_basenum;
            // $table_row[] = $susp->surc_basenumdate;
            // $table_row[] = $susp->s_name;
            // $table_row[] = $susp->s_ic;
            // $table_row[] = $susp->p_no;
            // $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            // $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            // $table_row[] = $susp->s_date;
            // $table_row[] = $susp->r_county.$susp->r_district.$susp->r_address;
            // $table_row[] = $susp->s_office;
            // $table_row[] = '6';
            // $table_row[] = $susp->fdd_date;//過後要改成送達時間 資料未填入
            // $table_row[] = $susp->surc_inprison;//過後要改成送達時間 資料未填入
            // if($susp->surc_inprison == '否'){
            //     $table_row[] = '';
            // }
            // else{
            //     if($susp->surc_prison == null){
            //         $table_row[] = '未輸入';
            //     }
            //     else{
            //         $table_row[] = $susp->surc_prison;
            //     }
            // }
            // if($susp->surc_inprison == '否'){
            //     $table_row[] = '';
            // }
            // else{
            //     if($susp->surc_prison_doc == null){
            //         $table_row[] = '未上傳';
            //     }
            //     else{
            //         $table_row[] = $susp->surc_prison_doc;
            //     }
            // }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    function table_SurcProject() {//怠金專案(裁罰)
        $this->load->library('table');
        $query = $this->getsqlmod->getSurcharge_normal_Project()->result(); 
        $tmpl = array (
            'table_open' => '<table style="max-width: 1800px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號','專案時間','專案數量','操作');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $sp)
        {
            $Surc = $this->getsqlmod->getsurc($sp->sp_no); 
			$surc_hasedit = $this->getsqlmod->getsurc_hasedit($sp->sp_no);
            
            $table_row = NULL;
            $table_row[] = $sp->sp_num;
			$table_row[] = anchor('Surcharges/listdp1/' . $sp->sp_no, $sp->sp_no);
            //$table_row[] = $sp->sp_date;
            $table_row[] = $this->tranfer2RCyearTrad($sp->sp_time);
            $table_row[] = $Surc->num_rows();
            $table_row[] = '<span class="fa fa-trash text-danger" aria-hidden="true" style="cursor:pointer;" onclick="delEvent('.$sp->sp_num.')"></span><br/>' . (($surc_hasedit->num_rows() > 0)?'<small><strong>注：內有已編輯之案件</strong></small>':'');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_SurcPrisonProject() {//怠金專案(在監)
        $this->load->library('table');
        $query = $this->getsqlmod->getSurcharge_prison_Project()->result(); 
        $tmpl = array (
            'table_open' => '<table style="max-width: 1800px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table2">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '專案編號','專案時間','專案數量','操作');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $sp)
        {
            $Surc = $this->getsqlmod->getsurc($sp->sp_no); 
			$surc_hasedit = $this->getsqlmod->getsurc_hasedit($sp->sp_no);
            
            $table_row = NULL;
            // $table_row[] = $sp->sp_num;
            // $table_row[] = anchor('Surcharges/listProjectNormal/' . $sp->sp_no, $sp->sp_no);
			$table_row[] = anchor('Surcharges/listProjectNormal/' . $sp->sp_no, $sp->sp_no);
            //$table_row[] = $sp->sp_date;
            $table_row[] = $this->tranfer2RCyearTrad($sp->sp_time);
            $table_row[] = $Surc->num_rows();
			$table_row[] = '<span class="fa fa-trash text-danger" aria-hidden="true" style="cursor:pointer;" onclick="delEvent('.$sp->sp_num.')"></span><br/>' . (($surc_hasedit->num_rows() > 0)?'<small><strong>注：內有已編輯之案件</strong></small>':'');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_SurcReturnProject() {//怠金專案(不裁罰)
        $this->load->library('table');
        $query = $this->getsqlmod->getSurcharge_return_Project()->result(); 
        $tmpl = array (
            'table_open' => '<table style="max-width: 1800px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table3">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '專案編號','專案時間','專案數量','操作');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $sp)
        {
            $Surc = $this->getsqlmod->getsurc($sp->sp_no); 
			$surc_hasedit = $this->getsqlmod->getsurc_hasedit($sp->sp_no);
            
            $table_row = NULL;
            // $table_row[] = $sp->sp_num;
            // $table_row[] = anchor('Surcharges/listProjectNormal/' . $sp->sp_no, $sp->sp_no);
			$table_row[] = anchor('Surcharges/listProjectNormal/' . $sp->sp_no, $sp->sp_no);
            //$table_row[] = $sp->sp_date;
            $table_row[] = $this->tranfer2RCyearTrad($sp->sp_time);
            $table_row[] = $Surc->num_rows();
			$table_row[] = '<span class="fa fa-trash text-danger" aria-hidden="true" style="cursor:pointer;" onclick="delEvent('.$sp->sp_num.')"></span><br/>' . (($surc_hasedit->num_rows() > 0)?'<small><strong>注：內有已編輯之案件</strong></small>':'');
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_SurcProject_rollback() {//怠金專案(重處)
        $this->load->library('table');
        $query = $this->getsqlmod->getSurcharge_rb_Project()->result(); 
        $tmpl = array (
            'table_open' => '<table style="max-width: 1800px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號','專案時間','專案數量','操作');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $sp)
        {
            $Surc = $this->getsqlmod->getsurc($sp->sp_no); 
			// $surc_hasedit = $this->getsqlmod->getsurc_hasedit($sp->sp_no);
            
            $table_row = NULL;
            $table_row[] = $sp->sp_num;
			$table_row[] = anchor('Surcharges/listdp1/' . $sp->sp_no, $sp->sp_no);
            //$table_row[] = $sp->sp_date;
            $table_row[] = $this->tranfer2RCyearTrad($sp->sp_time);
            $table_row[] = $Surc->num_rows();
            $table_row[] = '<span class="fa fa-trash text-danger" aria-hidden="true" style="cursor:pointer;" onclick="delEvent('.$sp->sp_num.')"></span><br/>';
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    public function index() {
        $this->load->helper('form');
        $uoffice = $this->session-> userdata('uoffice');
        $sp = $this->getsqlmod->getSurcharge_Project()->result();
		$sp_rollback = $this->getsqlmod->getdpproject_rollback()->result();
        $type_options = array();
		$rb_options = array();
        foreach ($sp as $sp1){
            $type_options[$sp1->sp_no] = $sp1->sp_no.$sp1->sp_type;
        }
		foreach ($sp_rollback as $rb_opt){
            $rb_options[$rb_opt->sp_no] = $rb_opt->sp_no;
        }
        $data['s_table'] = $this->table_surcharge();
		$data['roll_table'] = $this->table_dp_rollback();
        $data['title'] = "待處理怠金案件";
        $data['opt'] = $type_options;
		$data['rb_opt'] = $rb_options;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'surcharges/list';
        else $data['include'] = 'Surcharges_query/list';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function surc_prisoned() {
        $this->load->helper('form');
        $uoffice = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $prison = $this->getsqlmod->getprison()->result();
        $type_options = array();
        foreach ($prison as $p){
            $type_options[$p->prison_sname] = $p->prison_sname;
        }
        $data['title'] = "編輯在監資料:".$id;
        $data['surc_no'] = $id;
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surcharges/prison_ed';
        else $data['include'] = 'Surcharges_query/prison_ed';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function savePrison() {//收文更新
        $this->load->helper('form');
        $uoffice = $this->session-> userdata('uoffice');
        $uic = $this -> session -> userdata('uic');
        $uname = $this -> session -> userdata('uname');
        //var_dump($_FILES);
        if(!empty($_FILES['surc_prison_doc']['name'])){
            $_FILES['file']['name'] = $_POST['surc_no'].'_prison.'.'pdf';
            $_FILES['file']['type'] = $_FILES['surc_prison_doc']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['surc_prison_doc']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['surc_prison_doc']['error'];
            $_FILES['file']['size'] = $_FILES['surc_prison_doc']['size'];
            $config['upload_path'] = 'surcharge_doc/'; 
            $config['allowed_types'] = 'pdf|doc|docx|csv|xlsx';
            $config['max_size'] = '50000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $surc_prison_doc = $uploadData['file_name'];
            }
        }
        $surcharge = array(
            'surc_inprison' => $_POST['surc_inprison'],
            'surc_prison' => $_POST['surc_prison'],
            'surc_prison_doc' => $surc_prison_doc,
        );
        $this->getsqlmod->updateSurc($_POST['surc_no'],$surcharge);
        redirect('Surcharges/index/'); 
    }
    public function listProjectNormal() {//一般送達專案列表詳細
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        $uoffice = $this->session-> userdata('uoffice');

		/**
		 * 要改回來
		 */
        $sp = $this->getsqlmod->getsurchangelistSP($id)->result();
        //$sp1 = $this->getsqlmod->getsurchangelist1($id)->result();
        $data['sp'] = $id;
        $data['sp1'] = $sp;
        $data['s_table'] = $this->table_list($id);
        $data['title'] = "怠金專案處理：".$id .'('.$sp[0]->sp_type.')';
        $data['user'] = $this -> session -> userdata('uic');
        $data['nav'] = 'navbar3';
        // if($sp[0]->sp_type=='郵務送達'){
        //     if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surcharges/listProjectPost';
        //     else $data['include'] = 'Surcharges_query/listProjectPost';
        // }
        // else if($sp[0]->sp_type=='囑託監所送達'){
        //     if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surcharges/listProjectPrison';
        //     else $data['include'] = 'Surcharges_query/listProjectPrison';
        // }
        // else if($sp[0]->sp_type=='公示送達'){
        //     if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surcharges/listProjectPublic';
        //     else $data['include'] = 'Surcharges_query/listProjectPublic';
        // }
        // else if($sp[0]->sp_type=='不裁罰'){
            if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'surcharges/listProjectNo';
            else $data['include'] = 'surcharges_query/listProjectNo';
        // }
		$data['url_1'] = "案件作業";
		$data['url'] = 'surcharges/listProject';
        $this->load->view('template', $data);
    }
    
    // public function updatestatus_ready() {
    //     $this->load->helper('form');
    //     $s_cnum =mb_split(",",$_POST['s_cnum']);
    //     var_dump($s_cnum);
    //     if($_POST['s_status']=='1'){
    //         foreach ($s_cnum as $key => $value) {
    //             $this->getsqlmod->updateSurcharge_Project_sp($value,'已寄出');
    //         }
    //         redirect('Surcharges/listProject/'); 
    //     }
    // }    
    
    public function listProject() {//專案列表
        $this->load->helper('form');
        $uoffice = $this->session-> userdata('uoffice');
        $data['s_table'] = $this->table_SurcProject();
		$data['s_prison_table'] = $this->table_SurcPrisonProject();
		$data['s_return_table'] = $this->table_SurcReturnProject();
        $data['title'] = "怠金專案列表";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'surcharges/listProject';
        else $data['include'] = 'Surcharges_query/listProject';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function updatesurchargesproject() {//加入專案
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $uname = $this -> session -> userdata('uname');
		$taiwan_date = date('Y')-1911; 			

        $now = date('mdHis'); 
		if(isset($_POST['front_word']) && $_POST['front_word'] == 'P')
			$rp_num = 'P'.((isset($_POST['sp_type']) && $_POST['sp_type'] == '裁罰')?$_POST['surc_send_num']:$_POST['surc_olec_date']); 
		else
        	$rp_num = ((isset($_POST['sp_type']) && $_POST['sp_type'] == '裁罰')?$_POST['surc_send_num']:$_POST['surc_olec_date']);

        $s_cnum =mb_split(",",$_POST['s_cnum']);
        // var_dump($s_cnum);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    'surc_projectnum' => $rp_num,
                );
                $this->getsqlmod->updateSurc($value,$data);
            }
            $data1 = array(
                    'sp_no' => $rp_num,
                    // 'sp_date' => $_POST['sp_date'],
                    'sp_type' => ((isset($_POST['sp_type']))?$_POST['sp_type']:null),
                    // 'sp_reason1' => $_POST['sp_reason1'],
                    // 'sp_reason2' => $_POST['sp_reason2'],
					'sp_send_no' => ((isset($_POST['sp_type']) && $_POST['sp_type'] == '裁罰')?$_POST['surc_send_num']:NULL),
                    'sp_empno' => $uname,
                    'sp_time' => date('Ymd'),
                    //'dp_status' => '送批',
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addSurc($data1);
            // redirect('Surcharges/index/'); 
			if(isset($_POST['front_word']) && $_POST['front_word'] == 'P')
				echo base_url('Surcharges/listDpRollbackProject');
			else
				echo base_url('Surcharges/listProject');
			
        }
		
        if($_POST['s_status']=='0'){//加入舊案
            foreach ($s_cnum as $key => $value) {
                $data = array(
                    'surc_projectnum' => $_POST['sp_num'],
                );
                $this->getsqlmod->updateSurc($value,$data);
            }
            // redirect('Surcharges/index/'); 
			echo base_url('Surcharges/listProject');
        }
    }
    
    public function surcharges_receive() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $data['title'] = "怠金收文處理";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'surcharges/surcharges_receive';
        else $data['include'] = 'Surcharges_query/surcharges_receive';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    public function test_doc(){
		$response = array(
			'status' => 'OK',
			'message' => 'The Latest Lazy Certno follow.',
			'CertNo' => '123'
		);
		$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
				->_display();
			exit;
		// echo json_decode($result);
		// var_dump($_FILES);
	}
    public function receiveupdate() {//收文更新
		set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);   
        $this->load->helper('form');
        $uoffice = $this->session-> userdata('uoffice');
        $uic = $this -> session -> userdata('uic');
        $uname = $this -> session -> userdata('uname');
		if (!empty($_FILES)) {
			$error = $_FILES['other_doc']['error'];
			switch($error) {
				case UPLOAD_ERR_OK:
				$response = 'OK';
				break;
				case UPLOAD_ERR_INI_SIZE:
				$response = 'Error: file size exceeds the allowed.';
				break;
				case UPLOAD_ERR_PARTIAL:
				$response = 'Error: file was only partially uploaded.';
				break;
				case UPLOAD_ERR_NO_FILE:
				$response = 'Error: no file have been uploaded.';
				break;
				case UPLOAD_ERR_NO_TMP_DIR:
				$response = 'Error: missing temp directory.';
				break;
				case UPLOAD_ERR_CANT_WRITE:
				$response = 'Error: Failed to write file';
				break;
				case UPLOAD_ERR_EXTENSION:
				$response = 'Error: A PHP extension stopped the file upload.';
				break;
				default:
				$response = 'An unexpected error occurred; the file could not be uploaded.';
				break;
			}
			if($response != 'OK')
			{
				$response = array(
					'msg' => $response,
					'directurl' => '#'
				);
				echo json_encode($response);
				exit;
			}
			
		} else {
			$response = 'Error: Form is not uploaded.';
			$response = array(
				'msg' => $response,
				'directurl' => '#'
			);
			echo json_encode($response);
			exit;
		}
		
        // var_dump($_POST);
		// var_dump($_FILES);
		// exit;
        if(!empty($_FILES['other_doc']['name'])){
            $_FILES['file']['name'] = $_POST['surc_basenum'].'.'.'pdf';
            $_FILES['file']['type'] = $_FILES['other_doc']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['other_doc']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['other_doc']['error'];
            $_FILES['file']['size'] = $_FILES['other_doc']['size'];
            $config['upload_path'] = 'surcharge_doc/'; 
            $config['allowed_types'] = 'pdf|doc|docx|csv|xlsx';
            $config['max_size'] = '102400'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $other_doc = $uploadData['file_name'];
            }
        }
		// var_dump($_FILES['excel']);
        if(!empty($_FILES['excel']['name'])){
            $_FILES['file']['name'] = $_POST['surc_basenum'].'.'.'xlsx';
            $_FILES['file']['type'] = $_FILES['excel']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['excel']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['excel']['error'];
            $_FILES['file']['size'] = $_FILES['excel']['size'];
            $config['upload_path'] = 'surcharge_doc/'; 
            $config['allowed_types'] = 'csv|xlsx';
            $config['max_size'] = '50000'; // max_size in kb
            $this->load->library('upload',$config); 
            if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $excel = $uploadData['file_name'];
                // echo $excel;
            }
            else{
                $error = array('error' => $this->upload->display_errors());
                echo $_FILES['file']['name'];
                // var_dump($error);
            }
        }
		$ext = pathinfo($_FILES['excel']['name'], PATHINFO_EXTENSION);
		// 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }
		try {
			$reader->setReadDataOnly(true);
			$reader->setLoadAllSheets();
			$spreadsheet = $reader->load($_FILES['excel']['tmp_name']);
			
			$sheetcount = $spreadsheet->getSheetCount();
			$sheetnames = $spreadsheet->getSheetNames();
			
			
			// asort($caseid);
		} catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
			die($e->getMessage());
		}
		// print_r($sheetcount);
		// print_r($sheetnames);
		// echo '<br/>';
		foreach ($sheetnames as $name) {
			
			$worksheet = $spreadsheet->getSheetByName($name);
			$highestRow = $worksheet->getHighestRow();
			$highestColumn = $worksheet->getHighestColumn();
			$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 
			$rowspsh = $worksheet->toArray();
			$readrows = array();
			
			for ($j=2; $j <= $highestRow; $j++) { 
				if (!empty($worksheet->getCellByColumnAndRow(1, $j)->getValue()))
				{
					$readcols = array();
					for ($i=1; $i <= $highestColumnIndex; $i++) { 
						if (!empty($worksheet->getCellByColumnAndRow($i, $j)->getValue()))
						{
							if(strpos($worksheet->getCellByColumnAndRow($i, 1)->getValue(), '講習') !== false)
							{
								$v = $worksheet->getCellByColumnAndRow($i, $j)->getValue();
								$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($v);
								$readcols[] = date('Y-m-d', $date);
							}
							else
							{
								$readcols[] = $worksheet->getCellByColumnAndRow($i, $j)->getFormattedValue();
							}
						}
						
					}
					$readrows[] = $readcols;
				}
				
			}
			// print_r($readrows);
			// echo '<br/>';
			// print_r($rowspsh);
			// print_r($highestColumn);
			
			if(strpos($name, '新案') !== false)
			{
				foreach ($readrows as $data) {
					$query = $this->getsqlmod->getsurcAll()->result();
					// $countid = $query[0]->surc_no;
					// $lastNum = (((int)mb_substr($countid, 1, 3) == (date('Y')-1911))?(int)mb_substr($countid, -4, 4):0);
					// $lastNum++;
					// $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
					// $taiwan_date = date('Y')-1911; 
					// $surc_no= 'A' . $taiwan_date.$value;
					$susp = $this->getsqlmod->getsuspect('s_ic',$data[4])->result();
					if(isset($susp[0]->s_cnum)){
						$surcharge = array(
							'surc_no' => null,
							'surc_listed_no' => $data[1],
							'surc_lastnum' => null,
							'surc_psend_num' => $data[2],
							'surc_sic' => $data[4],
							'surc_olec_date' => $data[5],
							'surc_cnum' => $susp[0]->s_cnum,
							'surc_snum' => $susp[0]->s_num,
							'surc_name' => $data[3],
							'surc_target' => $data[3],
							'surc_basenum' => $_POST['surc_basenum'],
							'surc_basenumdate' => $this->tranfer2ADyear($_POST['surc_basenumdate']),
							'surc_empno' => $uname,
							'surc_other_doc' => $other_doc,
							'surc_excel' => $excel,
						);
					}else if(!isset($susp[0]->s_cnum)){
						$surcharge = array(
							'surc_no' => null,
							'surc_listed_no' => $data[1],
							'surc_lastnum' => null,
							'surc_psend_num' => $data[2],
							'surc_sic' => $data[4],
							'surc_olec_date' => $data[5],
							'surc_name' => $data[3],
							'surc_target' => $data[3],
							'surc_basenum' => $_POST['surc_basenum'],
							'surc_basenumdate' => $this->tranfer2ADyear($_POST['surc_basenumdate']),
							'surc_empno' => $uname,
							'surc_other_doc' => $other_doc,
							'surc_excel' => $excel,
						);
					}
					// filter=> 原罰鍰處分編號, 應講習日, 身分證字號
					$check = $this->getsqlmod->getSurc_by_checkimprot($data[1],$data[5],$data[4])->result();
					if(!isset($check[0])) $this->getsqlmod->addsurc_receive($surcharge);
					else{ $this->getsqlmod->updateSurc($check[0]->surc_num,$surcharge);}
				}
			}
			else
			{
				foreach ($readrows as $data) {
					$query = $this->getsqlmod->getsurcAll()->result();
					// $countid = $query[0]->surc_no;
					// $lastNum = (((int)mb_substr($countid, 1, 3) == (date('Y')-1911))?(int)mb_substr($countid, -4, 4):0);
					// $lastNum++;
					// $value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
					// $taiwan_date = date('Y')-1911; 
					// $surc_no= 'A' . $taiwan_date.$value;
					$susp = $this->getsqlmod->getsuspect('s_ic',$data[5])->result();
					if(!isset($susp[0]->s_cnum)){
						$surcharge = array(
							'surc_no' => null,
							'surc_listed_no' => $data[2],
							'surc_lastnum' => $data[1],
							'surc_psend_num' => $data[3],
							'surc_sic' => $data[5],
							'surc_olec_date' => $data[6],
							'surc_name' => $data[4],
							'surc_target' => $data[4],
							'surc_basenum' => $_POST['surc_basenum'],
							'surc_basenumdate' => $this->tranfer2ADyear($_POST['surc_basenumdate']),
							'surc_empno' => $uname,
							'surc_other_doc' => $other_doc,
							'surc_excel' => $excel,
						);
					}
					elseif (isset($susp[0]->s_cnum)) {
						$surcharge = array(
							'surc_no' => null,
							'surc_listed_no' => $data[2],
							'surc_lastnum' => $data[1],
							'surc_psend_num' => $data[3],
							'surc_sic' => $data[5],
							'surc_olec_date' => $data[6],
							'surc_cnum' => $susp[0]->s_cnum,
							'surc_snum' => $susp[0]->s_num,
							'surc_name' => $data[4],
							'surc_target' => $data[4],
							'surc_basenum' => $_POST['surc_basenum'],
							'surc_basenumdate' => $this->tranfer2ADyear($_POST['surc_basenumdate']),
							'surc_empno' => $uname,
							'surc_other_doc' => $other_doc,
							'surc_excel' => $excel,
						);
					}
					$check = $this->getsqlmod->getSurc_by_checkimprot($data[2],$data[6],$data[5])->result();
					if(!isset($check[0])) $this->getsqlmod->addsurc_receive($surcharge);
					else{ $this->getsqlmod->updateSurc($check[0]->surc_num,$surcharge);}
				}
			}
		}
		// redirect('Surcharges/index/'); 
		$response = array(
            'msg' => 'OK',
            'directurl' => 'Surcharges/index'
        );
		echo json_encode($response);
		exit;
        // try {
			
        //     $spreadsheet = $reader->load($_FILES['excel']['tmp_name']);
        // } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
        //     die($e->getMessage());
        // }

        // $sheet = $spreadsheet->getActiveSheet();

        // $res = array();
        
        // foreach ($sheet->getRowIterator(2) as $row) {
        //     $tmp = array();
        //     foreach ($row->getCellIterator() as $cell) {
        //         //$tmp[] = $cell->getFormattedValue();
        //         $tmp[] = $cell->getValue();
        //     }
        //     $res[] = $tmp;
        // }
            
        
        foreach($res as $value){//讀取後加入資料庫
            $surc_no++;
            //$date = strtotime($value[6]);
            $date = ((INT)$value[6] - 25569) * 86400;
            $date = gmdate("Y-m-d", $date);
            $susp = $this->getsqlmod->getsuspect('s_ic',$value[5])->result();
            if(isset($susp[0]->s_cnum) && $value[2]!=''){
                $surcharge = array(
                    'surc_no' => $surc_no,
                    'surc_listed_no' => $value[2],
                    'surc_lastnum' => $value[1],
                    'surc_psend_num' => $value[3],
                    'surc_sic' => $value[5],
                    'surc_olec_date' => $date,
                    'surc_cnum' => $susp[0]->s_cnum,
                    'surc_snum' => $susp[0]->s_num,
                    'surc_target' => $value[4],
                    'surc_basenum' => $_POST['surc_basenum'],
                    'surc_basenumdate' => $_POST['surc_basenumdate'],
                    'surc_empno' => $uname,
                    'surc_other_doc' => $other_doc,
                    'surc_excel' => $excel,
                );
            }else if(isset($susp[0]->s_cnum) && $value[2]==''){
                $surcharge = array(
                    'surc_no' => $surc_no,
                    'surc_listed_no' => $value[1],
                    'surc_lastnum' => $value[1],
                    'surc_psend_num' => $value[3],
                    'surc_sic' => $value[5],
                    'surc_olec_date' => $date,
                    'surc_cnum' => $susp[0]->s_cnum,
                    'surc_snum' => $susp[0]->s_num,
                    'surc_target' => $value[4],
                    'surc_basenum' => $_POST['surc_basenum'],
                    'surc_basenumdate' => $_POST['surc_basenumdate'],
                    'surc_empno' => $uname,
                    'surc_other_doc' => $other_doc,
                    'surc_excel' => $excel,
                );
            }else if(!isset($susp[0]->s_cnum) && $value[2]!=''){
                $surcharge = array(
                    'surc_no' => $surc_no,
                    'surc_listed_no' => $value[2],
                    'surc_lastnum' => $value[1],
                    'surc_psend_num' => $value[3],
                    'surc_sic' => $value[5],
                    'surc_olec_date' => $date,
                    'surc_target' => $value[4],
                    'surc_basenum' => $_POST['surc_basenum'],
                    'surc_basenumdate' => $_POST['surc_basenumdate'],
                    'surc_empno' => $uname,
                    'surc_other_doc' => $other_doc,
                    'surc_excel' => $excel,
                );
            }else if(!isset($susp[0]->s_cnum) && $value[2]==''){
                $surcharge = array(
                    'surc_no' => $surc_no,
                    'surc_listed_no' => $value[1],
                    'surc_lastnum' => $value[1],
                    'surc_psend_num' => $value[3],
                    'surc_sic' => $value[5],
                    'surc_olec_date' => $date,
                    'surc_target' => $value[4],
                    'surc_basenum' => $_POST['surc_basenum'],
                    'surc_basenumdate' => $_POST['surc_basenumdate'],
                    'surc_empno' => $uname,
                    'surc_other_doc' => $other_doc,
                    'surc_excel' => $excel,
                );
            }else{
                $surcharge = array(
                    'surc_no' =>  $surc_no,
                    'surc_listed_no' => $value[1],
                    'surc_lastnum' => $value[1],
                    'surc_psend_num' => $value[3],
                    'surc_sic' => $value[5],
                    'surc_olec_date' => $date,
                    'surc_target' => $value[4],
                    'surc_basenum' => $_POST['surc_basenum'],
                    'surc_basenumdate' => $_POST['surc_basenumdate'],
                    'surc_empno' => $uname,
                    'surc_other_doc' => $other_doc,
                    'surc_excel' => $excel,
                );
            }
            $check = $this->getsqlmod->getsurc1($value[1])->result();
             if(!isset($check[0]->surc_no)) $this->getsqlmod->addsurc_receive($surcharge);
             else{ $this->getsqlmod->updateSurc($value[1],$surcharge);}
        }
        redirect('Surcharges/index/'); 
    }
    
    function editSurc(){//批次裁處
        $this->load->library('table');
        $id1 = $this->uri->segment(3);
        $surc=$this->getsqlmod->getsurchangelist2($id1)->result();
        $surcProject=$this->getsqlmod->getSurcharge_Project1($surc[0]->surc_projectnum)->result();
        $id = $surc[0]->surc_snum;
        $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
        $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
        $DpOrder=$this->getsqlmod->get1SuercOrder('surc_projectnum',$surc[0]->surc_projectnum)->result();
        $sp=$this->getsqlmod->get3SP1($id)->result();
        $spcount=$this->getsqlmod->getfdAll()->result();
        $prison=$this->getsqlmod->getprison()->result();
        //var_dump($DpOrder);
        foreach ($DpOrder as $order){
            $arr[] = $order->surc_no;
        }
        $id; 
        $i=0;
        //echo count($arr);
        //echo $id1;
        //print_r($arr);
        while ($arr[$i] !== $id1) {
            $i++;
            next($arr); 
        }
        //echo $arr[$i];
        if($i>0){
            $data['prev'] = $arr[$i-1];
        }
        if($i==0){
            $data['prev'] = NULL;
        }
        if($i == count($arr)-1){
            $data['next'] = NULL;
        }
        if($i < count($arr)-1){
            $data['next'] = $arr[$i+1];
        }
        $data['title'] = "裁處修改:". $id1;
        if(isset($sp[0]))$data['sp'] = $sp[0];
        $data['surcProject'] = $surcProject[0];
        $data['surc'] = $surc[0];
        $data['susp'] = $susp[0];
        if(isset($fine[0]))$data['fine'] = $fine[0];
        $data['empno'] = $this->session->userdata('uname');
        $data['nav'] = "navbar3";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surcharges/Surcedit';
        else $data['include'] = 'Surcharges_query/Surcedit';
        $this->load->view('template', $data);
    } 

    function updateSucrEd(){
        //var_dump($_POST);
        $uname = $this -> session -> userdata('uname');
        $fr1 = $this->getsqlmod->getFR('f_surcnum',$_POST['surc_no'])->result();
        //echo $_POST['surc_projectnum'];
        $sp = $this->getsqlmod->getSurcharge_Project1($_POST['surc_projectnum'])->result();
        $delivery1 = $this->getsqlmod->getdelivery('fdd_snum',$_POST['s_num'])->result();
        //$BVC = '21196'.$_POST['sp_go_no'].$d;
        $code =  '21719'.$_POST['surc_send_num'];
        $code1 = preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY);
        $code2=0;
        for($i=0;$i<count($code1);$i++){
            if($i==0||$i==3||$i==6||$i==9||$i==12||$i==15) $code2=$code2+$code1[$i]*3;
            if($i==1||$i==4||$i==7||$i==10||$i==13) $code2=$code2+($code1[$i]*7);
            if($i==2||$i==5||$i==8||$i==11||$i==14) $code2=$code2+($code1[$i]*1);
        }
        $code3 = $code2%10;
        $code4 = 10-$code3;
        $fin = $code.$code4;
        $surc = array(
            'surc_send_num'  => $_POST['surc_send_num'],
            'surc_send_bnum'  => $_POST['surc_send_bnum'],
            'surc_lec_place'  => $_POST['surc_lec_place'],
            'surc_lec_date'  => $_POST['surc_lec_date'],
            'surc_amount'  => $_POST['surc_amount'],
            'surc_findate'  => $_POST['surc_findate'],
            'surc_BVC'  => $fin,
        );
        $fine_rec = array(
            'f_samount' => $_POST['surc_amount'],
            'f_snum' => $_POST['s_num'],
            'f_sic' => $_POST['s_ic'],
            'f_cnum' => $_POST['s_cnum'],
            'f_surcnum' => $_POST['surc_no'],
            //'f_date' => $_POST['f_date'],
            'f_empno' => $uname,
            //'f_BVC' => $fin,
        );
        ////var_dump($_POST);
        $this->getsqlmod->updateSurc($_POST['surc_no'],$surc);
        if(isset($fr1[0]))$this->getsqlmod->updateFRSurc($_POST['surc_no'],$fine_rec);
        else {
            $this->getsqlmod->addFR($fine_rec);
        }
        if($_POST['next']!=null) redirect('Surcharges/editSurc/' . $_POST['next'],'refresh'); 
        if($_POST['prev']!=null) redirect('Surcharges/editSurc/' . $_POST['prev'],'refresh'); 
        if($sp[0]->sp_type=='郵務送達') redirect('Surcharges/listProjectNormal/' . $_POST['surc_projectnum'],'refresh'); 
        if($sp[0]->sp_type=='囑託監所送達') redirect('Surcharges/listProjectNormal/' . $_POST['surc_projectnum'],'refresh'); 
        if($sp[0]->sp_type=='公示送達') redirect('Surcharges/listProjectNormal/' . $_POST['surc_projectnum'],'refresh'); 
        if($sp[0]->sp_type=='不裁罰') redirect('Surcharges/listProjectNormal/' . $_POST['surc_projectnum'],'refresh'); 
    }
    
    function updateSucrdate(){
        $surc = array(
            'surc_date' => $_POST['surc_date'],
        );
        $this->getsqlmod->updateSurcPro($_POST['surc_projectnum'],$surc);
        redirect('Surcharges/editSurc/' . $_POST['surc_no'],'refresh');
    }

    function table_delivery() {//送達登入列表
        $this->load->library('table');
        $query = $this->getsqlmod->getsurchangelisttable()->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        if(preg_match("/b/i", $this->session-> userdata('3permit'))) $this->table->set_heading( '','怠金列管編號','發文對象','發文時間','繳交期限', '送達情形', '送達情形修改','送達日期','送達證書');
        else $this->table->set_heading( '','怠金列管編號','發文對象','發文時間','繳交期限', '送達情形', '送達證書');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $i++;
            $query1 = $this->getsqlmod->get2Susp($susp->s_num); // 抓snum
            $drug = $this->getsqlmod->get3Drug($susp->s_ic); // 抓sic
            $drug_doc = $this->getsqlmod->get1Drug($susp->s_cnum)->result(); // 抓scnum
            $table_row = NULL;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->surc_target;
            $table_row[] = $susp->surc_date;
            $table_row[] = $susp->surc_findate;
            if($susp->surc_delivery_date == null){
                $table_row[] = '<strong>未登入</strong>';
            }
            else{
                $table_row[] = $susp->surc_delivery_status.'；'.$susp->surc_delivery_date;
            }
            if(preg_match("/b/i", $this->session-> userdata('3permit'))){
                if($susp->surc_delivery_status == '已送達'){
                    $table_row[] = '<select id="surc_delivery_status" name="surc_delivery_status['.$susp->surc_no.']" type="text" >
                            <option selected value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
                            <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
                            <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
                            </select>';
                }
                else if($susp->surc_delivery_status == '現住地未送達'){
                    $table_row[] = '<select id="surc_delivery_status" name="surc_delivery_status['.$susp->surc_no.']" type="text" >
                            <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
                            <option selected value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
                            <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
                            </select>';
                }
                else if($susp->surc_delivery_status == '戶籍地未送達'){
                    $table_row[] = '<select id="surc_delivery_status" name="surc_delivery_status['.$susp->surc_no.']" type="text" >
                            <option value="已送達">已送達</option><option selected value="戶籍地未送達">戶籍地未送達</option>
                            <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
                            <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
                            </select>';
                }
                else if($susp->surc_delivery_status == '已超過1月半無送達'){
                    $table_row[] = '<select id="surc_delivery_status" name="surc_delivery_status['.$susp->surc_no.']" type="text" >
                            <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
                            <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
                            <option selected value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
                            </select>';
                }
                else if($susp->surc_delivery_status == '已出監'){
                    $table_row[] = '<select id="surc_delivery_status" name="surc_delivery_status['.$susp->surc_no.']" type="text" >
                            <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
                            <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
                            <option value="已超過1月半無送達">已超過1月半無送達</option><option selected value="公示送達">公示送達</option>
                            </select>';
                }
                else if($susp->surc_delivery_status == '公示送達'){
                    $table_row[] = '<select id="surc_delivery_status" name="surc_delivery_status['.$susp->surc_no.']" type="text" >
                            <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
                            <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
                            <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
                            </select>';
                }
                else{
                    $table_row[] = '<select id="surc_delivery_status" name="surc_delivery_status['.$susp->surc_no.']" type="text" >
                            <option value="已送達">已送達</option><option value="戶籍地未送達">戶籍地未送達</option>
                            <option value="現住地未送達">現住地未送達</option><option value="已出監">已出監</option>
                            <option value="已超過1月半無送達">已超過1月半無送達</option><option value="公示送達">公示送達</option>
                            </select>';
                }
            }
            if(preg_match("/b/i", $this->session-> userdata('3permit'))){
                if($susp->surc_delivery_date == null){
                    $table_row[] = '<input id="surc_delivery_date" name="surc_delivery_date['.$susp->surc_no.']" value="'.$today.'"type="date"/> ';
                }
                else{
                    $table_row[] = '<input id="surc_delivery_date" name="surc_delivery_date['.$susp->surc_no.']" value="'.$susp->surc_delivery_date.'"type="date"/> ';
                }
            }
            if($susp->surc_delivery_doc == null){
                $table_row[] = '<strong>無送達文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->surc_delivery_doc, '送達證書');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    public function listdelivery_Surc() {//怠金送達登入
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table_delivery();
        $data['s_table'] = $test_table;
        $data['title'] = "送達登入";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surcharges/listdelivery';
        else $data['include'] = 'Surcharges_query/listdelivery';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }   
    
    public function updatedelivery(){
        ////var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        foreach ($s_cnum1 as $key => $value) {
            //echo $value."<br>";
            if(isset($_POST['surc_delivery_date'][$value])){
                $data = array(
                    'surc_delivery_date' => $_POST['surc_delivery_date'][$value],
                    'surc_delivery_status' => $_POST['surc_delivery_status'][$value],
                    //'surc_projectnum' => NULL,
                );
                $this->getsqlmod->updateSurc($value,$data);
            }
        };
        redirect('Surcharges/listdelivery_Surc/'); 
    }
    public function uploaddeliverydoc(){
        ////var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //$s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        ////var_dump($_FILES);
        $countfiles = count($_FILES['files']['name']);
        for($i=0;$i<$countfiles;$i++){
            if(!empty($_FILES['files']['name'][$i])){
                $fd_num = substr($_FILES['files']['name'][$i],0,8);
				
                //$fd=$this->getsqlmod->getFD('fd_num',$fd_num)->result();//取得送達文件所需資料
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                $config['upload_path'] = '送達文件/'; 
                $config['allowed_types'] = 'pdf|jpg|jpeg';
                $config['max_size'] = '50000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    $delivery = array(
                        'sdd_doc'  => $filename,
                    );
                	$this->getsqlmod->updateSdd_doc($fd_num ,$delivery);
                }
				else{
					echo $this->upload->display_errors();
				}
            }        
        }
        redirect('Surcharges/listdelivery/'); 
    }
    
    public function listPetitions() {
        $this->load->helper('form');
        $id = $this->uri->segment(3);
        //$rp = $this->getsqlmod->getRP1($id)->result();
        $this->load->library('table');
        //var_dump($_POST);
        $name="";
        $ic="";
        $surc_no="";
        $first = date("Y-m-d");
        $last = date("Y-m-d");
        if(!empty($_POST['name']))
        {
            $name = $_POST['name'];
            $first = '2020-01-01';
            $last = date("Y-m-d");
        }
        if(!empty($_POST["ic"]))
        {
            $ic = $_POST["ic"];
            $first = '2020-01-01';
            $last = date("Y-m-d");
        } 
        if(!empty($_POST["fd_num"]))
        {
            $surc_no = $_POST["fd_num"];
            $first = '2020-01-01';
            $last = date("Y-m-d");
        } 
        //echo $surc_no;
        $query = $this->getsqlmod->getSurcPetition_search($name,$ic,$surc_no,$first,$last)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width:2400px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','怠金列管編號', '犯嫌人姓名', '身份證','答辯日期','聲明異議文件1','聲明異議文件2','聲明異議文件3','公文書1','公文書2','公文書3');
        $table_row = array();
        $i=0;
        foreach ($query as $susp){
            $table_row =NUll;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->s_ic;
            if(!isset($susp->petition_date)||$susp->petition_date=='0000-00-00'){
                $table_row[] = '<input name="petition_date['.$susp->surc_no.']" type="date" class="form-control" value='.date('Y-m-s').'>';
            }
            else{
                $table_row[] = $susp->petition_date;
            }
            if(!isset($susp->petition_doc1)||$susp->petition_doc1 == null){
                $table_row[] = form_upload('petition_doc1['.$susp->surc_no.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc1, '聲明異議文件1');
            }
            if(!isset($susp->petition_doc2)||$susp->petition_doc2==null){
                $table_row[] = form_upload('petition_doc2['.$susp->surc_no.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc2, '聲明異議文件2');
            }
            if(!isset($susp->petition_doc3)||$susp->petition_doc3==null){
                $table_row[] = form_upload('petition_doc3['.$susp->surc_no.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc3, '聲明異議文件3');
            }
            if(!isset($susp->petition_doc_ap1)||$susp->petition_doc_ap1==null){
                $table_row[] = form_upload('petition_doc_ap1['.$susp->surc_no.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc_ap1, '公文書1');
            }
            if(!isset($susp->petition_doc_ap2)||$susp->petition_doc_ap2==null){
                $table_row[] = form_upload('petition_doc_ap2['.$susp->surc_no.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc_ap2, '公文書2');
            }
            if(!isset($susp->petition_doc_ap3)||$susp->petition_doc_ap3==null){
                $table_row[] = form_upload('petition_doc_ap3['.$susp->surc_no.']','','');
            }
            else{
                $table_row[] = anchor_popup('訴願文件/' . $susp->petition_doc_ap3, '公文書3');
            }
            $this->table->add_row($table_row);
        }   
        $data['title'] = "查詢已送出案件";
        $data['s_table'] = $this->table->generate();
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/b/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surcharges/listPetitions';
        else $data['include'] = 'Surcharges_query/listPetitions';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
      
    public function uploadPetitions() {
        //var_dump($_FILES);
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        $petition_answer='';
        $petition_doc1='';
        $petition_doc2='';
        $petition_doc3='';
        $petition_doc_ap1='';
        $petition_doc_ap2='';
        $petition_doc_ap3='';
        foreach ($s_cnum1 as $key => $value) {
            $petition = $this->getsqlmod->getpetitionSurc($value)->result();
            $susp = $this->getsqlmod->getpetitionlist($value)->result();
            //if(!isset($petition[0]->petition_fdnum)){$this->getsqlmod->addpetition($data);}
            if(!empty($_FILES['petition_answer']['name'][$value])){//答辯書
                echo $value;
                $_FILES['file']['name'] = $value.'petition_answer.pdf';
                $_FILES['file']['type'] = $_FILES['petition_answer']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_answer']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_answer']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_answer']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_answer = $uploadData['file_name'];
                }
                else{
                    $error = array('error' => $this->upload->display_errors());
                    //var_dump($error);
                }
            }
            else{
                if(isset($petition[0]->petition_answer))$petition_answer = $petition[0]->petition_answer;
                else $petition_answer =null;
            }
            if(!empty($_FILES['petition_doc1']['name'][$value])){//公文
                $_FILES['file']['name'] = $value.'petition_doc1.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc1']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc1']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc1']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc1']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc1 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc1))$petition_doc1 = $petition[0]->petition_doc1;
                else $petition_doc1 =null;
            }
            if(!empty($_FILES['petition_doc2']['name'][$value])){//公文
                $_FILES['file']['name'] =$value.'petition_doc2.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc2']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc2']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc2']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc2']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc2 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc2))$petition_doc2 = $petition[0]->petition_doc2;
                else $petition_doc2 =null;
            }
            if(!empty($_FILES['petition_doc3']['name'][$value])){//公文
                $_FILES['file']['name'] = $value.'petition_doc3.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc3']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc3']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc3']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc3']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc3 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc3))$petition_doc3 = $petition[0]->petition_doc3;
                else $petition_doc3 =null;
            }
            if(!empty($_FILES['petition_doc_ap1']['name'][$value])){//行政訴訟
                $_FILES['file']['name'] = $value.'petition_doc_ap1.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc_ap1']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc_ap1']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc_ap1']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc_ap1']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc_ap1 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc_ap1))$petition_doc_ap1 = $petition[0]->petition_doc_ap1;
                else $petition_doc_ap1 =null;
            }
            if(!empty($_FILES['petition_doc_ap2']['name'][$value])){//行政訴訟
                $_FILES['file']['name'] = $value.'petition_doc_ap2.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc_ap2']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc_ap2']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc_ap2']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc_ap2']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc_ap2 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc_ap2))$petition_doc_ap2 = $petition[0]->petition_doc_ap2;
                else $petition_doc_ap2 =null;
            }
            if(!empty($_FILES['petition_doc_ap3']['name'][$value])){//行政訴訟
                $_FILES['file']['name'] = $value.'petition_doc_ap3.pdf';
                $_FILES['file']['type'] = $_FILES['petition_doc_ap3']['type'][$value];
                $_FILES['file']['tmp_name'] = $_FILES['petition_doc_ap3']['tmp_name'][$value];
                $_FILES['file']['error'] = $_FILES['petition_doc_ap3']['error'][$value];
                $_FILES['file']['size'] = $_FILES['petition_doc_ap3']['size'][$value];
                $config['upload_path'] = '訴願文件/'; 
                $config['allowed_types'] = 'pdf|doc|docx';
                $config['max_size'] = '5000'; // max_size in kb
                $this->load->library('upload',$config); 
                // File upload
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $petition_doc_ap3 = $uploadData['file_name'];
                }
            }
            else{
                if(isset($petition[0]->petition_doc_ap3))$petition_doc_ap3 = $petition[0]->petition_doc_ap3;
                else $petition_doc_ap3 =null;
            }
            //echo $_POST['petition_date'][$value];
            $data = array(
                //'value' => $value,
                'petition_date' => $_POST['petition_date'][$value],
                'petition_doc1' => $petition_doc1,
                'petition_doc2' => $petition_doc2,
                'petition_doc3' => $petition_doc3,
                'petition_doc_ap1' => $petition_doc_ap1,
                'petition_doc_ap2' => $petition_doc_ap2,
                'petition_doc_ap3' => $petition_doc_ap3,
                'petition_snum' => $susp[0]->s_num,
                'petition_cnum' => $susp[0]->s_cnum,
                'petition_surcnum' => $value,
            );
            ///var_dump($data);
            ///echo '<br>';
            if(isset($petition[0]->petition_surcnum)){
                $this->getsqlmod->updatepetitionSurc($value,$data);
            }
            else{
                $this->getsqlmod->addpetitionSurc($data);
            }
            redirect('Surcharges/listPetitions/'); 
        };
    }
    
    public function exportpublicCSV(){//公示清冊
        $this->load->dbutil();
        $this->load->helper('download');
        $id = $this->uri->segment(3);
        $filename = '怠金公示清冊'.date('Ymd').'.csv';
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/vnd.ms-excel; "); 
        $usersData = $this->getsqlmod->getsurchangelistSP($id)->result();
        $file = fopen('php://output', 'a');
        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
        $header = array("編號","發文日期","發文字號","怠金列管編號","受處分人姓名","身份證統一編號","公告原因"); 
        fputcsv($file, $header);
        $i=0;
        foreach ($usersData as $key=>$line){ 
            $i++;
            fputcsv($file,array($i,$line->surc_date,$line->surc_send_num,$line->surc_no,$line->s_name,$line->s_ic,'住居所不明'));
        }
        fclose($file); 
        exit;
        //$query = $this->getsqlmod->getfine_list($id); 
        //$data = $this->dbutil->csv_from_result($query);
        //force_download($filename,  $data);
    }
    
    public function exportnopunishCSV(){//不罰怠金清冊
        $this->load->dbutil();
        $this->load->helper('download');
        $id = $this->uri->segment(3);
        $filename = '怠金不罰清冊'.date('Ymd').'.csv';
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/vnd.ms-excel; "); 
        $usersData = $this->getsqlmod->getsurchangelistSP($id)->result();
        $file = fopen('php://output', 'a');
        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
        $header = array("編號","發文日期","發文字號","怠金列管編號","受處分人姓名","身份證統一編號","不罰原因1","不罰原因"); 
        fputcsv($file, $header);
        $i=0;
        foreach ($usersData as $key=>$line){ 
            $i++;
            fputcsv($file,array($i,$line->surc_date,$line->surc_send_num,$line->surc_no,$line->s_name,$line->s_ic,$line->sp_reason1,$line->sp_reason2));
        }
        fclose($file); 
        exit;
        //$query = $this->getsqlmod->getfine_list($id); 
        //$data = $this->dbutil->csv_from_result($query);
        //force_download($filename,  $data);
    }
	public function listdp1() {//每日處理專案
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $sp = $this->getsqlmod->getSP1($id)->result();
		// if(substr($id, 0, 1) != 'P')
		// {
			$test_table = $this->table_sp1($table,$id);
		// }
		// else
		// {
		// 	$test_table = $this->table_dprb1($table,$id);
		// }

		$courses=$this->getsqlmod->getCourseData()->result();
		$course_options = array(null => '請選擇(無)');
        foreach ($courses as $course){
            $course_options[trim($course->c_date) . '-' . trim($course->c_week) . '-' . trim($course->c_time) . '-' .preg_replace('/\r\n|\n/',"",trim($course->c_place))] = trim($course->c_date) . ' (' . trim($course->c_week) . ')時段：' . trim($course->c_time);
        }

        $data['s_table'] = $test_table;
        $data['sp_num'] = $sp[0]->sp_num;
		$data['sp_send_date'] = $this->tranfer2RCyear($sp[0]->sp_send_date);
		$data['sp_expirdate'] = $this->tranfer2RCyear($sp[0]->sp_expirdate);
		$data['sp_course'] = $sp[0]->sp_course;
		$data['sp_empno'] = $sp[0]->sp_empno;
		
		$data['course_opt'] = $course_options;
        $data['id'] = $id;
        $data['sp_status'] = $sp[0]->sp_status;
		if(substr($id, 0, 1) != 'P')
		{
			$data['url_1'] = "案件作業";
			$data['url'] = 'surcharges/listProject';
		}
		else
		{
			$data['url_1'] = "重處作業";
			$data['url'] = 'surcharges/listDpRollbackProject';
		}
		
        $data['title'] = "每日處理專案:".$sp[0]->sp_no;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/dplist1';
        else $data['include'] = 'Surcharges/dplist1';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
	function editSanc(){//批次裁處
        $this->load->library('table');
        $id = $this->uri->segment(3);
        $pjid = $this->uri->segment(4);
        // $fd1 = $this->getsqlmod->getFD('fd_snum',$id)->result();
        // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
        $surc=$this->getsqlmod->get1Sur_ed('surc_num',$id)->result();
		$DpOrder=$this->getsqlmod->get1SurOrder('surc_projectnum',$surc[0]->surc_projectnum)->result();
        // $sp=$this->getsqlmod->get3SP1($id)->result();
        // $spcount=$this->getsqlmod->getsurcAll()->result();
        // $cases=$this->getsqlmod->getCases($susp[0]->s_cnum)->result();
        // $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp[0]->s_dp_project)->result();
        // $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
        // $susp1 = $this->getsqlmod->get2Susp($susp[0]->s_num)->result(); // 抓snum
		
        $suspfadai = $this->getsqlmod->get1SuspFadai($surc[0]->surc_sic)->result();

        $prison=$this->getsqlmod->getWordlibPrison()->result();
        $courses=$this->getsqlmod->getCourseData()->result();

		$sp = $this->getsqlmod->getSP1($pjid)->result();
        ////var_dump($DpOrder);
        foreach ($DpOrder as $order){
            $arr[] = $order->surc_num;
        }
        $id; 
        $i=0;
       // echo count($arr);
        //print_r($arr);
        while ($arr[$i] !== $id) {
            $i++;
            next($arr); 
        }
        //echo $arr[$i];
        if($i>0){
            $data['prev'] = $arr[$i-1];
        }
        if($i==0){
            $data['prev'] = NULL;
        }
        if($i == count($arr)-1){
            $data['next'] = NULL;
        }
        if($i < count($arr)-1){
            $data['next'] = $arr[$i+1];
        }
        $type_options = array(null => '無');
        foreach ($prison as $prison){
            $type_options[$prison->prison_name . ':' . $prison->prison_address] = $prison->prison_name . ' （' . $prison->prison_address . '）';
        }
        $course_options = array(null => '請選擇(無)');
        foreach ($courses as $course){
            $course_options[trim($course->c_date) . '-' . trim($course->c_week) . '-' . trim($course->c_time) . '-' .preg_replace('/\r\n|\n/',"",trim($course->c_place))] = trim($course->c_date) . ' (' . trim($course->c_week) . ')時段：' . trim($course->c_time);
        }
		
        $query = $this->getsqlmod->getsurcAll()->result();
		$countid = $query[0]->surc_no;
		$lastNum = (((int)mb_substr($countid, 1, 3) == (date('Y')-1911))?(int)mb_substr($countid, -4, 4):0);
		$lastNum++;
		$value = str_pad($lastNum,4,'0',STR_PAD_LEFT);
		$taiwan_date = date('Y')-1911; 
		$surc_no= 'A' . $taiwan_date.$value;


        $data['title'] = "怠金裁處修改:". $id;
		
        // if(isset($sp[0]))$data['sp'] = $sp[0];
        // $data['cases'] = (isset($cases[0]))?$cases[0]:[];
        $data['surc'] = $surc[0];
        // if(isset($phone[0]))$data['phone'] = $phone[0];
        // if(isset($fine[0]))$data['fine'] = $fine[0];
        if(isset($suspfadai[0]))$data['suspfadai'] = $suspfadai[0];
		
        $data['empno'] = $sp[0]->sp_empno;//$this->session->userdata('uname');
		$data['current_loc'] = $i;
		$data['total_nums'] = $this->getsqlmod->getsurc($pjid)->num_rows(); 
        $data['opt'] = $type_options;
        $data['course_opt'] = $course_options;
        // $data['spcount'] = $spcount[0];
		$data['surc_no'] = $surc_no;
		$data['sp'] = $sp[0];
        // if(isset($fd1[0])){$data['fd'] = $fd1[0]; }else{ $data['fd'] = null; }
        $data['pjid'] = $pjid;
        $data['nav'] = "navbar3";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/dpediteach';
        else $data['include'] = 'Surcharges_query/dpediteach';
        //$data['include'] = 'disciplinary/dpediteach';
        $this->load->view('template', $data);
    }
	function editSanc_ready(){//批次裁處
        $this->load->library('table');
        $id = $this->uri->segment(3);
        $pjid = $this->uri->segment(4);
        // $fd1 = $this->getsqlmod->getFD('fd_snum',$id)->result();
        // $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
        $surc=$this->getsqlmod->get1Sur_ed('surc_num',$id)->result();
		$DpOrder=$this->getsqlmod->get1SurOrder('surc_projectnum',$surc[0]->surc_projectnum)->result();
        // $sp=$this->getsqlmod->get3SP1($id)->result();
        // $spcount=$this->getsqlmod->getsurcAll()->result();
        // $cases=$this->getsqlmod->getCases($susp[0]->s_cnum)->result();
        // $susp_dp=$this->getsqlmod->get1Susp_ed2('s_dp_project',$susp[0]->s_dp_project)->result();
        // $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
        // $susp1 = $this->getsqlmod->get2Susp($susp[0]->s_num)->result(); // 抓snum
		
        $suspfadai = $this->getsqlmod->get1SuspFadai($surc[0]->surc_sic)->result();

        $prison=$this->getsqlmod->getWordlibPrison()->result();
        $courses=$this->getsqlmod->getCourseData()->result();

		$sp = $this->getsqlmod->getSP1($pjid)->result();
        ////var_dump($DpOrder);
        foreach ($DpOrder as $order){
            $arr[] = $order->surc_num;
        }
        $id; 
        $i=0;
       // echo count($arr);
        //print_r($arr);
        while ($arr[$i] !== $id) {
            $i++;
            next($arr); 
        }
        //echo $arr[$i];
        if($i>0){
            $data['prev'] = $arr[$i-1];
        }
        if($i==0){
            $data['prev'] = NULL;
        }
        if($i == count($arr)-1){
            $data['next'] = NULL;
        }
        if($i < count($arr)-1){
            $data['next'] = $arr[$i+1];
        }
        $type_options = array(null => '無');
        foreach ($prison as $prison){
            $type_options[$prison->prison_name . ':' . $prison->prison_address] = $prison->prison_name . ' （' . $prison->prison_address . '）';
        }
        $course_options = array(null => '請選擇(無)');
        foreach ($courses as $course){
            $course_options[trim($course->c_date) . '-' . trim($course->c_week) . '-' . trim($course->c_time) . '-' .preg_replace('/\r\n|\n/',"",trim($course->c_place))] = trim($course->c_date) . ' (' . trim($course->c_week) . ')時段：' . trim($course->c_time);
        }
		
       

        $data['title'] = "怠金裁處修改:". $id;
		
        // if(isset($sp[0]))$data['sp'] = $sp[0];
        // $data['cases'] = (isset($cases[0]))?$cases[0]:[];
        $data['surc'] = $surc[0];
        // if(isset($phone[0]))$data['phone'] = $phone[0];
        // if(isset($fine[0]))$data['fine'] = $fine[0];
        if(isset($suspfadai[0]))$data['suspfadai'] = $suspfadai[0];
		
        $data['empno'] = $this->session->userdata('uname');
		$data['current_loc'] = $i;
		$data['total_nums'] = $this->getsqlmod->getsurc($pjid)->num_rows(); 
        $data['opt'] = $type_options;
        $data['course_opt'] = $course_options;
        // $data['spcount'] = $spcount[0];
		$data['sp'] = $sp[0];
        // if(isset($fd1[0])){$data['fd'] = $fd1[0]; }else{ $data['fd'] = null; }
        $data['pjid'] = $pjid;

		// 支號
		$noary = range(1,9);
		$charary = range('A','Z');
		$bnum = array_merge($noary,$charary);		
		$data['sort'] = $bnum[$i];

        $data['nav'] = "navbar3";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/dpedready';
        else $data['include'] = 'Surcharges_query/dpedready';
        //$data['include'] = 'disciplinary/dpediteach';
        $this->load->view('template', $data);
    }
	function getsur_coursedup(){
		$data = array(
			'surc_lec_date' => ((!empty($_POST['surc_lec_date']))?$_POST['surc_lec_date']:null),
			'surc_lec_time' => ((!empty($_POST['surc_lec_time']))?$_POST['surc_lec_time']:null),
			'surc_lec_address' => ((!empty($_POST['surc_lec_address']))?explode("(", $_POST['surc_lec_address'])[0]:null),
			'surc_sic' => $_POST['surc_sic']
		);
        $query = $this->getsqlmod->getsur_coursedup($data); 
        
        echo json_encode(array('data' => $query));
    }

	function uploaddpexpdate(){
        //var_dump($_POST);
		$pjid = $this->uri->segment(3);
        // $dp = $this->getsqlmod->getdpproject123($_POST['dp_num'])->result();
        //var_dump($dp);
        // $fp3 = $this->getsqlmod->getfp3($dp[0]->dp_name)->result();       
        $this->getsqlmod->updatesp_expirdate($_POST['sp_num'], $this->tranfer2ADyear($_POST['sp_expirdate']), $_POST['sp_empno']);
        // $this->getsqlmod->updatedpdateAll($fp3[0]->s_num,$_POST['dp_send_date']);
        redirect('Surcharges/listdp1/'.$pjid,'refresh');
    }
	function uploaddpcourse(){
        //var_dump($_POST);
		$pjid = $this->uri->segment(3);
        // $dp = $this->getsqlmod->getdpproject123($_POST['dp_num'])->result();
        //var_dump($dp);
        // $fp3 = $this->getsqlmod->getfp3($dp[0]->dp_name)->result();       
        $this->getsqlmod->updatesp_course($_POST['sp_num'], preg_replace('/\r\n|\n/',"",$_POST['sp_lec']));
        // $this->getsqlmod->updatedpdateAll($fp3[0]->s_num,$_POST['dp_send_date']);
        redirect('Surcharges/listdp1/'.$pjid,'refresh');
    }
	function uploaddpdate(){
		$pjid = $this->uri->segment(3);
        $fp3 = $this->getsqlmod->getsplist($pjid)->result();       
		$updatedata = array();
		$updatedata = array(
			'sp_send_date' => ((isset($_POST['sp_send_date']) && !empty($_POST['sp_send_date']))?$this->tranfer2ADyear($_POST['sp_send_date']):null),
			'sp_send_no' => $_POST['sp_send_no']
		);
		
        $this->getsqlmod->updatespdate($_POST['sp_num'], $updatedata);
		foreach ($fp3 as $sp) {
			$updatedata2 = array(
				'surc_date' => ((isset($_POST['sp_send_date']) && !empty($_POST['sp_send_date']))?$this->tranfer2ADyear($_POST['sp_send_date']):null)
			);

			$this->getsqlmod->updatespdateAll($sp->surc_num, $updatedata2);
		}
        
        redirect('Surcharges/listdp1_ready/'.$pjid,'refresh');
    }
	function updateSanc(){
		//    var_dump($_POST);
		//    exit;
			// $fd1 = $this->getsqlmod->getFD('fd_snum',$_POST['s_num'])->result();
			$surc1=$this->getsqlmod->get1Sur_ed('surc_num',$_POST['surc_num'])->result();
			// $susp1 = $this->getsqlmod->get1Susp_ed('s_num',$_POST['s_num'])->result();
			$fr1 = $this->getsqlmod->getFR('f_snum',$_POST['s_num'])->result();
			$phone1 = $this->getsqlmod->getPhone('p_s_num',$_POST['s_num'])->result();
			$sc1 = $this->getsqlmod->get2Susp($_POST['s_num'])->result();
			$delivery1 = $this->getsqlmod->getdelivery('fdd_snum',$_POST['s_num'])->result();
			$suspfadai = $this->getsqlmod->get1SuspFadai($_POST['surc_sic'])->result();
			if(!empty($_FILES['surc_prison_doc']['name'])){
				$_FILES['surc_prison_doc']['name']  = $_POST['surc_sic'] . $_FILES['surc_prison_doc']['name'];
				$config['upload_path']          = 'jiansuo/';
				$config['allowed_types']        = 'pdf';
				$config['max_size']             = 10000;
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('surc_prison_doc')){
					$error = array('error' => $this->upload->display_errors());
					//var_dump($error);
					//redirect('disciplinary_c/listDpProject/','refresh');
				}
				else{
					$uploadData = $this->upload->data();
					$_POST['surc_prison_doc']=$uploadData['file_name'];
					//$this->getsqlmod->uploadgongwen($_POST['dp_num'],$uploadData['file_name']);
				}
			}else{
				if($surc1[0]->surc_prison_doc != null)$_POST['surc_prison_doc']=$surc1[0]->s_prison_doc;
				else $_POST['surc_prison_doc']=null;
			}
			
			// $susp = array(
			// 	's_roffice' => $_POST['s_roffice'],
			// 	's_roffice1'  => $_POST['s_roffice1'],
			// 	's_fno'  => $_POST['s_fno'],
			// 	's_fdate'  => $this->tranfer2ADyear($_POST['s_fdate']),
			// 	 's_name'  => $_POST['s_name'],
			// 	 's_ic'  => $_POST['s_ic'],
			// 	 's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
			// 	's_prison'  => $_POST['s_prison'],
			// 	's_prison_doc'  => $_POST['s_prison_doc'],
			// 	's_utest_num'  => $_POST['s_utest_num'],
			// 	's_live_state' => (((int)$_POST['ad'] == 1)?1:((isset($_POST['s_prison']) && !empty($_POST['s_prison']))?3:2)),
			// 	's_rpcounty' => (isset($_POST['s_rpcounty']))?$_POST['s_rpcounty']:null,
			// 	's_rpdistrict' => (isset($_POST['s_rpdistrict']))?$_POST['s_rpdistrict']:null,
			// 	's_rpzipcode' => (isset($_POST['s_rpzipcode']))?$_POST['s_rpzipcode']:null,
			// 	's_rpaddress' => (isset($_POST['s_rpaddress']))?$_POST['s_rpaddress']:null,
			// 	's_dpcounty' => (isset($_POST['s_dpcounty']))?$_POST['s_dpcounty']:null,
			// 	's_dpdistrict' => (isset($_POST['s_dpdistrict']))?$_POST['s_dpdistrict']:null,
			// 	's_dpzipcode' => (isset($_POST['s_dpzipcode']))?$_POST['s_dpzipcode']:null,
			// 	's_dpaddress' => (isset($_POST['s_dpaddress']))?$_POST['s_dpaddress']:null,
			// 	'sp_doc' => $_POST['sp_doc']
			// );
			
			// $phone = array(
			// 	'p_no' => $_POST['p_no'],
			// 	'p_s_ic' => $_POST['s_ic'],
			// 	'p_s_num' => $_POST['s_num'],
			// 	'p_s_cnum' => $_POST['s_cnum'],
			// );
			// $delivery = array(
			// 	'fdd_fdnum'  => $_POST['fd_num'],
			// 	'fdd_snum'  => $_POST['s_num'],
			// 	'fdd_cnum'  => $_POST['s_cnum'],
			// );
			//$BVC = '21196'.$_POST['s_go_no'].$d;
			// $code =  '21720'.$_POST['sp_send_no'];
			// $code1 = preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY);
			// $code2=0;
			// for($i=0;$i<count($code1);$i++){
			// 	if($i==0||$i==3||$i==6||$i==9||$i==12||$i==15) $code2=$code2+$code1[$i]*3;
			// 	if($i==1||$i==4||$i==7||$i==10||$i==13) $code2=$code2+($code1[$i]*7);
			// 	if($i==2||$i==5||$i==8||$i==11||$i==14) $code2=$code2+($code1[$i]*1);
			// }
			// $code3 = $code2%10;
			// $code4 = 10-$code3;
			// $fin = $code.$code4;
			$surc = array(
				'surc_no' => $_POST['surc_no'],
				 'surc_amount' => $_POST['surc_amount'],
				 'surc_findate' => ((isset($_POST['surc_findate']))?$this->tranfer2ADyear($_POST['surc_findate']):null),
				//  'surc_BVC' => $fin,
				 'surc_prison' => $_POST['surc_prison'],
				 'surc_phone' => $_POST['surc_phone'],
				 'surc_date' =>  ((isset($_POST['surc_date']))?$this->tranfer2ADyear($_POST['surc_date']):null),
				 'surc_target' => (isset($_POST['ifsend2susp']))?join(",",$_POST['surc_target']):$_POST['surc_target'][0],
				 'surc_address' => (isset($_POST['ifsend2susp']))?join(",",$_POST['surc_address']):$_POST['surc_address'][0],
				 'surc_zipcode' => (isset($_POST['ifsend2susp']))?join(",",$_POST['surc_zipcode']):$_POST['surc_zipcode'][0],
				'surc_send_num' => $_POST['sp_send_no'],
				'surc_live_state' => (((int)$_POST['ad'] == 1)?1:((isset($_POST['surc_prison']) && !empty($_POST['surc_prison']))?3:2)),
				'surc_prison_doc' => $_POST['surc_prison_doc'],
				 'surc_other_lec' => 1,
				 'surc_lec_time'  => ((isset($_POST['surc_lec_time']) && !empty($_POST['surc_lec_time']))?$_POST['surc_lec_time']:null),
				 'surc_lec_date'  => ((isset($_POST['surc_lec_date']) && !empty($_POST['surc_lec_date']))?$_POST['surc_lec_date']:null),
				 'surc_lec_address'  => ((isset($_POST['surc_lec_address']) && !empty($_POST['surc_lec_address']))?rtrim(explode("(", $_POST['surc_lec_address'])[1], ')'):null),
				 'surc_lec_place'  => ((isset($_POST['surc_lec_address']) && !empty($_POST['surc_lec_address']))?explode("(", $_POST['surc_lec_address'])[0]:''),
				 'surc_empno' => $_POST['surc_empno'],
				 'surc_projectnum' =>$_POST['surc_projectnum']
			);		
			
			if(isset($_POST['s_fadai_name']))
			 {
				$susp_fadai = array(
					's_fadai_name' => $_POST['s_fadai_name'],
					's_fadai_ic'  => $_POST['s_fadai_ic'],
					's_fadai_sic'  => $_POST['surc_sic'],
					's_fadai_county'  => $_POST['s_fadai_county'],
					's_fadai_district'  => $_POST['s_fadai_district'],
					's_fadai_zipcode'  => $_POST['s_fadai_zipcode'],
					's_fadai_address'  => $_POST['s_fadai_address'],
					's_fadai_birth'  => $this->tranfer2ADyear($_POST['s_fadai_birth']),
					's_fadai_phone'  => $_POST['s_fadai_phone'],
					's_fadai_gender'  => $_POST['s_fadai_gender'],
				);
				if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['surc_sic'],$susp_fadai);
				else 
				{
					if(isset($_POST['s_fadai_name']))
					{
						$this->getsqlmod->addSuspFadai($susp_fadai);
					}
				}
			 }
			
			
			
			// $fine_rec = array(
			// 	'f_snum' => $_POST['s_num'],
			// 	'f_sic' => $_POST['s_ic'],
			// 	'f_cnum' => $_POST['s_cnum'],
			// 	'f_date' => $this->tranfer2ADyear($_POST['f_date']),
			// 	'f_damount' => $_POST['f_damount'],
			// 	'f_empno' => $_POST['fd_empno'][0],
			// 	'f_BVC' => $fin,
			// );
			////var_dump($_POST);
			//$this->getsqlmod->updateCase($_POST['sp_cnum'],$cases);
			// $this->getsqlmod->updateSusp($_POST['s_num'],$susp);
			// if(isset($sc1[0]))$this->getsqlmod->updatesc_up($_POST['s_num'],$sc);
			// else {
			//    $this->getsqlmod->addsc($sc);
			// }
			// if(isset($phone1[0]))
			// {
			// 	$this->getsqlmod->updatePhone($_POST['s_num'],$phone);
			// }
			// else {
			//    $this->getsqlmod->addphone($phone);
			// }
			// if(isset($delivery1[0]))$this->getsqlmod->updatedelivery($_POST['s_num'],$delivery);
			// else {
			//    $this->getsqlmod->adddelivery($delivery);
			// }
			if(isset($surc1[0]))
			{
				$this->getsqlmod->updateSurc_updatesanc($_POST['surc_num'],$surc);
			}
			else {
			   $this->getsqlmod->addsurc_receive($surc);
			}
			// if(isset($fr1[0]))$this->getsqlmod->updateFR($_POST['s_num'],$fine_rec);
			// else {
			//    $this->getsqlmod->addFR($fine_rec);
			// }
			
	
			// if(isset($e_id))
			// {
			//     $this->getsqlmod->updateDrug1($e_id, $drug);
			// }
			// else
			// {
			//     $d_count = (int)mb_substr($this->getsqlmod->get1DrugCount()->result()[0]->e_id, -5, 5);
			//     $eid = array(
			//         'e_id' => 'EN' . (date('Y')-1911) . str_pad($d_count++,5,'0',STR_PAD_LEFT)
			//     );
			//     $this->getsqlmod->adddrug(array_merge($drug, $eid));
			// }
			if($_POST['next']!=null) redirect('Surcharges/editSanc/' . $_POST['next'] . '/' . $_POST['surc_projectnum'],'refresh'); 
			if($_POST['prev']!=null) redirect('Surcharges/editSanc/' . $_POST['prev'] . '/' . $_POST['surc_projectnum'],'refresh'); 
			
			redirect('Surcharges/listdp1/' . $_POST['surc_projectnum'],'refresh'); 
		}
	
		// 自動儲存
		function autoUpdateSanc(){
			
			$surc1=$this->getsqlmod->get1Sur_ed('surc_num',$_POST['surc_num'])->result();
			// $susp1 = $this->getsqlmod->get1Susp_ed('s_num',$_POST['s_num'])->result();
			$fr1 = $this->getsqlmod->getFR('f_snum',$_POST['s_num'])->result();
			$phone1 = $this->getsqlmod->getPhone('p_s_num',$_POST['s_num'])->result();
			$sc1 = $this->getsqlmod->get2Susp($_POST['s_num'])->result();
			$delivery1 = $this->getsqlmod->getdelivery('fdd_snum',$_POST['s_num'])->result();
			$suspfadai = $this->getsqlmod->get1SuspFadai($_POST['surc_sic'])->result();
			if(!empty($_FILES['surc_prison_doc']['name'])){
				$_FILES['surc_prison_doc']['name']  = $_POST['surc_sic'] . $_FILES['surc_prison_doc']['name'];
				$config['upload_path']          = 'jiansuo/';
				$config['allowed_types']        = 'pdf';
				$config['max_size']             = 10000;
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('surc_prison_doc')){
					$error = array('error' => $this->upload->display_errors());
					//var_dump($error);
					//redirect('disciplinary_c/listDpProject/','refresh');
				}
				else{
					$uploadData = $this->upload->data();
					$_POST['surc_prison_doc']=$uploadData['file_name'];
					//$this->getsqlmod->uploadgongwen($_POST['dp_num'],$uploadData['file_name']);
				}
			}else{
				if($surc1[0]->surc_prison_doc != null)$_POST['surc_prison_doc']=$surc1[0]->s_prison_doc;
				else $_POST['surc_prison_doc']=null;
			}
	
			//  if(!empty($_FILES['sp_doc']['name'])){
			// 	$_FILES['sp_doc']['name']  = $_POST['s_ic'] . $_FILES['sp_doc']['name'];
			// 	$config['upload_path']          = 'drugdoc/';
			// 	$config['allowed_types']        = 'pdf';
			// 	$config['max_size']             = 10000;
			// 	$this->load->library('upload', $config);
			// 	if ( ! $this->upload->do_upload('sp_doc')){
			// 		$error = array('error' => $this->upload->display_errors());
			// 		//var_dump($error);
			// 		//redirect('disciplinary_c/listDpProject/','refresh');
			// 	}
			// 	else{
			// 		$uploadData = $this->upload->data();
			// 		$_POST['sp_doc']=$uploadData['file_name'];
			// 		//$this->getsqlmod->uploadgongwen($_POST['dp_num'],$uploadData['file_name']);
			// 	}
			// }else{
			// 	if($susp1[0]->sp_doc != null)$_POST['sp_doc']=$susp1[0]->sp_doc;
			// 	else $_POST['sp_doc']=null;
			// }
			//  $susp = array(
			// 	 's_roffice' => $_POST['s_roffice'],
			// 	 's_roffice1'  => $_POST['s_roffice1'],
			// 	 's_fno'  => $_POST['s_fno'],
			// 	 's_fdate'  => $this->tranfer2ADyear($_POST['s_fdate']),
			// 	 's_name'  => $_POST['s_name'],
			// 	 's_ic'  => $_POST['s_ic'],
			// 	 's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
			// 	 's_prison'  => $_POST['s_prison'],
			// 	 's_prison_doc'  => $_POST['s_prison_doc'],
			// 	 's_utest_num'  => $_POST['s_utest_num'],
			// 	 's_live_state' => (((int)$_POST['ad'] == 1)?1:((isset($_POST['s_prison']) && !empty($_POST['s_prison']))?3:2)),
			// 	 's_rpcounty' => (isset($_POST['s_rpcounty']))?$_POST['s_rpcounty']:null,
			// 	's_rpdistrict' => (isset($_POST['s_rpdistrict']))?$_POST['s_rpdistrict']:null,
			// 	's_rpzipcode' => (isset($_POST['s_rpzipcode']))?$_POST['s_rpzipcode']:null,
			// 	's_rpaddress' => (isset($_POST['s_rpaddress']))?$_POST['s_rpaddress']:null,
			// 	's_dpcounty' => (isset($_POST['s_dpcounty']))?$_POST['s_dpcounty']:null,
			// 	's_dpdistrict' => (isset($_POST['s_dpdistrict']))?$_POST['s_dpdistrict']:null,
			// 	's_dpzipcode' => (isset($_POST['s_dpzipcode']))?$_POST['s_dpzipcode']:null,
			// 	's_dpaddress' => (isset($_POST['s_dpaddress']))?$_POST['s_dpaddress']:null,
			// 	 'sp_doc' => $_POST['sp_doc']
			//  );
			//  $phone = array(
			// 	 'p_no' => $_POST['p_no'],
			// 	 'p_s_ic' => $_POST['s_ic'],
			// 	 'p_s_num' => $_POST['s_num'],
			// 	 'p_s_cnum' => $_POST['s_cnum'],
			//  );
			//  $delivery = array(
			// 	 'fdd_fdnum'  => $_POST['fd_num'],
			// 	 'fdd_snum'  => $_POST['s_num'],
			// 	 'fdd_cnum'  => $_POST['s_cnum'],
			//  );
			 //$BVC = '21196'.$_POST['s_go_no'].$d;
			//  $code =  '21720'.$_POST['sp_send_no'];
			// $code1 = preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY);
			// $code2=0;
			// for($i=0;$i<count($code1);$i++){
			// 	if($i==0||$i==3||$i==6||$i==9||$i==12||$i==15) $code2=$code2+$code1[$i]*3;
			// 	if($i==1||$i==4||$i==7||$i==10||$i==13) $code2=$code2+($code1[$i]*7);
			// 	if($i==2||$i==5||$i==8||$i==11||$i==14) $code2=$code2+($code1[$i]*1);
			// }
			// $code3 = $code2%10;
			// $code4 = 10-$code3;
			// $fin = $code.$code4;
			$surc = array(
				'surc_no' => $_POST['surc_no'],
				 'surc_amount' => $_POST['surc_amount'],
				 'surc_findate' => ((isset($_POST['surc_findate']))?$this->tranfer2ADyear($_POST['surc_findate']):null),
				//  'surc_BVC' => $fin,
				 'surc_prison' => $_POST['surc_prison'],
				 'surc_phone' => $_POST['surc_phone'],
				 'surc_date' =>  ((isset($_POST['surc_date']))?$this->tranfer2ADyear($_POST['surc_date']):null),
				 'surc_target' => (isset($_POST['ifsend2susp']))?join(",",$_POST['surc_target']):$_POST['surc_target'][0],
				 'surc_address' => (isset($_POST['ifsend2susp']))?join(",",$_POST['surc_address']):$_POST['surc_address'][0],
				 'surc_zipcode' => (isset($_POST['ifsend2susp']))?join(",",$_POST['surc_zipcode']):$_POST['surc_zipcode'][0],
				'surc_send_num' => $_POST['sp_send_no'],
				'surc_live_state' => (((int)$_POST['ad'] == 1)?1:((isset($_POST['surc_prison']) && !empty($_POST['surc_prison']))?3:2)),
				'surc_prison_doc' => $_POST['surc_prison_doc'],
				 'surc_other_lec' => 1,
				 'surc_lec_time'  => ((isset($_POST['surc_lec_time']) && !empty($_POST['surc_lec_time']))?$_POST['surc_lec_time']:null),
				 'surc_lec_date'  => ((isset($_POST['surc_lec_date']) && !empty($_POST['surc_lec_date']))?$_POST['surc_lec_date']:null),
				 'surc_lec_address'  => ((isset($_POST['surc_lec_address']) && !empty($_POST['surc_lec_address']))?rtrim(explode("(", $_POST['surc_lec_address'])[1], ')'):null),
				 'surc_lec_place'  => ((isset($_POST['surc_lec_address']) && !empty($_POST['surc_lec_address']))?explode("(", $_POST['surc_lec_address'])[0]:''),
				 'surc_empno' => $_POST['surc_empno'],
				 'surc_projectnum' =>$_POST['surc_projectnum']
			);		
			
			if(isset($_POST['s_fadai_name']))
			 {
				$susp_fadai = array(
					's_fadai_name' => $_POST['s_fadai_name'],
					's_fadai_ic'  => $_POST['s_fadai_ic'],
					's_fadai_sic'  => $_POST['surc_sic'],
					's_fadai_county'  => $_POST['s_fadai_county'],
					's_fadai_district'  => $_POST['s_fadai_district'],
					's_fadai_zipcode'  => $_POST['s_fadai_zipcode'],
					's_fadai_address'  => $_POST['s_fadai_address'],
					's_fadai_birth'  => $this->tranfer2ADyear($_POST['s_fadai_birth']),
					's_fadai_phone'  => $_POST['s_fadai_phone'],
					's_fadai_gender'  => $_POST['s_fadai_gender'],
				);
				if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['surc_sic'],$susp_fadai);
				else 
				{
					if(isset($_POST['s_fadai_name']))
					{
						$this->getsqlmod->addSuspFadai($susp_fadai);
					}
				}
			 }
			 
			 
			 
			//  $fine_rec = array(
			// 	 'f_snum' => $_POST['s_num'],
			// 	 'f_sic' => $_POST['s_ic'],
			// 	 'f_cnum' => $_POST['s_cnum'],
			// 	 'f_date' => $this->tranfer2ADyear($_POST['f_date']),
			// 	 'f_damount' => $_POST['f_damount'],
			// 	 'f_empno' => $_POST['fd_empno'][0],
			// 	 'f_BVC' => $fin,
			//  );
			 ////var_dump($_POST);
			 //$this->getsqlmod->updateCase($_POST['sp_cnum'],$cases);
			//  $this->getsqlmod->updateSusp($_POST['s_num'],$susp);
			//  if(isset($sc1[0]))$this->getsqlmod->updatesc_up($_POST['s_num'],$sc);
			//  else {
			//     $this->getsqlmod->addsc($sc);
			//  }
			//  if(isset($phone1[0]))$this->getsqlmod->updatePhone($_POST['s_num'],$phone);
			//  else {
			// 	$this->getsqlmod->addphone($phone);
			//  }
			//  if(isset($delivery1[0]))$this->getsqlmod->updatedelivery($_POST['s_num'],$delivery);
			//  else {
			// 	$this->getsqlmod->adddelivery($delivery);
			//  }
			//  if(isset($fd1[0]))$this->getsqlmod->updateFD($_POST['s_num'],$fd);
			// else {
			//    $this->getsqlmod->addFD($fd);
			// }
			//  if(isset($fr1[0]))$this->getsqlmod->updateFR($_POST['s_num'],$fine_rec);
			//  else {
			// 	$this->getsqlmod->addFR($fine_rec);
			//  }
			
	
			//  if(isset($e_id))
			// {
			//     $this->getsqlmod->updateDrug1($e_id, $drug);
			// }
			// else
			// {
			//     $d_count = (int)mb_substr($this->getsqlmod->get1DrugCount()->result()[0]->e_id, -5, 5);
			//     $eid = array(
			//         'e_id' => 'EN' . (date('Y')-1911) . str_pad($d_count++,5,'0',STR_PAD_LEFT)
			//     );
			//     $this->getsqlmod->adddrug(array_merge($drug, $eid));
			// }
			if(isset($surc1[0]))
			{
				$this->getsqlmod->updateSurc_updatesanc($_POST['surc_num'],$surc);
			}
			else {
			   $this->getsqlmod->addsurc_receive($surc);
			}
			if($_POST['next']!=null) redirect('Surcharges/editSanc/' . $_POST['next'] . '/' . $_POST['surc_projectnum'],'refresh'); 
			if($_POST['prev']!=null) redirect('Surcharges/editSanc/' . $_POST['prev'] . '/' . $_POST['surc_projectnum'],'refresh'); 
			redirect('Surcharges/listdp1/' . $_POST['surc_projectnum'],'refresh'); 
		 }
		 function updateready(){
				$surc1=$this->getsqlmod->get1Sur_ed('surc_num',$_POST['surc_num'])->result();
				// $susp1 = $this->getsqlmod->get1Susp_ed('s_num',$_POST['s_num'])->result();
				$fr1 = $this->getsqlmod->getFR('f_snum',$_POST['s_num'])->result();
				$phone1 = $this->getsqlmod->getPhone('p_s_num',$_POST['s_num'])->result();
				$sc1 = $this->getsqlmod->get2Susp($_POST['s_num'])->result();
				$delivery1 = $this->getsqlmod->getdelivery('fdd_snum',$_POST['s_num'])->result();
				$suspfadai = $this->getsqlmod->get1SuspFadai($_POST['surc_sic'])->result();
				
				
				// $susp = array(
				// 	's_roffice' => $_POST['s_roffice'],
				// 	's_roffice1'  => $_POST['s_roffice1'],
				// 	's_fno'  => $_POST['s_fno'],
				// 	's_fdate'  => $this->tranfer2ADyear($_POST['s_fdate']),
				// 	 's_name'  => $_POST['s_name'],
				// 	 's_ic'  => $_POST['s_ic'],
				// 	 's_birth'  => $this->tranfer2ADyear($_POST['s_birth']),
				// 	's_prison'  => $_POST['s_prison'],
				// 	's_prison_doc'  => $_POST['s_prison_doc'],
				// 	's_utest_num'  => $_POST['s_utest_num'],
				// 	's_live_state' => (((int)$_POST['ad'] == 1)?1:((isset($_POST['s_prison']) && !empty($_POST['s_prison']))?3:2)),
				// 	's_rpcounty' => (isset($_POST['s_rpcounty']))?$_POST['s_rpcounty']:null,
				// 	's_rpdistrict' => (isset($_POST['s_rpdistrict']))?$_POST['s_rpdistrict']:null,
				// 	's_rpzipcode' => (isset($_POST['s_rpzipcode']))?$_POST['s_rpzipcode']:null,
				// 	's_rpaddress' => (isset($_POST['s_rpaddress']))?$_POST['s_rpaddress']:null,
				// 	's_dpcounty' => (isset($_POST['s_dpcounty']))?$_POST['s_dpcounty']:null,
				// 	's_dpdistrict' => (isset($_POST['s_dpdistrict']))?$_POST['s_dpdistrict']:null,
				// 	's_dpzipcode' => (isset($_POST['s_dpzipcode']))?$_POST['s_dpzipcode']:null,
				// 	's_dpaddress' => (isset($_POST['s_dpaddress']))?$_POST['s_dpaddress']:null,
				// 	'sp_doc' => $_POST['sp_doc']
				// );
				
				// $phone = array(
				// 	'p_no' => $_POST['p_no'],
				// 	'p_s_ic' => $_POST['s_ic'],
				// 	'p_s_num' => $_POST['s_num'],
				// 	'p_s_cnum' => $_POST['s_cnum'],
				// );
				// $delivery = array(
				// 	'fdd_fdnum'  => $_POST['fd_num'],
				// 	'fdd_snum'  => $_POST['s_num'],
				// 	'fdd_cnum'  => $_POST['s_cnum'],
				// );
				//$BVC = '21196'.$_POST['s_go_no'].$d;
				$main_sendno = $_POST['main_send_no'];
				$code =  '21720'. substr($main_sendno, 0, 4). (($_POST['sort'] != 'A')?($_POST['sort']*1 -1):9). substr($main_sendno, 5);
				$code1 = preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY);
				$code2=0;
				for($i=0;$i<count($code1);$i++){
					if($i==0||$i==3||$i==6||$i==9||$i==12||$i==15) $code2=$code2+$code1[$i]*3;
					if($i==1||$i==4||$i==7||$i==10||$i==13) $code2=$code2+($code1[$i]*7);
					if($i==2||$i==5||$i==8||$i==11||$i==14) $code2=$code2+($code1[$i]*1);
				}
				$code3 = $code2%10;
				$code4 = 10-$code3;
				// $fin = $code.$code4;
				$fin = $code.((count(preg_split('//', $code4, -1, PREG_SPLIT_NO_EMPTY)) > 1)?preg_split('//', $code4, -1, PREG_SPLIT_NO_EMPTY)[count(preg_split('//', $code4, -1, PREG_SPLIT_NO_EMPTY))-1]:$code4);
				$surc = array(
					 'surc_BVC' => $fin,
					 'surc_prison' => $_POST['surc_prison'],
					 'surc_address' => $_POST['surc_address'],
					 'surc_zipcode' => $_POST['surc_zipcode'],
					'surc_send_num' => $_POST['sp_send_no'],
					'surc_live_state' => (((int)$_POST['ad'] == 1)?1:((isset($_POST['surc_prison']) && !empty($_POST['surc_prison']))?3:2)),
					 'surc_other_lec' => (isset($_POST['special_situ'])?1:null),
					 'surc_lec_time'  => ((isset($_POST['surc_lec_time']) && !empty($_POST['surc_lec_time']))?$_POST['surc_lec_time']:null),
					 'surc_lec_date'  => ((isset($_POST['surc_lec_date']) && !empty($_POST['surc_lec_date']))?$_POST['surc_lec_date']:null),
					 'surc_lec_address'  => ((isset($_POST['surc_lec_address']) && !empty($_POST['surc_lec_address']))?rtrim(explode("(", $_POST['surc_lec_address'])[1], ')'):null),
					 'surc_lec_place'  => ((isset($_POST['surc_lec_address']) && !empty($_POST['surc_lec_address']))?explode("(", $_POST['surc_lec_address'])[0]:''),
					 'surc_projectnum' =>$_POST['surc_projectnum']
				);		
				
				// if(isset($_POST['s_fadai_name']))
				//  {
				// 	$susp_fadai = array(
				// 		's_fadai_name' => $_POST['s_fadai_name'],
				// 		's_fadai_ic'  => $_POST['s_fadai_ic'],
				// 		's_fadai_sic'  => $_POST['surc_sic'],
				// 		's_fadai_county'  => $_POST['s_fadai_county'],
				// 		's_fadai_district'  => $_POST['s_fadai_district'],
				// 		's_fadai_zipcode'  => $_POST['s_fadai_zipcode'],
				// 		's_fadai_address'  => $_POST['s_fadai_address'],
				// 		's_fadai_birth'  => $this->tranfer2ADyear($_POST['s_fadai_birth']),
				// 		's_fadai_phone'  => $_POST['s_fadai_phone'],
				// 		's_fadai_gender'  => $_POST['s_fadai_gender'],
				// 	);
				// 	if(isset($suspfadai[0]))$this->getsqlmod->updateSuspFadai($_POST['surc_sic'],$susp_fadai);
				// 	else 
				// 	{
				// 		if(isset($_POST['s_fadai_name']))
				// 		{
				// 			$this->getsqlmod->addSuspFadai($susp_fadai);
				// 		}
				// 	}
				//  }
				
				
				
				// $fine_rec = array(
				// 	'f_snum' => $_POST['s_num'],
				// 	'f_sic' => $_POST['s_ic'],
				// 	'f_cnum' => $_POST['s_cnum'],
				// 	'f_date' => $this->tranfer2ADyear($_POST['f_date']),
				// 	'f_damount' => $_POST['f_damount'],
				// 	'f_empno' => $_POST['fd_empno'][0],
				// 	'f_BVC' => $fin,
				// );
				////var_dump($_POST);
				//$this->getsqlmod->updateCase($_POST['sp_cnum'],$cases);
				// $this->getsqlmod->updateSusp($_POST['s_num'],$susp);
				// if(isset($sc1[0]))$this->getsqlmod->updatesc_up($_POST['s_num'],$sc);
				// else {
				//    $this->getsqlmod->addsc($sc);
				// }
				// if(isset($phone1[0]))
				// {
				// 	$this->getsqlmod->updatePhone($_POST['s_num'],$phone);
				// }
				// else {
				//    $this->getsqlmod->addphone($phone);
				// }
				// if(isset($delivery1[0]))$this->getsqlmod->updatedelivery($_POST['s_num'],$delivery);
				// else {
				//    $this->getsqlmod->adddelivery($delivery);
				// }
				if(isset($surc1[0]))
				{
					$this->getsqlmod->updateSurc_updatesanc($_POST['surc_num'],$surc);
				}
				else {
				   $this->getsqlmod->addsurc_receive($surc);
				}
				// if(isset($fr1[0]))$this->getsqlmod->updateFR($_POST['s_num'],$fine_rec);
				// else {
				//    $this->getsqlmod->addFR($fine_rec);
				// }
				
		
				// if(isset($e_id))
				// {
				//     $this->getsqlmod->updateDrug1($e_id, $drug);
				// }
				// else
				// {
				//     $d_count = (int)mb_substr($this->getsqlmod->get1DrugCount()->result()[0]->e_id, -5, 5);
				//     $eid = array(
				//         'e_id' => 'EN' . (date('Y')-1911) . str_pad($d_count++,5,'0',STR_PAD_LEFT)
				//     );
				//     $this->getsqlmod->adddrug(array_merge($drug, $eid));
				// }
				if($_POST['next']!=null) redirect('Surcharges/editSanc_ready/' . $_POST['next'] . '/' . $_POST['surc_projectnum'],'refresh'); 
				if($_POST['prev']!=null) redirect('Surcharges/editSanc_ready/' . $_POST['prev'] . '/' . $_POST['surc_projectnum'],'refresh'); 
				
				redirect('Surcharges/editSanc_ready/'. $_POST['surc_num'].'/' . $_POST['surc_projectnum'],'refresh'); 
			}
		 public function listDpProject_Ready() {//送批
			$this->load->helper('form');
			$table = $this->session-> userdata('uoffice');
			
			$test_table = $this->table_SPProject1Ready($table);
			
			$data['s_table'] = $test_table;
			// $data['title'] = "專案處理";
			$data['title'] = "送批列表";
			$data['rollback'] = "N";
			$data['user'] = $this -> session -> userdata('uic');
			if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/dpprojectlist_ready';
			else $data['include'] = 'disciplinary_query/dpprojectlist_ready';
			$data['nav'] = 'navbar3';
			$this->load->view('template', $data);
		}
		public function updatestatus() {

			$this->load->helper('form');
			//var_dump($_POST);
			$s_cnum =mb_split(",",$_POST['s_cnum']);
			if($_POST['s_status']=='1'){
				foreach ($s_cnum as $key => $value) {
					$this->getsqlmod->updateSurcharge_Project_sp($value,'送批');
				}
				$sp_no = $this->getsqlmod->getSurcharge_Project2($s_cnum[0])->result()[0]->sp_no;
				if(substr($sp_no, 0, 1) != 'P')
				{
					redirect('Surcharges/listDpProject_Ready/'); 
				}
				else
				{
					redirect('Surcharges/listDpRollbackProject_Ready/'); 
				}
				
			}
		} 
		function listdp1_ready(){//送批列表
			$this->load->helper('form');
			$table = $this->session-> userdata('uoffice');
			$id = $this->uri->segment(3);
			$dp = $this->getsqlmod->getSP1($id)->result();
			if(substr($id, 0, 1) != 'P')
			{
				$test_table = $this->table_sp1($table,$id);
			}
			else
			{
				$test_table = $this->table_sp1($table,$id);
			}
			
			$data['s_table'] = $test_table;
			$data['sp_num'] = $dp[0]->sp_num;
			$data['sp_status'] = $dp[0]->sp_status;
			$data['sp_name'] = $dp[0]->sp_no;
			$data['sp_send_date'] = $this->tranfer2RCyear($dp[0]->sp_send_date);
			$data['sp_send_no'] = $dp[0]->sp_send_no;
			$data['id'] = $id;
			$data['title'] = "每日處理專案:".$dp[0]->sp_no;
			if(substr($id, 0, 1) != 'P')
			{
				$data['url_1'] = "案件作業";
				$data['url'] = 'surcharges/listDpProject_Ready';
				$data['rollback'] = 'N';
			}
			else
			{
				$data['url_1'] = "重處作業";
				$data['url'] = 'surcharges/listDpRollbackProject_Ready';
				$data['rollback'] = 'Y';
			}
			$data['user'] = $this -> session -> userdata('uic');
			if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/dplist1Ready';
			else $data['include'] = 'surcharges/dplist1Ready';
			//$data['include'] = 'disciplinary/dplist1Ready';
			$data['nav'] = 'navbar3';
			$this->load->view('template', $data);
		} 
		public function updatestatus_ready() {
			$this->load->helper('form');
			//var_dump($_POST);
			$user = $this -> session -> userdata('uic');
			$s_cnum =mb_split(",",$_POST['s_cnum']);
			if($_POST['s_status']=='1'){
				foreach ($s_cnum as $key => $value) {
					$this->getsqlmod->updateSurcharge_Project_sp($value,'已送批');
				}
				$sp_no = $this->getsqlmod->getSurcharge_Project2($s_cnum[0])->result()[0]->sp_no;
				if(substr($sp_no, 0, 1) != 'P')
				{
					redirect('surcharges/listDpProject_Ready/'); 
				}
				else
				{
					redirect('surcharges/listDpRollbackProject_Ready/'); 
				}
				
			}
			if($_POST['s_status']=='0'){
				foreach ($s_cnum as $key => $value) {
					$this->getsqlmod->updateSurcharge_Project_sp($value,'已寄出');
	
					$sp_no = $this->getsqlmod->getSurcharge_Project2($value)->result();  
					$sp3 = $this->getsqlmod->getsurchangelistSP($sp_no[0]->sp_no)->result();  
					foreach ($sp3 as $sp) {
						$updatedata2 = array(
							'sdd_maildate' => date('Y-m-d'),
							'sdd_status' => '已寄出',
							'sdd_fdnum' => $sp->surc_no
						);
			
						$this->getsqlmod->addSdddelivery($updatedata2);
					}
				}
				redirect('surcharges/listdelivery/'); 
			}
			if($_POST['s_status']=='rb'){
				foreach ($s_cnum as $key => $value) {
					$data = array(
							'sp_status' => '公示',
							//'dp_projectpublic_num' => $value
							//'fdp_status' => '未處理',
					);
					$this->getsqlmod->updatedrbspproject($value,$data);
	
					// $data1 = array(
					// 		'fpd_no' => $value,
					// 		'fdp_empno' => $user,
					// 		//'fdp_status' => '未處理',
					// );
					// if($value!=NULL);$this->getsqlmod->addfdp($data1);
				}
				redirect('surcharges/listPublicity1/'); 
			}
			if($_POST['s_status'] == 'rb_no')
			{
				foreach ($s_cnum as $key => $value) {
					$this->getsqlmod->updateSurcharge_Project_sp($value,'已寄出');
	
					$sp_no = $this->getsqlmod->getSurcharge_Project2($value)->result();  
					$sp3 = $this->getsqlmod->getsurchangelistSP($sp_no[0]->sp_no)->result(); 
					foreach ($sp3 as $sp) {
						$updatedata2 = array(
							'sdd_maildate' => date('Y-m-d'),
							'sdd_status' => '已寄出',
						);
			
						$this->getsqlmod->updatesurcdelivery($sp->surc_no, $updatedata2);
					}
				}
				redirect('surcharges/listdelivery/'); 
			}
		}
		public function listdelivery() {
			$this->load->helper('form');
			$table = $this->session-> userdata('uoffice');
			$test_table = $this->table_surcdelivery('文山第一分局');
			$success_table = $this->table_delivery_success($table);
			$data['s_table'] = $test_table;
			$data['s_success_table'] = $success_table;
			$data['title'] = "送達情形登錄";
			$data['user'] = $this -> session -> userdata('uic');
			if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/listdelivery';
			else $data['include'] = 'surcharges_query/listdelivery';
			$data['nav'] = 'navbar3';
			$this->load->view('template', $data);
		} 
		public function updatesurcdelivery(){
			// var_dump($_POST);
			// exit;
			$this->load->helper('form');
			$user = $this -> session -> userdata('uic');
			$office = $this -> session -> userdata('uoffice');
			$s_cnum =mb_split(",",$_POST['s_cnum']);
			
			
			foreach ($s_cnum as $key => $value) {
				//echo $value."<br>";
				if(isset($_POST['sdd_date'][$value]) && !empty($_POST['sdd_date'][$value])){
					$data = array(
						'sdd_date' => $this->tranfer2ADyear($_POST['sdd_date'][$value]),
						'sdd_status' => '已送達'
						//'surc_projectnum' => NULL,
					);
					$surc_no = $this->getsqlmod->getSurc_by_field('surc_num', $value)->result();
					$this->getsqlmod->updateSdd_doc($surc_no[0]->surc_no ,$data);
				}
				if(isset($_FILES['files']['name'][$value]))
				{
					$_FILES['file']['name'] = $_FILES['files']['name'][$value];
					$_FILES['file']['type'] = $_FILES['files']['type'][$value];
					$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$value];
					$_FILES['file']['error'] = $_FILES['files']['error'][$value];
					$_FILES['file']['size'] = $_FILES['files']['size'][$value];
					$config['upload_path'] = '送達文件/'; 
					$config['allowed_types'] = 'pdf|doc|docx';
					$config['max_size'] = '5000'; // max_size in kb
					$this->load->library('upload',$config); 
					// File upload
					if($this->upload->do_upload('file')){
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];
						$delivery = array(
							'sdd_doc'  => $filename,
						);
					$surc_no = $this->getsqlmod->getSurc_by_field('surc_num', $value)->result();
					$this->getsqlmod->updateSdd_doc($surc_no[0]->surc_no ,$delivery);
					}
				} 
			}
			redirect('Surcharges/listdelivery/'); 
		}
		function getCase_courseDup(){
			$data = array(
				'surc_lec_date' => $_POST['surc_lec_date'],
				'surc_lec_time' => $_POST['surc_lec_time'],
				'surc_lec_place' => explode("(", $_POST['surc_lec_place'])[0],
				'surc_sic' => $_POST['surc_sic']
			);
			$query = $this->getsqlmod->getsurccase_coursedup($data); 
			
			echo json_encode(array('data' => $query));
		}
		function rollback_fd(){
			$snum = $this->uri->segment(3);
			$fdd_maildate = $this->uri->segment(4);
			$fd = $this->getsqlmod->getSurc_by_field('surc_num', $snum)->result()[0];
			$fd_updatedate = array(
				'surc_rollback' => 1,
				'surc_backlog' => $this->tranfer2RCyear($fd->surc_date).'_'.$fd->surc_send_num. "_作廢\n",
				'surc_projectnum ' => null
			);
			$fdd_updatedate = array(
				'sdd_maildate ' => null,
				'sdd_status' => null
			);
			$this->getsqlmod->updaterollback_surc($snum, $fd_updatedate);
			$this->getsqlmod->updaterollback_sdd($fd->surc_no, $fdd_updatedate);
			redirect('surcharges/index','refresh' );
		}
		public function listDpRollbackProject() {//重處 - 專案處理(處分書)
			$this->load->helper('form');
			$table = $this->session-> userdata('uoffice');
			$rp=$this->getsqlmod->getrewardproject($table)->result();
			$test_table = $this->table_SurcProject_rollback($table);
			$data['s_table'] = $test_table;
			$data['title'] = "專案處理(重處)";
			$data['user'] = $this -> session -> userdata('uic');
			if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/dpprojectlist';
			else $data['include'] = 'surcharges_query/dpprojectlist';
			$data['nav'] = 'navbar3';
			$this->load->view('template', $data);
		}
		public function listDpRollbackProject_Ready() {//送批
			$this->load->helper('form');
			$table = $this->session-> userdata('uoffice');
			$rp=$this->getsqlmod->getrewardproject($table)->result();
			$test_table = $this->table_SPProject1Ready_rb($table);
			
			$data['s_table'] = $test_table;
			// $data['title'] = "專案處理";
			$data['title'] = "送批列表（重處）";
			$data['rollback'] = "Y";
			$data['user'] = $this -> session -> userdata('uic');
			if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/dpprojectlist_ready';
			else $data['include'] = 'surcharges_query/dpprojectlist_ready';
			$data['nav'] = 'navbar3';
			$this->load->view('template', $data);
		}
		public function listPublicity1() {//公示送達專案列表
			$this->load->helper('form');
			$table = $this->session-> userdata('uoffice');
			$test_table = $this->table_Publicity1();
			$data['s_table'] = $test_table;
			$data['title'] = "公示送達";
			$data['user'] = $this -> session -> userdata('uic');
			if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/listPublicity1';
			else $data['include'] = 'surcharges_query/listPublicity1';
			$data['nav'] = 'navbar3';
			$this->load->view('template', $data);
		} 
		public function updatepublicproject() {//公示專案修改
			$this->load->helper('form');
			$user = $this -> session -> userdata('uic');
			$office = $this -> session -> userdata('uoffice');
			$s_cnum1 =mb_split(",",$_POST['s_cnum1']);
			//var_dump($_POST);
			foreach ($s_cnum1 as $key => $value) {
				if(!empty($_FILES['files']['name'][$value])){
					$_FILES['file']['name'] = $_FILES['files']['name'][$value];
					$_FILES['file']['type'] = $_FILES['files']['type'][$value];
					$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$value];
					$_FILES['file']['error'] = $_FILES['files']['error'][$value];
					$_FILES['file']['size'] = $_FILES['files']['size'][$value];
					$config['upload_path'] = '送達文件/'; 
					$config['allowed_types'] = 'pdf|doc|docx';
					$config['max_size'] = '50000'; // max_size in kb
					$this->load->library('upload',$config); 
					// File upload
					if($this->upload->do_upload('file')){
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];
					}
				}
				if(!isset($filename))$filename=$_POST['sp_an'][$value];
				$data = array(
					'sp_public_date' => $this->tranfer2ADyear($_POST['sp_public_date'][$value]),
					'sp_an' => $filename,
					'sp_status' => '公示送達',
				);
				$data1 = array(
					'sdd_date' => $this->tranfer2ADyear($_POST['sp_public_date'][$value]),
					'sdd_status' => '公示送達',
					//'fpd_status' => '公示送達',
				);
				//var_dump($data);
				// echo '<br>';
				$this->getsqlmod->updatespdate($value,$data);
				$sp_no = $this->getsqlmod->getSurcproject_by_field('sp_num', $value)->result();
				$surcs = $this->getsqlmod->getSurc_by_field('surc_projectnum', $sp_no[0]->sp_no)->result();
				foreach ($surcs as $surc) {
					$this->getsqlmod->updatedsurcelivery($surc->surc_no,$data1);
				}
				
			};
			redirect('surcharges/listPublicity1/'); 
		}
		public function listPublicityProject() {
			$this->load->helper('form');
			$table = $this->session-> userdata('uoffice');
			$id = $this->uri->segment(3);
			//$dp = $this->getsqlmod->getDP1($id)->result();
			$test_table = $this->table_PublicityProject($id);
			$data['sp_no'] = $id;
			//$data['dp_name'] = $dp[0]->dp_name;
			$data['s_table'] = $test_table;
			$data['title'] = "公示送達專案：". $id;
			$data['url'] = 'surcharges/listPublicity1';
			$data['user'] = $this -> session -> userdata('uic');
			if(preg_match("/a/i", $this->session-> userdata('3permit')))$data['include'] = 'surcharges/listPublicityProject';
			else $data['include'] = 'surcharges_query/listPublicityProject';
			$data['nav'] = 'navbar3';
			$this->load->view('template', $data);
		}
		public function insertcomment(){
			if(isset($_POST['surc_comment']) && !empty($_POST['surc_comment']))
			{
				$this->getsqlmod->uploadsurc_comment($_POST['surc_num'],$_POST['surc_comment']);
				echo 'ok';
			}
			else
			{
				$this->getsqlmod->uploadsurc_comment($_POST['surc_num'],null);
				echo 'error';
			}
			// 
			// redirect('Acc_cert/listfp1/'.$_POST['fp_no']); 
			
		}
		function getthirdempno(){
			if(isset($_GET['search']))
			{
				$query = $this->getsqlmod->getthirdempnobysearch($_GET['search'], $_GET['unselected'])->result_array(); 
			}
			else
			{
				$query = $this->getsqlmod->getthirdempno($_GET['unselected'])->result_array(); 
			}
			
			echo json_encode( array('results' => $query));
		}
	function table_sp1($table,$id) {//
        $this->load->library('table');
        $query = $this->getsqlmod->getsplist($id)->result(); 
        $tmpl = array (
            'table_open' => '<table border="0" style="width:2400px" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','裁處','新怠金編號','罰鍰列管編號', '舊怠金編號','應講習時間',
        '依據單位', '依據日期', '依據字號', '受處分人姓名', '身分證號', '聯絡電話', '戶籍地址', '發文日期', '發文字號', '移送分局', '講習時數', '罰鍰送達時間');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $table_row = NULL;
            $table_row[] = $susp->surc_num;
			if(preg_match("/a/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('surcharges/editSanc_ready/' . $susp->surc_num . '/' . $id, '編輯');
            else $table_row[] ='';
			// if(preg_match("/b/i", $this->session-> userdata('3permit'))) $table_row[] = anchor('Surcharges/surc_prisoned/' . $susp->surc_no,'編輯在監資料');
            // else $table_row[] = '';
			$table_row[] = '<span class="text-danger">'.$susp->surc_no.'</span>';
            $table_row[] = $susp->surc_listed_no;
			$table_row[] = '<span class="text-info">'.$susp->surc_lastnum.'</span>';
            $table_row[] = $this->tranfer2RCyearTrad($susp->surc_olec_date);

			$table_row[] = '臺北市政府衛生局';

            $table_row[] = $this->tranfer2RCyearTrad($susp->surc_basenumdate);
            $table_row[] = $susp->surc_basenum;

            // $table_row[] = $susp->s_name;
            // $table_row[] = $susp->s_ic;
            // $table_row[] = $susp->p_no;
			$table_row[] = $susp->surc_name;
			$table_row[] = $susp->surc_sic;
			$table_row[] = [];

            // $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            // $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            // $table_row[] = $susp->s_date;
            // $table_row[] = $susp->r_county.$susp->r_district.$susp->r_address;
            // $table_row[] = $susp->s_office;
			$table_row[] = [];
            // $table_row[] = $susp->fd_lec_place;
            // $table_row[] = $susp->fd_date;
            // $table_row[] = $susp->fd_send_num;
            // $table_row[] = $susp->s_roffice;
            // $table_row[] = $susp->s_roffice1;
			$table_row[] = [];
			$table_row[] = [];
			$table_row[] = [];
			

            $table_row[] = '6';
            // $table_row[] = $susp->fdd_date;//過後要改成送達時間 資料未填入
			$table_row[] = [];
            
            
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_SPProject1Ready() {//怠金專案(送批)
        $this->load->library('table');
        $query = $this->getsqlmod->getSurcharge_send_Project()->result(); 
        $tmpl = array (
            'table_open' => '<table style="max-width: 1800px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號','專案時間','專案數量','專案狀態');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $sp)
        {
            $Surc = $this->getsqlmod->getsurc($sp->sp_no); 
            
            $table_row = NULL;
            $table_row[] = $sp->sp_num;
			$table_row[] = anchor('Surcharges/listdp1_ready/' . $sp->sp_no, $sp->sp_no);
            //$table_row[] = $sp->sp_date;
            $table_row[] = $this->tranfer2RCyearTrad($sp->sp_time);
            $table_row[] = $Surc->num_rows();
			$table_row[] = $sp->sp_status;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_SPProject1Ready_rb() {//怠金專案(重處-送批)
        $this->load->library('table');
        $query = $this->getsqlmod->getspproject_Ready_rollback()->result(); 
        $tmpl = array (
            'table_open' => '<table style="max-width: 1800px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號','專案時間','專案數量','專案狀態');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $sp)
        {
            $Surc = $this->getsqlmod->getsurc($sp->sp_no); 
            
            $table_row = NULL;
            $table_row[] = $sp->sp_num;
			$table_row[] = anchor('Surcharges/listdp1_ready/' . $sp->sp_no, $sp->sp_no);
            //$table_row[] = $sp->sp_date;
            $table_row[] = $this->tranfer2RCyearTrad($sp->sp_time);
            $table_row[] = $Surc->num_rows();
			$table_row[] = $sp->sp_status;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_surcdelivery($id) {//送達登入列表
        $this->load->library('table');
        $query = $this->getsqlmod->getsurcdeliverylist()->result(); 
		// var_dump($query);
		// exit;
        $tmpl = array (
            'table_open' => '<table style="width: 1400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        if(preg_match("/a/i", $this->session-> userdata('3permit'))) $this->table->set_heading( '','退回/重處','處分書編號'/*,'發文對象','發文地址','發文時間', '送達情形', '送達情形修改'*/,'送達日期','送達證書');
        else $this->table->set_heading( '','退回/重處','處分書編號'/*,'發文對象','發文地址','發文時間', '送達情形'*/, '送達證書');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $i++;
            
            $table_row = NULL;
            $table_row[] = $susp->surc_num;
			$table_row[] = anchor('surcharges/rollback_fd/'.$susp->surc_num .'/'.$susp->sdd_maildate , '退回/重處', array('class' => 'text-danger'));
            $table_row[] = $susp->surc_no . ' (' . $susp->surc_target . ')';
            
			$sdd_maildate = strtotime($susp->sdd_maildate);
			$today = strtotime(date('Y-m-d'));  
			$daydiff = round(($today - $sdd_maildate)/3600/24) ;
            if(preg_match("/a/i", $this->session-> userdata('3permit'))){
                if($susp->sdd_date == null){
                    $table_row[] = '<input id="sdd_date" name="sdd_date['.$susp->surc_num.']" value="" type="text" class="rcdate form-control"/> <span class="'.(($daydiff >=45)?'text-danger':'text-primary').'">寄送日：'.$this->tranfer2RCyear($susp->sdd_maildate).'</span>';
                }
                else{
                    $table_row[] = '<input id="sdd_date" name="sdd_date['.$susp->surc_num.']" value="'. $this->tranfer2RCyear($susp->sdd_date).'"  type="text" class="rcdate form-control"/> ';
                }
            }
            if($susp->sdd_doc == null){
                $table_row[] = '<input id="files" name="files['.$susp->surc_num.']" value=""  type="file" class=" form-control"/> ';
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->sdd_doc, '送達證書');
            }

			
			$this->table->add_row($table_row);
            
        }   
        return $this->table->generate();
    }
	function table_delivery_success($id) {//送達登入列表
        $this->load->library('table');
        $query = $this->getsqlmod->getsurcdeliverysuccesslist()->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table2">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
		$this->table->set_heading('退回/重處', '處分書編號','送達日期','送達證書');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $i++;
            
            $table_row = NULL;
			$table_row[] = anchor('surcharges/rollback_fd/'.$susp->surc_num .'/'.$susp->sdd_maildate , '退回/重處', array('class' => 'text-danger'));
            $table_row[] = $susp->surc_no . ' (' . $susp->surc_target . ')';
            
            $table_row[] = $this->tranfer2RCyear($susp->sdd_date);
            if($susp->sdd_doc == null){
                $table_row[] = '<strong>無送達文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->sdd_doc, '送達證書');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_Publicity1() {//公示送達專案列表
        $this->load->library('table');
        $query = $this->getsqlmod->getSurcharge_public_Project()->result(); 
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','專案編號', '公示送達時間','公報上傳');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $table_row = NULL;
            $table_row[] = $susp->sp_num;
            $table_row[] = anchor('surcharges/listPublicityProject/'.$susp->sp_no, $susp->sp_no);

			if($susp->sp_public_date == null){
				$table_row[] = '<input id="sp_public_date" name="sp_public_date['.$susp->sp_num.']" value="" type="text" class="rcdate form-control"/> <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>';
			}
			else{
				$table_row[] = '<input id="sp_public_date" name="sp_public_date['.$susp->sp_num.']" value="'. $this->tranfer2RCyear($susp->sp_public_date).'"  type="text" class="rcdate form-control"/> <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>';
			}

            // if($susp->fdp_delivery_date == null){
            //     $table_row[] = '<input name="fdp_delivery_date['.$susp->fdp_num.']" type="date" value="'.$susp->fdp_delivery_date.'"/> ';
            // }
            // else{
            //     $table_row[] = '<input name="fdp_delivery_date['.$susp->fdp_num.']" type="date" value="'.$susp->fdp_delivery_date.'"/> ';
            // }
            if($susp->sp_an == null){
                $table_row[] = form_upload('files['.$susp->sp_num.']','','');
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->sp_an, '公報').form_hidden('sp_an['.$susp->sp_num.']', $susp->sp_an);
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	function table_PublicityProject($id) {//公示送達列表
        $this->load->library('table');
        $query = $this->getsqlmod->getsurchangelistSP_public($id)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','發文對象','發文地址','發文時間', '送達情形', '送達證書');
        $table_row = array();
        $i=0;
        $today = date('Y-m-d');
        foreach ($query as $susp)
        {
            $table_row = NULL;
            $table_row[] = $susp->surc_num;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->surc_target;
            $table_row[] = $susp->surc_address;
            $table_row[] = $susp->surc_date;
            if($susp->sdd_date == null){
                $table_row[] = '<strong>未登入</strong>';
            }
            else{
                $table_row[] = $susp->sdd_status.'；'.$susp->sdd_date;
            }
            if($susp->sdd_doc == null){
                $table_row[] = '<strong>無送達文件</strong>';
            }
            else{
                $table_row[] = anchor_popup('送達文件/' . $susp->sdd_doc, '送達證書');
            }
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
	public function delete_Surc_project(){
        if(isset($_POST['sp_num']))
        {
            $this->getsqlmod->delSurcProject($_POST['sp_num']);

            echo 'ok';
        }
        else
        {
            echo 'error';
        }
        
    }

	   /** 轉西元年 */
	   public function tranfer2ADyear($date)
	   {
		   if(substr($date, 0, 1) == 0 || substr($date, 0, 1) == '0')
		   {
			   $date = substr($date, 1, 6);            
		   }
			   
		   if(strlen($date) == 6)
		   {
			   $ad = ((int)substr($date, 0, 2)) + 1911;
			   return (string)$ad .'-' . substr($date, 2, 2) . '-' . substr($date, 4, 2);
		   }
		   elseif(strlen($date) == 7)
		   {
			   
			   $ad = ((int)substr($date, 0, 3)) + 1911;
			   return (string)$ad . '-' . substr($date, 3, 2) . '-' . substr($date, 5, 2);
		   }
		   else
		   {
			   return '';
		   }
   
		   
	   }

	   /** 轉民國年 */
	   public function tranfer2RCyear($date)
	   {
		   $datestr = str_replace('-', '', $date);
		   
		   $rc = ((int)substr($datestr, 0, 4)) - 1911;
		   return (string)$rc . substr($datestr, 4, 4) ;
	   }

	   /** 轉民國年 中文年-月-日 */
	   function tranfer2RCyearTrad($date)
	   {
		   $date = str_replace('-', '', $date);
		   $rc = ((int)substr($date, 0, 4)) - 1911;
		   return (string)$rc . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2) ;
	   }

	   /** 轉西元年(2021/01/10) */
	   public function tranfer2ADyearStyle2($date)
	   {
		   $datestr = explode('/', $date);
		   
		   $ad = $datestr[0] . '-'. str_pad($datestr[1],2,'0',STR_PAD_LEFT). '-'. str_pad($datestr[2],2,'0',STR_PAD_LEFT);
		   return $ad ;
	   }
    
}
