<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
class Integration extends CI_Controller {//帳務憑證
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }
    // 裁罰excel讀取規則
    public function getFH_excel_rule()
    {
        return array(
			'111'=>array(
                'sheet' => '總表111',
                'usecols' => 'A,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '110'=>array(
                'sheet' => '總表110',
                'usecols' => 'A,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '109'=>array(
                'sheet' => '總表107',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '108'=>array(
                'sheet' => '總表107',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '107'=>array(
                'sheet' => '總表107',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '106'=>array(
                'sheet' => '總表106',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '105'=>array(
                'sheet' => '總表',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '104'=>array(
                'sheet' => '總表',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '103'=>array(
                'sheet' => '總表',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '102'=>array(
                'sheet' => '總表',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '101'=>array(
                'sheet' => '總表',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '100'=>array(
                'sheet' => '總表',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
            '99'=>array(
                'sheet' => '99年度',
                'usecols' => 'L,B,C,D,V,W,X,AI,AJ,AK,AC,AD,E,M,R'
            ),
        );
    }
    // 移送excel讀取規則
    public function getES_excel_rule()
    {
        return array(            
            'ATRS109'=>array(
                'sheet' => '109年再移送',
                'usecols' => 'CD,CE,CF,BT,X,D,C,CC',
                'skiprow' => 0,
                'use' => true
            ),
			'111'=>array(
                'sheet' => '使用郵票 ',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => true
            ),
            '110'=>array(
                'sheet' => '使用郵票 ',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => true
            ),
            '109'=>array(
                'sheet' => '使用郵票',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => false
            ),            
            '108'=>array( 
                'sheet' => '使用郵票 ',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => true
            ),
            '107'=>array(
                'sheet' => '使用郵票',
                'usecols' => 'A,B,C,D,E,F,G,H',
                'skiprow' => 0,
                'use' => true
            ),
            '106'=>array(
                'sheet' => '使用郵票登記簿',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => true
            ),
            '105'=>array(
                'sheet' => '使用郵票登記簿',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => true
            ),
            '104'=>array(
                'sheet' => '使用郵票登記簿',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => true
            ),
            '103'=>array(
                'sheet' => '使用郵票登記簿',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 0,
                'use' => true
            ),
            '102'=>array(
                'sheet' => '使用郵票登記簿',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 0,
                'use' => true
            ),
            '101'=>array(
                'sheet' => '使用郵票登記簿',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => true
            ),
            '100'=>array(
                'sheet' => '使用郵票登記簿',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => true
            ),
            '99'=>array(
                'sheet' => '使用郵票登記簿',
                'usecols' => 'A,B,C,D,E,F,G,K',
                'skiprow' => 1,
                'use' => false
            ),
        );
    }

    // 所有罰鍰案件(Psh99to109)excel讀取規則
    public function getAllFH_excel_rule()
    {
        // '所屬年度', '案件編號', "受處分人姓名","身分證編號","罰鍰（萬元）"
        return array(
            'usecols' => 'A,B,O,P,L'            
        );
    }
    // 所有罰鍰案件(Psh99to109)excel讀取規則
    public function getAllES_excel_rule()
    {
        // '移送案號','案件編號','移送日期','分署'
        return array(
            'usecols' => 'A,F,J,H'            
        );
    }
    // 帳務excel讀取規則
    public function getCW_excel_rule()
    {
        return array(
            '完納' => array(
                '108' => array(
                    'sheet' => '完納',
                    'usecols' => 'E,G,I',
                    'skiprow' => 0,
                    'use' => true
                ),
                '109' => array(
                    'sheet' => '完納',
                    'usecols' => 'E,G,K',
                    'skiprow' => 1,
                    'use' => true
                ),
                '110' => array(
                    'sheet' => '完納',
                    'usecols' => 'E,G,K',
                    'skiprow' => 1,
                    'use' => true
				),
				'111' => array(
                    'sheet' => '完納',
                    'usecols' => 'E,G,K',
                    'skiprow' => 1,
                    'use' => true
                )
            ),  
            '分期完納' => array(
                '109special' => array(
                    'sheet' => '',
                    'usecols' => 'F,H,J',
                    'skiprow' => 0,
                    'use' => true
                ),
                '110' => array(
                    'sheet' => '分期完',
                    'usecols' => 'E,G,I',
                    'skiprow' => 1,
                    'use' => true
				),
				'111' => array(
                    'sheet' => '分期完',
                    'usecols' => 'E,G,I',
                    'skiprow' => 1,
                    'use' => true
                )
            ),  
            '分期' => array(
				'111' => array(
                    'sheet' => '分期',
                    'usecols' => 'E,G,I',
                    'skiprow' => 1,
                    'use' => true
                )
            ),        
        );
    }

    // 憑證excel讀取規則
    public function getPC_excel_rule()
    {
        return array(   
			'111'=>array(
                'sheet' => '罰鍰憑證111',
                'usecols' => 'A,E,I',
                'skiprow' => 1,
                'use' => true
            ),
            '110'=>array(
                'sheet' => '債權憑證-全',
                'usecols' => 'B,F,J',
                'skiprow' => 0,
                'use' => true
            ),
            '109'=>array(
                'sheet' => '罰鍰憑證110',
                'usecols' => 'A,E,I',
                'skiprow' => 1,
                'use' => true
            )
        );
    }
    // 撤註銷excel讀取規則
    public function getCS_excel_rule()
    {
        return array(   
            '110'=>array(
                'sheet' => array(
                    '註銷與撤銷明細',
                    '註銷與撤銷明細2'
                ),
                'usecols' => 'A,C,E,F',
                'skiprow' => 0
            )
        );
    }
    // 執行命令excel讀取規則
    public function getML_excel_rule()
    {
        return array(   
            '110'=>array(
                'sheet' => '執行命令',
                'usecols' => 'A,C,D,F',
                'skiprow' => 0,
                'use' => true
            )
        );
    }
    function index(){
        $data['title'] = "整合入口";
        $data['user'] = $this -> session -> userdata('uic');
        $data['data'] = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        $data['include'] = 'integration/enter';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    // 上傳並處理 裁罰資料
    function uploadfiles()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");

        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');

        $f_year = $_POST['f_year'];
        $f_belong = $_POST['f_belong'];
        $newfilename = $_POST['f_belong'] . '_' . $_POST['f_year'];
        $oldfilename = $_FILES['upload_file']['name'];
        $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
        $dirpath = 'integradoc';

        // integradoc 目錄是否存在，不存在即建立
        if (!file_exists($dirpath)) {
            mkdir($dirpath, 0755, true);
        }
        // 先儲存原始excel檔
        $config['upload_path'] = $dirpath . '/'; 
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = '50000'; // max_size in kb
        $config['overwrite'] = true; // 覆蓋
        $this->load->library('upload',$config); 
        // File upload
        if ( ! $this->upload->do_upload('upload_file'))
        {
                $error = array('error' => $this->upload->display_errors());
                $status = $error;
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $status = 'done';
        }

        // 讀取上傳的excel
        // 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }
        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        try {
            $reader->setReadDataOnly(TRUE);
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }
        
        $worksheet = $spreadsheet->getSheetByName($this->getFH_excel_rule()[$f_year]['sheet']);
        // Get the highest row and column numbers referenced in the worksheet
        $highestRow = $worksheet->getHighestRow(); // e.g. 10  

        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $useCols = explode(',', $this->getFH_excel_rule()[$f_year]['usecols']);

        // 合併原始檔的特定欄數到新檔
        for ($row = 1; $row <= $highestRow; ++$row) {
            
                if($f_year == '107')
                {
                    // 產生「所屬年度」欄位
                    $newworksheet->setCellValueByColumnAndRow(1, 1, '年度');
                    $newworksheet->setCellValueByColumnAndRow(1, $row, $f_year);
                    foreach ($useCols as $key => $value) {
                        $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($value);
                        $colValue = $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue(). PHP_EOL;
                        // $colValue = ((null !== $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue())?$worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue():'0');
                        $newworksheet->setCellValueByColumnAndRow(($key + 2), $row, $colValue);
                    }
                }
                else
                {
                    // 移除沒有用的資料
                    if (!empty($worksheet->getCellByColumnAndRow(12, $row)->getValue()) )
                    {
                        // 產生「所屬年度」欄位
                        $newworksheet->setCellValueByColumnAndRow(1, 1, '年度');
                        $newworksheet->setCellValueByColumnAndRow(1, $row, $f_year);
                        foreach ($useCols as $key => $value) {
                            $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($value);
                            $colValue = $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue(). PHP_EOL;
                            // $colValue = ((null !== $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue())?$worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue():'0');
                            $newworksheet->setCellValueByColumnAndRow(($key + 2), $row, $colValue);
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            
        }
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $newfilename . '.' . $ext);    

        // 記錄新增至資料表
        $data = array(
            "f_belong" => $f_belong,
            "f_year" => $f_year,
            "f_filename" => $newfilename . '.' . $ext,
            "f_ori_filename" => $oldfilename,
            "f_upload_date" => date('Y-m-d H:i:s'),
            "f_status" => $status,
            "emp_no" => $user
        );
        if($this->getsqlmod->addIntegrationLog($data))
            echo 'ok';
        else
            echo 'error';
        
    }
    // 上傳並處理 帳務
    function uploadCWfiles()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");

        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');

        $f_year = $_POST['f_year'];
        $f_belong = $_POST['f_belong'];
        $newfilename = $_POST['f_belong'] . '_' . $_POST['f_year'];
        $oldfilename = $_FILES['upload_file']['name'];
        $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
        $dirpath = 'integradoc';

        // integradoc 目錄是否存在，不存在即建立
        if (!file_exists($dirpath)) {
            mkdir($dirpath, 0755, true);
        }
        // 先儲存原始excel檔
        $config['upload_path'] = $dirpath . '/'; 
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = '50000'; // max_size in kb
        $config['overwrite'] = true; // 覆蓋
        $this->load->library('upload',$config); 
        // File upload
        if ( ! $this->upload->do_upload('upload_file'))
        {
                $error = array('error' => $this->upload->display_errors());
                $status = $error;
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $status = 'done';
        }

        // 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }

        try {
            $reader->setReadDataOnly(TRUE);
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }

        // 完納 匯出header
        $exp_file_header_0 = array('案件編號', '完納日期', "完納金額");
        // 分期完納, 分期 匯出header
        $exp_file_header_1 = array('案件編號', '繳款日期', "繳款金額");
        if($f_year != '109special')
        {
            // 108 109 110年規則 - 處理 0完納,1分期完納,2分期
            $pay110Rule = array_column($this->getCW_excel_rule(), $f_year);
            

            // loop三圈
            foreach ($pay110Rule as $ruleKey => $ruleValue) {

                // 該年度是否處理，false則跳出
                if(!$pay110Rule[$ruleKey]['use'])
                {
                    echo 'ok';
                    exit;
                }
                $newfilename = $f_belong . '_' . $f_year . '_' . (($ruleKey == 0)?'完納':(($ruleKey == 1)?'分期完納':'分期'));

                $worksheet = ((null != $spreadsheet->getSheetByName($pay110Rule[$ruleKey]['sheet']))?$spreadsheet->getSheetByName($pay110Rule[$ruleKey]['sheet']):$spreadsheet->getActiveSheet());
                // Get the highest row and column numbers referenced in the worksheet
                $highestRow = $worksheet->getHighestRow(); // e.g. 10  

                // 新開一個空的excel檔做寫入
                $newsheet = new Spreadsheet(); 
                $newworksheet = $newsheet->getActiveSheet(); 
                $useCols = explode(',', $pay110Rule[$ruleKey]['usecols']);
                $startRow = (int)$pay110Rule[$ruleKey]['skiprow'];

                // 合併原始檔的特定欄數到新檔
                for ($row = 1; $row <= $highestRow; ++$row) {
                    $startRow++;
                    // 移除沒有用的資料
                    if (!empty($worksheet->getCellByColumnAndRow(5, $startRow)->getValue()) && !empty($worksheet->getCellByColumnAndRow(6, $startRow)->getValue()))
                    {
                        foreach ($useCols as $key => $value) {
                            if($row == 1)
                            {
                                if($ruleKey == 0)                                
                                    $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, $exp_file_header_0[$key]);
                                else
                                    $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, $exp_file_header_1[$key]);
                            }
                            else
                            {
                                $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($value);

                                if($ruleKey == 0)
                                {
                                    if($colIndex == 11)
                                    {
                                        $colValue = ((int)$worksheet->getCellByColumnAndRow(9, $startRow)->getFormattedValue() - (int)$worksheet->getCellByColumnAndRow(12, $startRow)->getFormattedValue());
                                    }
                                    else
                                    { 
                                        $colValue = $worksheet->getCellByColumnAndRow($colIndex, $startRow)->getValue();
                                    }                                    
                                }
                                else
                                {
                                    $colValue = $worksheet->getCellByColumnAndRow($colIndex, $startRow)->getFormattedValue();
                                }
                                
                                $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, $colValue);
                            }                            
                        }
                    }
                    else
                    {
                        continue;
                    }
                    
                }
                $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
                $writer->setPreCalculateFormulas(false);
                $writer->save($dirpath . '/' . $newfilename . '.' . $ext);   

                // 記錄新增至資料表
                $data = array(
                    "f_belong" => $f_belong,
                    "f_year" => $f_year,
                    "f_filename" => $newfilename . '.' . $ext,
                    "f_ori_filename" => $oldfilename,
                    "f_upload_date" => date('Y-m-d H:i:s'),
                    "f_status" => $status,
                    "emp_no" => $user
                );
                if($this->getsqlmod->addIntegrationLog($data))
                    echo 'ok';
                else
                    echo 'error';
            }            
            
        }
		else // 處理 CP99to109.xlsx
        {
            // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            // try {
            //     $reader->setReadDataOnly(TRUE);
            //     $spreadsheet = $reader->load($dirpath . '/CP99to109.xlsx');
            // } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            //     die($e->getMessage());
            // }
			if(!$this->getCW_excel_rule()['分期完納']['109special']['use'])
			{
				echo 'ok';
				exit;
			}
            $newfilename = $f_belong . '_109_分期完納';
            $worksheet = $spreadsheet->getActiveSheet();
            $highestRow = $worksheet->getHighestRow(); // e.g. 10  

            // 新開一個空的excel檔做寫入
            $newsheet = new Spreadsheet(); 
            $newworksheet = $newsheet->getActiveSheet(); 
            $useCols = explode(',', $this->getCW_excel_rule()['分期完納']['109special']['usecols']);
            $startRow = (int)$this->getCW_excel_rule()['分期完納']['109special']['skiprow'];

            // 合併原始檔的特定欄數到新檔
            for ($row = 1; $row <= $highestRow; ++$row) {
                $startRow++;
                // 移除沒有用的資料
                if (!empty($worksheet->getCellByColumnAndRow(6, $startRow)->getValue()) && !empty($worksheet->getCellByColumnAndRow(7, $startRow)->getValue()))
                {
                    foreach ($useCols as $key => $value) {
                        if($row == 1)
                        {
                            $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, $exp_file_header_1[$key]);
                        }
                        else
                        {
                            $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($value);

                            $colValue = $worksheet->getCellByColumnAndRow($colIndex, $startRow)->getFormattedValue();
                            
                            $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, $colValue);
                        }                            
                    }
                }
                else
                {
                    continue;
                }
                
            }
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
            $writer->setPreCalculateFormulas(false);
            $writer->save($dirpath . '/' . $newfilename . '.xlsx'); 

            // 記錄新增至資料表
            $data = array(
                "f_belong" => $f_belong,
                "f_year" => $f_year,
                "f_filename" => $newfilename . '.xlsx',
                "f_ori_filename" => $oldfilename,
                "f_upload_date" => date('Y-m-d H:i:s'),
                "f_status" => 'done',
                "emp_no" => $user
            );
			if($this->getsqlmod->addIntegrationLog($data))
				echo 'ok';
			else
				echo 'error';
        }
        
    }
    // 上傳並處理 執行命令
    function uploadMLfiles()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");

        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');

        $f_year = $_POST['f_year'];
        $f_belong = $_POST['f_belong'];
        $newfilename = $_POST['f_belong'] . '_' . $_POST['f_year'];
        $oldfilename = $_FILES['upload_file']['name'];
        $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
        $dirpath = 'integradoc';

        // integradoc 目錄是否存在，不存在即建立
        if (!file_exists($dirpath)) {
            mkdir($dirpath, 0755, true);
        }
        // 先儲存原始excel檔
        $config['upload_path'] = $dirpath . '/'; 
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = '50000'; // max_size in kb
        $config['overwrite'] = true; // 覆蓋
        $this->load->library('upload',$config); 
        // File upload
        if ( ! $this->upload->do_upload('upload_file'))
        {
                $error = array('error' => $this->upload->display_errors());
                $status = $error;
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $status = 'done';
        }

        // 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }
        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        try {
            $reader->setReadDataOnly(FALSE);
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }
        
        $worksheet = $spreadsheet->getSheetByName($this->getML_excel_rule()[$f_year]['sheet']);
        // Get the highest row and column numbers referenced in the worksheet
        $highestRow = $worksheet->getHighestRow(); // e.g. 10  

        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $useCols = explode(',', $this->getML_excel_rule()[$f_year]['usecols']);

        // 合併原始檔的特定欄數到新檔
        for ($row = 1; $row <= $highestRow; ++$row) {
            // 移除沒有用的資料
            if (!empty($worksheet->getCellByColumnAndRow(1, $row)->getValue()) && !empty($worksheet->getCellByColumnAndRow(3, $row)->getValue()))
            {
                foreach ($useCols as $key => $value) {
                    $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($value);
                    try {
                        if(1 != $worksheet->getCellByColumnAndRow($colIndex, $row)->isFormula())
                        {
                            $colValue = $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue(). PHP_EOL;
                        }
                        else
                        {
                            $colValue = $worksheet->getCellByColumnAndRow($colIndex, $row)->getOldCalculatedValue(). PHP_EOL;
                        }
                        
                    } catch (\PhpOffice\PhpSpreadsheet\Calculation\Exception $e) {
                        die($e->getMessage());
                    }
                    // $colValue = $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue(). PHP_EOL;
                    // $colValue = ((null !== $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue())?$worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue():'0');
                    $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, $colValue);
                }
            }
            else
            {
                continue;
            }
            
        }
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $newfilename . '.' . $ext);    

        // 記錄新增至資料表
        $data = array(
            "f_belong" => $f_belong,
            "f_year" => $f_year,
            "f_filename" => $newfilename . '.' . $ext,
            "f_ori_filename" => $oldfilename,
            "f_upload_date" => date('Y-m-d H:i:s'),
            "f_status" => $status,
            "emp_no" => $user
        );
        if($this->getsqlmod->addIntegrationLog($data))
            echo 'ok';
        else
            echo 'error';
        
    }
    // 上傳並處理 移送清冊
    function uploadESfiles()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");

        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');

        $f_year = $_POST['f_year'];
        $f_belong = $_POST['f_belong'];
        $newfilename = $_POST['f_belong'] . '_' . $_POST['f_year'];
        $oldfilename = $_FILES['upload_file']['name'];
        $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
        $dirpath = 'integradoc';

        // integradoc 目錄是否存在，不存在即建立
        if (!file_exists($dirpath)) {
            mkdir($dirpath, 0755, true);
        }
        // 先儲存原始excel檔
        $config['upload_path'] = $dirpath . '/'; 
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = '20000'; // max_size in kb
        $config['overwrite'] = true; // 覆蓋
        $this->load->library('upload',$config); 
        // File upload
        if ( ! $this->upload->do_upload('upload_file'))
        {
                $error = array('error' => $this->upload->display_errors());
                $status = json_encode($error);
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $status = 'done';
        }

        // 該年度是否處理，false則跳出
        if(!$this->getES_excel_rule()[$f_year]['use'])
        {
            echo 'ok';
            exit;
        }

        // 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }
        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        try {
            $reader->setReadDataOnly(TRUE);
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }
        
        $worksheet = ((null != $spreadsheet->getSheetByName($this->getES_excel_rule()[$f_year]['sheet']))?$spreadsheet->getSheetByName($this->getES_excel_rule()[$f_year]['sheet']):$spreadsheet->getActiveSheet());
        // Get the highest row and column numbers referenced in the worksheet
        $highestRow = $worksheet->getHighestRow(); // e.g. 10  

        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $useCols = explode(',', $this->getES_excel_rule()[$f_year]['usecols']);
        $startRow = (int)$this->getES_excel_rule()[$f_year]['skiprow'];

        // 合併原始檔的特定欄數到新檔
        for ($row = 1; $row <= $highestRow; ++$row) {
            $startRow++;
            // 移除沒有用的資料
            if (!empty($worksheet->getCellByColumnAndRow(4, $startRow)->getValue()) && !empty($worksheet->getCellByColumnAndRow(6, $startRow)->getValue()))
            {
                foreach ($useCols as $key => $value) {
                    $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($value);
                    $colValue = $worksheet->getCellByColumnAndRow($colIndex, $startRow)->getFormattedValue(). PHP_EOL;
                    // $colValue = ((null !== $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue())?$worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue():'0');
                    $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, $colValue);
                }
            }
            else
            {
                continue;
            }
            
        }
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $newfilename . '.' . $ext);    

        // 記錄新增至資料表
        $data = array(
            "f_belong" => $f_belong,
            "f_year" => $f_year,
            "f_filename" => $newfilename . '.' . $ext,
            "f_ori_filename" => $oldfilename,
            "f_upload_date" => date('Y-m-d H:i:s'),
            "f_status" => $status,
            "emp_no" => $user
        );
        if($this->getsqlmod->addIntegrationLog($data))
            echo 'ok';
        else
            echo 'error';
        
    }
    // 上傳並處理 憑證
    function uploadPCfiles()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");

        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');

        $f_year = $_POST['f_year'];
        $f_belong = $_POST['f_belong'];
        $newfilename = $_POST['f_belong'] . '_' . $_POST['f_year'];
        $oldfilename = $_FILES['upload_file']['name'];
        $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
        // $ext = 'xlsx';
        $dirpath = 'integradoc';

        // integradoc 目錄是否存在，不存在即建立
        if (!file_exists($dirpath)) {
            mkdir($dirpath, 0755, true);
        }
        // 先儲存原始excel檔
        $config['upload_path'] = $dirpath . '/'; 
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = '50000'; // max_size in kb
        $config['overwrite'] = true; // 覆蓋
        $this->load->library('upload',$config); 
        // File upload
        if ( ! $this->upload->do_upload('upload_file'))
        {
                $error = array('error' => $this->upload->display_errors());
                $status = $error;
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $status = 'done';
        }
       
        // 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }
        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        try {
            // 110 年讀取會又問題 要去 vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet 修改檔案 NamedFormula.php
            /**
             * // Validate data
            * if (!isset($formula)) {
            * throw new Exception('Tou must specify a Formula value for a Named Formula');
            * }
             */
            if($f_year == '109')
                $reader->setReadDataOnly(False);
            else
                $reader->setReadDataOnly(True);

            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
            // $spreadsheet = $reader->load($dirpath . '/債權憑證全-持續更新.xlsx');
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }
        
        $worksheet = $spreadsheet->getSheetByName($this->getPC_excel_rule()[$f_year]['sheet']);
        
        // Get the highest row and column numbers referenced in the worksheet
        $highestRow = $worksheet->getHighestRow(); // e.g. 10  

        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $useCols = explode(',', $this->getPC_excel_rule()[$f_year]['usecols']);
        $startRow = 1;
        // 匯出header
        $exp_file_header = array('憑證編號', '案件編號', "核發日期");

        // 合併原始檔的特定欄數到新檔
        for ($row = (1 + (int)$this->getPC_excel_rule()[$f_year]['skiprow']); $row <= $highestRow; ++$row) {
            
            // 移除沒有用的資料
            if (!empty($worksheet->getCellByColumnAndRow(9, $row)->getValue()))
            {
                
                foreach ($useCols as $key => $value) {
                    if($row == 1)
                    {
                        $newworksheet->setCellValueByColumnAndRow(($key + 1), $startRow, $exp_file_header[$key]);
                    }
                    else
                    {
                        $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($value);

                        try {
                            if(1 != $worksheet->getCellByColumnAndRow($colIndex, $row)->isFormula())
                            {
                                $colValue = $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue(). PHP_EOL;
                            }
                            else
                            {
                                $colValue = $worksheet->getCellByColumnAndRow($colIndex, $row)->getOldCalculatedValue(). PHP_EOL;
                            }
                            
                        } catch (\PhpOffice\PhpSpreadsheet\Calculation\Exception $e) {
                            die($e->getMessage());
                        }
                       
                        
                        $newworksheet->setCellValueByColumnAndRow(($key + 1), $startRow, $colValue);
                    }
                }
                $startRow++;
            }            
            else
            {
                continue;
            }
            
        }
        // $startRow = $startRow - 1;

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $newfilename . '.' . $ext);    

        // 記錄新增至資料表
        $data = array(
            "f_belong" => $f_belong,
            "f_year" => $f_year,
            "f_filename" => $newfilename . '.' . $ext,
            "f_ori_filename" => $oldfilename,
            "f_upload_date" => date('Y-m-d H:i:s'),
            "f_status" => $status,
            "emp_no" => $user
        );
        if($this->getsqlmod->addIntegrationLog($data))
            echo 'ok';
        else
            echo 'error';
        
    }
    // 上傳並處理 撤註銷
    function uploadCSfiles()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '-1');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");

        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');

        $f_year = $_POST['f_year'];
        $f_belong = $_POST['f_belong'];
        $newfilename = $_POST['f_belong'] . '_' . $_POST['f_year'];
        $oldfilename = $_FILES['upload_file']['name'];
        $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
        // $ext = 'xlsx';
        $dirpath = 'integradoc';

        // integradoc 目錄是否存在，不存在即建立
        if (!file_exists($dirpath)) {
            mkdir($dirpath, 0755, true);
        }
        // 先儲存原始excel檔
        $config['upload_path'] = $dirpath . '/'; 
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = '20000'; // max_size in kb
        $config['overwrite'] = true; // 覆蓋
        $this->load->library('upload',$config); 
        // File upload
        if ( ! $this->upload->do_upload('upload_file'))
        {
                $error = array('error' => $this->upload->display_errors());
                $status = $error;
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $status = 'done';
        }

        // 讀取上傳的excel
        switch ($ext) {
            case 'xlsx':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
            case 'xls':
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                break;
            default:
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                break;
        }
        // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        try {
            $reader->setReadDataOnly(TRUE);
            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
            // $spreadsheet = $reader->load($dirpath . '/99to109撤銷清查.xlsx');
        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }

        $rules = $this->getCS_excel_rule()[$f_year]['sheet'];
        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet();         
        $startRow = 1;

        // 匯出header
        $exp_file_header = array('性質', '案件編號', "撤銷或註銷金額", "撤銷註銷");

        foreach ($rules as $ruleKey => $ruleValue) {
            $worksheet = $spreadsheet->getSheetByName($ruleValue);
            // Get the highest row and column numbers referenced in the worksheet
            $highestRow = $worksheet->getHighestRow(); // e.g. 10  
            $useCols = explode(',', $this->getCS_excel_rule()[$f_year]['usecols']);
            

            // 合併原始檔的特定欄數到新檔
            for ($row = 1; $row <= $highestRow; ++$row) {
                // 移除沒有用的資料
                if (!empty($worksheet->getCellByColumnAndRow(3, $row)->getValue()) && !empty($worksheet->getCellByColumnAndRow(4, $row)->getValue()))
                {
                    foreach ($useCols as $key => $value) {
                        if($row == 1)
                        {
                            $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, $exp_file_header[$key]);
                        }
                        else
                        {
                            $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($value);

                            $colValue = $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue();

                            $newworksheet->setCellValueByColumnAndRow(($key + 1), $startRow, $colValue);
                        }
                    }
                    if(substr($worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue().PHP_EOL, 0, 1) != 'A')
                    {
                        $startRow++;
                    }
                }
                
            }
            // $startRow = $startRow - 1;
        }
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $newfilename . '.' . $ext);    

        // 記錄新增至資料表
        $data = array(
            "f_belong" => $f_belong,
            "f_year" => $f_year,
            "f_filename" => $newfilename . '.' . $ext,
            "f_ori_filename" => $oldfilename,
            "f_upload_date" => date('Y-m-d H:i:s'),
            "f_status" => $status,
            "emp_no" => $user
        );
        if($this->getsqlmod->addIntegrationLog($data))
            echo 'ok';
        else
            echo 'error';
        
    }
    function process()
    {
        $data['FH_data'] = json_decode(json_encode($this->getsqlmod->getIntegrationFHData()->result()), true);
        $data['ES_data'] = json_decode(json_encode($this->getsqlmod->getIntegrationESData()->result()), true);
        $data['title'] = "檔案處理";
        $data['user'] = $this -> session -> userdata('uic');
        $data['include'] = 'integration/process';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    // 整合合併裁罰
    function integratFH()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);        
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        // $filename = $this->filter_by_value($data, 'f_belong', 'A');
        $post_years = json_decode($_POST['years'], true);
        $inte_files_url = array();
        $exp_file_header = array('所屬年度', '案件編號', "發文年","發文月","發文日","查獲年","查獲月","查獲日","繳納期限年","繳納期限月","繳納期限日","罰鍰（萬元）","講習時數","發文字號"	,"受處分人姓名","身分證編號","繳納期限");
        $exp_filename = "Psh99to109.xlsx";
        $exp_base_filename = "Psh99to109_base.xlsx";
        // foreach ($filename as $key => $value) {
        //     array_push($inte_files_url, $dirpath . $filename[$key]['f_filename']);
        // }

        foreach ($post_years as $key => $value) {
            array_push($inte_files_url, $dirpath . $value);
        }
        // var_dump($inte_files_url);
        // exit;
        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        // 存案件編號,身分證,處分人姓名
        $basesheet = new Spreadsheet(); 
        $baseworksheet = $basesheet->getActiveSheet(); 
        $startRow = 1;
		$obj_count = 1;        

        foreach ($inte_files_url as $value) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($value);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            $worksheet = $spreadsheet->getActiveSheet();
            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn(); 
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 
            $base_col =  1;
			if($obj_count == 1)
			{
				$readrow = 1;
			}
			else
			{
				$readrow = 2;
			}
            for ($row = $readrow; $row <= $highestRow; ++$row) {

                // 移除沒有用的資料
                if (!empty($worksheet->getCellByColumnAndRow(2, $row)->getValue()) && null != $worksheet->getCellByColumnAndRow(2, $row)->getValue()  && trim($worksheet->getCellByColumnAndRow(15, $row)->getValue()) != '#N/A' && trim($worksheet->getCellByColumnAndRow(15, $row)->getValue()) != '0'  && null != $worksheet->getCellByColumnAndRow(15, $row)->getValue())
                {
                    for ($col = 1; $col <= $highestColumnIndex; ++$col) {
                        if($row == 1)
                        {
                            $newworksheet->setCellValueByColumnAndRow($col, $row, $exp_file_header[$col-1]);
                        }
                        else
                        {
                            // 儲存格值為null，設為0
                            $colValue = ((null !== $worksheet->getCellByColumnAndRow($col, $row)->getFormattedValue())?$worksheet->getCellByColumnAndRow($col, $row)->getFormattedValue():'0');//. PHP_EOL;
                                                
                            $newworksheet->setCellValueByColumnAndRow($col, $startRow, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));

							$newworksheet->setCellValueByColumnAndRow(17, $startRow, trim($worksheet->getCellByColumnAndRow(9, $row)->getValue()).trim($worksheet->getCellByColumnAndRow(10, $row)->getValue()).trim($worksheet->getCellByColumnAndRow(11, $row)->getValue()));
    
                            if($col == 2 || $col == 15 || $col == 16)
                            {
                                $baseworksheet->setCellValueByColumnAndRow($base_col++, $startRow, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));
                            }
                            
                        }
                    }
                    $startRow++;
                    $base_col = 1;
                }
                
                
            }
            // $startRow = $startRow - 1;
			$obj_count++;
        }
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);    

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($basesheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_base_filename);    
        echo $exp_filename;

    }
    // 整合合併移送
    function integratES()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        // $filename = $this->filter_by_value($data, 'f_belong', 'D');
        $post_years = json_decode($_POST['years'], true);
        $inte_files_url = array();
        $exp_file_header = array("移送案號","移送文號","移送年","移送月","移送日","案件編號","義務人","分署","身分證編號","移送日期");
        $read_inte_file_cols = array('G','E','A','B','C','D','F','H',null,null);
        $exp_filename = "TRS99to108.xlsx";
        // $exp_djfilename = "TRSdj99to108.xlsx";
        // foreach ($filename as $key => $value) {
        //     array_push($inte_files_url, $dirpath . $filename[$key]['f_filename']);
        // }
        foreach ($post_years as $key => $value) {
            array_push($inte_files_url, $dirpath . $value);
        }
        // var_dump($inte_files_url);
        // exit;
        // 讀取Psh99to109.xlsx
        $readerpsh = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		// $readerpshDJ = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        try {
            $readerpsh->setReadDataOnly(TRUE);
            $spreadsheetpsh = $readerpsh->load($dirpath . 'Psh99to109_base.xlsx');
            $worksheetpsh = $spreadsheetpsh->getActiveSheet();
            $rowspsh = $worksheetpsh->toArray();

			// $spreadsheetpshDJ = $readerpshDJ->load($dirpath . 'DJ/PshDJ99to109_base.xlsx');
            // $worksheetpshDJ = $spreadsheetpshDJ->getActiveSheet();
            // $rowspshDJ = $worksheetpshDJ->toArray();

        } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
            die($e->getMessage());
        }
        
        
        // 新開一個空的excel檔做寫入(罰鍰)
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $startRow = 1;

        // 新開一個空的excel檔做寫入(怠金)
        // $newsheetdj = new Spreadsheet(); 
        // $newworkdjsheet = $newsheetdj->getActiveSheet(); 
        // $startdjRow = 2;
        

        foreach ($inte_files_url as $value) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($value);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            $worksheet = $spreadsheet->getActiveSheet();
            $highestRow = $worksheet->getHighestRow();
            $highestColumnIndex = count($exp_file_header);
            // $highestColumn = $worksheet->getHighestColumn(); 
            // $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 
            

            for ($row = 1; $row <= $highestRow; ++$row) {
                // 移除沒有用的資料
                if (null != $worksheet->getCellByColumnAndRow(4, $row)->getValue() && null != $worksheet->getCellByColumnAndRow(6, $row)->getValue() && !empty($worksheet->getCellByColumnAndRow(4, $row)->getValue()) && !empty($worksheet->getCellByColumnAndRow(6, $row)->getValue()) && trim($worksheet->getCellByColumnAndRow(6, $row)->getValue()) != '#REF!')
                {
                    for ($col = 1; $col <= $highestColumnIndex; ++$col) {
                        if($row == 1)
                        {
                            // if(substr($worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue().PHP_EOL, 0, 1) != 'A')
                            // {
                                $newworksheet->setCellValueByColumnAndRow($col, 1, $exp_file_header[$col-1]);
                            // }
                            // else
                            // {
                                // $newworkdjsheet->setCellValueByColumnAndRow($col, 1, $exp_file_header[$col-1]);
                            // }
                        }
                        else
                        {
                            // row >= 2
                             if(null != $read_inte_file_cols[$col-1])
                                {
                                    $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($read_inte_file_cols[$col-1]);
                                    $colValue = $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue(). PHP_EOL;

                                    if(substr($worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue().PHP_EOL, 0, 1) != 'A')
                                    {
                                        $newworksheet->setCellValueByColumnAndRow($col, $startRow, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));
                                    }
                                    else
                                    {
                                        // $newworkdjsheet->setCellValueByColumnAndRow($col, $startdjRow, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));
                                    }
                                    
                                }
                                else
                                {
                                    // 移送日期
                                    if(($col-1) == 9)
                                    {
                                        // 合併「移送日期」
                                        $year = $worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue(). PHP_EOL;
                                        $month = $worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue(). PHP_EOL;
                                        $day = $worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue(). PHP_EOL;
    
                                        $month2 = ((strlen(trim($month)) == 2)?$month:('0'. (string)trim($month)));
                                        $day2 = ((strlen(trim($day)) == 2)?$day:('0'. (string)trim($day)));
                                        $ESdate = str_replace(PHP_EOL, '',$year) . str_replace(PHP_EOL, '',$month2) . str_replace(PHP_EOL, '',$day2);
    
                                        if(substr($worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue().PHP_EOL, 0, 1) != 'A')
                                        {
                                            $newworksheet->setCellValueByColumnAndRow($col, $startRow, $ESdate);
                                        }
                                        else
                                        {
                                            // $newworkdjsheet->setCellValueByColumnAndRow($col, $startdjRow, $ESdate);
                                        }
                                        
                                    }
                                    // 身分證字號
                                    if(($col-1) == 8)
                                    {
                                        if(substr($worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue().PHP_EOL, 0, 1) != 'A')
                                        {
											$searchRowIndex = array_search(trim($worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue().PHP_EOL), array_column($rowspsh, 0));
                                        
                                        	$userid = $rowspsh[(($searchRowIndex)?$searchRowIndex:0)][2];
                                            $newworksheet->setCellValueByColumnAndRow($col, $startRow, $userid);
                                        }
                                        else
                                        {
											// $searchRowIndex = array_search(trim($worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue().PHP_EOL), array_column($rowspshDJ, 0));
                                        
                                        	// $userid = $rowspshDJ[(($searchRowIndex)?$searchRowIndex:0)][2];
											
                                            // $newworkdjsheet->setCellValueByColumnAndRow($col, $startdjRow, $userid);
                                        }
                                    }
                                }
                            
                        }
                    }
                    if($row != 1)
                    {
                        if(substr($worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue().PHP_EOL, 0, 1) != 'A')
                        {
                            $startRow++;                        
                        }
                        else
                        {
                            // $startdjRow++;
                        }
                    }
                    
                }
            }
            if(substr($worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue().PHP_EOL, 0, 1) != 'A')
            {
                // $startRow = $startRow - 1;
            }
            else
            {
                // $startdjRow = $startdjRow -1;
            }
            
            
        }
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);    

        // $writerdj = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheetdj);
        // $writerdj->setPreCalculateFormulas(false);
        // $writerdj->save($dirpath . '/' . $exp_djfilename);    

        echo $exp_filename;
    }

    // 讀取所有罰鍰案件
    function readFH()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        $readfilename = 'Psh99to109.xlsx';
        $exp_file_header = array('所屬年度', '案件編號', "受處分人姓名","身分證編號","罰鍰（萬元）","繳費期限");
        $exp_filename = "step1-AllFH.xlsx";
        // var_dump($inte_files_url);
        // exit;
        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        
        
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($dirpath . $readfilename);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            $worksheet = $spreadsheet->getActiveSheet();
            $highestRow = $worksheet->getHighestRow();
            $useCols = explode(',', $this->getAllFH_excel_rule()['usecols']); 

            for ($row = 1; $row <= $highestRow; ++$row) {

                foreach ($useCols as $key => $value){
                    if($row == 1)
                    {
                        $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, $exp_file_header[$key]);
                    }
                    else
                    {
                        $colIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($value);

                        // 儲存格值為null，設為0
                        $colValue = ((null !== $worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue())?$worksheet->getCellByColumnAndRow($colIndex, $row)->getFormattedValue(). PHP_EOL:'0');//. PHP_EOL;
                                            
                        $newworksheet->setCellValueByColumnAndRow(($key + 1), $row, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));

                    }
                }
            }
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);   
        echo $exp_filename;

    }
    // 讀取所有完納案件
    function readCW_done()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        $readfilename = array('B_108_完納.xlsx', 'B_109_完納.xlsx', 'B_110_完納.xlsx', 'B_111_完納.xlsx');
        $exp_filename = "罰鍰完納資料.xlsx";
        // var_dump($inte_files_url);
        // exit;
        // 先整合成一個檔 CwDone99to109.xlsx        
        $intePC_filename = 'CwDone99to109.xlsx';
        $intenewsheet = new Spreadsheet(); 
        $inteworksheet = $intenewsheet->getActiveSheet(); 
        $startRow = 1;
		$obj_count = 1;
        foreach ($readfilename as  $filevalue) {
			if (file_exists($dirpath . $filevalue)) 
			{
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				try {
					$reader->setReadDataOnly(TRUE);
					$readspreadsheet = $reader->load($dirpath . $filevalue);
				} catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
					die($e->getMessage());
					break;
				}
				$readworksheet = $readspreadsheet->getActiveSheet();
				$highestRow = $readworksheet->getHighestRow();
				$highestColumn = $readworksheet->getHighestColumn(); 
				$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 
				if($obj_count == 1)
				{
					$readrow = 1;
				}
				else
				{
					$readrow = 2;
				}
				for ($row = $readrow; $row <= $highestRow; ++$row) {
					// 移除沒有用的資料
					if (!empty($readworksheet->getCellByColumnAndRow(1, $row)->getValue()))
					{
						for ($col = 1; $col <= $highestColumnIndex; ++$col) {
							// 儲存格值為null，設為0
							$colValue = ((null !== $readworksheet->getCellByColumnAndRow($col, $row)->getFormattedValue())?$readworksheet->getCellByColumnAndRow($col, $row)->getFormattedValue():'');//. PHP_EOL;
														
							$inteworksheet->setCellValueByColumnAndRow($col, $startRow, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));
						}
						$startRow++;
					}
				}
				// $startRow = $startRow - 1;
				$obj_count++;
			}
            
        }
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($intenewsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $intePC_filename); 
        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $startRow = 1;
        
        // foreach ($readfilename as $filekey => $filevalue) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($dirpath . '/' . $intePC_filename);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            $worksheet = $spreadsheet->getActiveSheet();
            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn(); 
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 

            for ($row = 1; $row <= $highestRow; ++$row) {

                for ($col = 1; $col <= $highestColumnIndex; ++$col) {
                    if($startRow == 1)
                    {
                        $newworksheet->setCellValueByColumnAndRow($col, $startRow, $worksheet->getCellByColumnAndRow($col, $row)->getFormattedValue(). PHP_EOL);
                    }
                    else
                    {
                        if($row >= 2)
                        {
                            // 儲存格值為null，設為0
                            $colValue = ((null !== $worksheet->getCellByColumnAndRow($col, $row)->getFormattedValue())?$worksheet->getCellByColumnAndRow($col, $row)->getFormattedValue(). PHP_EOL:'0');//. PHP_EOL;
                                                                                                
                            $newworksheet->setCellValueByColumnAndRow($col, $startRow, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));
                        }
                        
                    }
                    
                }
                if(substr($worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue().PHP_EOL, 0, 1) != 'A')
                {
                    if(!empty($worksheet->getCellByColumnAndRow(1, $row)->getValue()))
                    {
                        $startRow++;
                    }
                    
                }
                
            }
            // $startRow = $startRow - 1;
        // }
            
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);   
        echo $exp_filename;

    }
    // 讀取所有分期完納LOG
    function readCW_partdone()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        $readfilename = array('B_109_分期完納.xlsx', 'B_110_分期完納.xlsx', 'B_111_分期完納.xlsx');
        $exp_file_header = array('案件編號','完納日期', '分期完LOG');
        $exp_filename = "分期完納.xlsx";
        // var_dump($inte_files_url);
        // exit;
        // 先整合成一個檔 CwPartDone99to109.xlsx        
        $intePC_filename = 'CwPartDone99to109.xlsx';
        $intenewsheet = new Spreadsheet(); 
        $inteworksheet = $intenewsheet->getActiveSheet(); 
        $startRow = 1;
		$obj_count = 1;
        foreach ($readfilename as  $filevalue) {
			if (file_exists($dirpath . $filevalue)) 
			{
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				try {
					$reader->setReadDataOnly(TRUE);
					$readspreadsheet = $reader->load($dirpath . $filevalue);
				} catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
					die($e->getMessage());
					break;
				}
				$readworksheet = $readspreadsheet->getActiveSheet();
				$highestRow = $readworksheet->getHighestRow();
				$highestColumn = $readworksheet->getHighestColumn(); 
				$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 
				if($obj_count == 1)
				{
					$readrow = 1;
				}
				else
				{
					$readrow = 2;
				}
				for ($row = $readrow; $row <= $highestRow; ++$row) {
					// 移除沒有用的資料
					if (!empty($readworksheet->getCellByColumnAndRow(1, $row)->getValue()))
					{
						for ($col = 1; $col <= $highestColumnIndex; ++$col) {
							// 儲存格值為null，設為0
							$colValue = ((null !== $readworksheet->getCellByColumnAndRow($col, $row)->getFormattedValue())?$readworksheet->getCellByColumnAndRow($col, $row)->getFormattedValue():'');//. PHP_EOL;
														
							$inteworksheet->setCellValueByColumnAndRow($col, $startRow, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));
						}
						$startRow++;
					}
				}
				// $startRow = $startRow - 1;
				$obj_count++;
			}
            
        }
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($intenewsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $intePC_filename); 
        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $startRow = 2;
        $str = "";

        $newworksheet->setCellValueByColumnAndRow(1, 1, $exp_file_header[0]);
        $newworksheet->setCellValueByColumnAndRow(2, 1, $exp_file_header[1]);
        $newworksheet->setCellValueByColumnAndRow(3, 1, $exp_file_header[2]);
        
        // foreach ($readfilename as $filekey => $filevalue) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($dirpath . $intePC_filename);
                $worksheet = $spreadsheet->getActiveSheet();
                $rowspsh = $worksheet->toArray();
                $caseid = array_unique(array_column($rowspsh,0));
                // asort($caseid);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            // var_dump(array_count_values(array_column($rowspsh,0)));
            // exit;

            $temp_row = 1;
            foreach($caseid as $caseKey=>$caseValue)
            {
                if($temp_row != 1)
                {
                    $newworksheet->setCellValueByColumnAndRow(1, $startRow, $caseValue);
                    $newworksheet->getStyle('A'.$startRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $ary_log = array();
                    foreach($rowspsh as $ak=>$av)
                    {
                        if($ak != 0)
                        {
                            if($av[0] == $caseValue)
                            {
                                $str .=$rowspsh[$ak][1] . '_' . $rowspsh[$ak][2] . "\n";
                                array_push($ary_log, $rowspsh[$ak][1]);
                            }
                            
                        }
                        
                    }
                    
                    $newworksheet->setCellValueByColumnAndRow(2, $startRow, max($ary_log));
                    $newworksheet->getStyle('B'.$startRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $newworksheet->setCellValueByColumnAndRow(3, $startRow, rtrim($str, " \n"));
                    $newworksheet->getStyle('C'.$startRow)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $str = "";
                    $startRow++;
                }
                $temp_row++;
            }
            
            // $startRow = $startRow - 1;
        // }
            
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);   
        echo $exp_filename;

    }
    // 讀取讀取所有分期資料
    function readCW_part()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        $readfilename = array('B_111_分期.xlsx');
        $exp_file_header = array('案件編號','繳納日期','本期核銷金額', '分期LOG');
        $exp_filename = "罰鍰分期資料.xlsx";
        // var_dump($inte_files_url);
        // exit;
		// 先整合成一個檔 CwPartDone99to109.xlsx        
        $intePC_filename = 'CwPart99to109.xlsx';
        $intenewsheet = new Spreadsheet(); 
        $inteworksheet = $intenewsheet->getActiveSheet(); 
        $startRow = 1;
		$obj_count = 1;
        foreach ($readfilename as  $filevalue) {
			if (file_exists($dirpath . $filevalue)) 
			{
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				try {
					$reader->setReadDataOnly(TRUE);
					$readspreadsheet = $reader->load($dirpath . $filevalue);
				} catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
					die($e->getMessage());
					break;
				}
				$readworksheet = $readspreadsheet->getActiveSheet();
				$highestRow = $readworksheet->getHighestRow();
				$highestColumn = $readworksheet->getHighestColumn(); 
				$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 
				if($obj_count == 1)
				{
					$readrow = 1;
				}
				else
				{
					$readrow = 2;
				}
				for ($row = $readrow; $row <= $highestRow; ++$row) {
					// 移除沒有用的資料
					if (!empty($readworksheet->getCellByColumnAndRow(1, $row)->getValue()))
					{
						for ($col = 1; $col <= $highestColumnIndex; ++$col) {
							// 儲存格值為null，設為0
							$colValue = ((null !== $readworksheet->getCellByColumnAndRow($col, $row)->getFormattedValue())?$readworksheet->getCellByColumnAndRow($col, $row)->getFormattedValue():'');//. PHP_EOL;
														
							$inteworksheet->setCellValueByColumnAndRow($col, $startRow, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));
						}
						$startRow++;
					}
				}
				// $startRow = $startRow - 1;
				$obj_count++;
			}
            
        }
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($intenewsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $intePC_filename); 


        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $startRow = 2;
        $str = "";

        $newworksheet->setCellValueByColumnAndRow(1, 1, $exp_file_header[0]);
        $newworksheet->setCellValueByColumnAndRow(2, 1, $exp_file_header[1]);
        $newworksheet->setCellValueByColumnAndRow(3, 1, $exp_file_header[2]);
        $newworksheet->setCellValueByColumnAndRow(4, 1, $exp_file_header[3]);
        
        // foreach ($readfilename as $filekey => $filevalue) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($dirpath . $intePC_filename);
                $worksheet = $spreadsheet->getActiveSheet();
                $rowspsh = $worksheet->toArray();
                $caseid = array_unique(array_column($rowspsh,0));
                // asort($caseid);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
				
            }
            // var_dump(array_count_values(array_column($rowspsh,0)));
            // exit;

            $temp_row = 1;
            foreach($caseid as $caseKey=>$caseValue)
            {
                if($temp_row != 1)
                {
                    $newworksheet->setCellValueByColumnAndRow(1, $startRow, $caseValue);
                    $newworksheet->getStyle('A'.$startRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $sumOfLog = 0;
                    $ary_log = array();

                    foreach($rowspsh as $ak=>$av)
                    {
                        if($av[0] == $caseValue)
                        {
                            $str .=$rowspsh[$ak][1] . '_' . $rowspsh[$ak][2] . "\n";

                            $sumOfLog = $sumOfLog + (int)$rowspsh[$ak][2];
                            array_push($ary_log, $rowspsh[$ak][1]);
                        }
                        
                    }
                    $newworksheet->setCellValueByColumnAndRow(2, $startRow, max($ary_log));
                    $newworksheet->getStyle('B'.$startRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $newworksheet->setCellValueByColumnAndRow(3, $startRow, $sumOfLog);
                    $newworksheet->getStyle('C'.$startRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $newworksheet->setCellValueByColumnAndRow(4, $startRow, rtrim($str, " \n"));
                    $newworksheet->getStyle('D'.$startRow)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $str = "";
                    $startRow++;
                }
                $temp_row++;
            }
            
            // $startRow = $startRow - 1;
        // }
            
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);   
        echo $exp_filename;

    }

    // 讀取所有移送案號
    function readES()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        $readfilename = array('TRS99to108.xlsx');
        $exp_file_header = array('案件編號','移送LOG');
        $exp_filename = "罰鍰移送資料.xlsx";
        // var_dump($inte_files_url);
        // exit;
        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $startRow = 2;
        $str = "";

        $newworksheet->setCellValueByColumnAndRow(1, 1, $exp_file_header[0]);
        $newworksheet->setCellValueByColumnAndRow(2, 1, $exp_file_header[1]);
        
        foreach ($readfilename as $filekey => $filevalue) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($dirpath . $filevalue);
                $worksheet = $spreadsheet->getActiveSheet();
                $rowspsh = $worksheet->toArray();
                $caseid = array_unique(array_column($rowspsh,5));
                // asort($caseid);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            // var_dump(array_count_values(array_column($rowspsh,0)));
            // exit;
            // $useCols = explode(',', $this->getAllES_excel_rule()['usecols']);
            $temp_row = 1;
            foreach($caseid as $caseKey=>$caseValue)
            {
                if($temp_row != 1)
                {
                    $newworksheet->setCellValueByColumnAndRow(1, $startRow, $caseValue);
                    $newworksheet->getStyle('A'.$startRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    foreach($rowspsh as $ak=>$av)
                    {
                        if($av[5] == $caseValue)
                        {
                            $str .=$rowspsh[$ak][9] . '_' . $rowspsh[$ak][0]. '_' . $rowspsh[$ak][7] . "\n";

                        }
                        
                    }
                    $str = implode("\n",array_unique(explode("\n", $str)));
                    $newworksheet->setCellValueByColumnAndRow(2, $startRow, rtrim($str, " \n"));
                    $newworksheet->getStyle('B'.$startRow)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $str = "";
                    $startRow++;
                }
                $temp_row++;
            }
            
            // $startRow = $startRow - 1;
        }
            
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);   
        echo $exp_filename;

    }

    // 讀取憑證資料
    function readPC()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        $readfilename = array('E_109.xlsx', 'E_110.xlsx', 'E_111.xlsx');
        $exp_file_header = array('案件編號','憑證LOG');
        $exp_filename = "罰鍰憑證資料 .xlsx";
        // var_dump($inte_files_url);
        // exit;

        // 先整合成一個檔 Pc99to109.xlsx        
        $intePC_filename = 'Pc99to109.xlsx';
        $intenewsheet = new Spreadsheet(); 
        $inteworksheet = $intenewsheet->getActiveSheet(); 
        $startRow = 1;
		$obj_count = 1;
        foreach ($readfilename as  $filevalue) {
			if (file_exists($dirpath . $filevalue)) 
			{
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				try {
					$reader->setReadDataOnly(TRUE);
					$readspreadsheet = $reader->load($dirpath . $filevalue);
				} catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
					die($e->getMessage());
				}
				$readworksheet = $readspreadsheet->getActiveSheet();
				$highestRow = $readworksheet->getHighestRow();
				$highestColumn = $readworksheet->getHighestColumn(); 
				$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 
				if($obj_count == 1)
				{
					$readrow = 1;
				}
				else
				{
					$readrow = 2;
				}
				for ($row = $readrow; $row <= $highestRow; ++$row) {
					// 移除沒有用的資料
					if (!empty($readworksheet->getCellByColumnAndRow(2, $row)->getValue()))
					{
						for ($col = 1; $col <= $highestColumnIndex; ++$col) {
							// 儲存格值為null，設為0
							$colValue = ((null !== $readworksheet->getCellByColumnAndRow($col, $row)->getFormattedValue())?$readworksheet->getCellByColumnAndRow($col, $row)->getFormattedValue():'');//. PHP_EOL;
														
							$inteworksheet->setCellValueByColumnAndRow($col, $startRow, str_replace(array('\"','\r\n','\n'),array('','',''),trim($colValue)));
						}
						$startRow++;
					}
				}
				// $startRow = $startRow - 1;
				$obj_count++;
			}
            
        }
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($intenewsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $intePC_filename); 


        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $startRow = 2;
        $str = "";

        $newworksheet->setCellValueByColumnAndRow(1, 1, $exp_file_header[0]);
        $newworksheet->setCellValueByColumnAndRow(2, 1, $exp_file_header[1]);
        
        // foreach ($readfilename as $filekey => $filevalue) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($dirpath . '/' . $intePC_filename);
                $worksheet = $spreadsheet->getActiveSheet();
                $rowspsh = $worksheet->toArray();
                $caseid = array_unique(array_column($rowspsh,1));
                // asort($caseid);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            // var_dump(array_count_values(array_column($rowspsh,0)));
            // exit;
            // $useCols = explode(',', $this->getAllES_excel_rule()['usecols']);
            $temp_row = 1;
            foreach($caseid as $caseKey=>$caseValue)
            {
                if($temp_row != 1)
                {
                    $newworksheet->setCellValueByColumnAndRow(1, $startRow, $caseValue);
                    $newworksheet->getStyle('A'.$startRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    foreach($rowspsh as $ak=>$av)
                    {
                        if($av[1] == $caseValue)
                        {
                            $str .=(rtrim($rowspsh[$ak][2], " \n") . '_憑證_' . rtrim($rowspsh[$ak][0], " \n") . "\n");

                        }
                        
                    }
                    $newworksheet->setCellValueByColumnAndRow(2, $startRow, rtrim($str, " \n"));
                    $newworksheet->getStyle('B'.$startRow)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $str = "";
                    $startRow++;
                }
                $temp_row++;
            }
            
            // $startRow = $startRow - 1;
        // }
            
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);   
        echo $exp_filename;

    }

    // 讀取撤銷及註銷資料
    function readCS()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        $readfilename = array('F_110.xlsx');
        $exp_file_header = array('案件編號','撤銷註銷LOG');
        $exp_filename = "所有撤銷註銷 .xlsx";
        // var_dump($inte_files_url);
        // exit;
        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $startRow = 2;
        $str = "";

        $newworksheet->setCellValueByColumnAndRow(1, 1, $exp_file_header[0]);
        $newworksheet->setCellValueByColumnAndRow(2, 1, $exp_file_header[1]);
        
        foreach ($readfilename as $filekey => $filevalue) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($dirpath . $filevalue);
                $worksheet = $spreadsheet->getActiveSheet();
                $rowspsh = $worksheet->toArray();
                $caseid = array_unique(array_column($rowspsh,1));
                // asort($caseid);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            // var_dump(array_count_values(array_column($rowspsh,0)));
            // exit;
            
            $temp_row = 1;
            foreach($caseid as $caseKey=>$caseValue)
            {
                if($temp_row != 1)
                {
                    $newworksheet->setCellValueByColumnAndRow(1, $startRow, $caseValue);
                    $newworksheet->getStyle('A'.$startRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    foreach($rowspsh as $ak=>$av)
                    {
                        if($av[1] == $caseValue)
                        {
                            $str .=rtrim($rowspsh[$ak][3], " \n") . '_' . rtrim($rowspsh[$ak][2], " \n") . "\n";

                        }
                        
                    }
                    $newworksheet->setCellValueByColumnAndRow(2, $startRow, rtrim($str, " \n"));
                    $newworksheet->getStyle('B'.$startRow)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $str = "";
                    $startRow++;
                }
                $temp_row++;
            }
            
            // $startRow = $startRow - 1;
        }
            
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);   
        echo $exp_filename;

    }

    // 讀取執行命令資料
    function readML()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $data = json_decode(json_encode($this->getsqlmod->getIntegrationData()->result()), true);
        $readfilename = array('C_110.xlsx');
        $exp_file_header = array('案件編號','執行命令LOG');
        $exp_filename = "所有執行命令 .xlsx";
        // var_dump($inte_files_url);
        // exit;
        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $startRow = 2;
        $str = "";

        $newworksheet->setCellValueByColumnAndRow(1, 1, $exp_file_header[0]);
        $newworksheet->setCellValueByColumnAndRow(2, 1, $exp_file_header[1]);
        
        foreach ($readfilename as $filekey => $filevalue) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($dirpath . $filevalue);
                $worksheet = $spreadsheet->getActiveSheet();
                $rowspsh = $worksheet->toArray();
                $caseid = array_unique(array_column($rowspsh,2));
                // asort($caseid);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            // var_dump(array_count_values(array_column($rowspsh,0)));
            // exit;
            
            $temp_row = 1;
            foreach($caseid as $caseKey=>$caseValue)
            {
                if($temp_row != 1)
                {
                    $newworksheet->setCellValueByColumnAndRow(1, $startRow, $caseValue);
                    $newworksheet->getStyle('A'.$startRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    foreach($rowspsh as $ak=>$av)
                    {
                        if($av[2] == $caseValue)
                        {
                            $str .=rtrim($rowspsh[$ak][0], " \n") . '_' . rtrim($rowspsh[$ak][3], " \n") . '_' . rtrim($rowspsh[$ak][1], " \n") . "\n";

                        }
                        
                    }
                    $newworksheet->setCellValueByColumnAndRow(2, $startRow, rtrim($str, " \n"));
                    $newworksheet->getStyle('B'.$startRow)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

                    $str = "";
                    $startRow++;
                }
                $temp_row++;
            }
            
            // $startRow = $startRow - 1;
        }
            
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);   
        echo $exp_filename;

    }

    // 產生 99to109整合年度總表_罰鍰
    function exportFH()
    {
        set_time_limit(0);        
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 259200);
        ini_set('max_execution_time', 259200);         
        date_default_timezone_set("Asia/Taipei");
        
        $dirpath = 'integradoc/';
        $readfilename = array('step1-AllFH.xlsx','罰鍰完納資料.xlsx','分期完納.xlsx','罰鍰分期資料.xlsx','所有執行命令 .xlsx','罰鍰移送資料.xlsx','罰鍰憑證資料 .xlsx','所有撤銷註銷 .xlsx');
        $exp_file_header = array('所屬年度','案件編號',	'受處分人姓名','身分證編號','罰鍰（萬元）',"完納日期"	,"完納金額","分期完LOG",'繳納日期','本期核銷金額','分期LOG','執行命令LOG','移送LOG','憑證LOG','撤銷註銷LOG','繳納期限','創建日期');
        $exp_file_header_en = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q');
        $exp_filename = "99to109整合年度總表_罰鍰".date('Ymd').".xlsx";
        // var_dump($inte_files_url);
        // exit;
        
        // 新開一個空的excel檔做寫入
        $newsheet = new Spreadsheet(); 
        $newworksheet = $newsheet->getActiveSheet(); 
        $startRow = 2;
        $str = "";

        foreach ($exp_file_header as $headerkey => $headervalue) {
            $newworksheet->setCellValueByColumnAndRow(($headerkey + 1), 1, $headervalue);
            $newworksheet->getColumnDimension($exp_file_header_en[$headerkey])->setWidth(20);
        }
        
        foreach ($readfilename as $filekey => $filevalue) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            try {
                $reader->setReadDataOnly(TRUE);
                $spreadsheet = $reader->load($dirpath . $filevalue);
                $worksheet = $spreadsheet->getActiveSheet();
                $rowspsh = $worksheet->toArray();
                // asort($caseid);
            } catch (\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
                die($e->getMessage());
            }
            // var_dump(array_count_values(array_column($rowspsh,0)));
            // exit;
            if($filekey == 0)
            {
                $caseid = array_unique(array_column($rowspsh,1));

                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn(); 
                $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 
                for ($a=2; $a <= $highestRow ; ++$a) { 
                    for ($aCol=1; $aCol <= $highestColumnIndex ; ++$aCol) { 
						if($aCol >= 6)
                        {
                            $newworksheet->setCellValueByColumnAndRow(15, $a, rtrim($worksheet->getCellByColumnAndRow($aCol, $a)->getFormattedValue()));

							$newworksheet->setCellValueByColumnAndRow(16, $a, date('Y-m-d H:i:s'));
                        }
						else
						{
							$newworksheet->setCellValueByColumnAndRow($aCol, $a, rtrim($worksheet->getCellByColumnAndRow($aCol, $a)->getFormattedValue()));
							if($aCol == 2)
							{
								// background color: pink
								$newworksheet->getStyle('B'.$a)->getFill()
								->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
								->getStartColor()->setARGB('FFF2DCDB');
							}
							if($aCol == 5)
							{
								// background color: yellow
								$newworksheet->getStyle('E'.$a)->getFill()
								->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
								->getStartColor()->setARGB('FFFFFFCC');
							}
						}

                        
                    }
                }
            }
            else
            {
                $temp_row = 1;
                foreach($caseid as $caseKey=>$caseValue)
                {
                    switch ($filekey) {
                        // 完納
                        case 1:
                            $datarow = 1;
                            foreach($rowspsh as $ak=>$av)
                            {
                                if($datarow != 1)
                                {
                                    if(trim((string)$av[0]) == trim((string)$caseValue))
                                    {
                                        $newworksheet->setCellValueByColumnAndRow(6, $temp_row, rtrim($rowspsh[$ak][1], " \n"));
                                        $newworksheet->getStyle('F'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    
                                        
                                        $newworksheet->setCellValueByColumnAndRow(7, $temp_row, rtrim($rowspsh[$ak][2], " \n"));
                                        $newworksheet->getStyle('G'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                                        // background color: blue
                                        $newworksheet->getStyle('G'.$temp_row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FFDCE6F1');
    
                                    }
                                }
                                
                                $datarow++;
                            }
                            
                            break;
                        // 分期完納
                        case 2:
                            $datarow = 1;
                            foreach($rowspsh as $ak=>$av)
                            {
                                if($datarow != 1)
                                {
                                    if(trim((string)$av[0]) == trim((string)$caseValue))
                                    {
                                        $newworksheet->setCellValueByColumnAndRow(8, $temp_row, rtrim($rowspsh[$ak][2], " \n"));
                                        $newworksheet->getStyle('H'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    
                                    }  
                                }
                                
                                $datarow++;                               
                            }   
                                                    
                            break;
                        // 分期
                        case 3:
                            $datarow = 1;
                            foreach($rowspsh as $ak=>$av)
                            {
                                if($datarow != 1)
                                {
                                    if(trim((string)$av[0]) == trim((string)$caseValue))
                                    {
                                        
                                        $newworksheet->setCellValueByColumnAndRow(9, $temp_row, rtrim($rowspsh[$ak][1], " \n"));
                                        $newworksheet->getStyle('I'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                                        // background color: green
                                        $newworksheet->getStyle('I'.$temp_row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FFEBF1DE');
    
                                        $newworksheet->setCellValueByColumnAndRow(10, $temp_row, rtrim($rowspsh[$ak][2], " \n"));
                                        $newworksheet->getStyle('J'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    
                                        
                                        $newworksheet->setCellValueByColumnAndRow(11, $temp_row, rtrim($rowspsh[$ak][3], " \n"));
                                        $newworksheet->getStyle('K'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                                        // background color: purple
                                        $newworksheet->getStyle('K'.$temp_row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('FFD8D8EB');;
    
                                    } 
                                }
                                   
                                $datarow++;                              
                            }                            
                            break;
                        // 執行命令
                        case 4:
                            $datarow = 1;
                            foreach($rowspsh as $ak=>$av)
                            {
                                if($datarow != 1)
                                {
                                    if(trim((string)$av[0]) == trim((string)$caseValue))
                                    {
                                        $newworksheet->setCellValueByColumnAndRow(12, $temp_row, rtrim($rowspsh[$ak][1], " \n"));
                                        $newworksheet->getStyle('L'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    
                                    }  
                                }
                                
                                $datarow++;                               
                            }                            
                            break;
                        // 移送
                        case 5:
                            $datarow = 1;
                            foreach($rowspsh as $ak=>$av)
                            {
                                if($datarow != 1)
                                {
                                    if(trim((string)$av[0]) == trim((string)$caseValue))
                                    {
                                        $newworksheet->setCellValueByColumnAndRow(13, $temp_row, rtrim($rowspsh[$ak][1], " \n"));
                                        $newworksheet->getStyle('M'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    
                                    } 
                                }
                                 
                                $datarow++;                                 
                            }                            
                            break;
                        // 憑證
                        case 6:
                            $datarow = 1;
                            foreach($rowspsh as $ak=>$av)
                            {
                                if($datarow != 1)
                                {
                                    if(trim((string)$av[0] )== trim((string)$caseValue))
                                    {
                                        $newworksheet->setCellValueByColumnAndRow(14, $temp_row, rtrim($rowspsh[$ak][1], " \n"));
                                        $newworksheet->getStyle('N'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    
                                    } 
                                }
                                  
                                $datarow++;                             
                            }                            
                            break;
                        // 撤註銷
                        case 7:
                            $datarow = 1;
                            foreach($rowspsh as $ak=>$av)
                            {
                                if($datarow != 1)
                                {
                                    if(trim((string)$av[0]) == trim((string)$caseValue))
                                    {
                                        $newworksheet->setCellValueByColumnAndRow(15, $temp_row, rtrim($rowspsh[$ak][1], " \n"));
                                        $newworksheet->getStyle('O'.$temp_row)->getAlignment()->setWrapText(true)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    
                                    } 
                                }
                                    
                                $datarow++;                            
                            }                            
                            break;
                        default:
                            # code...
                            break;
                    }
                    
    
                    ++$temp_row;
                }
            }
            // $startRow = $startRow - 1;
        }
            
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($newsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($dirpath . '/' . $exp_filename);   
        echo $exp_filename;

    }
    function filter_by_value($array, $index, $value){
        if(is_array($array) && count($array)>0) 
        {
            foreach(array_keys($array) as $key){
                $temp[$key] = $array[$key][$index];
                
                if ($temp[$key] == $value){
                    $newarray[$key] = $array[$key];
                }
            }
          }
      return $newarray;
    }
    
}
