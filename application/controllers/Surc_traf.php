<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class Surc_traf extends CI_Controller {//移送怠金
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }

    function table_dp($id) {//
        $this->load->library('table');
        $query = $this->getsqlmod->getFTListSurc($id)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 1400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限');
        $table_row = array();
        foreach ($query as $susp)
        {
            $date = new DateTime($susp->f_date); // Y-m-d
            $date->add(new DateInterval('P30D'));
            if($date->format('d')>25){
                $date->add(new DateInterval('P5D'));
            }
            $day = $date->format('d')%5;
            //$day = 6;
            if($day < 5){
                while($day  < 5){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            else if($day < 10){
                while($day  < 10){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->fd_zipcode;
            $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            $table_row[] = $susp->surc_findate;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }

    public function index() {
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $cp=$this->getsqlmod->getstproject($table)->result();
        $test_table = $this->table_dp($table);
        $type_options = array();
        foreach ($cp as $rp1){
            $type_options[$rp1->stp_name] = $rp1->stp_name;
        }
        $data['s_table'] = $test_table;
        $data['title'] = "待移送怠金案件";
        $data['opt'] = $type_options;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surc_traf/listdp';
        else $data['include'] = 'Surc_traf_query/listdp';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function updateProject() {//加入專案
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $uname = $this -> session -> userdata('uname');
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        //var_dump($_POST['s_cnum']);
        if($_POST['s_status']=='1' && $_POST['s_cnum']!=''){//
            foreach ($s_cnum as $key => $value) {
                //echo $value.'<br>';
                $query = $this->getsqlmod->getSurcTdetail($value)->result(); 
                $fine_traf = $this->getsqlmod->getsurcTrac_withsnum($value)->result(); 
                $fine_t = $this->getsqlmod->getSurc_traf()->result(); 
                $fine_tnum = $fine_t[0]->ft_no; 
                $c_value = substr($fine_tnum,-4);
                $c_value++;
                $c_value = str_pad($c_value,4,'0',STR_PAD_LEFT);
                $taiwan_date = date('Y')-1911; 
                $ft_no='A'.$taiwan_date .'-'.$c_value ;
                //echo  $ft_no.'<br>';
                //var_dump($query[0]);
                $susp = $query[0]; 
                $data = array(
                    'ft_no' => $ft_no,
                    'ft_send_no' => $_POST['projectname'],
                    'ft_projectnum' => $_POST['projectname'],
                    'ft_empno' => $uname,
                    'ft_snum' => $susp->f_snum,
                    'ft_sic' => $susp->f_sic,
                    'ft_cnum' => $susp->f_cnum,
                    'ft_dnum' => $susp->fd_num,
                    'ft_surcnum' => $susp->surc_no,
                );
                if(!isset($fine_traf[0]->ft_snum))$this->getsqlmod->addst($data);
                else $this->getsqlmod->updatest($value,$data);
            }
            $data1 = array(
                    'stp_name' => $_POST['projectname'],
                    'stp_date' => date('Y-m-d'),
                    'stp_empno' => $uname,
            );
            if($_POST['s_cnum']!=NULL);$this->getsqlmod->addstp($data1);
            redirect('Surc_traf/ProjectList/'); 
        }
        else if($_POST['s_status']=='0' && $_POST['s_cnum']!=''){//加入舊案
            foreach ($s_cnum as $key => $value) {
                $query = $this->getsqlmod->getSurcTdetail($value)->result(); 
                $fine_traf = $this->getsqlmod->getsurcTrac_withsnum($value)->result(); 
                $fine_t = $this->getsqlmod->getSurc_traf()->result(); 
                $fine_tnum = $fine_t[0]->ft_no; 
                $c_value = substr($fine_tnum,-4);
                $c_value++;
                $c_value = str_pad($c_value,4,'0',STR_PAD_LEFT);
                $taiwan_date = date('Y')-1911; 
                $ft_no='A'.$taiwan_date .'-'.$c_value ;
                //echo  $ft_no.'<br>';
                $susp = $query[0]; 
                //var_dump($susp);
                $data = array(
                    'ft_no' => $ft_no,
                    'ft_send_no' => $_POST['stp_name'],
                    'ft_projectnum' => $_POST['stp_name'],
                    'ft_empno' => $uname,
                    'ft_snum' => $susp->f_snum,
                    'ft_sic' => $susp->f_sic,
                    'ft_cnum' => $susp->f_cnum,
                    'ft_dnum' => $susp->fd_num,
                    'ft_surcnum' => $susp->surc_no,
                );
                if(!isset($fine_traf[0]->ft_snum))$this->getsqlmod->addst($data);
                else $this->getsqlmod->updatest($value,$data);
            }
            redirect('Surc_traf/ProjectList/'); 
        }
        else redirect('Surc_traf/index/'); 
    }

    function table_DPProject1($id) {
        $this->load->library('table');
        $query = $this->getsqlmod->getstproject($id)->result(); // 
        ////var_dump($query->result());
        $tmpl = array (
            'table_open' => '<table border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('','專案編號','發文日期');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $table_row = NULL;
            $table_row[] = $susp->stp_name;
            $table_row[] = anchor('Surc_traf/listtraf/' . $susp->stp_name, $susp->stp_name);
            $table_row[] = $susp->stp_date;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    
    public function ProjectList() {//(催繳專案)
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $test_table = $this->table_DPProject1($table);
        $data['s_table'] = $test_table;
        $data['title'] = "待移送怠金專案列表";
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surc_traf/dpprojectlist';
        else $data['include'] = 'Surc_traf_query/dpprojectlist';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    function table_CP($id) {//催繳案件一般
        $this->load->library('table');
        $query = $this->getsqlmod->getFTListfromProjectSurc($id)->result(); 
        $tmpl = array (
            'table_open' => '<table style="width: 2400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限','發文日期(怠金)');
        $table_row = array();
        foreach ($query as $susp)
        {
            $date = new DateTime($susp->surc_findate); // Y-m-d
            $date->add(new DateInterval('P30D'));
            if($date->format('d')>25){
                $date->add(new DateInterval('P5D'));
            }
            $day = $date->format('d')%5;
            //$day = 6;
            if($day < 5){
                while($day  < 5){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            else if($day < 10){
                while($day  < 10){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->s_name;
            $table_row[] = $susp->fd_zipcode;
            $table_row[] = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
            $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            $table_row[] = $susp->surc_date;
            $table_row[] = $susp->surc_date;
            $this->table->add_row($table_row);
        }   
        return $this->table->generate();
    }
    
    public function listtraf() {//處理專案
        $this->load->helper('form');
        $table = $this->session-> userdata('uoffice');
        $id = $this->uri->segment(3);
        $dp = $this->getsqlmod->getstprojecttype($id)->result();
        $susp = $this->getsqlmod->getFTListfromProjectSurc($id)->result();
        $test_table = $this->table_CP($id);
        $data['s_table'] = $test_table;
        $data['title'] = "待移送怠金專案:".$id;
        //$data['type'] = $dp[0]->cp_type;
        $data['sp'] = $dp[0]->stp_name;
        //$data['fd_num'] = $susp[0]->s_dp_project;
        $data['user'] = $this -> session -> userdata('uic');
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surc_traf/dplist1';
        else $data['include'] = 'Surc_traf_query/dplist1';
        $data['nav'] = 'navbar3';
        $data['id'] = $id;
        $this->load->view('template', $data);
    }
    
    public function listtraf_ed() {//專案編輯（一般）
        $this->load->library('table');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getFTListfromProjectSurc($id)->result(); 
        $dp = $this->getsqlmod->getstprojecttype($id)->result();
        $data['sp'] = $dp[0]->stp_name;
        $tmpl = array (
            'table_open' => '<table style="width: 2400px;" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">',
            'heading_row_start' => '<tr>',
            'row_start' => '<tr>'
            );
        $this->table->set_template($tmpl);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading( '','處分書編號','受處分人','郵遞區號','戶籍地址', '現住地址', '繳納期限','發文日期(怠金)','發文日期(移送)','本號(移送發文文號)','支號');
        $table_row = array();
        $i=0;
        foreach ($query as $susp)
        {
            $i++;
            $date = new DateTime($susp->f_date); // Y-m-d
            $date->add(new DateInterval('P30D'));
            if($date->format('d')>25){
                $date->add(new DateInterval('P5D'));
            }
            $day = $date->format('d')%5;
            //$day = 6;
            if($day < 5){
                while($day  < 5){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            else if($day < 10){
                while($day  < 10){
                    $day++;
                    $date->add(new DateInterval('P1D'));
                }
            }
            $table_row = NULL;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->surc_no;
            $table_row[] = $susp->s_name;
            if($susp->surc_address == null || $susp->surc_address == '0'){
                $table_row[] = '<input name="fd_zipcode['.$susp->surc_no.']" required="required" class="form-control"  value="'.$susp->s_dpzipcode.'" >';
            }
            else{
                $table_row[] = '<input name="fd_zipcode['.$susp->surc_no.']" type="number" required="required" value='.$susp->surc_zipcode.'>';
            }
            if($susp->surc_address == null || $susp->surc_address == '0'){
                $table_row[] = '<input name="fd_address['.$susp->surc_no.']" required="required" class="form-control"  value="'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress.'" >';
            }
            else{
                $table_row[] = '<input name="fd_address['.$susp->surc_no.']" required="required" style="width:250px" value="'.$susp->surc_address.'" >';
            }
            $table_row[] = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
            $table_row[] = $susp->surc_findate;
            $table_row[] = $susp->surc_date;
            $table_row[] = '<input name="ft_send_date['.$susp->surc_no.']" type="date" required="required" value='.$susp->ft_send_date.' class="form-control" >';
            $table_row[] = '<input name="ft_send_no['.$susp->surc_no.']"  required="required" value='.$susp->ft_send_no.' >';
            if($susp->ft_send_bno == null || $susp->ft_send_bno == '0'){
                $table_row[] = '<input name="ft_send_bno['.$susp->surc_no.']" style="width:20px" required="required" value='.$i.' >';
            }
            else{
                $table_row[] = '<input name="ft_send_bno['.$susp->surc_no.']" style="width:20px" required="required" value='.$susp->ft_send_bno.' >';
            }
            $this->table->add_row($table_row);
        }   
        $data['title'] = "待移送怠金專案:".$id;
        $data['fpid'] = $id;
        $data['ft_send_date'] = $query[0]->ft_send_date;
        $data['user'] = $this -> session -> userdata('uic');
        $data['s_table'] = $this->table->generate();
        $data['id'] = $id;
        if(preg_match("/c/i", $this->session-> userdata('3permit'))) $data['include'] = 'Surc_traf/dplist_ed';
        else $data['include'] = 'Surc_traf_query/dplist_ed';
        $data['nav'] = 'navbar3';
        $this->load->view('template', $data);
    }
    
    public function updateTPDate(){
        //var_dump($_POST);
        $this->getsqlmod->updateSTdate($_POST['ft_projectnum'],$_POST['ft_send_date']);
        redirect('Surc_traf/listtraf_ed/'.$_POST['ft_projectnum']); 
    }
    
    public function updatepublicdate(){
        //var_dump($_POST);
        $this->getsqlmod->updatepublicdate($_POST['call_no'],$_POST['call_date']);
        redirect('Surc_traf/listtraf_ed_public/'.$_POST['call_no']); 
    }
    
    public function updateCP(){
        //var_dump($_POST);
        $this->load->helper('form');
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        //var_dump($_POST);
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        $s_cnum1 =mb_split(",",$_POST['s_cnum1']);
        //var_dump($_POST);
        foreach ($s_cnum1 as $key => $value) {
            $surcharges = array(
                'surc_address' => $_POST['fd_address'][$value],
                'surc_zipcode' => $_POST['fd_zipcode'][$value],
            );
            $ft = array(
                'ft_send_date' => $_POST['ft_send_date'][$value],
                'ft_send_no' => $_POST['ft_send_no'][$value],
                'ft_send_bno' => $_POST['ft_send_bno'][$value],
            );
            //var_dump($surcharges);
           // echo '<br>';
           // var_dump($ft);
            $this->getsqlmod->updatest($value,$ft);
            $this->getsqlmod->updateSurc($value,$surcharges);
        };
        redirect('Surc_traf/listtraf/'.$_POST['call_projectnum']); 
    }
    
    public function updatestatus_ready() {
        $this->load->helper('form');
        $s_cnum =mb_split(",",$_POST['s_cnum']);
        //var_dump($s_cnum);
        if($_POST['s_status']=='1'){
            foreach ($s_cnum as $key => $value) {
                $this->getsqlmod->updatestp_sp($value,'寄出');
            }
            redirect('Surc_traf/ProjectList/'); 
        }
    }    
    
    public function exportCSV(){//公示清冊
        $this->load->dbutil();
        $this->load->helper('download');
        $id = $this->uri->segment(3);
        $filename = '發文簿'.date('Ymd').'.csv';
        header("Content-Description: File Transfer"); 
        header("Content-Disposition: attachment; filename=$filename"); 
        header("Content-Type: application/vnd.ms-excel; "); 
        $usersData = $this->getsqlmod->getFTListfromProjectSurc($id)->result();
        $file = fopen('php://output', 'a');
        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
        $header = array("編號","總收發文字","收(發)文日期","移送案號","事由","辦理情形","備考"); 
        fputcsv($file, $header);
        $i=0;
        foreach ($usersData as $key=>$line){ 
            $i++;
            fputcsv($file,array($i.'.',$line->ft_send_no,$line->ft_send_date,$line->ft_no,$line->s_name.'移送書','移'.$line->ft_target.'分署辦理',''));
        }
        fclose($file); 
        exit;
        //$query = $this->getsqlmod->getfine_list($id); 
        //$data = $this->dbutil->csv_from_result($query);
        //force_download($filename,  $data);
    }

    public function fine_doc_project_fin_no(){//產生處分書pdf 移送產生處分書
        $this->load->library('PHP_TCPDF');
        $project = $this->uri->segment(3);
        $query = $this->getsqlmod->getFTListfromProjectSurc($project); 
        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            //$pdf->setPrintFooter(false); //不要頁尾
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5.5, 3.5, 10,true);
            // set some language-dependent strings (optional)
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->AddPage('P', 'A4');
            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $html='';       
            foreach ($query->result() as $suspect)
            {
                $id = $suspect->s_num;
                $susp=$this->getsqlmod->get1Susp_ed('s_num',$id)->result();
                $susp=$susp[0];
                $fine=$this->getsqlmod->getfineR('f_snum',$id)->result();
                $fine=$fine[0];
                $DpOrder=$this->getsqlmod->get1SuspOrder('s_dp_project',$susp->s_dp_project)->result();
                $sp1=$this->getsqlmod->get3SP1($id)->result();
                $sp=$sp1[0];
                $spcount=$this->getsqlmod->getfdAll()->result();
                $cases=$this->getsqlmod->getCases($susp->s_cnum)->result();
                $cases=$cases[0];
                $phone=$this->getsqlmod->getPhone('p_s_num',$id)->result();
                //$phone=$phone[0];
                if(!isset($phone))$phone1=null;
                if(isset($phone))$phone1=$phone[0]->p_no;
                $drug=$this->getsqlmod->get1Drug_ic($susp->s_ic)->result();
                $drug=$drug[0];
                $drug2=$this->getsqlmod->get2DrugCk('ddc_drug',$drug->e_id)->result();
                $drug2=$drug2[0];
                $prison=$this->getsqlmod->getprison()->result();
                $prison=$prison[0];
                $html .='
                                <div style="width:680px" class="panel-body"><p>
                                    <table>
                                        <tr><td ><h2>列管編號：'.$suspect->surc_no .'</h2> </td><td colspan="2"><h2>原處分管制編號：'.$sp->fd_num .'</h2></td></tr>
                                        <tr><td colspan="3">正本：'.$sp->fd_target.'君'.$sp->fd_zipcode.' '.$sp->fd_address.'</td></tr>
                                        <tr><td colspan="3">副本：我是測試系統刑事警察大隊毒品查緝中心<br>臺北市政府毒品危害防制中心</td></tr>
                                    </table>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680"><h3 align="center">我是測試系統違反毒品危害防制條例案件（怠金）處分書</h3></td>
                                            </tr>
                                            <tr>
                                                <td width="77">依據</td><td colspan="7" width="603"><strong>臺北市政府衛生局'.(date('Y')-1911).'年'.date('m', strtotime($suspect->surc_basenumdate)).'月'.date('d', strtotime($suspect->surc_basenumdate)).'日北市警刑毒緝字第號'.$sp->fd_send_num.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77">
                                                <br>
                                                <h3 align="center">受處分人</h3></td>
                                                <td align="center" colspan="2" width="70">姓名</td>
                                                <td align="center" width="88"><strong>'.$susp->s_name.'</strong></td>
                                                <td align="center" width="49">性別</td>
                                                <td align="center" width="50"><strong>'.$susp->s_gender.'</strong></td>
                                                <td align="center" width="70" valign="top">出生年月日</td>
                                                <td align="center" width="276"><strong>民國'.(date('Y', strtotime($susp->s_birth))-1911) .'年'.(date('m', strtotime($susp->s_birth))) .'月'.(date('d', strtotime($susp->s_birth))) .'日</strong></td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" width="70" valign="top">身分證<br>統一編號</td>
                                                <td align="center" align="center"width="88"><strong>'.$susp->s_ic.'</strong></td>
                                                <td align="center" colspan="3" width="169" valign="center">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"><strong>'.$phone1.'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"><strong>'.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress .'</strong></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77"><h3 align="center">法　定<br>代理人</h3></td>
                                                <td colspan="2" width="70">姓名</td>
                                                <td width="88"></td>
                                                <td width="49">性別</td>
                                                <td width="50"></td>
                                                <td width="70" valign="top">出生年月日</td>
                                                <td width="276"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="70" valign="top">身分證統一編號</td>
                                                <td width="88"></td>
                                                <td colspan="3" width="169" valign="top">其他足資辨別之特徵<br>及聯絡電話</td>
                                                <td width="276"></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">地址</td>
                                                <td width="70">現住地</td>
                                                <td colspan="5" width="481"></td>
                                            </tr>
                                            <tr>
                                                <td width="70">戶籍地</td>
                                                <td colspan="5" width="481"></td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">主旨</h3></td>
                                                <td colspan="7" width="603">受處分人處罰：<br>
                                                        一、新臺幣<strong>'.($suspect->surc_amount).'</strong>元整<br>
                                                            二、仍須參加講習，講習時地詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續<br>
                                                                      處以怠金。。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center"><br><br><br>事實</h3></td>
                                                <td colspan="7" width="603" height="100px">緣受處分人'.$susp->s_name.'涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於民國'.(date('Y', strtotime($sp->fd_lec_date))-1911) .'年'.(date('m', strtotime($fine->f_date))) .'月'.(date('d', strtotime($fine->f_date))) .'日至'.$sp->fd_lec_place.'參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。</td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">理由及<br>法令依據</h3></td>
                                                <td colspan="7" width="603">
                                                        ■一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習者，依行政執行法規定處以怠金。<br>
                                                        ■二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不能由他人代為履行者，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。<br>
                                                        ■三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續處以怠金。<br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">繳納期限<br>及方式</h3></td>
                                                <td colspan="7" width="603"> 一、怠金限於<strong>民國'.(date('Y', strtotime($suspect->surc_findate))-1911) .'年'.(date('m', strtotime($suspect->surc_findate))) .'月'.(date('d', strtotime($suspect->surc_findate))) .'日</strong>前選擇下列方式之一繳款：
                                                            (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                            (二)至金融機構臨櫃繳款至「臨櫃帳戶」。<br>
                                                        二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong><br>
                                                            (三)輸入本案虛擬帳號：<strong>'.$suspect->surc_BVC.'</strong>
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong>16112470361019</strong>，戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話」，俾利辦理銷案。<br>
                                                        四、	匯款繳納時，請於匯款單上之匯款人欄填寫受處分人之姓名「'.$suspect->s_name.'」，並應要求於備註(附言)欄填入案件列管編號「'.$suspect->surc_no.'」及身分證統一編號「'.$suspect->s_ic.'」，俾利辦理銷案。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">毒品講習</h3></td>
                                                <td colspan="7" width="603">
                                                    一、可選擇參與實體講習或線上課程（線上課程每年可參與三次，惟應以不同套裝課程申請認定）。
                                                    二、講習時間：民國'.(date('Y', strtotime($suspect->surc_lec_date))-1911) .'年'.(date('m', strtotime($suspect->surc_lec_date))) .'月'.(date('d', strtotime($suspect->surc_lec_date))) .'日（請攜帶有相片之證件報到，逾時將無法入場），地點：'.$suspect->surc_lec_place.'。有正當理由無法參加或要選擇線上課程者，請至下列網址：https://nodrug.gov.taipei下載異動申請表或依指示完成線上課程。講習相關問題，請逕向臺北市政府毒品危害防制中心洽詢，電話(02)2375-4068。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77"><h3 align="center">注意事項</h3></td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：10042臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：'.$suspect->surc_empno.'、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <table> 
                                    <tr>
                                    <td height="190"></td>   
                                    </tr>            
                                    </table> 
                                </div>';
            }
            $pdf->writeHTML($html, true, false, false, false, '');                        
        $pdf->Output($project.'.pdf', 'D');    //download
        //$pdf->Output($project.'.pdf', 'D');    //read
    }
}
