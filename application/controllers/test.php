<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    public function __construct()
    {
        parent::__construct(); 
        $this->load->helper('url_helper');               
        $this->load->helper('date'); 
    }
    
    public function index()
    {

        $this->load->library('PHP_TCPDF');

        ob_end_clean(); // slove problem TCPDF ERROR: Some data has already been output, can't send PDF file
        //$pdf = new PHP_TCPDF();
        //$pdf->AddPage();
        //$pdf->Cell(40,10,'Hello World!');
        //$pdf->Output();*/
            $pdf = new PHP_TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false); //不要頁首
            $pdf->setPrintFooter(false); //不要頁尾
            //$pdf->addFormat("custom", 680, 768); 
            // set default header data
            //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,77));
            //$pdf->setFooterData(array(0,64,0), array(0,64,77));

            // set header and footer fonts
            //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            // 版面配置 > 邊界
            //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(10, 1.5, 70.5,true);

            // 頁首上方與頁面頂端的距離
            //$pdf->SetHeaderMargin(1);
            // 頁尾上方與頁面底端的距離
            //$pdf->SetFooterMargin(1);

            // set auto page breaks
            // 自動分頁
            //$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
            $pdf->setFontSubsetting(true);
            $pdf->SetFont('msungstdlight', '', 10);
            $pdf->AddPage();
            $html='';
            $html='
                                <div style="width:680px" class="panel-body">
                                    <p>
                                        列管編號： echo $sp->fd_num 郵寄 以稿代簽 限制開放<br>
                                        第二層決行 檔號：/07270399 保存年限：3年<strong></strong><br>
                                        校對：                                           監印：                                            發文：    <\n>                                                                        
                                        正本：<br>
                                        副本：我是測試系統、我是測試系統刑事警察大隊、臺北市政府毒品危害防制中心</strong>
                                    </p>
                                    <table width="680" cellspacing="0" cellpadding="0" border="1">
                                            <tr>
                                                <td colspan="8" width="680">
                                                    <h3 align="center">我是測試系統 違反毒品危害防制條例案件處分書(稿)</h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">
                                                        發文日期字號：北市警刑毒緝字第號
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="680">
                                                        依據：我是測試系統日北市警分刑字第號
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77">
                                                        <h3 align="center">受處分人</h3>
                                                </td>
                                                <td colspan="2" width="70">
                                                        姓名
                                                </td>
                                                <td width="88">
                                                        <strong> echo $susp->s_name</strong>
                                                </td>
                                                <td width="49">
                                                        性別
                                                </td>
                                                <td width="50">
                                                       <strong>  echo $susp->s_gender</strong>
                                                </td>
                                                <td width="70" valign="top">
                                                        出生
                                                        年月日
                                                </td>
                                                <td width="276">
                                                        <strong>民國</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="70" valign="top">
                                                        身  分  證
                                                        統一編號
                                                </td>
                                                <td width="88">
                                                        <strong> echo $susp->s_ic</strong>
                                                </td>
                                                <td colspan="3" width="169" valign="top">
                                                        其他足資辨別之特徵及聯絡電話
                                                </td>
                                                <td width="276">
                                                        <strong>1</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">
                                                        地址
                                                </td>
                                                <td width="70">
                                                        現住地
                                                </td>
                                                <td colspan="5" width="481">
                                                        <strong> echo $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="70">
                                                        戶籍地
                                                </td>
                                                <td colspan="5" width="481">
                                                        <strong> echo $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="77">
                                                        <h3 align="center">法　定<br>
                                                        代理人</h3>
                                                </td>
                                                <td colspan="2" width="70">
                                                        姓名
                                                </td>
                                                <td width="88">
                                                </td>
                                                <td width="49">
                                                    
                                                        性別
                                                </td>
                                                <td width="50">
                                                    
                                                </td>
                                                <td width="70" valign="top">
                                                        出生
                                                        年月日
                                                </td>
                                                <td width="276">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="70" valign="top">
                                                        身分證
                                                        統一編號
                                                </td>
                                                <td width="88">
                                                </td>
                                                <td colspan="3" width="169" valign="top">
                                                        其他足資辨別之特徵及聯絡電話
                                                </td>
                                                <td width="276">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">
                                                    
                                                        地址
                                                </td>
                                                <td width="70">
                                                        現住地
                                                </td>
                                                <td colspan="5" width="481">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="70">
                                                        戶籍地
                                                </td>
                                                <td colspan="5" width="481">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77">
                                                    <h3 align="center">主旨</h3>
                                                </td>
                                                <td colspan="7" width="603">
                                                        受處分人處罰：
                                                        一、新臺幣<strong> echo ($fine->f_damount/10000) </strong>萬元整
                                                            二、毒品危害 <strong>6 </strong>小時。(講習時間詳見下方「毒品講習」欄位)
                                                        三、<strong> echo $drug->e_count.$drug->e_type</strong>沒入。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77">
                                                    <h3 align="center">事實</h3>
                                                </td>
                                                <td colspan="7" width="603" height="100px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77">
                                                    <h3 align="center">理由及<br>法令依據</h3>
                                                </td>
                                                <td colspan="7" width="603">
                                                        ■
                                                        依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                        <br>■
                                                        依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77">
                                                    <h3 align="center">繳納期限<br>及方式</h3>
                                                </td>
                                                <td colspan="7" width="603">
                                                        一、罰鍰限於<strong>民國前選擇下列方式之一繳款：
                                                        (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                        (二)至金融機構臨櫃繳款至「臨櫃帳戶」。
                                                        二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：
                                                        (三)輸入本案虛擬帳號：
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong> 16112470361019</strong><strong>，</strong>戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<u>備註(附言)欄位</u>填寫受處分人「姓名、身分證字號、電話</strong><strong>」</strong>，俾利辦理銷案。
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77">
                                                    <h3 align="center">毒品講習</h3>
                                                </td>
                                                <td colspan="7" width="603">
                                                        一、可選擇參與實體講習或線上課程(每年可參與三次，惟應以申請不同套裝課程)。
                                                        二、講習時間： 民國109年10月 17日 09:30分（請攜帶有相片之證件報到，逾時將無法入場），地點：臺北市非政府(NGO)會館1919樓 多功能資料室 。如要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。
                                                        三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="77">
                                                    <h3 align="center">注意事項</h3>
                                                </td>
                                                <td colspan="7" width="603">
                                                    一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：10042臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    二、承辦人：«承辦人»、電話：(02）2393-2397。                 
                                                </td>
                                            </tr>
                                        </table>
                                    <p>
                                        承辦單位 (2539) 核稿 決行
                                    </p>            
                                </div>';

            $pdf->writeHTML($html, true, false, true, false, '');
                        
        $pdf->Output('test.pdf', 'I');    
    }
}

// Include the main TCPDF library (search for installation path).
/*class MYPDF extends TCPDF {
}*/