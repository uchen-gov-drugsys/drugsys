<?php
if (! defined ( 'BASEPATH' ))  exit ( 'No direct script access allowed' );

class XMLcreate extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // load base_url
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->model ( 'getsqlmod' ); // 載入model
        $this -> load -> library('Session/session');
    }
    public function downloadsignxml(){//怠金簽
        ////var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getdeliverylist_forpublicProjectSurc($id); 
        $count = $query->num_rows();
        $su = $query->result();
        $datet = (date('Y')-1911).'年'.date('m').'月'.date('m').'日';
        $dom = new DOMDocument("1.0","utf-8"); 
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $implementation = new DOMImplementation();
        $dom->appendChild($implementation->createDocumentType('簽 SYSTEM "104_5_utf8.dtd"')); 
        // display document in browser as plain text 
        // for readability purposes 
        header("Content-Type: text/plain"); 
         
        $root = $dom->createElement("簽"); 
        $dom->appendChild($root); 
        
        $item = $dom->createElement("於"); 
        $root->appendChild($item); 
        
        $item = $dom->createElement("檔號"); 
        $root->appendChild($item); 
        
        $item2 = $dom->createElement("年月日"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(date('Y')-1911 .'年'. date('m').'月'.date('d').'日'); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("機關"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('全銜'); 
        $text = $dom->createTextNode("刑事警察大隊(毒品查緝中心)"); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        
        $item2 = $dom->createElement("主旨"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $text = $dom->createTextNode('有關受處分人'.$su[0]->s_name.' 等'. $count . '人違反「毒品危害防制條例」案件怠金處分書公示送達一案，請核示。'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
          
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "說明：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "一、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("依據文號臚列如下："); 
        $item3->appendChild($text); 
        $i=0;
        foreach($su as $susp){
            $i++;
            $item3 = $dom->createElement('條列'); 
            $item3->setAttribute("序號", $i."、");  
            $item2->appendChild($item3); 
            $item4 = $dom->createElement('文字'); 
            $item3->appendChild($item4); 
            $text = $dom->createTextNode("本局".$susp->s_roffice."北市警". (date("Y",strtotime($susp->surcdate))- 1911) .'月'. date("m",strtotime($susp->surcdate)) .'月'.date("d",strtotime($susp->surcdate)) .'日'.$susp->s_roffice1."分刑字第".$susp->surc_basenum."號(列管編號：".$susp->surc_no.")。"); 
            $item4->appendChild($text); 
        }
        
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "擬辦：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("奉核後，製作公告及怠金處分書函送臺北市政府秘書處刊登市府公報。"); 
        $item2->appendChild($text); 

        $item = $dom->createElement("敬陳"); 
        $root->appendChild($item); 
        $text = $dom->createTextNode("敬陳　大隊長"); 
        $item->appendChild($text); 

        echo $dom->saveXML();  
        $this->load->helper('download');
        force_download($datet.'.di', $dom->saveXML());
        redirect('Surcharges/listProjectNormal/'.$id); 
    }

    public function downloadpublicxml(){//下載怠金公告XML
        ////var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $uname = $this -> session -> userdata('uname');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($id); 
        $count = $query->num_rows();
        $su = $query->result();
        $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
        $dom = new DOMDocument("1.0","utf-8"); 
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $implementation = new DOMImplementation();
        $dom->appendChild($implementation->createDocumentType('簽 SYSTEM "104_5_utf8.dtd"')); 
        // display document in browser as plain text 
        // for readability purposes 
        header("Content-Type: text/plain"); 
         
        $root = $dom->createElement("公告"); 
        $dom->appendChild($root); 
        
        $item = $dom->createElement("發文機關"); 
        $root->appendChild($item); 
       
        $item2 = $dom->createElement("全銜"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("我是測試系統"); 
        $item2->appendChild($text); 
       
        $item2 = $dom->createElement("機關代碼"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("379130000C"); 
        $item2->appendChild($text); 
       
        $item = $dom->createElement("發文日期"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("年月日"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(date('Y')-1911 .'年'. date('m').'月'.date('d').'日'); 
        $item2->appendChild($text); 
        
        $item = $dom->createElement("發文字號"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("北市警刑毒緝"); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("文號"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('年度'); 
        $text = $dom->createTextNode(date('Y')-1911); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        $item3 = $dom->createElement('流水號'); 
        $text = $dom->createTextNode('3002347'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        $item3 = $dom->createElement('支號'); 
        $text = $dom->createTextNode('2'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
          
        $item = $dom->createElement("附件"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("主旨"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $text = $dom->createTextNode('公示送達應受送達人'.$su[0]->s_name.' 等'. $count . '人違反「毒品危害防制條例」案怠金處分書'.$count.'份'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "依據：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "一、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("毒品危害防制條例第11條之1第2項。"); 
        $item3->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "二、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("行政程序法第78條第3項。"); 
        $item3->appendChild($text); 
        $suspect ='';
        foreach($su as $susp){
            $suspect = $suspect . $susp->s_name.'、';
        }

        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "公告事項：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("應受送達人".substr($suspect,0,-3)."等".$count."人違反毒品危害防制條例，應送達之怠金處分書9份，現由本局刑事警察大隊毒品查緝中心(地址：臺北市中正區北平東路1號6樓、電話：23932397、承辦人警務員".$uname."保管，應受送達人得隨時前來領取。"); 
        $item2->appendChild($text); 

        echo $dom->saveXML();  
        $this->load->helper('download');
        force_download($datet.'.di', $dom->saveXML());
        redirect('Surcharges/listProjectNormal/'.$id); 
    }
    
    public function downloadnopunishxml(){//下載怠金不罰函XML
        ////var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $uname = $this -> session -> userdata('uname');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getsurchangelist1($id); 
        $count = $query->num_rows();
        $su = $query->result();
        $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
        $dom = new DOMDocument("1.0","utf-8"); 
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $implementation = new DOMImplementation();
        $dom->appendChild($implementation->createDocumentType('簽 SYSTEM "104_5_utf8.dtd"')); 
        // display document in browser as plain text 
        // for readability purposes 
        header("Content-Type: text/plain"); 
         
        $root = $dom->createElement("公告"); 
        $dom->appendChild($root); 
        
        $item = $dom->createElement("發文機關"); 
        $root->appendChild($item); 
       
        $item2 = $dom->createElement("全銜"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("我是測試系統"); 
        $item2->appendChild($text); 
       
        $item2 = $dom->createElement("機關代碼"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("379130000C"); 
        $item2->appendChild($text); 
       
        $item = $dom->createElement("發文日期"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("年月日"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(date('Y')-1911 .'年'. date('m').'月'.date('d').'日'); 
        $item2->appendChild($text); 
        
        $item = $dom->createElement("發文字號"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("北市警刑毒緝"); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("文號"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('年度'); 
        $text = $dom->createTextNode(date('Y')-1911); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        $item3 = $dom->createElement('流水號'); 
        $text = $dom->createTextNode('3002347'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        $item3 = $dom->createElement('支號'); 
        $text = $dom->createTextNode('2'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
          
        $item = $dom->createElement("附件"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("主旨"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $text = $dom->createTextNode('公示送達應受送達人'.$su[0]->s_name.' 等'. $count . '人違反「毒品危害防制條例」案怠金處分書'.$count.'份'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "依據：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "一、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("毒品危害防制條例第11條之1第2項。"); 
        $item3->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "二、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("行政程序法第78條第3項。"); 
        $item3->appendChild($text); 
        $suspect ='';
        foreach($su as $susp){
            $suspect = $suspect . $susp->s_name.'、';
        }

        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "公告事項：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("應受送達人".substr($suspect,0,-3)."等".$count."人違反毒品危害防制條例，應送達之怠金處分書9份，現由本局刑事警察大隊毒品查緝中心(地址：臺北市中正區北平東路1號6樓、電話：23932397、承辦人警務員".$uname."保管，應受送達人得隨時前來領取。"); 
        $item2->appendChild($text); 

        echo $dom->saveXML();  
        $this->load->helper('download');
        force_download($datet.'.di', $dom->saveXML());
        redirect('Surcharges/listProjectNormal/'.$id); 
    }
    
    public function downloadcallsignxml(){//催繳簽
        ////var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getCallListfromProject($id); 
        $count = $query->num_rows();
        $su = $query->result();
        $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
        $dom = new DOMDocument("1.0","utf-8"); 
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $implementation = new DOMImplementation();
        $dom->appendChild($implementation->createDocumentType('簽 SYSTEM "104_8_utf8.dtd" [<!ENTITY 表單 SYSTEM "簽.sw" NDATA DI><!NOTATION DI SYSTEM ""><!NOTATION _X SYSTEM "">]')); 
        // display document in browser as plain text 
        // for readability purposes 
        header("Content-Type: text/plain"); 
         
        $root = $dom->createElement("簽"); 
        $dom->appendChild($root); 
        
        $item = $dom->createElement("檔號"); 
        $root->appendChild($item); 
        
        $item2 = $dom->createElement("年度"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(date('Y')-1911); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("案次號"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("1"); 
        $item2->appendChild($text);

        $item = $dom->createElement("稿面註記"); 
        $root->appendChild($item); 

        $item2 = $dom->createElement("擬辦方式"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("簽稿併陳"); 
        $item2->appendChild($text);
        
        $item2 = $dom->createElement("決行層級"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("二層決行"); 
        $item2->appendChild($text);
        
        $item2 = $dom->createElement("應用限制"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("限制開放"); 
        $item2->appendChild($text);
        
        $item = $dom->createElement("於"); 
        $root->appendChild($item); 

        $item = $dom->createElement("年月日"); 
        $text = $dom->createTextNode(date('Y')-1911 .'年'. date('m').'月'.date('d').'日'); 
        $item->appendChild($text);
        $root->appendChild($item); 

        $item = $dom->createElement("機關"); 
        $root->appendChild($item); 

        $item2 = $dom->createElement("全銜"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("刑事警察大隊毒品查緝中心"); 
        $item2->appendChild($text);
        

        $item2 = $dom->createElement("主旨"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $text = $dom->createTextNode('有關受處分人'.$su[0]->s_name.' 等'. $count . '人違反「毒品危害防制條例」案罰鍰逾期未繳納，請核示。'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
          
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "說明：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "一、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("裁罰受處分人".$su[0]->s_name.' 等'. $count . "人罰鍰逾期未繳納(列管編號詳如附件)，經函發委由中華郵政股份有限公司代為送達受處分人戶籍地催繳，惟案內受處分人郵務送達時因查「無此人」、設籍戶政事務所等事由將函文退還本局刑事警察大隊，致無法通知受處分人進行催繳。"); 
        $item3->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "二、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("依行政執行法第78條規定：「當事人變更其送達之處所而不向行政機關陳明，致有第一項之情形者，行政機關得依職權命為公示送達。」，另依公示送達程序需張貼與發行公報20日後，始發生效力，張貼公報以為公示送達。"); 
        $item3->appendChild($text); 
        
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "擬辦：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("奉核後，製作公告函送臺北市政府秘書處刊登市府公報及張貼公佈欄為公告。"); 
        $item2->appendChild($text); 

        $item = $dom->createElement("敬陳"); 
        $root->appendChild($item); 
        $text = $dom->createTextNode("敬陳　大隊長"); 
        $item->appendChild($text); 

        echo $dom->saveXML();  
        $this->load->helper('download');
        force_download($datet.'.di', $dom->saveXML());
        redirect('Call/listcp_ed_public/'.$id); 
    }

    public function downloadCallpublicxml(){//催繳公告XML
        ////var_dump($_POST);
        $user = $this -> session -> userdata('uic');
        $uname = $this -> session -> userdata('uname');
        $office = $this -> session -> userdata('uoffice');
        $id = $this->uri->segment(3);
        $query = $this->getsqlmod->getCallListfromProject($id); 
        $count = $query->num_rows();
        $su = $query->result();
        $datet = (date('Y')-1911).'年'.date('m').'月'.date('d').'日';
        $dom = new DOMDocument("1.0","utf-8"); 
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $implementation = new DOMImplementation();
        $dom->appendChild($implementation->createDocumentType('簽 SYSTEM "104_5_utf8.dtd"')); 
        // display document in browser as plain text 
        // for readability purposes 
        header("Content-Type: text/plain"); 
         
         
        $root = $dom->createElement("公告"); 
        $dom->appendChild($root); 
        
        $item = $dom->createElement("檔號"); 
        $root->appendChild($item); 
        
        $item2 = $dom->createElement("年度"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(date('Y')-1911); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("分類號"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("07270399"); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("案次號"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("1"); 
        $item2->appendChild($text);

        $item = $dom->createElement("保存年限"); 
        $text = $dom->createTextNode('3'); 
        $item->appendChild($text);
        $root->appendChild($item); 

        $item = $dom->createElement("稿面註記"); 
        $root->appendChild($item); 

        $item2 = $dom->createElement("擬辦方式"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("以稿代簽"); 
        $item2->appendChild($text);
        
        $item2 = $dom->createElement("決行層級"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("二層決行"); 
        $item2->appendChild($text);
        
        $item2 = $dom->createElement("應用限制"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("限制開放"); 
        $item2->appendChild($text);
        
        $item = $dom->createElement("發文機關"); 
        $root->appendChild($item); 
       
        $item2 = $dom->createElement("全銜"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("我是測試系統"); 
        $item2->appendChild($text); 
       
        $item2 = $dom->createElement("機關代碼"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("379130000C"); 
        $item2->appendChild($text); 
       
        $item = $dom->createElement("發文日期"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("年月日"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(date('Y')-1911 .'年'. date('m').'月'.date('d').'日'); 
        $item2->appendChild($text); 
        
        $item = $dom->createElement("發文字號"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("北市警刑毒緝"); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("文號"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('年度'); 
        $text = $dom->createTextNode(date('Y')-1911); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        $item3 = $dom->createElement('流水號'); 
        $text = $dom->createTextNode('3002347'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        $item3 = $dom->createElement('支號'); 
        $text = $dom->createTextNode('2'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
          
        $item = $dom->createElement("附件"); 
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement("主旨"); 
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $text = $dom->createTextNode('公示送達應受送達人'.$su[0]->s_name.' 等'. $count . '人違反「毒品危害防制條例」第11條之1案件罰鍰逾期未繳納，催繳案應受送達人名冊'.$count.'份。'); 
        $item3->appendChild($text); 
        $item2->appendChild($item3);
        
        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "依據：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode(""); 
        $item2->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "一、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("毒品危害防制條例第11條之1。"); 
        $item3->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "二、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("行政程序法第78、81條。"); 
        $item3->appendChild($text); 

        $item = $dom->createElement("段落"); 
        $item->setAttribute("段名", "公告事項：");  
        $root->appendChild($item); 
        $item2 = $dom->createElement("文字"); 
        $item->appendChild($item2); 
        $text = $dom->createTextNode("");
        $item2->appendChild($text); 

        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "一、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("催繳函應受送達人".substr($suspect,0,-3)."等".$count."人違反毒品危害防制條例之處分書罰鍰催繳案，繳納罰鍰請至各金融機構填具匯款單匯款，行庫：台北富邦銀行公庫處，帳號：16112470361019，戶名：我是測試系統刑事警察大隊，(匯款時，應於備註欄輸入受處分人之姓名、身分證統一編號)。");
        $item3->appendChild($text); 
        
        $item2 = $dom->createElement('條列'); 
        $item2->setAttribute("序號", "二、");  
        $item->appendChild($item2); 
        $item3 = $dom->createElement('文字'); 
        $item2->appendChild($item3); 
        $text = $dom->createTextNode("上述名冊所列受處分人，自公示送達生效(刊登市府公報滿20日)仍不依法完納前項案件罰鍰者，將依法移請法務部行政執行署各地分署強制執行。"); 
        $item3->appendChild($text); 
        echo $dom->saveXML();  
        $this->load->helper('download');
        force_download($datet.'.di', $dom->saveXML());
        redirect('Surcharges/listProjectNormal/'.$id); 
    }
    
}