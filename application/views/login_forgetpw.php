
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel " style="background:none !important;">
                    <?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'panel-title',
                            'width' => '180',
                            'height' => '180',
                            'style' => 'display:block; margin:auto;box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;border-radius: 100%;',
                                    );     
                                ?>
                        <?php echo img($image_properties); ?>
                        <h4 class="text-center" style="color:#fff;letter-spacing:5px;">忘記密碼</h4>
                        <div class="panel-body">
                            <form action=<?php echo base_url("login/changepw_forget") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <fieldset>
                                    <div class="form-group">
                                        <small style="color: rgba(255,255,255,0.7);">登入類型</small>
                                        <select name="login_type" id="sel" class="form-control">
                                            <option value="0">ID登入</option>
                                            <option value="1">身份證登入</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input name='uid' class="form-control" placeholder="請輸入賬號" required>
                                    </div>
                                    <div class="form-group">
                                        <input name='u_pw' class="form-control" placeholder="請輸入新密碼" id="pw1" name="password" type="password" value="">
                                    </div>
                                    <div class="form-group">
                                        <input name='u_pw' class="form-control" placeholder="確認新密碼" required name="password" type="password" id="pw2" onkeyup="validate()" value=""><span id="tishi"></span>
                                    </div>
                                    <div class="form-group">
                                        <input name='u_changecode' class="form-control" placeholder="請輸入認證碼" required>
                                    </div>
                                    <div style='color:red;'><label>需先取得驗證碼，若無驗證碼請點擊<a href="<?php echo base_url("login/getcode")?>" target="_blank">取得驗證碼</a></label></div>
                                    <div style='color:red;'><?php echo $error ?></div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" id="submit" value="修改">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
    body{
        /* background: linear-gradient(to left, #13547a 0%, #80d0c7 100%); */
        background-image: linear-gradient(to left, #29323c 0%, #485563 100%);
    }
    #sel {
        background: none;
        color: #fff;
        border: none;
        border-bottom: 1px solid #eee;
        border-radius: 0px;
        box-shadow: inset 0 5px 5px rgb(0 0 0 / 8%);
    }
    input.form-control {
        background: rgba(255,255,255,0.1);
        color: #fff;
        border: none;
        border-bottom: 1px solid #eee;
        border-radius: 0px;
        box-shadow: none;
        height: 40px;
        font-size: 14px;
        line-height: 20px;
    }
    input[type="submit"] {
        background: rgba(255,255,255,0.1);
        color: #fff;
        border: none;
        font-size: 14px;
        border-radius:0px;
        height: 40px;
    }
    input[type="submit"]:hover {
        background-color: rgba(255,255,255,1);
        color:#333;
    }
</style>
<script>
function validate() {
    var pw1 = document.getElementById("pw1").value;
    var pw2 = document.getElementById("pw2").value;
    if(pw1 == pw2) {
        document.getElementById("tishi").innerHTML="<font color='green'>兩次密碼相同</font>";
        document.getElementById("submit").disabled = false;
    }
    else {
        document.getElementById("tishi").innerHTML="<font color='red'>兩次密碼不相同</font>";
        document.getElementById("submit").disabled = true;
    }
}
</script>