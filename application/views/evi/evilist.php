        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><button id='yes' class="btn btn-default"style="padding:0px 0px;" >確認修改</button></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
					<blockquote style="margin-top:35px;letter-spacing:5px;">
						<div class="row">
							<div class="col-md-6">
								<p><?php echo $title; ?></p>
							</div>
							<div class="col-md-6 text-right">
								<button id='yes' class="btn btn-warning">確認修改</button>
							</div>
						</div>
                        
                    </blockquote>
                    <div class="row">
                        <div class="col-lg-12">
							<ul class="nav nav-pills e-state-tab" role="tablist">
								<li role="presentation" class="active"><a href="#keep" aria-controls="keep" role="tab" data-toggle="tab">單位保管中</a></li>
								<li role="presentation" ><a href="#process" aria-controls="process" role="tab" data-toggle="tab">送驗中</a></li>
								<!-- <li role="presentation"><a href="#prison" aria-controls="prison" role="tab" data-toggle="tab">在監</a></li> -->
								<li role="presentation"><a href="#check" aria-controls="check" role="tab" data-toggle="tab">送地檢署(法院)</a></li>
								<li role="presentation"><a href="#inlocal" aria-controls="inlocal" role="tab" data-toggle="tab">單位保管中【已驗】</a></li>
								<!-- <li role="presentation"><a href="#indrug" aria-controls="indrug" role="tab" data-toggle="tab">送警察局</a></li> -->
								<li role="presentation"><a href="#receive" aria-controls="receive" role="tab" data-toggle="tab">警局沒入</a></li>
								<li role="presentation"><a href="#clean" aria-controls="clean" role="tab" data-toggle="tab">已銷毀</a></li>
							</ul>
							<div class="tab-content" style="margin-top:15px;">
								<div role="tabpanel" class="tab-pane active" id="keep">
									<div class="panel panel-primary">
											<div class="panel-heading">列表清單</div>
											<div class="panel-body">
												<div class="table-responsive">
													<?php echo $keep_table;?>
												</div>                       
											</div>
											<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
											<input id="enum" type="hidden" name="enum" value=''> 
											<input id="s_status" type="hidden" name="s_status" value=''> 
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="process">
									<div class="panel panel-primary">
											<div class="panel-heading">列表清單</div>
											<div class="panel-body">
												<div class="table-responsive">
													<?php echo $process_table;?>
												</div>                       
											</div>
											<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
											<input id="enum" type="hidden" name="enum" value=''> 
											<input id="s_status" type="hidden" name="s_status" value=''> 
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="check">
									<div class="panel panel-primary">
											<div class="panel-heading">列表清單</div>
											<div class="panel-body">
												<div class="table-responsive">
													<?php echo $check_table;?>
												</div>                       
											</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="inlocal">
									<div class="panel panel-primary">
											<div class="panel-heading">列表清單</div>
											<div class="panel-body">
												<div class="table-responsive">
													<?php echo $inlocal_table;?>
												</div>                       
											</div>
									</div>
								</div>
								<!-- <div role="tabpanel" class="tab-pane" id="indrug">
									<div class="panel panel-primary">
											<div class="panel-heading">
												<div class="row">
													<div class="col-md-6">
														警局沒入物室
													</div>
													<div class="col-md-6 text-right">
														<button class="btn btn-default" data-toggle="modal" data-target="#downloadModal">入庫清冊</button>
													</div>
													<div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="downloadModalLabel">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																<h4 class="modal-title text-muted" id="downloadModalLabel">下載入庫清冊</h4>
															</div>
															<div class="modal-body">
																<form class="" id="searchForm" name="spreadsheet" >
																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group">
																				<label>請選擇時間起訖</label>
																				<div class="input-group date">
																					<span class="input-group-addon">起始時間</span>
																					<input type="text" class="form-control" id="startdate" name="startdate" aria-describedby="basic-addon3">
																					<span class="input-group-addon">結束時間</span>
																					<input type="text" class="form-control" id="enddate" name="enddate" aria-describedby="basic-addon3">
																				</div>                                           
																			</div>  
																														
																		</div>                                            
																	</div>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
																<button type="button" class="btn btn-primary" onclick="loadDownloadLink();">確定下載</button>
															</div>
															</div>
														</div>
													</div>
												</div>	
											</div>
											<div class="panel-body">
												<div class="table-responsive">
													<?php //echo $indrug_table;?>
												</div>                       
											</div>
									</div>
								</div> -->
								<div role="tabpanel" class="tab-pane" id="receive">
									<div class="panel panel-primary">
											<div class="panel-heading">列表清單</div>
											<div class="panel-body">
												<div class="table-responsive">
													<?php echo $receive_table;?>
												</div>                       
											</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="clean">
									<div class="panel panel-primary">
											<div class="panel-heading">列表清單</div>
											<div class="panel-body">
												<div class="table-responsive">
													<?php echo $clean_table;?>
												</div>                       
											</div>
									</div>
								</div>
							</div>
                            
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
			$("#startdate, #enddate").datepicker(
                {
                    todayHighlight: true,
                    format: "yyyy-mm-dd",
                    locale: {
                        format: 'YYYY-MM-DD'
                    }
                } 
            );
            now = new Date();
            firsthdate = now.getFullYear() + '-' + (((now.getMonth()+1) > 9)?(now.getMonth()+1):('0' + (now.getMonth()+1).toString())) + '-01';
            today = now.getFullYear() + '-' + (((now.getMonth()+1) > 9)?(now.getMonth()+1):('0' + (now.getMonth()+1).toString())) + '-' +(((now.getDate()) > 9)?(now.getDate()):('0' + (now.getDate()).toString()));
            $("#startdate").val(firsthdate);
            $("#enddate").val(today);
			
			chooseAry1 = [];
			chooseAry2 = [];
			chooseAry3 = [];	
			currenttab = "";
			url = location.href;
			var url = location.href;

			if(url.indexOf('#')!=-1)
			{ 
				currenttab = url.split('#')[1];
				
			}
			if(currenttab != '')
			{
				$(`.e-state-tab a[href="#${currenttab}"]`).tab('show');
			}
			drug_table = $('.drug-table').DataTable({
					columnDefs: [ {
							targets:   0,
							visible: true
					},{
							className: 'select-checkbox',
							targets:   0,
					} ],
					select: {
							style:    'os',
							selector: 'td:first-child',
							style: 'multi'
					},
					"fnRowCallback": function (nRow, aData, iDisplayIndex) {
							$(nRow).attr("data-id", aData[2]);
							return nRow;
					},
				'order': [[1, 'asc']],
				"language": {
							"processing": "資料載入中...",
							"lengthMenu": "每頁顯示 _MENU_ 筆",
							"zeroRecords": "資料庫中未有相關資料。",
							"info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
							"infoEmpty": "資料庫中未有相關資料。",
							"search": "搜尋:",
							"paginate": {
								"first": "第一頁",
								"last": "最後一頁",
								"next": "下一頁",
								"previous": "上一頁"
							}
						}
			});
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

				  $("#yes").click(function (){
				$(".drug-table tr.selected").each(function(){
					if($(this).children('td:eq(8)').find('select').val() == '送地檢')
					{
						if($(this).children('td:eq(8)').find('input').val() == '')
						{
							alert('需上傳地檢入庫清單掃描檔');
							return false;
						}
						else
						{
							let formdata = new FormData();
							formdata.append('eid', $(this).attr("data-id"));
							formdata.append('estatus', $(this).children('td:eq(8)').find('select').val());
							if($(this).children('td:eq(8)').find('input[type="file"]').length > 0)
							{
								formdata.append('estatusdoc', $(this).children('td:eq(8)').find('input')[0].files[0]);
							}
							
							formdata.append('eplace', $(this).children('td:eq(9)').find('select').val());
							$.ajax({
								type: 'POST',
								url: '<?php echo base_url("drug_evi_fin/updateDrugStatus") ?>',
								dataType: 'json',
								// data: {'addr': obj.val()},
								data: formdata,
								processData: false,
								contentType: false,
								cache: false,
								complete: function (resp) {
									// $('.drug-table').DataTable().ajax.reload(null, false);
									location.reload();
								}
							});
						}
					}
					else
					{
						let formdata = new FormData();
							formdata.append('eid', $(this).attr("data-id"));
							formdata.append('estatus', $(this).children('td:eq(8)').find('select').val());
							formdata.append('eplace', $(this).children('td:eq(9)').find('select').val());
							$.ajax({
								type: 'POST',
								url: '<?php echo base_url("drug_evi_fin/updateDrugStatus") ?>',
								dataType: 'json',
								// data: {'addr': obj.val()},
								data: formdata,
								processData: false,
								contentType: false,
								cache: false,
								complete: function (resp) {
									// $('.drug-table').DataTable().ajax.reload(null, false);
									location.reload();
								}
							});
					}
					
				});
                // $("#s_status").val('1');
                //     //alert("Submitted");
                // $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            // var rows_selected = table.column(0).checkboxes.selected();
            //     $('#s_cnum').val(rows_selected.join(","));
            //     $('input[name="id\[\]"]', form).remove();
            // var allrows= table.column(1).data();
            //     $('#enum').val(allrows.join(","));
            //e.preventDefault();
           });
		   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				currenttab = e.currentTarget.hash // newly activated tab
				location.href = location.href.split('#')[0]+currenttab;
			})
    });
	function updateStatus(eid, obj)
	{
		obj.parent().find('input[type="file"]').remove();
		if(obj.val() == '送地檢')
		{
			obj.after('<input type="file" name="e_state_doc['+eid+']" accept=".pdf" required/>');
			obj.parent().parent().find('td:eq(9)').html(`<input type="text" class=" form-control" name="e_place['${eid}']" />`);
		}
		else if(obj.val() == '送驗中')
		{
			obj.parent().parent().find('td:eq(9)').html(`<select class="form-control" id="e_state" name="e_place['${eid}']" type="text" >
                        <option>航醫中心</option><option>刑事局</option><option>調查局</option><option  selected="selected">鑑識中心</option>
                        </select>`);
		}
		else
		{
			obj.parent().parent().find('td:eq(9)').html(`刑案證物室`);
		}
		
	}
	
	function loadDownloadLink(){
		location.href = '<?php echo base_url("PhpspreadsheetController/export_drugsreceive_list") ?>'+'?startdate='+$("#startdate").val()+'&enddate='+$("#enddate").val();
	}
    </script>
