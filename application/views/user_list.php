            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </br>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $data_table;?>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            /*$(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });*/
            $(document).ready(function() {
              var table = $('#table1').DataTable({
                "lengthMenu": [
                          [20,-1],    // 具体的数量
                          [20,"全部"] // 文字描述
                      ],
                      "paging": true,    // 是否开启分页功能(默认开启)
                      'info': true,      // 是否显示分页的统计信息(默认开启)
                      "searching":true,  // 是否开启搜索功能(默认开启)
                      "ordering": true,  // 是否开启排序功能(默认开启)
                      "order":[ [0,'asc'] ], // 设置默认排序的表格列[参数1是表格列的下标，从0开始]
                      "stateSave": true,      // 是否保存当前datatables的状态(刷新后当前保持状态)
                      "processing": true,     // 显示处理中的字样[数量多的时候提示用户在处理中](默认开启)
                      "serverSide": true,    // 是否开启服务器模式
                                              // false时，会一次性查询所有的数据，dataTables帮我们完成分页等。
                                              // true时，点击分页页码就会每次都到后台提取数据。   
                });
            });
        </script>