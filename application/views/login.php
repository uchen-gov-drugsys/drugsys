
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel" style="background:none !important;">
                    <?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'class' => 'panel-title',
                            'width' => '180',
                            'height' => '180',
                            'style' => 'display:block; margin:auto;box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;border-radius: 100%;',
                                    );     
                                ?>
                        <?php echo img($image_properties); ?>
                        <!-- <div class="panel-heading">
                            <h3 style="text-align: center;" class="panel-title">請登入</h3>
                        </div> -->
                        <div class="panel-body">
                            <form action=<?php echo base_url("login/checklogin") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <fieldset>
                                    <div class="form-group">
                                        <small style="color: rgba(255,255,255,0.7);">登入類型</small>
                                        <select name="login_type" id="sel" class="form-control">
                                            <option value="0">ID登入</option>
                                            <option value="1">身份證登入</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input name='uid' class="form-control" placeholder="請輸入賬號" required>
                                    </div>
                                    <div class="form-group">
                                        <input name='u_pw' class="form-control" placeholder="請輸入密碼" name="password" type="password" value="">
                                    </div>
                                    <div style='color:red;'><?php echo $error ?></div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input class="btn btn-lg btn-block" type="submit" name="submit" value="登入" style="">
                                    <a href="<?php echo base_url("login/forgetpw")?>" class="btn btn-link pull-right">忘記密碼</a>
                                    <!--a href="<?php echo base_url("login/showalluser")?>" class="btn btn-lg btn-warning">所有賬號(測試分頁)</a-->
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<style>
    body{
        /* background: linear-gradient(to left, #13547a 0%, #80d0c7 100%); */
        background-image: linear-gradient(to left, #29323c 0%, #485563 100%);
    }
    #sel {
        background: none;
        color: #fff;
        border: none;
        border-bottom: 1px solid #eee;
        border-radius: 0px;
        box-shadow: inset 0 5px 5px rgb(0 0 0 / 8%);
    }
    input.form-control {
        background: rgba(255,255,255,0.1);
        color: #fff;
        border: none;
        border-bottom: 1px solid #eee;
        border-radius: 0px;
        box-shadow: none;
        height: 60px;
        font-size: 16px;
        line-height: 20px;
    }
    input[type="submit"] {
        background: rgba(255,255,255,0.1);
        color: #fff;
        border: none;
        font-size: 14px;
        border-radius:0px;
        height: 60px;
    }
    input[type="submit"]:hover {
        background-color: rgba(255,255,255,1);
        color:#333;
    }
</style>
