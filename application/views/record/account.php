        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="newcases" id="list"></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            </br>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <?php echo form_open_multipart('record/updateadmin','id="newcases"') ?>
                                                <table class="table">
                                                    <tbody class="form-group">
                                                        <tr>
                                                            <td>
                                                                <label>修改3階權限(email: <?php echo $uic; ?> ):</label>
                                                                <label class="checkbox-inline"><input type="checkbox" name="CheckAll" value="核取方塊" id="CheckAll" />全選</label>
                                                                <label class="checkbox-inline"><input type="checkbox" name="em_3permit[]" value="a" <?php if(preg_match("/a/i", $permit)){echo 'checked';}?> />處分書 </label>
                                                                <label class="checkbox-inline"><input type="checkbox" name="em_3permit[]" value="b" <?php if(preg_match("/b/i", $permit)){echo 'checked';}?> />怠金處分書 </label>
                                                                <label class="checkbox-inline"><input type="checkbox" name="em_3permit[]" value="c" <?php if(preg_match("/c/i", $permit)){echo 'checked';}?> />移送 </label>
                                                                <label class="checkbox-inline"><input type="checkbox" name="em_3permit[]" value="d" <?php if(preg_match("/d/i", $permit)){echo 'checked';}?> />帳務</label>
                                                                <label class="checkbox-inline"><input type="checkbox" name="em_3permit[]" value="e" <?php if(preg_match("/e/i", $permit)){echo 'checked';}?> />獎金</label>
                                                                <label class="checkbox-inline"><input type="checkbox" name="em_3permit[]" value="f" <?php if(preg_match("/f/i", $permit)){echo 'checked';}?> />證物 </label>
                                                                <label class="checkbox-inline"><input id="nopermit" type="checkbox" name="em_3permit[]" value="g" <?php if(preg_match("/g/i", $permit)){echo 'checked';}?> />一般三階(查詢) </label>
                                                            </td>
                                                            <input id="uid" type="hidden" name="uid" value=<?php echo $uic;?>> 
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            <?php// echo form_submit('', 'newcases');
                                            echo form_close();?>                                  
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
            $(document).ready(function(){
                    if($("#nopermit").prop("checked")){
                    $("input[name='em_3permit[]']").each(function(){
                        $(this).prop("checked",false);
                        $(this).attr("disabled",true);
                        $('#nopermit').prop('checked', true);
                        $('#nopermit').attr("disabled",false);
                    })
                    }else{
                    $("input[name='em_3permit[]']").each(function(){
                        $(this).prop("checked",false);
                        $(this).attr("disabled",false);
                        //$('#nopermit').attr("disabled",true);
                    })
                   }
                $("#CheckAll").click(function(){
                    if($("#CheckAll").prop("checked")){
                    $("input[name='em_3permit[]']").each(function(){
                        $(this).prop("checked",true);
                        $(this).attr("disabled",false);
                        $('#nopermit').prop('checked', false);
                    })
                    }else{
                    $("input[name='em_3permit[]']").each(function(){
                        $(this).prop("checked",false);
                    })
                   }
                })      
                $("#Other").click(function(){
                    if($("#Other").prop("checked")){
                    $("input[name='em_3permit[]']").each(function(){
                        $('#nopermit').prop('checked', false);
                    })
                    }
                })      
                $("#nopermit").click(function(){
                    if($("#nopermit").prop("checked")){
                    $("input[name='em_3permit[]']").each(function(){
                        $(this).prop("checked",false);
                        $(this).attr("disabled",true);
                        $('#nopermit').prop('checked', true);
                        $('#nopermit').attr("disabled",false);
                    })
                    }else{
                    $("input[name='em_3permit[]']").each(function(){
                        $(this).prop("checked",false);
                        $(this).attr("disabled",false);
                        //$('#nopermit').attr("disabled",true);
                    })
                   }
                })      
            });            
        </script>
    </body>
</html>
