        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <!--li><a href="http://localhost/main2/drug_evi_fin/exportDrugCSV"><button class="btn btn-default"style="padding:0px 0px;">下載所有日誌csv</button></a></li-->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                            <div class="panel panel-default">
                                
                            <?php echo form_open_multipart('cases2/editevistatus','id="sp_checkbox"') ?>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="example" class="display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>受處分人姓名</th>
                                                        <th>身份證</th>
                                                        <th>性別</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th>受處分人姓名</th>
                                                        <th>身份證</th>
                                                        <th>性別</th>
                                                    </tr>
                                                </tfoot>
                                            </table>         
                                        </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="enum" type="hidden" name="enum" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                        </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
            $(document).ready(function() {
                $('#example').DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "<?php echo base_url('record/testajax') ?>"
                } );
            } );
        </script>
