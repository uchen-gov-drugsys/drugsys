        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                            <div class="panel panel-default">
                                
                            <?php echo form_open_multipart('Cases/editAdmin','id="sp_checkbox"') ?>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="enum" type="hidden" name="enum" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                        </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
        <div class="modal large fade" id="login_hire" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>修改二/三階權限</h3>
              </div>
              <div class="modal-body">
                <form method="post" onSubmit="javascript:void(0)">
                  <label class="title">證號*</label>
                  <input type="text" id="id" required value="">
                  <label class="title">帳號</label>
                  <input type="text" id="email" readonly value="">
                  <div class="controls controls-row">
                  <label class="title">姓*</label>
                  <input type="text" id="lastname" required value="">
                  <label class="title">名*</label>
                  <input type="text" id="firstname" required value="">
                  </div>
                  <div class="controls controls-row">
                    <label class="title">內外部異動*</label>
                  </div>
                  <div class="controls controls-row">
                    <input type="submit" onClick="login(); return false;" value="Click Here to LogIn" class="btn" />
                </form>
              </div>
              <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
              </div>
            </div>
          </div>
        </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function(){
            $('#table1').DataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                "order": [[ 6, "desc" ]],
                //"search": {
                //    "search": "科技犯罪偵查隊"
                //},
                'ajax': {
                  'url':'<?php echo base_url("Record/testjson")?>'
                },
                "language": //把文字變為中文
                {  
                    "sProcessing": "處理中...",  
                    "sLengthMenu": "顯示 _MENU_ 項結果",  
                    "sZeroRecords": "沒有匹配結果",  
                    "sInfo": "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",  
                    "sInfoEmpty": "顯示第 0 至 0 項結果，共 0 項",  
                    "sInfoFiltered": "(由 _MAX_ 項結果過濾)",  
                    "sInfoPostFix": "",  
                    "sSearch": "搜索:",  
                    "sUrl": "",  
                    "sEmptyTable": "表中數據為空",  
                    "sLoadingRecords": "載入中...",  
                    "sInfoThousands": ",",  
                    "oPaginate": {  
                        "sFirst": "首頁",  
                        "sPrevious": "上頁",  
                        "sNext": "下頁",  
                        "sLast": "末頁"  
                    },  
                    "oAria": {  
                        "sSortAscending": ": 以升序排列此列",  
                        "sSortDescending": ": 以降序排列此列"  
                    }  
                },
                'columns': [
                    { data: 'em_mailId' },
                    { data: 'em_lastname' },
                    { data: 'em_office' },
                    { data: 'em_job' },
                    { data: 'em_roffice' },
                    { data: 'em_IsAdmin' },
                    { data: 'em_3permit' },
                    { data: 'em_change_code' }
                ]
            });
            
            $('body').on("click",".callModal", function() {//用於抓取
                var job_id = $(this).data('job-id');
                $('#job').val( job_id );//職稱
                var e_id = $(this).data('e-id');
                $('#id').val( e_id );//證號
                var ln_id = $(this).data('ln-id');
                $('#lastname').val( ln_id );
                var fn_id = $(this).data('fn-id');
                $('#firstname').val( fn_id );
                var email_id = $(this).data('email-id');
                $('#email').val( email_id );
            });
        });        
    </script>
