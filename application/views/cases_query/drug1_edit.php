        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>

            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    
                    <div class="row" style="margin-top:35px;">
                        <div class="col-md-6" style="letter-spacing:5px;">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?php echo base_url('cases/edit/'.$e_c_num)?>" class="btn btn-default" >返回</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    毒品修改
                                </div>
                                <div class="panel-body">
                                
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <?php echo form_open_multipart('cases/update_drug1','id="newdrug1"') ?>
                                                <div class="row" id="drugall">
                                                    <div class="form-group col-md-2">
                                                         <label>證物號</label>
                                                        <?php 
                                                            echo form_input('e_id',$e_id, 'class="form-control" id="cnum" disabled');                                                        
                                                            echo form_hidden('e_c_num',$e_c_num);                                                        
                                                        ?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>品名:</label>
                                                        <?php echo form_dropdown('e_name',$nameopt , $drugAll->e_name , 'class="form-control" id="sel" disabled')?>  
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>數量</label>
                                                        <?php 
                                                            echo form_input('e_count',$drugAll->e_count, 'class="form-control" id="e_count" disabled');                                                        
                                                        ?>   
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>單位</label>
                                                        <?php echo form_dropdown('e_type',$countopt , $drugAll->e_type , 'class="form-control" id="sel" disabled')?>    
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>初驗淨重</label>
                                                        <?php 
                                                            echo form_input('e_1_N_W',$drugAll->e_1_N_W, 'class="form-control" id="e_1_N_W" disabled');                                                        
                                                        ?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>持有人</label>
                                                        <?php echo form_dropdown('e_s_ic',$test , $drugAll->e_s_ic , 'class="form-control" disabled')?> 
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                    <label>初驗毒品成分</label>
                                                        <div id="drug_s"></div>
                                                        <!--label><button type="button" name="add" id="add" class="btn btn-success">新增成分</button></label-->
                                                        <?php 
                                                        foreach ($drugck as $drugck)
                                                        {
                                                            echo '<div id="row">';
                                                            echo form_dropdown('df[]',$nwopt , $drugck->df_ingredient , 'class="form-control" id="sel" disabled');
                                                            echo anchor('cases/deleteCk/'. $drugck->df_num,'X','class="btn btn-danger btn_remove" disabled ');
                                                            //echo '<button type="button" class="btn btn-danger btn_remove">X</button></';
                                                            echo '</div>';
                                                        }   
                                                        ?> 
                                                        
                                                    </div>
                                                </div>
                                            <?php 
                                            echo form_close();?>                                  
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <script type="text/javascript">
                $(document).ready(function(){      
                    var i=1;  
                    $('#add').click(function(){  
                       i++;  
                       $('#drug_s').append('<div id="row'+i+'">'+
                        '<select name="df[]" class="form-control"><option>海洛因</option><option>安非他命</option>'+
                        '<option>搖頭丸(MDMA)</option><option>愷他命(KET)</option><option>一粒眠</option>'+
                        '<option>大麻</option><option>卡西酮類</option><option>古柯鹼</option><option>無毒品反應</option>'+
                        '</select><button type="button" name="remove" id="'+i+'"'+
                        'class="btn btn-danger btn_remove">X</button></div>');  
                    });
                    $(document).on('click', '.btn_remove', function(){  
                       var button_id = $(this).attr("id");   
                       $('#row'+button_id+'').remove();  
                    });  
                $( "#newdrug1" ).validate({
                    rules: {
                        e_1_N_W: {
                            //required: true,
                            digits: true,
                            maxlength: 6
                        },
                        e_s_ic: {
                            required: true,
                        },
                    },
                    messages: {
                        e_1_N_W: {
                            //required: "此欄位不得為空",
                            digits: "請輸入數字",
                            maxlength: "請輸入合理重量",
                        },
                        e_s_ic: {
                            required: "請選擇犯嫌人",
                        },
                    }
                });        
                });  
</script>
