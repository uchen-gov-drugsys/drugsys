        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><button id='yes' class="btn btn-default"style="padding:0px 0px;" form="sp_checkbox" >修改</button></a></li>
                    <li><a><button class="btn btn-default"style="padding:0px 0px;" data-toggle="modal" data-target="#exampleModal">批次上傳</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							<button id='yes' class="btn btn-warning" form="sp_checkbox" >修改</button>
                            
							<button class="btn btn-default" data-toggle="modal" data-target="#exampleModal">批次上傳</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
								<div class="panel-heading list_view">
									<h4 class="panel-title">列表清單(寄送中...)</h4>
                                </div>
								<form action= <?php echo base_url('disciplinary_c/updatedelivery')?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
									<div class="panel-body">
										<div class="table-responsive">
											<?php echo $s_table;?>
										</div>                       
									</div>
									
									<input id="s_cnum" type='hidden' name="s_cnum" value=''> 
									<input id="s_cnum1" type='hidden' name="s_cnum1" value=''> 
									<input id="s_status" type='hidden' name="s_status" value=''> 
                            	</form>   
                           </div>
                            <!-- /.panel -->
                        </div>
						<div class="col-lg-12">
                            <div class="panel panel-success">
								<div class="panel-heading list_view">
									<h4 class="panel-title">已送達</h4>
                                </div>
                           
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_success_table;?>
                                    </div>                       
                                </div>
                           </div>
                            <!-- /.panel -->
                        </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <form action= <?php echo base_url('disciplinary_c/uploaddeliverydoc') ?>  id="uploaddeliverydoc" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">上傳送達證書</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          <div class="form-group">
                                            <label>上傳送達證書(可多筆上傳)</label>
                                            <?php echo form_upload('files[]','','multiple'); ?>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button id='no' class="btn btn-default" >確認上傳</button>
                                      </div>
                                    </div>
                                </div>
                                </form>
                            </div>         
                        <!-- /.col-lg-12 -->
                        <div class="col-lg-12">
                            <!-- <div class="panel panel-default">
                                <div class="panel-body">
                                    <a class="media" href="test.pdf"></a>    
                                </div>
                            </div> -->
                            <!-- /.panel -->
                        </div>                    
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
			$(".rcdate").change(function(){
				let strlen = $(this).val().length;
				if(strlen < 7 || strlen < "7")
				{
					if(strlen != 0)
						$(this).val("0"+$(this).val());
				}
				else
				{
					return false;
				}
					
			});
			$(".rcdate").keypress(function(){
				if($(this).val().length >= 7)
					return false;
			});  
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': 0,
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
            //   'select': {
            //      'style': 'multi'
            //   },
              "width":"100px",'order': [[0, 'asc']],
			  dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });

		   var table2 = $('#table2').DataTable({
              "width":"100px",'order': [[1, 'asc']],
			  dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
           
            /*$('#table1 tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
            } );*/
            
            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
            $("#yes1").click(function (){
                $("#s_status").val('2');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });

           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
            var form = this;

            var rows_selected = table.column(0).checkboxes.selected();
                $('#example-console-rows').text(rows_selected.join(","));
                $('#s_cnum').val(rows_selected.join(","));
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
              
           });
           
            $( "#uploaddeliverydoc" ).validate({
                    rules: {
                        'files[]': {
                            extension: "pdf"
                        },
                    },
                    messages: {
                        'files[]': {
                            extension: "只接受pdf類型",
                        },
                    }
                });        
        });
</script>
