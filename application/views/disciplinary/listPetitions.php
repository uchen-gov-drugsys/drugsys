        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><button id='yes' class="btn btn-default"style="padding:0px 0px;" form="sp_checkbox" >修改</button></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                            <div class="panel panel-default">
                                <div class="panel-heading list_view">
                                <form action=<?php echo base_url("Disciplinary_c/listPetitions")?> method="post">
                                    <div class="form-group">
                                        姓名：<input type="text" name="name"/>
                                        身份證字號：<input type="text" name="ic"/>
                                        處分書編號：<input type="text" name="fd_num"/><br>
                                        <!--選擇日期區間:<label><input  id="dater"  type="text"  name="datepicker"/></label>-->
                                        <button class='btn btn-default'>查詢</button>
                                    </div>
                                </form>
                                </div>
                            <?php echo form_open_multipart('Disciplinary_c/uploadPetitions','id="sp_checkbox"') ?>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php  echo $s_table;?>
                                        </tbody>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_num" type="hidden" name="s_num" value=''> 
                                <input id="c_num" type="hidden" name="c_num" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>舊專案</label>
                                        <?php echo form_dropdown('rp_num',$opt ,'', 'class="form-control" id="sel"')?>                                                            
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button id='no' class="btn btn-default" >加入舊專案</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                            </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
            $("#dater").daterangepicker(
            {
            //startDate: '2020-09-01',  
            locale: {
                  format: 'YYYY-MM-DD'
                }
            } 
            );
            var table = $('#table1').DataTable({
                'columnDefs': [
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']]
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
    });
    </script>
