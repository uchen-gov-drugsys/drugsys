        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h4><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h4></li>
                    <li><a><input type="submit" value="儲存" class="btn btn-default" style="padding:0px 0px;" form="newdrug1"></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <input type="submit" value="儲存" class="btn btn-warning" form="newdrug1">
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading list_view">
                                    編輯作業
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <?php echo form_open_multipart('disciplinary_c/update_drug2','id="newdrug1"') ?>
                                            <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <label>證物號</label>
                                                        <?php 
                                                            echo form_input('e_id',$e_id, 'class="form-control" readonly');                                                        
                                                            echo form_hidden('e_c_num',$e_c_num);                                                        
                                                            echo form_hidden('e_s_ic',$e_s_ic);                                                        
                                                        ?>                    
                                                        <input id="link" type="hidden" name="link" value=" ">  
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>品名:</label>
                                                        <?php echo form_input('e_name',$drugAll->e_name, 'class="form-control" id="sel"');//form_dropdown('e_name',$nameopt , $drugAll->e_name , 'class="form-control" id="sel"')?>   
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>數量</label>
                                                        <?php 
                                                            echo form_input('e_count',$drugAll->e_count, 'class="form-control" id="e_count"');                                                        
                                                        ?>                    
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>量數</label>
                                                        <?php echo form_dropdown('e_unit',$countopt , $drugAll->e_unit , 'class="form-control"')?>                 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>毒報</br></label>
                                                        <?php 
                                                        if($drugAll->e_doc == ""){
                                                            echo form_upload('e_doc');
                                                        }
                                                        else {
                                                            echo anchor_popup('drugdoc/' . $drugAll->e_doc, '</br>毒報');
                                                            echo form_hidden('e_doc',$drugAll->e_doc);
                                                        }
                                                    ?> 
													<span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制10MB</small> </span>
                                                    </div>
                                            </div>
                                            <div class="row"  id="drugall">
                                                <div class="col-md-12">
                                                    <label>檢驗毒品級別成分</label>
                                                    <?php echo form_submit('', '新增成分', 'class="btn btn-default btn-sm" form="newdrug1" id="ddc"');?>
                                                </div>
                                                <hr>
                                                <div class="col-md-12">
                                                    
                                                    <?php 
													foreach ($drugck as $drugck2)
                                                    //foreach ($drugck2 as $drugck2)
                                                    {
														if($drugck2->df_type == '複驗')
														{
															echo '<div class="row">';
															echo '<div class="form-group col-md-2">';
															echo '<label>成份 <span class="text-danger">*</span></label>';
															echo form_dropdown('df2[]',$drug_opt , $drugck2->df_level .'級' .$drugck2->df_ingredient , 'class="form-control" id="sel"');
															echo form_hidden('df_num2[]',$drugck2->df_num); 
															echo '</div>';
															echo '<div class="form-group col-md-2">';
															echo '<label>淨重(g) <span class="text-danger">*</span></label>';
															echo form_input('df_PNW[]',$drugck2->df_PNW, 'class="form-control" required');              
															echo '</div>';         
															echo '<div class="form-group col-md-2">';                                 
															echo '<label>驗後餘重(g) <span class="text-danger">*</span></label>';
															echo form_input('df_NW[]',$drugck2->df_NW, 'class="form-control" required');            
															echo '</div>';          
															echo '<div class="form-group col-md-2">';                                       
															echo '<label>純質淨重(g)</label>';
															echo form_input('df_PNWS[]',$drugck2->df_PNWS, 'class="form-control"');   
															echo '</div>';     
															echo '<div class="form-group col-md-1"> <br/>';                                           
															echo anchor('cases2/deleteCk/'. $drugck2->df_num,'X','class="btn btn-danger btn_remove"');
															echo '</div>';    
															//echo '<button type="button" class="btn btn-danger btn_remove">X</button></';
															echo '</div>';
														}

                                                        // echo '<div class="form-group col-md-2" id="row">';
                                                        // echo form_dropdown('df2[]',$drug_opt , $drugck2->ddc_level .'級' .$drugck2->ddc_ingredient , 'class="form-control" id="sel"');
                                                        // echo form_hidden('ddc_num[]',$drugck2->ddc_num); 
                                                        // echo '<label>純質淨重(g)</label>';
                                                        // echo form_input('ddc_nw[]',$drugck2->ddc_nw, 'class="form-control "');                                                        
                                                        // echo anchor('disciplinary_c/deleteCk2/'. $drugck2->ddc_num,'X','class="btn btn-danger btn_remove btn-sm"');
                                                        //echo '<button type="button" class="btn btn-danger btn_remove">X</button></';
                                                        // echo '</div>';
                                                    }   
                                                    ?>   
                                                </div>
                                            </div>
                                            
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
                $(document).ready(function(){      
                    var i=1;  
                    $('#add').click(function(){  
                       i++;  
                       $('#drug_s').append('<div id="row'+i+'">'+
                        '<select name="df[]" class="form-control"><option>海洛因</option><option>安非他命</option>'+
                        '<option>搖頭丸(MDMA)</option><option>愷他命(KET)</option><option>一粒眠</option>'+
                        '<option>大麻</option><option>卡西酮類</option><option>古柯鹼</option><option>無毒品反應</option>'+
                        '</select><button type="button" name="remove" id="'+i+'"'+
                        'class="btn btn-danger btn_remove">X</button></div>');  
                    });
                    $(document).on('click', '.btn_remove', function(){  
                       var button_id = $(this).attr("id");   
                       $('#row'+button_id+'').remove();  
                    });  
                    $( "#newdrug1" ).validate({
                        rules: {
                            e_1_N_W: {
                                //required: true,
                                digits: true,
                                maxlength: 6
                            },
                            e_s_ic: {
                                required: true,
                            },
                            'df2[]': {
                                required: true,
                            },
                        },
                        messages: {
                            e_1_N_W: {
                                //required: "此欄位不得為空",
                                digits: "請輸入數字",
                                maxlength: "請輸入合理重量",
                            },
                            e_s_ic: {
                                required: "請選擇犯嫌人",
                            },
                        }
                    });        
                    $("#ddc").click(function (){
                        $("#link").val('1');
                        //alert($("#link").val());
                        $("#newdrug1").submit();
                    });        
                });  
    </script>
    </body>
</html>
