		<style>
			#drugListTable tbody tr.selected {
				background-color: #f5f5f5;
			}
		</style>
		<div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">
        <style>
            strong { 
                font-weight: bold;
                color:black;
            }
        </style>
		<?php 
			if (!$this -> session -> userdata('uic')){
				$this->output
						->set_status_header(403)
						->set_content_type('text/html')
						->set_output(file_get_contents( $this->load->view('403')))
						->_display();

						sleep(5);
						redirect('login/index','refresh');
				exit;
			}
			if($this -> session -> userdata('em_priority') != '3' ){
				
				redirect('login/logout','refresh');
				exit;
			}

			function isAudlt($birth, $catchdate){
				list($year,$month,$day) = explode("-",$birth);
				$nowyear = date("Y", strtotime($catchdate));
				$nowmonth = date("m", strtotime($catchdate));
				$nowday = date("d", strtotime($catchdate));
				$audlt = 1; // 成年

				if(($year+20) <= $nowyear)
				{
					if(($year+20) == $nowyear)
					{
						if($month*1 <= $nowmonth*1)
						{
							if($month*1 == $nowmonth*1)
							{
								$audlt = ($day > $nowday)?0:1;
							}
						}							
						else
							$audlt = 0;
					}
				}
				else
				{
					$audlt = 0;
				}

				return (($audlt==0)?false:true);
			}
		?>
                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><a href><?php //echo $title?></a></li>
                    <li><a href=<?php //echo base_url('PDFcreate/fine_doc/'.$sp->fd_snum)?>><button style="padding:0px 0px;" class="btn btn-default" >下載處分書</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" form="updateready">修改</button></a></li> -->
                    <?php //if($prev != NULL){?>
                        <!-- <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='prev'>上一個</button></a></li> -->
                    <?php //}else{ ?>
                        <!-- <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">第一筆</button></a></li> -->
                    <?php //} ?>
                    <?php //if($next != NULL){?>
                        <!-- <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='next'>下一個</button></a></li> -->
                    <?php //}else{ ?>
                        <!-- <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">最後一筆</button></a></li> -->
                    <?php //} ?>
                    <!-- <li><a href="<?php//echo base_url('Disciplinary_c/listdp1_ready/'.$id)?>"><button style="padding:0px 0px;" class="btn btn-default" >返回列表</button></a></li> -->
                    <!--<li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" >送批</button></a></li> 先拿掉 怕使用者沒確認修改完直接送批-->
                </ul>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href= <?php echo base_url("login/logout") ?>><i class="fa fa-sign-out fa-fw"></i> 登出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
                <!-- /.navbar-static-side -->
                <div id="page-wrapper" style="margin-left:20px;">
                <div class="container-fluid">
                    <div class="row"  style="margin-top:35px;">
                        <div class="col-md-3">
                            <ol class="breadcrumb">
                                <li><a href="<?php echo base_url("Disciplinary_c/listdp1_ready/". $dp->dp_num);?>">每日處理專案: <?php echo $id;?></a></li>
                                <li class="active"><?php echo $title;?></li>
                            </ol>
                        </div>
                        <div class="col-md-9 text-right">
                            
                            <a href="<?php echo base_url('Disciplinary_c/listdp1_ready/'.$dp->dp_num)?>"><button  class="btn btn-default" >返回列表</button></a>
                            <a href=<?php echo base_url('PDFcreate/fine_doc/'.$sp->fd_snum . '/'.$dp->dp_num)?>><button class="btn btn-default" >下載處分書</button></a>
                            <?php if($prev != NULL){?>
                                <a href="#"><button class="btn btn-link" id='prev'>上一個</button></a>
                            <?php }else{ ?>
                                <a href="#"><button class="btn btn-link">第一筆</button></a>
                            <?php } ?>
                            <?php if($next != NULL){?>
                                <a href="#"><button class="btn btn-link" id='next'>下一個</button></a>
                            <?php }else{ ?>
                                <a href="#"><button class="btn btn-link">最後一筆</button></a>
                            <?php } ?>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="letter-spacing:5px;">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <p class="page-header"></p>
                        </div>
                    </div> -->
                <!-- /.navbar-static-side -->
                    <form action="<?php echo base_url('disciplinary_c/updateready'); ?>" id="updateready" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                    <div class="row">
                        <div class="col-lg-4">
							<div class="row">
								<div class="col-md-12 text-right">
									<a href="#"><button  class="btn btn-warning btn-sm" form="updateready">修改</button></a>
								</div>
							</div>
							<hr>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingZero">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#acrroding" aria-expanded="true" aria-controls="collapseOne">
											依據
											</a>
											
										</h4>
                                    </div>
                                    <div id="acrroding" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingZero">
                                    <div class="panel-body">
                                        <div class="row">
												<div class="form-group col-md-12">
                                                    <label>依據單位</label>
                                                    <?php echo form_input('fd_acrrod_unit',((isset($sp->fd_acrrod_unit))?$sp->fd_acrrod_unit:(((mb_strlen($susp->s_roffice1) < 4)?'我是測試系統'.$susp->s_roffice :$susp->s_roffice))), 'class="form-control"')?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>依據日期</label>
                                                    <?php echo form_input('fd_acrrod_date',(isset($sp->fd_acrrod_date))?str_pad(((int)substr($sp->fd_acrrod_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($sp->fd_acrrod_date, 5, 2).substr($sp->fd_acrrod_date, 8, 2):((isset($susp->s_fdate))?((strlen($susp->s_fdate) > 7 && $susp->s_fdate != '0000-00-00')?str_pad(((int)substr($susp->s_fdate, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->s_fdate, 5, 2).substr($susp->s_fdate, 8, 2):''):''), 'class="form-control"')?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>依據單位字</label>
                                                    <?php echo form_input('fd_accrod_no',(isset($sp->fd_accrod_no))?$sp->fd_accrod_no:((mb_strlen($susp->s_roffice1) < 4)?'北市警'.$susp->s_roffice1.((strpos($susp->s_roffice, '分局') !== false)?'分刑':'') :$susp->s_roffice1), 'class="form-control"')?>
                                                </div>
												<div class="form-group col-md-12">
                                                    <label>依據號</label>
                                                    <?php echo form_input('fd_accrod_num',(isset($sp->fd_accrod_num))?$sp->fd_accrod_num:$susp->s_fno, 'class="form-control"')?>
                                                </div>
                                            </div>
                                            
                                    	</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#susp" aria-expanded="true" aria-controls="collapseOne">
											受處分人
											</a>
											
										</h4>
                                    </div>
                                    <div id="susp" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div class="row">
												<div class="form-group col-md-12">
                                                    <label>發文字號</label>
                                                    <?php echo form_input('fd_send_num',$sp->fd_send_num, 'class="form-control"')?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>受處分人姓名</label>
                                                    <?php echo form_input('s_name',$susp->s_name, 'class="form-control"')?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>身份證字號</label>
                                                    <?php echo form_input('s_ic',$susp->s_ic, 'class="form-control"')?>
                                                </div>
                                                
                                                <div class="form-group col-md-12">
                                                    <label>出生日期</label>
                                                    <input id="s_birth" name="s_birth" type="text" class="rcdate form-control" value="<?php echo (isset($susp->s_birth))?((strlen($susp->s_birth) > 7 && $susp->s_birth != '0000-00-00')?str_pad(((int)substr($susp->s_birth, 0, 4) - 1911),3,"0",STR_PAD_LEFT).substr($susp->s_birth, 5, 2).substr($susp->s_birth, 8, 2):''):'';?>">

                                                    <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>  
                                                    <?php echo form_hidden('s_num',$susp->s_num)?>
                                                    <?php echo form_hidden('s_cnum',$susp->s_cnum)?>
                                                    <?php echo form_hidden('s_dp_project',$dp->dp_num)?>                                             
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>聯絡電話</label>
                                                    <?php
                                                        if(isset($sp->fd_phone)) {
                                                            echo form_input('p_no',$sp->fd_phone, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_input('p_no','', 'class="form-control"');
                                                        }
                                                    ?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>在監狀況</label>
                                                    <?php echo form_dropdown('s_prison',$opt,$susp->s_prison,'class="form-control" onChange="copyAddress($(this))"') ?>
                                                </div>
                                            </div>
                                            <div class="row">
												<div class="form-group col-md-12">
													<label>郵遞區號</label>
													<?php echo form_input('fd_zipcode', $sp->fd_zipcode, 'class="form-control"'); ?>	
												</div>
                                                <div class="form-group col-md-12">
                                                        <div class="radio">
                                                            <label>
                                                                <input id="now" name="ad" type="radio" <?php echo (isset($susp->s_live_state) && $susp->s_live_state != 1)?'checked':''; ?> value="2">現居地地址(勾選是否送達)
                                                            </label>
                                                        </div>
                                                        <input id="s_rpaddress" class="form-control" placeholder="e.g.XX街/路XX號" name="s_rpaddress" value="<?php echo (!empty($susp->s_prison))?explode(':', $susp->s_prison)[0] .'('.explode(':', $susp->s_prison)[1].')' :$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress ?>" >
                                                        
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <div class="radio">
                                                        <label>
                                                            <input id="orgin" name="ad" type="radio" <?php echo ($susp->s_live_state == 1 || null == $susp->s_live_state)?'checked':''; ?> value="1">戶籍地址(勾選是否送達)
                                                        </label>
                                                    </div>
                                                    <input id="ori_rpaddress" class="form-control" placeholder="e.g.XX街/路XX號" name="s_dpaddress" value="<?php echo $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress ?>" >
                                                </div>
												<?php echo form_hidden('s_dpzipcode_addr', $susp->s_dpaddress); ?>
												<?php echo form_hidden('s_rpzipcode_addr', $susp->s_rpaddress); ?>
												<?php echo form_hidden('s_dpzipcode', $susp->s_dpzipcode); ?>
												<?php echo form_hidden('s_rpzipcode', $susp->s_rpzipcode); ?>
												<?php echo form_hidden('fd_address', $sp->fd_address); ?>
												
                                            </div>
                                    </div>
                                    </div>
                                </div>
                                <?php
                                    list($year,$month,$day) = explode("-",$susp->s_birth);
                                    $year_diff = date("Y") - $year;
                                    $month_diff = date("m") - $month;
                                    $day_diff  = date("d") - $day;
                                    if ($day_diff < 0 || $month_diff < 0)
                                        $year_diff--;

                                    if(!isAudlt($susp->s_birth, $cases->s_date)){
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#susp_fadi" aria-expanded="false" aria-controls="collapseTwo">
                                        法代人(未年滿20歲開啟)
                                        </a>
                                    </h4>
                                    </div>
                                    <div id="susp_fadi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>姓名</label>
                                                    <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_name',$suspfadai->s_fadai_name, 'class="form-control" id="s_fadai_name"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_name','', 'class="form-control" id="s_fadai_name"');
                                                        }?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>性別</label>
                                                    <?php 
                                                        $options = array(
                                                            '男'=> '男',
                                                            '女'=> '女',
                                                        );
                                                        if(isset($suspfadai)) {
                                                            echo form_dropdown('s_fadai_gender',$options,$suspfadai->s_fadai_gender, 'class="form-control" id="s_fadai_gender"');
                                                        }
                                                             else {
                                                                 echo form_dropdown('s_fadai_gender',$options,'', 'class="form-control" id="s_fadai_gender"');
                                                        }?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>身份證字號</label>
                                                    <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_ic',$suspfadai->s_fadai_ic, 'class="form-control" id="s_fadai_ic"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_ic','', 'class="form-control" id="s_fadai_ic"');
                                                        }?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>出生日期</label>
                                                    <input id="s_fadai_birth" name="s_fadai_birth" type="text" class="rcdate form-control" value="<?php echo (isset($suspfadai->s_fadai_birth))?((strlen($suspfadai->s_fadai_birth) > 7 && $suspfadai->s_fadai_birth != '0000-00-00')?str_pad(((int)substr($suspfadai->s_fadai_birth, 0, 4) - 1911),3,"0",STR_PAD_LEFT).substr($suspfadai->s_fadai_birth, 5, 2).substr($suspfadai->s_fadai_birth, 8, 2):''):'';?>">
                                                    <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>     
                                                    
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>聯絡電話</label>
                                                    <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_phone',$suspfadai->s_fadai_phone, 'class="form-control" id="s_fadai_phone"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_phone','', 'class="form-control" id="s_fadai_phone"');
                                                        }?>
                                                </div>                                                
                                                <div class="form-group col-md-12">
                                                    <label>地址</label>
                                                    <div id="zipcode3"></div>
                                                        <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_address',$suspfadai->s_fadai_address, 'class="form-control" id="s_fadai_address" placeholder="e.g.XX路XX門牌"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_address',$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress, 'class="form-control" id="s_fadai_address" placeholder="e.g.XX路XX門牌"');
                                                        }?>
                                                </div>

                                            </div>
                                    </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#title" aria-expanded="false" aria-controls="collapseThree">
                                        主旨
                                        </a>
                                    </h4>
                                    </div>
                                    <div id="title" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-md-12 hidden">
                                                <label>是否要罰</label>
                                                <input type="radio" name="fd_wantdis" id="optionsRadios1" value="是" checked>是
                                                <input type="radio" name="fd_wantdis" id="optionsRadios2" value="否">否
                                            </div>
											<div class="col-md-12">
												<blockquote>
													<p class="text-primary">裁罰資料</p>
												</blockquote>
											</div>											
                                            <div class="form-group col-md-12">
												<div class="row">
													<div class="col-md-6">
														<label>第幾次裁罰 </label> 
													</div>
													
												</div>
                                                <select id="seldate" class="form-control" name="seltimes">
													<option value="-1" selected>請選擇</option>
													<option value="1">第一次</option>
													<option value="2">第二次</option>
													<option value="3">第三次</option>
													<option value="4">第四次</option>
													<option value="5">第五次</option>
													<option value="6">第六次</option>
													<option value="7">第七次</option>
													<!-- <option value="2">第七次</option> -->
												</select>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>裁罰金額</label>
                                                <input class="form-control" id="pmoney" name="f_damount" value="<?php echo $fine->fd_amount ?>">
                                            </div>
											<div class="col-md-12">
												<blockquote>
													<p class="text-primary">毒尿檢資料</p>
												</blockquote>
											</div>											
											<div class="col-md-12 text-left">
												<label>
													<input type="checkbox" id="fd_merge" name="fd_merge" <?php echo (isset($fd->fd_merge) && !empty($fd->fd_merge))?'checked':''; ?>/> 單沒入
												</label>
											</div>
                                            <div class="col-md-12">
												<div>
													<!-- Nav tabs -->
													<ul class="nav nav-tabs" role="tablist">
														<li role="presentation" class="active"><a href="#drugtab" aria-controls="home" role="tab" data-toggle="tab">持毒描述</a></li>
														<li role="presentation"><a href="#suspchecktab" aria-controls="profile" role="tab" data-toggle="tab">尿檢</a></li>
													</ul>

													<!-- Tab panes -->
													<div class="tab-content">
													<div role="tabpanel" class="tab-pane active" id="drugtab">
														<div class="panel panel-default">
															<!-- <div class="panel-heading">持毒描述</div> -->
															<div class="panel-body">
																<div class="row">
																	<div class="col-md-12 ">
																		<table class="table table-bordered  table-condensed" id="drugListTable">
																			<thead>
																				<tr >
																					<th class="text-center">物件</th>
																					<th class="text-center">數量</th>
																					<th class="text-center">成份</th>
																					<th class="text-center">重量</th>
																					<th class="text-center"><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#drugModal" onclick="resetDrugInd()">新增</button></th>
																				</tr>
																			</thead>
																			<tbody>
																				<?php
																				if(isset($drug))
																				{
																					if(count($drug) > 0)
																					{
																						foreach ($drug as $e) {
																							echo '<tr data-id="'. $e->e_id.'">
																									<td>'. $e->e_name.'</td>
																									<td>'. $e->e_count . $e->e_unit.'</td>
																									<td>'. $e->e_type.'</td>
																									<td>'. $e->e_1_N_W.'</td>
																									<td>
																									<a class="btn btn-link" data-toggle="modal" data-target="#drugModal" onclick="loadDrugInd(\''. $e->e_id.'\')">編輯</a>
																									<br>
																									<a class="btn text-danger" onclick="delDrugInd(\''. $e->e_id.'\')">刪除</a>
																									</td>
																								</tr>';
																						}
																					}
																					else
																					{
																						echo '<tr><td colspan="5">無相關毒證物資料</td></tr>';
																					}
																					
																				}
																				else
																				{
																					echo '<tr><td colspan="5">無相關毒證物資料</td></tr>';
																				}
																				?>
																				<!-- <tr>
																					<td>即溶包</td>
																					<td>1包</td>
																					<td>
																						2級大麻,
																						1級海洛因 ,
																						2級安非他命
																					</td>
																					<td>
																					<button type="button" class=" btn-link" data-toggle="modal" data-target="#drugModal">編輯</button>
																					</td>
																				</tr>
																				<tr>
																					<td>錠劑</td>
																					<td>5顆</td>
																					<td>
																					3級硝甲西泮,
																					4級硝西泮
																					</td>
																					<td>
																					<button type="button" class=" btn-link">編輯</button>
																					</td>
																				</tr> -->
																			</tbody>
																		</table>
																		<div class="modal fade " id="drugModal" tabindex="-1" role="dialog" aria-labelledby="drugModalLabel">
																			<div class="modal-dialog " role="document">
																				<div class="modal-content">
																					<div class="modal-header bg-primary">
																						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																						<h4 class="modal-title" id="drugModalLabel">物件</h4>
																					</div>
																					<div class="modal-body">
																						<div class="row">
																							<div class="col-md-5">
																								<form id="drugIndForm">
																									<div class="row">
																										<div class="form-group form-group-sm col-md-12">
																											<label class=" control-label">物品</label>
																											<?php 
																											echo form_dropdown('e_name_sel',$opt1,'','class="form-control" id=sel onchange="changeEnameOther($(this))" ') ?>
																											<input type="text" name="e_name_other" class="form-control hidden" id="e_name_other" value="">
																											<input type="hidden" name="e_name">
																										</div>
																										<div class="form-group form-group-sm col-md-8">
																											<label class=" control-label">數量</label>
																											<?php echo form_input('e_count','', 'class="form-control" id="drugpack_count"')?>
																										</div>
																										<div class="form-group form-group-sm col-md-4">
																											<label class=" control-label">單位</label>
																											<?php echo form_dropdown('e_type',$opt2,'','class="form-control" id="sel3" ') ?>
																										</div>
																										<div class="form-group form-group-sm col-md-12">
																											<label class=" control-label">重量</label>
																											<?php echo form_input('e_weight','', 'class="form-control" id="drugpack_weight"')?>
																										</div>
																									</div>
																									
																								</form>
																							</div>
																							<div class="col-md-7">
																								<div class="row">
																									<div class="form-group form-group-sm col-md-12">
																										<label class="col-md-6 control-label">成份</label>
																										<div class="col-md-6 text-right">
																											<button type="button" class="btn btn-default btn-xs" onclick="addDrugTr()">增加</button>
																										</div>
																										
																									</div>
																									<!-- <div class="form-group form-group-sm col-md-4">
																										<select class="form-control" id="druglevel">
																											<option value="">請選擇</option>
																											<option value="1">1級</option>
																											<option value="2">2級</option>
																											<option value="3">3級</option>
																											<option value="4">4級</option>
																										</select>
																									</div>
																									<div class="form-group form-group-sm col-md-8">
																										<select class="form-control" id="drugname"></select>
																									</div> -->
																								</div>
																								<!-- <hr> -->
																								<table class="table table-bordered table-striped table-condensed" id="drugIndTable">
																									<thead>
																										<tr >
																											<th class="text-center">級數</th>
																											<th class="text-center">成份</th>
																											<th class="text-center">數量</th>
																										</tr>
																									</thead>
																									<tbody>
																									</tbody>
																								</table>

																								<p><small class="text-danger">雙擊資料列即可編輯</small></p>
																							</div>
																						</div>
																						<button type="button" class="btn btn-warning btn-sm btn-block" id="saveDrugInd">儲存</button>																					
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<input type="hidden" name="e_id" value="<?php //echo $drug->e_id;?>">
																	<div class="form-group col-md-6">
																		<label>物件 <br><small class="text-danger">點擊上表任一資料列帶入</small></label>
																		<p id="drugObj"></p>
																	</div>
																	<div class="form-group col-md-6">
																		<label>樣態1</label>
																		<select class="form-control" id="sel1" name="sel1"> 
																			<option value="沾">沾</option>
																			<option value="摻">摻</option>
																			<option value="">沒有</option>
																		</select>	
																	</div>
																	
																</div>
																<div class="row">
																	<div class="form-group col-md-12">
																		<label>級數、成分</label>
																		<button class="btn btn-default btn-sm pull-right" type="button" onclick="addDrugtype($(this))">加入毒物描述</button>
																		<textarea  id="drug" name="drug" class="form-control" placeholder="3級硝甲西泮;4級硝西泮" rows="3"><?php 
																		$patterns[0] = "/沾/";
																		$patterns[1] = "/第/";
																		$patterns[2] = "/毒品/";
																		$patterns[3] = "/「/";
																		$patterns[4] = "/」/";
																		$patterns[5] = "/及/";
																		$patterns[6] = "/摻/";

																		$replacements[6] = "";
																		$replacements[5] = "";
																		$replacements[4] = "";
																		$replacements[3] = "";
																		$replacements[2] = "";
																		$replacements[1] = ";";
																		$replacements[0] = "";
																		//echo preg_replace($patterns, $replacements, $drug[0]->e_type); ?></textarea>
																		<span class="text-danger"><small>如：第三級毒品「3,4-亞甲基雙氧苯基乙基胺丁酮」，請輸入 3級3,4-亞甲基雙氧苯基乙基胺丁酮</small> </span> <br>
																		<span class="text-danger"><small>如：第三級毒品「硝甲西泮」及第四級毒品「硝西泮」，請輸入 3級硝甲西泮;4級硝西泮</small> </span> 
																		
																	</div>
																	
																</div>
																<!-- <div class="form-group col-md-12">
																	<label>物品</label>
																	<?php //echo form_dropdown('e_name',$opt1,$drug->e_name,'class="form-control" id=sel') ?>
																</div>
																<div class="form-group col-md-6">
																	<label>數量</label>
																	<?php //echo form_input('e_count',$drug->e_count, 'class="form-control"')?>
																</div>
																<div class="form-group col-md-6">
																	<label>單位</label>
																	<?php //echo form_dropdown('e_type',$opt2,$drug->	e_unit,'class="form-control" id="sel3"') ?>
																</div>
																<div class="form-group col-md-12">
																	<label>級數、成分</label>
																	<textarea  id="drug" name="drug" class="form-control"><?php //echo $drug->e_type; ?></textarea>
																</div>
																<div class="form-group col-md-4">
																	<label>樣態1</label>
																	<select class="form-control" id="sel1" name="sel1"> 
																		<option value="沾">沾</option>
																		<option value="摻">摻</option>
																		<option value=" ">沒有</option>
																	</select>
																</div>-->
																<div class="row">
																	<div class="form-group col-md-12">                                                        
																		<label>樣態2</label>
																		<button class="btn btn-default btn-sm pull-right" type="button" onclick="addDrugloc($('#sel2'))">加入採集描述</button>
																		<select id="sel2" name="sel2" class="form-control"> 
																			<option value="(採集自車內)">採集自車內</option>
																			<option value="(採集自隨身包包內)">採集自隨身包包內</option>
																			<option value="(採集自車內殘渣裝袋)">採集自車內殘渣裝袋</option>
																		</select>
																	</div>
																
																	<div class="form-group col-md-12">
																		<label>描述文字</label>
																		<textarea name="fd_drug_message" id="messageSpan" rows="3" class="form-control"><?php if(isset($sp->fd_drug_msg))echo $sp->fd_drug_msg;
																				else echo '';
																			?></textarea>
																			
																	</div> 
																</div>
																
															</div>
														</div>
													</div>
													<div role="tabpanel" class="tab-pane" id="suspchecktab">
														<div class="panel panel-default">
															<!-- <div class="panel-heading">尿檢</div> -->
															<div class="panel-body">
																<div class="row">
																	<div class="form-group col-md-6">
																		<label>尿液編號</label>
																		<?php echo form_input('s_utest_num',$susp->s_utest_num, 'class="form-control"')?>
																	</div>
																	<div class="form-group form-group-sm col-md-12">
																		<label class="col-md-6 control-label">成份</label>
																		<div class="col-md-6 text-right">
																			<button type="button" class="btn btn-default btn-xs" onclick="addUrineTr()">增加</button>
																		</div>
																	</div>
																	<div class="col-md-12">
																		<table class="table table-bordered table-striped table-condensed" id="UrineTestTable">
																			<thead>
																				<tr >
																					<th class="text-center">級數</th>
																					<th class="text-center">成份</th>
																					<th></th>
																				</tr>
																			</thead>
																			<tbody>
																				<?php
																				if(isset($urine) && count($urine) > 0)
																				{
																					$ui = 0;
																					foreach ($urine as $u) {
																						echo '<tr data-id="'. $u->sc_num.'" id="urinetr'.$ui.'" ondblclick="editUrineStateEvent($(this), '.$ui.')">
																								<td style="width:8rem;">'. $u->sc_level.'級</td>
																								<td>'. $u->sc_ingredient .'</td>
																								<td style="width:8rem;">
																								
																								</td>
																							</tr>';
																						$ui++;
																					}
																				}
																				else
																				{
																					echo '<tr class=" empty-tr"><td colspan="2" class="text-center">點擊右上角新增成分</td></tr>';
																				}
																					
																				?>
																			</tbody>
																		</table>

																		<p><small class="text-danger">雙擊資料列即可編輯</small></p>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

											</div>
                                                
												
                                            </div>
                                            
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#fact" aria-expanded="false" aria-controls="collapseThree">
                                        事實
                                        </a>
                                    </h4>
                                    </div>
                                    <div id="fact" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<label>刑事&不罰情形</label>
												<div class="input-group">
													<select id="seltype" class="form-control" > </select>
													<span class="input-group-btn">
														<button class="btn btn-link" type="button" onclick="addSeltype($(this))">加入描述</button>
													</span>
												</div>
											</div>
										</div>
                                        <div class="row">
                                            <div class="col-md-12">
												<span class="text-danger">注：前面資料如有修改，請先儲存，以便顯示最新事實內容。</span>
                                            <?php 
                                            $s_date = tranfer2RCyear($cases->s_date);
                                            $s_time = tranfer2RChour($cases->s_time);
											$eary = array();
											$uary = array();
											$drug_and_urine_desp = '';
											$patterns = array('1','2','3','4');
											$replacements = array('第一級毒品','第二級毒品','第三級毒品','第四級毒品');
											
											// 毒檢陽 尿檢陰
											if(!isset($urine) && isset($drug))
											{
												foreach ($drug as $e) {
													$ea = explode(',', $e->e_type);
													foreach ($ea as $value) {
														array_push($eary, array('lv'=>explode('級', $value)[0], 'ind'=>explode('級', $value)[1]));
													}
												}
												$tmp_ary = array();
												foreach (array_unique(array_column($eary, 'lv')) as $lvv) {
													$enum = 0;
													foreach ($eary as $evalue) {
														if(trim($evalue['lv']) == trim($lvv))
														{
															$tmp_ary[$lvv][$enum] = trim($evalue['ind']);
															$enum++;
														}
													}
												}
												$str_ary = array();
												foreach ($tmp_ary as $str_k => $str_v) {
													array_push($str_ary, str_replace($patterns, $replacements, $str_k).'「'.implode('、', array_unique($str_v)).'」');
													
												}
												$drug_and_urine_desp = '經採集毒品檢體送專業單位鑑驗，呈' . implode('及', $str_ary) . '反應';
											}
											elseif (isset($urine) && !isset($drug)) { // 毒檢陰 尿檢陽
												foreach ($urine as $u) {
													array_push($uary, array('lv'=>$u->sc_level, 'ind'=>$u->sc_ingredient));
												}
												$tmp_ary = array();
												foreach (array_unique(array_column($uary, 'lv')) as $lvv) {
													$enum = 0;
													foreach ($uary as $uvalue) {
														if(trim($uvalue['lv']) == trim($lvv))
														{
															$tmp_ary[$lvv][$enum] = trim($uvalue['ind']);
															$enum++;
														}
													}
												}
												$str_ary = array();
												foreach ($tmp_ary as $str_k => $str_v) {
													array_push($str_ary, str_replace($patterns, $replacements, $str_k).'「'.implode('、', array_unique($str_v)).'」');
													
												}
												$drug_and_urine_desp = '經採集尿液檢體送專業單位鑑驗，呈' . implode('及', $str_ary) . '反應';
											}
											elseif (isset($urine) && isset($drug))
											{
												// var_dump($drug);
												// 毒品
												foreach ($drug as $e) {
													$ea = explode(',', $e->e_type);
													
													// $ea = explode('、', $e->e_name);
													foreach ($ea as $value) {
														array_push($eary, array('lv'=>explode('級', $value)[0], 'ind'=>explode('級', $value)[1]));
														// array_push($eary, array('lv'=>explode('第',explode('級', $value)[0])[1], 'ind'=>explode('」',explode('「', explode('級', $value)[1])[1])[0]));
													}
												}
												// var_dump($eary);
												$etmp_ary = array();
												foreach (array_unique(array_column($eary, 'lv')) as $lvv) {
													$enum = 0;
													foreach ($eary as $evalue) {
														if(trim($evalue['lv']) == trim($lvv))
														{
															$etmp_ary[$lvv][$enum] = trim($evalue['ind']);
															$enum++;
														}
													}
												}
												// var_dump($etmp_ary);
												$estr_ary = array();
												foreach ($etmp_ary as $str_k => $str_v) {
													array_push($estr_ary, str_replace($patterns, $replacements, $str_k).'「'.implode('、', array_unique($str_v)).'」');
													
												}

												// 尿檢
												foreach ($urine as $u) {
													array_push($uary, array('lv'=>$u->sc_level, 'ind'=>$u->sc_ingredient));
												}
												$utmp_ary = array();
												foreach (array_unique(array_column($uary, 'lv')) as $lvv) {
													$enum = 0;
													foreach ($uary as $uvalue) {
														if(trim($uvalue['lv']) == trim($lvv))
														{
															$utmp_ary[$lvv][$enum] = trim($uvalue['ind']);
															$enum++;
														}
													}
												}
												$ustr_ary = array();
												foreach ($utmp_ary as $str_k => $str_v) {
													array_push($ustr_ary, str_replace($patterns, $replacements, $str_k).'「'.implode('、', array_unique($str_v)).'」');
													
												}

												$diffary = array_diff(array_unique(array_column($eary, 'ind')), array_unique(array_column($uary, 'ind')));
												if(count(array_unique(array_column($eary, 'lv'))) != count(array_unique(array_column($uary, 'lv'))) || count($diffary) > 0)
												{
													// $drug_and_urine_desp = '經採集毒品及尿液檢體送專業單位鑑驗，毒品檢體呈'. implode('及', $estr_ary) . '反應，尿液檢體呈' . implode('及', $ustr_ary) . '反應';
													$drug_and_urine_desp = '經採集毒品及尿液檢體送專業單位鑑驗，毒品檢體呈'. ((count($estr_ary)>0)?str_replace(array('一','二','三','四'), array('一','二','三','四'), implode('及', $estr_ary)):'無') . '反應，尿液檢體呈' . ((count($ustr_ary)>0)?str_replace(array('一','二','三','四'), array('一','二','三','四'), implode('及', $ustr_ary)):'無') . '反應';
												}
												else
												{
													// $drug_and_urine_desp = '經採集毒品及尿液檢體送專業單位鑑驗，均呈'. implode('及', $estr_ary) . '反應';
													$drug_and_urine_desp = '經採集毒品及尿液檢體送專業單位鑑驗，均呈'. str_replace(array('一','二','三','四'), array('一','二','三','四'), implode('及', $estr_ary)) . '反應';
													
												}
											}
											else
											{
												$drug_and_urine_desp = '經採集毒品及尿液檢體送專業單位鑑驗，均無反應';
											}
											
											$patterns = array('1級','2級','3級','4級');
											$replacements = array('一級','二級','三級','四級');

                                            // if(!isset($sp->fd_fact_text)||($sp->fd_fact_text==null)||($sp->fd_fact_text=='')){?>
                                                    <textarea name="fd_fact_text" id="fd_fact_text"  class="form-control" rows="10"><?php echo "受處分人" . $susp->s_name. $s_date . $s_time .'許，在本市' . $cases->s_place .'，為'.$cases->s_office.'員警查獲持有'.str_replace($patterns, $replacements, $sp->fd_drug_msg).'，'.$drug_and_urine_desp.'，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg . $sp->fd_summary; ?></textarea>
                                                   
                                                <?php //}else{?>
                                                    <!-- <textarea name="fd_fact_text" id="fd_fact_text"  class="form-control" rows="10"><?php //echo strip_tags($sp->fd_fact_text); ?></textarea> -->
                                                <?php //} ?>
                                            
                                            </div>
                                            <!-- <div class="form-group col-md-12">
                                                <label>處分書類型</label>
                                                <select id="seltype" class="form-control">
                                                    <option selected>請選擇</option>
                                                    <option value="0">type1</option>
                                                    <option value="1">type2</option>
                                                    <option value="2">type3</option>
                                                </select>
                                                <select id="seltype2" class="form-control"></select>
                                                <button type="button" class="sub2 btn btn-default">產生列句</button></td>
                                                <textarea name="fd_dis_msg" id="messageSpan2" class="form-control"><?php //echo $sp->fd_dis_msg?></textarea>
                                            </div>                                            
                                            <div class="form-group col-md-12">
                                                <label>毒品成分級別</label>
                                                <input type="text" id="drug" name="drug" class="form-control" value="<?php //echo $drug_ingredient?>"/>
                                                    <input id="nextvalue" type="hidden" name="next" value=""> 
                                                    <input id="prevvalue" type="hidden" name="prev" value=""> 
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>尿液成分級別</label>
                                                <?php //echo form_input('sc_ingredient',$sc_ingredient, 'class="form-control"')?>
                                            </div> -->
                                            
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#course" aria-expanded="false" aria-controls="collapseThree">
                                        毒品講習
                                        </a>
                                    </h4>
                                    </div>
                                    <div id="course" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                    <input id="nextvalue" type="hidden" name="next" value=""> 
                                    <input id="prevvalue" type="hidden" name="prev" value=""> 
										<div class="row">
											<div class="form-group col-md-12">
												<p class="text-primary"><label>專案預設講習</label><span>
												<?php 
												if(isset($dp->dp_course) && !empty($dp->dp_course) )
												{
														$default = explode('-', str_replace('_', '-', preg_replace('/\r\n|\n/',"",$dp->dp_course)));
														echo $default[0] . '(' .$default[1].') ' .$default[2].'-'.$default[3] . ' ' .$default[4];
												}
												else
												{
													echo '';
												}; ?>
												</span></p>
												<p class="text-muted"><label>個案目前講習</label><span>
												<?php
												if(!empty($sp->fd_lec_date) && isset($sp->fd_lec_date) )
												{
														echo $sp->fd_lec_date . $sp->fd_lec_time . $sp->fd_lec_place . '(' . $sp->fd_lec_address . ')';
												}
												else
												{
													echo '';
												}; ?>
												</span></p>
											</div>
										</div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label>講習時段(預設講習時段)</label> 
                                                <?php 
												$courseAry = '';
												if(isset($dp->dp_course) && !empty($dp->dp_course) )
												{
													// if(explode('_', preg_replace('/\r\n|\n/',"",$dp->dp_course))[0] == explode('(', $sp->fd_lec_date)[0])
													// {
														$courseAry = explode('-', str_replace('_', '-', preg_replace('/\r\n|\n/',"",$dp->dp_course)));
													// }
													// else
													// {
													// 	$courseAry = '';
													// }
												};
												
												foreach ($course_opt as $key => $value) {														
													if(($courseAry != ''))
													{
														if(trim($courseAry[0]) == explode(' (', preg_replace('/\r\n|\n/',"",$course_opt[$key]))[0])
														{
															// unset($course_opt[$key]);
														}
													}
													else
													{
														if(isset($sp->fd_lec_date))
														{
															if(trim(explode('(', $sp->fd_lec_date)[0]) == explode(' (', preg_replace('/\r\n|\n/',"",$course_opt[$key]))[0])
															{
																// unset($course_opt[$key]);
															}
														}
														
													}
													$value = preg_replace('/\r\n|\n/',"",$value);
												}
												
												echo form_dropdown('fd_lec',$course_opt,((isset($dp->dp_course) && !empty($dp->dp_course))?str_replace('_', '-', $dp->dp_course):''),'class="form-control" id="fd_lec" onchange="changeCourseEvent($(this))"') ?>
												
                                            </div>
											<div class="col-md-12">
											<?php  
											
											if(is_array($courseAry) && !empty($sp->fd_lec_date)  && isset($sp->fd_lec_date))
											{
												if($courseAry[0] . '(' .$courseAry[1].')' != $sp->fd_lec_date)
												{
													echo '<p class="text-danger">注意：所選的時段與預設時段不一樣，如需更改為預設請選擇『無』，否請忽略該訊息。</p>';
												}
											}
											elseif (!is_array($courseAry) && !empty($sp->fd_lec_date)  && isset($sp->fd_lec_date)) {
												echo '<p class="text-danger">注意：所選的時段與預設時段不一樣，如需更改為預設請選擇『無』，否請忽略該訊息。</p>';
											}
											
											?>
											</div>	
											<div <?php echo (($courseAry == '' && empty($sp->fd_lec_time))?'hidden':''); ?> id="fd_lec_wrap">
                                            <div class="form-group col-md-12">
                                                <label>講習時數</label>
                                                <?php
													if(is_array($courseAry) && empty($sp->fd_lec_time) ) {
														echo form_input('fd_lec_time',$courseAry[2].'-'.$courseAry[3], 'class="form-control"');
													}
													elseif (is_array($courseAry) && !empty($sp->fd_lec_time)) {
														if($courseAry[0] . '(' .$courseAry[1].')' != $sp->fd_lec_date)
														{
															echo form_input('fd_lec_time',$sp->fd_lec_time, 'class="form-control"');
														}
														else
														{
															echo form_input('fd_lec_time',$courseAry[2].'-'.$courseAry[3], 'class="form-control"');
														}
														
													}
													elseif (!is_array($courseAry) && !empty($sp->fd_lec_time)) {
														echo form_input('fd_lec_time',$sp->fd_lec_time, 'class="form-control"');
													}
													else 
													{
															echo form_input('fd_lec_time','', 'class="form-control"');
															
													}
													
                                                ?>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>講習時間</label>
                                                <?php
													if(is_array($courseAry) && empty($sp->fd_lec_time) ) {
														echo form_input('fd_lec_date',$courseAry[0] . '(' .$courseAry[1].')', 'class="form-control"');
													}
													elseif (is_array($courseAry) && !empty($sp->fd_lec_time)) {
														if($courseAry[0] . '(' .$courseAry[1].')' != $sp->fd_lec_date)
														{
															echo form_input('fd_lec_date',$sp->fd_lec_date, 'class="form-control"');
														}
														else
														{
															echo form_input('fd_lec_date',$courseAry[0] . '(' .$courseAry[1].')', 'class="form-control"');
														}
														
													}
													elseif (!is_array($courseAry) && !empty($sp->fd_lec_time)) {
														echo form_input('fd_lec_date',$sp->fd_lec_date, 'class="form-control"');
													}
													else 
													{
														echo form_input('fd_lec_date','', 'class="form-control"');
															
													}
                                                ?>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>講習地點</label>
                                                <?php
													if(is_array($courseAry) && empty($sp->fd_lec_time) ) {
														echo form_input('fd_lec_address',$courseAry[4], 'class="form-control"');
													}
													elseif (is_array($courseAry) && !empty($sp->fd_lec_time)) {
														if($courseAry[0] . '(' .$courseAry[1].')' != $sp->fd_lec_date)
														{
															echo form_input('fd_lec_address',$sp->fd_lec_place . '(' . $sp->fd_lec_address . ')', 'class="form-control"');
														}
														else
														{
															echo form_input('fd_lec_address',$courseAry[4], 'class="form-control"');
														}
														
													}
													elseif (!is_array($courseAry) && !empty($sp->fd_lec_time)) {
														echo form_input('fd_lec_address',$sp->fd_lec_place . '(' . $sp->fd_lec_address . ')', 'class="form-control"');
													}
													else 
													{
														echo form_input('fd_lec_address','', 'class="form-control"');
															
													}
                                                ?>
                                            </div>
											<span id="info-txt" class="text-danger hidden">該個案已有重複之講習，請選擇其他。</span>
											<div class="form-group col-md-12">
												<label>特殊情況</label>
												<div class="checkbox">
													<label>
														<input type="checkbox" disabled <?php echo ((isset($susp->s_prison) && !empty($susp->s_prison))?'checked':''); ?> > 在監(出監後自動找毒防中心)
													</label>
													<br>
													<label>
														<input type="checkbox" name="special_situ" <?php echo ((isset($sp->fd_other_lec) && !empty($sp->fd_other_lec))?'checked':''); ?> > 疫情另通知
													</label>
												</div>
											</div>
											</div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <?php
									$has_f_damount = ($fine->fd_amount > 0)?true:false; // 是否有罰鍰
									$has_course = (isset($sp->fd_lec_date) && !empty($sp->fd_lec_date))?true:false;  // 是否有講習
									$has_merge = (isset($sp->fd_merge) && !empty($sp->fd_merge))?true:false;  //是否有單沒入

									$has_ary = array($has_f_damount, $has_course, $has_merge);
									$count_has_ch = array('一、', '二、', '三、');
								?>
                                <div class="panel-body" style="HEIGHT: 770px; BACKGROUND-COLOR: #FFFFFF; overflow-y:scroll;">
                                    <p>
                                        列管編號：<?php echo $sp->fd_num?> 郵寄 以稿代簽 限制開放
                                        第二層決行 檔號：<?php echo date('Y')-1911 ?>/07270399 保存年限：3年<strong></strong>
                                    </p>
                                    <p>
                                        校對： &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                        監印： &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                        發文： &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                    </p>
									<?php if($has_f_damount == false && $has_course == false && $has_merge == true){ ?>
											<p>正本：<?php echo ((mb_strlen($susp->s_roffice1) < 4)?'我是測試系統'.$susp->s_roffice :$susp->s_roffice); ?></p>
											<p>副本：我是測試系統刑事警察大隊</p>
										<?php } else { ?>
                                        
                                    <p>
										正本：
                                        <?php 
                                        
                                            if(null != $fd && count(explode(",", $fd->fd_address)) > 1)
                                            {
                                                $fd_name = explode(",", $fd->fd_target)[0];
                                                $fd_addr = explode(",", $fd->fd_address)[0];
                                                $fd_zip = explode(",", $fd->fd_zipcode)[0];
                                                // echo '<mark>'. $fd_name.'</mark> 君 <br> <span style="margin-left: 3em;">'. $fd_zip . ' ' . $fd_addr.'</span>';
                                                // echo '<br/>';
                                                $fd_name1 = explode(",", $fd->fd_target)[1];
                                                $fd_addr1 = explode(",", $fd->fd_address)[1];
                                                $fd_zip1 = $susp->s_dpzipcode;
                                                echo '<mark >'. $fd_name.'</mark>('.$fd_name1.'法定代理人) 君 <br> <span style="margin-left: 3em;">'. $fd_zip1 . ' ' . $fd_addr1.'</span>';
                                                if($susp->s_live_state == 2)
                                                {
                                                    echo '（指定送達現住地）';
                                                }
                                            }
                                            else
                                            {
                                                echo '<mark>'. $sp->fd_target.'</mark> 君 <br> <span style="margin-left: 3em;">'. $sp->fd_zipcode . ' ' . $sp->fd_address .'</span>';
                                                if($susp->s_live_state == 2)
                                                {
                                                    echo '（指定送達現住地）';
                                                }
                                            }
                                        ?>
                                    <p>
                                        副本：<?php echo ((mb_strlen($susp->s_roffice1) < 4)?'我是測試系統'.$susp->s_roffice :$susp->s_roffice); ?> 、我是測試系統刑事警察大隊、臺北市政府毒品危害防制中心</strong>
                                    </p>
									<?php } ?>
                                    <table width="890" cellspacing="0" cellpadding="0" border="1">
                                        <tbody>
                                            <tr>
                                                <td colspan="8" width="890">
                                                    <p align="center">
                                                        我是測試系統 違反毒品危害防制條例案件處分書(稿)
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="890">
													<?php 
                                                         if($sp->fd_date != '0000-00-00' && isset($sp->fd_date))
                                                         {
                                                    ?>
                                                        <p>
															發文日期字號：<?php echo (date('Y')-1911).'年'?> <?php echo date('m', strtotime($sp->fd_date))*1 .'月' ?> <?php echo date('d', strtotime($sp->fd_date))*1 .'日' ?>北市警刑毒緝字第<?php echo $sp->fd_send_num . ((isset($susp->s_prison) && !empty($susp->s_prison) && $susp->s_live_state == 3)?'1':''); ?>號
														</p>
                                                    <?php }else{
														if($dp->dp_send_date !=  '0000-00-00' && isset($dp->dp_send_date))
														{ ?>
														<p>
															發文日期字號：<?php echo ((int)substr($dp->dp_send_date, 0, 4)- 1911) . '年' . (int)substr($dp->dp_send_date, 5, 2)*1 . '月' . (int)substr($dp->dp_send_date, 8, 2)*1 . '日'; ?>北市警刑毒緝字第<?php echo$sp->fd_send_num . ((isset($susp->s_prison) && !empty($susp->s_prison) && $susp->s_live_state == 3)?'1':''); ?>號
														</p>
													   <?php }
													   		else{
														?>
														<p>
															發文日期字號：<strong>民國<?php echo (date('Y')-1911); ?>年<?php echo date('m')*1; ?>月  日</strong>北市警刑毒緝字第<?php echo$sp->fd_send_num  . ((isset($susp->s_prison) && !empty($susp->s_prison) && $susp->s_live_state == 3)?'1':''); ?>號
														</p>
														<?php } ?>
                                                    <?php } ?>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="890">
                                                    <p>
                                                        依據：<?php echo ((isset($sp->fd_acrrod_unit))?$sp->fd_acrrod_unit:(((mb_strlen($susp->s_roffice1) < 4)?'我是測試系統'.$susp->s_roffice :$susp->s_roffice))); ?><?php echo (isset($sp->fd_acrrod_date))?(date('Y', strtotime($sp->fd_acrrod_date))-1911) .'年'.date('m', strtotime($sp->fd_acrrod_date))*1 .'月' . date('d', strtotime($sp->fd_acrrod_date))*1 .'日':((isset($susp->s_fdate))?((strlen($susp->s_fdate) > 7 && $susp->s_fdate != '0000-00-00')?(date('Y', strtotime($susp->s_fdate))-1911) .'年'.date('m', strtotime($susp->s_fdate))*1 .'月' . date('d', strtotime($susp->s_fdate))*1 .'日':''):'');?><?php echo (isset($sp->fd_accrod_no))?$sp->fd_accrod_no:((mb_strlen($susp->s_roffice1) < 4)?'北市警'.$susp->s_roffice1.((strpos($susp->s_roffice, '分局') !== false)?'分刑':'') :$susp->s_roffice1); ?>字第<?php echo (isset($sp->fd_accrod_num))?$sp->fd_accrod_num:$susp->s_fno;?>號
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="128">
                                                    <p align="center">
                                                        受處分人
                                                    </p>
                                                </td>
                                                <td colspan="2" width="122">
                                                    <p align="center">
                                                        姓名
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong><?php echo $susp->s_name?></strong>
                                                    </p>
                                                </td>
                                                <td width="49">
                                                    <p align="center">
                                                        性別
                                                    </p>
                                                </td>
                                                <td width="50">
                                                    <p>
                                                       <strong> <?php echo $susp->s_gender?></strong>
                                                    </p>
                                                </td>
                                                <td width="70" valign="top">
                                                    <p>
                                                        出生
                                                    </p>
                                                    <p>
                                                        年月日
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <p>
                                                        <strong>民國<?php echo (date('Y', strtotime($susp->s_birth))-1911) .'年'?>
                                                        <?php echo (date('m', strtotime($susp->s_birth))) .'月'?>
                                                        <?php echo (date('d', strtotime($susp->s_birth))) .'日'?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="122" valign="top">
                                                    <p>
                                                        身分證
                                                    </p>
                                                    <p>
                                                        統一編號
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong><?php echo $susp->s_ic?></strong>
                                                    </p>
                                                </td>
                                                <td colspan="3" width="169" valign="top">
                                                    <p>
                                                        其他足資辨別之
                                                    </p>
                                                    <p>
                                                        特徵及聯絡電話
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <p>
                                                        <strong><?php 
														echo (isset($fine->fd_phone)?$fine->fd_phone:'');
                                                            // if(isset($phone)) {
                                                            //     if(!empty($phone->p_no))
                                                            //         echo $phone->p_no;
                                                            // }else {}
															?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">
                                                    <p align="center">
                                                        地址
                                                    </p>
                                                </td>
                                                <td width="70">
                                                    <p>
                                                        現住地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                        <strong><?php echo (isset($susp->s_prison) && !empty($susp->s_prison))?explode(':', $susp->s_prison)[1] .'('.explode(':', $susp->s_prison)[0].')' :$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress ?></strong>
                                                        <?php 
                                                            if($susp->s_live_state == 2)
                                                            {
                                                                echo '（指定送達現住地）';
                                                            }
                                                        ?>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="70">
                                                    <p>
                                                        戶籍地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                        <strong><?php echo $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress ?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <?php 
												
                                                // list($year,$month,$day) = explode("-",$susp->s_birth);
                                                // $year_diff = date("Y") - $year;
                                                // $month_diff = date("m") - $month;
                                                // $day_diff  = date("d") - $day;
                                                // if ($day_diff < 0 || $month_diff < 0)
                                                //     $year_diff--;

                                                ?>
                                                <td rowspan="4" width="128">
                                                    <p align="center">
                                                        法　定
                                                    </p>
                                                    <p align="center">
                                                        代理人
                                                    </p>
                                                </td>
                                                <td colspan="2" width="122">
                                                    <p align="center">
                                                        姓名
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong> <?php echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_name:''; ?></strong>
                                                    </p>
                                                </td>
                                                <td width="49">
                                                    <p align="center">
                                                        性別
                                                    </p>
                                                </td>
                                                <td width="50">
                                                    <p align="center">
                                                    <strong>  <?php echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_gender:''; ?></strong>
                                                    </p>
                                                </td>
                                                <td width="70" valign="top">
                                                    <p>
                                                        出生
                                                    </p>
                                                    <p>
                                                        年月日
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <p>
                                                        <?php if(isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !isAudlt($susp->s_birth, $cases->s_date)) {?>
                                                        <strong>民國<?php echo (date('Y', strtotime($suspfadai->s_fadai_birth))-1911) .'年'?>
                                                        <?php echo (date('m', strtotime($suspfadai->s_fadai_birth))) .'月'?>
                                                        <?php echo (date('d', strtotime($suspfadai->s_fadai_birth))) .'日'?></strong>
                                                        <?php } ?>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="122" valign="top">
                                                    <p>
                                                        身分證
                                                    </p>
                                                    <p>
                                                        統一編號
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong><?php echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_ic:''; ?></strong>
                                                    </p>
                                                </td>
                                                <td colspan="3" width="169" valign="top">
                                                    <p>
                                                        其他足資辨別之
                                                    </p>
                                                    <p>
                                                        特徵及聯絡電話
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <strong><?php echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !isAudlt($susp->s_birth, $cases->s_date))?$suspfadai->s_fadai_phone:''; ?></strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">
                                                    <p align="center">
                                                        地址
                                                    </p>
                                                </td>
                                                <td width="70">
                                                    <p>
                                                        現住地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>                                                    
                                                        
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="70">
                                                    <p>
                                                        戶籍地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                        <strong><?php echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name) && !isAudlt($susp->s_birth, $cases->s_date))?$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress:''; ?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        主旨
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <!-- <span>
                                                        受處分人處罰：
                                                    </span> -->
                                                    <p>
													<?php 
													$ch_j = 0;
													$has_ch = array();
													for ($i=0; $i < count($has_ary); $i++) { 
														if($has_ary[$i])
															array_push($has_ch, $count_has_ch[$ch_j++]);
														else
															array_push($has_ch, '');
													}
													if($ch_j > 1)
														echo '受處分人處罰：<br/>';

													if($has_ary[0]){
														echo '<p>'.  (($ch_j > 1)?$has_ch[0]:'受處分人處罰：') . '新臺幣<mark><strong>' . ($fine->fd_amount/10000) . '</strong></mark>萬元整。'.((isset($fd->fd_times) && $fd->fd_times > 1)?'( '.(date('Y')-1911).'年第'.$fd->fd_times.'次裁罰 )':'').'</p>';
													}

													if($has_ary[1]){
														echo  '<p>'. (($ch_j > 1)?$has_ch[1]:'受處分人處罰：') . '毒品危害 <mark><strong>6 </strong></mark>小時。(講習時間詳見下方「毒品講習」欄位)</p>';
													}

													if($has_ary[2]){
														$patterns = array('1級','2級','3級','4級');
														$replacements = array('一級','二級','三級','四級');
														echo  '<p>'. (($ch_j > 1)?$has_ch[2]:'受處分人處罰：') . '<mark><strong>' . str_replace($patterns, $replacements, $sp->fd_drug_msg) . '</strong></mark>沒入。</p>';
													}

													?>
													</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        事實
                                                    </p>
                                                </td>
                                                <?php
                                                    /** 轉民國年 */
                                                    function tranfer2RCyear($date)
                                                    {
                                                        $date = str_replace('-', '', $date);
                                                        $rc = ((int)substr($date, 0, 4)) - 1911;
                                                        return (string)$rc . '年' . (int)substr($date, 4, 2)*1 . '月' . (int)substr($date, 6, 2)*1 . '日';
                                                    }
                                                    /** 轉時分 */
                                                    function tranfer2RChour($time)
                                                    {
                                                        if(null != $time)
                                                        {
                                                            $hour = explode(':', $time)[0];
															$min = explode(':', $time)[1];
                                                            return $hour*1 . '時'.  $min*1 . '分';
                                                        }
                                                        else
                                                        {
                                                            return '';
                                                        }
                                                        
                                                    }
                                                    $s_date = tranfer2RCyear($cases->s_date);
                                                    $s_time = tranfer2RChour($cases->s_time);
                                                ?>
                                                <td colspan="7" width="762" height="100px" >
													
                                                <?php //if(!isset($sp->fd_fact_text)||($sp->fd_fact_text==null)||($sp->fd_fact_text=='')){?>
                                                    <!-- <textarea name="fd_fact_text1" class="hidden form-control"></textarea>
                                                    <div id="fd_fact_text_div" style="overflow-y:scroll;height:100px;">
                                                    受處分人<?php //echo $susp->s_name. $s_date . $s_time;?>許，在本市<?php //echo $cases->s_place; ?>，為<?php //echo $cases->s_office ?>員警查獲持有<?php //echo str_replace($patterns, $replacements, $sp->fd_drug_msg).'，'.$drug_and_urine_desp;?>，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。<?php //echo $sp->fd_dis_msg ?>
                                                    </div> -->
                                                   
                                                <?php //}else{?>
                                                    <textarea name="fd_fact_text1" class="hidden form-control"></textarea>    
                                                    <div id="fd_fact_text_div" style="overflow-y:scroll;height:100px;">
														<?php
															$doc_fact_str = "受處分人" . $susp->s_name. $s_date . $s_time .'許，在本市' . $cases->s_place .'，為'.$cases->s_office.'員警查獲持有'.str_replace($patterns, $replacements, $sp->fd_drug_msg).'，'.$drug_and_urine_desp.'，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。'.$sp->fd_dis_msg . $sp->fd_summary;
															if(strpos($sp->fd_fact_text, $doc_fact_str) === false)
															{
																echo '<strong style="color:red;">* 非最後更新內容，請記得儲存。</strong><br/>';
															}
															// if(!isset($sp->fd_fact_text) || empty($sp->fd_fact_text))
															// {
															// 	echo '<br/><strong style="color:red;">* 非最後更新內容，請記得儲存。</strong>';
															// }
														?>
                                                        <?php echo $sp->fd_fact_text; ?>
														
                                                    </div> 
                                                <?php //} ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        理由及
                                                    </p>
                                                    <p align="center">
                                                        法令依據
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
													<?php echo ($has_ary[0] || $has_ary[1])?'■':'□'; ?>
                                                        依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                    </p>
                                                    <p>
                                                        <?php echo ($has_ary[2])?'■':'□'; ?>
                                                        依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                    </p>
                                                </td>
                                            </tr>
											<!-- 繳款欄 -->
											<?php if($has_ary[0]) {?>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        繳納期限及方式
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
                                                    <?php 
                                                        if($fine->fd_limitdate != '0000-00-00')
                                                        {
                                                    ?>
                                                        一、罰鍰限於<strong>民國<?php echo (date('Y', strtotime($fine->fd_limitdate))-1911) .'年'?><?php echo (date('m', strtotime($fine->fd_limitdate))) .'月'?><?php echo (date('d', strtotime($fine->fd_limitdate))) .'日'?></strong>前選擇下列方式之一繳款：
                                                    <?php }else{?>
                                                        一、罰鍰限於<strong>民國Ｏ年Ｏ月Ｏ日</strong>前選擇下列方式之一繳款：
                                                    <?php } ?>
                                                    </p>
                                                    <p>
                                                        （一）以自動化設備（ATM、網路ATM、網路銀行）匯款至「虛擬帳號」（限本處分書）。
                                                    </p>
                                                    <p>
                                                        （二）至金融機構臨櫃繳款至「臨櫃帳戶」。
                                                    </p>
                                                    <p>
                                                        二、自動化設備匯款方式：（一）選擇【繳費】服務 （二）輸入轉入銀行代號：<strong>012</strong>
                                                        （三）輸入本案虛擬帳號：<strong><?php echo $fine->fd_bvc?></strong>
                                                    </p>
                                                    <p>
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong> 16112470361019</strong><strong>，</strong>戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<strong><u>備註（附言）欄位</u>填寫受處分人「姓名、</strong>                    <strong>身分證字號、電話</strong><strong>」</strong>，俾利辦理銷案。
                                                    </p>
                                                </td>
                                            </tr>
											<?php }?>
											<!-- 講習欄 -->
											<?php if($has_ary[1]) {?>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        <strong>毒品講習</strong>
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
                                                        一、<strong>可選擇參與實體講習或線上課程（每年可參與4次，惟應以申請不同套裝課程）。</strong>
                                                    </p>
                                                    <p>
                                                        <strong>二、講習時間、地點：</strong>
                                                        <?php 
                                                        if(isset($susp->s_prison) && !empty($susp->s_prison))
                                                        {
                                                        ?>
                                                            <strong class="text-primary">請受處分人出監後，主動聯繫臺北市政府毒品危害防制中心。</strong>
                                                        <?php } else {
                                                            if(isset($sp->fd_lec_date) && !empty($sp->fd_lec_date) && $sp->fd_other_lec == null){
                                                                
                                                                $md_str = explode("(", $sp->fd_lec_date)[0];
                                                                $week_str = explode("(", $sp->fd_lec_date)[1];
                                                                ?>                                                            
                                                            <strong class="text-primary"><?php echo "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日(". $week_str . '，時間：' . explode('-', $sp->fd_lec_time)[0] . '（請攜帶有相片之證件報到，逾時將無法入場），地點：'.$sp->fd_lec_place .'(' . $sp->fd_lec_address . ')'; ?></strong>
                                                            <?php }else{?>
                                                                <strong class="text-primary">由臺北市政府毒品危害防制中心另行通知。</strong>
                                                            <?php } ?>
                                                        <?php }?>
                                                        <!-- <strong>民國<?php //echo (date('Y', strtotime($sp->fd_lec_date))-1911) .'年'?><?php //echo (date('m', strtotime($sp->fd_lec_date))) .'月'?>
                                                        <?php //echo (date('d', strtotime($sp->fd_lec_date))) .'日'?></strong>
                                                        <strong><?php //echo substr($sp->fd_lec_time,0,5) ?>分（請攜帶有相片之證件報到，逾時將無法入場），地點：<?php //echo $sp->fd_lec_place ?></strong> -->
                                                        如要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。
                                                    </p>
                                                    <p>
                                                        三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。
                                                    </p>
                                                </td>
                                            </tr>
											<?php }?>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        注意事項
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
                                                        一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    </p>
                                                    <p>
                                                        二、承辦人：<?php echo $dp->dp_empno; ?>、電話：(02）2393-2397。
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p>
                                        承辦單位 (2539) 核稿 決行
                                    </p>            
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div>
                    </form>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <script>
            $(".rcdate").change(function(){
                let strlen = $(this).val().length;
                if(strlen < 7 || strlen < "7")
                {
                    if(strlen != 0)
                        $(this).val("0"+$(this).val());
                }
                else
                {
                    return false;
                }
                    
            });
            $(".rcdate").keypress(function(){
                if($(this).val().length >= 7)
                    return false;
            }); 
			listzipcode($("input[name='s_dpzipcode_addr']"), $("input[name='s_dpzipcode']"));
			listzipcode($("input[name='s_rpzipcode_addr']"), $("input[name='s_rpzipcode']"));
			if(!$("input[name='fd_zipcode']").val())
			{
				listzipcode($("input[name='fd_address']"), $("input[name='fd_zipcode']"));
			}
			
            $("#next").click(function (){
                $("#nextvalue").val('<?php echo $next ?>');
               // alert($("#nextvalue").val());
                $("#updateready").submit();
            });
            $("#prev").click(function (){
                $("#prevvalue").val('<?php echo $prev ?>');
                //alert($("#prevvalue").val());
                $("#updateready").submit();
            });

			$("#seldate").val("<?php echo (isset($fd->fd_times) && $fd->fd_times != 0)?$fd->fd_times:-1; ?>").change();

			$("input[name='ad']").bind('change', function(){
				if($(this).val().indexOf('1') > -1)
				{
					$("input[name='fd_address']").val($("#ori_rpaddress").val());
					listzipcode($("input[name='fd_address']"), $("input[name='fd_zipcode']"));
				}
				else
				{
					$("input[name='fd_address']").val($("#s_rpaddress").val());
					listzipcode($("input[name='fd_address']"), $("input[name='fd_zipcode']"));
				}
			});

			$("#e_name_other").bind('change', function(){
				$("input[name='e_name']").val($(this).val() + '-' + '其他');
			});
			$("#drugListTable tbody tr").bind('click', function(){
				$("#drugListTable tbody tr").removeClass('selected');
				$(this).toggleClass('selected');

				$("#drugObj").text($("#drugListTable tbody tr.selected td:eq(0)").text() + $("#drugListTable tbody tr.selected td:eq(1)").text());
				$("#drug").val($("#drugListTable tbody tr.selected td:eq(2)").text().replace(/\r\n|\n|\r|\t|\s/g,"").replace(/,|、/gi, ";"));
				
			});

            $('#sub').click(function (){
                
                var arr =[];
                var level =[];
                var drugn =[];
                var all =[];
                var fin = '';
                var drug =$('#drug').val().split(';');
                for(let i=0;i<drug.length-1;i++){
                    level[i]=drug[i].substr(0,1);
                    drugn[i]=drug[i].substr(2);
                    all[i]="第"+level[i]+"級毒品「"+drugn[i]+"」";
                    fin = all[i];
                    if(drug.length > 2) fin= fin + "及" + fin;
                    else fin = all[i];
                    console.log(drug.length);
                }
                $sel=$('#sel').val();
                $sel1=$('#sel1').val();
                $sel2=$('#sel2').val();
                $sel3=$('#sel3').val();
                $("input[name='e_count']").each(function(){
                    arr.push($(this).val());
                })
                $("#messageSpan").val($sel1+fin+$sel+arr[0]+$sel3+$sel2);
                //console.log(arr,$sel,$sel2);
                
            });
            $("#seldate").change(function(){
                $("#seldatef").val($("#seldate").val());
            switch (parseInt($(this).val())){
				case 1: 
                $("#pmoney value").remove();
                $("#pmoney").val("20000");
                break;
                case 2: 
                $("#pmoney value").remove();
                $("#pmoney").val("30000");
                break;
                case 3:
				case 4:
				case 5:
				case 6:
				case 7:	 
                $("#pmoney value").remove();
                $("#pmoney").val("50000");
                break;
				default:
					$("#pmoney value").remove();
					$("#pmoney").val("0");
                break;
            }});
            $('.sub2').click(function (){
                $seltype2=$('#seltype2').val();
                $("#messageSpan2").val($seltype2);
                //console.log($seltype2);
                
            });
			seltype_A = { 
                "受處分人另涉及意圖販賣而持有及加重持有第三級毒品部分，業經臺灣臺北地方檢察署不起訴處分，併此敘明。":"(A) 受處分人另涉及意圖販賣而持有及加重持有第三級毒品部分，業經臺灣臺北地方檢察署不起訴處分，併此敘明。",
				"原本局109年06月18日北市警刑毒緝字第1093010905號處分書作廢(列管編號：1091381)，若已完納罰鍰或完成講習，無須重復履行義務。":"(A) 原本局109年06月18日北市警刑毒緝字第1093010905號處分書作廢(列管編號：1091381)，若已完納罰鍰或完成講習，無須重復履行義務。"
			};
			seltype_B = { 
				"受處分人另涉及加重持有第三級毒品部分，業經臺灣臺北地方法院刑事簡易判決處有期徒刑2月，併予敘明。":"(B) 受處分人另涉及加重持有第三級毒品部分，業經臺灣臺北地方法院刑事簡易判決處有期徒刑2月，併予敘明。",
				"受處分人另涉及販賣、持有第二級毒品部分，業經臺灣桃園方法院刑事判決，分別處有期徒刑4年7月及4月，併予敘明。":"(B) 受處分人另涉及販賣、持有第二級毒品部分，業經臺灣桃園方法院刑事判決，分別處有期徒刑4年7月及4月，併予敘明。",
				"受處分人另涉及持有第二級毒品部分，業經臺灣臺北地方檢察署緩起訴處分確定，並向國庫支付新臺幣2萬元，併予敘明。":"(B) 受處分人另涉及持有第二級毒品部分，業經臺灣臺北地方檢察署緩起訴處分確定，並向國庫支付新臺幣2萬元，併予敘明。",
				"因本案係受處分人於違反行政法義務遭發覺前，由親屬主動向員警報案而發現，故免處罰鍰。":"(B) 因本案係受處分人於違反行政法義務遭發覺前，由親屬主動向員警報案而發現，故免處罰鍰。",
				"因本案係受處分人於違反行政法義務遭發覺前，主動向員警報案坦承犯行，故免處罰鍰。":"(B) 因本案係受處分人於違反行政法義務遭發覺前，主動向員警報案坦承犯行，故免處罰鍰。"
			};
			seltype_C = { 
				"受處分人另涉及施用第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署檢察官不起訴之處分，併予敘明。":"(C) 受處分人另涉及施用第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署檢察官不起訴之處分，併予敘明。",
				"受處分人另涉及施用、持有第二級毒品部分，業經臺灣臺北地方檢察署處分緩起訴2年，並至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併予敘明。":"(C) 受處分人另涉及施用、持有第二級毒品部分，業經臺灣臺北地方檢察署處分緩起訴2年，並至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併予敘明。",
				"受處分人另涉及施用、持有第二級毒品部分，業經臺灣士林地方檢察署處分緩起訴1年6月，並至指定醫療院所於1年之治療期程內完成戒癮治療，併予敘明。":"(C) 受處分人另涉及施用、持有第二級毒品部分，業經臺灣士林地方檢察署處分緩起訴1年6月，並至指定醫療院所於1年之治療期程內完成戒癮治療，併予敘明。",
			};
			seltype_D = { 
				"惟受處分人於警詢筆錄否認施用毒品，查受處分人之尿液檢體未檢出有毒品反應，且無其他積極證據證明受處分人近期施用毒品，故不處罰鍰、講習，查獲之毒品證物依規定沒入。":"(D) 惟受處分人於警詢筆錄否認施用毒品，查受處分人之尿液檢體未檢出有毒品反應，且無其他積極證據證明受處分人近期施用毒品，故不處罰鍰、講習，查獲之毒品證物依規定沒入。",
				"本案業經新北市政府警察局於110年4月29日新北警刑字第1104491592號處分書，裁處罰鍰3萬元、毒品危害講習6小時，本局僅針對毒品為沒入處分。":"(D) 本案業經新北市政府警察局於110年4月29日新北警刑字第1104491592號處分書，裁處罰鍰3萬元、毒品危害講習6小時，本局僅針對毒品為沒入處分。",
				"本案查獲時毒品危害防制條例第11條之1第2項尚未生效(98年11月20日前)，爰不處罰鍰、講習，僅針對毒品沒入處分，併與敘明。":"(D) 本案查獲時毒品危害防制條例第11條之1第2項尚未生效(98年11月20日前)，爰不處罰鍰、講習，僅針對毒品沒入處分，併與敘明。",
				"本案已逾3年裁罰時效，故不處罰鍰、講習，查獲之毒品證物依規定沒入。":"(D) 本案已逾3年裁罰時效，故不處罰鍰、講習，查獲之毒品證物依規定沒入。",
				"經查受處分人已於0年0月0日註記死亡，故本案僅就毒品為沒入處分。":"(D) 經查受處分人已於0年0月0日註記死亡，故本案僅就毒品為沒入處分。",
				"惟本案起獲毒品於查獲後始列為第三級毒品管制，行為時尚屬不罰，僅針對毒品沒入處分，併予敘明。":"(D) 惟本案起獲毒品於查獲後始列為第三級毒品管制，行為時尚屬不罰，僅針對毒品沒入處分，併予敘明。",
				"本案業經桃園市政府警察局刑事警察大隊109年7月24日桃警刑大偵字第1090015331號函認本案已逾裁罰時效，故不處罰鍰、講習。":"(D) 本案業經桃園市政府警察局刑事警察大隊109年7月24日桃警刑大偵字第1090015331號函認本案已逾裁罰時效，故不處罰鍰、講習。",
				"本案業經新北市政府警察局109年7月24日新北警刑字第1094555725號函認受處分人行為時尚屬不罰，不裁處罰鍰及講習，本局僅針對毒品沒入處分，併予敘明。":"(D) 本案業經新北市政府警察局109年7月24日新北警刑字第1094555725號函認受處分人行為時尚屬不罰，不裁處罰鍰及講習，本局僅針對毒品沒入處分，併予敘明。",
				"惟查行為人復於同(30)日22時40分另因施用、持有第三級毒品為本局查獲並裁罰在案(109年8月19日北市警刑毒緝字第1093013951號處分書)，故不處罰鍰、講習，查獲之毒品證物依規定沒入。":"(D) 惟查行為人復於同(30)日22時40分另因施用、持有第三級毒品為本局查獲並裁罰在案(109年8月19日北市警刑毒緝字第1093013951號處分書)，故不處罰鍰、講習，查獲之毒品證物依規定沒入。",
				"受處分人另涉及持有第二級毒品部分，經法院提審後，認逮捕程序於法未合，已當庭釋放；案經臺灣臺北地方檢察署偵查，認本案無令狀搜索適法性可議，其後所衍生之逮捕、調查及採尿等偵查作為，不具證據能力，故不予裁處，查獲之毒品證物依規定沒入。":"(D) 受處分人另涉及持有第二級毒品部分，經法院提審後，認逮捕程序於法未合，已當庭釋放；案經臺灣臺北地方檢察署偵查，認本案無令狀搜索適法性可議，其後所衍生之逮捕、調查及採尿等偵查作為，不具證據能力，故不予裁處，查獲之毒品證物依規定沒入。",
				"惟因本案屬無令狀搜索，程序合法部分查獲員警未能舉證，故其後所衍生之調查及採尿等偵查作為認定無證據能力，，故不予裁處，查獲之毒品證物依規定沒入。":"(D) 惟因本案屬無令狀搜索，程序合法部分查獲員警未能舉證，故其後所衍生之調查及採尿等偵查作為認定無證據能力，，故不予裁處，查獲之毒品證物依規定沒入。"
			};
			$('#seltype').append("<option value=''>請選擇加入</option>");
			$('#seltype').append("<option disabled>單純註記照罰</option>");
			$.each(seltype_A, function(key, value) {
				$('#seltype')
						.append($('<option>', { value : key })
						.text(value));
			});
			$('#seltype').append("<option disabled>免罰鍰</option>");
			$.each(seltype_B, function(key, value) {
				$('#seltype')
						.append($('<option>', { value : key })
						.text(value));
			});
			$('#seltype').append("<option disabled>免講習</option>");
			$.each(seltype_C, function(key, value) {
				$('#seltype')
						.append($('<option>', { value : key })
						.text(value));
			});
			$('#seltype').append("<option disabled>免罰鍰免講習</option>");
			$.each(seltype_D, function(key, value) {
				$('#seltype')
						.append($('<option>', { value : key })
						.text(value));
			});

            $("#seltype").change(function(){
            switch (parseInt($(this).val())){
                case 0: 
                $("#seltypef").val($("#seltype").val());
                $("#seltype2 option").remove();
                $("#seltype2").append($("<option value='原本局108年4月10日北市警刑毒緝第1-83--45411號處分書作廢(列管編號:1081106)"
                                    + "'>上年度處分書至本年度重發</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>單純不起訴處分</option>"
                                    ));
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='原本局108年4月10日北市警刑毒緝第1-83--45411號處分書作廢(列管編號:1081106)"
                                    + "'>上年度處分書至本年度重發</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>單純不起訴處分</option>"
                                    ));
                break;
                case 1: 
                $("#seltypef").val($("#seltype").val());
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署處分緩起訴2年，且應至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>臺北地檢緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。經臺灣士林地方檢察署處分緩起訴2年，且應至臺灣士林地方檢察署指定醫院於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>士林緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>已經勒戒</option>"
                                    ));
                $("#seltype2 option").remove();
                $("#seltype2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署處分緩起訴2年，且應至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>臺北地檢緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。經臺灣士林地方檢察署處分緩起訴2年，且應至臺灣士林地方檢察署指定醫院於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>士林緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>已經勒戒</option>"
                                    ));
                break;
                case 2: 
                $("#seltypef").val($("#seltype").val());
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署刑事簡易判決處有期徒刑貳月，併此敘明。"
                                    + "'>刑事簡易判決</option>"
                                    +"<option value='受處分人另涉及第一級毒品部分。業經臺灣臺北地方檢察署刑事判決處有期徒刑七月。"
                                    + "'>刑事判決</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，由親屬主動向員警報案發現犯行，故不予處罰。"
                                    + "'>親屬不罰</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，主動向員警報案坦承犯行，故不予處罰。"
                                    + "'>自首</option>"
                                    ));
                $("#seltype2 option").remove();
                $("#seltype2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署刑事簡易判決處有期徒刑貳月，併此敘明。"
                                    + "'>刑事簡易判決</option>"
                                    +"<option value='受處分人另涉及第一級毒品部分。業經臺灣臺北地方檢察署刑事判決處有期徒刑七月。"
                                    + "'>刑事判決</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，由親屬主動向員警報案發現犯行，故不予處罰。"
                                    + "'>親屬不罰</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，主動向員警報案坦承犯行，故不予處罰。"
                                    + "'>自首</option>"
                                    ));
                break;
            }   
            });
            $("#seldatef").change(function(){
            switch (parseInt($(this).val())){
                case 1: 
                $("#pmoneyf value").remove();
                $("#pmoneyf").val("20000");
                break;
                case 2: 
                $("#pmoneyf value").remove();
                $("#pmoneyf").val("30000");
                break;
                case 3:
				case 4:
				case 5:
				case 6:
				case 7:	 
                $("#pmoneyf value").remove();
                $("#pmoneyf").val("50000");
                break;
            }});
            $('.sub2f').click(function (){
                $seltypef2=$('#seltypef2').val();
                $("#messageSpan2f").val($seltypef2);
                //console.log($seltype2);
                
            });
            $("#seltypef").change(function(){
            switch (parseInt($(this).val())){
                case 0: 
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='原本局108年4月10日北市警刑毒緝第1-83--45411號處分書作廢(列管編號:1081106)"
                                    + "'>上年度處分書至本年度重發</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>單純不起訴處分</option>"
                                    ));
                break;
                case 1: 
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署處分緩起訴2年，且應至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>臺北地檢緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。經臺灣士林地方檢察署處分緩起訴2年，且應至臺灣士林地方檢察署指定醫院於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>士林緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>已經勒戒</option>"
                                    ));
                break;
                case 2: 
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署刑事簡易判決處有期徒刑貳月，併此敘明。"
                                    + "'>刑事簡易判決</option>"
                                    +"<option value='受處分人另涉及第一級毒品部分。業經臺灣臺北地方檢察署刑事判決處有期徒刑七月。"
                                    + "'>刑事判決</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，由親屬主動向員警報案發現犯行，故不予處罰。"
                                    + "'>親屬不罰</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，主動向員警報案坦承犯行，故不予處罰。"
                                    + "'>自首</option>"
                                    ));
                break;
            }   
            });
            $("#zipcode").twzipcode({
            "zipcodeIntoDistrict": true,
            "css": ["city form-control", "town form-control"],
            "countyName": "city", // 指定城市 select name
            "districtName": "town" // 指定地區 select name
            });         
            $("#zipcode2").twzipcode({
            "zipcodeIntoDistrict": true,
            "css": ["city form-control", "town form-control"],
            "countyName": "city", // 指定城市 select name
            "districtName": "town" // 指定地區 select name
            });  
            $(function() {  
               $('a.media').media({width:1200, height:700});  

               $("textarea[name='fd_fact_text1']").val($("#fd_fact_text_div").html().trim());
            });  

            /** 轉民國年 */
            function tranfer2ADyear(date)
            {
                date = date.replace('-','');
                date = (parseInt(date.substr(0, 2)) - 1911) + "年" + date.substr(4, 2) + "月" + date.substr(6, 2) + "日";
                 return date;                
            }   
			function addDrugtype(){
				var drug =$('#drug').val().split(';');
				var arr =[];
                var level =[];
                var drugn =[];
                var all =[];
                var fin = '';
				let lv1 = [], lv2 = [], lv3 = [], lv4 = [], strdesp = [];
                for(let i=0;i<drug.length;i++){
					level[i]=drug[i].substr(0,1);
					drugn[i]=drug[i].substr(2);
					$rm_str = drugn[i].split("（");
					switch (level[i]) {
						case '1':							
							lv1.push(($rm_str.length > 0)?$rm_str[0]:drugn[i]);
							break;
						case '2':
							lv2.push(($rm_str.length > 0)?$rm_str[0]:drugn[i]);
							break;
						case '3':
							lv3.push(($rm_str.length > 0)?$rm_str[0]:drugn[i]);
							break;
						default:
							lv4.push(($rm_str.length > 0)?$rm_str[0]:drugn[i]);
							break;
					}
                }
				
				if(lv1.length > 0) strdesp.push(`第1級毒品「${lv1.join('、')}」`);
				if(lv2.length > 0) strdesp.push(`第2級毒品「${lv2.join('、')}」`);
				if(lv3.length > 0) strdesp.push(`第3級毒品「${lv3.join('、')}」`);
				if(lv4.length > 0) strdesp.push(`第4級毒品「${lv4.join('、')}」`);
				fin = strdesp.join("及");
				// all[i]="第"+level[i]+"級毒品「"+drugn[i]+"」";
				// fin = all[i];
				// if(i > 0) fin= fin + "及" + all[i];
				// else fin = all[i];

                $sel=(($('#sel').val() == '其他')?$("#e_name_other").val():$('#sel').val());
                $sel1=$('#sel1').val();
                
                // $sel3=$('#sel3').val();
				$sel3 = $("#drugObj").text();
                $("input[name='e_count']").each(function(){
                    arr.push($(this).val());
                })
				
				$messageSpan = $("#messageSpan").val()+ (($("#messageSpan").val())?"、":"") ;
					

                $("#messageSpan").val($messageSpan+$sel1+fin+$sel3.replace('-其他', ''));
			}
			function addDrugloc(obj){
				$sel2=$('#sel2').val();
				$messageSpan = $("#messageSpan").val();
                $("#messageSpan").val($messageSpan+$sel2);
			}
            function copyAddress(obj)
            {
                if(obj != 'undefined' && obj.val() != '')
                {
                    $("#now").attr('checked', true);
                    // $("#now").val(obj.val().split(':')[1] + ` (${obj.val().split(':')[0]})`);
                    $("#s_rpaddress").val(obj.val().split(':')[1] + ` (${obj.val().split(':')[0]})`);

                }
                else
                {
                    $("#now").attr('checked', true);
                    $("#s_rpaddress").val('<?php echo $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress; ?>');

                }
				$("input[name='fd_address']").val($("#s_rpaddress").val());
				listzipcode($("input[name='fd_address']"), $("input[name='fd_zipcode']"));
            } 
            function changeCourseEvent(obj)
            {
                $val = obj.val().replace(/\r\n|\n/g,"");
                $data = $val.split('-');
                if($val)
                {
					$("#fd_lec_wrap").removeClass('hidden');
                    $("input[name=fd_lec_time]").val($data[2]+'-'+$data[3]);
                    $("input[name=fd_lec_date]").val($data[0]+'('+$data[1]+')');
                    $("input[name=fd_lec_address]").val($data[4]);
                }
                else
                {
					$("#fd_lec_wrap").addClass('hidden');
                    $("input[name=fd_lec_time]").val('');
                    $("input[name=fd_lec_date]").val('');
                    $("input[name=fd_lec_address]").val('');
                }
				checkDupCourse();
            }
			function checkDupCourse(){
				if($("input[name='fd_lec_date']").val())
				{
					let formdata = new FormData();
					formdata.append('fd_lec_date', $("input[name='fd_lec_date']").val());
					formdata.append('fd_lec_time', $("input[name='fd_lec_time']").val());
					formdata.append('fd_lec_place', $("input[name='fd_lec_address']").val());
					formdata.append('fd_sic', $("input[name='s_ic']").val());
					$.ajax({
						type: 'POST',
						url: '<?php echo base_url("disciplinary_c/getCase_courseDup"); ?>',
						data: formdata,
						dataType: 'json',
						processData: false,
						contentType: false,
						cache: false,
						complete: function (resp) {
							
							if(resp.responseJSON.data > 1)
							{
								if(!$("input[name=fd_lec_time]").val())
								{
									$("input[name=fd_lec_time]").val('');
									$("input[name=fd_lec_date]").val('');
									$("input[name=fd_lec_address]").val('');
								}
								
								$('#info-txt').removeClass('hidden');
							}							
							else
								$('#info-txt').addClass('hidden');
						}
					});
				}
				
            }
			function getzipcode33(addr) {
				// var dataUrl= "https://cors-anywhere.herokuapp.com/https://www.yijingtw.com/taoyuan/daxi/system/api/zipcodeSoap?postaddr="+addr
				// var xhr = new XMLHttpRequest()
				// xhr.open('GET',dataUrl, true)
				// xhr.send()
				// xhr.onreadystatechange = function(){
				// 	if(this.readyState === 4 && this.status === 200){
				// 		var data = JSON.parse(this.responseText);
				// 			console.log(data);
				// 	}
				// }
            	$.ajax({
            		url: 'https://www.yijingtw.com/taoyuan/daxi/system/api/zipcodeSoap?postaddr='+addr,
            		method: 'GET',
					dataType: 'json',
					processData: false,
					contentType: false,
					cache: false
            	}).done(function(resp){
					console.log(resp)
				});
				
            }
			function listzipcode(obj, wrap) {
                $wrap = wrap;
                let formdata = new FormData();
                formdata.append('addr', obj.val());
            	$.ajax({
            		type: 'POST',
            		url: '<?php echo base_url("disciplinary_c/getZipCode") ?>',
            		dataType: 'json',
                    // data: {'addr': obj.val()},
                    data: formdata,
            		processData: false,
            		contentType: false,
            		cache: false,
					success: function(data){
						// console.log(data);
						wrap.val(data.data);
						
					},
            		// complete: function (resp) {
                    //     // console.log(resp)						
                    //     $($wrap).val(resp.responseJSON.data);
						
            		// }
            	});
				
            }
			function changeEnameOther(obj){
				if(obj.val() == '其他')
				{
					$("#e_name_other").removeClass('hidden');
					$("input[name='e_name']").val($("#e_name_other").val() + '-' + '其他');
				}
				else
				{
					$("#e_name_other").addClass('hidden');
					$("input[name='e_name']").val(obj.val());
				}
			}
			function getDruglevel(obj, wrap)
			{
				// console.log(wrap);
				let formdata = new FormData();
				formdata.append('druglv', obj.val());
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url("disciplinary_c/getDrugLevel") ?>',
					dataType: 'json',
					// data: {'addr': obj.val()},
					data: formdata,
					processData: false,
					contentType: false,
					cache: false,
					success: function(data){
						// console.log(data);
						wrap.html('');
						$.each(data.data, function(key, value) {
							wrap
								.append($('<option>', { value : value.drug_name })
								.text(value.drug_name));
						});
					},
					complete: function (resp) {
						// console.log(resp.data)								
					}
				});
			}
			function addDrugTr()
			{
				$('#drugIndTable tbody tr.empty-tr').remove();
				ind = $("#drugIndTable tbody tr").length;
				$("#drugIndTable tbody").append(`<tr class="editstate" data-id="" data-type="insert">
					<td>
					<select class="form-control input-sm" style="width: 8rem;" id="druglevel${ind+1}" onchange="getDruglevel($('#druglevel${ind+1}'), $('#drugname${ind+1}'))">
						<option value="">請選擇</option>
						<option value="1">1級</option>
						<option value="2">2級</option>
						<option value="3">3級</option>
						<option value="4">4級</option>
					</select>
					</td>
					<td>
					<select class="form-control input-sm" style="width: 15rem;" id="drugname${ind+1}"></select>
					</td>
					<td>
						<input type="text" class="form-control input-sm" id="drugcount${ind+1}" />
					</td>
				</tr>`);
			}
			function loadDrugInd(id){
				$("#saveDrugInd").removeClass('btn-success').addClass('btn-warning').attr('onclick', 'updateDrugIndEvent(\'update\', $(this))').text('儲存');
				let formdata = new FormData();
				formdata.append('e_id', id);
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url("disciplinary_c/getDrudInd") ?>',
					dataType: 'json',
					// data: {'addr': obj.val()},
					data: formdata,
					processData: false,
					contentType: false,
					cache: false,
					success: function(data){
						// console.log(data.data);
						let postdata = data.data;
						let isother = false;
						let i = 0;
						$("#drugIndTable tbody").html("");
						postdata.forEach(element => {
							if(element.df_ingredient)
							{
								$("#drugIndTable tbody").append(`<tr data-id="${element.df_num}" data-type="update" ondblclick="editStateEvent($(this), ${i})">
																<td>${element.df_level}級</td>
																<td>${element.df_ingredient}</td>
																<td>${element.df_count}</td>
															</tr>`);
							
								i++;
							}
							
						});
						$("#saveDrugInd").attr('data-id', id);
						$("#sel option").each(function(el, index){
							// console.log($(this).val().indexOf(postdata[0].e_name));
							if($(this).val().indexOf(postdata[0].e_name) > -1)
							{
								$("#sel").val($(this).val()).change();
								$("#e_name_other").addClass('hidden');

								isother = false;

								return false;
							}
							else
							{
								isother = true;
							}
						})
						if(isother)
						{
							$("#sel").val("其他").change();
							$("#e_name_other").removeClass('hidden').val(postdata[0].e_name.replace('-其他', ''));
							changeEnameOther($("#sel"));
						}
						$("#drugpack_count").val(((postdata[0].e_count)?postdata[0].e_count:0));
						$("#sel3").val(((postdata[0].e_unit)?postdata[0].e_unit:'包'));
						$("#drugpack_weight").val(((postdata[0].e_1_N_W)?postdata[0].e_1_N_W:''));
					}
				});
			}
			function delDrugInd(id){
				let formdata = new FormData();
				formdata.append('e_id', id);
				formdata.append('e_s_ic', $("input[name='s_ic']").val());
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url("disciplinary_c/delDrudInd") ?>',
					dataType: 'json',
					// data: {'addr': obj.val()},
					data: formdata,
					processData: false,
					contentType: false,
					cache: false,
					success: function(data){
						postdata = data.data;
						if(postdata.indexOf('error') == -1)
						{
							postdata = data.data;
							$("#drugListTable tbody").html('');
							postdata.forEach(element => {
								$("#drugListTable tbody").append(`<tr data-id="${element.e_id}" >
																	<td>${element.e_name}</td>
																	<td>${(element.e_count)?element.e_count:''}${(element.e_unit)?element.e_unit:''}</td>
																	<td>${element.e_type}</td>
																	<td>${(element.e_1_N_W)?element.e_1_N_W:''}</td>
																	<td>
																	<a type="button" class="btn btn-link" data-toggle="modal" data-target="#drugModal" onclick="loadDrugInd(\'${element.e_id}\')">編輯</a>
																	<br>
																	<a class="btn text-danger" onclick="delDrugInd(\'${element.e_id}\')">刪除</a>
																	</td>
																</tr>`);
							});
							$("#drugListTable tbody tr").bind('click', function(){
								$("#drugListTable tbody tr").removeClass('selected');
								$(this).toggleClass('selected');

								$("#drugObj").text($("#drugListTable tbody tr.selected td:eq(0)").text() + $("#drugListTable tbody tr.selected td:eq(1)").text());
								$("#drug").val($("#drugListTable tbody tr.selected td:eq(2)").text().replace(/\r\n|\n|\r|\t|\s/g,"").replace(/,|、/gi, ";"));
								
							});
							$("#drugModal").modal('hide');
						}
						else
						{
							swal({
								title: "注意!",
								text: "資料庫作業錯誤",
								icon: "warning"
							});
						}
					}
				});
			}
			function editStateEvent(obj, index){
				let td_level = obj.find('td:eq(0)').text().replace("級",'');
				let td_ind = obj.find('td:eq(1)').text();
				let td_count = obj.find('td:eq(2)').text();
				// console.log(td_level);
				obj.addClass('editstate');
				obj.find('td:eq(0)').html(`<select class="form-control input-sm" style="width: 8rem;" id="druglevel${index}" onchange="getDruglevel($('#druglevel${index}'), $('#drugname${index}'))">
						<option value="">請選擇</option>
						<option value="1">1級</option>
						<option value="2">2級</option>
						<option value="3">3級</option>
						<option value="4">4級</option>
					</select>`);
				$(`#druglevel${index}`).val(td_level).change();
				obj.find('td:eq(1)').html(`<select class="form-control input-sm" style="width: 15rem;" id="drugname${index}"></select>`);
				getDruglevel($('#druglevel'+index), $('#drugname'+index));
				
				obj.find('td:eq(2)').html(`<input type="text" class="form-control input-sm" id="drugcount${index}" />`);
				setTimeout(() => {
					$(`#drugname${index} option`).each(function(){
						if($(this).val() == td_ind)
						{
							$(this).attr('selected', true);
							return false;
						}
						else if($(this).val().indexOf(td_ind) > -1)
						{
							$(this).attr('selected', true);
						}
							
					});
                    // $(`#drugname${index}`).val(td_ind).change();
                  }, 500);
				
			}
			function resetDrugInd(){
				// $("#drugIndForm")[0].reset();
				$("#sel").val('晶體粉末、碎塊').change();
				$("#e_name_other").addClass('hidden').val('');
				$("#drugpack_count").val('0');
				$("#drugpack_weight").val('');
				$("#sel3").val('包').change();
				$("#drugIndTable tbody").html('<tr class=" empty-tr"><td colspan="3" class="text-center">點擊右上角新增成分</td></tr>');
				$("#saveDrugInd").removeClass('btn-warning').addClass('btn-success').attr('onclick', 'updateDrugIndEvent(\'insert\', $(this))').attr('data-id', '').text('新增');
			}
			function updateDrugIndEvent(type, obj){
				var mapped_df_drug = $("#drugIndTable tbody tr.editstate").find('select:eq(1)').map(function( index ) { 
					$rm_str = $(this).val().split("（");
					
					return ($rm_str.length > 0)?$rm_str[0]:$(this).val();
				})
				var mapped_df_level = $("#drugIndTable tbody tr.editstate").find('select:eq(0)').map(function( index ) { return $(this).val();})
				var mapped_df_count = $("#drugIndTable tbody tr.editstate").find('input').map(function( index ) { return $(this).val();})
				var mapped_df_id = $("#drugIndTable tbody tr.editstate").map(function( index ) { return $(this).attr('data-id');})
				var mapped_df_writetype = $("#drugIndTable tbody tr.editstate").map(function( index ) { return $(this).attr('data-type');})

				let formdata = new FormData();
				formdata.append('method', type);
				formdata.append('e_id', obj.attr('data-id'));
				formdata.append('e_c_num', $("input[name='s_cnum']").val());
				formdata.append('e_type', mapped_df_drug.toArray().join('、'));
				formdata.append('e_count', $("#drugpack_count").val());
				formdata.append('e_unit', $("#sel3").val());
				formdata.append('e_name', $("input[name='e_name']").val());
				formdata.append('e_1_N_W', $("#drugpack_weight").val());
				formdata.append('e_suspect', $("input[name='s_name']").val());
				formdata.append('e_s_ic', $("input[name='s_ic']").val());
				formdata.append('df_drug', mapped_df_drug.toArray().join(','));
				formdata.append('df_level', mapped_df_level.toArray().join(','));
				formdata.append('df_count', mapped_df_count.toArray().join(','));
				formdata.append('df_id', mapped_df_id.toArray().join(','));
				formdata.append('df_wtype', mapped_df_writetype.toArray().join(','));
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url("disciplinary_c/addDrugInd") ?>',
					dataType: 'json',
					// data: {'addr': obj.val()},
					data: formdata,
					processData: false,
					contentType: false,
					cache: false,
					success: function(data){
						// console.log(data);
						postdata = data.data;
						if(postdata.indexOf('error') == -1)
						{
							postdata = data.data;
							$("#drugListTable tbody").html('');
							postdata.forEach(element => {
								$("#drugListTable tbody").append(`<tr data-id="${element.e_id}" >
																	<td>${element.e_name}</td>
																	<td>${(element.e_count)?element.e_count:''}${(element.e_unit)?element.e_unit:''}</td>
																	<td>${element.e_type}</td>
																	<td>${(element.e_1_N_W)?element.e_1_N_W:''}</td>
																	<td>
																	<a type="button" class="btn btn-link" data-toggle="modal" data-target="#drugModal" onclick="loadDrugInd(\'${element.e_id}\')">編輯</a>
																	<br>
																	<a class="btn text-danger" onclick="delDrugInd(\'${element.e_id}\')">刪除</a>
																	</td>
																</tr>`);
							});
							$("#drugListTable tbody tr").bind('click', function(){
								$("#drugListTable tbody tr").removeClass('selected');
								$(this).toggleClass('selected');

								$("#drugObj").text($("#drugListTable tbody tr.selected td:eq(0)").text() + $("#drugListTable tbody tr.selected td:eq(1)").text());
								$("#drug").val($("#drugListTable tbody tr.selected td:eq(2)").text().replace(/\r\n|\n|\r|\t|\s/g,"").replace(/,|、/gi, ";"));
								
							});
							$("#drugModal").modal('hide');
						}
						else
						{
							swal({
								title: "注意!",
								text: "資料庫作業錯誤",
								icon: "warning"
							});
						}
					}
				});
			}
			function addUrineTr()
			{
				$('#UrineTestTable tbody tr.empty-tr').remove();
				ind = $("#UrineTestTable tbody tr").length;
				$("#UrineTestTable tbody").append(`<tr id="urinetr${ind+1}">
					<td>
					<select class="form-control input-sm" style="width: 8rem;" id="urinelevel${ind+1}" onchange="getDruglevel($('#urinelevel${ind+1}'), $('#urinename${ind+1}'))">
						<option value="">請選擇</option>
						<option value="1">1級</option>
						<option value="2">2級</option>
						<option value="3">3級</option>
						<option value="4">4級</option>
					</select>
					</td>
					<td>
					<select class="form-control input-sm" style="width: 15rem;" id="urinename${ind+1}"></select>
					</td>
					<td>
					<button type="button" class="btn btn-warning btn-xs" onclick="updateUrineEvent(\'insert\', $(\'#urinetr${ind+1}\'))"><span class="fa fa-floppy-o" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-danger btn-xs" onclick="removeUrineEvent($(\'#urinetr${ind+1}\'))"><span class="fa fa-trash" aria-hidden="true"></span></button>
					</td>
				</tr>`);
			}
			function editUrineStateEvent(obj, index){
				let td_level = obj.find('td:eq(0)').text().replace("級",'');
				let td_ind = obj.find('td:eq(1)').text();
				// console.log(td_level);
				obj.find('td:eq(0)').html(`<select class="form-control input-sm" style="width: 8rem;" id="urinelevel${index}" onchange="getDruglevel($('#urinelevel${index}'), $('#urinename${index}'))">
						<option value="">請選擇</option>
						<option value="1">1級</option>
						<option value="2">2級</option>
						<option value="3">3級</option>
						<option value="4">4級</option>
					</select>`);
				$(`#urinelevel${index}`).val(td_level).change();
				obj.find('td:eq(1)').html(`<select class="form-control input-sm" style="width: 15rem;" id="urinename${index}"></select>`);
				getDruglevel($('#urinelevel'+index), $('#urinename'+index));
				
				obj.find('td:eq(2)').html(`<button type="button" class="btn btn-warning btn-xs" onclick="updateUrineEvent(\'update\', $('#urinetr${index}'))"><span class="fa fa-floppy-o" aria-hidden="true"></span></button>`);
				setTimeout(() => {
					$(`#urinename${index} option`).each(function(){
						if($(this).val() == td_ind)
						{
							$(this).attr('selected', true);
							return false;
						}
						// else if($(this).val().indexOf(td_ind) > -1)
						// {
						// 	$(this).attr('selected', true);
						// 	// return false;
						// }
						else{
							$(`#urinename${index} option:eq(0)`).attr('selected', true);
						}
							
					});
                    // $(`#drugname${index}`).val(td_ind).change();
                  }, 500);
				
			}
			function removeUrineEvent(obj)
			{
				obj.remove();
			}
			function updateUrineEvent(type, obj){
				var mapped_u_ind = obj.find('select:eq(1)').val();
				var mapped_u_level = obj.find('select:eq(0)').val();
				var mapped_u_id = obj.attr('data-id');

				let formdata = new FormData();
				formdata.append('method', type);
				formdata.append('sc_cnum', $("input[name='s_cnum']").val());
				formdata.append('sc_snum', $("input[name='s_num']").val());
				formdata.append('sc_ingredient', mapped_u_ind);
				formdata.append('sc_level', mapped_u_level);
				formdata.append('sc_num', mapped_u_id);
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url("disciplinary_c/addUrineTest") ?>',
					dataType: 'json',
					// data: {'addr': obj.val()},
					data: formdata,
					processData: false,
					contentType: false,
					cache: false,
					success: function(data){
						// console.log(data);
						postdata = data.data;
						if(postdata.indexOf('error') == -1)
						{
							postdata = data.data;
							objid = obj.attr('id');
							obj.find('td:eq(0)').html(postdata[0].sc_level + '級');
							obj.find('td:eq(1)').html(postdata[0].sc_ingredient);
							obj.find('td:eq(2)').html('');
							obj.attr('data-id', postdata[0].sc_num);
							obj.attr('ondblclick', 'editUrineStateEvent($(this), '+objid.replace('urinetr','')+')')
							
						}
						else
						{
							swal({
								title: "注意!",
								text: "資料庫作業錯誤",
								icon: "warning"
							});
						}
					}
				});
			}
			function addSeltype(obj){
				$sel_str = $("#seltype").val();
				$fd_dis_msg_content = $("#fd_fact_text").val()
				$("#fd_fact_text").val($fd_dis_msg_content + $sel_str);
			}
        </script>

    </body>
</html>
