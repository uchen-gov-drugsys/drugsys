        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                    <!-- <li><a><input type="button" value="新增專案" class="btn btn-default" style="padding:0px 0px;" id="yes"></a></li>
                    <li><a><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#exampleModal">加入舊專案</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a><input type="button" value="新增專案" class="btn btn-success"  id="yes"></a>
                            <a><button class="btn btn-default"data-toggle="modal" data-target="#exampleModal">加入舊專案</button></a>
                        </div>
                    </div>
                <?php echo form_open_multipart('disciplinary_c/addcasesother','id="addcases"') ?>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    公函基資
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                        <!-- <label>發文字號</label> -->
                                            <label>發文日期</label>                                            
                                            <input id="s_go_no" type="text" name="s_go_no" value="<?php echo (isset($susp->s_go_no))?((strlen($susp->s_go_no) > 7 && $susp->s_go_no != '0000-00-00')?str_pad(((int)substr($susp->s_go_no, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->s_go_no, 5, 2).substr($susp->s_go_no, 8, 2):''):'';?>" class="rcdate form-control"> 
                                            <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                            <?php //echo form_input('s_go_no',$susp->s_go_no, 'class="form-control"')?>
                                        </div>
                                        <div class="form-group col-md-12">
                                        <!-- <label>移送分局</label> -->
                                            <label>發文單位</label>
                                            <?php echo form_input('s_roffice',$cases->r_office, 'class="form-control"')?>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <?php echo form_hidden('c_num',$c_num)?>
                                            <?php echo form_hidden('s_num',$susp->s_num)?>
                                            <?php echo form_hidden('link','')?>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>依據單位(字)</label>
                                            <?php echo form_input('s_roffice1',$cases->r_office1, 'class="form-control"')?>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>依據字號</label>
                                            <?php echo form_input('s_fno',$susp->s_fno, 'class="form-control"')?>
                                        </div>
                                    </div>                     
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                        <div class="col-lg-9">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    戶役政、筆錄、搜扣筆錄基本資料
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label>受處分人姓名</label>
                                            <?php echo form_input('s_name',$susp->s_name, 'class="form-control"')?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>出生日期</label>                                            
                                            <input id="s_birth" type="text" name="s_birth" value="<?php echo (isset($susp->s_birth))?((strlen($susp->s_birth) > 7 && $susp->s_birth != '0000-00-00')?str_pad(((int)substr($susp->s_birth, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->s_birth, 5, 2).substr($susp->s_birth, 8, 2):''):'';?>" class="rcdate form-control"> 
                                            <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="idoptions" id="idoptions1" value="id" checked> 身份證字號
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="idoptions" id="idoptions2" value="passport"> 護照號碼
                                                </label>
                                            </label>
                                            <?php echo form_input('s_ic',$susp->s_ic, 'class="form-control"')?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label>戶籍地址</label>
                                            <div id="zipcode">
                                            </div>
                                            <?php echo form_input('s_dpaddress',$susp->s_dpaddress, 'class="form-control" id="s_no"')?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>現住地址
                                                <label class='receiverchange checkbox-inline checkboxeach '><input id='receiverchange' class='receiverchange btn btn-default btn-sm' type='button' name='receiverchange' value='戶籍地與現居地相同' ></label>
                                            </label>
                                            <div id="zipcode2">
                                            </div>
                                            <?php echo form_input('s_rpaddress',$susp->s_rpaddress, 'class="form-control" id="s_no2"')?>
                                            
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>查獲日期</label>
                                            
                                            <input type="text" name="s_date" value="<?php echo (isset($cases->s_date))?((strlen($cases->s_date) > 7 && $cases->s_date != '0000-00-00')?str_pad(((int)substr($cases->s_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($cases->s_date, 5, 2).substr($cases->s_date, 8, 2):''):'';?>" class="rcdate form-control">      
                                            <input type="hidden" name="s_status" value="" class="form-control" id="s_status">  
                                            <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                            <br>
                                            <label>查獲時間</label>
                                            
                                            <input type="text" name="s_time" value="<?php echo (isset($cases->s_time))?$cases->s_time:'';?>" class="form-control">      
                                            <!-- <input type="hidden" name="s_status" value="" class="form-control" id="s_status">   -->
                                            <span class="text-danger"><small>(如：晚上9點55分，請輸入21.55)</small> </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label>戶役政/筆錄/搜扣</br></label>
                                            <?php 
                                                if($cases->doc_file == ""){
                                                    echo form_upload('doc_file');
                                                }
                                                else {
                                                    echo anchor_popup('uploads/' . $cases->doc_file, '</br>下載戶役政/筆錄/搜扣');
                                                    //echo form_hidden('doc_file',$row->doc_file);
                                                }
                                            ?>  
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>司法文書</br></label>
                                            <?php 
                                                if($susp->sp_doc == ""){
                                                    echo form_upload('sp_doc');
                                                }
                                                else {
                                                    echo anchor_popup('drugdoc/' . $susp->sp_doc, '</br>下載司法文書');
                                                }?> 
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>查獲地</label>
                                            <?php echo form_input('s_place',$cases->s_place, 'class="form-control"')?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4 col-md-offset-8">
                                            <label>查獲單位</label>
                                            <?php echo form_input('s_office',$cases->s_office, 'class="form-control"')?>
                                        </div>
                                    </div>
                                    </div>                                                       
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            持有毒品
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <label><button id='drug'  type="button" class="btn btn-default btn-sm" >新增</button></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div id="new" class="row new">
                                    <?php echo $drug_table; ?>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    尿報
                                </div>
                                <div class="panel-body">
                                    <div id="new" class="row new">
                                        <table class="table">
                                                    <tbody>
                                                        <tr><td><label>尿液編號</label><?php echo form_input('s_utest_num',$susp->s_utest_num, 'class="form-control"')?></td>
                                                        <td style="width:300px">
                                                            <div id="drug_s">
                                                                <label>尿液成分</label>
                                                                    <?php 
                                                                    foreach ($susp2 as $susp2)
                                                                    {
                                                                        echo '<div id="row">';
                                                                        echo form_dropdown('sc[]',$opt1 ,$susp2->sc_level .'級'. $susp2->sc_ingredient , 'class="form-control" id="sel"');
                                                                        echo form_hidden('sc_num[]',$susp2->sc_num); 
                                                                        echo anchor('disciplinary_c/deleteSuspCk/'. $susp2->sc_num,'X','class="btn btn-danger btn_remove"');
                                                                        //echo '<button type="button" class="btn btn-danger btn_remove">X</button></';
                                                                        echo '</div>';
                                                                    }   
                                                                    ?>                                                            
                                                            </div>
                                                            <button id='susp' type="button" class="btn btn-success" >新增成分</button>
                                                        </td>
                                                        <td  style="width:10px">
                                                            <label>尿報</br></label>
                                                                <?php 
                                                                if($susp->s_utest_doc == ""){
                                                                    echo form_upload('s_utest_doc');
                                                                }
                                                                else {
                                                                    echo anchor_popup('utest/' . $susp->s_utest_doc, '</br>尿報');
                                                                    echo form_hidden('s_utest_doc',$susp->s_utest_doc);
                                                                }
                                                            ?>                                                            
                                                        </td></tr>
                                                    </tbody>
                                                </table>

                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          <div class="form-group">
                                            <label>舊專案</label>
                                            <?php echo form_dropdown('dp_num',$opt ,'', 'class="form-control" id="sel"')?>                                                            
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button id='no' type="button" class="btn btn-default" >加入舊專案</button>
                                      </div>
                                    </div>
                                </div>
                            </div>         
                    </div>
                <!-- /.container-fluid -->
                </div>
            <!-- /#page-wrapper -->
            </div>
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
            $(document).ready(function() {
                $('#fadai').hide();//用ifelse判斷是否大於20歲
                //if($('#s_birth').val())
                var age = 100;
                $("#s_birth").change(function(){
                    var today = new Date();
                    var birthdate = new Date(tranfer2ADyear($(this).val()));
                    age = today.getFullYear() - birthdate.getFullYear();
                    var m = today.getMonth() - birthdate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
                        age--;
                    }
                });    
                $(".rcdate").change(function(){
                    let strlen = $(this).val().length;
                    if(strlen < 7 || strlen < "7")
                    {
                        if(strlen != 0)
                            $(this).val("0"+$(this).val());
                    }
                    else
                    {
                        return false;
                    }
                        
                });
                $(".rcdate").keypress(function(){
                    if($(this).val().length >= 7)
                        return false;
                });          
            $( "#addcases" ).validate({
                    rules: {
                        s_name: {
                            required: true,
                            minlength: 2
                        },
                        s_ic: {
                            required: true,
                            minlength: 9
                        },
                        s_birth: {
                            required: true,
                        },
                        s_dpdistrict: {
                            required: true,
                        },
                        s_dpaddress: {
                            required: true,
                        },
                    },
                    messages: {
                        s_name: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        s_ic: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        s_dpdistrict: {
                            required: "此欄位不得為空",
                        },
                        s_dpaddress: {
                            required: "此欄位不得為空",
                        },
                    }
            });  
                $("#yes").click(function (){
                    $("#s_status").val('1');
                    if(age < 20){
                        $('#fadai').show();
                        if($('#fadaival').val()!=""){
                            $("#addcases").submit();
                        }else{alert('受處分人未滿20歲，請輸入法代人資料');}
                    }else{
                        $("#addcases").submit();
                    }
                });
                $("#no").click(function (){
                    $("#s_status").val('0');
                    if(age < 20){
                        $('#fadai').show();
                        if($('#fadaival').val()!=""){
                            $("#addcases").submit();
                        }else{alert('受處分人未滿20歲，請輸入法代人資料');}
                    }else{
                        $("#addcases").submit();
                    }
                });
                $("#drug").click(function (){
                    $("#s_status").val('88');
                    $("#link").val('disciplinary_c/editdrug/'+$("#c_num").val());
                    if(age < 20){
                        $('#fadai').show();
                        if($('#fadaival').val()!=""){
                            $("#addcases").submit();
                        }
                        else{alert('受處分人未滿20歲，請輸入法代人資料');}
                    }else{
                        $("#addcases").submit();
                    }
                });
                $("#susp").click(function (){
                    $("#s_status").val('99');
                    //alert($("#link").val());
                    if(age < 20){
                        $('#fadai').show();
                        if($('#fadaival').val()!=""){
                            $("#addcases").submit();
                        }else{alert('受處分人未滿20歲，請輸入法代人資料');}
                    }else{
                        $("#addcases").submit();
                    }
                });
        });  
            $("#zipcode").twzipcode({
            "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_dpcounty',   // 預設值為 county
                'districtName' : 's_dpdistrict', // 預設值為 district
                'zipcodeName'  : 's_dpzipcode' ,// 預設值為 zipcode
                'zipcodeSel'   : <?php if(isset($susp->s_dpzipcode)){echo $susp->s_dpzipcode;}else{echo '100';}  ?>
            });         
            $("#zipcode2").twzipcode({
            "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_rpcounty',   // 預設值為 county
                'districtName' : 's_rpdistrict', // 預設值為 district
                'zipcodeName'  : 's_rpzipcode', // 預設值為 zipcode
                'zipcodeSel'   : <?php if(isset($susp->s_rpzipcode)){echo $susp->s_rpzipcode;}else{echo '100';}  ?>
            });  
            $("#zipcode3").twzipcode({
            "zipcodeIntoDistrict": true,
            "css": ["city form-control", "town form-control"],
                'countyName'   : 's_fadai_county',   // 預設值為 county
                'districtName' : 's_fadai_district', // 預設值為 district
                'zipcodeName'  : 's_fadai_zipcode', // 預設值為 zipcode
                'zipcodeSel'   : <?php if(isset($suspfadai->s_fadai_zipcode)){echo $suspfadai->s_fadai_zipcode;}else{echo '100';}  ?>
            }); 
 
                $('#receiverchange').on('click', function() {
                   // $("#zipcode2").empty();
                    var city = $(zipcode).twzipcode('get', 'city');
                    var zipc = $(zipcode).twzipcode('get', 'zipcode');
                    //console.log(zipc);   // 縣市
                    if( zipc == ""){
                        alert("請務必選擇鄉鎮市區");return false;
                    } 
                    $("#zipcode2").twzipcode('set', zipc[0]);
                    var sno=$("#s_no").val();

                    // 移除空格確定是否空值
                    if (!$.trim($("#s_no").val())) {
                        if( $("#s_no").val()==""){
                            $("#s_no").focus();
                            alert("請務必填入路門牌");return false;
                        }     
                    }   
                    $("#s_no2").val(sno);
                }); 
            
                
        /** 轉西元年 */
        function tranfer2ADyear(date)
        {
            if(date.substr(0, 1) == 0 || date.substr(0, 1) == '0')
            {
                date = date.substr(1, 6);            
            }
                
            if(date.length == 6)
            {
                ad = (parseInt(date.substr(0, 2)) + 1911);
                return ad.toString() + '-' + date.substr(2, 2) + '-' + date.substr(4, 2);
            }
            else if(date.length == 7)
            {
                
                ad = (parseInt(date.substr(0, 3)) + 1911);
                return ad.toString() + '-' + date.substr(3, 2) + '-' + date.substr(5, 2);
            }
            else
            {
                return '';
            }

            
        }    
        </script>
