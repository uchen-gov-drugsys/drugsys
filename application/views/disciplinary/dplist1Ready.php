        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><button id='edit' class="btn btn-default"style="padding:0px 0px;" >線上處分書校稿</button></a></li>
                    <li><a href=<?php //echo base_url('PDFcreate/fine_doc_project/'.$dp_name)?>><button class="btn btn-default"style="padding:0px 0px;" >下載處分書(稿)</button></a></li>
                    <li><a href=<?php //echo base_url('PDFcreate/fine_doc_project_fin/'.$dp_name)?>><button class="btn btn-default"style="padding:0px 0px;" >下載處分書(正本)</button></a></li>
                    <li><a href=<?php //echo base_url('Disciplinary_c/listDpProject_Ready/'.$dp_name)?>><button class="btn btn-default"style="padding:0px 0px;" >返回列表</button></a></li>
                    <li><a href=<?php //echo base_url('Disciplinary_c/listDpProject_Ready/'.$dp_name)?>><button class="btn btn-default"style="padding:0px 0px;" >處分書開立完成(正本)</button></a></li> -->
                    <!--li><button id='yes' class="btn btn-default"style="padding:0px 0px;" >確認修改</button></li-->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
            
                <div class="container-fluid"> 
                    <div class="row" style="margin-top:35px;">
                        <div class="col-md-12">
                            <ol class="breadcrumb">
								<li class="active"><?php echo $url_1; ?></li>
                                <li><a href="<?php echo base_url($url);?>">送批列表</a></li>
                                <li class="active"><?php echo $title;?></li>
                            </ol>
                        </div>
                        <div class="col-md-12 text-right">
                            <button id='edit' class="btn btn-default btn-link" >線上處分書校稿</button>
                            <a href=<?php echo base_url('PDFcreate/fine_doc_project/'.$dp_num)?>><button class="btn btn-default btn-link">下載處分書(稿)</button></a>
                            <a href=<?php echo base_url('PDFcreate/fine_doc_project_fin/'.$dp_num)?>><button class="btn btn-default btn-link">下載處分書(正本)</button></a>
							<a href=<?php echo base_url('PDFcreate/fine_doc_project_delivery/'.$dp_num)?>><button class="btn btn-default btn-link">下載送達證書</button></a>
                            <!-- <a href=<?php //echo base_url('Disciplinary_c/listDpProject_Ready/'.$dp_name)?>><button class="btn btn-default">返回列表</button></a> -->
                            <a href=<?php echo base_url('Disciplinary_c/listDpProject_Ready')?>><button class="btn btn-default btn-link" >處分書開立完成(正本)</button></a>
							<?php
							echo (($rollback == 'Y')?'<a href="'. base_url('PDFcreate/fine_doc_public_fin/'.$dp_num).'"><button class="btn btn-link">下載公示處分書(正本)</button></a>':'');
							?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <blockquote style="letter-spacing:5px;">
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>

                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div  div class="panel-heading list_view">
									<div class="row">
										<div class="col-md-6">
											<h4 class="panel-title">列表清單</h4>
										</div>
										<div class="col-md-6 text-right">
											<button class="btn btn-default btn-sm" data-toggle="modal" data-target="#exampleModal">輸入發文日期</button>
											<button class="btn btn-default btn-sm" data-toggle="modal" data-target="#paydateModal">輸入承辦人</button>
										</div>
									
									</div>
                                </div>
                                <!--div class="panel-heading list_view">
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 案件編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 人員編號
                                    <input type="checkbox" name="list" data-target ="3" checked> 姓名
                                    <input type="checkbox" name="list" data-target ="4" checked> 證號
                                    <input type="checkbox" name="list" data-target ="5" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="6" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="7" checked> 犯罪手法
                                    <input type="checkbox" name="list" data-target ="8" checked> 毒品號
                                    <input type="checkbox" name="list" data-target ="9" checked> 成分
                                    <input type="checkbox" name="list" data-target ="10" checked> 級數
                                    <input type="checkbox" name="list" data-target ="11" checked> 淨重
                                    <input type="checkbox" name="list" data-target ="12" checked> 純質淨重
                                    <input type="checkbox" name="list" data-target ="13" checked> 建議金額
                                </div-->
                                <div class="panel-body">
									<div class="row">
										<div class="col-md-12">
											<form class="row">
												<!-- <div class="form-group col-md-2">
													<label>發文字號</label>
													<h4><span class="label label-default"><?php echo $dp_send_no; ?></span></h4>
												</div> -->
												<div class="form-group col-md-2">
													<label>發文日期</label>
													<h4><span class="label label-default"><?php echo (($dp_send_date == '-19110000' || $dp_send_date == '-1911')?'':$dp_send_date); ?></span></h4>
												</div>
												<div class="form-group col-md-2">
													<label>專案承辦人</label>
													<h4><span class="label label-default"><?php echo ((isset($dp_empno))?$dp_empno:''); ?></span></h4>
												</div>
											</form>
										</div>
									</div>
									<hr>
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
								<?php echo form_open_multipart('disciplinary_c/updatestatus_ready','id="uploaddpdate"') ?>
                                <input id="s_cnum" type="hidden" name="s_cnum" value='<?php echo $id; ?>'> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value='<?php echo $id; ?>'> 
                                <input id="s_status" type="hidden" name="s_status" value='0'> 
                                <input id="dp_status" type="hidden" name="dp_status" value='<?php echo $dp_status;?>'> 
                                <input id="dp_num" type="hidden" name="dp_num" value='<?php echo $dp_num; ?>'> 
								<?php  echo form_close();  ?>
                           </div>
                            <!-- /.panel -->
                        </div>
                            <!-- 發文視窗 -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title">發文日期</h4>
                                  </div>
								  <?php echo form_open('disciplinary_c/uploaddpdate/'.$id); ?>
                                  <div class="modal-body">
								  		<!-- <div class="form-group">
											<label>發文字號</label>
											<?php
											// $dp_send_no = array(
											// 		'name'          => 'dp_send_no',
											// 		'id'            => 'dp_send_no',
											// 		'class'         => 'form-control',
											// 		'value'			=> $dp_send_no
											// );
											
											// echo form_input($dp_send_no);
											?>
                                      </div> -->
                                      <div class="form-group">
                                        <label>輸入發文日期</label>
										<?php
										$dp_send_date = array(
												'name'          => 'dp_send_date',
												'id'            => 'dp_send_date',
												'class'         => 'rcdate form-control',
												'value'			=> (($dp_send_date == '-1911')?'':$dp_send_date)
										);
										
										echo form_input($dp_send_date);
										echo form_hidden('dp_num', $dp_num);
										?>
                                        <!-- <input name="dp_send_date" type="text" class="rcdate form-control">                                      -->
										<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>                      
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
									<?php echo form_submit('save_dpdate', '儲存', "class='btn btn-default'"); ?>
                                    <!-- <button id='no' class="btn btn-default" >儲存</button> -->
                                  </div>
								  <?php  echo form_close();  ?>
                                </div>
                              </div>
                            </div> 
							<!-- 繳款視窗 -->
                            <div class="modal fade" id="paydateModal" tabindex="-1" role="dialog" aria-labelledby="paydateModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title">繳款日期</h4>
                                  </div>
								  <?php echo form_open('disciplinary_c/uploaddpexpdate/'.$id); ?>
                                  <div class="modal-body">
                                      <div class="form-group hidden">
                                        <label>輸入繳款日期</label>
										<?php
										$dp_send_date = array(
												'name'          => 'f_date',
												'id'            => 'f_date',
												'class'         => 'rcdate form-control',
												'value'			=> ($dp_expdate == '-1911' || $dp_expdate == '-19110000')?'':$dp_expdate
										);
										
										echo form_input($dp_send_date);
										echo form_hidden('dp_num', $dp_num);
										?>
                                        <!-- <input name="f_date" type="text" class="rcdate form-control">                 -->                     
										<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>                       
                                      </div>
									  <div class="form-group">
                                        <label>專案承辦人</label>
										<br>
										<label class="text-primary"><?php echo ((isset($dp_empno))?$dp_empno:''); ?></label>
										<select name="dp_empno" id="dp_empno" style="width: 100%" class="form-control js-example-basic-single js-states" ></select>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
									<?php echo form_submit('save_dpdate', '儲存', "class='btn btn-default'"); ?>
                                    <!-- <button id='no' class="btn btn-default" >儲存</button> -->
                                  </div>
								  <?php  echo form_close();  ?>
                                </div>
                              </div>
                            </div> 
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </form>
                </div>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
                // $( "#uploaddpdate" ).validate({
                //     rules: {
                //         dp_send_date: {
                //             required: true,
                //         },
                //     },
                //     messages: {
                //         dp_send_date: {
                //             required: "此欄位不得為空",
                //         },
                //     }
                // });        
           var table = $('#table1').DataTable({
               "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    },
                ],
                dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                },
           });
            $("#edit").click(function (){
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);     
                //alert($('#s_cnum1').val());
                window.location.href = "<?php echo base_url('disciplinary_c/editSanc_ready/'); ?>" +$('#s_cnum1').val() + '<?php echo ((null !== $this->uri->segment(3))?"/" .$this->uri->segment(3):''); ?>';
                //$("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);     
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);
                //alert($('#s_cnum1').val());
            e.preventDefault();
           });
		   $('#dp_empno').select2({
				placeholder: "選擇其他承辦人",
				ajax: {
					url: '<?php echo base_url("disciplinary_c/getthirdempno?unselected=".$dp_empno) ?>',
					dataType: 'json',
					data: function (params) {
					var query = {
						search: params.term,
						type: 'public'
					}

					// Query parameters will be ?search=[term]&type=public
					return query;
					}
				}
			});
			$('#paydateModal').on('hide.bs.modal', function (event) {
				$('#dp_empno').val('').change(); 
			});
    });
    </script>
