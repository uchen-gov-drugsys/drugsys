        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><button id='yes' class="btn btn-default"style="padding:0px 0px;" >建立專案</button></li>
                    <li><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#exampleModal">加入舊專案</button></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <!-- <button id='yes' class="btn btn-success">建立專案</button>
                            <button class="btn btn-default"  data-toggle="modal" data-target="#exampleModal">加入舊專案</button> -->
                        </div>
                    </div>
                    <!-- <div class="row pb-0">
                        <div class="col-md-12">
                            <div class="panel panel-info" >
                                <div class="panel-heading">
                                    匯入作業          
									<a href="<?php echo base_url('upload_sample/個案_匯入格式.xlsx'); ?>"><span class="label label-default pull-right">匯入格式下載</span></a>                          
                                </div>       
                                <div class="panel-body">
                                    <form class="form-inline" id="importForm" name="spreadsheet" >
                                        <div class="row">                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>上傳檔案</label>
                                                    <input type="file" size="40px" name="upload_file" accept=".csv,.xls,.xlsx"/>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <input class="btn btn-default" type="button" value="匯入資料" id="importBT"/>
                                            </div>
                                            
                                        </div>
                                        
                                    </form>
                                    <p id="uploadtxt" class="text-danger"></p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div id="loading" class="row hidden" style="
                                        position: absolute;
                                        width: 100%;
                                        height: 100%;
                                        background-color: rgba(0,0,0,0.3);
                                        z-index: 1030;
                                    ">
                            <div class="loader" id="loader-1"></div>
                            <p class="text-danger text-center" id="loadingTxt" style="background-color:#fff;"></p>
                    </div> -->
                    <div class="row">
                        <div class="col-lg-6" >
							
                            <div class="panel panel-primary">
                            
                                <div class="panel-heading list_view">
									<div class="row">
										<div class="col-md-6">
											<h3 class="panel-title">列表清單（自一、二階）</h3>
										</div>
										<div class="col-md-6 text-right">
											<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#renamePJModal">建立專案</button>
											<button class="btn btn-default btn-sm"  data-toggle="modal" data-target="#exampleModal">加入舊專案</button>
										</div>
									</div>
								
                                    <!-- <input type="checkbox" name="list"  data-target ="1"  checked> 案件編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 發文字號
                                    <input type="checkbox" name="list" data-target ="3" checked> 受處分人
                                    <input type="checkbox" name="list" data-target ="4" checked> 身份證號
                                    <input type="checkbox" name="list" data-target ="5" checked> 移送分局
                                    <input type="checkbox" name="list" data-target ="6" checked> 依據單位(字)
                                    <input type="checkbox" name="list" data-target ="7" checked> 戶籍
                                    <input type="checkbox" name="list" data-target ="8" checked> 現住地址
                                    <input type="checkbox" name="list" data-target ="9" checked> 出生日期
                                    <input type="checkbox" name="list" data-target ="10" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="11" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="12" checked> 查獲單位
                                    <input type="checkbox" name="list" data-target ="13" checked> 檢驗成分
                                    <input type="checkbox" name="list" data-target ="14" checked> 尿液編號
                                    <input type="checkbox" name="list" data-target ="15" checked> 持有毒品
                                    <input type="checkbox" name="list" data-target ="16" checked> 毒報
                                    <input type="checkbox" name="list" data-target ="17" checked> 尿報
                                    <input type="checkbox" name="list" data-target ="18" checked> 編輯 -->
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
								<form id="sp_checkbox">
								<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
								<input id="s_status" type="hidden" name="s_status" value=''> 
								<input id="front_word" type="hidden" name="front_word" value=''> 
								<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label>舊專案</label>
												<?php echo form_dropdown('dp_num',$opt ,'', 'class="form-control" id="sel"')?>                                                            
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
											<button id='no' class="btn btn-default"  type="button">加入舊專案</button>
										</div>
										</div>
									</div>
								</div>  
								<div class="modal fade" id="renamePJModal" tabindex="-1" role="dialog" aria-labelledby="renamePJModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="renamePJModalLabel">建立專案</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label>專案名稱</label>
												<?php echo form_input('pjname','', 'class="form-control"');?>                                                            
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
											<button id='yes' class="btn btn-default" type="button">確定</button>
										</div>
										</div>
									</div>
								</div>  
								</form>  
               				</div>
                            <!-- /.panel -->       
							
                        </div>
                        <!-- /.col-lg-12 -->
						<div class="col-lg-6" >
							
							<div class="panel panel-danger" >
								<div class="panel-heading">
									<div class="row">
										<div class="col-md-8">											
											<h3 class="panel-title">待重處列表（退回、重處）<small>一般重處與公示重處請分開專案建立</small></h3>
										</div>
										<div class="col-md-4 text-right">
											<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#renamerbPJModal">建立專案</button>
											<button class="btn btn-default btn-sm"  data-toggle="modal" data-target="#rollbackModal">加入舊專案</button>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
                                        <?php echo $roll_table; ?>
                                    </div>  
								</div>
								<form id="sp_checkbox_rb">
								<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
								<input id="s_status" type="hidden" name="s_status" value=''>
								<input id="front_word" type="hidden" name="front_word" value='P'> 
								<div class="modal fade" id="rollbackModal" tabindex="-1" role="dialog" aria-labelledby="rollbackModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label>舊專案</label>
												<?php echo form_dropdown('dp_num',$rb_opt ,'', 'class="form-control" id="sel"')?>                                                            
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button id='no_rb' class="btn btn-default"  type="button">加入舊專案</button>
										</div>
										</div>
									</div>
								</div> 
								<div class="modal fade" id="renamerbPJModal" tabindex="-1" role="dialog" aria-labelledby="renamerbPJModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="renamerbPJModalLabel">建立專案</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label>專案名稱</label>
												<?php echo form_input('pjname','', 'class="form-control"');?>                                                            
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
											<button id='yes_rb' class="btn btn-default"  type="button">確定</button>
										</div>
										</div>
									</div>
								</div>
								</form>  
							</div>
						</div>
                        <!-- <div class="col-lg-12"> -->
                            <!-- <div class="panel panel-default"> -->
                                <!-- <div class="panel-body"> -->
                                    <!-- <a class="media" href="test.pdf"></a>     -->
                                <!-- </div> -->
                                <!-- /.panel-body -->
                            <!-- </div> -->
                            <!-- /.panel -->
                        <!-- </div>                     -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           table = $('#table1').DataTable({
				'columnDefs': [
					{
							targets:   0,
							visible: false
					},
					{
						'targets': 1,
						"orderable": false,
						className: 'select-checkbox'
						// 'checkboxes': {
						//    'selectRow': true
						// }
					}
				],
				select: {
							style:    'os',
							selector: 'td:first-child',
							style: 'multi'
				},
				stateSave: true,
				stateSaveCallback: function(settings,data) {
					_iDisplayStart = settings._iDisplayStart;
				},
              "width":"100px",
			  "fnRowCallback": function (nRow, aData, iDisplayIndex) {
					// $("td:first", nRow).html(iDisplayIndex + 1); //自動序號
					$(nRow).attr("data-id", aData[0]);
				},
              dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
            //   'order': [[1, 'asc']]
           });
		   $('#table1 thead tr:eq(0) th:eq(0)').html('<span class="label label-default" id="select10">一次10筆</span>');
		   $("#select10").on( "click", function(e) {			   
				$(this).toggleClass('selected10');
				if ($(this).hasClass('selected10')) {
					for (let index = _iDisplayStart; index < (_iDisplayStart+10); index++) {
						table.rows(index).select(); 
					}
					       
				} else {
					for (let index = _iDisplayStart; index < (_iDisplayStart+10); index++) {
						table.rows(index).deselect(); 
					}
				}
			});

		   table_rollback = $('#table_rollback').DataTable({
			'columnDefs': [
				{
						targets:   0,
						visible: false
				},
				{
				'targets': 1,
				"orderable": false,
				className: 'select-checkbox'
				// 'checkboxes': {
				//    'selectRow': true
				// }
				}
              ],
			  select: {
						style:    'os',
						selector: 'td:first-child',
						style: 'multi'
			},
			  stateSave: true,
				stateSaveCallback: function(settings,data) {
					_iDisplayStart_rb = settings._iDisplayStart;
			},
            //   'select': {
            //      'style': 'multi'
            //   },
              "width":"100px",
			  "fnRowCallback": function (nRow, aData, iDisplayIndex) {
					// $("td:first", nRow).html(iDisplayIndex + 1); //自動序號
					$(nRow).attr("data-id", aData[0]);
				},
              dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
            //   'order': [[1, 'asc']]
           });

		   // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });
                  $("#importBT").click(function(){
                        $('#loading').removeClass('hidden');
                        importEvent();
                    });

			$("#yes").click(function (){
				addProjectEvent('1', $("#sp_checkbox"), table);
			});

			$("#no").click(function (){
				addProjectEvent('0', $("#sp_checkbox"), table);
			});

			$("#yes_rb").click(function (){
				addProjectEvent('1', $("#sp_checkbox_rb"), table_rollback);
			});

			$("#no_rb").click(function (){
				addProjectEvent('0', $("#sp_checkbox_rb"), table_rollback);
			});
            // $("#yes").click(function (){
            //     $("#s_status").val('1');
            //         //alert("Submitted");
            //         $("#sp_checkbox").submit();
            // });
            // $("#no").click(function (){
            //     $("#s_status").val('0');
            //         //alert("Submitted");
            //         $("#sp_checkbox").submit();
            // });
            $("#yes1").click(function (){
                $("#s_status").val('2');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });

           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
              var form = this;

              var rows_selected = table.column(0).checkboxes.selected();

              // Iterate over all selected checkboxes
              /*$.each(rows_selected, function(index, rowId){
                 // Create a hidden element
                 $(form).append(
                     $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'id[]')
                        .val(rowId)
                 );
              });*/
                $('#example-console-rows').text(rows_selected.join(","));
                $('#s_cnum').val(rows_selected.join(","));
              
                // Output form data to a console     
                //$('#example-console-form').text($(form).serialize());
               
                // Remove added elements
                $('input[name="id\[\]"]', form).remove();
               
                // Prevent actual form submission
                //e.preventDefault();
           });
        });
        function importEvent(){
        $.ajax({            
            type: 'POST',
            url: '<?php echo base_url(); ?>PhpspreadsheetController/surcharge_import',
            data: new FormData($("#importForm")[0]),
            // dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
                beforeSend:function(){
                $("#loadingTxt").text("資料匯入中...請稍候...請勿關掉或跳出畫面喔！電腦會壞掉!");
            },
            error:function(){
                $('#loading').addClass('hidden');
                $("#uploadtxt").text("資料匯入出現錯誤，請重新執行操作！");
            },
            success: function(resp){
                $('#loading').addClass('hidden');
                if(resp == 'ok'){
                    $("#uploadtxt").text("資料匯入成功，可以進一步搜尋資料囉！");
                    $("#importForm")[0].reset();
                    location.reload();
                }else if(resp == 'error'){
                    $("#uploadtxt").text("資料匯入失敗，請重新執行操作！");
                }
            },
            complete:function(){
                $("#uploadtxt").text("資料匯入成功，可以進一步搜尋資料囉！");
                $("#importForm")[0].reset();
                // location.reload();
            }
        });
    	}

		function addProjectEvent(s_status, form, select_table)
    	{
			var formi = form;

			var rows_selected = $.map(select_table.rows('.selected').nodes(), function (item) {return $(item).attr("data-id");});
			// var rows_selected = select_table.column(0).checkboxes.selected();
			$('#example-console-rows').text(rows_selected.join(","));
			form.find('#s_cnum').val(rows_selected.join(","));
			form.find("#s_status").val(s_status);

			// Remove added elements
			$('input[name="id\[\]"]', formi).remove();
            
            $.ajax({            
                type: 'POST',
                url: '<?php echo base_url('disciplinary_c/updatedpproject') ?>',
                data: new FormData(form[0]),
                // dataType: 'json',
                processData : false, 
                contentType: false,
                cache: false,
                error:function(){
                    // console.log('error')
                },
                success: function(resp){
                    
                    // console.log(resp);
                },
                complete:function(resp){
                    // location.href = `../${resp.responseText}`;
					// console.log(resp);
					// return false;
                    swal({
                        title: "成功!",
                        text: "已加入處理專案!",
                        icon: "success",
                        buttons: {
                            goto: {
                            text: "前往專案處理",
                            value: "goto",
                            },
                            ok: {
								text: "好",
                            	value: "ok",
                            },
                        },
                    }).then((value) => {
                        switch (value) {                        
                            case "goto":
                            	location.href = resp.responseText;
                            break;
							case "ok":
								location.reload();
                            break;                        
                            default:
                        }
                    });
                    // location.reload();
                    // console.log(resp)
                }
            });
            
    	}
</script>
