        <style>
			#drugListTable tbody tr.selected {
				background-color: #f5f5f5;
			}
		</style>
		<div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        if (!$this -> session -> userdata('uic') && $this->uri->segment(1) != 'login' && $this->uri->segment(1) != '' && $this->uri->segment(2) != ''){
                            $this->output
                                    ->set_status_header(403)
                                    ->set_content_type('text/html')
                                    ->set_output(file_get_contents( $this->load->view('403')))
                                    ->_display();
                            exit;
                        }
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);

                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><a href="#"><?php //echo $title?></a></li> -->
                    <!--li><a href="#"><button style="padding:0px 0px;" id='odoc' class="btn btn-default" >公文(函)</button></a></li-->
                    <!-- <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='result'>監所查詢結果</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='drugdoc'>毒報</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='suspdoc'>尿報</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='casedoc'>筆錄/搜扣/戶役政</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='judoc'>司法文書(判決書/不起訴處分書/緩起訴/他縣市裁罰書)</button></a></li>
                    <?php //if($prev != NULL){?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='prev'>上一個</button></a></li>
                    <?php //}else{ ?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">第一筆</button></a></li>
                    <?php //} ?>
                    <?php //if($next != NULL){?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='next'>下一個</button></a></li>
                    <?php //}else{ ?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">最後一筆</button></a></li>
                    <?php //} ?>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" form="updateSanc">存儲</button></a></li> -->
                </ul>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href=<?php echo base_url("login/logout") ?>><i class="fa fa-sign-out fa-fw"></i> 登出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
                <!-- /.navbar-static-side -->
            <div id="page-wrapper" style="margin-left:20px;">
                <div class="container-fluid">
                    <div class="row" style="margin-top:35px;">
                        <div class="col-md-3">
                            <ol class="breadcrumb">
                                <li><a href="<?php echo base_url("disciplinary_c/listdp1/". $pjid);?>">每日處理專案: <?php echo $pjid;?></a></li>
                                <li class="active"><?php echo $title;?></li>
                            </ol>
                        </div>
                        
                        <div class="col-md-9 text-right">
                            <a href="#" class="btn btn-default" id='result'>監所查詢結果</a>
                            <a href="#" class="btn btn-default" id='drugdoc'>毒報</a>
                            <a href="#"><button class="btn btn-default" id='suspdoc'>尿報</button></a>
                            <a href="#"><button class="btn btn-default" id='casedoc'>筆錄/搜扣/戶役政</button></a>
                            <a href="#"><button class="btn btn-default" id='judoc'>司法文書(判決書/不起訴處分書/緩起訴/他縣市裁罰書)</button></a>
                            <?php if($prev != NULL){?>
                                <a href="#"><button class="btn btn-link" id='prev'>上一個</button></a>
                            <?php }else{ ?>
                                <a href="#"><button class="btn btn-link">第一筆</button></a>
                            <?php } ?>
                            <?php if($next != NULL){?>
                                <a href="#"><button class="btn btn-link" id='next'>下一個</button></a>
                            <?php }else{ ?>
                                <a href="#"><button class="btn btn-link">最後一筆</button></a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="alert alert-warning hidden text-center" role="alert" style="width:150px;position: absolute;top: 120px;left: calc(50% - 75px);z-index: 1030;">
                            <span class="text-muted" id="autoSavetxt"></span>   
                        </div>   
                        <div class="col-md-6" style="letter-spacing:5px;">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">     
                                  
                                      
                            <a href="#"><button class="btn btn-warning" form="updateSanc">儲存</button></a>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <form action="<?php echo base_url('disciplinary_c/updateSanc') ?>" id="updateSanc" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-body">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab1" data-toggle="tab">
											<span >【公文】</span>
											<span >【基本資料/在監】</span>
											<span >【法代人(未年滿20歲開啟)】</span>
										</a></li>
										<li class=""><a href="#tab2" data-toggle="tab">
											<span>【毒報/尿報】</span>
											<span>【開罰】</span>
											<span>【講習】</span>
											<span>【文件上傳】</span>
										</a></li>
									</ul>
									<div  class="tab-content" style="padding: 10px;">
										<div class="tab-pane fade in active" id="tab1">
											<blockquote>
												<p class="text-primary">公文</p>
											</blockquote>
											<div class="row">
                                                <div class="form-group col-md-3">
                                                    <label>處分書編號</label>
                                                    <?php
                                                        if(isset($sp)) {
                                                            echo form_input('fd_num',$sp->fd_num, 'class="form-control" readonly');
                                                        }
                                                        else {
                                                            echo form_input('fd_num',$fd_num, 'class="form-control" readonly');
                                                        }
                                                    ?>
                                                </div>
                                                <div class="form-group col-md-3 col-md-offset-6">
                                                    <label>承辦人姓名</label>
                                                    <?php echo form_input('fd_empno[]',$empno, 'class="form-control"')?>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <label>移送分局</label>
                                                    <?php echo form_input('s_roffice',(($cases->r_office)?$cases->r_office:''), 'class="form-control" readonly')?>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>依據單位(字)</label>
                                                    <?php echo form_input('s_roffice1',(($cases->r_office1)?$cases->r_office1:''), 'class="form-control" readonly')?>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>依據年月日</label>
                                                    <input id="s_fdate" name="s_fdate" type="text" class="rcdate form-control" value="<?php echo (isset($susp->s_fdate))?((strlen($susp->s_fdate) > 7 && $susp->s_fdate != '0000-00-00')?str_pad(((int)substr($susp->s_fdate, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->s_fdate, 5, 2).substr($susp->s_fdate, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md');?>">

                                                    <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                                    
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>依據字號</label>
                                                    <?php echo form_input('s_fno',$susp->s_fno, 'class="form-control"')?>
                                                        <input id="nextvalue" type="hidden" name="next" value=""> 
                                                        <input id="prevvalue" type="hidden" name="prev" value=""> 
                                                        <input type="hidden" name="pjid" value="<?php echo $pjid; ?>">
                                                </div>
                                            </div>
											<div class="row">
												<div class="form-group col-md-3">
                                                    <label>發文日</label>
                                                    
                                                    <input id="sendofficedate" name="fd_date" type="text" class="rcdate form-control" value="<?php echo (isset($dp->dp_send_date))?((strlen($dp->dp_send_date) > 7 && $dp->dp_send_date != '0000-00-00')?str_pad(((int)substr($dp->dp_send_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($dp->dp_send_date, 5, 2).substr($dp->dp_send_date, 8, 2):''):'';?>">
                                                    <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>發文字號</label>
                                                    <?php
                                                        if(isset($sp)) {
                                                            echo form_input('fd_send_num',$sp->fd_send_num, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_input('fd_send_num','', 'class="form-control"');
                                                        }
                                                    ?>
                                                </div>
											</div>
											<div class="row">
                                                <?php 
                                                if(null != $fd)
                                                {
                                                    if(count(explode(",", $fd->fd_address)) > 1)
                                                    {
                                                        $fd_name = explode(",", $fd->fd_target)[0];
                                                        $fd_addr = explode(",", $fd->fd_address)[0];
                                                        $fd_zip = explode(",", $fd->fd_zipcode)[0];
                                                    }
                                                    else
                                                    {
                                                        $fd_name = $fd->fd_target;
                                                        $fd_addr = $fd->fd_address;
                                                        $fd_zip = $fd->fd_zipcode;
                                                    }
                                                }
                                                else
                                                {
                                                    $fd_name = $susp->s_name;
                                                    $fd_addr = $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress;
                                                    $fd_zip = (isset($susp->s_dpzipcode)?$susp->s_dpzipcode:'');
                                                }
                                                ?>
                                                <div class="form-group col-md-3">
                                                    <label>發文對象</label>
                                                    <?php echo form_input('fd_target[]',$fd_name, 'class="form-control" id="fd_target_1"')?>                                                        
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>郵遞區號</label>
                                                    <?php //echo form_input('fd_zipcode[]',$fd_zip, 'class="form-control" id="zipcodefin"')?>
													<input type="text" class="form-control" id="zipcodefin" name="fd_zipcode[]"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>發文地址</label>
                                                    <?php 
                                                    
                                                        
                                                    echo form_input('fd_address[]',$fd_addr, 'class="form-control" id="address" onkeypress="listzipcode($(this), \'#zipcodefin\')"  onchange="listzipcode($(this), \'#zipcodefin\')"')?>
                                                </div>
                                                
                                            </div> 
                                            <?php
                                                list($year,$month,$day) = explode("-",$susp->s_birth);
                                                $year_diff = date("Y") - $year;
                                                $month_diff = date("m") - $month;
                                                $day_diff  = date("d") - $day;
                                                if ($day_diff < 0 || $month_diff < 0)
                                                    $year_diff--;

                                                if($year_diff < 20){
                                            ?>
                                            <div class="row">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="ifsend2susp" name="ifsend2susp" <?php echo (count(explode(",", $fd->fd_address)) > 1)?'checked':''; ?>/> 是否發給處分人
                                                    </label>
                                                </div>
                                            </div>  
                                            <div class="row">
                                                <?php
                                                if(isset($fd->fd_address))
                                                {
                                                    if(count(explode(",", $fd->fd_address)) > 1)
                                                    {
                                                        $fd_name = explode(",", $fd->fd_target)[1];
                                                        $fd_addr2 = explode(",", $fd->fd_address)[1];
                                                        $fd_zip2 = explode(",", $fd->fd_zipcode)[1];
                                                    }
                                                    else
                                                    {
                                                        $fd_name = $fd->fd_target;
                                                        $fd_addr2 = $fd->fd_address;
                                                        $fd_zip2 = $fd->fd_zipcode;
                                                    }
                                                }
                                                else
                                                {
                                                    $fd_name = $susp->s_name;
                                                    $fd_addr2 = $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress;
                                                    $fd_zip2 = (isset($susp->s_dpzipcode)?$susp->s_dpzipcode:'');
                                                }
                                                ?>
                                                <div class="form-group col-md-3">
                                                    <label>發文對象</label>
                                                    <?php echo form_input('fd_target[]',$fd_name, 'class="form-control"')?>                                                        
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>郵遞區號</label>
                                                    <?php echo form_input('fd_zipcode[]',$fd_zip2, 'class="form-control" id="zipcodefin2"')?>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>發文地址</label>
                                                    <?php 
                                                    
                                                    
                                                    echo form_input('fd_address[]',$fd_addr2, 'class="form-control" id="address2" onkeypress="listzipcode($(this), \'#zipcodefin2\')" onchange="listzipcode($(this), \'#zipcodefin2\')"')?>
                                                </div>
                                            </div>   
                                            <?php } ?> 
											<hr>
											<blockquote>
												<p class="text-primary">基本資料/在監</p>
											</blockquote>
											<div class="row">
                                                <div class="form-group col-md-2">
                                                    <label>受處分人姓名</label>
                                                    <?php echo form_input('s_name',$susp->s_name, 'class="form-control"')?>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>身份證字號</label>
                                                    <?php echo form_input('s_ic',$susp->s_ic, 'class="form-control"')?>
                                                </div>
                                                
                                                <div class="form-group col-md-2">
                                                    <label>出生日期</label>
                                                    <input id="s_birth" name="s_birth" type="text" class="rcdate form-control" value="<?php echo (isset($susp->s_birth))?((strlen($susp->s_birth) > 7 && $susp->s_birth != '0000-00-00')?str_pad(((int)substr($susp->s_birth, 0, 4) - 1911),3,"0",STR_PAD_LEFT).substr($susp->s_birth, 5, 2).substr($susp->s_birth, 8, 2):''):'';?>">

                                                    <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>  
                                                    <?php echo form_hidden('s_num',$susp->s_num)?>
                                                    <?php echo form_hidden('s_cnum',$susp->s_cnum)?>
                                                    <?php echo form_hidden('s_dp_project',$susp->s_dp_project)?>                                             
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>聯絡電話</label>
                                                    <?php
                                                        if(isset($phone)) {
                                                            echo form_input('p_no',$phone->p_no, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_input('p_no','', 'class="form-control"');
                                                        }
                                                    ?>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>在監狀況</label>
                                                    <?php echo form_dropdown('s_prison',$opt,$susp->s_prison,'class="form-control" onChange="copyAddress($(this))"') ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                        <div class="radio">
                                                            <label>
                                                                <input id="now" name="ad" type="radio" <?php echo (isset($susp->s_live_state) && $susp->s_live_state != 1)?'checked':''; ?> value="2">現居地地址(勾選是否送達)
                                                            </label>
                                                        </div>
                                                        <div id="zipcode"></div>
                                                        <input id="s_rpaddress" class="form-control" name="s_rpaddress" placeholder="e.g.XX街/路XX號" value="<?php echo (!empty($susp->s_prison))?explode(':', $susp->s_prison)[0] .'('.explode(':', $susp->s_prison)[1].')' :((isset($susp->s_rpcounty) && !empty($susp->s_rpcounty))?$susp->s_rpaddress:$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress) ?>">
                                                        
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="radio">
                                                        <label>
                                                            <input id="orgin" name="ad" type="radio" <?php echo ($susp->s_live_state == 1 || null == $susp->s_live_state)?'checked':''; ?> value="1">戶籍地址(勾選是否送達)
                                                        </label>
                                                    </div>
                                                    <div id="zipcode2"></div>
                                                    <input id="ori_rpaddress" name="s_dpaddress" class="form-control" placeholder="e.g.XX街/路XX號" value="<?php echo ((isset($susp->s_dpcounty) && !empty($susp->s_dpcounty))?$susp->s_dpaddress:$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress) ?>">
                                                </div>
                                                
                                                <div class="form-group col-md-2">
                                                    <label>在監證明</label>
                                                    <?php if($susp->s_prison_doc == null){ echo form_upload('s_prison_doc','','class="form-control" accept=".pdf"');}
                                                            else{
                                                                echo '已上傳';
                                                            }
                                                        ?>
                                                </div>
                                                <!-- <td colspan="2">
                                                        <div class="radio">
                                                        <label>
                                                            <input id="prison" name="ad" type="radio" value="">所在監所(勾選是否送達)
                                                        </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        監所查詢結果上傳
                                                    </td>-->
                                            </div>
											<hr>
											<?php
                                                list($year,$month,$day) = explode("-",$susp->s_birth);
                                                $year_diff = date("Y") - $year;
                                                $month_diff = date("m") - $month;
                                                $day_diff  = date("d") - $day;
                                                if ($day_diff < 0 || $month_diff < 0)
                                                    $year_diff--;

                                                if($year_diff < 20){
                                            ?>
											<blockquote>
												<p class="text-primary">法定代理人</p>
											</blockquote>
											<div class="row">
                                                <div class="form-group col-md-2">
                                                    <label>姓名</label>
                                                    <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_name',$suspfadai->s_fadai_name, 'class="form-control" id="s_fadai_name"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_name','', 'class="form-control" id="s_fadai_name"');
                                                        }?>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>身份證字號</label>
                                                    <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_ic',$suspfadai->s_fadai_ic, 'class="form-control" id="s_fadai_ic"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_ic','', 'class="form-control" id="s_fadai_ic"');
                                                        }?>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>性別</label>
                                                    <?php 
                                                        $options = array(
                                                            '男'=> '男',
                                                            '女'=> '女',
                                                        );
                                                        if(isset($suspfadai)) {
                                                            echo form_dropdown('s_fadai_gender',$options,$suspfadai->s_fadai_gender, 'class="form-control" id="s_fadai_gender"');
                                                        }
                                                             else {
                                                                 echo form_dropdown('s_fadai_gender',$options,'', 'class="form-control" id="s_fadai_gender"');
                                                        }?>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>出生日期</label>
                                                    <input id="s_fadai_birth" name="s_fadai_birth" type="text" class="rcdate form-control" value="<?php echo (isset($suspfadai->s_fadai_birth))?((strlen($suspfadai->s_fadai_birth) > 7 && $suspfadai->s_fadai_birth != '0000-00-00')?str_pad(((int)substr($suspfadai->s_fadai_birth, 0, 4) - 1911),3,"0",STR_PAD_LEFT).substr($suspfadai->s_fadai_birth, 5, 2).substr($suspfadai->s_fadai_birth, 8, 2):''):'';?>">
                                                    <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>     
                                                    
                                                </div>
                                                <div class="form-group col-md-2 hidden">
                                                    <label>聯絡電話</label>
                                                    <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_phone',$suspfadai->s_fadai_phone, 'class="form-control" id="s_fadai_phone"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_phone','', 'class="form-control" id="s_fadai_phone"');
                                                        }?>
                                                </div>
												<div class="form-group col-md-4">
                                                    <label>地址</label>
                                                    <div id="zipcode3"></div>
                                                        <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_address',$suspfadai->s_fadai_address, 'class="form-control" id="s_fadai_address" placeholder="e.g.XX路XX門牌"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_address',$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress, 'class="form-control" id="s_fadai_address" placeholder="e.g.XX路XX門牌"');
                                                        }?>
                                                </div>
                                            </div>
											<?php }?>
										</div>
										<div class="tab-pane fade" id="tab2">
											<blockquote>
												<p class="text-primary">毒報/尿報</p>
											</blockquote>
											<div class="row">
												<div class="col-md-7">
													<div class="panel panel-default">
														<div class="panel-body">
															<fieldset>
																<legend class="lead">
																	毒報
																	
																</legend>
															</fieldset>
															<div class="row">
																<div class="col-md-12">
																	<label class="pull-right">
																		<input type="checkbox" id="fd_merge" name="fd_merge" <?php echo (isset($fd->fd_merge) && !empty($fd->fd_merge))?'checked':''; ?>/> 單沒入
																	</label>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12 ">
																	<table class="table table-bordered  table-condensed" id="drugListTable">
																		<thead>
																			<tr >
																				<th class="text-center">物件</th>
																				<th class="text-center">數量</th>
																				<th class="text-center">成份</th>
																				<th class="text-center">重量</th>
																				<th class="text-center"><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#drugModal" onclick="resetDrugInd()">新增</button></th>
																			</tr>
																		</thead>
																		<tbody>
																			<?php
																			if(isset($drug))
																			{
																				if(count($drug) > 0)
																				{
																					foreach ($drug as $e) {
																						echo '<tr data-id="'. $e->e_id.'">
																								<td>'. $e->e_name.'</td>
																								<td>'. $e->e_count . $e->e_unit.'</td>
																								<td>'. $e->e_type.'</td>
																								<td>'. $e->e_1_N_W.'</td>
																								<td>
																								<button type="button" class=" btn-link" data-toggle="modal" data-target="#drugModal" onclick="loadDrugInd(\''. $e->e_id.'\')">編輯</button>
																								</td>
																							</tr>';
																					}
																				}
																				else
																				{
																					echo '<tr><td colspan="5">無相關毒證物資料</td></tr>';
																				}
																				
																			}
																			else
																			{
																				echo '<tr><td colspan="5">無相關毒證物資料</td></tr>';
																			}
																				
																			?>
																			<!-- <tr>
																				<td>即溶包</td>
																				<td>1包</td>
																				<td>
																					2級大麻,
																					1級海洛因 ,
																					2級安非他命
																				</td>
																				<td>
																				<button type="button" class=" btn-link" data-toggle="modal" data-target="#drugModal">編輯</button>
																				</td>
																			</tr>
																			<tr>
																				<td>錠劑</td>
																				<td>5顆</td>
																				<td>
																				3級硝甲西泮,
																				4級硝西泮
																				</td>
																				<td>
																				<button type="button" class=" btn-link">編輯</button>
																				</td>
																			</tr> -->
																		</tbody>
																	</table>
																	<div class="modal fade " id="drugModal" tabindex="-1" role="dialog" aria-labelledby="drugModalLabel">
																		<div class="modal-dialog " role="document">
																			<div class="modal-content">
																				<div class="modal-header bg-primary">
																					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        																			<h4 class="modal-title" id="drugModalLabel">物件</h4>
																				</div>
																				<div class="modal-body">
																					<div class="row">
																						<div class="col-md-5">
																							<form id="drugIndForm">
																								<div class="row">
																									<div class="form-group form-group-sm col-md-12">
																										<label class=" control-label">物品</label>
																										<?php 
																										echo form_dropdown('e_name_sel',$opt1,'','class="form-control" id=sel onchange="changeEnameOther($(this))" ') ?>
																										<input type="text" name="e_name_other" class="form-control hidden" id="e_name_other" value="">
																										<input type="hidden" name="e_name">
																									</div>
																									<div class="form-group form-group-sm col-md-8">
																										<label class=" control-label">數量</label>
																										<?php echo form_input('e_count','', 'class="form-control" id="drugpack_count"')?>
																									</div>
																									<div class="form-group form-group-sm col-md-4">
																										<label class=" control-label">單位</label>
																										<?php echo form_dropdown('e_type',$opt2,'','class="form-control" id="sel3" ') ?>
																									</div>
																									<div class="form-group form-group-sm col-md-12">
																										<label class=" control-label">重量</label>
																										<?php echo form_input('e_weight','', 'class="form-control" id="drugpack_weight"')?>
																									</div>
																								</div>
																								
																							</form>
																						</div>
																						<div class="col-md-7">
																							<div class="row">
																								<div class="form-group form-group-sm col-md-12">
																									<label class="col-md-6 control-label">成份</label>
																									<div class="col-md-6 text-right">
																										<button type="button" class="btn btn-default btn-xs" onclick="addDrugTr()">增加</button>
																									</div>
																									
																								</div>
																								<!-- <div class="form-group form-group-sm col-md-4">
																									<select class="form-control" id="druglevel">
																										<option value="">請選擇</option>
																										<option value="1">1級</option>
																										<option value="2">2級</option>
																										<option value="3">3級</option>
																										<option value="4">4級</option>
																									</select>
																								</div>
																								<div class="form-group form-group-sm col-md-8">
																									<select class="form-control" id="drugname"></select>
																								</div> -->
																							</div>
																							<!-- <hr> -->
																							<table class="table table-bordered table-striped table-condensed" id="drugIndTable">
																								<thead>
																									<tr >
																										<th class="text-center">級數</th>
																										<th class="text-center">成份</th>
																										<th class="text-center">數量</th>
																									</tr>
																								</thead>
																								<tbody>
																								</tbody>
																							</table>

																							<p><small class="text-danger">雙擊資料列即可編輯</small></p>
																						</div>
																					</div>
																					<button type="button" class="btn btn-warning btn-sm btn-block" id="saveDrugInd">儲存</button>																					
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<input type="hidden" name="e_id" value="<?php //echo $drug->e_id;?>">
																<div class="form-group col-md-4">
																	<label>物件 <small class="text-danger">點擊上表任一資料列帶入</small></label>
																	<p id="drugObj"></p>
																</div>
																<div class="form-group col-md-4">
																	<label>樣態1</label>
																	<select class="form-control" id="sel1" name="sel1"> 
																		<option value="沾">沾</option>
																		<option value="摻">摻</option>
																		<option value="">沒有</option>
																	</select>	
																</div>
																
															</div>
															<div class="row">
																<div class="form-group col-md-8">
																	<label>級數、成分</label>
																	<textarea  id="drug" name="drug" class="form-control" placeholder="3級硝甲西泮;4級硝西泮" rows="3"><?php 
																	$patterns[0] = "/沾/";
																	$patterns[1] = "/第/";
																	$patterns[2] = "/毒品/";
																	$patterns[3] = "/「/";
																	$patterns[4] = "/」/";
																	$patterns[5] = "/及/";
																	$patterns[6] = "/摻/";
	
																	$replacements[6] = "";
																	$replacements[5] = "";
																	$replacements[4] = "";
																	$replacements[3] = "";
																	$replacements[2] = "";
																	$replacements[1] = ";";
																	$replacements[0] = "";
																	//echo preg_replace($patterns, $replacements, $drug[0]->e_type); ?></textarea>
																	<span class="text-danger"><small>如：第三級毒品「3,4-亞甲基雙氧苯基乙基胺丁酮」，請輸入 3級3,4-亞甲基雙氧苯基乙基胺丁酮</small> </span> <br>
																	<span class="text-danger"><small>如：第三級毒品「硝甲西泮」及第四級毒品「硝西泮」，請輸入 3級硝甲西泮;4級硝西泮</small> </span> 
																	
																</div>
																<div class="form-group col-md-4">
																	<button class="btn btn-link btn-block" type="button" onclick="addDrugtype($(this))">加入毒物描述</button>
																</div>
															</div>
															<!-- <div class="row">
																<div class="form-group col-md-4">
																	<label>物品</label>
																	<?php 
																	//echo form_dropdown('e_name_sel',$opt1,((strpos($drug->e_name, '其他') !== false)?'其他':$drug->e_name),'class="form-control" id=sel onchange="changeEnameOther($(this))" ') ?>
																	<input type="text" name="e_name_other" class="form-control <?php //echo ((strpos($drug->e_name, '其他') === false)?'hidden':''); ?>" id="e_name_other" value="<?php //echo ((strpos($drug->e_name, '其他') !== false)?explode('-',$drug->e_name)[0]:''); ?>">
																	<input type="hidden" name="e_name" value="">
																</div>
																<div class="form-group col-md-4">
																	<label>數量</label>
																	<?php //echo form_input('e_count',$drug->e_count, 'class="form-control"')?>
																</div>
																<div class="form-group col-md-4">
																	<label>單位</label>
																	<div class="input-group">
																		<?php //echo form_dropdown('e_type',$opt2,$drug->e_unit,'class="form-control" id="sel3"') ?>
																		<span class="input-group-btn">
																			<button class="btn btn-link" type="button" onclick="addDrugtype($(this))">加入毒物描述</button>
																		</span>
																	</div>
																	
																</div>
															</div> -->
															<hr>
															<div class="row">
																<div class="form-group col-md-8">
																
																	<label>樣態2</label>
																	<select id="sel2" name="sel2" class="form-control"> 
																		<option value="(採集自車內)">採集自車內</option>
																		<option value="(採集自隨身包包內)">採集自隨身包包內</option>
																		<option value="(採集自車內殘渣裝袋)">採集自車內殘渣裝袋</option>
																	</select>
																	
																</div>		
																<div class="form-group col-md-4">
																	<button class="btn btn-link  btn-block" type="button" onclick="addDrugloc($('#sel2'))">加入採集描述</button>
																</div>
															</div>
															<hr>
															<div class="row">
																<div class="col-md-12">
																	<div class="form-grou">
																		<label>描述文字</label>
																		<!-- <button id='sub' type="button" class="btn btn-link btn-sm pull-right">加入描述</button> -->
																		<textarea name="fd_drug_message" rows="3" id="messageSpan" class="form-control"><?php if(isset($sp->fd_drug_msg))echo $sp->fd_drug_msg;
																				else echo '';
																			?></textarea>
																			
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-5">
													<div class="panel panel-default">
														<div class="panel-body">
															<fieldset>
																<legend class="lead">尿檢</legend>
															</fieldset>
															<div class="row">
																<div class="form-group col-md-6">
																	<label>尿液編號</label>
																	<?php echo form_input('s_utest_num',$susp->s_utest_num, 'class="form-control"')?>
																</div>
																<div class="form-group form-group-sm col-md-12">
																	<label class="col-md-6 control-label">成份</label>
																	<div class="col-md-6 text-right">
																		<button type="button" class="btn btn-default btn-xs" onclick="addUrineTr()">增加</button>
																	</div>
																</div>
																<div class="col-md-12">
																	<table class="table table-bordered table-striped table-condensed" id="UrineTestTable">
																		<thead>
																			<tr >
																				<th class="text-center">級數</th>
																				<th class="text-center">成份</th>
																				<th></th>
																			</tr>
																		</thead>
																		<tbody>
																			<?php
																			if(isset($urine) && count($urine) > 0)
																			{
																				$ui = 0;
																				foreach ($urine as $u) {
																					echo '<tr data-id="'. $u->sc_num.'" id="urinetr'.$ui.'" ondblclick="editUrineStateEvent($(this), '.$ui.')">
																							<td style="width:8rem;">'. $u->sc_level.'級</td>
																							<td>'. $u->sc_ingredient .'</td>
																							<td style="width:8rem;">
																							
																							</td>
																						</tr>';
																					$ui++;
																				}
																			}
																			else
																			{
																				echo '<tr class=" empty-tr"><td colspan="2" class="text-center">點擊右上角新增成分</td></tr>';
																			}
																				
																			?>
																		</tbody>
																	</table>

																	<p><small class="text-danger">雙擊資料列即可編輯</small></p>
																</div>
																<div class="form-group col-md-6 hidden">
																	<label>級數、成分</label>
																	<?php echo form_input('sc_ingredient',$sc_ingredient, 'class="form-control"')?>
																</div>
																<div class="form-group col-md-2 hidden">
																	<label>是否要罰</label>
																	<div>
																		<label class="radio-inline">
																			<input type="radio" name="fd_wantdis" id="optionsRadios1" value="是" checked>是
																		</label>
																		<label class="radio-inline">
																			<input type="radio" name="fd_wantdis" id="optionsRadios2" value="否">否
																		</label>
																	</div>
																</div>
																<!-- <td><input type="text" id="drug" name="drug" class="form-control" value="<?php //echo $drug_ingredient?>"/></td> -->
															</div>
														</div>
													</div>
												</div>
											</div>
											<hr>
											<blockquote>
												<p class="text-primary">開罰</p>
											</blockquote>
											<div class="row">
                                                <div class="form-group col-md-4">
                                                        <label>刑事&不罰情形</label>
														<div class="input-group">
															<select id="seltype" class="form-control" > </select>
															<span class="input-group-btn">
																<button class="btn btn-link" type="button" onclick="addSeltype($(this))">加入描述</button>
															</span>
														</div>
                                                        
                                                            <!-- <option selected>請選擇</option>
                                                            <option value="0">type1</option>
                                                            <option value="1">type2</option>
                                                            <option value="2">type3</option> -->
                                                       
														<span class="seltype_txt text-primary"><small></small> </span>  
                                                </div>
                                                <div class="form-group col-md-2">
														<label>第幾次裁罰 </label> 
                                                        <select id="seldate" class="form-control" name="seltimes">
                                                            <option value="-1">請選擇</option>
                                                            <option value="1">第一次</option>
                                                            <option value="2">第二次</option>
                                                            <option value="3">第三次</option>
                                                            <option value="4">第四次</option>
                                                            <option value="5">第五次</option>
                                                            <option value="6">第六次</option>
                                                            <option value="7">第七次</option>
                                                            <!-- <option value="2">第七次</option> -->
                                                        </select>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>警政署系統編號</label>
                                                    <input name="fd_psystem[]" type="text" class="form-control">
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label>罰鍰</label>
                                                    <?php
                                                        if(isset($fine)) {
                                                            echo form_input('f_damount',$fine->f_damount, 'class="form-control" id="pmoney" readonly required="required"');
                                                        }
                                                        else {
                                                            echo form_input('f_damount','', 'class="form-control" id="pmoney" readonly required="required"');
                                                        }
                                                    ?>
                                                </div>
                                                <div class="form-group col-md-2">
                                                        <label>繳款日</label>
                                                        <input id="f_date" name="f_date" type="text" class="rcdate form-control" value="<?php echo ((strlen($dp->dp_expirdate) > 7 && $dp->dp_expirdate != '0000-00-00')?str_pad(((int)substr($dp->dp_expirdate, 0, 4) - 1911),3,"0",STR_PAD_LEFT).substr($dp->dp_expirdate, 5, 2).substr($dp->dp_expirdate, 8, 2):'');?>">

                                                        <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>      
                                                </div>
												<div class="col-md-4 hidden">
                                                        <div style="display: flex;">
                                                            <select id="seltype2" class="form-control"></select>
                                                            <button type="button" class="sub2 btn btn-default">產生列句</button>

                                                        </div>
                                                    <!-- <textarea rows="5" name="fd_dis_msg_hidden" id="messageSpan2" class="form-control"><?php //if(isset($sp))echo $sp->fd_dis_msg;
                                                        //else echo '';
                                                    ?></textarea> -->
                                                </div>
                                            </div>
                                            <div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label>刑事&不罰情形描述文字</label>
														<textarea id="fd_dis_msg"  name="fd_dis_msg" class="form-control" rows="5"><?php echo ((isset($sp))?$sp->fd_dis_msg:'');?></textarea>
													</div>
												</div>
                                               
                                            </div>
											<hr>
											<blockquote>
												<p class="text-primary">講習</p>
											</blockquote>
											<div class="row">
												<div class="form-group col-md-12">
													<p class="text-primary"><label>專案預設講習</label><span>
													<?php 
													$default = '';
													if(isset($dp->dp_course) && !empty($dp->dp_course) )
													{
															$default = explode('-', str_replace('_', '-', preg_replace('/\r\n|\n/',"",$dp->dp_course)));
															echo $default[0] . '(' .$default[1].') ' .$default[2].'-'.$default[3] . ' ' .$default[4];
													}
													else
													{
														echo '';
													}; ?>
													</span></p>
													<p class="text-muted"><label>個案目前講習</label><span>
													<?php
													if(!empty($sp->fd_lec_date) && isset($sp->fd_lec_date) )
													{
														if(is_array($default))
														{
															if($default[0] . '(' .$default[1].')' == $sp->fd_lec_date)
															{
																echo '同上';
															}
															else
															{
																echo $sp->fd_lec_date . $sp->fd_lec_time . $sp->fd_lec_place. '(' . $sp->fd_lec_address . ')';
															}
														}
														
													}
													else
													{
														echo '同上';
													}; ?>
													</span></p>
												</div>
											</div>
											<div class="row">
                                                <div class="form-group col-md-4">
                                                    <label>講習時段(預設講習時段)</label> 
                                                    <?php 
													 
													$courseAry = '';
													if(isset($dp->dp_course) && !empty($dp->dp_course) )
													{
														// if(explode('_', preg_replace('/\r\n|\n/',"",$dp->dp_course))[0] == explode('(', $sp->fd_lec_date)[0])
														// {
															$courseAry = explode('-', str_replace('_', '-', preg_replace('/\r\n|\n/',"",$dp->dp_course)));
														// }
														// else
														// {
														// 	$courseAry = '';
														// }
													};
													
													foreach ($course_opt as $key => $value) {														
														if(($courseAry != ''))
														{
															if(trim($courseAry[0]) == explode(' (', preg_replace('/\r\n|\n/',"",$course_opt[$key]))[0])
															{
																// unset($course_opt[$key]);
															}
														}
														else
														{
															if(isset($sp->fd_lec_date))
															{
																if(trim(explode('(', $sp->fd_lec_date)[0]) == explode(' (', preg_replace('/\r\n|\n/',"",$course_opt[$key]))[0])
																{
																	// unset($course_opt[$key]);
																}
															}
															
														}
														$value = preg_replace('/\r\n|\n/',"",$value);
													}
													
													echo form_dropdown('fd_lec',$course_opt,((isset($dp->dp_course) && !empty($dp->dp_course))?str_replace('_', '-', $dp->dp_course):''),'class="form-control" id="fd_lec" onchange="changeCourseEvent($(this))"') ?>
													
                                                </div>
                                            </div>
											
											<?php  
											if(is_array($courseAry) && !empty($sp->fd_lec_date) && isset($sp->fd_lec_date))
											{
												if($courseAry[0] . '(' .$courseAry[1].')' != $sp->fd_lec_date)
												{
													echo '<span class="text-danger">注意：所選的時段與預設時段不一樣，如需更改為預設請選擇『無』，否請忽略該訊息。</span>';
												}
											}
											elseif (!is_array($courseAry) && !empty($sp->fd_lec_date)  && isset($sp->fd_lec_date)) {
												echo '<span class="text-danger">注意：所選的時段與預設時段不一樣，如需更改為預設請選擇『無』，否請忽略該訊息。</span>';
											}
											
											?>
                                            <div class="row <?php echo (($courseAry == '' && empty($sp->fd_lec_time))?'hidden':''); ?>" id="fd_lec_wrap">
                                                <div class="form-group col-md-3">
                                                    <label>講習時數</label>
                                                    <?php
                                                        if(is_array($courseAry) && empty($sp->fd_lec_time) ) {
                                                            echo form_input('fd_lec_time',$courseAry[2].'-'.$courseAry[3], 'class="form-control"');
                                                        }
														elseif (is_array($courseAry) && !empty($sp->fd_lec_time)) {
															if($courseAry[0] . '(' .$courseAry[1].')' != $sp->fd_lec_date)
															{
																echo form_input('fd_lec_time',$sp->fd_lec_time, 'class="form-control"');
															}
															else
															{
																echo form_input('fd_lec_time',$courseAry[2].'-'.$courseAry[3], 'class="form-control"');
															}
															
														}
														elseif (!is_array($courseAry) && !empty($sp->fd_lec_time)) {
															echo form_input('fd_lec_time',$sp->fd_lec_time, 'class="form-control"');
														}
                                                        else 
														{
                                                                 echo form_input('fd_lec_time','', 'class="form-control"');
                                                                
                                                        }
                                                    ?>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label>講習時間</label>
                                                    <?php
                                                        // $options1 = array(
                                                        //     '2020-04-10'=> '04/10',
                                                        //     '2020-05-15'=> '05/15',
                                                        //     '2020-06-15'=> '06/15',
                                                        //     '2020-11-27'=> '11/27',
                                                        //     '2020-12-02'=> '12/2',
                                                        //     '2020-12-04'=> '12/4',
                                                        //     '2020-12-11'=> '12/11',
                                                        //     '2020-12-18'=> '12/18',
                                                        // );
                                                        // if(isset($sp)) {
                                                        //     echo form_dropdown('fd_lec_date',$cdate,$sp->fd_lec_date, 'class="form-control"');
                                                        // }
                                                        //      else {
                                                        //          echo form_dropdown('fd_lec_date',$cdate,'', 'class="form-control"');
                                                        // }
														if(is_array($courseAry) && empty($sp->fd_lec_time) ) {
                                                            echo form_input('fd_lec_date',$courseAry[0] . '(' .$courseAry[1].')', 'class="form-control"');
                                                        }
														elseif (is_array($courseAry) && !empty($sp->fd_lec_time)) {
															if($courseAry[0] . '(' .$courseAry[1].')' != $sp->fd_lec_date)
															{
																echo form_input('fd_lec_date',$sp->fd_lec_date, 'class="form-control"');
															}
															else
															{
																echo form_input('fd_lec_date',$courseAry[0] . '(' .$courseAry[1].')', 'class="form-control"');
															}
															
														}
														elseif (!is_array($courseAry) && !empty($sp->fd_lec_time)) {
															echo form_input('fd_lec_date',$sp->fd_lec_date, 'class="form-control"');
														}
                                                        else 
														{
															echo form_input('fd_lec_date','', 'class="form-control"');
                                                                
                                                        }
                                                    ?>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>講習地點</label>
                                                    <?php
														if(is_array($courseAry) && empty($sp->fd_lec_time) ) {
															echo form_input('fd_lec_address',$courseAry[4], 'class="form-control"');
														}
														elseif (is_array($courseAry) && !empty($sp->fd_lec_time)) {
															if($courseAry[0] . '(' .$courseAry[1].')' != $sp->fd_lec_date)
															{
																echo form_input('fd_lec_address',$sp->fd_lec_place . '(' . $sp->fd_lec_address . ')', 'class="form-control"');
															}
															else
															{
																echo form_input('fd_lec_address',$courseAry[4], 'class="form-control"');
															}
															
														}
														elseif (!is_array($courseAry) && !empty($sp->fd_lec_time)) {
															echo form_input('fd_lec_address',$sp->fd_lec_place . '(' . $sp->fd_lec_address . ')', 'class="form-control"');
														}
														else 
														{
															echo form_input('fd_lec_address','', 'class="form-control"');
																
														}

                                                    ?>
                                                </div>
                                            </div> 
											<span id="info-txt" class="text-danger hidden">該個案已有重複之講習，請選擇其他。</span>
											<hr>
											<blockquote>
												<p class="text-primary">文件上傳</p>
											</blockquote>
											<div class="row">
                                                <div class="form-group col-md-4">
                                                    <label>司法文書(判決書/不起訴處分書/緩起訴/他縣市裁罰書)</label>
                                                    <?php if(!isset($susp->sp_doc)){ echo form_upload('sp_doc','','class="form-control" accept=".pdf"');}
                                                            else{
                                                                echo '已上傳';
                                                            }
                                                        ?>
                                                    <!-- <input type="file" class="form-control" name="sp_doc" id="sp_doc"> -->
                                                </div>
                                                
                                            </div>
										</div>
									</div>
                                    <!-- <ul class="nav nav-tabs">
                                        <li class="active"><a href="#gongwen" data-toggle="tab">公文</a></li>
                                        <li><a href="#zaijian" data-toggle="tab">基本資料/在監</a></li>
                                        <li><a href="#fadai" data-toggle="tab">法代人(未年滿20歲開啟)</a></li>
                                        <li><a href="#report" data-toggle="tab">毒報/尿報</a></li>
                                        <li><a href="#fin" data-toggle="tab">開罰</a></li>
                                        <li><a href="#fawen" data-toggle="tab">發文</a></li>
                                        <li><a href="#jiang" data-toggle="tab">講習</a></li>
                                        <li><a href="#upload" data-toggle="tab">文件上傳</a></li>
                                    </ul> -->

                                    <!-- Tab panes -->
                                    <!-- <div  class="tab-content" style="padding: 10px;">
                                        <div class="tab-pane fade in active" id="gongwen">
                                            
                                        </div>
                                        <div class="tab-pane fade" id="zaijian">
                                            
                                        
                                        </div>
                                        <div class="tab-pane fade" id="fadai">
                                            
                                        </div>
                                        <div class="tab-pane fade" id="report">
                                            
                                        
                                        </div>
                                        <div class="tab-pane fade" id="fin">
                                            
                                        
                                        </div>
                                        <div class="tab-pane fade" id="fawen">
                                                                           
                                        </div>
                                        <div class="tab-pane fade" id="jiang">
                                                                                   
                                        </div>
                                        <div class="tab-pane fade in " id="upload">
                                            
                                        </div>
                                    </div> -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <div class="col-lg-4">
							<label>文件預覽：</label>
                            <iframe style="width:100%;height:800px" frameborder="0" name="pdfviewdrug" id="pdfviewdrug"></iframe>

                            <iframe style="width:100%;height:800px" frameborder="0" name="pdfviewsusp" id="pdfviewsusp"></iframe>

                            <iframe style="width:100%;height:800px" frameborder="0" name="pdfviewcase" id="pdfviewcase"></iframe>

                            <iframe style="width:100%;height:800px" frameborder="0" name="pdfviewjud" id="pdfviewjud"></iframe>

                            <iframe style="width:100%;height:800px" frameborder="0" name="pdfviewresult" id="pdfviewresult"></iframe>
                        </div>
                        <!-- <iframe width="100%" height="800"  id="pdfviewod"  src=<?php //echo base_url('/result/S36C-120093013580-1-18.pdf')?>></iframe> -->
                        

                            <!-- <a class="media" id="pdfviewod" href=<?php //echo base_url('/result/S36C-120093013580-1-18.pdf')?>></a> -->
                            <!-- <a class="media" id="pdfviewdrug" href=<?php //echo base_url('/drugdoc/'.$drug->e_doc)?>></a>     -->
                            <!-- <a class="media" id="pdfviewsusp" href=<?php //echo base_url('/utest/'.$susp->s_utest_doc)?>></a>     -->
                            <!-- <a class="media" id="pdfviewcase" href=<?php //echo base_url('/uploads/'.$cases->doc_file)?>></a>     -->
                            <!-- <a class="media" id="pdfviewjud" href=<?php //echo base_url('/jiansuo/'.$susp->s_prison_doc)?>></a>     -->
                            <!-- <a class="media" id="pdfviewresult" href=<?php //echo base_url('/jiansuo/'.$susp->s_prison_doc)?>></a>     -->
                        <!-- /.col-lg-12 -->
                        </form>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <script>
            // $( "#sendofficedate, #s_fdate, #s_birth, #s_fadai_birth, #f_date" ).datepicker({
            //     dateFormat: "yy-mm-dd",
            //     changeMonth: true,
            //     changeYear: true,
            //     yearRange: "c-30:*"
            // });
            // $( "#sendofficedate" ).datepicker( $.datepicker.regional[ "zh-TW" ] );
            // window.onbeforeunload = function(e) {
            //     var dialogText = '確定關閉視窗嗎？';
            //     e.returnValue = dialogText;
            //     return dialogText;
            // };
            $(function(){
                // savetimer = setInterval('autoSave()', 30000);
				autoSave();
                $("input, select, textarea").change(function(){
                    setTimeout(autoSave, 1500);
                    // autoSave();
                });
                $(".rcdate").change(function(){
                    let strlen = $(this).val().length;
                    if(strlen < 7 || strlen < "7")
                    {
                        if(strlen != 0)
                            $(this).val("0"+$(this).val());
                    }
                    else
                    {
                        return false;
                    }
                        
                });
                $(".rcdate").keypress(function(){
                    if($(this).val().length >= 7)
                        return false;
                });  
                $("#s_fadai_name").change(function(){
                    if($(this).val())
                        $("#fd_target_1").val($(this).val());
                    else
                        $("#fd_target_1").val('<?php echo $susp->s_name; ?>');
                }); 

				$("input[name='ad']").bind('change', function(){
					if($(this).val().indexOf('1') > -1)
					{
						$s_dpcounty = ($("select[name='s_dpcounty']").val())?$("select[name='s_dpcounty']").val():'';
						$s_dpdistrict = ($("select[name='s_dpdistrict']").val())?$("select[name='s_dpdistrict']").val():'';
						$("#address").val($s_dpcounty+$s_dpdistrict+ $("#ori_rpaddress").val());
						listzipcode($("#address"), $("#zipcodefin"))
					}
					else
					{
						$s_rpcounty = ($("select[name='s_rpcounty']").val())?$("select[name='s_rpcounty']").val():'';
						$s_rpdistrict = ($("select[name='s_rpdistrict']").val())?$("select[name='s_rpdistrict']").val():'';
						$("#address").val($s_rpcounty+$s_rpdistrict+$("#s_rpaddress").val());
						listzipcode($("#address"), $("#zipcodefin"))
					}
				});
				$("#e_name_other").bind('change', function(){
					$("input[name='e_name']").val($(this).val() + '-' + '其他');
				});
				$("#drugListTable tbody tr").bind('click', function(){
					$("#drugListTable tbody tr").removeClass('selected');
					$(this).toggleClass('selected');

					$("#drugObj").text($("#drugListTable tbody tr.selected td:eq(0)").text() + $("#drugListTable tbody tr.selected td:eq(1)").text());
					$("#drug").val($("#drugListTable tbody tr.selected td:eq(2)").text().replace(/\r\n|\n|\r|\t|\s/g,"").replace(/,|、/gi, ";"));
					
				});

				$("#seldate").val("<?php echo (isset($fd->fd_times) && $fd->fd_times != 0)?$fd->fd_times:-1; ?>").change();

				listzipcode($("#address"), '#zipcodefin');
				checkDupCourse();
            });
           
               
            $("#next").click(function (){
                $("#nextvalue").val('<?php echo $next; ?>');
                //alert($("#nextvalue").val());
                $("#updateSanc").submit();
            });
            $("#prev").click(function (){
                $("#prevvalue").val('<?php echo $prev; ?>');
                //alert($("#prevvalue").val());
                $("#updateSanc").submit();
            });
            $("#drugdoc").click(function (){
                $('#pdfviewdrug').show();  
                var $iFrame=$("#pdfviewdrug");
                $iFrame.prop("src","<?php echo base_url('/drugdoc/'.(isset($drug[0]->e_doc)?$drug[0]->e_doc:'EN10900006毒品檢驗報告.pdf'));?>")
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewresult').hide();     
                $('#pdfviewjud').hide();   
                $('#pdfviewod').hide();   
            });
            $("#suspdoc").click(function (){
                $('#pdfviewsusp').show(); 
                var $iFrame=$("#pdfviewsusp");
                $iFrame.prop("src","<?php echo base_url('/utest/'.(($susp->s_utest_doc)?$susp->s_utest_doc:'CN1099018尿液檢驗報告.pdf'));?>")
                
                $('#test').hide();  
                $('#pdfviewdrug').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewresult').hide();     
                $('#pdfviewjud').hide();   
                $('#pdfviewod').hide();  
            });
            $("#odoc").click(function (){
                $('#pdfviewod').show();  
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewdrug').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewresult').hide();     
                $('#pdfviewjud').hide();     
            });
            $("#casedoc").click(function (){
                $('#pdfviewcase').show(); 
                var $iFrame=$("#pdfviewcase");
                $iFrame.prop("src","<?php echo base_url('/uploads/'.(($cases->doc_file)?$cases->doc_file:'CN1090010person_other.pdf'));?>")
                
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewdrug').hide();  
                $('#pdfviewod').hide();  
                $('#pdfviewresult').hide();     
                $('#pdfviewjud').hide();     
            });
            $("#result").click(function (){
                $('#pdfviewresult').show();  
                var $iFrame=$("#pdfviewresult");
                $iFrame.prop("src","<?php echo base_url('/jiansuo/'.(($susp->s_prison_doc)?$susp->s_prison_doc:'A11122211110900053.pdf'));?>")
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewdrug').hide();     
                $('#pdfviewod').hide();     
                $('#pdfviewjud').hide();     
            });
            $("#judoc").click(function (){
                $('#pdfviewjud').show();  
                var $iFrame=$("#pdfviewjud");
                $iFrame.prop("src","<?php echo base_url('/jiansuo/'.(($susp->sp_doc)?$susp->sp_doc:'A11122211110900053.pdf'));?>")
                
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewdrug').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewod').hide();     
                $('#pdfviewresult').hide();     
            });

            /*$("#next").click(function (){
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);     
                //alert($('#s_cnum1').val());
                window.location.href = "//localhost/main2/disciplinary_c/editSanc/"+$('#s_cnum1').val();
                //$("#sp_checkbox").submit();
            });*/
            $('#sub').click(function (){
                
                var arr =[];
                var level =[];
                var drugn =[];
                var all =[];
                var fin = '';
                var drug =$('#drug').val().split(';');
                for(let i=0;i<drug.length-1;i++){
                    level[i]=drug[i].substr(0,1);
                    drugn[i]=drug[i].substr(2);
                    all[i]="第"+level[i]+"級毒品「"+drugn[i]+"」";
                    fin = all[i];
                    if(drug.length > 2) fin= fin + "及" + fin;
                    else fin = all[i];
                    // console.log(drug.length);
                }
                $sel=$('#sel').val();
                $sel1=$('#sel1').val();
                $sel2=$('#sel2').val();
                $sel3=$('#sel3').val();
                $("input[name='e_count']").each(function(){
                    arr.push($(this).val());
                })
                $("#messageSpan").val($sel1+fin+$sel+arr[0]+$sel3+$sel2);
                //console.log(arr,$sel,$sel2);
                
            });
            $('.sub2').click(function (){
                $seltype2=$('#seltype2').val();
                $("#messageSpan2").val($seltype2);
                //console.log($seltype2);
                
            });
			seltype_A = { 
                "受處分人另涉及意圖販賣而持有及加重持有第三級毒品部分，業經臺灣臺北地方檢察署不起訴處分，併此敘明。":"(A) 受處分人另涉及意圖販賣而持有及加重持有第三級毒品部分，業經臺灣臺北地方檢察署不起訴處分，併此敘明。",
				"原本局109年06月18日北市警刑毒緝字第1093010905號處分書作廢(列管編號：1091381)，若已完納罰鍰或完成講習，無須重復履行義務。":"(A) 原本局109年06月18日北市警刑毒緝字第1093010905號處分書作廢(列管編號：1091381)，若已完納罰鍰或完成講習，無須重復履行義務。"
			};
			seltype_B = { 
				"受處分人另涉及加重持有第三級毒品部分，業經臺灣臺北地方法院刑事簡易判決處有期徒刑2月，併予敘明。":"(B) 受處分人另涉及加重持有第三級毒品部分，業經臺灣臺北地方法院刑事簡易判決處有期徒刑2月，併予敘明。",
				"受處分人另涉及販賣、持有第二級毒品部分，業經臺灣桃園方法院刑事判決，分別處有期徒刑4年7月及4月，併予敘明。":"(B) 受處分人另涉及販賣、持有第二級毒品部分，業經臺灣桃園方法院刑事判決，分別處有期徒刑4年7月及4月，併予敘明。",
				"受處分人另涉及持有第二級毒品部分，業經臺灣臺北地方檢察署緩起訴處分確定，並向國庫支付新臺幣2萬元，併予敘明。":"(B) 受處分人另涉及持有第二級毒品部分，業經臺灣臺北地方檢察署緩起訴處分確定，並向國庫支付新臺幣2萬元，併予敘明。",
				"因本案係受處分人於違反行政法義務遭發覺前，由親屬主動向員警報案而發現，故免處罰鍰。":"(B) 因本案係受處分人於違反行政法義務遭發覺前，由親屬主動向員警報案而發現，故免處罰鍰。",
				"因本案係受處分人於違反行政法義務遭發覺前，主動向員警報案坦承犯行，故免處罰鍰。":"(B) 因本案係受處分人於違反行政法義務遭發覺前，主動向員警報案坦承犯行，故免處罰鍰。"
			};
			seltype_C = { 
				"受處分人另涉及施用第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署檢察官不起訴之處分，併予敘明。":"(C) 受處分人另涉及施用第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署檢察官不起訴之處分，併予敘明。",
				"受處分人另涉及施用、持有第二級毒品部分，業經臺灣臺北地方檢察署處分緩起訴2年，並至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併予敘明。":"(C) 受處分人另涉及施用、持有第二級毒品部分，業經臺灣臺北地方檢察署處分緩起訴2年，並至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併予敘明。",
				"受處分人另涉及施用、持有第二級毒品部分，業經臺灣士林地方檢察署處分緩起訴1年6月，並至指定醫療院所於1年之治療期程內完成戒癮治療，併予敘明。":"(C) 受處分人另涉及施用、持有第二級毒品部分，業經臺灣士林地方檢察署處分緩起訴1年6月，並至指定醫療院所於1年之治療期程內完成戒癮治療，併予敘明。",
			};
			seltype_D = { 
				"惟受處分人於警詢筆錄否認施用毒品，查受處分人之尿液檢體未檢出有毒品反應，且無其他積極證據證明受處分人近期施用毒品，故不處罰鍰、講習，查獲之毒品證物依規定沒入。":"(D) 惟受處分人於警詢筆錄否認施用毒品，查受處分人之尿液檢體未檢出有毒品反應，且無其他積極證據證明受處分人近期施用毒品，故不處罰鍰、講習，查獲之毒品證物依規定沒入。",
				"本案業經新北市政府警察局於110年4月29日新北警刑字第1104491592號處分書，裁處罰鍰3萬元、毒品危害講習6小時，本局僅針對毒品為沒入處分。":"(D) 本案業經新北市政府警察局於110年4月29日新北警刑字第1104491592號處分書，裁處罰鍰3萬元、毒品危害講習6小時，本局僅針對毒品為沒入處分。",
				"本案查獲時毒品危害防制條例第11條之1第2項尚未生效(98年11月20日前)，爰不處罰鍰、講習，僅針對毒品沒入處分，併與敘明。":"(D) 本案查獲時毒品危害防制條例第11條之1第2項尚未生效(98年11月20日前)，爰不處罰鍰、講習，僅針對毒品沒入處分，併與敘明。",
				"本案已逾3年裁罰時效，故不處罰鍰、講習，查獲之毒品證物依規定沒入。":"(D) 本案已逾3年裁罰時效，故不處罰鍰、講習，查獲之毒品證物依規定沒入。",
				"經查受處分人已於0年0月0日註記死亡，故本案僅就毒品為沒入處分。":"(D) 經查受處分人已於0年0月0日註記死亡，故本案僅就毒品為沒入處分。",
				"惟本案起獲毒品於查獲後始列為第三級毒品管制，行為時尚屬不罰，僅針對毒品沒入處分，併予敘明。":"(D) 惟本案起獲毒品於查獲後始列為第三級毒品管制，行為時尚屬不罰，僅針對毒品沒入處分，併予敘明。",
				"本案業經桃園市政府警察局刑事警察大隊109年7月24日桃警刑大偵字第1090015331號函認本案已逾裁罰時效，故不處罰鍰、講習。":"(D) 本案業經桃園市政府警察局刑事警察大隊109年7月24日桃警刑大偵字第1090015331號函認本案已逾裁罰時效，故不處罰鍰、講習。",
				"本案業經新北市政府警察局109年7月24日新北警刑字第1094555725號函認受處分人行為時尚屬不罰，不裁處罰鍰及講習，本局僅針對毒品沒入處分，併予敘明。":"(D) 本案業經新北市政府警察局109年7月24日新北警刑字第1094555725號函認受處分人行為時尚屬不罰，不裁處罰鍰及講習，本局僅針對毒品沒入處分，併予敘明。",
				"惟查行為人復於同(30)日22時40分另因施用、持有第三級毒品為本局查獲並裁罰在案(109年8月19日北市警刑毒緝字第1093013951號處分書)，故不處罰鍰、講習，查獲之毒品證物依規定沒入。":"(D) 惟查行為人復於同(30)日22時40分另因施用、持有第三級毒品為本局查獲並裁罰在案(109年8月19日北市警刑毒緝字第1093013951號處分書)，故不處罰鍰、講習，查獲之毒品證物依規定沒入。",
				"受處分人另涉及持有第二級毒品部分，經法院提審後，認逮捕程序於法未合，已當庭釋放；案經臺灣臺北地方檢察署偵查，認本案無令狀搜索適法性可議，其後所衍生之逮捕、調查及採尿等偵查作為，不具證據能力，故不予裁處，查獲之毒品證物依規定沒入。":"(D) 受處分人另涉及持有第二級毒品部分，經法院提審後，認逮捕程序於法未合，已當庭釋放；案經臺灣臺北地方檢察署偵查，認本案無令狀搜索適法性可議，其後所衍生之逮捕、調查及採尿等偵查作為，不具證據能力，故不予裁處，查獲之毒品證物依規定沒入。",
				"惟因本案屬無令狀搜索，程序合法部分查獲員警未能舉證，故其後所衍生之調查及採尿等偵查作為認定無證據能力，，故不予裁處，查獲之毒品證物依規定沒入。":"(D) 惟因本案屬無令狀搜索，程序合法部分查獲員警未能舉證，故其後所衍生之調查及採尿等偵查作為認定無證據能力，，故不予裁處，查獲之毒品證物依規定沒入。"
			};
			$('#seltype').append("<option value=''>請選擇加入</option>");
			$('#seltype').append("<option disabled>單純註記照罰</option>");
			$.each(seltype_A, function(key, value) {
				$('#seltype')
						.append($('<option>', { value : key })
						.text(value));
			});
			$('#seltype').append("<option disabled>免罰鍰</option>");
			$.each(seltype_B, function(key, value) {
				$('#seltype')
						.append($('<option>', { value : key })
						.text(value));
			});
			$('#seltype').append("<option disabled>免講習</option>");
			$.each(seltype_C, function(key, value) {
				$('#seltype')
						.append($('<option>', { value : key })
						.text(value));
			});
			$('#seltype').append("<option disabled>免罰鍰免講習</option>");
			$.each(seltype_D, function(key, value) {
				$('#seltype')
						.append($('<option>', { value : key })
						.text(value));
			});
			
			
            $("#seldate").change(function(){
            switch (parseInt($(this).val())){
                case 1: 
                $("#pmoney value").remove();
                $("#pmoney").val("20000");
                break;
                case 2: 
                $("#pmoney value").remove();
                $("#pmoney").val("30000");
                break;
                case 3:
				case 4:
				case 5:
				case 6:
				case 7:	 
                $("#pmoney value").remove();
                $("#pmoney").val("50000");
                break;
                default:
                $("#pmoney value").remove();
                $("#pmoney").val("0");
                break;
            }});
            $("#seltype").change(function(){
				$(".seltype_txt").text($(this).val())
            // switch (parseInt($(this).val())){
            //     case 0: 
            //     $("#seltype2 option").remove();
            //     $("#seltype2").append($("<option value='原本局108年4月10日北市警刑毒緝第1-83--45411號處分書作廢(列管編號:1081106)"
            //                         + "'>上年度處分書至本年度重發</option>"
            //                         +"<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署不起訴處分，倂此敘明"
            //                         + "'>單純不起訴處分</option>"
            //                         ));
            //     break;
            //     case 1: 
            //     $("#seltype2 option").remove();
            //     $("#seltype2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署處分緩起訴2年，且應至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併此敘明。"
            //                         + "'>臺北地檢緩起訴戒癮治療</option>"
            //                         +"<option value='受處分人另涉及第二級毒品部分。經臺灣士林地方檢察署處分緩起訴2年，且應至臺灣士林地方檢察署指定醫院於1年之治療期程內完成戒癮治療，併此敘明。"
            //                         + "'>士林緩起訴戒癮治療</option>"
            //                         +"<option value='受處分人另涉及第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署不起訴處分，倂此敘明"
            //                         + "'>已經勒戒</option>"
            //                         ));
            //     break;
            //     case 2: 
            //     $("#seltype2 option").remove();
            //     $("#seltype2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署刑事簡易判決處有期徒刑貳月，併此敘明。"
            //                         + "'>刑事簡易判決</option>"
            //                         +"<option value='受處分人另涉及第一級毒品部分。業經臺灣臺北地方檢察署刑事判決處有期徒刑七月。"
            //                         + "'>刑事判決</option>"
            //                         +"<option value='因本案受處分人與犯罪遭發覺前，由親屬主動向員警報案發現犯行，故不予處罰。"
            //                         + "'>親屬不罰</option>"
            //                         +"<option value='因本案受處分人與犯罪遭發覺前，主動向員警報案坦承犯行，故不予處罰。"
            //                         + "'>自首</option>"
            //                         ));
            //     break;
            // }
            });
            
            $("#zipcode2").twzipcode({
                "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_dpcounty',   // 預設值為 county
                'districtName' : 's_dpdistrict', // 預設值為 district
                'zipcodeName'  : 's_dpzipcode',// 預設值為 zipcode
				'zipcodeSel': '<?php if(isset($susp->s_dpzipcode))echo $susp->s_dpzipcode; ?>'
            });       
			$("#zipcode2").twzipcode('set', {
				'county': '<?php echo (isset($susp->s_dpcounty) && !empty($susp->s_dpcounty))?$susp->s_dpcounty:''; ?>',
				'district':' <?php echo (isset($susp->s_dpdistrict) && !empty($susp->s_dpdistrict))?$susp->s_dpdistrict:''; ?>'
			});  
            
            $("#zipcode").twzipcode({
                "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_rpcounty',   // 預設值為 county
                'districtName' : 's_rpdistrict', // 預設值為 district
                'zipcodeName'  : 's_rpzipcode', // 預設值為 zipcode
				'zipcodeSel':  '<?php if(isset($susp->s_rpzipcode))echo $susp->s_rpzipcode; ?>'
            }); 
			$("#zipcode").twzipcode('set', {
				'county': '<?php echo (isset($susp->s_rpcounty) && !empty($susp->s_rpcounty))?$susp->s_rpcounty:''; ?>',
				'district': '<?php echo (isset($susp->s_rpdistrict) && !empty($susp->s_rpdistrict))?$susp->s_rpdistrict:''; ?>'
			});  

            $("#zipcode3").twzipcode({
                "css": ["city form-control", "district form-control"],
                "zipcodeIntoDistrict": true,
                'countyName'   : 's_fadai_county',   // 預設值為 county
                'districtName' : 's_fadai_district', // 預設值為 district
                'zipcodeName'  : 's_fadai_zipcode', // 預設值為 zipcode
                'zipcodeSel'   : '<?php if(isset($suspfadai->s_fadai_zipcode)){echo $suspfadai->s_fadai_zipcode;}else{echo '100';}  ?>'
            });  
			$("#zipcode").twzipcode('set', {
				'county': '<?php if(isset($susp->s_dpcounty))echo $susp->s_dpcounty; ?>',
				'district': '<?php if(isset($susp->s_dpdistrict))echo $susp->s_dpdistrict; ?>'
			});
            
            $(function() {  
               $('a#pdfviewdrug').media({width:1400, height:800});  
               $('a#pdfviewod').media({width:1400, height:800});  
               $('a#pdfviewsusp').media({width:1400, height:800});  
               $('a#pdfviewcase').media({width:1400, height:800});  
               $('a#pdfviewresult').media({width:1400, height:800});  
               $('a#pdfviewjud').media({width:1400, height:800});  
               $('#pdfviewdrug').hide();  
               $('#pdfviewod').hide();  
               $('#pdfviewsusp').hide();  
               $('#pdfviewcase').hide();  
               $('#pdfviewresult').hide();  
               $('#pdfviewjud').hide();  
            });  
            
            $(document).ready(function (){
                $('#updateSanc').on('submit', function(e){
                    var age = 100;
                    var today = new Date();
                    var birthdate = new Date($('#s_birth').val());
                    age = today.getFullYear() - birthdate.getFullYear();
                    var m = today.getMonth() - birthdate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
                        age--;
                    }
                    //alert(age);
                    //e.preventDefault();
                    if( age < 20 ){
                        //alert('1');
                        if($('#s_fadai_ic').val()!="" && $('#s_fadai_name').val()!=""&& $('#s_fadai_address').val()!=""&& $('#s_fadai_phone').val()!=""){
                            //$("#updateSanc").submit();
                            //alert('2');
                            //e.preventDefault();
                        }else{
                            alert('受處分人未滿20歲，請輸入法代人資料');
                                e.preventDefault();
                            }
                    }
                    else{
                        //alert(age);
                            //$("#updateSanc").submit();
                        //e.preventDefault();
                    }
                });
            });
            function copyAddress(obj)
            {
                if(obj != 'undefined' && obj.val() != '')
                {
                    $("#now").attr('checked', true);
                    // $("#now").val(obj.val().split(':')[1] + ` (${obj.val().split(':')[0]})`);
                    $("#s_rpaddress").val(obj.val().split(':')[1] + ` (${obj.val().split(':')[0]})`);
					$("#address").val($("#s_rpaddress").val());
                }
                else
                {
                    $("#now").attr('checked', true);
                    $("#s_rpaddress").val('<?php echo $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress; ?>');
					$("#address").val('<?php echo $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress; ?>');
                }
            } 
            function listzipcode(obj, wrap) {
                $wrap = wrap;
                let formdata = new FormData();
                formdata.append('addr', obj.val());
            	$.ajax({
            		type: 'POST',
            		url: '<?php echo base_url("disciplinary_c/getZipCode") ?>',
            		dataType: 'json',
                    // data: {'addr': obj.val()},
                    data: formdata,
            		processData: false,
            		contentType: false,
            		cache: false,
            		complete: function (resp) {
                        // console.log(resp)						
                        $($wrap).val(resp.responseJSON.data);
						
            		}
            	});
				
            }
            function changeCourseEvent(obj)
            {
                $val = obj.val().replace(/\r\n|\n/g,"");
                $data = $val.split('-');
                if($val)
                {
					$("#fd_lec_wrap").removeClass('hidden');
                    $("input[name=fd_lec_time]").val($data[2]+'-'+$data[3]);
                    $("input[name=fd_lec_date]").val($data[0]+'('+$data[1]+')');
                    $("input[name=fd_lec_address]").val($data[4]);
                }
                else
                {
					$("#fd_lec_wrap").addClass('hidden');
                    $("input[name=fd_lec_time]").val('');
                    $("input[name=fd_lec_date]").val('');
                    $("input[name=fd_lec_address]").val('');
                }
				checkDupCourse();
            }
            function autoSave(){
                $.ajax({
            		type: 'POST',
            		url: '<?php echo base_url("disciplinary_c/autoUpdateSanc") ?>',
                    data: new FormData($("#updateSanc")[0]),
            		dataType: 'json',
            		processData: false,
            		contentType: false,
            		cache: false,
            		complete: function (resp) {
                        // console.log(resp.responseJSON)
                        $("#autoSavetxt").text('已自動儲存');
                        $(".alert").removeClass('hidden').fadeIn(500);

                        setTimeout(function () {
                            $('#autoSavetxt').text('');
                            $(".alert").addClass('hidden').fadeOut(500);
                        }, 2000);

                        // clearInterval(savetimer);
            		}
            	});
            }
			function addSeltype(obj){
				$sel_str = $("#seltype").val();
				$fd_dis_msg_content = $("#fd_dis_msg").val()
				$("#fd_dis_msg").val($fd_dis_msg_content + $sel_str);
			}
			function addDrugtype(){
				var drug =$('#drug').val().split(';');
				var arr =[];
                var level =[];
                var drugn =[];
                var all =[];
                var fin = '';
				let lv1 = [], lv2 = [], lv3 = [], lv4 = [], strdesp = [];
                for(let i=0;i<drug.length;i++){
                    level[i]=drug[i].substr(0,1);
					drugn[i]=drug[i].substr(2);
					$rm_str = drugn[i].split("（");
					switch (level[i]) {
						case '1':							
							lv1.push(($rm_str.length > 0)?$rm_str[0]:drugn[i]);
							break;
						case '2':
							lv2.push(($rm_str.length > 0)?$rm_str[0]:drugn[i]);
							break;
						case '3':
							lv3.push(($rm_str.length > 0)?$rm_str[0]:drugn[i]);
							break;
						default:
							lv4.push(($rm_str.length > 0)?$rm_str[0]:drugn[i]);
							break;
					}
                }
				if(lv1.length > 0) strdesp.push(`第1級毒品「${lv1.join('、')}」`);
				if(lv2.length > 0) strdesp.push(`第2級毒品「${lv2.join('、')}」`);
				if(lv3.length > 0) strdesp.push(`第3級毒品「${lv3.join('、')}」`);
				if(lv4.length > 0) strdesp.push(`第4級毒品「${lv4.join('、')}」`);
				fin = strdesp.join("及");

                $sel=(($('#sel').val() == '其他')?$("#e_name_other").val():$('#sel').val());
                $sel1=$('#sel1').val();
                
                // $sel3=$('#sel3').val();
				$sel3 = $("#drugObj").text();
                $("input[name='e_count']").each(function(){
                    arr.push($(this).val());
                })
				
				$messageSpan = $("#messageSpan").val()+ (($("#messageSpan").val())?"、":"") ;
					

                $("#messageSpan").val($messageSpan+$sel1+fin+$sel3.replace('-其他', ''));
			}
			function addDrugloc(obj){
				$sel2=$('#sel2').val();
				$messageSpan = $("#messageSpan").val();
                $("#messageSpan").val($messageSpan+$sel2);
			}
			function checkDupCourse(){
				if($("input[name='fd_lec_date']").val())
				{
					let formdata = new FormData();
					formdata.append('fd_lec_date', $("input[name='fd_lec_date']").val());
					formdata.append('fd_lec_time', $("input[name='fd_lec_time']").val());
					formdata.append('fd_lec_place', $("input[name='fd_lec_address']").val());
					formdata.append('fd_sic', $("input[name='s_ic']").val());
					$.ajax({
						type: 'POST',
						url: '<?php echo base_url("disciplinary_c/getCase_courseDup") ?>',
						data: formdata,
						dataType: 'json',
						processData: false,
						contentType: false,
						cache: false,
						complete: function (resp) {
							// console.log(resp.responseJSON.data)
							if(resp.responseJSON.data > 1)
							{
								if(!$("input[name=fd_lec_time]").val())
								{
									$("input[name=fd_lec_time]").val('');
									$("input[name=fd_lec_date]").val('');
									$("input[name=fd_lec_address]").val('');
								}
								
								$('#info-txt').removeClass('hidden');
							}							
							else
								$('#info-txt').addClass('hidden');
						}
					});
				}
				
            }
			function changeEnameOther(obj){
				if(obj.val() == '其他')
				{
					$("#e_name_other").removeClass('hidden');
					$("input[name='e_name']").val($("#e_name_other").val() + '-' + '其他');
				}
				else
				{
					$("#e_name_other").addClass('hidden');
					$("input[name='e_name']").val(obj.val());
				}
				
			}
			function getDruglevel(obj, wrap)
			{
				// console.log(wrap);
				let formdata = new FormData();
				formdata.append('druglv', obj.val());
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url("disciplinary_c/getDrugLevel") ?>',
					dataType: 'json',
					// data: {'addr': obj.val()},
					data: formdata,
					processData: false,
					contentType: false,
					cache: false,
					success: function(data){
						// console.log(data);
						wrap.html('');
						$.each(data.data, function(key, value) {
							wrap
								.append($('<option>', { value : value.drug_name })
								.text(value.drug_name));
						});
					},
					complete: function (resp) {
						// console.log(resp.data)								
					}
				});
			}
			function addDrugTr()
			{
				$('#drugIndTable tbody tr.empty-tr').remove();
				ind = $("#drugIndTable tbody tr").length;
				$("#drugIndTable tbody").append(`<tr class="editstate" data-id="" data-type="insert">
					<td>
					<select class="form-control input-sm" style="width: 8rem;" id="druglevel${ind+1}" onchange="getDruglevel($('#druglevel${ind+1}'), $('#drugname${ind+1}'))">
						<option value="">請選擇</option>
						<option value="1">1級</option>
						<option value="2">2級</option>
						<option value="3">3級</option>
						<option value="4">4級</option>
					</select>
					</td>
					<td>
					<select class="form-control input-sm" style="width: 15rem;" id="drugname${ind+1}"></select>
					</td>
					<td>
						<input type="text" class="form-control input-sm" id="drugcount${ind+1}" />
					</td>
				</tr>`);
			}
			function addUrineTr()
			{
				$('#UrineTestTable tbody tr.empty-tr').remove();
				ind = $("#UrineTestTable tbody tr").length;
				$("#UrineTestTable tbody").append(`<tr id="urinetr${ind+1}">
					<td>
					<select class="form-control input-sm" style="width: 8rem;" id="urinelevel${ind+1}" onchange="getDruglevel($('#urinelevel${ind+1}'), $('#urinename${ind+1}'))">
						<option value="">請選擇</option>
						<option value="1">1級</option>
						<option value="2">2級</option>
						<option value="3">3級</option>
						<option value="4">4級</option>
					</select>
					</td>
					<td>
					<select class="form-control input-sm" style="width: 15rem;" id="urinename${ind+1}"></select>
					</td>
					<td>
					<button type="button" class="btn btn-warning btn-xs" onclick="updateUrineEvent(\'insert\', $(\'#urinetr${ind+1}\'))"><span class="fa fa-floppy-o" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-danger btn-xs" onclick="removeUrineEvent($(\'#urinetr${ind+1}\'))"><span class="fa fa-trash" aria-hidden="true"></span></button>
					</td>
				</tr>`);
			}
			function loadDrugInd(id){
				$("#saveDrugInd").removeClass('btn-success').addClass('btn-warning').attr('onclick', 'updateDrugIndEvent(\'update\', $(this))').text('儲存');
				let formdata = new FormData();
				formdata.append('e_id', id);
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url("disciplinary_c/getDrudInd") ?>',
					dataType: 'json',
					// data: {'addr': obj.val()},
					data: formdata,
					processData: false,
					contentType: false,
					cache: false,
					success: function(data){
						// console.log(data.data);
						let postdata = data.data;
						let isother = false;
						let i = 0;
						$("#drugIndTable tbody").html("");
						postdata.forEach(element => {
							if(element.df_ingredient)
							{
								$("#drugIndTable tbody").append(`<tr data-id="${element.df_num}" data-type="update" ondblclick="editStateEvent($(this), ${i})">
																<td>${element.df_level}級</td>
																<td>${element.df_ingredient}</td>
																<td>${element.df_count}</td>
															</tr>`);
							
								i++;
							}
							
						});
						$("#saveDrugInd").attr('data-id', id);
						$("#sel option").each(function(el, index){
							// console.log($(this).val().indexOf(postdata[0].e_name));
							if($(this).val().indexOf(postdata[0].e_name) > -1)
							{
								$("#sel").val($(this).val()).change();
								
								$("#e_name_other").addClass('hidden');

								isother = false;

								return false;
							}
							else
							{
								isother = true;
							}
						})
						
						if(isother)
						{
							$("#sel").val("其他").change();
							$("#e_name_other").removeClass('hidden').val(postdata[0].e_name.replace('-其他', ''));
							changeEnameOther($("#sel"));
						}
						$("#drugpack_count").val(((postdata[0].e_count)?postdata[0].e_count:0));
						$("#sel3").val(((postdata[0].e_unit)?postdata[0].e_unit:'包'));
						$("#drugpack_weight").val(((postdata[0].e_1_N_W)?postdata[0].e_1_N_W:''));
					}
				});
			}
			
			function editStateEvent(obj, index){
				let td_level = obj.find('td:eq(0)').text().replace("級",'');
				let td_ind = obj.find('td:eq(1)').text();
				let td_count = obj.find('td:eq(2)').text();
				// console.log(td_level);
				obj.addClass('editstate');
				obj.find('td:eq(0)').html(`<select class="form-control input-sm" style="width: 8rem;" id="druglevel${index}" onchange="getDruglevel($('#druglevel${index}'), $('#drugname${index}'))">
						<option value="">請選擇</option>
						<option value="1">1級</option>
						<option value="2">2級</option>
						<option value="3">3級</option>
						<option value="4">4級</option>
					</select>`);
				$(`#druglevel${index}`).val(td_level).change();
				obj.find('td:eq(1)').html(`<select class="form-control input-sm" style="width: 15rem;" id="drugname${index}"></select>`);
				getDruglevel($('#druglevel'+index), $('#drugname'+index));
				
				obj.find('td:eq(2)').html(`<input type="text" class="form-control input-sm" id="drugcount${index}" />`);
				setTimeout(() => {
					$(`#drugname${index} option`).each(function(){
						if($(this).val() == td_ind)
						{
							$(this).attr('selected', true);
							return false;
						}
						else if($(this).val().indexOf(td_ind) > -1)
						{
							$(this).attr('selected', true);
						}
							
					});
                    // $(`#drugname${index}`).val(td_ind).change();
                  }, 500);
				
			}
			function editUrineStateEvent(obj, index){
				let td_level = obj.find('td:eq(0)').text().replace("級",'');
				let td_ind = obj.find('td:eq(1)').text();
				// console.log(td_level);
				obj.find('td:eq(0)').html(`<select class="form-control input-sm" style="width: 8rem;" id="urinelevel${index}" onchange="getDruglevel($('#urinelevel${index}'), $('#urinename${index}'))">
						<option value="">請選擇</option>
						<option value="1">1級</option>
						<option value="2">2級</option>
						<option value="3">3級</option>
						<option value="4">4級</option>
					</select>`);
				$(`#urinelevel${index}`).val(td_level).change();
				obj.find('td:eq(1)').html(`<select class="form-control input-sm" style="width: 15rem;" id="urinename${index}"></select>`);
				getDruglevel($('#urinelevel'+index), $('#urinename'+index));
				
				obj.find('td:eq(2)').html(`<button type="button" class="btn btn-warning btn-xs" onclick="updateUrineEvent(\'update\', $('#urinetr${index}'))"><span class="fa fa-floppy-o" aria-hidden="true"></span></button>`);
				setTimeout(() => {
					$(`#urinename${index} option`).each(function(){
						if($(this).val() == td_ind)
						{
							$(this).attr('selected', true);
							return false;
						}
						// else if($(this).val().indexOf(td_ind) > -1)
						// {
						// 	$(this).attr('selected', true);
						// 	// return false;
						// }
						else{
							$(`#urinename${index} option:eq(0)`).attr('selected', true);
						}
							
					});
                    // $(`#drugname${index}`).val(td_ind).change();
                  }, 500);
				
			}
			function resetDrugInd(){
				// $("#drugIndForm")[0].reset();
				$("#sel").val('晶體粉末、碎塊').change();
				$("#e_name_other").addClass('hidden').val('');
				$("#drugpack_count").val('0');
				$("#drugpack_weight").val('');
				$("#sel3").val('包').change();
				$("#drugIndTable tbody").html('<tr class=" empty-tr"><td colspan="3" class="text-center">點擊右上角新增成分</td></tr>');
				$("#saveDrugInd").removeClass('btn-warning').addClass('btn-success').attr('onclick', 'updateDrugIndEvent(\'insert\', $(this))').attr('data-id', '').text('新增');
			}
			function updateDrugIndEvent(type, obj){
				var mapped_df_drug = $("#drugIndTable tbody tr.editstate").find('select:eq(1)').map(function( index ) { 
					$rm_str = $(this).val().split("（");
					
					return ($rm_str.length > 0)?$rm_str[0]:$(this).val();
				})
				var mapped_df_level = $("#drugIndTable tbody tr.editstate").find('select:eq(0)').map(function( index ) { return $(this).val();})
				var mapped_df_count = $("#drugIndTable tbody tr.editstate").find('input').map(function( index ) { return $(this).val();})
				var mapped_df_id = $("#drugIndTable tbody tr.editstate").map(function( index ) { return $(this).attr('data-id');})
				var mapped_df_writetype = $("#drugIndTable tbody tr.editstate").map(function( index ) { return $(this).attr('data-type');})

				let formdata = new FormData();
				formdata.append('method', type);
				formdata.append('e_id', obj.attr('data-id'));
				formdata.append('e_c_num', $("input[name='s_cnum']").val());
				formdata.append('e_type', mapped_df_drug.toArray().join('、'));
				formdata.append('e_count', $("#drugpack_count").val());
				formdata.append('e_unit', $("#sel3").val());
				formdata.append('e_name', $("input[name='e_name']").val());
				formdata.append('e_1_N_W', $("#drugpack_weight").val());
				formdata.append('e_suspect', $("input[name='s_name']").val());
				formdata.append('e_s_ic', $("input[name='s_ic']").val());
				formdata.append('df_drug', mapped_df_drug.toArray().join(','));
				formdata.append('df_level', mapped_df_level.toArray().join(','));
				formdata.append('df_count', mapped_df_count.toArray().join(','));
				formdata.append('df_id', mapped_df_id.toArray().join(','));
				formdata.append('df_wtype', mapped_df_writetype.toArray().join(','));
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url("disciplinary_c/addDrugInd") ?>',
					dataType: 'json',
					// data: {'addr': obj.val()},
					data: formdata,
					processData: false,
					contentType: false,
					cache: false,
					success: function(data){
						// console.log(data);
						postdata = data.data;
						if(postdata.indexOf('error') == -1)
						{
							postdata = data.data;
							$("#drugListTable tbody").html('');
							postdata.forEach(element => {
								$("#drugListTable tbody").append(`<tr data-id="${element.e_id}" >
																	<td>${element.e_name}</td>
																	<td>${(element.e_count)?element.e_count:''}${(element.e_unit)?element.e_unit:''}</td>
																	<td>${element.e_type}</td>
																	<td>${(element.e_1_N_W)?element.e_1_N_W:''}</td>
																	<td>
																	<button type="button" class=" btn-link" data-toggle="modal" data-target="#drugModal" onclick="loadDrugInd(\'${element.e_id}\')">編輯</button>
																	</td>
																</tr>`);
							});
							$("#drugListTable tbody tr").bind('click', function(){
								$("#drugListTable tbody tr").removeClass('selected');
								$(this).toggleClass('selected');

								$("#drugObj").text($("#drugListTable tbody tr.selected td:eq(0)").text() + $("#drugListTable tbody tr.selected td:eq(1)").text());
								$("#drug").val($("#drugListTable tbody tr.selected td:eq(2)").text().replace(/\r\n|\n|\r|\t|\s/g,"").replace(/,|、/gi, ";"));
								
							});
							$("#drugModal").modal('hide');
						}
						else
						{
							swal({
								title: "注意!",
								text: "資料庫作業錯誤",
								icon: "warning"
							});
						}
					}
				});
			}
			function removeUrineEvent(obj)
			{
				obj.remove();
			}
			function updateUrineEvent(type, obj){
				var mapped_u_ind = obj.find('select:eq(1)').val();
				var mapped_u_level = obj.find('select:eq(0)').val();
				var mapped_u_id = obj.attr('data-id');

				let formdata = new FormData();
				formdata.append('method', type);
				formdata.append('sc_cnum', $("input[name='s_cnum']").val());
				formdata.append('sc_snum', $("input[name='s_num']").val());
				formdata.append('sc_ingredient', mapped_u_ind);
				formdata.append('sc_level', mapped_u_level);
				formdata.append('sc_num', mapped_u_id);
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url("disciplinary_c/addUrineTest") ?>',
					dataType: 'json',
					// data: {'addr': obj.val()},
					data: formdata,
					processData: false,
					contentType: false,
					cache: false,
					success: function(data){
						// console.log(data);
						postdata = data.data;
						if(postdata.indexOf('error') == -1)
						{
							postdata = data.data;
							objid = obj.attr('id');
							obj.find('td:eq(0)').html(postdata[0].sc_level + '級');
							obj.find('td:eq(1)').html(postdata[0].sc_ingredient);
							obj.find('td:eq(2)').html('');
							obj.attr('data-id', postdata[0].sc_num);
							obj.attr('ondblclick', 'editUrineStateEvent($(this), '+objid.replace('urinetr','')+')')
							
						}
						else
						{
							swal({
								title: "注意!",
								text: "資料庫作業錯誤",
								icon: "warning"
							});
						}
					}
				});
			}
        </script>
    </body>
</html>
