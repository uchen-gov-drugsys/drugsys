        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><button id='yes' class="btn btn-default"style="padding:0px 0px;" >批次確認送批</button></li>
                    <li><button id='no' class="btn btn-default"style="padding:0px 0px;" >批次確認寄出</button></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
            <form action="http://localhost/main2/disciplinary_c/uploaddp_doc" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                </form>
                <div class="container-fluid"> 
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <button id='yes' class="btn btn-default">批次確認送批</button>
							<?php
							echo (($rollback == 'Y')?'<button id="no_rb" class="btn btn-link">批次確認寄出</button>':'<button id="no" class="btn btn-link">批次確認寄出</button>');
							?>
                            
							<?php
							echo (($rollback == 'Y')?'<button id="rollback" class="btn btn-link">批次確認公示</button>':'');
							?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo form_open_multipart('disciplinary_c/updatestatus_ready','id="sp_checkbox"') ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading list_view">
                                        列表清單
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <?php echo $s_table;?>
                                        </div>                       
                                    </div>
                                    <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                    <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                    <input id="s_status" type="hidden" name="s_status" value=''> 
                                </div>
                                
                           </div>
                            <!-- /.panel -->
                        </div>
                            </form>
                        <!-- /.col-lg-12 -->
                    </div>
                   </form> <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': false
                    }
                 },
				 {
                    'targets': [4],
					"render": function(data, type, row, meta) {
                        return `<span class="fa fa-trash text-danger" aria-hidden="true" style="cursor:pointer;" onclick="delEvent(${data})"></span>`;
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
            //   'order': [[1, 'asc']]
            dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                $("#sp_checkbox").submit();
            });
			$("#rollback").click(function (){
                $("#s_status").val('rb');
                $("#sp_checkbox").submit();
            });
			$("#no_rb").click(function (){
                $("#s_status").val('rb_no');
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
    });
	function delEvent(id){
        swal({
            title: "確定刪除該專案?",
            text: "注意一經刪除將不能復原!",
            icon: "warning",
            buttons: ["取消", "確定"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var formData = new FormData();
                formData.append('dp_num', id);

                $.ajax({            
                    type: 'POST',
                    url: '../disciplinary_c/delete_DC_project',
                    data: formData,
                    // dataType: 'json',
                    processData : false, 
                    contentType: false,
                    cache: false,
                    error:function(){
                        console.log('error')
                    },
                    success: function(resp){
                        console.log('success');
                        if(resp !== 'error')
                            location.reload();
                        else
                            return false;
                    },
                    complete:function(resp){
                        // location.href = `../${resp.responseText}`;
                        console.log('complete')
                    }
                });
            } else {
                return false;
            }
        });
        
    }
    </script>
