<?php 
ini_set('display_errors','off');   
if (!$this -> session -> userdata('uic') && $this->uri->segment(1) != 'login' && $this->uri->segment(1) != '' && $this->uri->segment(2) != ''){
    $this->output
            ->set_status_header(403)
            ->set_content_type('text/html')
            ->set_output(file_get_contents( $this->load->view('403')))
            ->_display();
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $title;?></title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="<?php echo base_url(); ?>css/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="<?php echo base_url(); ?>css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.datatables.net/searchpanes/1.2.1/css/searchPanes.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
		<link href="<?php echo base_url(); ?>css/dataTables/select.dataTables.min.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="<?php echo base_url(); ?>css/dataTables/dataTables.responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/dataTables.checkboxes.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>css/startmin.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <style>
            strong { 
                font-weight: bold;
                color:red;
            }
            .error{
                color: red;
            }

        .thumbnail{
        position: relative;
        z-index: 0;
        }

        .thumbnail:hover{
        background-color: transparent;
        z-index: 50;
        }

        .thumbnail span{ /*CSS for enlarged image*/
        position: absolute;
        background-color: lightyellow;
        padding: 1px;
        top: -70px;
        left: 175px;
        border: 1px dashed gray;
        color: black;
        text-decoration: none;
        }

        .thumbnail span img{ /*CSS for enlarged image*/
        border-width: 0;
        padding: 2px;
        }
        .error {
            /* 當格式錯誤時，則新增此類別 */
            border-color: red !important
        }
		.lds-dual-ring {
			display: inline-block;
			width: 80px;
			height: 80px;
		}
		.lds-dual-ring:after {
			content: " ";
			display: block;
			width: 64px;
			height: 64px;
			margin: 8px;
			border-radius: 50%;
			border: 6px solid #337ab7;
			border-color: #337ab7 transparent #337ab7 transparent;
			animation: lds-dual-ring 1.2s linear infinite;
		}
		@keyframes lds-dual-ring {
		0% {
			transform: rotate(0deg);
		}
		100% {
			transform: rotate(360deg);
		}
		}
 /*position where enlarged image should offset horizontally */

        </style>
        <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?php echo base_url(); ?>js/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url(); ?>js/startmin.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.twzipcode.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery-validation-1.19.2/dist/jquery.validate.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery-validation-1.19.2/dist/additional-methods.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery-validation-1.19.2/dist/localization/messages_zh_TW.min.js"></script>
        <script src="<?php echo base_url(); ?>js/dataTables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>js/dataTables/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>js/dataTables.checkboxes.js"></script>
        <script src="<?php echo base_url(); ?>js/dataTables.checkboxes.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.media.js"></script>       
        <!--script src="https://cdnjs.com/libraries/pdf.js"></script-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/searchpanes/1.2.1/js/dataTables.searchPanes.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>          <!-- jQuery -->
        <script src="<?php echo base_url(); ?>js/jquery.multi-select.js"></script>
        <!-- jQuery -->
        <!-- Select2 -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <!-- Select2 i18 中文翻譯 -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/i18n/zh-TW.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/i18n/zh-TW.js"></script>

        <!-- Sweet Alert -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
         <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
<body>
    
    <?php $this->load->view($nav);?>
    <?php $this->load->view($include);?>
</body>
</html>
