                    <?php 
                        //$hidden = array('c_num' => $c_num, 's_office' => $s_office, 'e_ed_empno' =>e_ed_empno);
                        ini_set('display_errors','off');   
                        if (!$this -> session -> userdata('uic')){
                            $this->output
                                    ->set_status_header(403)
                                    ->set_content_type('text/html')
                                    ->set_output(file_get_contents( $this->load->view('403')))
                                    ->_display();

                                    sleep(5);
                                    redirect('login/index','refresh');
                            exit;
                            // redirect('login/logout','refresh');
                        }
                        if($this -> session -> userdata('em_priority') != '1' ){
                            
                            redirect('login/logout','refresh');
                            exit;
                        }
                        echo form_open('cases/createcases_num','id="newcases_num"');
                    ?>
                <!-- /.navbar-top-links -->
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 案件管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="javascript:{}" onclick="document.getElementById('newcases_num').submit(); return false;">案件新增</a>
                                        <?php echo form_close();?>
                                    </li>
                                    <li>
                                        <a href=<?php echo base_url("cases/listCases") ?>>案件列表</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 獎金管理<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href=<?php echo base_url("cases/list3Rewardlist") ?>>獎金申請</a>
                                    </li>
                                    <li>
                                        <a href=<?php echo base_url("cases/listRewardProject") ?>>一階獎金專案</a>
                                    </li>
                                    <li>
                                        <a href=<?php echo base_url("cases/list3RewardProject") ?>>獎金專案</a>
                                    </li>
                                    <li>
                                        <a href=<?php echo base_url("cases/rewardstate") ?>>獎金案件狀態</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <?php echo '<li><a href='.base_url("cases/admin").'>管理權限/修改密碼</span></a></li>'; ?>
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href=<?php echo base_url("login/changepw") ?>><i class="fa fa-sign-out fa-fw"></i> 更改密碼</a>
                            </li>
                            <li><a href=<?php echo base_url("login/logout") ?>><i class="fa fa-sign-out fa-fw"></i> 登出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>

