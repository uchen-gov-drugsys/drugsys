        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
										<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><button id='edit' class="btn btn-default" style="padding:0px 0px;">批次裁處</button></a></li>
                    <li><a><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#all">共用文件下載</button></a></li>
                    <li><a><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#other">其他文件下載</button></a></li>
                    <li><a href=<?php //echo base_url('surcharges/listProject')?>><button style="padding:0px 0px;" class="btn btn-default" >返回專案列表</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
										<div class="row" style="margin-top:35px;">
											<div class="col-md-12">
												<ol class="breadcrumb">
													<li class="active"><?php echo $url_1; ?></li>
													<li><a href="<?php echo base_url($url);?>">專案列表</a></li>
													<li class="active"><?php echo $title;?></li>
												</ol>
											</div>
										</div>
										<div class="row" style="letter-spacing:5px;">						
											<div class="col-md-6">
													<blockquote>
															<p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
													</blockquote>
											</div>
											<div class="col-md-6 text-right">
												<button class="btn btn-default" data-toggle="modal" data-target="#all">共用文件下載</button>
												<button class="btn btn-default" data-toggle="modal" data-target="#other">其他文件下載</button>
											</div>
										</div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                      
                                <!-- /.panel-heading -->
																<div class="panel-heading list_view">											
																	<h3 class="panel-title">列表清單</h3>
																</div>
																<?php echo form_open_multipart('Surcharges/updatesurchargesproject','id="sp_checkbox"') ?>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                    <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                    </div>                       
                                </div>
                                <!-- /.panel-body -->
                                <div class="modal fade" id="all" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
																			<div class="modal-header bg-primary">
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																				<h4 class="modal-title" id="othernewLabel">共用文件檢視</h4>
                                        
                                      </div>
																			<div class="modal-body">
																					<a href=<?php echo base_url('PDFcreate/fine_doc_project_fin_surc/'.$sp) ?> download >
																						<button  class="btn btn-default" type="button">原罰鍰處分書</button>
																					</a>
																					<a href=<?php if(isset($sp1[0]->fdd_doc)){echo base_url('送達文件/'.$sp1[0]->fdd_doc);}
																														else echo "#";?> download>
																						<button class="btn btn-default" type="button">罰鍰送達證書</button>
																					</a>
																					<a href=<?php echo base_url('surcharge_doc/'.$sp1[0]->surc_other_doc)?> download>
																						<button class="btn btn-default" type="button">毒防中心函&毒防中心名冊</button>
																					</a>
																			</div>
                                      
                                    </div>
                                  </div>
                                </div>
                                <div class="modal fade" id="other" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
																			<div class="modal-header bg-primary">
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																				<h4 class="modal-title"  id="othernewLabel">其他文件下載</h4>
                                        
                                      </div>
																			<div class="modal-body">
																					<a href=<?php echo base_url('XMLcreate/downloadnopunishxml/'.$sp)?> >
																						<button type="button" class="btn btn-default" >說明原因函</button>
																					</a>
																					<a href=<?php echo base_url('Surcharges/exportnopunishCSV/'.$sp)?> >
																						<button class="btn btn-default" type="button">清冊</button>
																					</a>
																			</div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
						<div class="modal fade" id="documentModal" tabindex="-1" role="dialog" aria-labelledby="documentModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
											<div class="modal-header bg-primary">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
													</button>
													<h4 class="modal-title" id="documentModalLabel">輸入備註</h4>
											</div>
											<div class="modal-body">
													<div class="form-group">
															<label>輸入備註</label>
															<textarea type="text" name="surc_comment" id="surc_comment" class="form-control" rows="5"></textarea>
																							
													</div>
											</div>
											<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
													<input type="button"  class="btn btn-default" data-id="" id="updateCommentBT" value="儲存" onclick="saveEvent($(this), 0)"/>
											</div>
									</div>
							</div>	
						</div>
        </div>
        <script>
            /*$(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });*/
        $(document).ready(function (){
           var table = $('#table1').DataTable({
               "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    },
                ],
              "width":"100px",
							'order': [[2, 'asc']],
							dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#edit").click(function (){
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);     
                //alert($('#s_cnum1').val());
                window.location.href = "<?php echo base_url('Surcharges/editSurc/') ?>"+$('#s_cnum1').val();
                //$("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
              var form = this;

              var rows_selected = table.column(0).checkboxes.selected();
                $('#example-console-rows').text(rows_selected.join(","));
                $('#s_cnum').val(rows_selected.join(","));
              
                // Output form data to a console     
                //$('#example-console-form').text($(form).serialize());
               
                // Remove added elements
                $('input[name="id\[\]"]', form).remove();
               
                // Prevent actual form submission
                //e.preventDefault();
           });
            
            $("#sel").change(function(){
            switch ($(this).val()){
                case "郵務送達" : 
                    $("#sel1 select").remove();
                break;
                case "囑託監所送達" : 
                    $("#sel1 select").remove();
                break;
                case "公示送達" : 
                    $("#sel1 select").remove();
                break;
                case "不裁罰" : 
                    $("#sel1 select").remove();
                    var array = [ "在監","未合法送達","其他" ];
                    //利用each遍歷array中的值並將每個值新增到Select中
                    $("#sel1").append("<select name='sp_reason1' class='form-control'>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                        $("#sel1").append("<select name='sp_reason2' class='form-control'>"
                        +"<option selected value=NULL>原因2</option>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                break;
            }});
        });
				function modalEvent(id, default_fp_num = '')
				{
						$("#surc_comment").val(default_fp_num);
						$('#updateCommentBT').attr('data-id', id);
				}
				function saveEvent(obj, pj_type = 0)
				{
								var formData = new FormData();
								formData.append('surc_num', obj.attr('data-id'));
								formData.append('surc_comment', $("#surc_comment").val());
										

								$.ajax({            
										type: 'POST',
										url: '<?php echo base_url('Surcharges/insertcomment'); ?>',
										data: formData,
										// dataType: 'json',
										processData : false, 
										contentType: false,
										cache: false,
										error:function(){
												console.log('error')
										},
										success: function(resp){
												console.log('success');
												if(resp !== 'error')
														location.reload();
														// return true;
												else
														location.reload();
														return false;
										},
										complete:function(resp){
												// location.href = `../${resp.responseText}`;
												console.log('complete')
										}
								});
								
				}
    </script>

    </body>
</html>
