        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><button id='yes' class="btn btn-default"style="padding:0px 0px;" form="sp_checkbox" >批次寄出</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							
							<!-- <a><button id='yes' class="btn btn-info" form="sp_checkbox" >批次寄出</button></a> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
							<!-- Nav tabs -->
							<ul class="nav nav-pills" role="tablist">
								<li role="presentation" class="active"><a href="#normal" aria-controls="normal" role="tab" data-toggle="tab">一般裁罰</a></li>
								<!-- <li role="presentation"><a href="#prison" aria-controls="prison" role="tab" data-toggle="tab">在監</a></li> -->
								<li role="presentation"><a href="#return" aria-controls="return" role="tab" data-toggle="tab">不裁罰</a></li>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content" style="margin-top:15px;">
								<div role="tabpanel" class="tab-pane active" id="normal">
									<div class="panel panel-primary">
										<div class="panel-heading list_view">											
											<div class="row">
												<div class="col-md-6">
													<h3 class="panel-title">列表清單 ( 一般裁罰 )</h3>
												</div>
												<div class="col-md-6 text-right">
													<button id='yes' class="btn btn-info btn-sm">批次發文</button>
												</div>
											</div>
										</div>
										<!-- /.panel-heading -->
										<div class="panel-body">
										<?php echo form_open_multipart('Surcharges/updatestatus','id="sp_checkbox"') ?>  
											  
											<div class="table-responsive">
												<?php echo $s_table;?>
											</div>                       
											<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
											<input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
											<input id="s_status" type="hidden" name="s_status" value=''> 
										<?php echo form_close(); ?>
										</div>
										
										<!-- /.panel-body -->
									</div>
								</div>
								<!-- <div role="tabpanel" class="tab-pane" id="prison">
									<div class="panel panel-primary">
										<div class="panel-heading list_view">
											<h3 class="panel-title">列表清單 ( 在監 )</h3>
										</div>
										<div class="panel-body"> 
											<div class="table-responsive">
												<?php //echo $s_prison_table;?>
											</div>                       
										</div>
									</div>
								</div> -->
								<div role="tabpanel" class="tab-pane" id="return">
									<div class="panel panel-primary">
										<div class="panel-heading list_view">
											<h3 class="panel-title">列表清單 ( 不裁罰 )</h3>
										</div>
										<!-- /.panel-heading -->
										<div class="panel-body">  
											<div class="table-responsive">
												<?php echo $s_return_table;?>
											</div>   
										</div>
										
										<!-- /.panel-body -->
									</div>
								</div>
							</div>
                            
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <script>
            /*$(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });*/
        $(document).ready(function (){
           table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': 0,
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
            //   'select': {
            //      'style': 'multi'
            //   },
            //   "width":"100px",
			  'order': [[2, 'asc']],
			  dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
		   table2 = $('#table2').DataTable({
            //   'columnDefs': [
            //      {
            //         'targets': 0,
            //         'checkboxes': {
            //            'selectRow': true
            //         }
            //      }
            //   ],
            //   'select': {
            //      'style': 'multi'
            //   },
            //   "width":"100px",
			  'order': [[2, 'asc']],
			  dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
		   table3 = $('#table3').DataTable({
            //   'columnDefs': [
            //      {
            //         'targets': 0,
            //         'checkboxes': {
            //            'selectRow': true
            //         }
            //      }
            //   ],
            //   'select': {
            //      'style': 'multi'
            //   },
              "width":"100px",
			  'order': [[1, 'asc']],
			  dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

			$("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
            
            $("#sel").change(function(){
            switch ($(this).val()){
                case "郵務送達" : 
                    $("#sel1 select").remove();
                break;
                case "囑託監所送達" : 
                    $("#sel1 select").remove();
                break;
                case "公示送達" : 
                    $("#sel1 select").remove();
                break;
                case "不裁罰" : 
                    $("#sel1 select").remove();
                    var array = [ "在監","未合法送達","其他" ];
                    //利用each遍歷array中的值並將每個值新增到Select中
                    $("#sel1").append("<select name='sp_reason1' class='form-control'>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                        $("#sel1").append("<select name='sp_reason2' class='form-control'>"
                        +"<option selected value=NULL>原因2</option>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                break;
            }});
        });
		function delEvent(id){
			swal({
				title: "確定刪除該怠金專案?",
				text: "注意一經刪除將不能復原!",
				icon: "warning",
				buttons: ["取消", "確定"],
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					var formData = new FormData();
					formData.append('sp_num', id);
					$.ajax({            
						type: 'POST',
						url: '../Surcharges/delete_Surc_project',
						data: formData,
						// dataType: 'json',
						processData : false, 
						contentType: false,
						cache: false,
						error:function(){
							console.log('error')
						},
						success: function(resp){
							// console.log('success');
							if(resp !== 'error')
								location.reload();
							else
								return false;
						},
						complete:function(resp){
							// location.href = `../${resp.responseText}`;
							console.log('complete')
						}
					});
				} else {
					return false;
				}
			});
		}
    </script>

    </body>
</html>
