        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><button id='edit' class="btn btn-default"style="padding:0px 0px;" >批次裁處編輯</button></li> -->
                    <!--li><button id='yes' class="btn btn-default"style="padding:0px 0px;" >確認修改</button></li-->
                    <!-- <li><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#exampleModal">輸入發文日期</button></li>
                    <li><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#uploadlfile">上傳公文函(第一頁)</button></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
            
                <div class="container-fluid"> 
					<div class="row" style="margin-top:35px;">
						<div class="col-md-12">
                            <ol class="breadcrumb">
								<li class="active"><?php echo $url_1; ?></li>
                                <li><a href="<?php echo base_url($url);?>">專案處理</a></li>
                                <li class="active"><?php echo $title;?></li>
                            </ol>
                        </div>
					</div>
                    <div class="row" style="letter-spacing:5px;">						
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <button id='edit' class="btn btn-warning" >批次裁處編輯</button>
                            
                            <!-- <button class="btn btn-default" data-toggle="modal" data-target="#uploadlfile">上傳公文函(第一頁)</button> -->
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading list_view">
									<div class="row">
										<div class="col-md-6">
											<h4 class="panel-title">列表清單</h4>
										</div>
										<div class="col-md-6 text-right">
											<button class="btn btn-default btn-sm" data-toggle="modal" data-target="#paydateModal">輸入繳款日期、承辦人</button>
											<button class="btn btn-default btn-sm" data-toggle="modal" data-target="#courseModal">輸入講習</button>
										</div>
									
									</div>
                                    
                                </div>
                                <!--div class="panel-heading list_view">
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 案件編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 人員編號
                                    <input type="checkbox" name="list" data-target ="3" checked> 姓名
                                    <input type="checkbox" name="list" data-target ="4" checked> 證號
                                    <input type="checkbox" name="list" data-target ="5" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="6" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="7" checked> 犯罪手法
                                    <input type="checkbox" name="list" data-target ="8" checked> 毒品號
                                    <input type="checkbox" name="list" data-target ="9" checked> 成分
                                    <input type="checkbox" name="list" data-target ="10" checked> 級數
                                    <input type="checkbox" name="list" data-target ="11" checked> 淨重
                                    <input type="checkbox" name="list" data-target ="12" checked> 純質淨重
                                    <input type="checkbox" name="list" data-target ="13" checked> 建議金額
                                </div-->
                                <div class="panel-body">
									<div class="row">
										<div class="col-md-12">
										<form class="row">
											<div class="form-group col-md-2">
												<label>繳款日期</label>
												<h4><span class="label label-default"><?php echo (($sp_expirdate == '-1911' || $sp_expirdate == '-19110000')?'':$sp_expirdate); ?></span></h4>
											</div>
											<div class="form-group col-md-2">
												<label>專案承辦人</label>
												<h4><span class="label label-default"><?php echo ((isset($sp_empno))?$sp_empno:''); ?></span></h4>
											</div>
											<div class="form-group col-md-6">
												<label>講習</label>
												<h4>
													<?php $courseAry = '';
													if(isset($sp_course) && !empty($sp_course) )
													{
															$courseAry = explode('-', preg_replace('/\r\n|\n/',"",$sp_course));
															echo '<span class="label label-default">'.$courseAry[0] . '(' .$courseAry[1].') ' .$courseAry[2].'-'.$courseAry[3] . ' ' .$courseAry[4] .'</span>';
													}
													else
													{
														echo '';
													}; ?>
													
												</h4>
											</div>
										</form>
										</div>
									</div>
									<hr>
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>     
									<?php echo form_open_multipart('disciplinary_c/uploaddpdate','id="uploaddpdate"') ?>
									<form  id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
									<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
									<input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
									<input id="dp_status" type="hidden" name="dp_status" value='<?php echo$sp_status?>'> 
									<input id="dp_num" type="hidden" name="dp_num" value='<?php echo$sp_num?>'>  
									</form>    
									</form             
                                </div>
                                
                           </div>
                            <!-- /.panel -->
                        </div>

							
							<!-- 繳款視窗 -->
                            <div class="modal fade" id="paydateModal" tabindex="-1" role="dialog" aria-labelledby="paydateModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title">繳款日期</h4>
                                  </div>
								  <?php echo form_open('Surcharges/uploaddpexpdate/'.$id); ?>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>輸入繳款日期</label>
										<?php
										$sp_send_date = array(
												'name'          => 'sp_expirdate',
												'id'            => 'sp_expirdate',
												'class'         => 'rcdate form-control',
												'value'			=> ($sp_expirdate == '-1911' || $sp_expirdate == '-19110000')?'':$sp_expirdate
										);
										
										echo form_input($sp_send_date);
										echo form_hidden('sp_num', $sp_num);
										?>
                                        <!-- <input name="f_date" type="text" class="rcdate form-control">                 -->                     
										<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>                       
                                      </div>
									  <div class="form-group">
                                        <label>專案承辦人</label>
										<br>
										<label class="text-primary"><?php echo ((isset($sp_empno))?$sp_empno:''); ?></label>
										<select name="sp_empno" id="sp_empno" style="width: 100%" class="form-control js-example-basic-single js-states" ></select>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
									<?php echo form_submit('save_dpdate', '儲存', "class='btn btn-default'"); ?>
                                    <!-- <button id='no' class="btn btn-default" >儲存</button> -->
                                  </div>
								  <?php  echo form_close();  ?>
                                </div>
                              </div>
                            </div> 
							<!-- 講習視窗 -->
                            <div class="modal fade" id="courseModal" tabindex="-1" role="dialog" aria-labelledby="courseModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header bg-primary">
								  	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        							<h4 class="modal-title">講習時段</h4>
                                  </div>
								  <?php echo form_open('Surcharges/uploaddpcourse/'.$id); ?>
                                  <div class="modal-body">
                                      <div class="form-group">
									  	<label>講習時段</label> 
										<?php 
										$courseAry = '';
										if(isset($sp_course) && !empty($sp_course) )
										{
												$courseAry = explode('-', preg_replace('/\r\n|\n/',"",$sp_course));
										};
										foreach ($course_opt as $key => $value) {	
											$value = preg_replace('/\r\n|\n/',"",$value);
										}
										
										echo form_dropdown('sp_lec',$course_opt,((isset($sp_course) && !empty($sp_course))?str_replace('_', '-', $sp_course):''),'class="form-control" id="sp_lec" onchange="changeCourseEvent($(this))"'); ?>                    
                                      </div>
									  <div class="form-group">
									  	<label>講習時數</label>
										<p class="fd_lec_time"><?php echo (($courseAry != '')?$courseAry[2].'-'.$courseAry[3] :''); ?></p>
									  </div>
									  <div class="form-group">
									  	<label>講習時間</label>
										<p class="fd_lec_date"><?php echo (($courseAry != '')?$courseAry[0] . '(' .$courseAry[1].')' :''); ?></p>
									  </div>
									  <div class="form-group">
									  	<label>講習地點</label>
										<p class="fd_lec_address"><?php echo (($courseAry != '')?$courseAry[4] :''); ?></p>
									  </div>
									  <?php echo form_hidden('sp_num', $sp_num); ?>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
									<?php echo form_submit('save_dpdate', '儲存', "class='btn btn-default'"); ?>
                                    <!-- <button id='no' class="btn btn-default" >儲存</button> -->
                                  </div>
								  <?php  echo form_close();  ?>
                                </div>
                              </div>
                            </div> 

							
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
            
                <?php //echo form_open_multipart('disciplinary_c/uploadpublicdoc') ?>
                            <div class="modal fade" id="uploadlfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">上傳公文函(第一頁)</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        Type Folder Name:<input type="text" name="foldername" /><br/><br/>
                                        <input id="id" type="hidden" name="id" value='<?php echo$id?>'> 
                                        Select Folder to Upload: <input type="file" name="files[]" id="files" multiple directory="" webkitdirectory="" moxdirectory="" /><br/><br/>                                      </div>
                                    <input type="Submit" value="Upload" name="upload" />
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button  class="btn btn-default" >確認上傳</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
            </form>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
                $( "#uploaddpdate" ).validate({
                    rules: {
                        dp_send_date: {
                            required: true,
                        },
                    },
                    messages: {
                        dp_send_date: {
                            required: "此欄位不得為空",
                        },
                    }
                });        
           table = $('#table1').DataTable({
               "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    },
                ],
                dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
            $("#edit").click(function (){
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);     
                //alert($('#s_cnum1').val());
                window.location.href = '<?php echo base_url("surcharges/editSanc/") ?>' +$('#s_cnum1').val() + '/<?php echo $id; ?>';
                //$("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);     
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);
                //alert($('#s_cnum1').val());
            e.preventDefault();
           });

		   $(".rcdate").change(function(){
				let strlen = $(this).val().length;
				if(strlen < 7 || strlen < "7")
				{
					if(strlen != 0)
						$(this).val("0"+$(this).val());
				}
				else
				{
					return false;
				}
					
			});
			$(".rcdate").keypress(function(){
				if($(this).val().length >= 7)
					return false;
			}); 

			$('#sp_empno').select2({
				placeholder: "選擇其他承辦人",
				ajax: {
					url: '<?php echo base_url("surcharges/getthirdempno?unselected=".$sp_empno) ?>',
					dataType: 'json',
					data: function (params) {
					var query = {
						search: params.term,
						type: 'public'
					}

					// Query parameters will be ?search=[term]&type=public
					return query;
					}
				}
			});
			$('#paydateModal').on('hide.bs.modal', function (event) {
				$('#sp_empno').val('').change(); 
			});
    });

	function changeCourseEvent(obj)
	{
		$val = obj.val().replace(/\r\n|\n/g,"");
		$data = $val.split('-');
		if($val)
		{
			$(".fd_lec_time").text($data[2]+'-'+$data[3]);
			$(".fd_lec_date").text($data[0]+'('+$data[1]+')');
			$(".fd_lec_address").text($data[4]);
		}
		else
		{
			$(".fd_lec_time").text('');
			$(".fd_lec_date").text('');
			$(".fd_lec_address").text('');
		}
	}
    </script>
