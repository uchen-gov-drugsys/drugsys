		<style>
			#drugListTable tbody tr.selected {
				background-color: #f5f5f5;
			}
		</style>
		<div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">
        <style>
            strong { 
                font-weight: bold;
                color:black;
            }
        </style>
		<?php 
			if (!$this -> session -> userdata('uic')){
				$this->output
						->set_status_header(403)
						->set_content_type('text/html')
						->set_output(file_get_contents( $this->load->view('403')))
						->_display();

						sleep(5);
						redirect('login/index','refresh');
				exit;
			}
			if($this -> session -> userdata('em_priority') != '3' ){
				
				redirect('login/logout','refresh');
				exit;
			}

			function isAudlt($birth, $catchdate){
				list($year,$month,$day) = explode("-",$birth);
				$nowyear = date("Y", strtotime($catchdate));
				$nowmonth = date("m", strtotime($catchdate));
				$nowday = date("d", strtotime($catchdate));
				$audlt = 1; // 成年

				if(($year+20) <= $nowyear)
				{
					if(($year+20) == $nowyear)
					{
						if($month*1 <= $nowmonth*1)
						{
							if($month*1 == $nowmonth*1)
							{
								$audlt = ($day > $nowday)?0:1;
							}
						}							
						else
							$audlt = 0;
					}
				}
				else
				{
					$audlt = 0;
				}

				return (($audlt==0)?false:true);
			}
		?>
                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><a href><?php //echo $title?></a></li>
                    <li><a href=<?php //echo base_url('PDFcreate/fine_doc/'.$sp->fd_snum)?>><button style="padding:0px 0px;" class="btn btn-default" >下載處分書</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" form="updateready">修改</button></a></li> -->
                    <?php //if($prev != NULL){?>
                        <!-- <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='prev'>上一個</button></a></li> -->
                    <?php //}else{ ?>
                        <!-- <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">第一筆</button></a></li> -->
                    <?php //} ?>
                    <?php //if($next != NULL){?>
                        <!-- <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='next'>下一個</button></a></li> -->
                    <?php //}else{ ?>
                        <!-- <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">最後一筆</button></a></li> -->
                    <?php //} ?>
                    <!-- <li><a href="<?php//echo base_url('Disciplinary_c/listdp1_ready/'.$id)?>"><button style="padding:0px 0px;" class="btn btn-default" >返回列表</button></a></li> -->
                    <!--<li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" >送批</button></a></li> 先拿掉 怕使用者沒確認修改完直接送批-->
                </ul>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href= <?php echo base_url("login/logout") ?>><i class="fa fa-sign-out fa-fw"></i> 登出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
                <!-- /.navbar-static-side -->
                <div id="page-wrapper" style="margin-left:20px;">
                <div class="container-fluid">
                    <div class="row"  style="margin-top:35px;">
                        <div class="col-md-3">
                            <ol class="breadcrumb">
                                <li><a href="<?php echo base_url("Surcharges/listdp1_ready/". $pjid);?>">每日處理專案: <?php echo $pjid;?></a></li>
                                <li class="active"><?php echo $title;?></li>
                            </ol>
                        </div>
                        <div class="col-md-9 text-right">
							<span class="label label-default" style="font-size:16px;">目前位置：<?php echo ($current_loc + 1) . '/' . $total_nums; ?></span>
                            <a href="<?php echo base_url('Surcharges/listdp1_ready/'.$pjid)?>"><button  class="btn btn-default" >返回列表</button></a>
                            <a href=<?php echo base_url('PDFcreate/surc_doc/'.$surc->surc_num. '/'.$pjid)?>><button class="btn btn-default" >下載處分書</button></a>
                            <?php if($prev != NULL){?>
                                <a href="#"><button class="btn btn-link" id='prev'>上一個</button></a>
                            <?php }else{ ?>
                                <a href="#"><button class="btn btn-link">第一筆</button></a>
                            <?php } ?>
                            <?php if($next != NULL){?>
                                <a href="#"><button class="btn btn-link" id='next'>下一個</button></a>
                            <?php }else{ ?>
                                <a href="#"><button class="btn btn-link">最後一筆</button></a>
                            <?php } ?>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="letter-spacing:5px;">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <p class="page-header"></p>
                        </div>
                    </div> -->
                <!-- /.navbar-static-side -->
                    <form action="<?php echo base_url('surcharges/updateready'); ?>" id="updateready" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                    <div class="row">
                        <div class="col-lg-4">
							<div class="row">
								<div class="col-md-12 text-right">
									<a href="#"><button  class="btn btn-warning btn-sm" form="updateready">修改</button></a>
								</div>
							</div>
							<hr>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#susp" aria-expanded="true" aria-controls="collapseOne">
											受處分人
											</a>
											
										</h4>
                                    </div>
                                    <div id="susp" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div class="row">
												<div class="form-group col-md-12">
                                                    <label>發文字號</label>
                                                    <?php echo form_input('sp_send_no',$sp->sp_send_no.$sort, 'class="form-control"')?>
													<?php echo form_hidden('sort', $sort); ?>
													<?php echo form_hidden('main_send_no',$sp->sp_send_no ); ?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>受處分人姓名</label>
                                                    <?php echo form_input('surc_name',$surc->surc_name, 'class="form-control"')?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>身份證字號</label>
                                                    <?php echo form_input('surc_sic',$surc->surc_sic, 'class="form-control"')?>
                                                </div>
                                                
                                                <div class="form-group col-md-12">
                                                    <label>出生日期</label>
                                                    <input id="s_birth" name="s_birth" type="text" class="rcdate form-control" value="<?php echo (isset($surc->s_birth))?((strlen($surc->s_birth) > 7 && $surc->s_birth != '0000-00-00')?str_pad(((int)substr($surc->s_birth, 0, 4) - 1911),3,"0",STR_PAD_LEFT).substr($surc->s_birth, 5, 2).substr($surc->s_birth, 8, 2):''):'';?>">

                                                    <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>  
                                                    <?php echo form_hidden('s_num','')?>
													<?php echo form_hidden('s_cnum','')?>
													<?php echo form_hidden('surc_num', $surc->surc_num); ?>
													<?php echo form_hidden('surc_projectnum',$surc->surc_projectnum);?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>聯絡電話</label>
                                                    <?php
                                                        // if(isset($phone)) {
														//     echo form_input('p_no',$phone->p_no, 'class="form-control"');
														// }
														//      else {
															echo form_input('surc_phone','', 'class="form-control"');
															// }
                                                    ?>
                                                </div>
                                                <div class="form-group col-md-12">
												<label>在監狀況</label>
												<?php echo form_dropdown('surc_prison',$opt,$surc->surc_prison,'class="form-control" onChange="copyAddress($(this))"') ?>
                                                </div>
                                            </div>
                                            <div class="row">
												<div class="form-group col-sm-12">
													<?php echo form_input('surc_zipcode', $surc->surc_zipcode, 'class="form-control"'); ?>
												</div>
                                                <div class="form-group col-md-12">
                                                        <div class="radio">
                                                            <label>
                                                                <input id="now" name="ad" type="radio" <?php echo (isset($surc->surc_live_state) && $surc->surc_live_state != 1)?'checked':''; ?> value="2">現居地地址(勾選是否送達)
                                                            </label>
                                                        </div>
                                                        <input id="s_rpaddress" class="form-control" placeholder="e.g.XX街/路XX號" value="<?php echo (!empty($surc->surc_prison))?explode(':', $surc->surc_prison)[0] .'('.explode(':', $surc->surc_prison)[1].')' :''; /*(!empty($susp->s_prison))?explode(':', $susp->s_prison)[0] .'('.explode(':', $susp->s_prison)[1].')' :$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress*/ ?>" readonly>
                                                        
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <div class="radio">
                                                        <label>
                                                            <input id="orgin" name="ad" type="radio" <?php echo ($surc->surc_live_state == 1 || null == $surc->surc_live_state)?'checked':''; ?> value="1">戶籍地址(勾選是否送達)
                                                        </label>
                                                    </div>
                                                    <input id="ori_rpaddress" class="form-control" placeholder="e.g.XX街/路XX號" value="<?php echo $surc->surc_address; /*$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress*/ ?>" readonly>
                                                </div>
												<?php echo form_hidden('surc_address', $surc->surc_address); ?>
												
                                            </div>
                                    </div>
                                    </div>
                                </div>
                                <?php
                                    // list($year,$month,$day) = explode("-",$surc->s_birt);
                                    // $year_diff = date("Y") - $year;
                                    // $month_diff = date("m") - $month;
                                    // $day_diff  = date("d") - $day;
                                    // if ($day_diff < 0 || $month_diff < 0)
                                    //     $year_diff--;

									// if(!isAudlt($surc->s_birth, $cases->s_date)){
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#susp_fadi" aria-expanded="false" aria-controls="collapseTwo">
                                        法代人(未年滿20歲開啟)
                                        </a>
                                    </h4>
                                    </div>
                                    <div id="susp_fadi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label>姓名</label>
                                                    <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_name',$suspfadai->s_fadai_name, 'class="form-control" id="s_fadai_name"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_name','', 'class="form-control" id="s_fadai_name"');
                                                        }?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>性別</label>
                                                    <?php 
                                                        $options = array(
                                                            '男'=> '男',
                                                            '女'=> '女',
                                                        );
                                                        if(isset($suspfadai)) {
                                                            echo form_dropdown('s_fadai_gender',$options,$suspfadai->s_fadai_gender, 'class="form-control" id="s_fadai_gender"');
                                                        }
                                                             else {
                                                                 echo form_dropdown('s_fadai_gender',$options,'', 'class="form-control" id="s_fadai_gender"');
                                                        }?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>身份證字號</label>
                                                    <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_ic',$suspfadai->s_fadai_ic, 'class="form-control" id="s_fadai_ic"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_ic','', 'class="form-control" id="s_fadai_ic"');
                                                        }?>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>出生日期</label>
                                                    <input id="s_fadai_birth" name="s_fadai_birth" type="text" class="rcdate form-control" value="<?php echo (isset($suspfadai->s_fadai_birth))?((strlen($suspfadai->s_fadai_birth) > 7 && $suspfadai->s_fadai_birth != '0000-00-00')?str_pad(((int)substr($suspfadai->s_fadai_birth, 0, 4) - 1911),3,"0",STR_PAD_LEFT).substr($suspfadai->s_fadai_birth, 5, 2).substr($suspfadai->s_fadai_birth, 8, 2):''):'';?>">
                                                    <span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>     
                                                    
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label>聯絡電話</label>
                                                    <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_phone',$suspfadai->s_fadai_phone, 'class="form-control" id="s_fadai_phone"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_phone','', 'class="form-control" id="s_fadai_phone"');
                                                        }?>
                                                </div>                                                
                                                <div class="form-group col-md-12">
                                                    <label>地址</label>
                                                    <div id="zipcode3"></div>
                                                        <?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_address',$suspfadai->s_fadai_address, 'class="form-control" id="s_fadai_address" placeholder="e.g.XX路XX門牌"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_address','', 'class="form-control" id="s_fadai_address" placeholder="e.g.XX路XX門牌"');
                                                        }?>
                                                </div>

                                            </div>
                                    </div>
                                    </div>
                                </div>
                                <?php //} ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#course" aria-expanded="false" aria-controls="collapseThree">
                                        講習
                                        </a>
                                    </h4>
                                    </div>
                                    <div id="course" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                    <input id="nextvalue" type="hidden" name="next" value=""> 
                                    <input id="prevvalue" type="hidden" name="prev" value=""> 
										<div class="row">
											<div class="form-group col-md-12">
												<p class="text-primary"><label>專案預設講習</label><span>
												<?php 
												$default = '';
												if(isset($sp->sp_course) && !empty($sp->sp_course) )
												{
														$default = explode('-', str_replace('_', '-', preg_replace('/\r\n|\n/',"",$sp->sp_course)));
														echo $default[0] . '(' .$default[1].') ' .$default[2].'-'.$default[3] . ' ' .$default[4];
												}
												else
												{
													echo '';
												}; ?>
												</span></p>
												<p class="text-muted"><label>個案目前講習</label><span>
												<?php
												if(!empty($surc->surc_lec_date) && isset($surc->surc_lec_date) )
												{
													if(is_array($default))
													{
														if($default[0] . '(' .$default[1].')' == $surc->surc_lec_date)
														{
															echo '同上';
														}
														else
														{
															echo $surc->surc_lec_date . $surc->surc_lec_time . $surc->surc_lec_place. '(' . $surc->surc_lec_address . ')';
														}
													}
													
												}
												else
												{
													echo '同上';
												}; ?>
												</span></p>
											</div>
										</div>
                                        <div class="row">
											<div class="form-group col-md-12">
												<label>講習時段(預設講習時段)</label> 
												<?php 
													
												$courseAry = '';
												if(isset($sp->sp_course) && !empty($sp->sp_course) )
												{
													// if(explode('_', preg_replace('/\r\n|\n/',"",$dp->dp_course))[0] == explode('(', $sp->fd_lec_date)[0])
													// {
														$courseAry = explode('-', str_replace('_', '-', preg_replace('/\r\n|\n/',"",$sp->sp_course)));
													// }
													// else
													// {
													// 	$courseAry = '';
													// }
												};
												
												foreach ($course_opt as $key => $value) {														
													if(($courseAry != ''))
													{
														if(trim($courseAry[0]) == explode(' (', preg_replace('/\r\n|\n/',"",$course_opt[$key]))[0])
														{
															// unset($course_opt[$key]);
														}
													}
													else
													{
														if(isset($surc->surc_lec_date))
														{
															if(trim(explode('(', $surc->surc_lec_date)[0]) == explode(' (', preg_replace('/\r\n|\n/',"",$course_opt[$key]))[0])
															{
																// unset($course_opt[$key]);
															}
														}
														
													}
													$value = preg_replace('/\r\n|\n/',"",$value);
												}
												
												echo form_dropdown('surc_lec',$course_opt,((isset($sp->sp_course) && !empty($sp->sp_course))?str_replace('_', '-', $sp->sp_course):''),'class="form-control" id="fd_lec" onchange="changeCourseEvent($(this))"') ?>
												
											</div>
											<div class="col-md-12">
											<?php  
											if(is_array($courseAry) && !empty($surc->surc_lec_date) && isset($surc->surc_lec_date))
											{
												if($courseAry[0] . '(' .$courseAry[1].')' != $surc->surc_lec_date)
												{
													echo '<span class="text-danger">注意：所選的時段與預設時段不一樣，如需更改為預設請選擇『無』，否請忽略該訊息。</span>';
												}
											}
											elseif (!is_array($courseAry) && !empty($surc->surc_lec_date)  && isset($surc->surc_lec_date)) {
												echo '<span class="text-danger">注意：所選的時段與預設時段不一樣，如需更改為預設請選擇『無』，否請忽略該訊息。</span>';
											}
											
											?>
											</div>	
											<div class="<?php echo (($courseAry == '' && empty($surc->surc_lec_time))?'hidden':''); ?>"id="fd_lec_wrap">
                                            <div class="form-group col-md-12">
												<label>講習時數</label>
												<?php
													if(is_array($courseAry) && empty($surc->surc_lec_time)) {
														echo form_input('surc_lec_time',$courseAry[2].'-'.$courseAry[3], 'class="form-control"');
													}
													elseif (is_array($courseAry) && !empty($surc->surc_lec_time)) {
														if($courseAry[0] . '(' .$courseAry[1].')' != $surc->surc_lec_date)
														{
															echo form_input('surc_lec_time',$surc->surc_lec_time, 'class="form-control"');
														}
														else
														{
															echo form_input('surc_lec_time',$courseAry[2].'-'.$courseAry[3], 'class="form-control"');
														}
														
													}
													elseif (!is_array($courseAry) && !empty($surc->surc_lec_time)) {
														echo form_input('surc_lec_time',$surc->surc_lec_time, 'class="form-control"');
													}
													else 
													{
																echo form_input('surc_lec_time','', 'class="form-control"');
															
													}
												?>
											</div>
                                            <div class="form-group col-md-12">
												<label>講習時間</label>
												<?php
													// $options1 = array(
													//     '2020-04-10'=> '04/10',
													//     '2020-05-15'=> '05/15',
													//     '2020-06-15'=> '06/15',
													//     '2020-11-27'=> '11/27',
													//     '2020-12-02'=> '12/2',
													//     '2020-12-04'=> '12/4',
													//     '2020-12-11'=> '12/11',
													//     '2020-12-18'=> '12/18',
													// );
													// if(isset($sp)) {
													//     echo form_dropdown('fd_lec_date',$cdate,$sp->fd_lec_date, 'class="form-control"');
													// }
													//      else {
													//          echo form_dropdown('fd_lec_date',$cdate,'', 'class="form-control"');
													// }
													if(is_array($courseAry) && empty($surc->surc_lec_time) ) {
														echo form_input('surc_lec_date',$courseAry[0] . '(' .$courseAry[1].')', 'class="form-control"');
													}
													elseif (is_array($courseAry) && !empty($surc->surc_lec_time)) {
														if($courseAry[0] . '(' .$courseAry[1].')' != $surc->surc_lec_date)
														{
															echo form_input('surc_lec_date',$surc->surc_lec_date, 'class="form-control"');
														}
														else
														{
															echo form_input('surc_lec_date',$courseAry[0] . '(' .$courseAry[1].')', 'class="form-control"');
														}
														
													}
													elseif (!is_array($courseAry) && !empty($surc->surc_lec_time)) {
														echo form_input('surc_lec_date',$surc->surc_lec_date, 'class="form-control"');
													}
													else 
													{
														echo form_input('surc_lec_date','', 'class="form-control"');
															
													}
												?>
											</div>
                                            <div class="form-group col-md-12">
												<label>講習地點</label>
												<?php
													if(is_array($courseAry) && empty($surc->surc_lec_time) ) {
														echo form_input('surc_lec_address',$courseAry[4], 'class="form-control"');
													}
													elseif (is_array($courseAry) && !empty($surc->surc_lec_time)) {
														if($courseAry[0] . '(' .$courseAry[1].')' != $surc->surc_lec_date)
														{
															echo form_input('surc_lec_address',$surc->surc_lec_place . '(' . $surc->surc_lec_address . ')', 'class="form-control"');
														}
														else
														{
															echo form_input('surc_lec_address',$courseAry[4], 'class="form-control"');
														}
														
													}
													elseif (!is_array($courseAry) && !empty($surc->surc_lec_time)) {
														echo form_input('surc_lec_address',$surc->surc_lec_place . '(' . $surc->surc_lec_address . ')', 'class="form-control"');
													}
													else 
													{
														echo form_input('surc_lec_address','', 'class="form-control"');
															
													}

												?>
											</div>
											<span id="info-txt" class="text-danger hidden">該個案已有重複之講習，請選擇其他。</span>
											<div class="form-group col-md-12">
												<label>特殊情況</label>
												<div class="checkbox">
													<label>
														<input type="checkbox" disabled <?php echo ((isset($surc->surc_prison) && !empty($surc->surc_prison))?'checked':''); ?> > 在監(出監後自動找毒防中心)
													</label>
													<br>
													<label>
														<input type="checkbox" name="special_situ" <?php echo ((isset($surc->surc_other_lec) && !empty($surc->surc_other_lec))?'checked':''); ?> > 疫情另通知
													</label>
												</div>
											</div>
											</div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-body" style="HEIGHT: 770px; BACKGROUND-COLOR: #FFFFFF; overflow-y:scroll;">
									<p class="text-right" style="max-width: 890px;">
									郵寄 以稿代簽 限制開放 第二層決行 檔號：<?php echo date('Y')-1911 ?>/07270399 保存年限：3年<strong></strong>
									</p>
									<p class="text-right"style="max-width: 890px;">
                                        校對： &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                        監印： &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                        發文： &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                    </p>
                                    <p>
                                        案件列管編號：<?php echo $surc->surc_no?>  
                                        原處分管制編號：<?php echo $surc->surc_listed_no; ?>
                                    </p>
                                    <p>
										正本：
                                        <?php 
                                        
                                            if(null != $surc && count(explode(",", $surc->surc_address)) > 1)
                                            {
                                                $fd_name = explode(",", $surc->surc_target)[0];
                                                $fd_addr = explode(",", $surc->surc_address)[0];
                                                $fd_zip = explode(",", $surc->surc_zipcode)[0];
                                                // echo '<mark>'. $fd_name.'</mark> 君 <br> <span style="margin-left: 3em;">'. $fd_zip . ' ' . $fd_addr.'</span>';
                                                // echo '<br/>';
                                                $fd_name1 = explode(",", $surc->surc_target)[1];
                                                $fd_addr1 = explode(",", $$surc->surc_address)[1];
                                                $fd_zip1 = explode(",", $surc->surc_zipcode)[1];
                                                echo '<mark >'. $fd_name.'</mark>('.$fd_name1.'法定代理人) 君 <br> <span style="margin-left: 3em;">'. $fd_zip1 . ' ' . $fd_addr1.'</span>';
                                                if($surc->surc_live_state == 2)
                                                {
                                                    echo '（指定送達現住地）';
                                                }
                                            }
                                            else
                                            {
                                                echo '<mark>'. $surc->surc_target.'</mark> 君 <br> <span style="margin-left: 3em;">'. ((!empty($surc->surc_prison))?explode(':', $surc->surc_prison)[0] .'('.explode(':', $surc->surc_prison)[1].')' :$surc->surc_zipcode . ' ' . $surc->surc_address) .'</span>';
                                                if($surc->surc_live_state == 2)
                                                {
                                                    echo '（指定送達現住地）';
                                                }
                                            }
                                        ?>
                                    <p>
                                        副本：我是測試系統刑事警察大隊毒品查緝中心 <br>  臺北市政府毒品危害防治中心</strong>
                                    </p>
                                    <table width="890" cellspacing="0" cellpadding="0" border="1">
                                        <tbody>
                                            <tr>
                                                <td colspan="8" width="890">
                                                    <p align="center">
                                                        我是測試系統違反毒品危害防制條例案件（怠金）處分書(稿)
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="890">
													<?php 
                                                         if($surc->surc_date != '0000-00-00' && isset($surc->surc_date))
                                                         {
                                                    ?>
                                                        <p>
															發文日期及字號：<?php echo (date('Y')-1911).'年'?> <?php echo date('m', strtotime($surc->surc_date))*1 .'月' ?> <?php echo date('d', strtotime($surc->surc_date))*1 .'日' ?>北市警刑毒緝字第<?php echo $surc->surc_send_num ; ?>號
														</p>
                                                    <?php }else{
														if($sp->sp_send_date !=  '0000-00-00' && isset($sp->sp_send_date))
														{ ?>
														<p>
															發文日期字號：<?php echo ((int)substr($sp->sp_send_date, 0, 4)- 1911) . '年' . (int)substr($sp->sp_send_date, 5, 2)*1 . '月' . (int)substr($sp->sp_send_date, 8, 2)*1 . '日'; ?>北市警刑毒緝字第<?php echo $surc->surc_send_num ; ?>號
														</p>
													   <?php }
													   		else{
														?>
														<p>
															發文日期字號：<strong>民國<?php echo (date('Y')-1911); ?>年<?php echo date('m')*1; ?>月  日</strong>北市警刑毒緝字第<?php echo $surc->surc_send_num ; ?>號
														</p>
														<?php } ?>
                                                    <?php } ?>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="890">
                                                    <p>
                                                        依據：臺北市政府衛生局<?php echo (date('Y', strtotime($surc->surc_basenumdate))-1911) .'年'?> <?php echo date('m', strtotime($surc->surc_basenumdate))*1 .'月' . date('d', strtotime($surc->surc_basenumdate))*1 ; ?>日北市衛醫傳防字第<?php echo $surc->surc_basenum?>號
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="128">
                                                    <p align="center">
                                                        受處分人
                                                    </p>
                                                </td>
                                                <td colspan="2" width="122">
                                                    <p align="center">
                                                        姓名
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong><?php echo $surc->surc_name?></strong>
                                                    </p>
                                                </td>
                                                <td width="49">
                                                    <p align="center">
                                                        性別
                                                    </p>
                                                </td>
                                                <td width="50">
                                                    <p>
                                                       <strong> <?php //echo $susp->s_gender?></strong>
                                                    </p>
                                                </td>
                                                <td width="70" valign="top">
                                                    <p>
                                                        出生
                                                    </p>
                                                    <p>
                                                        年月日
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <p>
														<?php if(isset($susp)){ ?>
                                                        <strong>民國<?php echo (date('Y', strtotime($susp->s_birth))-1911) .'年'?>
                                                        <?php echo (date('m', strtotime($susp->s_birth))) .'月'?>
                                                        <?php echo (date('d', strtotime($susp->s_birth))) .'日'?></strong>
														<?php } ?>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="122" valign="top">
                                                    <p>
                                                        身分證
                                                    </p>
                                                    <p>
                                                        統一編號
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong><?php echo $surc->surc_sic;?></strong>
                                                    </p>
                                                </td>
                                                <td colspan="3" width="169" valign="top">
                                                    <p>
                                                        其他足資辨別之
                                                    </p>
                                                    <p>
                                                        特徵及聯絡電話
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <p>
                                                        <strong><?php 
                                                            if(isset($phone)) {
                                                                if(!empty($phone->p_no))
                                                                    echo $phone->p_no;
                                                            }else {}?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">
                                                    <p align="center">
                                                        地址
                                                    </p>
                                                </td>
                                                <td width="70">
                                                    <p>
                                                        現住地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                        <strong><?php echo (isset($surc->surc_prison) && !empty($surc->surc_prison))?explode(':', $surc->surc_prison)[1] .'('.explode(':', $surc->surc_prison)[0].')' : $surc->surc_address/*$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress*/ ?></strong>
                                                        <?php 
                                                            if($surc->surc_live_state == 2)
                                                            {
                                                                echo '（指定送達現住地）';
                                                            }
                                                        ?>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="70">
                                                    <p>
                                                        戶籍地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                        <strong><?php echo $surc->surc_address/*$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress*/ ?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <?php 
                                                // list($year,$month,$day) = explode("-",$susp->s_birth);
                                                // $year_diff = date("Y") - $year;
                                                // $month_diff = date("m") - $month;
                                                // $day_diff  = date("d") - $day;
                                                // if ($day_diff < 0 || $month_diff < 0)
                                                //     $year_diff--;

                                                ?>
                                                <td rowspan="4" width="128">
                                                    <p align="center">
                                                        法　定
                                                    </p>
                                                    <p align="center">
                                                        代理人
                                                    </p>
                                                </td>
                                                <td colspan="2" width="122">
                                                    <p align="center">
                                                        姓名
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong> <?php //echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name)  && !isAudlt($surc->s_birth, $cases->s_date))?$suspfadai->s_fadai_name:''; ?></strong>
                                                    </p>
                                                </td>
                                                <td width="49">
                                                    <p align="center">
                                                        性別
                                                    </p>
                                                </td>
                                                <td width="50">
                                                    <p align="center">
                                                    <strong>  <?php //echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name)  && !isAudlt($surc->s_birth, $cases->s_date))?$suspfadai->s_fadai_gender:''; ?></strong>
                                                    </p>
                                                </td>
                                                <td width="70" valign="top">
                                                    <p>
                                                        出生
                                                    </p>
                                                    <p>
                                                        年月日
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <p>
                                                        <?php //if(isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name)  && !isAudlt($surc->s_birth, $cases->s_date)) {?>
                                                        <!-- <strong>民國<?php //echo (date('Y', strtotime($suspfadai->s_fadai_birth))-1911) .'年'?> -->
                                                        <?php //echo (date('m', strtotime($suspfadai->s_fadai_birth))) .'月'?>
                                                        <?php //echo (date('d', strtotime($suspfadai->s_fadai_birth))) .'日'?></strong>
                                                        <?php //} ?>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="122" valign="top">
                                                    <p>
                                                        身分證
                                                    </p>
                                                    <p>
                                                        統一編號
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong><?php //echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name)  && !isAudlt($surc->s_birth, $cases->s_date))?$suspfadai->s_fadai_ic:''; ?></strong>
                                                    </p>
                                                </td>
                                                <td colspan="3" width="169" valign="top">
                                                    <p>
                                                        其他足資辨別之
                                                    </p>
                                                    <p>
                                                        特徵及聯絡電話
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <strong><?php // echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name)  && !isAudlt($surc->s_birth, $cases->s_date))?$suspfadai->s_fadai_phone:''; ?></strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">
                                                    <p align="center">
                                                        地址
                                                    </p>
                                                </td>
                                                <td width="70">
                                                    <p>
                                                        現住地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>                                                    
                                                        
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="70">
                                                    <p>
                                                        戶籍地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                        <strong><?php //echo (isset($suspfadai->s_fadai_name) && !empty($suspfadai->s_fadai_name)  && !isAudlt($surc->s_birth, $cases->s_date))?$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress:''; ?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        主旨
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p ">
													一、受處分人處罰：怠金新台幣<?php echo number_format($surc->surc_amount); ?>元整。
													</p>
													<p >
													二、仍須參加講習，講習時地詳見詳見下方「毒品講習」欄位。無正當理由仍不參加毒品危害講習時，得連續處以怠金。
													</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        事實
                                                    </p>
                                                </td>
                                                <?php
                                                    /** 轉民國年 */
                                                    function tranfer2RCyear($date)
                                                    {
                                                        $date = str_replace('-', '', $date);
                                                        $rc = ((int)substr($date, 0, 4)) - 1911;
                                                        return (string)$rc . '年' . substr($date, 4, 2) . '月' . substr($date, 6, 2) . '日';
                                                    }
                                                    /** 轉時分 */
                                                    function tranfer2RChour($time)
                                                    {
                                                        if(null != $time)
                                                        {
                                                            $hour = explode(':', $time)[0];
															$min = explode(':', $time)[1];
                                                            return $hour*1 . '時'.  $min*1 . '分';
                                                        }
                                                        else
                                                        {
                                                            return '';
                                                        }
                                                        
                                                    }
                                                    $s_date = tranfer2RCyear($surc->surc_olec_date);
                                                ?>
                                                <td colspan="7" width="762" height="100px" >
													<p>
													緣受處分人<?php echo $surc->surc_target; ?>涉毒品危害事件，經本局裁處而負有受毒品危害講習義務6小時確定，合先敘明。本案經通知後受處分人應於<?php echo $s_date; ?>至ＯＯＯ參加毒品危害講習，惟無正當理由不參加講習違反上揭義務，經衛生局通知本局依行政執行法第三十條規定，處以怠金。
													</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        理由及
                                                    </p>
                                                    <p align="center">
                                                        法令依據
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
													<?php echo '■'; ?>
                                                        一、毒品危害事件統一裁罰基準及講習辦法第九條第二項規定，應受講習人無正當理由不參加毒品危害講習者，依行政執行法規定處以怠金。
                                                    </p>
                                                    <p >
                                                        <?php echo '■'; ?>
                                                        二、依據行政執行法第三十條第一項規定，依法令或本於法令之行政處分，負有行為義務而不為，其行為不能由他人代為履行者，依其情節輕重處新台幣五千元以上三十萬元以下怠金。
                                                    </p>
													<p >
                                                        <?php echo '■'; ?>
                                                        三、依據行政執行法第三十一條第一項規定，經依前條規定處以怠金，仍不履行其義務者，執行機關得連續處以怠金。
                                                    </p>
                                                </td>
                                            </tr>
											<!-- 繳款欄 -->
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        繳納期限<br/>及方式
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p >
                                                    <?php 
                                                        if($surc->surc_findate != '0000-00-00')
                                                        {
                                                    ?>
                                                        一、怠金限於<strong>民國<?php echo (date('Y', strtotime($surc->surc_findate))-1911) .'年'?><?php echo (date('m', strtotime($surc->surc_findate))) .'月'?><?php echo (date('d', strtotime($surc->surc_findate))) .'日'?></strong>前選擇下列方式之一繳款，逾期不繳納依法移送行政執行處執行：
                                                    <?php }else{?>
                                                        一、怠金限於<strong>民國Ｏ年Ｏ月Ｏ日</strong>前選擇下列方式之一繳款，逾期不繳納依法移送行政執行處執行：
                                                    <?php } ?>
                                                    </p>
                                                    <p >
                                                        （一）以自動化設備（ATM、網路ATM、網路銀行）匯款至「虛擬帳號」（限本處分書）。
                                                    </p>
                                                    <p >
                                                        （二）至金融機構臨櫃繳款至「臨櫃帳戶」。
                                                    </p>
                                                    <p >
                                                        二、自動化設備匯款方式：（一）選擇【繳費】服務 （二）輸入轉入銀行代號：<strong>012</strong>
														<?php
															$main_sendno = $sp->sp_send_no;
															$code =  '21720'. substr($main_sendno, 0, 4). (($sort != 'A')?($sort*1 -1):9). substr($main_sendno, 5);
															$code1 = preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY);
															$code2=0;
															for($i=0;$i<count($code1);$i++){
																if($i==0||$i==3||$i==6||$i==9||$i==12||$i==15) $code2=$code2+$code1[$i]*3;
																if($i==1||$i==4||$i==7||$i==10||$i==13) $code2=$code2+($code1[$i]*7);
																if($i==2||$i==5||$i==8||$i==11||$i==14) $code2=$code2+($code1[$i]*1);
															}
															$code3 = $code2%10;
															$code4 = 10-$code3;
															// $fin = $code.$code4;
															$fin = $code.((count(preg_split('//', $code4, -1, PREG_SPLIT_NO_EMPTY)) > 1)?preg_split('//', $code4, -1, PREG_SPLIT_NO_EMPTY)[count(preg_split('//', $code4, -1, PREG_SPLIT_NO_EMPTY))-1]:$code4);
														?>
                                                        （三）輸入本案虛擬帳號：<strong><?php echo (isset($surc->surc_BVC))?$surc->surc_BVC:$fin;?></strong>。（匯款手續費由繳款人負擔）
                                                    </p>
                                                    <p >
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong> 16112470361021</strong><strong>，</strong>戶名:<strong>我是測試系統刑事警察大隊</strong>。
                                                    </p>
													<p ">
                                                        四、匯款繳納時，請於匯款單上之<strong>匯款人欄</strong> 填寫受處分人姓名<strong>「<?php echo $surc->surc_name ?>」</strong>，並應要求於<strong>備註(附言)欄</strong>填入案件列管編號<strong>「<?php echo $surc->surc_no; ?>」</strong>及身分證統一編號<strong>「<?php echo $surc->surc_sic; ?>」</strong>，俾利辦理銷案。
                                                    </p>
                                                </td>
                                            </tr>
											<!-- 講習欄 -->
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        <strong>毒品講習</strong>
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
                                                        一、<strong>可選擇參與實體講習或線上課程（線上課程每年可參與三次，惟應以不同套裝課程申請認定）。</strong>
                                                    </p>
                                                    <p>
                                                        <strong>二、講習時間：</strong>
                                                        <?php 
                                                        if(isset($surc->surc_prison) && !empty($surc->surc_prison))
                                                        {
                                                        ?>
                                                            <strong class="text-primary">請受處分人出監後，主動聯繫臺北市政府毒品危害防制中心。</strong>
                                                        <?php } else {
                                                            if(isset($surc->surc_lec_date) && !empty($surc->surc_lec_date) && $surc->surc_other_lec != 1){
                                                                
                                                                $md_str = explode("(", $surc->surc_lec_date)[0];
                                                                $week_str = explode("(", $surc->surc_lec_date)[1];

                                                    			$lec_time = tranfer2RChour(explode('-', $surc->surc_lec_time)[0]);
                                                                ?>                                                            
                                                            <strong class="text-primary"><?php echo "民國" . (date("Y")-1911) . "年" . (explode("/", $md_str)[0]) . "月" . (explode("/", $md_str)[1]) . "日"  . $lec_time . '（請攜帶有相片之證件報到，逾時將無法入場），地點：'.$surc->surc_lec_place .'(' . $surc->surc_lec_address . ')'; ?>，請配合防疫相關措施。</strong>
                                                            <?php }else{?>
                                                                <strong class="text-primary">由臺北市政府毒品危害防制中心另行通知。</strong>
                                                            <?php } ?>
                                                        <?php }?>
                                                        <!-- <strong>民國<?php //echo (date('Y', strtotime($sp->fd_lec_date))-1911) .'年'?><?php //echo (date('m', strtotime($sp->fd_lec_date))) .'月'?>
                                                        <?php //echo (date('d', strtotime($sp->fd_lec_date))) .'日'?></strong>
                                                        <strong><?php //echo substr($sp->fd_lec_time,0,5) ?>分（請攜帶有相片之證件報到，逾時將無法入場），地點：<?php //echo $sp->fd_lec_place ?></strong> -->
                                                        有正當理由無法參加或要選擇線上課程者，請至下列網址：https://nodrug.gov.taipei下載異動申請表或依指示完成線上課程。講習相關問題，請逕向臺北市政府毒品危害防制中心洽詢，電話(02)2375-4068。
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        注意事項
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p >
                                                        一、不服本處分者，義務人或利害關係人對執行命令、執行方法、應遵守之程序或其他侵害利益之情事，得於執行程序終結前，向執行機關聲明異議。執行機關認其聲明異議有理由者，應即停止執行，並撤銷或更正之；認其無理由者，應於十日內加具意見，送直接上級主管機關於三十日內決定之。
                                                    </p>
                                                    <p>
                                                        二、本件處分書承辦人：<?php echo $surc->surc_empno; ?>，聯絡電話：(02）2393-2397。
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p>
                                        承辦單位 (2539) 核稿 決行
                                    </p>            
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div>
                    </form>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <script>
            $(".rcdate").change(function(){
                let strlen = $(this).val().length;
                if(strlen < 7 || strlen < "7")
                {
                    if(strlen != 0)
                        $(this).val("0"+$(this).val());
                }
                else
                {
                    return false;
                }
                    
            });
            $(".rcdate").keypress(function(){
                if($(this).val().length >= 7)
                    return false;
            }); 
            $("#next").click(function (){
                $("#nextvalue").val('<?php echo $next ?>');
               // alert($("#nextvalue").val());
                $("#updateready").submit();
            });
            $("#prev").click(function (){
                $("#prevvalue").val('<?php echo $prev ?>');
                //alert($("#prevvalue").val());
                $("#updateready").submit();
            });

			$("#seldate").val("<?php echo (isset($fd->fd_times) && $fd->fd_times != 0)?$fd->fd_times:-1; ?>").change();

			$("input[name='ad']").bind('change', function(){
				if($(this).val().indexOf('1') > -1)
				{
					$("input[name='fd_address']").val($("#ori_rpaddress").val());
					listzipcode($("input[name='fd_address']"), $("input[name='fd_zipcode']"));
				}
				else
				{
					$("input[name='fd_address']").val($("#s_rpaddress").val());
					listzipcode($("input[name='fd_address']"), $("input[name='fd_zipcode']"));
				}
			});

			$("#e_name_other").bind('change', function(){
				$("input[name='e_name']").val($(this).val() + '-' + '其他');
			});
			$("#drugListTable tbody tr").bind('click', function(){
				$("#drugListTable tbody tr").removeClass('selected');
				$(this).toggleClass('selected');

				$("#drugObj").text($("#drugListTable tbody tr.selected td:eq(0)").text() + $("#drugListTable tbody tr.selected td:eq(1)").text());
				$("#drug").val($("#drugListTable tbody tr.selected td:eq(2)").text().replace(/\r\n|\n|\r|\t|\s/g,"").replace(/,|、/gi, ";"));
				
			});

            
            
            
            $("#zipcode").twzipcode({
            "zipcodeIntoDistrict": true,
            "css": ["city form-control", "town form-control"],
            "countyName": "city", // 指定城市 select name
            "districtName": "town" // 指定地區 select name
            });         
            $("#zipcode2").twzipcode({
            "zipcodeIntoDistrict": true,
            "css": ["city form-control", "town form-control"],
            "countyName": "city", // 指定城市 select name
            "districtName": "town" // 指定地區 select name
            });  
            $(function() {  
               $('a.media').media({width:1200, height:700});  

               $("textarea[name='fd_fact_text1']").val($("#fd_fact_text_div").html());
            });  

            /** 轉民國年 */
            function tranfer2ADyear(date)
            {
                date = date.replace('-','');
                date = (parseInt(date.substr(0, 2)) - 1911) + "年" + date.substr(4, 2) + "月" + date.substr(6, 2) + "日";
                 return date;                
            }   
			
            function copyAddress(obj)
            {
                if(obj != 'undefined' && obj.val() != '')
                {
                    $("#now").attr('checked', true);
                    // $("#now").val(obj.val().split(':')[1] + ` (${obj.val().split(':')[0]})`);
                    $("#s_rpaddress").val(obj.val().split(':')[1] + ` (${obj.val().split(':')[0]})`);
                }
                else
                {
                    $("#now").attr('checked', true);
                    $("#s_rpaddress").val('');

                }
				$("input[name='fd_address']").val($("#s_rpaddress").val());
				listzipcode($("input[name='fd_address']"), $("input[name='fd_zipcode']"));
            } 
            function changeCourseEvent(obj)
            {
                $val = obj.val().replace(/\r\n|\n/g,"");
                $data = $val.split('-');
                if($val)
                {
					$("#fd_lec_wrap").removeClass('hidden');
                    $("input[name=surc_lec_time]").val($data[2]+'-'+$data[3]);
                    $("input[name=surc_lec_date]").val($data[0]+'('+$data[1]+')');
                    $("input[name=surc_lec_address]").val($data[4]);
                }
                else
                {
					$("#fd_lec_wrap").addClass('hidden');
                    $("input[name=surc_lec_time]").val('');
                    $("input[name=surc_lec_date]").val('');
                    $("input[name=surc_lec_address]").val('');
                }
				checkDupCourse();
            }
			function checkDupCourse(){
				if($("input[name='surc_lec_date']").val())
				{
					let formdata = new FormData();
					formdata.append('surc_lec_date', $("input[name='surc_lec_date']").val());
					formdata.append('surc_lec_time', $("input[name='surc_lec_time']").val());
					formdata.append('surc_lec_address', $("input[name='surc_lec_address']").val());
					formdata.append('surc_sic', $("input[name='surc_sic']").val());
					$.ajax({
						type: 'POST',
						url: '<?php echo base_url("surcharges/getCase_courseDup"); ?>',
						data: formdata,
						dataType: 'json',
						processData: false,
						contentType: false,
						cache: false,
						complete: function (resp) {
							
							if(resp.responseJSON.data > 1)
							{
								if(!$("input[name=surc_lec_time]").val())
								{
									$("input[name=surc_lec_time]").val('');
									$("input[name=surc_lec_date]").val('');
									$("input[name=surc_lec_address]").val('');
								}
								
								$('#info-txt').removeClass('hidden');
							}							
							else
								$('#info-txt').addClass('hidden');
						}
					});
				}
				
            }
			function listzipcode(obj, wrap) {
                $wrap = wrap;
                let formdata = new FormData();
                formdata.append('addr', obj.val());
            	$.ajax({
            		type: 'POST',
            		url: '<?php echo base_url("disciplinary_c/getZipCode") ?>',
            		dataType: 'json',
                    // data: {'addr': obj.val()},
                    data: formdata,
            		processData: false,
            		contentType: false,
            		cache: false,
            		complete: function (resp) {
                        // console.log(resp)						
                        $($wrap).val(resp.responseJSON.data);
						
            		}
            	});
				
            }
			
        </script>

    </body>
</html>
