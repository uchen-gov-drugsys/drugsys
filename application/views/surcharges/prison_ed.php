        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="saveprison"></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                <?php echo form_open_multipart('Surcharges/savePrison','id="saveprison"') ?>          
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body"><br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <table class="table" id="car">
                                                    <tbody  class="form-group">
                                                        <tr>
                                                            <td>
                                                                <label>當時是否在監</label>
                                                                <select name="surc_inprison" class="form-control">
                                                                    <option value="是">是</option>
                                                                    <option selected value="否">否</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <label>監所情形</label>
                                                                <?php echo form_dropdown('surc_prison',$opt ,'', 'class="form-control" id="sel"')?>                                                            
                                                                <?php echo form_hidden('surc_no',$surc_no);?>
                                                            </td>
                                                            <td>
                                                                <label>監所文件</label>
                                                                <?php echo form_upload('surc_prison_doc','', 'class="form-control" id="sel"')?>                                                            
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
            $(document).ready(function(){
                var i=1;  
                $( "#saveprison" ).validate({
                    rules: {
                        "v_license_no[]": {
                            required: true,
                        },
                    },
                    messages: {
                        "v_license_no[]": {
                            required: "此欄位不得為空",
                        },
                    }
                });        

            });
            
        </script>
    </body>
</html>
