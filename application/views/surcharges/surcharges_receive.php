        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><input type="submit" value="確認修改" class="btn btn-default" style="padding:0px 0px;" form="newcases" id="yes"></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                
                <div class="container-fluid">
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							<!-- <a><input type="submit" value="新增並上傳" class="btn btn-success" form="newcases" id="yes"></a> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="panel panel-info">
                                <div class="panel-heading">
									<h4 class="panel-title">新增上傳作業</h4>
                                </div>                                <!-- /.panel-heading -->
                                <div class="panel-body">									
									<div class="row">
										<div class="col-md-12">
											<!-- <form action=<?php //echo base_url("Surcharges/receiveupdate")?> id="addcases"  enctype="multipart/form-data" method="post" accept-charset="utf-8" > -->
												<form  id="sendForm">
													<div class="form-group">
														<label>
															上傳毒防中心公文&掃描檔清冊
														</label>
														<input class="form-controls" name='other_doc' type="file" accept=".pdf,.doc,.docx">
														<span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制100MB</small> </span>
													</div>
													<div class="form-group">
														<label >
															excel清冊
														</label>
														<input class="form-controls" name="excel" type="file" accept=".xls,.xlsx">
														<span class="text-danger"><small>限上傳xlsx,xls類型之檔案且檔案大小限制5MB</small> </span>
													</div>
													<div class="form-group">
														<label>
															怠金依據文號 <span class="text-danger">*</span>
														</label>
														<div class="input-group">
															<span class="input-group-addon" id="basic-addon3">北市衛醫傳防字第</span>
															<input class="form-control" name="surc_basenum" type="text" placeholder="請輸入文號">
															<span class="input-group-addon">號函</span>
														</div>
													</div>
													<div class="form-group">
														<label>
															怠金依據日期 <span class="text-danger">*</span>
														</label>
														<input class="form-control rcdate" name="surc_basenumdate" type="text">
														<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
													</div>
													<input type="button" value="新增並上傳" class="btn btn-success btn-block" id="yes">
												</form>
										</div>
									</div>
                                </div>
                                <!-- /.panel-body -->
                                <div class="modal fade" id="new_s" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="othernewLabel">怠金文號</h5>
                                        北市衛醫傳防字第<input class="form-control" type="text">號函
                                      </div>
                                      <div class="modal-footer">
                                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button class="btn btn-default" onclick="location.href='surcharges_收文.html'">確認文號</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
            <!-- /#page-wrapper -->
            </div>
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
            $(document).ready(function() {
				$(".rcdate").change(function(){
					let strlen = $(this).val().length;
					if(strlen < 7 || strlen < "7")
					{
						if(strlen != 0)
							$(this).val("0"+$(this).val());
					}
					else
					{
						return false;
					}
						
				});
				$(".rcdate").keypress(function(){
					if($(this).val().length >= 7)
						return false;
				});

				$("#yes").click(function(){
					sendEvent()
				});
				
            //     $('#fadai').hide();//用ifelse判斷是否大於20歲
            //     //if($('#s_birth').val())
            //     var age = 100;
            //     $("#s_birth").change(function(){
            //         var today = new Date();
            //         var birthdate = new Date($('#s_birth').val());
            //         age = today.getFullYear() - birthdate.getFullYear();
            //         var m = today.getMonth() - birthdate.getMonth();
            //         if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
            //             age--;
            //         }
            //     });                
            // $( "#addcases" ).validate({
            //         rules: {
            //             s_name: {
            //                 required: true,
            //                 minlength: 2
            //             },
            //             s_ic: {
            //                 required: true,
            //                 minlength: 9
            //             },
            //             s_birth: {
            //                 required: true,
            //             },
            //             s_dpdistrict: {
            //                 required: true,
            //             },
            //             s_dpaddress: {
            //                 required: true,
            //             },
            //         },
            //         messages: {
            //             s_name: {
            //                 required: "此欄位不得為空",
            //                 minlength: "請完整輸入",
            //             },
            //             s_ic: {
            //                 required: "此欄位不得為空",
            //                 minlength: "請完整輸入",
            //             },
            //             s_dpdistrict: {
            //                 required: "此欄位不得為空",
            //             },
            //             s_dpaddress: {
            //                 required: "此欄位不得為空",
            //             },
            //         }
            // });  
            //     $("#yes").click(function (){
            //         $("#s_status").val('1');
            //         if(age < 20){
            //             $('#fadai').show();
            //             if($('#fadaival').val()!=""){
            //                 $("#addcases").submit();
            //             }else{alert('受處分人未滿20歲，請輸入法代人資料');}
            //         }else{
            //             $("#addcases").submit();
            //         }
            //     });
            //     $("#no").click(function (){
            //         $("#s_status").val('0');
            //         if(age < 20){
            //             $('#fadai').show();
            //             if($('#fadaival').val()!=""){
            //                 $("#addcases").submit();
            //             }else{alert('受處分人未滿20歲，請輸入法代人資料');}
            //         }else{
            //             $("#addcases").submit();
            //         }
            //     });
            //     $("#drug").click(function (){
            //         $("#s_status").val('88');
            //         $("#link").val('disciplinary_c/editdrug/'+$("#c_num").val());
            //         if(age < 20){
            //             $('#fadai').show();
            //             if($('#fadaival').val()!=""){
            //                 $("#addcases").submit();
            //             }else{alert('受處分人未滿20歲，請輸入法代人資料');}
            //         }else{
            //             $("#addcases").submit();
            //         }
            //     });
            //     $("#susp").click(function (){
            //         $("#s_status").val('99');
            //         //alert($("#link").val());
            //         if(age < 20){
            //             $('#fadai').show();
            //             if($('#fadaival').val()!=""){
            //                 $("#addcases").submit();
            //             }else{alert('受處分人未滿20歲，請輸入法代人資料');}
            //         }else{
            //             $("#addcases").submit();
            //         }
            //     });
        });  
		function sendEvent(){
			$.ajax({            
				type: 'POST',
				url: '<?php echo base_url("surcharges/receiveupdate")?>',
				data: new FormData($("#sendForm")[0]),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData:false,
				error:function(){
					
				},
				success: function(resp){
					if(resp.msg.indexOf('OK') > -1)
					{
						location.href = `../${resp.directurl}`;
					}
					else
					{
						alert(resp.msg);
						location.href = `${resp.directurl}`;
					}
					
					// console.log(resp)
				},
				complete:function(resp){
					// location.href = `../${resp.responseText}`;
				}
			});
		}
		
            // $("#zipcode").twzipcode({
            // "zipcodeIntoDistrict": true,
            //     "css": ["city form-control", "district form-control"],
            //     'countyName'   : 's_dpcounty',   // 預設值為 county
            //     'districtName' : 's_dpdistrict', // 預設值為 district
            //     'zipcodeName'  : 's_dpzipcode' // 預設值為 zipcode
            // });         
            // $("#zipcode2").twzipcode({
            // "zipcodeIntoDistrict": true,
            //     "css": ["city form-control", "district form-control"],
            //     'countyName'   : 's_rpcounty',   // 預設值為 county
            //     'districtName' : 's_rpdistrict', // 預設值為 district
            //     'zipcodeName'  : 's_rpzipcode' // 預設值為 zipcode
            // });  
            // $("#zipcode3").twzipcode({
            // "zipcodeIntoDistrict": true,
            // "css": ["city form-control", "town form-control"],
            //     'countyName'   : 's_fadai_county',   // 預設值為 county
            //     'districtName' : 's_fadai_district', // 預設值為 district
            //     'zipcodeName'  : 's_fadai_zipcode' // 預設值為 zipcode
            // }); 
 
                // $('#receiverchange').on('click', function() {
                //    // $("#zipcode2").empty();
                //     var city = $(zipcode).twzipcode('get', 'city');
                //     var zipc = $(zipcode).twzipcode('get', 'zipcode');
                //     //console.log(zipc);   // 縣市
                //     if( zipc == ""){
                //         alert("請務必選擇鄉鎮市區");return false;
                //     } 
                //     $("#zipcode2").twzipcode('set', zipc[0]);
                //     var sno=$("#s_no").val();

                //     // 移除空格確定是否空值
                //     if (!$.trim($("#s_no").val())) {
                //         if( $("#s_no").val()==""){
                //             $("#s_no").focus();
                //             alert("請務必填入路門牌");return false;
                //         }     
                //     }   
                //     $("#s_no2").val(sno);
                // }); 
            
                       
        </script>
