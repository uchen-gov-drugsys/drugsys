        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                    <!-- <li><button id='yes' class="btn btn-default"style="padding:0px 0px;" >批次發文</button></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <button id='yes' class="btn btn-info">批次發文</button>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo form_open_multipart('surcharges/updatestatus','id="sp_checkbox"') ?>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <span>列表清單</span> 
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''>
                            </div>
                                 
                           </div>
                            <!-- /.panel -->
                        </div>
                            </form>
                        <!-- /.col-lg-12 -->
                    </div>
                   </form> <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': false
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
            //   'order': [[1, 'asc']]
            dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
           var courceTable = $('#courceTable').DataTable({
                dom: 'f',
                "destroy":true,	
                "ordering":false,
                fixedHeader: true,
                "language": {
                            "processing": "資料載入中...",
                            "lengthMenu": "每頁顯示 _MENU_ 筆",
                            "zeroRecords": "資料庫中未有相關資料。",
                            "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                            "infoEmpty": "資料庫中未有相關資料。",
                            "search": "搜尋:",
                            "paginate": {
                                "first": "第一頁",
                                "last": "最後一頁",
                                "next": "下一頁",
                                "previous": "上一頁"
                            }
                        }

           });
           listEvent();
           $("#importBT").click(function(){
               if(!$('input[name=upload_file]'))
                    return false;

                $('#loading').removeClass('hidden');
                importEvent();
            });
            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows(0));
                //console.log(allrows);
                alert($('#s_cnum1').val());
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });

           
    });
    function listEvent(){
        courceTable = $('#courceTable').DataTable({	
            "destroy": true,
            "bProcessing": true, //顯示『資料載入中』		
            "bAutoWidth": true,					
            "sAjaxSource": "<?php echo base_url(); ?>Disciplinary_c/getCourses", // API
            "aoColumns": [
                { "mData": "c_date"},
                { "mData": "c_week"},
                { "mData": "c_time"},
                { "mData": "c_place"}
            ],    
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $(nRow).attr("data-id", aData.ser);
                // $("td:first", nRow).html(iDisplayIndex + 1); //自動序號

                return nRow;
            },
            "language": {
                "processing": "資料載入中...",
                "lengthMenu": "每頁顯示 _MENU_ 筆",
                "zeroRecords": "資料庫中未有相關資料。",
                "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                "infoEmpty": "資料庫中未有相關資料。",
                "search": "搜尋:",
                "paginate": {
                    "first": "第一頁",
                    "last": "最後一頁",
                    "next": "下一頁",
                    "previous": "上一頁"
                }
            },  
            
        });
    }
    function importEvent(){

        $.ajax({            
            type: 'POST',
            url: '../PhpspreadsheetController/courses_import',
            data: new FormData($("#importForm")[0]),
            // dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
                beforeSend:function(){
                $("#loadingTxt").text("資料匯入中...請稍候...請勿關掉或跳出畫面喔！電腦會壞掉!");
            },
            error:function(){
                $('#loading').addClass('hidden');
                $("#uploadtxt").text("資料匯入出現錯誤，請重新執行操作！");
            },
            success: function(resp){
                $('#loading').addClass('hidden');
                if(resp == 'ok'){
                    $("#uploadtxt").text("資料匯入成功！");
                    $("#importForm")[0].reset();
                }else if(resp == 'error'){
                    $("#uploadtxt").text("資料匯入失敗，請重新執行操作！");
                }
            },
            complete:function(){
                $("#uploadtxt").text("資料匯入成功！");
                $("#importForm")[0].reset();
                listEvent();
            }
        });
    }
    function emptyEvent(){
        $.ajax({            
            type: 'GET',
            url: '../Disciplinary_c/emptyCourses',
            data: [],
            // dataType: 'json',
            contentType: false,
            cache: false,
            success: function(resp){
                if(resp == 'ok'){
                    swal({
                          title: "成功!",
                          text: "資料已清空",
                          icon: "success"
                      });
                }else if(resp == 'error'){
                    swal({
                          title: "警告!",
                          text: "資料庫作業錯誤",
                          icon: "warning"
                      });
                }
            },
            complete:function(){
                listEvent();
            }
        });
    }
    </script>
