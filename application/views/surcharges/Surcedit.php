        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><a href="#"><?php echo $title?></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" data-toggle="modal" data-target="#filedownload" data-whatever="文件檢視">文件檢視</button></a></li>
                    <?php if($prev != NULL){?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='prev'>上一個</button></a></li>
                    <?php }else{ ?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">第一筆</button></a></li>
                    <?php } ?>
                    <?php if($next != NULL){?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='next'>下一個</button></a></li>
                    <?php }else{ ?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">最後一筆</button></a></li>
                    <?php } ?>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" data-toggle="modal" data-target="#surc_date" data-whatever="批次輸入裁處日期">批次輸入裁處日期</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" form="updateSucrEd">存儲</button></a></li>
                </ul>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href=<?php echo base_url("login/logout") ?>><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
                <!-- /.navbar-static-side -->
            <div id="page-wrapper" style="margin-left:20px;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="page-header"></p>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">
                            <?php echo form_open_multipart('Surcharges/updateSucrEd','id="updateSucrEd"') ?>     
                            <input id="nextvalue" type="hidden" name="next" value=""> 
                            <input id="prevvalue" type="hidden" name="prev" value=""> 
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td>怠金列管編號</td>
                                                <td width='180'>裁處文號</td>
                                                <td width='120'>裁處文號(支號)</td>
                                                <td width='120'>怠金金額</td>
                                                <td width='120'>繳納期限</td>
                                                <td width='120'>重講習時間</td>
                                                <td width='440'>重講習地點</td>
                                            </tr>
                                            <tr>
                                                <td><?php echo form_input('surc_no',$surc->surc_no,'readonly class="form-control"'); 
                                                echo form_hidden('s_ic',$surc->surc_sic);
                                                echo form_hidden('s_num',$surc->surc_snum);
                                                echo form_hidden('s_cnum',$surc->surc_cnum);
                                                echo form_hidden('surc_num',$surc->surc_num );
                                                echo form_hidden('surc_projectnum',$surcProject->sp_no );
                                                echo form_hidden('surc_address',$surc->s_dpcounty.$surc->s_dpdistrict.$surc->s_dpaddress);
                                                ?></td>
                                                <td><?php echo form_input('surc_send_num',$surc->surc_send_num,' class="form-control"') ?></td>
                                                <td><?php echo form_input('surc_send_bnum',$surc->surc_send_bnum,' class="form-control"') ?></td>
                                                <td><input name="surc_amount" type="number" readonly class="form-control" value="<?php if($surc->surc_amount !=null)echo $surc->surc_amount;
                                                    else{
                                                        if(substr($surc->surc_lastnum,0,1)!='A')echo '5000';
                                                        else echo '10000';
                                                    }

                                                        echo '';?>"></td>
                                                <td><input name="surc_findate" type="date" class="form-control" value="<?php if($surc->surc_findate != null)echo $surc->surc_findate;
                                                    else echo '';?>"></td>
                                                <td><?php
                                                        $options1 = array(
                                                            '2020-04-10'=> '04/10',
                                                            '2020-05-15'=> '05/15',
                                                            '2020-06-15'=> '06/15',
                                                            '2020-11-27'=> '11/27',
                                                            '2020-12-02'=> '12/2',
                                                            '2020-12-04'=> '12/4',
                                                            '2020-12-11'=> '12/11',
                                                            '2020-12-18'=> '12/18',
                                                        );
                                                        if(isset($surc)) {
                                                            echo form_dropdown('surc_lec_date',$options1,$surc->surc_lec_date, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_dropdown('surc_lec_date',$options,'', 'class="form-control"');
                                                        }
                                                    ?></td>
                                                <td><?php if(isset($surc->surc_lec_place)&&$surc->surc_lec_place != null)echo form_input('surc_lec_place',$surc->surc_lec_place,'class="form-control"'); 
                                                     else echo  form_input('surc_lec_place','臺北市中正區青島東路8號','class="form-control"'); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                                <div class="modal fade" id="filedownload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="othernewLabel">文件檢視</h5>
                                            </div>
                                            <div class="modal-body">
                                                <a href=<?php echo base_url('PDFcreate/fine_doc/'.$sp->fd_snum)?> target="_blank"><button style="padding:0px 0px;" class="btn btn-default" type="button">原罰鍰處分書</button></a>
                                                <a href="#"><button style="padding:0px 0px;" class="btn btn-default" id="delivery" type="button">罰鍰送達證書</button></a>
                                                <a href="#"><button style="padding:0px 0px;" class="btn btn-default" id="prison" type="button">在監資料</button></a>
                                                <a href="#"><button style="padding:0px 0px;" class="btn btn-default" id="other" type="button">毒防中心函&毒防中心名冊(掃描)</button></a>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            <!-- /.panel -->
                        </div>
                                <div class="modal fade" id="surc_date" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <?php echo form_open_multipart('Surcharges/updateSucrdate','id="updateSucrEd"') ?>     
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="othernewLabel">輸入日期</h5>
                                            </div>
                                            <div class="modal-body">
                                            <div class="modal-body">
                                                <label>發文日期</label>
                                                <input name="surc_date" type="date" class="form-control" value="<?php if(isset($surc))echo $surc->surc_date;
                                                    else echo '';?>">
                                                <?php echo form_hidden('surc_no',$surc->surc_no);?>
                                                <?php echo form_hidden('s_ic',$surc->surc_sic);?>
                                                <?php echo form_hidden('surc_projectnum',$surc->surc_projectnum);?>
                                                <!--<label>繳納期限</label>
                                                <input name="f_date" type="date" class="form-control" value="<?php //if(isset($fine))echo $fine->f_date;
                                                    //else echo '';?>"> -->
                                            </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-default">確認</button>
                                            </div>
                                        </div>
                                    </div></form>
                                </div>
                            <a class="media" id="pdfviewod" href=<?php echo base_url('/result/S36C-120093013580-1-18.pdf')?>></a><!--過後在想要怎麼龍-->
                            <a class="media" id="pdfviewdelivery" href=<?php echo base_url('/送達文件/'.$surc->fdd_doc)?>></a>
                            <a class="media" id="pdfviewprison" href=<?php echo base_url('/surcharge_doc/'.$surc->surc_prison_doc)?>></a>
                            <a class="media" id="pdfviewother" href=<?php echo base_url('/surcharge_doc/'.$surc->surc_other_doc)?>></a>
                        <!-- /.col-lg-12 -->
                        </form>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <script>
            $("#next").click(function (){
                $("#nextvalue").val('<?php echo $next ?>');
                //alert($("#nextvalue").val());
                $("#updateSucrEd").submit();
            });
            $("#prev").click(function (){
                $("#prevvalue").val('<?php echo $prev ?>');
                //alert($("#prevvalue").val());
                $("#updateSucrEd").submit();
            });
            $("#delivery").click(function (){
                //alert('test');
                $('#pdfviewdelivery').show();  
                $('#pdfviewod').hide();  
                $('#pdfviewprison').hide();  
                $('#pdfviewother').hide();  
            });
            $("#prison").click(function (){
                $('#pdfviewprison').show();  
                $('#pdfviewod').hide();  
                $('#pdfviewdelivery').hide();  
                $('#pdfviewother').hide();  
            });
            $("#other").click(function (){
                $('#pdfviewother').show();  
                $('#pdfviewod').hide();  
                $('#pdfviewdelivery').hide();  
                $('#pdfviewprison').hide();  
            });

            $(function() {  
                $('a#pdfviewdelivery').media({width:1400, height:800});  
                $('a#pdfviewod').media({width:1400, height:800});  
                $('a#pdfviewprison').media({width:1400, height:800});  
                $('a#pdfviewother').media({width:1400, height:800});  
                $('#pdfviewdelivery').hide();  
                $('#pdfviewod').hide();  
                $('#pdfviewprison').hide();  
                $('#pdfviewother').hide();  
            });  
            $(document).ready(function(){
                $( "#updateSucrEd" ).validate({
                    rules: {
                        surc_send_num: {
                            required: true,
                            digits: true,
                            minlength: 10,
                            maxlength: 10
                        },
                        surc_send_bnum: {
                            required: true,
                            digits: true,
                            maxlength: 1
                        },
                    },
                    messages: {
                        surc_send_num: {
                            required: "此欄位不得為空",
                            digits: "請輸入數字",
                            minlength: "請完整輸入",
                            maxlength: "最多輸入10個數字",
                        },
                        surc_send_bnum: {
                            required: "此欄位不得為空",
                            digits: "請輸入數字",
                            maxlength: "最多輸入1個數字",
                        },
                    }
                }); 
            });
        </script>
    </body>
</html>
