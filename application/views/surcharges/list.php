        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#new">建立專案</button></a></li>
                    <li><a><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#addcases">加入舊專案</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-primary">
                                   
                                <div class="panel-heading list_view">
									<div class="row">
										<div class="col-md-6">
											<h3 class="panel-title">列表清單</h3>
										</div>
										<div class="col-md-6 text-right">
											<span class="label label-default" id="current_select_nums">已選取：0</span>
											<a><button class="btn btn-success btn-sm"  data-toggle="modal" data-target="#new">建立專案</button></a>
											<a><button class="btn btn-default btn-sm" data-toggle="modal" data-target="#addcases">加入舊專案</button></a>
										</div>
									</div>
									
                                    <!-- <input type="checkbox" name="list"  data-target ="1"  checked> 罰鍰列管編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 怠金依據文號
                                    <input type="checkbox" name="list" data-target ="3" checked> 應講習時間
                                    <input type="checkbox" name="list" data-target ="4" checked> 應講習地點
                                    <input type="checkbox" name="list" data-target ="5" checked> 發文日期
                                    <input type="checkbox" name="list" data-target ="6" checked> 發文字號
                                    <input type="checkbox" name="list" data-target ="7" checked> 移送分局
                                    <input type="checkbox" name="list" data-target ="8" checked> 依據單位
                                    <input type="checkbox" name="list" data-target ="9" checked> 依據日期
                                    <input type="checkbox" name="list" data-target ="10" checked> 依據字號
                                    <input type="checkbox" name="list" data-target ="11" checked> 受處分人姓名
                                    <input type="checkbox" name="list" data-target ="12" checked> 身分證號
                                    <input type="checkbox" name="list" data-target ="13" checked> 聯絡電話
                                    <input type="checkbox" name="list" data-target ="14" checked> 戶籍地址
                                    <input type="checkbox" name="list" data-target ="15" checked> 現住地址
                                    <input type="checkbox" name="list" data-target ="16" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="17" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="18" checked> 查獲單位
                                    <input type="checkbox" name="list" data-target ="19" checked> 罰鍰(萬元)
                                    <input type="checkbox" name="list" data-target ="20" checked> 講習時數
                                    <input type="checkbox" name="list" data-target ="21" checked> 罰鍰送達時間
                                    <input type="checkbox" name="list" data-target ="22" checked> 講習當時在監
                                    <input type="checkbox" name="list" data-target ="23,24" checked> 現在在監情形 -->
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
									<?php echo form_open_multipart('','id="sp_checkbox"') ?>   
										<div class="table-responsive">
											<?php echo $s_table;?>
										</div>                    
										<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
										<input id="s_status" type="hidden" name="s_status" value=''> 
										<input id="front_word" type="hidden" name="front_word" value=''>  
										<div class="modal fade" id="addcases" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header bg-primary">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title" id="othernewLabel">加入舊專案</h4>                                            
													</div>
													<div class="modal-body">
														<div class="form-group">
															<label id="othernewLabel">選擇專案</label>
															<?php echo form_dropdown('sp_num',$opt ,'', 'class="form-control" id="sel"')?>
														</div>
														
													</div>
													<div class="modal-footer">
														<button class="btn btn-default" data-dismiss="modal">關閉</button>
														<button id='no'  type="button" class="btn btn-default" >加入舊專案</button>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												<div class="modal-header bg-primary">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" >建立新專案</h4>
													<!-- <h5 class="modal-title" id="othernewLabel">批次繳款日期</h5> -->
													
												</div>
												<div class="modal-body">
														<div class="form-group hidden">
															<label id="othernewLabel">批次繳款日期</label>
															<input type='text' name='sp_date' class='form-control rcdate'/>
															<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
														</div>	
														<div class="form-group">
															<label>專案類型</label>
															<select name='sp_type' id="sp_type" class="form-control" onchange="changeSelEvent($(this))">
																<option value="裁罰" selected>裁罰</option>
																<!-- <option value="在監">在監</option> -->
																<option value="不裁罰">不裁罰</option>
																<!-- <option value="郵務送達">郵務送達</option>
																<option value="囑託監所送達">囑託監所送達</option>
																<option value="公示送達">公示送達</option>
																<option value="不裁罰">不裁罰</option> -->
															</select>
														</div>													
														
														<div  id="surc_olec_wrap" class="hidden">
															<div class="form-group ">
																<label id="othernewLabel">專案名稱【應講習日】</label>
																<input type='text' name='surc_olec_date' class='form-control rcdate'/>
																<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
															</div>
														</div>
														<div  id="surc_senddnum_wrap">
															<div class="form-group ">
																<label id="othernewLabel">專案名稱【發文字號（主號）】</label>
																<input type='text' name='surc_send_num' class='form-control'/>
															</div>
														</div>
												</div>
												<div class="modal-footer">
													<button class="btn btn-default" data-dismiss="modal">關閉</button>
													<button id='yes'  type="button" class="btn btn-success" >建立專案</button>
												</div>
												</div>
											</div>
										</div>
									<?php echo form_close(); ?>  
                                </div>
                                
                                
                                
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-danger">
                                   
                                <div class="panel-heading list_view">
									<div class="row">
										<div class="col-md-8">
											<h3 class="panel-title">待重處列表（退回、重處）<small>一般重處與公示重處請分開專案建立</small></h3>
										</div>
										<div class="col-md-4 text-right">
											<span class="label label-default" id="rb_current_select_nums">已選取：0</span>
											<a><button class="btn btn-success btn-sm" data-toggle="modal" data-target="#new_rb"  >建立專案</button></a>
											<a><button class="btn btn-default btn-sm" data-toggle="modal" data-target="#rollbackModal">加入舊專案</button></a>
										</div>
									</div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
									<div class="table-responsive">
                                        <?php echo $roll_table; ?>
                                    </div>  
								</div>
								<form id="sp_checkbox_rb">
									<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
									<input id="s_status" type="hidden" name="s_status" value=''>
									<input id="front_word" type="hidden" name="front_word" value='P'> 
									<div class="modal fade" id="rollbackModal" tabindex="-1" role="dialog" aria-labelledby="rollbackModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="form-group">
													<label>舊專案</label>
													<?php echo form_dropdown('sp_num',$rb_opt ,'', 'class="form-control" id="sel"')?>                                                            
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
												<button id='no_rb' class="btn btn-default" >加入舊專案</button>
											</div>
											</div>
										</div>
									</div> 
									<div class="modal fade" id="new_rb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
											<div class="modal-header bg-primary">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" >建立新專案</h4>
												<!-- <h5 class="modal-title" id="othernewLabel">批次繳款日期</h5> -->
												
											</div>
											<div class="modal-body">
													<div class="form-group hidden">
														<label id="othernewLabel">批次繳款日期</label>
														<input type='text' name='sp_date' class='form-control rcdate'/>
														<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
													</div>	
													<div class="form-group">
														<label>專案類型</label>
														<select name='sp_type' id="sp_type" class="form-control" onchange="changeSelEvent($(this))">
															<option value="裁罰" selected>裁罰</option>
															<!-- <option value="在監">在監</option> -->
															<!-- <option value="不裁罰">不裁罰</option> -->
															<!-- <option value="郵務送達">郵務送達</option>
															<option value="囑託監所送達">囑託監所送達</option>
															<option value="公示送達">公示送達</option>
															<option value="不裁罰">不裁罰</option> -->
														</select>
													</div>													
													
													<!-- <div  id="surc_olec_wrap" class="hidden">
														<div class="form-group ">
															<label id="othernewLabel">專案名稱【應講習日】</label>
															<input type='text' name='surc_olec_date' class='form-control rcdate'/>
															<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
														</div>
													</div> -->
													<div  id="surc_senddnum_wrap">
														<div class="form-group ">
															<label id="othernewLabel">專案名稱【發文字號（主號）】</label>
															<input type='text' name='surc_send_num' class='form-control'/>
														</div>
													</div>
											</div>
											<div class="modal-footer">
												<button class="btn btn-default" data-dismiss="modal">關閉</button>
												<button  id='yes_rb'  type="button" class="btn btn-success" >建立專案</button>
											</div>
											</div>
										</div>
									</div>
								</form>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <script>
            /*$(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });*/
        $(document).ready(function (){
			$(".rcdate").change(function(){
				let strlen = $(this).val().length;
				if(strlen < 7 || strlen < "7")
				{
					if(strlen != 0)
						$(this).val("0"+$(this).val());
				}
				else
				{
					return false;
				}
					
			});
			$(".rcdate").keypress(function(){
				if($(this).val().length >= 7)
					return false;
			});
			_iDisplayStart = 0;
           table = $('#table1').DataTable({
			fixedHeader: true,
              'columnDefs': [
				{
						targets:   0,
						visible: false
				},
                 {
                    'targets': 1,
					"orderable": false,
					className: 'select-checkbox'
                    // 'checkboxes': {
                    //    'selectRow': true
                    // }
                 }
              ],
			  select: {
						style:    'os',
						selector: 'td:first-child',
						style: 'multi'
			},
			stateSave: true,
			stateSaveCallback: function(settings,data) {
				_iDisplayStart = settings._iDisplayStart;
				},
            //   'select': {
            //      'style': 'multi'
            //   },
              "width":"100px",
			//   'order': [[0, 'desc']],
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {
				// $("td:first", nRow).html(iDisplayIndex + 1); //自動序號
				$(nRow).attr("data-id", aData[0]);
			},
			  dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: ""
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
		   $('#table1 thead tr:eq(0) th:eq(0)').html('<span class="label label-default" id="select10">一次10筆</span>');
		   $("#select10").on( "click", function(e) {			   
				$(this).toggleClass('selected10');
				if ($(this).hasClass('selected10')) {
					for (let index = _iDisplayStart; index < (_iDisplayStart+10); index++) {
						table.rows(index).select(); 
					}
					       
				} else {
					for (let index = _iDisplayStart; index < (_iDisplayStart+10); index++) {
						table.rows(index).deselect(); 
					}
				}
				updateCurrenNums(table, $("#current_select_nums"));
			});
			$('#table1 tbody').on( 'click', 'tr', function () {
            	$(this).toggleClass('selected');
				updateCurrenNums(table, $("#current_select_nums"));
			});
			_iDisplayStart_rb = 0;
		   table_rollback = $('#table_rollback').DataTable({
			'columnDefs': [
				{
						targets:   0,
						visible: false
				},
                 {
                    'targets': 1,
					"orderable": false,
					className: 'select-checkbox'
                    // 'checkboxes': {
                    //    'selectRow': true
                    // }
                 }
              ],
			  select: {
						style:    'os',
						selector: 'td:first-child',
						style: 'multi'
			},
			  stateSave: true,
				stateSaveCallback: function(settings,data) {
					_iDisplayStart_rb = settings._iDisplayStart;
				},
            //   'select': {
            //      'style': 'multi'
            //   },
              "width":"100px",
			//   'order': [[0, 'desc']],
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {
				// $("td:first", nRow).html(iDisplayIndex + 1); //自動序號
				$(nRow).attr("data-id", aData[0]);
			},
			  dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
		   $('#table_rollback thead tr:eq(0) th:eq(0)').html('<span class="label label-default" id="selectrb10">一次10筆</span>');
		   $("#selectrb10").on( "click", function(e) {			   
				$(this).toggleClass('selectedrb10');
				if ($(this).hasClass('selectedrb10')) {
					for (let index = _iDisplayStart_rb; index < (_iDisplayStart_rb+10); index++) {
						table_rollback.rows(index).select(); 
					}
					       
				} else {
					table_rollback.rows().deselect(); 
				}
				updateCurrenNums(table_rollback, $("#rb_current_select_nums"));
			});
			$('#table_rollback tbody').on( 'click', 'tr', function () {
            	$(this).toggleClass('selected');
				updateCurrenNums(table_rollback, $("#rb_current_select_nums"));
			});
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            // $("#yes").click(function (){
            //     $("#s_status").val('1');
            //         //alert($("#s_status").val());
            //     $("#sp_checkbox").submit();
            // });
			$("#yes").click(function (){
				addProjectEvent('1', $("#sp_checkbox"), table);
			});
            // $("#no").click(function (){
            //     $("#s_status").val('0');
            //         //alert("Submitted");
            //         $("#sp_checkbox").submit();
            // });
			$("#no").click(function (){
				addProjectEvent('0', $("#sp_checkbox"), table);
			});
			$("#yes_rb").click(function (){
				addProjectEvent('1', $("#sp_checkbox_rb"), table_rollback);
			});

			$("#no_rb").click(function (){
				addProjectEvent('0', $("#sp_checkbox_rb"), table_rollback);
			});

           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
              var form = this;

              var rows_selected = table.column(0).checkboxes.selected();
                $('#example-console-rows').text(rows_selected.join(","));
                $('#s_cnum').val(rows_selected.join(","));
              
                // Output form data to a console     
                //$('#example-console-form').text($(form).serialize());
               
                // Remove added elements
                $('input[name="id\[\]"]', form).remove();
               
                // Prevent actual form submission
                //e.preventDefault();
           });
		   $('#new').on('hidden.bs.modal', function (e) {
				$("#sp_type").val('裁罰').change();
				$("#surc_senddnum_wrap input, surc_olec_wrap input").val('');
				$("#surc_senddnum_wrap").removeClass('hidden');
			})
            
            $("#sel").change(function(){
            switch ($(this).val()){
                case "郵務送達" : 
                    $("#sel1 select").remove();
                break;
                case "囑託監所送達" : 
                    $("#sel1 select").remove();
                break;
                case "公示送達" : 
                    $("#sel1 select").remove();
                break;
                case "不裁罰" : 
                    $("#sel1 select").remove();
                    var array = [ "在監","未合法送達","其他" ];
                    //利用each遍歷array中的值並將每個值新增到Select中
                    $("#sel1").append("<select name='sp_reason1' class='form-control'>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                        $("#sel1").append("<select name='sp_reason2' class='form-control'>"
                        +"<option selected value=NULL>原因2</option>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                break;
            }});
        });
		function addProjectEvent(s_status, form, select_table)
    	{
			var formi = form;

			var rows_selected = $.map(select_table.rows('.selected').nodes(), function (item) {return $(item).attr("data-id");});
			//select_table.column(0).checkboxes.selected();
			$('#example-console-rows').text(rows_selected.join(","));
			form.find('#s_cnum').val(rows_selected.join(","));
			form.find("#s_status").val(s_status);

			// Remove added elements
			$('input[name="id\[\]"]', formi).remove();
            
            $.ajax({            
                type: 'POST',
                url: '<?php echo base_url('Surcharges/updatesurchargesproject') ?>',
                data: new FormData(form[0]),
                // dataType: 'json',
                processData : false, 
                contentType: false,
                cache: false,
                error:function(){
                    // console.log('error')
                },
                success: function(resp){
                    
                    // console.log(resp);
                },
                complete:function(resp){
					console.log(resp)
                    // location.href = `../${resp.responseText}`;
                    swal({
                        title: "成功!",
                        text: "已加入處理專案!",
                        icon: "success",
                        buttons: {
                            goto: {
                            text: "前往專案處理",
                            value: "goto",
                            },
                            ok: {
                            text: "好",
                            value: "ok",
                            },
                        },
                    }).then((value) => {
                        switch (value) {                        
                            case "goto":
                            	location.href = resp.responseText;
                            break;
							case "ok":
								location.reload();
                            break;                        
                            default:
                        }
                    });
                    
                    
                }
            });
            
    	}
		function changeSelEvent(obj){
			$val = obj.val();
			$("#surc_senddnum_wrap, #surc_olec_wrap").addClass('hidden');
			switch ($val) {
				case '裁罰':
					$("#surc_senddnum_wrap").removeClass('hidden');
					break;
				case '不裁罰':
					$("#surc_olec_wrap").removeClass('hidden');
					
					var data = table.rows( function ( idx, data, node ) {
						return $(node).find('input[type="checkbox"]').prop('checked');
					} ).data();
					$("#surc_olec_wrap input").val(tranfer2RCyear(data[0][6]));
					break;
				default:
					break;
			}
		}
		function updateCurrenNums(tabledom, dom){
			
			dom.text("已選取：" + tabledom.rows('.selected').count());
		}
		/** 轉民國年 */
		function tranfer2RCyear(date)
		{
			if(!date)
				return false;
			date = date.split('-');
			date = (parseInt(date[0]) - 1911) + date[1] + date[2];
				return date;                
		} 
    </script>

    </body>
</html>
