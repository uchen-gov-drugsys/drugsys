        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="save"></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php echo form_open_multipart('Acc_cert/save_transfer_change','id="save"') ?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <br><table class="table">
                                                    <tbody id="drugall" class="form-group">
                                                        <tr>
                                                            <td>
                                                                <label>處分書編號</label>
                                                                <input name="fd_num" class="form-control" value= "<?php if(isset($fcp->surc_no)) echo $fcp->surc_no.'-1'; 
                                                                else echo $fcp->fd_num.'-1' ?> "readonly>
                                                                <input id="f_num" type="hidden" name="f_num" value='<?php echo $fcp->f_num ?>'> 
                                                                <input id="f_cnum" type="hidden" name="f_cnum" value='<?php echo $fcp->f_cnum ?>'> 
                                                                <input id="fcp_no" type="hidden" name="fcp_no" value='<?php echo $fcp->fcp_no ?>'> 
                                                                <input id="f_snum" type="hidden" name="f_snum" value='<?php echo $fcp->f_snum ?>'> 
                                                                <input id="f_sic" type="hidden" name="f_sic" value='<?php echo $fcp->f_sic ?>'> 
                                                                <input id="f_BVC" type="hidden" name="f_BVC" value='<?php echo $fcp->f_BVC ?>'> 
                                                                <input id="f_date" type="hidden" name="f_date" value='<?php echo $fcp->f_date ?>'> 
                                                                <input id="f_project_num" type="hidden" name="f_project_num" value='<?php echo $fcp->f_project_num ?>'> 
                                                                <input id="f_payday" type="hidden" name="f_payday" value='<?php echo $fcp->f_payday ?>'> 
                                                                <input id="f_paytype" type="hidden" name="f_paytype" value='<?php echo $fcp->f_paytype ?>'> 
                                                                <input id="f_refund" type="hidden" name="f_refund" value='<?php echo $fcp->f_refund ?>'> 
                                                                <input id="f_turnsub" type="hidden" name="f_turnsub" value='<?php echo $fcp->f_turnsub ?>'> 
                                                                <input id="f_certno" type="hidden" name="f_certno" value='<?php echo $fcp->f_certno ?>'> 
                                                                <input id="f_certDate" type="hidden" name="f_certDate" value='<?php echo $fcp->f_certDate ?>'> 
                                                                <input id="f_check_unit" type="hidden" name="f_check_unit" value='<?php echo $fcp->f_check_unit ?>'> 
                                                                <input id="f_check_receipt" type="hidden" name="f_check_receipt" value='<?php echo $fcp->f_check_receipt ?>'> 
                                                                <input id="f_check_unit" type="hidden" name="f_check_unit" value='<?php echo $fcp->f_check_unit ?>'> 
                                                                <input id="f_check_officenum" type="hidden" name="f_check_officenum" value='<?php echo $fcp->f_check_officenum ?>'> 
                                                                <input id="f_check_checknum" type="hidden" name="f_check_checknum" value='<?php echo $fcp->f_check_checknum ?>'> 
                                                                <input id="f_remark" type="hidden" name="f_remark" value='<?php echo $fcp->f_remark ?>'> 
                                                                <input id="f_cancelAmount" type="hidden" name="f_cancelAmount" value='<?php echo $fcp->f_cancelAmount ?>'> 
                                                            </td>
                                                            <td>
                                                                <label>姓名</label>
                                                                <input name="s_name" class="form-control" value= "<?php echo $fcp->s_name ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>身份證編號</label>
                                                                <input name="f_sic" class="form-control" value= "<?php echo $fcp->s_ic ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>年度罰緩金額</label>
                                                                <input name="f_damount" class="form-control" value= "<?php echo $fcp->f_damount ?> ">
                                                            </td>
                                                            <td>
                                                                <label>年度怠金金額</label>
                                                                <input name="f_samount" class="form-control" value= "<?php echo $fcp->f_samount ?> ">
                                                            </td>
                                                            <td>
                                                                <label>完納金額</label>
                                                                <input name="f_payamount" class="form-control" value= "<?php echo $fcp->f_payamount ?> ">
                                                           </td>
                                                            <td>
                                                                <label>移送案號</label>
                                                                <input class="form-control" value= "<?php echo $fcp->ft_no ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>分署</label>
                                                                <input class="form-control" value= "<?php echo $fcp->s_roffice ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>執行費</label>
                                                                <input name="f_fee" class="form-control" value= "<?php echo $fcp->f_fee ?> ">
                                                            </td>
                                                            <td>
                                                                <label>轉正類型</label>
                                                                <select name="f_change_type" class="form-control">
                                                                    <option value="罰轉怠">罰轉怠</option>
                                                                    <option value="怠轉罰">怠轉罰</option>
                                                                    <option value="跨年度">跨年度</option>
                                                                    <option value="罰轉罰">罰轉罰</option>
                                                                    <option value="怠轉怠">怠轉怠</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $( "#save" ).validate({
                    rules: {
                        f_damount: {
                            required: true,
                        },
                        f_samount: {
                            required: true,
                        },
                        f_payamount: {
                            required: true,
                            max: <?php echo $fcp->f_payamount ?>
                        },
                    },
                });        
            });            
        </script>
    </body>
</html>
