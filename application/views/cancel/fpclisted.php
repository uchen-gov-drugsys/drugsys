        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><button id='yes' class="btn btn-default"style="padding:0px 0px;" >回到列表</button></a></li>
                    <li><a><button  id='mergesel' style="padding:0px 0px;" class="btn btn-default" data-toggle="modal" data-target="#com" data-whatever="合併">合併</button></a></li>
                    <li><a><button id='transfer' class="btn btn-default"style="padding:0px 0px;" >單筆轉移</button></a></li>
                    <li><a><button id='disassemble' class="btn btn-default"style="padding:0px 0px;" >分解</button></a></li>
                    <!--li><a><button id='disassemble' class="btn btn-default"style="padding:0px 0px;" >拆解</button></a></li-->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                                </br>
                                <div class="panel-heading list_view">
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 處分書編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 姓名
                                    <input type="checkbox" name="list" data-target ="3" checked> 身份證編號
                                    <input type="checkbox" name="list" data-target ="4" checked> 年度罰緩金額
                                    <input type="checkbox" name="list" data-target ="5"> 完納金額
                                    <input type="checkbox" name="list" data-target ="6"> 分期資訊
                                    <input type="checkbox" name="list" data-target ="7" checked> 移送案號
                                    <input type="checkbox" name="list" data-target ="8"> 分署
                                    <input type="checkbox" name="list" data-target ="9"> 憑證核發日期
                                    <input type="checkbox" name="list" data-target ="10"> 憑證編號
                                    <input type="checkbox" name="list" data-target ="11"> 註銷保留款
                                    <input type="checkbox" name="list" data-target ="12"> 虛擬帳號
                                </div>
                            <form action=<?php echo base_url("Acc_cert/editfcplist1") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                                <input id="f_no" type="hidden" name="f_no" value='<?php echo $f_no ?>'> 
                           </div>
                                <div class="modal fade" id="com" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="othernewLabel">合併欄位</h5>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table" id="example">
                                                    <tbody>
                                                        <tr>
                                                            <td>處分書編號</td>
                                                            <td><select id="select1" name="selcnum" class="form-control"></select></td>
                                                            <td><button id='merge' type="button" class="btn btn-default">帶入</button></td>
                                                        </tr>
                                                    </tbody> 
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                            </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'orderable': false,
                    'targets': [9],
                 },
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']]
           });
                table.column(5).visible(false);
                table.column(6).visible(false);
                table.column(8).visible(false);
                table.column(9).visible(false);
                table.column(10).visible(false);
                table.column(11).visible(false);
                table.column(12).visible(false);
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                $("#sp_checkbox").submit();
            });
            $("#merge").click(function (){
                $("#s_status").val('merge');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#mergesel").click(function (){
                var rows_selected = table.column(0).checkboxes.selected();
                //alert(rows_selected);
                jQuery.each( rows_selected, function( i, val ) {
                    $('#select1').append('<option value="'+val+'"> '+val+' </option>');        
                });
            });
            $("#transfer").click(function (){
                $("#s_status").val('transfer');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#disassemble").click(function (){
                $("#s_status").val('disassemble');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
                if($.isEmptyObject($('#s_cnum').val())){
                    alert('請先選擇案件');
                    e.preventDefault();
                }
                else{
                }
           });
    });
    </script>
