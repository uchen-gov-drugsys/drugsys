        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><input type="submit" value="確認修改" class="btn btn-default" style="padding:0px 0px;" form="newcases" id="yes"></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                
                <div class="container-fluid">
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							<!-- <a><input type="submit" value="新增並上傳" class="btn btn-success" form="newcases" id="yes"></a> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="panel panel-info">
                                <div class="panel-heading">
									<h4 class="panel-title">新增上傳作業</h4>
                                </div>                                <!-- /.panel-heading -->
                                <div class="panel-body">									
									<div class="row">
										<div class="col-md-12">
											<!-- <form action=<?php //echo base_url("Surcharges/receiveupdate")?> id="addcases"  enctype="multipart/form-data" method="post" accept-charset="utf-8" > -->
												<form  id="sendForm">
													<div class="form-group">
														<label >
															執行署TXT檔
														</label>
														<input class="form-controls" name="upload_file" type="file" accept=".txt">
														<span class="text-danger"><small>限上傳txt類型之檔案且檔案大小限制5MB</small> </span>
													</div>
													<input type="button" value="更新並上傳" class="btn btn-success btn-block" onclick="sendEvent()">
												</form>
										</div>
									</div>
									
                                </div>
								
                            </div>
							
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
					<div class="row">
						<div class="col-md-12">
							<hr>
							<div class="table-responsive">
								<table id="table1" class=" table table-bordered" style="width:100%;">
									<thead>
										<tr>
											<th>類型</th>
											<th>案件編號</th>
											<th>受處分人姓名</th>
											<th>身份證編號</th>
											<th>移送Log</th>
											<th>股別Log</th>
										</tr>
									</thead>
									<tbody></tbody>
									<tfoot></tfoot>
								</table>
							</div>
						</div>
					</div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
            <!-- /#page-wrapper -->
            </div>
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
		$(document).ready(function() {
			var table1 = $('#table1').DataTable({
            dom: 'f',
            "destroy":true,	
            "ordering":false,
            fixedHeader: true,
            "language": {
                        "processing": "資料載入中...",
                        "lengthMenu": "每頁顯示 _MENU_ 筆",
                        "zeroRecords": "資料庫中未有相關資料。",
                        "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                        "infoEmpty": "資料庫中未有相關資料。",
                        "search": "搜尋:",
                        "paginate": {
                            "first": "第一頁",
                            "last": "最後一頁",
                            "next": "下一頁",
                            "previous": "上一頁"
                        }
                    }
        	});
        });  
		function sendEvent(){
			$.ajax({            
				type: 'POST',
				url: '<?php echo base_url("Fine_traf/updateTxt")?>',
				data: new FormData($("#sendForm")[0]),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData:false,
				error:function(){
					
				},
				success: function(data){
					dataSet = data;
					$("#sendForm")[0].reset();
					// table1.DataTable().destroy();
					table1 = $('#table1').DataTable({						
						"destroy":true,	
						"ordering":false,
						data: dataSet,
						columns: [
							{ data: "type", width: "10%" },
							{ data: "f_caseid", width: "10%" },
							{ data: "f_username", width: "10%" },
							{ data: "f_userid", width: "10%" },
							{ data: "f_movelog" },
							{ data: "f_depmovelog", 
								"render": function ( data, type, row, meta ) {
									let str = "";
									if(data)
									{
										let ary = data.split('<br/>');
										
										for (let index = 0; index < ary.length; index++) {
											const element = ary[index].split('_');
											str += (element[2] + '_' + element[3] + '<br/>');
										}
									}
									
									return str;
									// return data;
								}
							}
						],
						"language": {
									"processing": "資料載入中...",
									"lengthMenu": "每頁顯示 _MENU_ 筆",
									"zeroRecords": "資料庫中未有相關資料。",
									"info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
									"infoEmpty": "資料庫中未有相關資料。",
									"search": "搜尋:",
									"paginate": {
										"first": "第一頁",
										"last": "最後一頁",
										"next": "下一頁",
										"previous": "上一頁"
									}
								}
					});
				},
				complete:function(resp){
					// location.href = `../${resp.responseText}`;
				}
			});
		}
		
            // $("#zipcode").twzipcode({
            // "zipcodeIntoDistrict": true,
            //     "css": ["city form-control", "district form-control"],
            //     'countyName'   : 's_dpcounty',   // 預設值為 county
            //     'districtName' : 's_dpdistrict', // 預設值為 district
            //     'zipcodeName'  : 's_dpzipcode' // 預設值為 zipcode
            // });         
            // $("#zipcode2").twzipcode({
            // "zipcodeIntoDistrict": true,
            //     "css": ["city form-control", "district form-control"],
            //     'countyName'   : 's_rpcounty',   // 預設值為 county
            //     'districtName' : 's_rpdistrict', // 預設值為 district
            //     'zipcodeName'  : 's_rpzipcode' // 預設值為 zipcode
            // });  
            // $("#zipcode3").twzipcode({
            // "zipcodeIntoDistrict": true,
            // "css": ["city form-control", "town form-control"],
            //     'countyName'   : 's_fadai_county',   // 預設值為 county
            //     'districtName' : 's_fadai_district', // 預設值為 district
            //     'zipcodeName'  : 's_fadai_zipcode' // 預設值為 zipcode
            // }); 
 
                // $('#receiverchange').on('click', function() {
                //    // $("#zipcode2").empty();
                //     var city = $(zipcode).twzipcode('get', 'city');
                //     var zipc = $(zipcode).twzipcode('get', 'zipcode');
                //     //console.log(zipc);   // 縣市
                //     if( zipc == ""){
                //         alert("請務必選擇鄉鎮市區");return false;
                //     } 
                //     $("#zipcode2").twzipcode('set', zipc[0]);
                //     var sno=$("#s_no").val();

                //     // 移除空格確定是否空值
                //     if (!$.trim($("#s_no").val())) {
                //         if( $("#s_no").val()==""){
                //             $("#s_no").focus();
                //             alert("請務必填入路門牌");return false;
                //         }     
                //     }   
                //     $("#s_no2").val(sno);
                // }); 
            
                       
        </script>
