        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><input type="submit" value="確認修改" class="btn btn-default" style="padding:0px 0px;" form="newcases" id="yes"></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                
                <div class="container-fluid">
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							<!-- <a><input type="submit" value="新增並上傳" class="btn btn-success" form="newcases" id="yes"></a> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="panel panel-info">
                                <div class="panel-heading">
									<h4 class="panel-title">新增執行命令</h4>
                                </div>                                <!-- /.panel-heading -->
                                <div class="panel-body">									
									<div class="row">
										<div class="col-md-12">
											<!-- <form action=<?php //echo base_url("Surcharges/receiveupdate")?> id="addcases"  enctype="multipart/form-data" method="post" accept-charset="utf-8" > -->
												<form  id="sendForm">
													<div class="form-group">
														<label>來文日期</label>
														<input type="text" name="getdocdate" class="form-control rcdate">
														<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
													</div>
													<div class="form-group">
														<label>本局文號</label>
														<input type="text" name="selfofficeno" class="form-control">
													</div>
													<div class="row">
														<div class="col-md-6 text-center">
															<button type="button" class="btn btn-danger" id="addAllowBT" onclick="addRejectEvent()">新增禁止</button>
															
														</div>
														<div class="col-md-6 text-center">
															<button type="button" class="btn btn-success" id="addAllowBT" onclick="addAllowEvent()">新增允許</button>
														</div>
													</div>
												</form>
										</div>
									</div>
									
                                </div>
								
                            </div>
							
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
					<div class="row">
						<div class="col-md-12">
							<hr>
							
								<form id="execForm" action="<?php echo base_url("Fine_traf/updateExec")?>" method="POST">
								<div class="table-responsive">
								<table id="table1" class=" table table-bordered" style="width:100%;">
									<thead>
										<tr>
											<th>來文日期</th>
											<th>移送案號</th>
											<th>本局文號</th>
											<th>案件編號</th>
											<th>姓名</th>
											<th>執行動作</th>
											<th></th>
										</tr>
									</thead>
									<tbody></tbody>
									<tfoot></tfoot>
								</table>
								</div>
								<div class="row text-center">
								<input type="submit" value="全部儲存" class="btn btn-warning">
								</div>
								
								</form>

							
						</div>
					</div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
            <!-- /#page-wrapper -->
            </div>
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
		
		$(document).ready(function() {
			
			$(".rcdate").change(function(){
				let strlen = $(this).val().length;
				if(strlen < 7 || strlen < "7")
				{
					if(strlen != 0)
						$(this).val("0"+$(this).val());
				}
				else
				{
					return false;
				}
					
			});
			$(".rcdate").keypress(function(){
				if($(this).val().length >= 7)
					return false;
			});

			table1 = $('#table1').DataTable({
            dom: 'f',
            "destroy":true,	
            "ordering":false,
            fixedHeader: true,
            "language": {
                        "processing": "資料載入中...",
                        "lengthMenu": "每頁顯示 _MENU_ 筆",
                        "zeroRecords": "資料庫中未有相關資料。",
                        "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                        "infoEmpty": "資料庫中未有相關資料。",
                        "search": "搜尋:",
                        "paginate": {
                            "first": "第一頁",
                            "last": "最後一頁",
                            "next": "下一頁",
                            "previous": "上一頁"
                        }
                    }
        	});
        });  
		function sendEvent(){
			$.ajax({            
				type: 'POST',
				url: '<?php echo base_url("Fine_traf/loadExecTxt")?>',
				data: new FormData($("#sendForm")[0]),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData:false,
				error:function(){
					
				},
				success: function(data){
					dataSet = data;
					$("#sendForm")[0].reset();
					// table1.DataTable().destroy();
					table1 = $('#table1').DataTable({						
						"destroy":true,	
						"ordering":false,
						data: dataSet,
						columns: [
							{ data: "getdocdate", width: "10%" },
							{ data: "moveno" },
							{ data: "selfofficeno"},
							{ data: "caseno", width: "10%" },
							{ data: "casename" },
							{ data: "action", 
								"render": function ( data, type, row, meta ) {
									// console.log(row);
									switch (data) {
										case '禁止':
											return `
											<input type="hidden" name="caseno[]" value="${row['caseno']}"/>
											<input type="hidden" name="getdocdate[]" value="${row['getdocdate']}"/>
											<input type="hidden" name="selfofficeno[]" value="${row['selfofficeno']}"/>
											<input type="hidden" name="moveno[]" value="${row['moveno']}"/>
											<select class="form-control" name="action[]">
											<option value="禁止" selected>禁止</option>
											<option value="准許">准許</option>
											</select>`; 
											break;									
										default:
											return `
											<input type="hidden" name="caseno[]" value="${row['caseno']}"/>
											<input type="hidden" name="getdocdate[]" value="${row['getdocdate']}"/>
											<input type="hidden" name="selfofficeno[]" value="${row['selfofficeno']}"/>
											<input type="hidden" name="moveno[]" value="${row['moveno']}"/>
											<select class="form-control" name="action[]">
											<option value="禁止" >禁止</option>
											<option value="准許" selected>准許</option>
											</select>`; 
											break;
									}
								}
							}
						],
						"language": {
									"processing": "資料載入中...",
									"lengthMenu": "每頁顯示 _MENU_ 筆",
									"zeroRecords": "資料庫中未有相關資料。",
									"info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
									"infoEmpty": "資料庫中未有相關資料。",
									"search": "搜尋:",
									"paginate": {
										"first": "第一頁",
										"last": "最後一頁",
										"next": "下一頁",
										"previous": "上一頁"
									}
								}
					});
				},
				complete:function(resp){
					// location.href = `../${resp.responseText}`;
				}
			});
		}
		function addAllowEvent(){
			$getdocdate = $("input[name='getdocdate']").val().trim();
			$selfofficeno = $("input[name='selfofficeno']").val().trim();
			table1.row.add( [
				`<input type="text" name="getdocdate[]" class="form-control rcdate" value="${$getdocdate}"/><span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>`,
				`<input type="text" name="moveno[]" class="form-control" onchange="loadEvent($(this))"/>`,
				`<input type="text" name="selfofficeno[]" class="form-control" value="${$selfofficeno}"/>`,
				`<span class="caseid"></span><input type="hidden" name="caseno[]"/>`,
				`<span class="casename"></span>`,
				`<select class="form-control" name="action[]">
					<option value="禁止" >禁止</option>
					<option value="准許" selected>准許</option>
					</select`,
				`<span class="fa fa-trash text-danger" onclick="removeEvent($(this).parent().parent())"></span>`
			] ).draw();
			// $('#table1 tbody').append(`<tr>
			// 	<td><input type="text" name="getdocdate[]" class="form-control rcdate" value="${$getdocdate}"/><span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span></td>
			// 	<td><input type="text" name="moveno[]" class="form-control" onchange="loadEvent($(this))"/></td>
			// 	<td><input type="text" name="selfofficeno[]" class="form-control" value="${$selfofficeno}"/></td>
			// 	<td><span class="caseid"></span><input type="hidden" name="caseno[]"/></td>
			// 	<td><span class="casename"></span></td>
			// 	<td><select class="form-control" name="action[]">
			// 		<option value="禁止" >禁止</option>
			// 		<option value="准許" selected>准許</option>
			// 		</select></td>
			// 	<td><span class="fa fa-trash text-danger" onclick="removeEvent($(this).parent().parent())"></span></td>
			// </tr>`);
		}
		function addRejectEvent(){
			$getdocdate = $("input[name='getdocdate']").val().trim();
			$selfofficeno = $("input[name='selfofficeno']").val().trim();
			table1.row.add( [
				`<input type="text" name="getdocdate[]" class="form-control rcdate" value="${$getdocdate}"/><span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>`,
				`<input type="text" name="moveno[]" class="form-control" onchange="loadEvent($(this))"/>`,
				`<input type="text" name="selfofficeno[]" class="form-control" value="${$selfofficeno}"/>`,
				`<span class="caseid"></span><input type="hidden" name="caseno[]"/>`,
				`<span class="casename"></span>`,
				`<select class="form-control" name="action[]">
					<option value="禁止" selected>禁止</option>
					<option value="准許" >准許</option>
					</select`,
				`<span class="fa fa-trash text-danger" onclick="removeEvent($(this).parent().parent())"></span>`
			] ).draw();
			// $('#table1 tbody').append(`<tr>
			// 	<td><input type="text" name="getdocdate[]" class="form-control rcdate" value="${$getdocdate}"/><span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span></td>
			// 	<td><input type="text" name="moveno[]" class="form-control" onchange="loadEvent($(this))"/></td>
			// 	<td><input type="text" name="selfofficeno[]" class="form-control" value="${$selfofficeno}"/></td>
			// 	<td><span class="caseid"></span><input type="hidden" name="caseno[]"/></td>
			// 	<td><span class="casename"></span></td>
			// 	<td><select class="form-control" name="action[]">
			// 		<option value="禁止" selected>禁止</option>
			// 		<option value="准許" >准許</option>
			// 		</select></td>
			// 	<td><span class="fa fa-trash text-danger" onclick="removeEvent($(this).parent().parent())"></span></td>
			// </tr>`);
		}
		function removeEvent(obj){
			// obj.remove();
			table1
			.row( obj )
			.remove()
			.draw();
		}
		function loadEvent(obj){
			let formdata = new FormData();
			formdata.append('moveno', obj.val());
			$.ajax({            
				type: 'POST',
				url: '<?php echo base_url("Fine_traf/loadSuspbymoveno")?>',
				data: formdata,
				dataType: 'json',
				processData : false, 
				contentType: false,
				cache: false,
				success: function(resp){
					obj.parent().parent().find("input[name='caseno[]']").val(resp.caseno);
					obj.parent().parent().find('.caseid').text(resp.caseno);
					obj.parent().parent().find('.casename').text(resp.casename);
				}
			});
		}
            // $("#zipcode").twzipcode({
            // "zipcodeIntoDistrict": true,
            //     "css": ["city form-control", "district form-control"],
            //     'countyName'   : 's_dpcounty',   // 預設值為 county
            //     'districtName' : 's_dpdistrict', // 預設值為 district
            //     'zipcodeName'  : 's_dpzipcode' // 預設值為 zipcode
            // });         
            // $("#zipcode2").twzipcode({
            // "zipcodeIntoDistrict": true,
            //     "css": ["city form-control", "district form-control"],
            //     'countyName'   : 's_rpcounty',   // 預設值為 county
            //     'districtName' : 's_rpdistrict', // 預設值為 district
            //     'zipcodeName'  : 's_rpzipcode' // 預設值為 zipcode
            // });  
            // $("#zipcode3").twzipcode({
            // "zipcodeIntoDistrict": true,
            // "css": ["city form-control", "town form-control"],
            //     'countyName'   : 's_fadai_county',   // 預設值為 county
            //     'districtName' : 's_fadai_district', // 預設值為 district
            //     'zipcodeName'  : 's_fadai_zipcode' // 預設值為 zipcode
            // }); 
 
                // $('#receiverchange').on('click', function() {
                //    // $("#zipcode2").empty();
                //     var city = $(zipcode).twzipcode('get', 'city');
                //     var zipc = $(zipcode).twzipcode('get', 'zipcode');
                //     //console.log(zipc);   // 縣市
                //     if( zipc == ""){
                //         alert("請務必選擇鄉鎮市區");return false;
                //     } 
                //     $("#zipcode2").twzipcode('set', zipc[0]);
                //     var sno=$("#s_no").val();

                //     // 移除空格確定是否空值
                //     if (!$.trim($("#s_no").val())) {
                //         if( $("#s_no").val()==""){
                //             $("#s_no").focus();
                //             alert("請務必填入路門牌");return false;
                //         }     
                //     }   
                //     $("#s_no2").val(sno);
                // }); 
            
                       
        </script>
