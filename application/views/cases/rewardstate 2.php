        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type='text' readonly id='search_fromdate' class="datepicker" placeholder='From date'>
                                                </td>
                                                <td>
                                                    <input type='text' readonly id='search_todate' class="datepicker" placeholder='To date'>
                                                </td>
                                                <td>
                                                    <input type='button' id="btn_search" value="Search">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>                       
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="enum" type="hidden" name="enum" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                        </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function(){

            // Datapicker 
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd'
            });
    
            var dataTable = $('#table1').DataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                //"order": [[ 6, "desc" ]],
                'ajax': {
                    'url':'<?php echo base_url("Cases/rewardjson")?>',
                    'data': function(data){
                        // Read values
                        var from_date = $('#search_fromdate').val();
                        var to_date = $('#search_todate').val();

                        // Append to data
                        data.searchByFromdate = from_date;
                        data.searchByTodate = to_date;
                    }
                },
                "language": //把文字變為中文
                {  
                    "sProcessing": "處理中...",  
                    "sLengthMenu": "顯示 _MENU_ 項結果",  
                    "sZeroRecords": "沒有匹配結果",  
                    "sInfo": "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",  
                    "sInfoEmpty": "顯示第 0 至 0 項結果，共 0 項",  
                    "sInfoFiltered": "(由 _MAX_ 項結果過濾)",  
                    "sInfoPostFix": "",  
                    "sSearch": "搜索:",  
                    "sUrl": "",  
                    "sEmptyTable": "表中數據為空",  
                    "sLoadingRecords": "載入中...",  
                    "sInfoThousands": ",",  
                    "oPaginate": {  
                        "sFirst": "首頁",  
                        "sPrevious": "上頁",  
                        "sNext": "下頁",  
                        "sLast": "末頁"  
                    }
                },
                'columns': [
                    { data: 's_cnum' },
                    { data: 's_name' },
                    { data: 's_ic' },
                    { data: 's_date' },
                    { data: 's_reward_fine' },
                    { data: 's_reward_status' },
                ]
            });
            
            $('#btn_search').click(function(){
                dataTable.draw();
            });
           
        });        
    </script>
