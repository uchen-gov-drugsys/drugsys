<div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                           
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
								<div class="panel-heading">
                                  獎金狀態列表
								</div>
                                <!-- <div class="panel-body">

                                    <div class="table-responsive">
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type='text' readonly id='search_fromdate' class="datepicker" placeholder='From date'>
                                                </td>
                                                <td>
                                                    <input type='text' readonly id='search_todate' class="datepicker" placeholder='To date'>
                                                </td>
                                                <td>
                                                    <input type='button' id="btn_search" value="Search">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>                       
                                </div> -->
                                <div class="panel-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">開始日期</span>
													<input type='text'   id='search_fromdate' class="datepicker form-control" placeholder='開始日期'>
													<span class="input-group-addon">結束日期</span>
													<input type='text'  id='search_todate' class="datepicker form-control" placeholder='結束日期'>
													<span class="input-group-btn">
														<input type='button' class="btn btn-default" id="btn_search" value="搜尋">
													</span>
												</div><!-- /input-group -->
											</div>
										</div>
									</div>
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="enum" type="hidden" name="enum" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                        </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function(){

            // Datapicker 
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd'
            });
    
            var dataTable = $('#table1').DataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                //"order": [[ 6, "desc" ]],
                'ajax': {
                    'url':'<?php echo base_url("Cases/rewardjson")?>',
                    'data': function(data){
                        // Read values
                        var from_date = $('#search_fromdate').val();
                        var to_date = $('#search_todate').val();

                        // Append to data
                        data.searchByFromdate = from_date;
                        data.searchByTodate = to_date;
                    }
                },
                "language": //把文字變為中文
                {  
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                },
                'columns': [
                    { data: 's_cnum' },
                    { data: 's_name' },
                    { data: 's_ic' },
                    { data: 's_date' },
                    { data: 's_reward_fine' },
                    { data: 's_reward_status' },
                ],
                dom: 'lBfrtip',
                    buttons: [
                        'excel', {
                        extend: 'csv',
                        text: 'CSV',
                        bom : true}
                    ],  
                    'select': {
                        'style': 'multi'
                    },
                    'order': [[3, 'desc']]            
            });

			$('#btn_search').click(function(){
                dataTable.draw();
            });
        });  
            
            
    </script>
