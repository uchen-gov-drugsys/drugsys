        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><button id='yes' class="btn btn-default"style="padding:0px 0px;" >建立新專案</button></li>
                    <li><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#exampleModal">加入舊專案</button></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                            <div class="panel panel-default">
                                <div class="panel-heading list_view">
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 案件編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 身份證
                                    <input type="checkbox" name="list" data-target ="3" checked> 犯嫌人姓名
                                    <input type="checkbox" name="list" data-target ="4" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="5" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="6" checked> 查獲單位
                                    <input type="checkbox" name="list" data-target ="7" checked> 持有毒品
                                    <input type="checkbox" name="list" data-target ="8" checked> 淨重
                                    <input type="checkbox" name="list" data-target ="9" checked> 驗後餘重
                                    <input type="checkbox" name="list" data-target ="10" checked> 純質淨重
                                    <input type="checkbox" name="list" data-target ="11" checked> 犯罪手法
                                    <input type="checkbox" name="list" data-target ="12" checked> 退回次數
                                </div>
                            <?php echo form_open_multipart('cases/addRewardProject2','id="sp_checkbox"') ?>          
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>舊專案</label>
                                        <?php echo form_dropdown('rp_num',$opt ,'', 'class="form-control" id="sel"')?>                                                            
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button id='no' class="btn btn-default" >加入舊專案</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                            </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){

            // Datapicker 
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd'
            });
    
            var table = $('#table1').DataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                //"order": [[ 6, "desc" ]],
                'ajax': {
                    'url':'<?php echo base_url("Cases/rewardApplication")?>',
                    'data': function(data){
                        // Read values
                        var from_date = $('#search_fromdate').val();
                        var to_date = $('#search_todate').val();

                        // Append to data
                        data.searchByFromdate = from_date;
                        data.searchByTodate = to_date;
                    }
                },
                "language": //把文字變為中文
                {  
                    "sProcessing": "處理中...",  
                    "sLengthMenu": "顯示 _MENU_ 項結果",  
                    "sZeroRecords": "沒有匹配結果",  
                    "sInfo": "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",  
                    "sInfoEmpty": "顯示第 0 至 0 項結果，共 0 項",  
                    "sInfoFiltered": "(由 _MAX_ 項結果過濾)",  
                    "sInfoPostFix": "",  
                    "sSearch": "搜索:",  
                    "sUrl": "",  
                    "sEmptyTable": "表中數據為空",  
                    "sLoadingRecords": "載入中...",  
                    "sInfoThousands": ",",  
                    "oPaginate": {  
                        "sFirst": "首頁",  
                        "sPrevious": "上頁",  
                        "sNext": "下頁",  
                        "sLast": "末頁"  
                    }
                },
                'columns': [
                    { data: 's_cnum' },
                    { data: 's_ic' },
                    { data: 's_name' },
                    { data: 's_date' },
                    { data: 'address' },
                    { data: 's_office' },
                    { data: 'ddc_ingredient'},
                    { data: 'ddc_ingredient'},
                    { data: 'ddc_ingredient'},
                    { data: 'ddc_ingredient'},
                    { data: 's_CMethods' },
                    { data: 's_reward_status' },
                ]
              'columnDefs': [
                 {
                    'orderable': false,
                    'targets': [9],
                 },
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']]
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

            $('#btn_search').click(function(){
                table.draw();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
    });
    </script>
