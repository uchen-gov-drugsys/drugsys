        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- /.row -->

                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
									<div class="row">
										<div class="col-md-6">
											案件修改：<?php echo $row->c_num; ?>
										</div>
										<div class="col-md-6 text-right">
											<input type="button" value="儲存" class="btn btn-warning btn-sm"   form="newcases" id="list">
										</div>
									</div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <?php echo form_open_multipart('cases/casesUpdate','id="casesedit"') ?>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>查獲單位 <span class="text-danger">*</span></label>
                                                        <?php echo form_error('s_office');?>
                                                        <?php echo form_input('s_office',$row->s_office, 'class="form-control" readonly')?>  
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>案件編號 <span class="text-danger">*</span></label>
                                                        <?php echo form_error('c_num'); ?>
                                                        <?php echo form_input('c_num',$row->c_num, 'class="form-control" id="cnum" readonly')?>
                                                        <input id="link" type="hidden" name="link" value=" ">  
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>查獲時間 <span class="text-danger">*</span></label>
                                                        <?php echo form_input('s_date',(isset($row->s_date))?((strlen($row->s_date) > 7 && $row->s_date != '0000-00-00')?str_pad(((int)substr($row->s_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($row->s_date, 5, 2).substr($row->s_date, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md'), 'class="rcdate form-control" id="date"')?>   
														<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>收案單位 <span class="text-danger">*</span></label>
                                                        <?php if($row->r_office == "保安警察大隊" || $row->r_office == "捷運警察隊" || $row->s_office == "特勤中隊"){
                                                            echo form_dropdown('r_office',$options ,$row->r_office , 'class="form-control" id="sel"');
                                                        }                         
                                                        else
                                                            echo form_input('r_office',$row->r_office, 'class="form-control" readonly')?> 
                                                    </div>
												</div>
                                                <div class="row">  
                                                    <div class="form-group col-md-6">
                                                        <label>查獲地點 <span class="text-danger">*</span></label>
                                                        <div id="zipcode">                    
                                                        </div>
                                                        <div class="form-group">
                                                            <?php echo form_error('r_address');?>
                                                            <?php echo form_input('r_address',$row->r_address, 'class="form-control"  placeholder="XX路XX門牌"')?>    
                                                        </div> 
                                                    </div>
													<div class="form-group col-md-6">
                                                        <label>偵破過程 <span class="text-danger">*</span></label>
                                                        <?php echo form_dropdown('detection_process',$options1 , $row->r_office , 'class="form-control"  id="sel"')?>  
														<label>營業場所名稱</label>
                                                        <?php echo form_input('s_place',$row->s_place, 'class="form-control"  placeholder="例:XX旅館 / XX酒店"')?>   
                                                    </div>
												</div>
                                                <div class="row">  
                                                    <div class="form-group col-md-4">
                                                        <label>戶役政/筆錄/搜扣</br></label>
                                                        <?php 
                                                            if($row->doc_file == ""){
																echo '<br>未上傳';
                                                                echo form_upload('doc_file');
                                                                
                                                            }
                                                            else {
                                                                echo anchor_popup('uploads/' . $row->doc_file, '</br>下載戶役政/筆錄/搜扣');
                                                                echo form_hidden('doc_file',$row->doc_file);
                                                            }
                                                        ?>    
                                                        <span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制5MB</small> </span>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>其他檔案(逮捕通知書/拘票/同意書)</br></label>
                                                        <?php 
                                                            if($row->other_doc == ""){
                                                                echo '<br>未上傳<br>';
                                                                echo form_upload('other_doc');
                                                            }
                                                            else {
                                                                echo anchor_popup('uploads/' . $row->other_doc, '下載其他檔案(逮捕通知書/拘票/同意書)</br>');
                                                                echo form_hidden('other_doc',$row->other_doc);
                                                            }
                                                        ?>    
                                                        <span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制5MB</small> </span>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>毒品照片(所有)</br></label>
                                                        <?php 
                                                            if($row->drug_pic == ""){
                                                                echo '<br>未上傳';
                                                                echo form_upload('drug_pic');
                                                            }
                                                            else {
                                                                echo anchor_popup('uploads/' . $row->drug_pic, '</br>毒品照片');
                                                                echo form_hidden('drug_pic',$row->drug_pic);
                                                            }
                                                        ?>
                                                        <span class="text-danger"><small>限上傳jpg,jpeg,png,gif,pdf,doc,docx類型之檔案且檔案大小限制5MB</small> </span>   
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    
                                                </div>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
						<div class="col-lg-6">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6">犯嫌清單</div>
                                        <div class="col-md-6 text-right">
                                            <label style="float:right"><input id="susp" style="float: left;" class="btn btn-default" type="button" value="新增" /></label>               
                                        </div>                                                       
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6">毒品證物與初驗結果</div>
                                        <div class="col-md-6 text-right">
                                            <label style="float:right"><input id="drug" style="float: left;" class="btn btn-default" type="button" value="新增" /></label>             
                                        </div>                                                       
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $drug_table;?>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
			$(".rcdate").change(function(){
				let strlen = $(this).val().length;
				if(strlen < 7 || strlen < "7")
				{
					if(strlen != 0)
						$(this).val("0"+$(this).val());
				}
				else
				{
					return false;
				}
					
			});
			$(".rcdate").keypress(function(){
				if($(this).val().length >= 7)
					return false;
			});
            $("#zipcode").twzipcode({
                "zipcodeIntoDistrict": true,
				"css": ["city form-control", "district form-control"],
                //'displayType'  : '',
                'countyName'   : 'r_county',   
                'districtName' : 'r_district', 
                'zipcodeName'  : 'r_zipcode',
                'zipcodeSel'   : <?php echo $row->r_zipcode ?>
            });         
            $(document).ready(function(){
                $("#list").click(function (){
                    $("#link").val('cases/listCases');
                    //alert("Submitted");
                    $("#casesedit").submit();
                });
                $("#drug").click(function (){
                    $("#link").val('cases/new_drug/'+$("#cnum").val());
                    //alert($("#link").val());
                    $("#casesedit").submit();
                });
                $("#susp").click(function (){
                    $("#link").val('cases/newsuspect/'+$("#cnum").val());
                    //alert($("#link").val());
                    $("#casesedit").submit();
                });
                $( "#casesedit" ).validate({
                    rules: {
                        r_address: {
                            required: true,
                        },
                        s_date: {
                            required: true,
                        },
                        r_district: {
                            required: true
                        },
                        'files[]': {
                            extension: "pdf|doc|docx"
                        },
                        'drugpic': {
                            extension: "jpg|jpeg|png|gif"
                        },
                    },
                    messages: {
                        r_address: {
                            required: "請輸入必填項目",
                        },
                        r_district: {
                            required: "請選擇必填項目",
                        },
                        'files[]': {
                            extension: "只接受pdf|doc|docx類型",
                        },
                        'drugpic': {
                            extension: "只接受jpg|jpeg|png|gif"
                        },
                    }
                });        
                // $('#date').datepicker({//bootstrap datepicker 1.9v 民國修改版
                //     format: "yyyy-mm-dd", //twy為民國
                //     autoclose: true,
                //     startDate: "today",
                //     todayHighlight: true,
                //     language: 'zh-TW'      
                // });
            });            
        </script>
    </body>
</html>
