        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h4><?php //echo $title;?></h4></li>
                    <li><a><input type="submit" value="儲存" class="btn btn-warning"  form="newcases" id="list"></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- /.row -->
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a><input type="submit" value="儲存" class="btn btn-success"  form="newcases" id="list"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    案件新增
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <?php echo form_open_multipart('cases/createcases','id="newcases"') ?>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>查獲單位 <span class="text-danger">*</span></label>
                                                        <?php echo form_error('s_office');?>
                                                        <?php echo form_input('s_office',$row->s_office, 'class="form-control" readonly')?> 
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>案件編號 <span class="text-danger">*</span></label>
                                                        <?php echo form_error('c_num');?>
                                                        <?php echo form_input('c_num',$c_num, 'class="form-control" id="cnum" readonly')?>                            
                                                        <input id="link" type="hidden" name="link" value=" "> 
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>查獲時間 <span class="text-danger">*</span><?php echo form_error('s_date');?></label>
                                                        <?php echo form_input('s_date',$row->s_date, 'class="rcdate form-control" id="date"')?>                           
														<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                                        <!--input name="s_date" value="<?php //echo set_value('s_date');?>"class="form-control" id="date" type="date" -->
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>收案單位 <span class="text-danger">*</span></label>
                                                        <?php if($row->r_office == "保安警察大隊" || $row->r_office == "捷運警察隊" || $row->s_office == "特勤中隊"){
                                                            echo form_dropdown('r_office',$opt ,$row->r_office , 'class="form-control" id="sel"');
                                                        }                                                                    
                                                        else
                                                            echo form_input('r_office',$row->r_office, 'class="form-control" readonly')?> 
                                                    </div>
												</div>
												<div class="row">
                                                    <div class="form-group col-md-6">
                                                         <label>查獲地點 <span class="text-danger">*</span><?php echo form_error('r_zipcode');?></label>
                                                        <div id="zipcode">       
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <input style="text-transform: uppercase;" name="r_address" value="<?php echo set_value('r_address');?>" class="form-control" placeholder="XX路XX門牌">                      <label><?php echo form_error('r_address');?></label>                                          
                                                        </div> 
                                                        
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>偵破過程 <span class="text-danger">*</span></label>
                                                        <select name="detection_process" id="sel" class="form-control">
                                                            <option value="路檢攔查">路檢攔查</option>
                                                            <option value="臨檢">臨檢</option>
                                                            <option value="專案勤務(無令狀)">專案勤務(無令狀)</option>
                                                            <option value="毒品調驗人口">毒品調驗人口</option>
                                                            <option value="拘票拘提">拘票拘提</option>
                                                            <option value="持票搜索">持票搜索</option><!-- 可能 要與拘票拘提合併為拘提/搜索 -->
                                                            <option value="通知到案">通知到案</option>
                                                            <option value="借訊(提)">借訊(提)</option>
                                                            <option value="報案(110)">報案(110)</option>
                                                            <option value="99">其他</option>
                                                        </select>
														<label>營業場所名稱</label> 
                                                        <input style="text-transform: uppercase;" name="s_place" class="form-control" placeholder="例:XX旅館 / XX酒店"> 
                                                    </div>
												</div>
												<div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label>戶役政/筆錄/搜扣</label><?php echo form_upload('doc_file'); ?>
                                                        <span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制5MB</small> </span>

                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>其他檔案(逮捕通知書/拘票/同意書/地檢署處分命令)</label><input type='file' name='other_doc'  >
                                                        <span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制5MB</small> </span>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>毒品照片(所有)</label><input type='file' name='drugpic'>
                                                        <span class="text-danger"><small>限上傳jpg,jpeg,png,gif,pdf,doc,docx類型之檔案且檔案大小限制5MB</small> </span>
                                                    </div>
                                                </div>
                                                
                                            <?php // echo form_submit('', 'newcases');
                                            echo form_close();?>                                  
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6">犯嫌清單</div>
                                        <div class="col-md-6 text-right">
                                            <label><input id="susp" class="btn btn-sm btn-default" type="button" value="增加" /></label>                 
                                        </div>                                                       
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">
                        
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6">毒品證物與初驗結果</div>
                                        <div class="col-md-6 text-right">
                                            <label><input id="drug"  class="btn btn-sm btn-default" type="button" value="增加" /></label>                 
                                        </div>                                                       
                                    </div>
                                    
                                    
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $drug_table;?>
                                    </div>                       
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
			$(".rcdate").change(function(){
				let strlen = $(this).val().length;
				if(strlen < 7 || strlen < "7")
				{
					if(strlen != 0)
						$(this).val("0"+$(this).val());
				}
				else
				{
					return false;
				}
					
			});
			$(".rcdate").keypress(function(){
				if($(this).val().length >= 7)
					return false;
			});
            $("#zipcode").twzipcode({
                "zipcodeIntoDistrict": true,
				"css": ["city form-control", "district form-control"],
                'countyName'   : 'r_county',   // 預設值為 county
                'districtName' : 'r_district', // 預設值為 district
                'zipcodeName'  : 'r_zipcode', // 預設值為 zipcode
                'zipcodeSel'   : <?php if(isset($row->r_zipcode)){echo $row->r_zipcode;}else{echo '100';}  ?>
            });  
            $(document).ready(function(){
                $("#list").click(function (){
                    $("#link").val('cases/listCases');
                    //alert("Submitted");
                    $("#newcases").submit();
                });
                $("#drug").click(function (){
                    $("#link").val('cases/new_drug/'+$("#cnum").val());
                    //alert($("#link").val());
                    $("#newcases").submit();
                });
                $("#susp").click(function (){
                    $("#link").val('cases/newsuspect/'+$("#cnum").val());
                    //alert($("#link").val());
                    $("#newcases").submit();
                });
                $( "#newcases" ).validate({
                    rules: {
                        r_address: {
                            required: true,
                        },
                        s_date: {
                            required: true,
                        },
                        r_district: {
                            required: true
                        },
                        'files[]': {
                            extension: "pdf|doc|docx"
                        },
                        'drugpic': {
                            extension: "jpg|jpeg|png|gif|pdf|doc|docx"
                        },
                    },
                    messages: {
                        'files[]': {
                            extension: "只接受pdf|doc|docx類型",
                        },
                        'drugpic': {
                            extension: "只接受jpg|jpeg|png|gif|pdf|doc|docx類型"
                        },
                    }
                });
                // $('#date').datepicker({//bootstrap datepicker 1.9v 民國修改版
                //     format: "yyyy-mm-dd", //twy為民國
                //     autoclose: true,
                //     todayHighlight: true,
                //     language: 'zh-TW'      
                // });
            });            
        </script>
    </body>
</html>
