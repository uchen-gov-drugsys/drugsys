        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h4><?php //echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="create_sm"></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a><input type="submit" value="儲存" class="btn btn-warning" form="create_sm"/></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-success">
                                
                                <div class="panel-heading">
                                   擁有社群帳號
                                </div>
                                <div class="panel-body">
                                <?php echo form_open_multipart('cases/createsocialmedia','id="create_sm"') ?>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <input class="btn btn-default btn-sm" type="button" onclick="CreateUploadAll()"  value="新增社群帳號" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="drugall">
                                                <div class="row" style="border-left:5px solid #337ab7;margin-left:20px;margin-bottom:10px;">
                                                    <div class="form-group col-md-2">
                                                        <label>社群軟體名稱</label>
                                                        <input name="social_media_name[]" class="form-control" >
                                                        <?php echo form_hidden('s_ic',$s_ic); ?>
                                                        <?php echo form_hidden('s_cnum',$s_cnum); ?>
                                                        <?php echo form_hidden('s_num',$s_num); ?>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號1</label>
                                                        <input name="social_media_ac1[]" class="form-control" >
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號2</label>
                                                        <input name="social_media_ac2[]" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號3</label>
                                                        <input name="social_media_ac3[]" class="form-control" >
                                                    </div>
                                                </div>
                                            </div>
                                                
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
            var all=1;
            function CreateUploadAll()
            {
                all++;
                // var tr=document.createElement('tr');
                var html = `<div class="row" id="drugrow${all}" style="border-left:5px solid ${((all%2) == 1)?'#337ab7':'#5bc0de'};margin-left:20px;margin-bottom:10px;">
                                                    <div class="form-group col-md-2">
                                                        <label>社群軟體名稱</label>
                                                        <input name="social_media_name[]" class="form-control" >
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號1</label>
                                                        <input name="social_media_ac1[]" class="form-control" >
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號2</label>
                                                        <input name="social_media_ac2[]" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號3</label>
                                                        <input  name="social_media_ac3[]" class="form-control" >
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <input style="float: left;" class="btn btn-danger" type="button" onclick="RemoveAddAll(${all})" value="刪除" />
                                                    </div>
                                                </div>`;
                // var html='<tr>'+
                //             '<td style="padding:5px">'+
                //             '<label>社群軟體名稱</label>'+
                //             '<input name="social_media_name[]" class="form-control" >'+
                //             '</td><td style="padding:5px">'+
                //             '<label>社群帳號1</label><input name="social_media_ac1[]" class="form-control" ></td>'+
                //             '<td style="padding:5px"><label>社群帳號2</label>'+
                //             '<input name="social_media_ac2[]"class="form-control"></td>'+
                //             '<td style="padding:5px"><label>社群帳號3</label><input  name="social_media_ac3[]" class="form-control" ></td>'+
                //             '<td>'+
                //             '<label><input style="float: left;" class="btn btn-danger" type="button" onclick="RemoveAddAll('+all+')"'+
                //             'value="X" /></label>'+
                //     '</td>'+
                //     '</tr>'
                // tr.innerHTML=html;
                // //div.setAttribute("class","form-control");
                // tr.setAttribute("id","drugrow"+all);
                //div.id="upDiv"+p;
                $('#drugall').append(html);
            }
            function RemoveAddAll(id)
            {
                 var div=document.getElementById('drugrow'+id);
                 var div2=document.getElementById('drugall');
                 div2.removeChild(div);
            }    
        </script>
    </body>
</html>
