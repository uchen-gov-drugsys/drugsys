        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
									<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><button type="button" class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#newcases">建立專案</button></a></li>
                    <li><a><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#publiccases">建立公示專案</button></a></li>
                    <li><a><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#exampleModal">加入舊專案</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                  
                <div class="container-fluid"> 
									<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
													<button type="button" class="btn btn-success" data-toggle="modal" data-target="#newcases">建立專案</button>
													<button class="btn btn-info" data-toggle="modal" data-target="#publiccases">建立公示專案</button>
													<button class="btn btn-default" data-toggle="modal" data-target="#exampleModal">加入舊專案</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
												<?php echo form_open_multipart('Call/updateProject','id="sp_checkbox"') ?>   
                            <div class="panel panel-primary">
																<div class="panel-heading">
                                    列表清單                      
                                </div> 
                                <div class="panel-body">
																
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
																		<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
																		<input id="s_status" type="hidden" name="s_status" value=''> 
                                		<div class="modal fade" id="newcases" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    	<div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h5 class="modal-title" id="exampleModalLabel">建立專案</h5>
                                          </div>
                                          <div class="modal-body">
                                              <div class="form-group">
                                                <label>發文本號</label>
                                                <?php echo form_input('projectname','', 'class="form-control" placeholder="請輸入發文本號(催繳專案號)"')?>                                                            
                                              </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                                            <button id='yes' type="button" class="btn btn-default" >建立專案</button>
                                          </div>
                                        </div>
																			</div>
																		</div>         
                                		<div class="modal fade" id="publiccases" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    	<div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h5 class="modal-title" id="exampleModalLabel">建立公示專案</h5>
                                          </div>
                                          <div class="modal-body">
                                              <div class="form-group">
                                                <label>發文本號</label>
                                                <?php echo form_input('projectname1','', 'class="form-control" placeholder="請輸入發文本號(催繳專案號)"')?>                                                            
                                              </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                                            <button id='public' type="button" class="btn btn-default" >建立專案</button>
                                          </div>
                                        </div>
                                    	</div>
                                		</div>         
                                		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    	<div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
                                          </div>
                                          <div class="modal-body">
                                              <div class="form-group">
                                                <label>舊專案</label>
                                                <?php echo form_dropdown('cp_name',$opt ,'', 'class="form-control" id="sel"')?>                                                            
                                              </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                                            <button id='no' type="button" class="btn btn-default" >加入舊專案</button>
                                          </div>
                                        </div>
                                    	</div>
                                		</div>  
                            </div>        
													</form>   
                        </div> 
                    </div> 
                		</div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': 0,
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              "width":"100px",'order': [[1, 'asc']],
							"language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                }, 
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert($('#s_cnum').val());
                    $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
            $("#public").click(function (){
                $("#s_status").val('2');
                //alert($("#s_status").val());
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
              var form = this;

              var rows_selected = table.column(0).checkboxes.selected();
              // Iterate over all selected checkboxes
              /*$.each(rows_selected, function(index, rowId){
                 // Create a hidden element
                 $(form).append(
                     $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'id[]')
                        .val(rowId)
                 );
              });*/
                $('#s_cnum').val(rows_selected.join(","));
                    //alert('isnnull');
                    //e.preventDefault();
                // if($.isEmptyObject($('#s_cnum').val())){
                //     alert('isnnull');
                //     e.preventDefault();
                // }
                // else{
                //     alert(rows_selected);
                //     alert('pass');
                //     e.preventDefault();
                // }
                // Output form data to a console     
                //$('#example-console-form').text($(form).serialize());
               
                // Remove added elements
               
                // Prevent actual form submission
           });
        });
</script>
