        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
										<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><button id='yes' class="btn btn-default"style="padding:0px 0px;" >確認修改</button></a></li>
                    <li><a><button class="btn btn-default"style="padding:0px 0px;" data-toggle="modal" data-target="#exampleModal">批次輸入發文日期</button></a></li>
                    <li><a><button class="btn btn-default"style="padding:0px 0px;" data-toggle="modal" data-target="#public">批次輸入送達情形</button></a></li>
                    <li><a href=<?php //echo base_url('XMLcreate/downloadcallsignxml/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >簽</button></a></li>
                    <li><a href=<?php //echo base_url('XMLcreate/downloadCallpublicxml/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >公告</button></a></li>
                    <li><a href=<?php //echo base_url('Call/exportpublicCSV/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >清冊</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                         
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>																		
                                    <h5 class="modal-title" id="exampleModalLabel">批次輸入發文日期</h5>
                                  </div>
                                  <div class="modal-body">
																		<?php echo form_open_multipart('Call/updateCPDatepublic','id="sp_checkbox"') ?> 
                                      <div class="form-group">
                                        <label>批次輸入發文日期</label>
                                        <input name="call_date" type="text" required="required" value='<?php echo (isset($call_date))?((strlen($call_date) > 7 && $call_date != '0000-00-00')?str_pad(((int)substr($call_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($call_date, 5, 2).substr($call_date, 8, 2):((int)date('Y') - 1911).date('md')):''?>' class="form-control rcdate" >                 
																				<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>                                                     
                                        <?php echo form_hidden('call_no',$fpid)?>                                                            
                                      </div>
                        						</form>    
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                                    <button  class="btn btn-default" >確定</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                        
                            <div class="modal fade" id="public" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h5 class="modal-title" id="exampleModalLabel">批次輸入送達情形</h5>
                                  </div>
                                  <div class="modal-body">
																			<?php echo form_open_multipart('Call/updatepublicdate','id="sp_checkbox"') ?>  
																				<div class="form-group">
																					<label>輸入送達日期</label>
																					<input name="call_date" type="text" required="required" value='<?php echo (isset($call_delivery_date))?((strlen($call_delivery_date) > 7 && $call_delivery_date != '0000-00-00')?str_pad(((int)substr($call_delivery_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($call_delivery_date, 5, 2).substr($call_delivery_date, 8, 2):((int)date('Y') - 1911).date('md')):''?>' class="form-control rcdate" >     
																					<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>                                                    
																					<?php echo form_hidden('call_no',$fpid)?>                                                            
																				</div>
																			</form> 
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                                    <button  class="btn btn-default" >確定</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                   
                <div class="container-fluid"> 
									<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote>
                                <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>                                             
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
													<button id='yes' class="btn btn-warning" >確認修改</button>
													<button class="btn btn-default" data-toggle="modal" data-target="#exampleModal">批次輸入發文日期</button>
													<button class="btn btn-default" data-toggle="modal" data-target="#public">批次輸入送達情形</button>
													<a href=<?php echo base_url('XMLcreate/downloadcallsignxml/'.$sp)?>><button type="button" class="btn btn-default" >簽</button></a>
													<a href=<?php echo base_url('XMLcreate/downloadCallpublicxml/'.$sp)?>><button type="button" class="btn btn-default">公告</button></a>
													<a href=<?php echo base_url('Call/exportpublicCSV/'.$sp)?>><button type="button" class="btn btn-default">清冊</button></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
																<div class="panel-heading">
                                    批次編輯                      
                                </div>      
                                <div class="panel-body">
																		<?php echo form_open_multipart('Call/updateCP','id="sp_checkbox"') ?> 
																			<div class="table-responsive">
																					<?php echo $s_table;?>
																			</div>  
																			<input id="s_cnum" type="hidden" name="s_cnum" value=''> 
																			<input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
																			<?php echo form_hidden('call_projectnum',$fpid)?>                                                            
																			<input id="s_status" type="hidden" name="s_status" value=''> 
																		</form>                     
                                </div>
                                
                           </div>
                            <!-- /.panel -->
                        </div>
                        
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
					$(".rcdate").change(function(){
							let strlen = $(this).val().length;
							if(strlen < 7 || strlen < "7")
							{
									if(strlen != 0)
											$(this).val("0"+$(this).val());
							}
							else
							{
									return false;
							}
									
					});
					$(".rcdate").keypress(function(){
							if($(this).val().length >= 7)
									return false;
					});

           var table = $('#table1').DataTable({
               "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    },
                ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']]
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
    });
		function listzipcode(obj, wrap) {
				$wrap = wrap;
				let formdata = new FormData();
				formdata.append('addr', obj.val());
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url("disciplinary_c/getZipCode") ?>',
				dataType: 'json',
						// data: {'addr': obj.val()},
						data: formdata,
				processData: false,
				contentType: false,
				cache: false,
				complete: function (resp) {
								// console.log(resp)						
								$($wrap).val(resp.responseJSON.data * 1);
		
				}
			});

		}
    </script>
