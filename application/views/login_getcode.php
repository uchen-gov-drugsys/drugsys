
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel" style="background:none !important;">
                    <?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'panel-title',
                            'width' => '180',
                            'height' => '180',
                            'style' => 'display:block; margin:auto;box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;border-radius: 100%;',
                                    );     
                                ?>
                        <?php echo img($image_properties); ?>
						<h4 class="text-center" style="color:#fff;letter-spacing:5px;">忘記密碼</h4>
                        <div class="panel-body">
                            <form action=<?php echo base_url("login/getcode") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <fieldset>
                                    <div class="form-group">
										<small style="color: rgba(255,255,255,0.7);">登入類型</small>
                                        <select name="login_type" id="sel" class="form-control">
                                            <option value="0">ID登入</option>
                                            <option value="1">身份證登入</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input name='uid' class="form-control" value="<?php echo $uid?>" placeholder="請輸入賬號" required>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="確認輸入">
                                </fieldset>
                            </form>
                                    <label  style='color:red;'>請聯繫承辦人獲得驗證碼</label>
                                    <div class="form-group" style='color:red;'><label>
                                    <?php 
                                        if(isset($code)) {
                                            foreach ($code as $user){
                                                echo $user->em_officeAll.$user->em_lastname.$user->em_centername.$user->em_firstname.$user->em_job.':'.$user->em_mailId.'<br>';
                                            }
                                        }
                                        if(isset($com)) {
                                            foreach ($com as $user){
                                                echo $user->em_officeAll.$user->em_lastname.$user->em_centername.$user->em_firstname.$user->em_job.':'.$user->em_mailId.'<br>';
                                            }
                                        }
                                        if(isset($baoda)) {
                                            foreach ($baoda as $user){
                                                echo $user->em_officeAll.$user->em_lastname.$user->em_centername.$user->em_firstname.$user->em_job.':'.$user->em_mailId.'<br>';
                                            }
                                        }
                                    ?>
                                    
                                    </label></div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <a href=<?php echo base_url("login/index") ?> class="btn btn-lg btn-success btn-block" type="submit" name="submit" id="submit" value="返回">返回</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<style>
    body{
        /* background: linear-gradient(to left, #13547a 0%, #80d0c7 100%); */
        background-image: linear-gradient(to left, #29323c 0%, #485563 100%);
    }
    #sel {
        background: none;
        color: #fff;
        border: none;
        border-bottom: 1px solid #eee;
        border-radius: 0px;
        box-shadow: inset 0 5px 5px rgb(0 0 0 / 8%);
    }
    input.form-control {
        background: rgba(255,255,255,0.1);
        color: #fff;
        border: none;
        border-bottom: 1px solid #eee;
        border-radius: 0px;
        box-shadow: none;
        height: 40px;
        font-size: 14px;
        line-height: 20px;
    }
    input[type="submit"],a[type="submit"] {
        background: rgba(255,255,255,0.1);
        color: #fff;
        border: none;
        font-size: 14px;
        border-radius:0px;
        height: 40px;
    }
    input[type="submit"]:hover,a[type="submit"]:hover {
        background-color: rgba(255,255,255,1);
        color:#333;
    }
</style>
<script>
function validate() {
    var pw1 = document.getElementById("pw1").value;
    var pw2 = document.getElementById("pw2").value;
    if(pw1 == pw2) {
        document.getElementById("tishi").innerHTML="<font color='green'>两次密码相同</font>";
        document.getElementById("submit").disabled = false;
    }
    else {
        document.getElementById("tishi").innerHTML="<font color='red'>两次密码不相同</font>";
        document.getElementById("submit").disabled = true;
    }
}
</script>
