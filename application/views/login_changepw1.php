
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default panel-heading">
                    <?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'panel-title',
                            'width' => '180',
                            'height' => '180',
                            'style' => 'display:block; margin:auto;',
                                    );     
                                ?>
                        <?php echo img($image_properties); ?>
                        <div class="panel-heading">
                            <h3 style="text-align: center;" class="panel-title">首次登入請先輸入帳號與更改密碼</h3>
                        </div>
                        <div class="panel-body">
                            <form action=<?php echo base_url("login/first_pw") ?> id='first_pw' enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <fieldset>
                                    <div class="form-group">
                                        <input name='u_id' class="form-control" required placeholder="請輸入帳號" id="u_id" value="">
                                    </div>
                                    <div class="form-group">
                                        <input name='u_pw' class="form-control" required placeholder="請輸入新密碼" id="pw1" name="password" type="password" value="">
                                    </div>
                                    <div class="form-group">
                                        <input name='u_pw' class="form-control" placeholder="確認新密碼" required name="password" type="password" id="pw2" onkeyup="validate1()" value=""><span id="tishi"></span>
                                    </div>
                                    <div style='color:red;'><?php echo $error ?></div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" id="submit" value="修改">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>
$(document).ready(function(){
    $( "#first_pw" ).validate({
        rules: {
            'u_id': {
                required: true,
                minlength: 1
            },
        },
        messages: {
            'u_id': {
                required: "此欄位不得為空",
                minlength: "請完整輸入",
            },
        }
    });
});
function validate1() {
    var pw1 = document.getElementById("pw1").value;
    var pw2 = document.getElementById("pw2").value;
    if(pw1 == pw2) {
        document.getElementById("tishi").innerHTML="<font color='green'>兩次密碼相同</font>";
        document.getElementById("submit").disabled = false;
    }
    else {
        document.getElementById("tishi").innerHTML="<font color='red'>兩次密碼不相同</font>";
        document.getElementById("submit").disabled = true;
    }
}
</script>
