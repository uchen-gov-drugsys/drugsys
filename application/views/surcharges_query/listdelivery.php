        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row"><br>
                        <div class="col-lg-11">
                            <div class="panel panel-default">
                            <form action= <?php echo base_url('Surcharges/updatedelivery')?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type='hidden' name="s_cnum" value=''> 
                                <input id="s_cnum1" type='hidden' name="s_cnum1" value=''> 
                                <input id="s_status" type='hidden' name="s_status" value=''> 
                            </form>   
                           </div>
                            <!-- /.panel -->
                        </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <form action= <?php echo base_url('Surcharges/uploaddeliverydoc') ?>  id="uploaddeliverydoc" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">上傳送達證書</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          <div class="form-group">
                                            <label>上傳送達證書(可多筆上傳)</label>
                                            <?php echo form_upload('files[]','','multiple'); ?>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button id='no' class="btn btn-default" >確認上傳</button>
                                      </div>
                                    </div>
                                </div>
                                </form>
                            </div>         
                        <!-- /.col-lg-12 -->
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <a class="media" href="test.pdf"></a>    
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>                    
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': 0,
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              "width":"100px",'order': [[1, 'asc']]
           });
           
            /*$('#table1 tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
            } );*/
            
            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
            $("#yes1").click(function (){
                $("#s_status").val('2');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });

           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
            var form = this;

            var rows_selected = table.column(0).checkboxes.selected();
                $('#example-console-rows').text(rows_selected.join(","));
                $('#s_cnum').val(rows_selected.join(","));
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
              
           });
           
            $( "#uploaddeliverydoc" ).validate({
                    rules: {
                        'files[]': {
                            extension: "pdf"
                        },
                    },
                    messages: {
                        'files[]': {
                            extension: "只接受pdf類型",
                        },
                    }
                });        
        });
</script>
