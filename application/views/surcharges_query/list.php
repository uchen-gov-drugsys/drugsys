        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row"><br>
                        <div class="col-lg-11">
                            <div class="panel panel-default">
                            <?php echo form_open_multipart('Surcharges/updatesurchargesproject','id="sp_checkbox"') ?>          
                                <div class="panel-heading list_view">
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 罰鍰列管編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 怠金依據文號
                                    <input type="checkbox" name="list" data-target ="3" checked> 應講習時間
                                    <input type="checkbox" name="list" data-target ="4" checked> 應講習地點
                                    <input type="checkbox" name="list" data-target ="5" checked> 發文日期
                                    <input type="checkbox" name="list" data-target ="6" checked> 發文字號
                                    <input type="checkbox" name="list" data-target ="7" checked> 移送分局
                                    <input type="checkbox" name="list" data-target ="8" checked> 依據單位
                                    <input type="checkbox" name="list" data-target ="9" checked> 依據日期
                                    <input type="checkbox" name="list" data-target ="10" checked> 依據字號
                                    <input type="checkbox" name="list" data-target ="11" checked> 受處分人姓名
                                    <input type="checkbox" name="list" data-target ="12" checked> 身分證號
                                    <input type="checkbox" name="list" data-target ="13" checked> 聯絡電話
                                    <input type="checkbox" name="list" data-target ="14" checked> 戶籍地址
                                    <input type="checkbox" name="list" data-target ="15" checked> 現住地址
                                    <input type="checkbox" name="list" data-target ="16" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="17" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="18" checked> 查獲單位
                                    <input type="checkbox" name="list" data-target ="19" checked> 罰鍰(萬元)
                                    <input type="checkbox" name="list" data-target ="20" checked> 講習時數
                                    <input type="checkbox" name="list" data-target ="21" checked> 罰鍰送達時間
                                    <input type="checkbox" name="list" data-target ="22" checked> 講習當時在監
                                    <input type="checkbox" name="list" data-target ="23,24" checked> 現在在監情形
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                                <div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="othernewLabel">批次繳款日期</h5>
                                        <input type='date' name='sp_date' class='form-control'/>
                                        <label>專案類型</label>
                                        <select id="sel" name='sp_type' class="form-control">
                                            <option value="郵務送達">郵務送達</option>
                                            <option value="囑託監所送達">囑託監所送達</option>
                                            <option value="公示送達">公示送達</option>
                                            <option value="不裁罰">不裁罰</option>
                                        </select>
                                      </div>
                                      <div id="sel1" class="modal-body">
                                      </div>
                                      <div class="modal-footer">
                                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button id='yes'  type="button" class="btn btn-default" >建立專案</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="modal fade" id="addcases" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="othernewLabel">選擇專案</h5>
                                                <?php echo form_dropdown('sp_num',$opt ,'', 'class="form-control" id="sel"')?>                                                            
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button id='no'  type="button" class="btn btn-default" >加入舊專案</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <script>
            /*$(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });*/
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': 0,
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              "width":"100px",'order': [[2, 'asc']]
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert($("#s_status").val());
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
              var form = this;

              var rows_selected = table.column(0).checkboxes.selected();
                $('#example-console-rows').text(rows_selected.join(","));
                $('#s_cnum').val(rows_selected.join(","));
              
                // Output form data to a console     
                //$('#example-console-form').text($(form).serialize());
               
                // Remove added elements
                $('input[name="id\[\]"]', form).remove();
               
                // Prevent actual form submission
                //e.preventDefault();
           });
            
            $("#sel").change(function(){
            switch ($(this).val()){
                case "郵務送達" : 
                    $("#sel1 select").remove();
                break;
                case "囑託監所送達" : 
                    $("#sel1 select").remove();
                break;
                case "公示送達" : 
                    $("#sel1 select").remove();
                break;
                case "不裁罰" : 
                    $("#sel1 select").remove();
                    var array = [ "在監","未合法送達","其他" ];
                    //利用each遍歷array中的值並將每個值新增到Select中
                    $("#sel1").append("<select name='sp_reason1' class='form-control'>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                        $("#sel1").append("<select name='sp_reason2' class='form-control'>"
                        +"<option selected value=NULL>原因2</option>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                break;
            }});
        });

    </script>

    </body>
</html>
