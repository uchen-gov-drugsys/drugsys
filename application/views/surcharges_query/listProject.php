        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row"><br>
                        <div class="col-lg-11">
                            <div class="panel panel-default">
                            <?php echo form_open_multipart('Surcharges/updatestatus_ready','id="sp_checkbox"') ?>          
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <script>
            /*$(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });*/
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': 0,
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              "width":"100px",'order': [[2, 'asc']]
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert($("#s_status").val());
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
              var form = this;

              var rows_selected = table.column(0).checkboxes.selected();
                $('#example-console-rows').text(rows_selected.join(","));
                $('#s_cnum').val(rows_selected.join(","));
              
                // Output form data to a console     
                //$('#example-console-form').text($(form).serialize());
               
                // Remove added elements
                $('input[name="id\[\]"]', form).remove();
               
                // Prevent actual form submission
                //e.preventDefault();
           });
            
            $("#sel").change(function(){
            switch ($(this).val()){
                case "郵務送達" : 
                    $("#sel1 select").remove();
                break;
                case "囑託監所送達" : 
                    $("#sel1 select").remove();
                break;
                case "公示送達" : 
                    $("#sel1 select").remove();
                break;
                case "不裁罰" : 
                    $("#sel1 select").remove();
                    var array = [ "在監","未合法送達","其他" ];
                    //利用each遍歷array中的值並將每個值新增到Select中
                    $("#sel1").append("<select name='sp_reason1' class='form-control'>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                        $("#sel1").append("<select name='sp_reason2' class='form-control'>"
                        +"<option selected value=NULL>原因2</option>"
                        +"<option value='在監'>在監</option>"
                        +"<option value='未合法送達'>未合法送達</option>"
                        +"<option value='其他'>其他</option>"
                        +"</select>");
                break;
            }});
        });

    </script>

    </body>
</html>
