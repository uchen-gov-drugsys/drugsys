<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>403 Forbidden</title>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Press+Start+2P');
		  @import url('https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@300;400;500;700;900&display=swap');
        
        html,body{
           width: 100%;
           height: 100%;
           margin: 0;
        }
        
        *{
           font-family: 'Press Start 2P', cursive;
           box-sizing: border-box;
        }
        #app{
           padding: 1rem;
           background: black;
           display: flex;
           height: 100%;
           justify-content: center; 
           align-items: center;
           color: #54FE55;
           text-shadow: 0px 0px 10px ;
           font-size: 6rem;
           flex-direction: column;
           
        }
        #app .txt {
              font-size: 1.8rem;
           }
        @keyframes blink {
            0%   {opacity: 0}
            49%  {opacity: 0}
            50%  {opacity: 1}
            100% {opacity: 1}
        }
        
        .blink {
           animation-name: blink;
            animation-duration: 1s;
           animation-iteration-count: infinite;
        }
		  button {
			  margin-top: 50px;
			  padding: 15px 40px;
			  border: 2px solid #54FE55;
			  color: #54FE55;
			  background-color: transparent;
			  font-family: 'Noto Sans TC', serif;
    			font-weight: 500;
				 letter-spacing: 4.5px;
				 font-size: 16px;
		  }
		  button:hover {
			border: 2px solid transparent;
			  color: #000;
			  background-color: #54FE55;
		  }
        </style>
</head>
<body>

    <div id="app">
        <div>403</div>
        <div class="txt">
        Forbidden<span class="blink">_</span>
        </div>
		  <a href="<?php echo base_url('login/index'); ?>"><button>回登入 </button></a>
    </div>    
</body>
</html>
