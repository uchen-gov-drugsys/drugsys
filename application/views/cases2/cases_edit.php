        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- /.row -->
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            
                        </div>
                    </div>

                    <div class="row">
						<div class="col-lg-5">
							<div class="panel panel-warning">
                                <div class="panel-heading">
									<div class="row">
										<div class="col-md-6">
											<h3 class="panel-title">案件修改： <?php echo $row->c_num; ?></h3>
										</div>
										<div class="col-md-6 text-right">
											<input type="button" value="儲存" class="btn btn-warning btn-sm"  form="casesedit" id="list">
										</div>
									</div>
									
                                    
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        	<div class="col-lg-12">
                                                <?php echo form_open_multipart('Cases2/casesUpdate','id="casesedit"') ?>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>案件編號 <span class="text-danger">*</span></label>
                                                        <?php echo form_error('c_num'); ?>
                                                        <?php echo form_input('c_num',$row->c_num, 'class="form-control" id="cnum" readonly')?>  
                                                        <input id="link" type="hidden" name="link" value=" ">   
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>查獲單位 <span class="text-danger">*</span></label>
                                                        <?php echo form_error('s_office');?>
                                                        <?php echo form_input('s_office',$row->s_office, 'class="form-control" ')?>  
                                                    </div>
												</div>
												<div class="row">
													<div class="form-group col-md-6">
                                                        <label>查獲時間 <span class="text-danger">*</span></label>
                                                        <?php echo form_input('s_date', (isset($row->s_date))?((strlen($row->s_date) > 7 && $row->s_date != '0000-00-00')?str_pad(((int)substr($row->s_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($row->s_date, 5, 2).substr($row->s_date, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md'), 'class="rcdate form-control"  id="date"')?> 
														<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span> 
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>收案單位 <span class="text-danger">*</span></label>
                                                        <?php if($row->r_office == "保安警察大隊" || $row->r_office == "捷運警察隊" || $row->s_office == "特勤中隊"){
                                                            echo form_dropdown('r_office',$opt ,$row->r_office , 'class="form-control" id="sel"');
                                                        }                   
                                                        else
                                                            echo form_input('r_office',$row->r_office, 'class="form-control" readonly')?> 
                                                    </div>
												</div>
                                                <div class="row">    
                                                    <div class="form-group col-md-6">
                                                        <label>查獲地點 <span class="text-danger">*</span></label>
                                                        <div id="zipcode">             
                                                        </div>
                                                        <div class="form-group">
                                                        <?php echo form_input('r_address',$row->r_address, 'class="form-control" placeholder="XX路XX門牌"')?>  														
                                                        <?php echo form_error('r_address');?>
                                                        </div> 
                                                    </div>

													<div class="form-group col-md-6"  id ="sel_td" >
                                                        <label>偵破過程 <span class="text-danger">*</span></label>
                                                        <?php echo form_dropdown('detection_process',$options1 , $row->r_office , 'class="form-control" id="sel"')?>   
														<label>營業場所名稱</label>
                                                        <?php echo form_input('s_place',$row->s_place, 'class="form-control" placeholder="例:XX旅館 / XX酒店"')?>
                                                    </div>
												</div>
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label>戶役政/筆錄/搜扣</br></label>
                                                        <?php 
                                                            if($row->doc_file == ""){
                                                                echo form_upload('doc_file');
                                                            }
                                                            else {
                                                                echo anchor_popup('uploads/' . $row->doc_file, '</br>下載戶役政/筆錄/搜扣') . '<br/>';
                                                                //echo form_hidden('doc_file',$row->doc_file);
                                                            }
                                                        ?> 
														<span class="text-danger"><small>限上傳pdf, doc, docx類型之檔案且檔案大小限制5MB</small> </span>   
                                                    </div>
                                                    <div class="form-group col-md-4 hidden">
                                                        <label>其他檔案(逮捕通知書/拘票/同意書)</br></label>
                                                        <?php 
                                                            if($row->other_doc == ""){
                                                                echo form_upload('other_doc');
                                                            }
                                                            else {
                                                                echo anchor_popup('uploads/' . $row->other_doc, '下載其他檔案(逮捕通知書/拘票/同意書)</br>') . '<br/>';
                                                                //echo form_hidden('other_doc',$row->other_doc);
                                                            }
                                                        ?> 
														 <span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制5MB</small> </span>
                                                    </div>
                                                    <div class="form-group col-md-4 hidden">
                                                        <label>毒品照片(所有)</br></label>
                                                        <?php 
                                                            if($row->drug_pic == ""){
                                                                echo form_upload('drug_pic');
                                                            }
                                                            else {
                                                                echo anchor_popup('uploads/' . $row->drug_pic, '</br>毒品照片') . '<br/>';
                                                                //echo form_hidden('drug_pic',$row->drug_pic);
                                                            }
                                                        ?>    
														<span class="text-danger"><small>限上傳jpg, jpeg, png, gif, pdf, doc, docx類型之檔案且檔案大小限制5MB</small> </span>
                                                    </div>
                                                </div>
												<?php echo form_close(); ?>
											</div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
						</div>
                        <div class="col-lg-7">
							<div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-6"><h3 class="panel-title">犯嫌證物清單</h3></div>
                                        <div class="col-md-6 text-right">
											<!-- <input id="susp" class="btn btn-default btn-sm" type="button" value="新增" />              -->
                                        </div>                                                       
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
										<table class="table table-bordered ">
												<thead>
													<tr>
														<th>身份證</th>
														<th>犯嫌人姓名</th>
														<th>犯罪手法</th>
														<th>照片</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<?php
														foreach ($s_table as $row) {
															echo '<tr  class="warning">
																<td>'.$row[1].'</td>
																<td>'.$row[2].'</td>
																<td>'.$row[3].'</td>
																<td>'.$row[4].'</td>
																<td>'.$row[5].'</td>
															</tr>';
															
															echo '<tr>
																<td colspan="7">';
															if(count($drug_table) > 0){
															echo	'<table class="table table-bordered ">
																		<thead>
																			<tr class="active">
																				<th>毒品編號</th>
																				<th>證物名稱</th>
																				<th>成分</th>
																				<th>初驗淨重(g)</th>
																				<th>初驗成分</th>
																				<th>檢驗成分</th>
																				<th>淨重(g)</th>
																				<th>驗後餘重(g)</th>
																				<th>純質淨重(g)</th>
																				<th>擁有人</th>
																				<th></th>
																			</tr>
																		</thead>
																		<tbody>';
																		foreach ($drug_table as $drug_row) {
																			if($row[0] == $drug_row[5] || $row[0] == $drug_row[12])
																			{
																				echo '<tr>
																					<td>'.$drug_row[0].'</td>
																					<td>'.$drug_row[1].'</td>
																					<td>'.$drug_row[2].'</td>
																					<td>'.$drug_row[3].'</td>
																					<td>'.$drug_row[4].'</td>
																					<td>'.$drug_row[7].'</td>
																					<td>'.$drug_row[8].'</td>
																					<td>'.$drug_row[9].'</td>
																					<td>'.$drug_row[10].'</td>
																					<td>'.((isset($drug_row[12]))?$drug_row[6].'(與'.$drug_row[13].'共同持有)':$drug_row[6]).'</td>
																					<td>'.$drug_row[11].'</td>
																				</tr>';
																			}
																		}
															echo 		'</tbody>
																	</table>';
															}
															else
															{
																echo '<p class="text-center">無相關證物。</p>';
															}
															echo	'</td>
															</tr>';
														}
													?>
												</tbody>
											</table>
                                        <?php //echo $s_table;?>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
			$(".rcdate").change(function(){
				let strlen = $(this).val().length;
				if(strlen < 7 || strlen < "7")
				{
					if(strlen != 0)
						$(this).val("0"+$(this).val());
				}
				else
				{
					return false;
				}
					
			});
			$(".rcdate").keypress(function(){
				if($(this).val().length >= 7)
					return false;
			});
            $("#zipcode").twzipcode({
                "zipcodeIntoDistrict": true,
				"css": ["city form-control", "district form-control"],
                'countyName'   : 'r_county',   
                'districtName' : 'r_district', 
                'zipcodeName'  : 'r_zipcode',
                'zipcodeSel'   : <?php echo $row->r_zipcode ?>
            });         
            $(document).ready(function(){
                $("#list").click(function (){
                    $("#link").val('cases2/listCases');
                    //alert("Submitted");
                    $("#casesedit").submit();
                });
                $("#drug").click(function (){
                    $("#link").val('cases2/new_drug/'+$("#cnum").val());
                    //alert($("#link").val());
                    $("#casesedit").submit();
                });
                $("#susp").click(function (){
                    $("#link").val('cases2/newsuspect/'+$("#cnum").val());
                    //alert($("#link").val());
                    $("#casesedit").submit();
                });
                $( "#casesedit" ).validate({
                    rules: {
                        r_address: {
                            required: true,
                        },
                        s_date: {
                            required: true,
                        },
                        r_district: {
                            required: true
                        },
                        'files[]': {
                            extension: "pdf|doc|docx"
                        },
                        'drug_pic': {
                            extension: "jpg|jpeg|png|gif"
                        },
                    },
                    messages: {
                        r_address: {
                            required: "請輸入必填項目",
                        },
                        r_district: {
                            required: "請選擇必填項目",
                        },
                        'files[]': {
                            extension: "只接受pdf|doc|docx類型",
                        },
                        'drug_pic': {
                            extension: "只接受jpg|jpeg|png|gif"
                        },
                    }
                });        
                // $('#date').datepicker({//bootstrap datepicker 1.9v 民國修改版
                //     format: "yyyy-mm-dd", //twy為民國
                //     autoclose: true,
                //     todayHighlight: true,
                //     language: 'zh-TW'      
                // });
            });            
        </script>
    </body>
</html>
