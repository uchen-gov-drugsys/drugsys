        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h4><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h4></li>
                    <li><a><?php //echo form_submit('', '儲存', 'class="btn btn-default" style="padding:0px 0px;"form="casesedit" id="list"');?></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>

            <div id="page-wrapper">
                <div class="container-fluid">
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?>【案件：<?php echo $cnum; ?>】</p>
								<footer>送出後案件資料才會出現在【三階移送裁罰列表】以及【證物管理系統】。</footer>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							<input id="drug" class="btn btn-success" type="button"  value="新增證物" />
							<input id="sendBT" class="btn btn-primary" type="button"  value="移送或裁罰" />
                        </div>
                    </div>
                    <div class="row">
						<div class="col-md-3">
							<div class="panel panel-warning">
								<div class="panel-heading">
									<div class="row">
										<div class="col-md-6">
										<h3 class="panel-title">案件</h3>
										</div>
										<div class="col-md-6 text-right">
											<?php echo form_submit('', '儲存', 'class="btn btn-warning btn-sm" form="casesedit" id="list"');?>
										</div>
									</div>
									
								</div>
								<div class="panel-body">
								<?php echo form_open_multipart('cases2/updateCases2','id="casesedit"') ?>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<h3>案件修改<i class="fa fa-sign-out fa-fw"></i> <?php echo anchor('cases2/edit/' . $cnum, $cnum) ?></h3>
												
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label>移送文號(移送地檢)</label>
												<input name='rm_num' class="form-control" value=<?php echo $row->rm_num?>>
												<input type="hidden" name='c_num' id='c_num' class="form-control" value=<?php echo $cnum?> >
												<input type="hidden" name='link' id='link' class="form-control" >
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label>發文時間</label>
												<!-- <input name='rm_time' class="form-control" id="date" type="date" value=<?php //echo $row->rm_time?>> --><input type="text" name="rm_time" class="rcdate form-control" value="<?php echo (isset($row->rm_time))?((strlen($row->rm_time) > 7 && $row->rm_time != '0000-00-00')?str_pad(((int)substr($row->rm_time, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($row->rm_time, 5, 2).substr($row->rm_time, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md')?>" id="date">
												<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label>毒報</label>
												<?php 
												if($row->drug_doc == "" || !isset($row->drug_doc)){
													echo form_upload('drug_doc');
												}
												else {
													echo anchor_popup('drugdoc/' . $row->drug_doc, '</br>毒報').'<a href="'.base_url('cases2/deletedrugpaper/'.$this->uri->segment(3)).'"><span class="fa fa-times-circle text-danger"></span></a>';
													echo form_hidden('drug_doc',$row->drug_doc);
												}
												?> 
												<br/>
												<span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制10MB</small> </span>
											</div>
										</div>
									</div>
								<?php echo form_close(); ?>
								</div>
							</div>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-info">
										<div class="panel-heading">
											<div class="row">
												<div class="col-md-6">
													<h3 class="panel-title">犯嫌證物清單</h3> 
												</div>
												<div class="col-md-6 text-right">
													
												</div>
											</div>
										</div>
										<div class="panel-body">
											<div class="table-responsive">
												<table class="table table-bordered ">
													<thead>
														<tr>
															<th>身份證</th>
															<th>犯嫌人姓名</th>
															<th>尿液編號</th>
															<th>檢驗級別成分</th>
															<th>尿報檔案</th>
															<th>處分書狀態</th>
															<th></th>
														</tr>
													</thead>
													<tbody>
														<?php
															foreach ($s_table as $row) {
																echo '<tr  class="warning">
																	<td>'.$row[1].'</td>
																	<td>'.$row[2].'</td>
																	<td>'.$row[3].'</td>
																	<td>'.$row[4].'</td>
																	<td>'.$row[5].'</td>
																	<td>'.$row[6].'</td>
																	<td>'.$row[7].'</td>
																</tr>';
																
																echo '<tr>
																	<td colspan="7">';
																if(count($drug_table) > 0){
																echo	'<table class="table table-bordered ">
																			<thead>
																				<tr class="active">
																					<th>毒品編號</th>
																					<th>證物名稱</th>
																					<th>成分</th>
																					<th>初驗淨重(g)</th>
																					<th>初驗成分</th>
																					<th>檢驗成分</th>
																					<th>淨重(g)</th>
																					<th>驗後餘重(g)</th>
																					<th>純質淨重(g)</th>
																					<th>擁有人</th>
																					<th></th>
																				</tr>
																			</thead>
																			<tbody>';
																			foreach ($drug_table as $drug_row) {
																				if($row[0] == $drug_row[5] || $row[0] == $drug_row[12])
																				{
																					echo '<tr>
																						<td>'.$drug_row[0].'</td>
																						<td>'.$drug_row[1].'</td>
																						<td>'.$drug_row[2].'</td>
																						<td>'.$drug_row[3].'</td>
																						<td>'.$drug_row[4].'</td>
																						<td>'.$drug_row[7].'</td>
																						<td>'.$drug_row[8].'</td>
																						<td>'.$drug_row[9].'</td>
																						<td>'.$drug_row[10].'</td>
																						<td>'.((isset($drug_row[12]))?$drug_row[6].'(與'.$drug_row[13].'共同持有)':$drug_row[6]).'</td>
																						<td>'.$drug_row[11].'</td>
																					</tr>';
																				}
																			}
																echo 		'</tbody>
																		</table>';
																}
																else
																{
																	echo '<p class="text-center">無相關證物。</p>';
																}
																echo	'</td>
																</tr>';
															}
														?>
													</tbody>
												</table>
												<?php //echo var_dump($s_table);?>
											</div>                       
										</div>
										<!-- /.panel-body -->
									</div>
									<!-- /.panel -->
								</div>
								<!-- /.col-lg-12 -->
							</div>
						</div>

						
                    </div>
					<!-- <div class="row">
						<div class="col-md-12">
							<div class="panel panel-info">
								<div class="panel-heading">
									<div class="row">
										<div class="col-md-6">
											<h3 class="panel-title">證物清單</h3> 
										</div>
										<div class="col-md-6 text-right">
											<input id="drug" class="btn btn-default btn-sm" type="button"  value="新增" />
										</div>
									</div>
								</div>
								<div class="panel-body">
								<?php //echo var_dump($drug_table);?>                      
								</div>
								
							</div>
							
						</div>
					</div> -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <script type="text/javascript">
            $(document).ready(function(){
				$(".rcdate").change(function(){
					let strlen = $(this).val().length;
					if(strlen < 7 || strlen < "7")
					{
						if(strlen != 0)
							$(this).val("0"+$(this).val());
					}
					else
					{
						return false;
					}
						
				});
				$(".rcdate").keypress(function(){
					if($(this).val().length >= 7)
						return false;
				});
                $("#list").click(function (){
                    $("#link").val('cases2/editCases2/'+$("#c_num").val());
                    //alert($("#link").val());
                    $("#casesedit").submit();
                });
                $("#drug").click(function (){
                    $("#link").val('cases2/createdrug2_num/'+$("#c_num").val());
                    //alert($("#link").val());
                    $("#casesedit").submit();
                });
                $("#susp").click(function (){
                    $("#link").val('cases2/createsusp2_num/'+$("#c_num").val());
                    //alert($("#link").val());
                    $("#casesedit").submit();
                });
                $( "#casesedit" ).validate({
                    rules: {
                        rm_num: {
                            digits: true,
                        },
                    },
                    messages: {
                    }
                });  
				$("#sendBT").click(function(){
					checkInputComplete();
				});      
            });       
			function checkInputComplete(){
				let formdata = new FormData();
				formdata.append('caseid', '<?php echo $this->uri->segment(3); ?>')
				$.ajax({            
					type: 'POST',
					url: '<?php echo base_url('Cases2/checkCasesInput') ?>',
					data: formdata,
					dataType: 'json',
					processData : false, 
					contentType: false,
					cache: false,
					success: function(resp){
						if(resp.status != 200)
						{
							alert(resp.msg);
						}
						else{
							location.href=resp.data;
						}
						
					}
				});
			}
        </script>
