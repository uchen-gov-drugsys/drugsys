<!DOCTYPE html>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
                <!--
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲12123123" class="btn btn-default" style="padding:0px 0px;" form="create_source"></a></li>
                </ul> -->
            <?php $this->load->view($nav);?>

            <div id="page-wrapper">
                <div class="container-fluid">

                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a><input type="submit" value="儲存" class="btn btn-warning"  form="create_source"></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
									<h3 class="panel-title">溯源修改</h3>
                                </div>
                                <?php echo form_open_multipart('cases2/updatesource','id="create_source"') ?> 
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <div class="row" >
                                                    <div class="form-group col-md-2">
                                                        <label>姓名</label>
                                                        <?php echo form_input('sou_name',$row->sou_name, 'class="form-control"');  ?>                                                      
                                                        <input id="sou_num" type="hidden" name="sou_num" value=<?php echo $row->sou_num;?>> 
                                                        <input id="sou_ic" type="hidden" name="sou_s_ic" value=<?php echo $row->sou_s_ic;?>> 
                                                        <input id="sou_cnum" type="hidden" name="sou_cnum" value=<?php echo $row->sou_cnum;?>>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>綽號</label>
                                                        <?php echo form_input('sou_nname',$row->sou_nname, 'class="form-control"');  ?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>特徵</label>
                                                        <?php echo form_input('sou_feature',$row->sou_feature, 'class="form-control"');  ?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>手機號碼</label>
                                                        <?php echo form_input('sou_phone',$row->sou_phone, 'class="form-control"');  ?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>毒品照片</label>
                                                        <?php 
                                                            if($row->sou_pic == ""){
                                                                echo form_upload('sourcepic');
                                                            }
                                                            else {
                                                                ?><img src=<?php echo base_url('sourcedrugpic/') . $row->sou_pic ?> alt="" width="50" height="50">
                                                                <?php echo anchor_popup('sourcedrugpic/' . $row->sou_pic, '</br>毒品照片下載');
                                                                //echo form_hidden('s_pic',$row->s_pic);
                                                            }
                                                        ?>  
														<br>
														<span class="text-danger"><small>限上傳jpg, jpeg, png, gif類型之檔案且檔案大小限制10MB</small> </span>
                                                    </div>
                                                </div>
                                                <div class="row" >
                                                    <div class="form-group col-md-2">
                                                        <label>交易時間</label>
                                                        <?php echo form_input('sou_time',$row->sou_time, 'class="form-control"');  ?> 
														<span class="text-danger"><small>(如：1100505 下午)</small> </span> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>交易地點</label>
                                                        <?php echo form_input('sou_place',$row->sou_place, 'class="form-control"');  ?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>交易金額</label>
                                                        <?php echo form_input('sou_amount',$row->sou_amount, 'class="form-control"');  ?>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>交易數量</label>
                                                        <?php echo form_input('sou_count',$row->sou_count, 'class="form-control"');  ?> 
                                                    </div>
                                                </div>
												<hr>
												<div class="row">
													<div class="col-md-12  text-right">
                                                        <label><input class="btn btn-default btn-sm" type="button" id="add"  value="增加一筆" /></label>
                                                    </div>
												</div>
                                                <div id="source_r">
													<?php
														for($i=0, $count = count($sourcesm);$i<$count;$i++) {
															echo '<div class="row"  id="row'.$i.'" style="border-left:5px solid '.((($i%2) == 1)?'#337ab7':'#5bc0de').';margin-left:20px;margin-bottom:10px;">';

															echo '<div class="form-group col-md-2">';
															echo form_hidden('sou_sm_num[]',$sourcesm[$i]->sou_sm_num);
															echo "<label>通訊軟體1</label>";
															echo form_input('sou_sm_name[]',$sourcesm[$i]->sou_sm_name , 'class="form-control"');
															echo '</div>';
															echo '<div class="form-group col-md-2">';
															echo "<label>社群帳號(ID)1</label>";
															echo form_input('sou_sm_ac1[]',$sourcesm[$i]->sou_sm_ac1 , 'class="form-control"');
															echo '</div>';
															echo '<div class="form-group col-md-2">';
															echo "<label>社群帳號(ID)2</label>";
															echo form_input('sou_sm_ac2[]',$sourcesm[$i]->sou_sm_ac2 , 'class="form-control"');
															echo '</div>';
															echo '<div class="form-group col-md-2">';
															echo "<label>社群帳號(ID)3</label>";
															echo form_input('sou_sm_ac3[]',$sourcesm[$i]->sou_sm_ac3 , 'class="form-control"');
															echo '</div>';
															if($i > 0)
															{
															echo '<div class="form-group col-md-2">';
															echo '<a href="cases2/delsourcesm/'.$sourcesm[$i]->sou_sm_num.'" name="remove" id="'.$i.'" class="btn btn-danger btn_remove">刪除</a>';
															echo '</div>';
															}
															echo '</div>';
														}
													?>
                                                </div>
                                            <?php // echo form_submit('', 'newcases');
                                            echo form_close();?>                                  
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>


                    <!-- /.row -->


                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
        $(document).ready(function(){      
            var i=1;  
            $('#add').click(function(){  
                i++;  
                if(i<10){
                    // $('#source_r').append('<div class="row" id="row'+i+'">'+
                    //     '<div class="form-group col-md-2" style="padding:5px"><label>通訊軟體</label><input name="sou_sm_name[]" class="form-control" ></div>'+
                    //     '<div class="form-group col-md-2" style="padding:5px"><label>社群帳號(ID)1</label><input name="sou_sm_ac1[]" class="form-control"></div>'+
                    //     '<div class="form-group col-md-2" style="padding:5px"><label>社群帳號(ID)2</label><input name="sou_sm_ac2[]" class="form-control"></div>'+
                    //     '<div class="form-group col-md-2" style="padding:5px"><label>社群帳號(ID)3</label><input name="sou_sm_ac3[]" class="form-control"></div>'+
                    //     '<div class="form-group col-md-2" style="padding:5px"><button type="button" name="remove" id="'+i+'"'+
                    //     'class="btn btn-danger btn_remove">X</button></div></div>');
					$("#source_r").append(`<div class="row"  id="row${i}" style="border-left:5px solid ${((i%2) == 1)?'#337ab7':'#5bc0de'};margin-left:20px;margin-bottom:10px;">
                                                    <div class="form-group col-md-2">
                                                        <label>通訊軟體</label><input name="sou_sm_name[]" class="form-control" >
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號(ID)1</label><input name="sou_sm_ac1[]" class="form-control"/>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號(ID)2</label><input name="sou_sm_ac2[]" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號(ID)3</label><input name="sou_sm_ac3[]" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <button type="button" name="remove" id="${i}"
                                                    class="btn btn-danger btn_remove">刪除</button>
                                                    </div>
                                                </div>`);
                }          
                else alert("通訊軟體最多輸入10筆");
            });
            $(document).on('click', '.btn_remove', function(){  
                i--;  
                var button_id = $(this).attr("id");   
                $('#row'+button_id+'').remove();  
            });  
                $( "#create_source" ).validate({
                    rules: {
                        sou_name: {
                            required: true,
                        },
                        /*sou_phone: {
                            required: true,
                            digits: true,
                            minlength: 10
                        },*/
                        sou_amount: {
                            number: true,
                        },
                        'sourcepic': {
                            extension: "jpg|jpeg|png|gif"
                        },
                    },
                    messages: {
                        sou_name: {
                            required: "請輸入必填項目",
                        },
                        /*sou_phone: {
                            required: "請選擇必填項目",
                            digits: "請輸入數字",
                            minlength: "請完整輸入",
                        },*/
                        sou_amount: {
                            number: "請輸入數字",
                        },
                        'sourcepic': {
                            extension: "只接受jpg|jpeg|png|gif"
                        },
                    }
                });        
            });            
        </script>
    </body>
</html>
