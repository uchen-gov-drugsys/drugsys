        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 

                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                  清單列表
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>
                                    <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                    <input id="enum" type="hidden" name="enum" value=''> 
                                    <input id="s_status" type="hidden" name="s_status" value=''> 
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <div class="modal large fade" id="change" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" >
						<div class="modal-header bg-primary">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">人員異動</h4>
						</div>
						<div class="modal-body">
							<?php echo form_open_multipart('Cases2/change_dep','id="change_dep" class="form-horizontal"') ?>
								<div class="form-group">
									<label class="title col-md-3 control-label">證號 <span class="text-danger">*</span></label>
									<div class="col-md-9">
										<input type="text" class="form-control" name='em_ic' id="id" required value="">
									</div>
								</div>
								<div class="form-group">
									<label class="title col-md-3 control-label">帳號 <span class="text-danger">*</span></label>
									<div class="col-md-9">
										<input type="text" class="form-control" name='em_mailId' id="email" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="title col-md-3 control-label">職稱 <span class="text-danger">*</span></label>
									<div class="col-md-9">
										<input type="text" class="form-control" name='em_job' id="job" required value="">
									</div>
								</div>
								<div class="form-group">
									<label class="title col-md-3 control-label">姓名 <span class="text-danger">*</span></label>
									<div class="col-md-9">
										<div class="input-group ">
											<span class="input-group-addon" id="basic-addon1">姓 <span class="text-danger">*</span></span>
											<!-- <label class="title col-md-2 control-label"> </label> -->
											<input type="text" class="form-control" name='em_lastname' id="lastname" required value="">
											<span class="input-group-addon" id="basic-addon1">名 <span class="text-danger">*</span></span>
											<!-- <label class="title col-md-2 control-label">名 <span class="text-danger">*</span></label> -->
											<input type="text" class="form-control" name='em_firstname' id="firstname" required value="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="title col-md-3 control-label">分配部門 <span class="text-danger">*</span></label>
									<div class="col-md-9">		
										<p><?php echo form_dropdown('em_roffice',$options,'', 'class="form-control"'); ?></p>	
										<div class="form-group" id='dep'></div>
									</div>
								</div>
							
							</form>
						</div>
						<div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
                            <input type="submit"  value="確認" class="btn btn-warning" />
                        </div>
                    </div>
                    </div>
            </div>
        </div>
        <script type="text/javascript"> 
        $(document).ready(function(){
            $('#table1').DataTable({
                dom: 'Bfrtip',
                 buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
                ],
                lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                ],
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    }
                },
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                "order": [[ 0, "desc" ]],
                //"search": {
                //    "search": "科技犯罪偵查隊"
                //},
                'ajax': {
                  'url':'<?php echo base_url("Cases2/changejson")?>'
                },
                'columns': [
                    { data: 'em_mailId' },
                    { data: 'em_lastname' },
                    { data: 'em_office' },
                    { data: 'em_job' },
                    { data: 'em_roffice' },
                    { data: 'em_IsAdmin' },
                ]
            });
            $('body').on("click",".callModal", function() {//用於抓取
                var job_id = $(this).data('job-id');
                $('#job').val( job_id );//職稱
                var e_id = $(this).data('e-id');
                $('#id').val( e_id );//證號
                var ln_id = $(this).data('ln-id');
                $('#lastname').val( ln_id );
                var fn_id = $(this).data('fn-id');
                $('#firstname').val( fn_id );
                var email_id = $(this).data('email-id');
                $('#email').val( email_id );
            });
            $( "#change_dep" ).validate({
                    rules: {
                        em_ic: {
                            required: true,
                            rangelength: [9, 10],
                        },
                        em_firstname: {
                            required: true,
                        },
                        em_lastname: {
                            required: true
                        },
                        em_job: {
                            required: true
                        },
                        em_mailId: {
                            required: true
                        },
                        em_roffice: {
                            required: true
                        },
                    },
                    messages: {
                        em_ic: {
                            required: "請輸入必填項目",
                            rangelength: "請輸入正確證號",
                        },
                        em_firstname: {
                            required: "請輸入必填項目",
                        },
                        em_lastname: {
                            required: "請輸入必填項目",
                        },
                        em_job: {
                            required: "請輸入必填項目",
                        },
                        em_mailId: {
                            required: "請輸入必填項目",
                        },
                        em_roffice: {
                            required: "請選擇必填項目",
                        },
                    }
            }); 
            var inoutEvent = $("input[name*=inout]:radio");
            inoutEvent.change(function () {
                if(inoutEvent.val() == '1')$("p").slideToggle();
            });
        });        
    </script>
