            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row" style="margin-top:35px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;letter-spacing:5px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
													<a  class="btn btn-default" href=<?php echo base_url("cases2/addNewCases") ?>>新增誤合併案件</span></a>
                          <button class="btn btn-success" id="mergeBT">合併案件</button>
													<button class="btn btn-primary" id="sendBT">移送或裁罰</button>
                        </div>
                    </div>
										<h4>選取要合併的同案共犯案件，案件查獲時間將以案件編號最小者之內容為主。</h4>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
																		<div class="row">
																			<div class="col-md-6">
																			案件列表
																			</div>
																			<div class="col-md-6 text-right">
																				資料來源：偵查情資系統-毒品資料庫
																			</div>
																		</div>
                                  
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $data_table;?>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            /*$(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });*/
            $(document).ready(function() {
							chooseAry = [];
              table = $('#table1').DataTable({
                dom: 'Bfrtip',
								columnDefs: [ {
										targets:   0,
										visible: true
								},{
										targets:   1,
										visible: false
								},{
										className: 'select-checkbox',
										targets:   0,
								} ],
								select: {
										style:    'os',
										selector: 'td:first-child',
										style: 'multi'
								},
                buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
                ],
                lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                ],
								"fnRowCallback": function (nRow, aData, iDisplayIndex) {
										$(nRow).attr("data-id", aData[1]);
										return nRow;
								},
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    }
                }, 
              });

              // Add event listener for opening and closing details
              $('#table1 tbody').on('click', 'td.details-control', function() {
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                  // This row is already open - close it
                  row.child.hide();
                  tr.removeClass('shown');
                } else {
                  // Open this row
                  row.child(format(row.data())).show();
                  tr.addClass('shown');
                }
              });

              $('a.toggle-vis').on('click', function(e) {
                e.preventDefault();

                // Get the column API object
                var column = table.column($(this).attr('data-column'));

                // Toggle the visibility
                column.visible(!column.visible());
              });
              $('.list_view input[type="checkbox"]').on('change', function(e) {


                // Get the column API object
                var col = table.column($(this).attr('data-target'));

                // Toggle the visibility
                col.visible(!col.visible());
              });

							$('#table1 tbody').on( 'click', 'tr', function () {
									$(this).toggleClass('selected');
									
									if($(this).hasClass('selected'))
									{
											chooseAry.push($(this).attr("data-id"))
									}
									else
									{
											delAry = $(this).attr("data-id");
											chooseAry.forEach(function(item, index, arr) {
													if(JSON.stringify(item) === JSON.stringify(delAry)) {
															return arr.splice(index, 1);
													}
											});
									}
							} );

							$("#mergeBT").click(function(){
								if(chooseAry.length > 0)
								{
									let formdata = new FormData();
									formdata.append('caseid_selected', JSON.stringify(chooseAry.sort()));
									$.ajax({            
													type: 'POST',
													url: '<?php echo base_url('Cases2/mergrCases'); ?>',
													dataType: 'json',
													data: formdata,
													processData: false,
													contentType: false,
													cache: false,
													complete: function (data) {
														location.reload();
													}
											});
								}
								else
								{
									alert('請勾選要合併之案件。');
								}
							});
							$("#sendBT").click(function(){
								if(chooseAry.length > 0)
								{
									let formdata = new FormData();
									formdata.append('caseid_selected', JSON.stringify(chooseAry.sort()));
									$.ajax({            
													type: 'POST',
													url: '<?php echo base_url('Cases2/checkCasesInput_batch'); ?>',
													dataType: 'json',
													data: formdata,
													processData: false,
													contentType: false,
													cache: false,
													success: function(resp){
														if(resp.status != 200)
														{
															alert(resp.msg);
														}
														else{
															location.href=resp.data;
														}
														
													}
											});
								}
								else
								{
									alert('請勾選要送裁罰及沒入案件。');
								}
							});
            });
						function nowChooseEvent(ary)
						{
								str = '';
								$("input[name='s_ic']").val(JSON.stringify(chooseAry));
								ary.forEach(function(item, index, arr) {
										str += '<span style="border-bottom:1px solid #d4d4d4;margin-right:20px;">'+item['userid']+' '+item['username']+'</span>';
								});
								$("#select-wrap").html(str);
						}
        </script>
