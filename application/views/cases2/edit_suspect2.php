        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
                <!--
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="create_suspect1" id="save"></a></li>
                </ul> -->
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">

                    <div class="row" style="margin-top:35px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;letter-spacing:5px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a><input type="submit" value="儲存" class="btn btn-warning"  form="create_suspect1" id="save"></a>
							<br/>
							<strong><?php echo $error; ?></strong>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
									<h3 class="panel-title">犯嫌姓名：<?php echo $row->s_name;?></h3>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                <div class="row">
                                        <?php echo form_open_multipart('Cases2/updateSusp2','id="create_suspect1"') ?>
                                        <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <label>姓名</label>
                                                        <?php echo form_input('s_name',$row->s_name, 'class="form-control"')?>                            
                                                        <input id="link" type="hidden" name="link" value=" ">                           
                                                        <input id="s_num" type="hidden" name="s_num" value=<?php echo $row->s_num;?>> 
                                                            <input id="s_cnum" type="hidden" name="s_cnum" value=<?php echo $row->s_cnum;?>> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>身份證字號</label>
                                                        <?php echo form_input('s_ic',$row->s_ic, 'class="form-control" id="s_ic"')?>
                                                    </div>
													<div class="form-group col-md-2">
                                                        <label>尿液編號</label><?php echo form_input('s_utest_num',$row->s_utest_num, 'class="form-control" required')?>
                                                    </div>
													<div class="form-group col-md-2 hidden">
                                                        <label>處分書狀態</label>
                                                        <?php echo form_dropdown('s_sac_state', $opt2 ,null , 'class="form-control" ');?>
                                                    </div>
													<div class="form-group col-md-2">
                                                        <label>尿報</br></label>
                                                        <?php 
                                                        if($row->s_utest_doc == ""){
                                                            echo form_upload('s_utest_doc');
                                                        }
                                                        else {
                                                            echo anchor_popup('drugdoc/' . $row->s_utest_doc, '</br>尿報').'<a href="'.base_url('cases2/deleteurinepaper/'.$this->uri->segment(3)).'"><span class="fa fa-times-circle text-danger"></span></a>'.'<br/>';
                                                            echo form_hidden('s_utest_doc',$row->s_utest_doc);
                                                        }
                                                        ?>
														<span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制10MB</small> </span>

                                                    </div>
												</div>
												<hr>
												<div class="row">
													<div class="col-md-12">
													<label>尿液成分<small class="text-muted">(可多筆新增)</small> <input type="button" value="新增成分" class="btn btn-default btn-sm" id="new"></label>
													</div>
													
												</div>
												<div class="row">
														<div id="drug_s">
																<?php 
																foreach ($susp2 as $susp2)
																{
																	echo '<div class="form-group col-md-2">';
																	echo form_dropdown('sc[]',$opt ,$susp2->sc_level .'級'. $susp2->sc_ingredient , 'class="form-control" id="sel" required');
																	echo form_hidden('sc_num[]',$susp2->sc_num); 
																	echo '</div>';
																	echo '<div class="form-group col-md-1">';
																	echo anchor('cases2/deleteSuspCk/'. $susp2->sc_num,'X','class="btn btn-danger btn_remove"');
																	//echo '<button type="button" class="btn btn-danger btn_remove">X</button></';
																	echo '</div>';
																}   
																?>                                                            
														</div>
												</div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript"> 
            $(document).ready(function(){
                $("#new").click(function (){
                    $("#link").val('1');
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $( "#create_suspect1" ).validate({
                    rules: {
                        s_name: {
                            required: true,
                            minlength: 2
                        },
                        s_ic: {
                            required: true,
                            minlength: 9
                        },
                        sc_no: {
                            required: true,
                        },
                    },
                    messages: {
                        s_name: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        s_ic: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        sc_no: {
                            required: "此欄位不得為空",
                        },
                    }
                });        
            });            
        </script>
    </body>
</html>
