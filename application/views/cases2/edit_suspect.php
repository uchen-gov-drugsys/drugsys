        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">

                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a><input type="submit" value="儲存" class="btn btn-warning"  form="create_suspect1" id="save"></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
									<h3 class="panel-title">犯嫌姓名：<span  class="label label-warning"><?php echo $row->s_name;?></span></h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <?php echo form_open_multipart('Cases2/suspect1Update','id="create_suspect1"') ?>
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <label>犯嫌姓名</label>
                                                        <?php echo form_input('s_name',$row->s_name, 'class="form-control"')?>                            
                                                        <input id="link" type="hidden" name="link" value=" ">                           
                                                        <input id="s_cnum" type="hidden" name="s_cnum" value=<?php echo $row->s_cnum;?>> 
                                                        <input id="s_num" type="hidden" name="s_num" value=<?php echo $row->s_num;?>> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>犯嫌身份證</label>
                                                        <?php echo form_input('s_ic',$row->s_ic, 'class="form-control" id="s_ic"')?>      
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>特徵</label>
                                                        <?php echo form_input('s_feature',$row->s_feature, 'class="form-control"')?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>性別</label><br>
                                                        <?php
                                                            if($row->s_gender =="男") echo form_radio('s_gender', '男',true).'男';
                                                            else echo form_radio('s_gender', '男',false).'男';
                                                            if($row->s_gender =="女")echo form_radio('s_gender', '女',true).'女';
                                                            else echo form_radio('s_gender', '女',false).'女';
                                                        ?>   
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>綽號</label>
                                                        <?php echo form_input('s_nname',$row->s_nname, 'class="form-control"')?>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>外國籍</label>
                                                        <?php echo form_input('s_nation',$row->s_nation, 'class="form-control"')?>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <label>出生年月日</label>
                                                        <?php echo form_input('s_birth',(isset($row->s_birth))?((strlen($row->s_birth) > 7 && $row->s_birth != '0000-00-00')?str_pad(((int)substr($row->s_birth, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($row->s_birth, 5, 2).substr($row->s_birth, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md'), 'class="rcdate form-control" id="date"')?> 
														<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>教育程度</label>
                                                        <?php echo form_dropdown('s_edu',$options , $row->s_edu , 'class="form-control"')?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>職業</label>
                                                        <?php echo form_input('s_job',$row->s_job, 'class="form-control"')?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>幫派堂口名稱</label>
                                                        <?php echo form_input('s_gang',$row->s_gang, 'class="form-control"')?>   
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>列管應受尿液採檢人口</label>
														<br>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="s_U_Col" id="optionsRadiosInline1" value="是" checked>是
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="s_U_Col" id="optionsRadiosInline2" value="否">否
                                                        </label>     
                                                    </div>
												</div>
                                                    
                                                
                                                <div class="row">
													<div class="form-group col-md-4">
                                                        <label>犯罪手法</label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="持有" <?php if($持有 == true){echo 'checked';}?> />持有 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="意圖販售而持有" <?php if($意圖販售而持有 == true){echo 'checked';}?> />意圖販售而持有 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="販賣" <?php if($販賣 == true){echo 'checked';}?> />販賣 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="運輸" <?php if($運輸 == true){echo 'checked';}?> />運輸</label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="施用" <?php if($施用 == true){echo 'checked';}?> />施用</label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="栽種" <?php if($栽種 == true){echo 'checked';}?> />栽種 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="製造" <?php if($製造 == true){echo 'checked';}?> />製造 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="轉讓" <?php if($轉讓 == true){echo 'checked';}?> />轉讓 </label>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>戶籍地</label>
                                                        <div id="zipcode">
                                                        </div>
														<textarea name="s_dpaddress" class="form-control" placeholder="路門牌" id="s_no"><?php echo $row->s_dpaddress; ?></textarea>
                                                        <?php //echo form_textarea('s_dpaddress',$row->s_dpaddress, ' class="form-control" placeholder="路門牌" id="s_no" ')?> 
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>現居地</label>
                                                        <label class='receiverchange checkbox-inline checkboxeach'><input id='receiverchange' class='receiverchange btn btn-default btn-sm' type='button' name='receiverchange' value='戶籍地與現居地相同'></label>
                                                        <div id="zipcode2">
                                                        </div>
														<textarea name="s_rpaddress" class="form-control" placeholder="路門牌" id="s_no2"><?php echo $row->s_rpaddress; ?></textarea>
                                                        <?php //echo form_textarea('s_rpaddress',$row->s_rpaddress, 'class="form-control" placeholder="路門牌" id="s_no2"  ')?> 
                                                    </div>
												</div>
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <div class="form-group">
                                                            <label>溯源可能性</label>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name='s_resource' id="optionsRadiosYes" value="yes" <?php echo ($row->s_resource == 'yes' || !isset($row->s_resource))?'checked':''; ?> >是
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name='s_resource' id="optionsRadiosNo" value="no" <?php echo ($row->s_resource == 'no')?'checked':''; ?> >否
                                                                </label>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                    <div class="form-group col-md-4" id="sel_td">
                                                        <label>未指認毒品來源-未指認原因</label>
                                                        <?php echo form_dropdown('s_noresource',$options1 , $row->s_noresource , 'class="form-control" id="sel" '.(($row->s_resource == 'yes' || !isset($row->s_resource))?'disabled="disabled"':''))?> 
                                                    </div>
                                                    
                                                    <div class="form-group col-md-6">
                                                        <label>犯嫌照片</label>
                                                        <?php 
                                                            if($row->s_pic == ""){
                                                                echo form_upload('susppic');
                                                            }
                                                            else {
                                                                ?><img src=<?php echo base_url("1susppic/") . $row->s_pic  ?> alt="" width="50" height="50">
                                                                <?php echo anchor_popup('1susppic/' . $row->s_pic, '</br>犯嫌照片');
                                                                //echo form_hidden('s_pic',$row->s_pic);
                                                            }
                                                        ?>  
														<span class="text-danger"><small>限上傳gif, jpg, png類型之檔案且檔案大小限制10MB</small> </span>
                                                    </div>
                                                </div>
                                            <?php // echo form_submit('', 'newcases');
                                            echo form_close();?>                                  
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <div class="row" id="mark1">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6"><h3 class="panel-title">指認毒品來源(溯源)</h3></div>
                                        <div class="col-md-6 text-right">
                                            
                                            <label style="float:right"><input id="source" style="float: left;" class="btn btn-default btn-sm" type="button" value="新增" /></label>                 
                                        </div>                                                 
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $sourcetable;?>
                                    </div>                       
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6"><h3 class="panel-title">犯嫌持有門號</h3></div>
                                        <div class="col-md-6 text-right">
                                            
                                            <label style="float:right"><input id="phone" style="float: left;" class="btn btn-default btn-sm" type="button" value="新增" /></label>                
                                        </div>                                                 
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $phonetable;?>
                                    </div>                       
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>  

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6"><h3 class="panel-title">擁有社群帳號</h3></div>
                                        <div class="col-md-6 text-right">
                                            
                                            <label style="float:right"><input id="social" style="float: left;" class="btn btn-default btn-sm" type="button" value="新增" /></label>                
                                        </div>                                                 
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $socialtable;?>
                                    </div>                       
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div> 

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6"><h3 class="panel-title">交通工具</h3></div>
                                        <div class="col-md-6 text-right">
                                            
                                            <label style="float:right"><input id="car" style="float: left;" class="btn btn-default btn-sm" type="button" value="新增" /></label>               
                                        </div>                                                 
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $cartable;?>
                                    </div>                       
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>   
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
			$(".rcdate").change(function(){
				let strlen = $(this).val().length;
				if(strlen < 7 || strlen < "7")
				{
					if(strlen != 0)
						$(this).val("0"+$(this).val());
				}
				else
				{
					return false;
				}
					
			});
			$(".rcdate").keypress(function(){
				if($(this).val().length >= 7)
					return false;
			});
            $("#zipcode").twzipcode({
                "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_dpcounty',   // 預設值為 county
                'districtName' : 's_dpdistrict', // 預設值為 district
                'zipcodeName'  : 's_dpzipcode',// 預設值為 zipcode
                <?php if(isset($row->s_dpzipcode)) echo "'zipcodeSel'   : ".$row->s_dpzipcode; ?>
            });         
            
            $("#zipcode2").twzipcode({
                "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_rpcounty',   // 預設值為 county
                'districtName' : 's_rpdistrict', // 預設值為 district
                'zipcodeName'  : 's_rpzipcode', // 預設值為 zipcode
                <?php if(isset($row->s_rpzipcode)) echo "'zipcodeSel'   : ".$row->s_rpzipcode; ?>
            }); 
            $('#receiverchange').on('click', function() {
               // $("#zipcode2").empty();
                var city = $(zipcode).twzipcode('get', 'city');
                var zipc = $(zipcode).twzipcode('get', 'zipcode');
                console.log(zipc);   // 縣市
                if( zipc == ""){
                    alert("請務必選擇鄉鎮市區");return false;
                } 
                $("#zipcode2").twzipcode('set', zipc[0]);
                var sno=$("#s_no").val();

                // 移除空格確定是否空值
                if (!$.trim($("#s_no").val())) {
                    if( $("#s_no").val()==""){
                        $("#s_no").focus();
                        alert("請務必填入路門牌");return false;
                    }     
                }   
                $("#s_no2").val(sno);
            }); 
            
           $('input[name=s_resource]').change(function(){
                var value = $( this ).val();
                //alert(value);
                if(value=='yes'){
                    $('#sel').attr('disabled', true);
                }
                else{
                    $('#sel').attr('disabled', false);
                }
            }); 
            
           $("#sel").change(function(){
                switch (parseInt($(this).val())){
                    case 0:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 1:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 2:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 3:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 4:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 5:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 6:
                    $("#sel_td").append($("<textarea id='del' name='s_noresource' class='form-control' placeholder='請描述其他未指認原因'></textarea>"));
                    break;
                }
            });              
 
            $(document).ready(function(){
                $("#save").click(function (){
                    $("#link").val('cases2/edit/'+$("#s_cnum").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $("#source").click(function (){
                    $("#link").val('cases2/newsource/'+$("#s_ic").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $("#phone").click(function (){
                    $("#link").val('cases2/newphone/'+$("#s_ic").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $("#social").click(function (){
                    $("#link").val('cases2/newsocialmedia/'+$("#s_ic").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $("#car").click(function (){
                    $("#link").val('cases2/newcar/'+$("#s_ic").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $( "#create_suspect1" ).validate({
                    rules: {
                        s_name: {
                            required: true,
                            minlength: 2
                        },
                        s_ic: {
                            required: true,
                            minlength: 9
                        },
                        susppic: {
                            extension: "jpg|jpeg|png|gif"
                        },
                        s_dpdistrict: {
                            required: true,
                        },
                        s_dpaddress: {
                            required: true,
                        },
                    },
                    messages: {
                        s_name: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        s_ic: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        susppic: {
                            extension: "只接受jpg|jpeg|png|gif"
                        },
                        s_dpdistrict: {
                            required: "此欄位不得為空",
                        },
                        s_dpaddress: {
                            required: "此欄位不得為空",
                        },
                    }
                });        
                // $('#date').datepicker({//bootstrap datepicker 1.9v 民國修改版
                //     format: "yyyy-mm-dd", //twy為民國
                //     autoclose: true,
                //     todayHighlight: true,
                //     language: 'zh-TW'      
                // });
            });            
        </script>
    </body>
</html>
