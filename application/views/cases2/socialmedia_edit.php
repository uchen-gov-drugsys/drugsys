        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>

            <div id="page-wrapper">
                <div class="container-fluid">

                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a><input type="submit" value="儲存" class="btn btn-warning"  form="create_sm"></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
									<h3 class="panel-title">擁有社群帳號</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <div class="row" id="drugall"> 
                                                <?php echo form_open_multipart('cases/updatesocialmedia','id="create_sm"') ?>
                                                    <div class="form-group col-md-2">
                                                        <label>社群軟體名稱</label>
                                                        <?php echo form_input('social_media_name',$row->social_media_name,'class="form-control"'); ?>
                                                        <?php echo form_hidden('s_ic',$row->s_ic); ?>
                                                        <?php echo form_hidden('s_cnum',$row->s_cnum); ?>
                                                        <?php echo form_hidden('s_num',$row->s_num); ?>
                                                        <?php echo form_hidden('social_media_num',$row->social_media_num); ?>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號1</label>
                                                        <?php echo form_input('social_media_ac1',$row->social_media_ac1,'class="form-control"'); ?>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號2</label>
                                                        <?php echo form_input('social_media_ac2',$row->social_media_ac2,'class="form-control"'); ?>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>社群帳號3</label>
                                                        <?php echo form_input('social_media_ac3',$row->social_media_ac3,'class="form-control"'); ?>
                                                    </div>
                                                </div>
                                            <?php // echo form_submit('', 'newcases');
                                            echo form_close();?>                                  
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
        </script>
    </body>
</html>
