        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h4><?php //echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="create_phone"></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							<a><input type="submit" value="儲存" class="btn btn-warning" form="create_phone"></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
									<h3 class="panel-title">犯嫌持有門號</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?php echo form_open_multipart('cases2/updatephone','id="create_phone"') ?>          
                                        <div class="col-lg-12">
											<div class="row">
												<div class="form-group col-md-2">
													<label>電話</label>
													<?php echo form_input('p_no',$row->p_no, 'class="form-control" id="p_no"');  ?>                                                      
													<input id="p_s_ic" type="hidden" name="p_s_ic" value=<?php echo $row->p_s_ic;?>> 
													<input id="p_s_cnum" type="hidden" name="p_s_cnum" value=<?php echo $row->p_s_cnum;?>> 
													<input id="p_num" type="hidden" name="p_num" value=<?php echo $row->p_num;?>> 
												</div>
												<div class="form-group col-md-2">
													<label>開機密碼</label>
													<?php echo form_input('p_oppw',$row->p_oppw, 'class="form-control" id="p_oppw"');  ?> 
												</div>
												<div class="form-group col-md-2">
													<label class="thumbnail">圖形密碼 <span><img id="img1"  src=<?php echo base_url("img/圖形密碼.png") ?> alt="圖形密碼" width="300" border="0"></span></label>
													<?php echo form_input('p_picpw',$row->p_picpw, 'class="form-control" placeholder="依據圖片輸入數字"');  ?>  
												</div>
												<div class="form-group col-md-2">
													<label>手機序號(IMEI)</label>
													<?php echo form_input('p_imei',$row->p_imei, 'class="form-control"');  ?>
													<span class="text-danger"><small>* 至少10碼</small> </span>
												</div>
												<div class="form-group col-md-2">
													<label>查扣手機</label>
													<?php echo form_dropdown('p_conf',$options , $row->p_conf , 'class="form-control" id="sel"')?>  
												</div>
											</div>
											<hr>
											<h4>通聯記錄 <small>(最多10筆)</small><input class="btn btn-default btn-sm pull-right" type="button" onclick="CreateUploadPhone_R()" value="新增通聯記錄" /></h4>
											<div id="phone_r">
												<?php
															for($i=0, $count = count($phonerec);$i<$count;$i++) {
																echo '<div class="container-fluid" style="border-left:5px solid '.((($i%2) == 1)?'#337ab7':'#5bc0de').';margin-left:20px;margin-bottom:10px;" id="phone_r'.$i.'">';
																echo '<div class="row">';

																echo '<div class="form-group col-md-2">';
																echo "<label>電話</label>";
																echo form_dropdown('pr_path[]',$options1,$phonerec[$i]->pr_path , 'class="form-control" id="sel"');
																echo '</div>';
																echo '<div class="form-group col-md-2">';
																echo "<label>電話</label>";
																echo form_input('pr_phone[]',$phonerec[$i]->pr_phone , 'class="form-control"');
																echo form_hidden('pr_num[]',$phonerec[$i]->pr_num);
																echo form_hidden('pr_num[]',$phonerec[$i]->pr_p_no);
																echo '</div>';
																echo '<div class="form-group col-md-2">';
																echo "<label>姓名</label>";
																echo form_input('pr_name[]',$phonerec[$i]->pr_name , 'class="form-control"');
																echo '</div>';
																echo '<div class="form-group col-md-2">';
																echo "<label>通聯時間</label>";
																echo '<input name="pr_time[]" value="'. set_value('pr_time[]',$phonerec[$i]->pr_time ).'"class="form-control" id="date" type="text" ><span class="text-danger"><small>(如：1100505 下午)</small> </span>';
																echo '</div>';
																echo '<div class="form-group col-md-2">';
																echo "<label>與犯嫌關係</label>";
																echo form_input('pr_relationship[]',$phonerec[$i]->pr_relationship , 'class="form-control" id="sel"');
																echo '</div>';
																echo '</div>';

																echo '<div class="row">';
																echo '<div class="form-group col-md-2">';
																echo "<label>被告知毒品上手</label>";
																echo form_dropdown('pr_has_drug[]',$options,$phonerec[$i]->pr_has_drug , 'class="form-control" id="sel"');
																echo '</div>';
																echo '<div class="form-group col-md-2">';
																echo "<label>其他毒品使用者</label>";
																echo form_dropdown('pr_user[]',$options,$phonerec[$i]->pr_user , 'class="form-control" id="sel"');
																echo '</div>';
																echo '<div class="form-group col-md-2">';
																echo "<label>其他販毒者</label>";
																echo form_dropdown('pr_seller[]',$options,$phonerec[$i]->pr_seller , 'class="form-control" id="sel"');
																echo '</div>';
																echo '<div class="form-group col-md-2">';
																echo anchor('cases2/delphonerec/'. $phonerec[$i]->pr_num,'刪除','class="btn btn-danger btn_remove"');
																echo "</div>";

																echo '</div></div>';
															}
														?>
											</div>
                                        </div>
										<?php echo form_close(); ?>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../../js/metisMenu.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-twzipcode@1.7.14/jquery.twzipcode.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../../js/startmin.js"></script>
        <script src="../../js/jquery.multi-select.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>    
        <script type="text/javascript">
            $(document).ready(function(){
                $("#img1").hide();
                $(".thumbnail").click(function(){
                $("#img1").toggle();
                });
                $( "#create_phone" ).validate({
                    rules: {
                        p_no: {
                            required: true,
                            digits: true,
                            minlength: 10
                        },
                        "pr_phone[]": {
                            digits: true,
                            minlength: 10
                        },
                        p_oppw: {
							required: false,
                            digits: true,
                            minlength: 3
                        },
                        p_picpw: {
                            digits: true,
                        },
                        p_imei: {
							required: false,
                            digits: true,
                            minlength: 10
                        },
                    },
                    messages: {
                        p_no: {
                            required: "此欄位不得為空",
                            digits: "請輸入數字",
                            minlength: "請完整輸入",
                        },
                        "pr_phone[]": {
                            digits: "請輸入數字",
                            minlength: "請完整輸入",
                        },
                        p_oppw: {
                            digits: "請輸入數字",
                        },
                        p_picpw: {
                            digits: "請輸入數字",
                        },
                        p_imei: {
                            digits: "請輸入數字",
                            minlength: "至少輸入10碼"
                        },
                    }
                });        
            });
            function RemoveAddPhone(id)
            {
                phone_r--;
                var div=document.getElementById('phone_r'+id);
                var div2=document.getElementById('phone_r');
                div2.removeChild(div);
            } 
            var phone_r = 0;
            function CreateUploadPhone_R()
            {
                phone_r++;
                if(phone_r < 10){
					html = `<div class="container-fluid" style="border-left:5px solid ${(phone_r%2 == 1)?'#337ab7':'#5bc0de'};margin-left:20px;margin-bottom:10px;" id="phone_r${phone_r}">
                                                    <div class="row">
                                                        <div class="form-group col-md-2">
                                                            <label>通聯方向</label>
                                                            <select name="pr_path[]" class="form-control">
                                                                <option>撥出</option>
                                                                <option>撥入</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>電話</label>
                                                            <input id="pr_phone" name="pr_phone[]" class="form-control" >
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>姓名</label>
                                                            <input id="pr_name" name="pr_name[]" class="form-control" >
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>通聯時間</label>
                                                            <input name="pr_time[]" class="form-control" input="date" type = "text">
															<span class="text-danger"><small>(如：1100505 下午)</small> </span>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>與犯嫌關係</label>
                                                            <input name="pr_relationship[]" class="form-control" >
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-2">
                                                            <label>被告知毒品上手</label>
                                                            <select name="pr_has_drug[]" class="form-control">
                                                                <option>是</option>
                                                                <option>否</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>其他毒品使用者</label>
                                                            <select name="pr_user[]" class="form-control">
                                                                <option>是</option>
                                                                <option>否</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label>其他販毒者</label>
                                                            <select name="pr_seller[]" class="form-control">
                                                                <option>是</option>
                                                                <option>否</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <input class="btn btn-danger" type="button" onclick="RemoveAddPhone(${phone_r})" value="刪除" />
                                                        </div>
                                                    </div>
                                                </div>`;
						$('#phone_r').append(html);
                // var td=document.createElement('tr');
                // var html='<tr><td style="padding:5px"><label>通聯方向</label><select name="pr_path[]" class="form-control">'+
                //         '<option>撥出</option><option>撥入</option></select></td><td style="padding:5px"><label>電話</label>'+
                //         '<input name="pr_phone[]" class="form-control" ></td><td style="padding:5px"><label>姓名</label>'+
                //         '<input name="pr_name[]" class="form-control" ></td><td style="padding:5px"><label>通聯時間</label>'+
                //         '<input name="pr_time[]" class="form-control" input="date" type = "date"></td><td style="padding:5px">'+
                //                                                 '<label>與犯嫌關係</label>'+
                //                                                 '<input name="pr_relationship[]" class="form-control" >'+
                //                                             '</td>'+
                //                                             '<td style="padding:5px">'+
                //                                                 '<label>被告知毒品上手</label>'+
                //                                                 '<select name="pr_has_drug[]" class="form-control">'+
                //                                                     '<option>是</option>'+
                //                                                     '<option>否</option>'+
                //                                                 '</select>'+
                //                                             '</td>'+
                //                                             '<td style="padding:5px">'+
                //                                                 '<label>其他毒品使用者</label>'+
                //                                                 '<select name="pr_user[]" class="form-control">'+
                //                                                    ' <option>是</option>'+
                //                                                     '<option>否</option>'+
                //                                                 '</select>'+
                //                                             '</td>'+
                //                                             '<td style="padding:5px">'+
                //                                                 '<label>其他販毒者</label>'+
                //                                                 '<select name="pr_seller[]" class="form-control">'+
                //                                                     '<option>是</option>'+
                //                                                     '<option>否</option>'+
                //                                                 '</select>'+
                //                                             '</td>'+
                //                                             '<td>'+
                //                                                '<label><input style="float: left;" class="btn btn-default" type="button" onclick="RemoveAddPhone('+phone_r+')"'+
                //                                                 'value="刪除" /></label>'+
                //                                             '</td>'+
                //     '</tr>'
                //     td.innerHTML=html;
                //     td.setAttribute("id","phone_r"+phone_r);
                //     //div.id="upDiv"+p;
                //     document.getElementById('phone_r').appendChild(td);
                }
                else{
                    alert('通聯記錄最多10筆');
                }
            }
        </script>
    </body>
</html>
