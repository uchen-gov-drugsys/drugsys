        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- /.row -->
                    
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
								<footer>資料來源為偵查情資毒品資料庫，實際仍以毒品檢驗報告為主。</footer>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a><input type="submit" value="儲存" class="btn btn-warning"  form="newdrug1" id="saveBT"></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
								<?php echo $title;?>
								
                                </div>
                                <?php echo form_open_multipart('cases2/update_drug2','id="newdrug1"') ?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12" id="drugall">
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <label>證物號</label>
                                                        <?php 
                                                            echo form_input('e_id',$e_id, 'class="form-control" readonly');                                                        
                                                            echo form_hidden('e_c_num',$e_c_num);                                                        
                                                        ?>                    
                                                        <input id="link" type="hidden" name="link" value=" "> 
														<hr/>
														<label>初驗毒品成分</label>
														<div id="drug_s" >
															
															<?php 
															$dfindex = 0;
															foreach ($drugck as $drugck1)
															{
																if($drugck1->df_type == '初驗')
																{
																	echo '<span>';
																	echo $drugck1->df_level .'級' .$drugck1->df_ingredient;
																	echo '</span><br/>';
																}
																$dfindex++;
															}   
															?>  
														</div>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>品名:</label>
                                                        <?php echo form_input('e_name',$drugAll->e_name, 'class="form-control" id="sel"');//form_dropdown('e_name',$nameopt , $drugAll->e_name , 'class="form-control" id="sel"')?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>數量</label>
                                                        <?php 
                                                            echo form_input('e_count',$drugAll->e_count, 'class="form-control" id="e_count"');                                                        
                                                        ?> 
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>量數</label>
                                                        <?php echo form_dropdown('e_unit',$countopt , $drugAll->e_unit , 'class="form-control"')?>  
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>初驗淨重</label>
                                                        <?php 
                                                            echo form_input('e_1_N_W',$drugAll->e_1_N_W, 'class="form-control" id="e_1_N_W"');                                                        
                                                        ?> 
                                                    </div>
													<div class="form-group col-md-2">
                                                        <label>持有人 <span class="text-danger">*</span></label>
														<?php
														$onlyU = '';
															foreach ($test as $rowkey=>$rowdata) {
																$haschecked = false;
																if(isset($drugAll->e_togethe))
																{
																	$togeAry = explode(',', $drugAll->e_togethe);
																	foreach ($togeAry as $toge) {
																		if($toge == $rowkey)
																		{
																			$haschecked = true;
																		}
																	}
																}
																if(count($test) == 2)
																{
																	$onlyU = $rowkey;
																}
															}
														?>
                                                        <?php echo form_dropdown('e_c_num',$test , ((!empty($drugAll->e_c_num))?$drugAll->e_c_num:((count($test) == 2)?$onlyU:'')) , 'class="form-control" required '.(($haschecked)?'disabled':''))?>  
														<hr>
														<label>共同持有人</label>
														<div class="together-wrap ">
															<?php
															foreach ($test as $rowkey=>$rowdata) {
																if($rowkey != $drugAll->e_c_num)
																{
																	$checked = false;
																	if(isset($drugAll->e_togethe))
																	{
																		$togeAry = explode(',', $drugAll->e_togethe);
																		foreach ($togeAry as $toge) {
																			if($toge == $rowkey)
																			{
																				$checked = true;
																			}
																		}
																	}
																	if($rowkey != '')
																	{
																		echo '<div class="checkbox">
																				<label>
																				<input type="checkbox" name="together[]" value="'.$rowkey.'" '.(($checked)?'checked':'').'/>'.$rowdata.'
																				</label>
																			</div>';
																	}
																}																
															}
															?>
														</div>
                                                    </div>
													<div class="form-group col-md-2 hidden">
                                                        <label>毒報</br></label>
                                                        <?php 
                                                        if($drugAll->e_doc == ""){
                                                            echo form_upload('e_doc');
                                                        }
                                                        else {
                                                            echo anchor_popup('drugdoc/' . $drugAll->e_doc, '</br>毒報');
                                                            echo form_hidden('e_doc',$drugAll->e_doc);
                                                        }
                                                        ?> 
														<span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制10MB</small> </span>
                                                    </div>
                                                </div>
												<hr>
												<div class="row">
													<div class="col-md-6">
													<label>檢驗結果</label>
													</div>
													<div class="col-md-6 text-right">
														
													</div>
												</div>
												<div class="row">
													<div class="form-group col-md-2">
														<label>淨重(g)</label>
														<?php 
															echo form_input('e_PNW',$drugAll->e_PNW, 'class="form-control" id="e_PNW" required');                                                        
														?> 
													</div>
													<div class="form-group col-md-2">
														<label>驗後餘重(g)</label>
														<?php 
															echo form_input('e_NW',$drugAll->e_NW, 'class="form-control" id="e_NW" required');                                                        
														?> 
													</div>
													<div class="form-group col-md-2">
														<label>純質淨重(g)</label>
														<?php 
															echo form_input('e_PNWS',$drugAll->e_PNWS, 'class="form-control" id="e_PNWS" required');                                                        
														?> 
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
													<label>檢驗毒品級別成分<small class="text-muted">(可多筆新增)</small> <?php echo form_submit('', '新增檢驗成分', 'class="btn btn-default btn-sm" form="newdrug1" id="ddc"');?></label>
													</div>
													<div class="col-md-6 text-right">
														
													</div>
												</div>
												<?php 
												foreach ($drugck as $drugck2)
												// foreach ($drugck2 as $drugck2)
												{
													if($drugck2->df_type == '複驗')
													{
														echo '<div class="row">';
														echo '<div class="form-group col-md-2">';
														echo '<label>成份 <span class="text-danger">*</span></label>';
														echo form_dropdown('df2[]',$drug_opt , $drugck2->df_level .'級' .$drugck2->df_ingredient , 'class="form-control" id="sel" required');
														echo form_hidden('df_num2[]',$drugck2->df_num); 
														echo '</div>';
														   
														echo '<div class="form-group col-md-1"> <br/>';                                           
														echo anchor('cases2/deleteCk/'. $drugck2->df_num.'/'.$this->uri->segment(4),'X','class="btn btn-danger btn_remove"');
														echo '</div>';    
														//echo '<button type="button" class="btn btn-danger btn_remove">X</button></';
														echo '</div>';
													}
													
												}   
												?>  
											</div>
                                            <?php // echo form_submit('', 'newcases');
                                            echo form_close();?>                                  
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
                $(document).ready(function(){      
                    var i=1;  
                    $('#add').click(function(){  
                       i++;  
                       $('#drug_s').append('<div  id="row'+i+'"><div  class="form-group col-md-2">'+
                        '<select name="df[]" class="form-control"><option>海洛因</option><option>安非他命</option>'+
                        '<option>搖頭丸(MDMA)</option><option>愷他命(KET)</option><option>一粒眠</option>'+
                        '<option>大麻</option><option>卡西酮類</option><option>古柯鹼</option><option>無毒品反應</option>'+
                        '</select></div><div class="form-group col-md-1"><button type="button" name="remove" id="'+i+'"'+
                        'class="btn btn-danger btn_remove">X</button></div></div>');  
                    });
                    $(document).on('click', '.btn_remove', function(){  
                       var button_id = $(this).attr("id");   
                       $('#row'+button_id+'').remove();  
                    });  
                    $( "#newdrug1" ).validate({
                        rules: {
                            e_1_N_W: {
                                //required: true,
                                // number: true,
                                // maxlength: 6
                            },
                            e_s_ic: {
                                required: true,
                            },
                            'df2[]': {
                                required: true,
                            },
                        },
                        messages: {
                            e_1_N_W: {
                                //required: "此欄位不得為空",
                                // number: "請輸入數字",
                                // maxlength: "請輸入合理重量",
                            },
                            e_s_ic: {
                                required: "請選擇犯嫌人",
                            },
                        }
                    });        
                    $("#ddc").click(function (){
                        $("#link").val('1');
                        //alert($("#link").val());
                        $("#newdrug1").submit();
                    });   
					$("#df").click(function (){
                        $("#link").val('2');
                        //alert($("#link").val());
                        $("#newdrug1").submit();
                    });  
					$("#saveBT").click(function (){
                        $("#link").val('3');
                        //alert($("#link").val());
						if($("select[name='df2[]']").length > 0)
						{
							$("#newdrug1").submit();
						}
						else
						{
							alert("請新增檢驗成分。");
							return false;
						}
                        
                    });     
					$("input[name='together[]']").click(function(){
						if($("input[name='together[]']:checked").length > 0)
						{
							$("select[name='e_c_num']").attr('disabled', true);
						} 
						else{
							$("select[name='e_c_num']").attr('disabled', false);
						}
					});
					
                }); 
				function getDruglevel(obj, wrap)
				{
					// console.log(wrap);
					let formdata = new FormData();
					formdata.append('druglv', obj.val());
					$.ajax({
						type: 'POST',
						url: '<?php echo base_url("disciplinary_c/getDrugLevel") ?>',
						dataType: 'json',
						// data: {'addr': obj.val()},
						data: formdata,
						processData: false,
						contentType: false,
						cache: false,
						success: function(data){
							// console.log(data);
							wrap.html('');
							$.each(data.data, function(key, value) {
								wrap
									.append($('<option>', { value : value.drug_name })
									.text(value.drug_name));
							});
						},
						complete: function (resp) {
							// console.log(resp.data)								
						}
					});
				}
    </script>
    </body>
</html>
