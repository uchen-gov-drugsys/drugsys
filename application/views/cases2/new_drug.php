        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>    
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- /.row -->
                    
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <input type="submit" class="btn btn-warning" value="儲存" form="newdrug1">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">
									<h3 class="panel-title">新增毒品證物</h3>
                                </div>
                                <div class="panel-body">
                                
                                    <div class="row">
                                        <div class="col-lg-12" id="drugall">
                                                <?php echo form_open_multipart('cases2/create_drug1','id="newdrug1"') ?>
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <label>證物號</label>
                                                        <?php 
                                                            echo form_input('e_id',$e_id, 'class="form-control" id="cnum" readonly');
                                                            echo form_hidden('e_c_num',$e_c_num);
                                                            echo form_hidden('e_empno',$e_empno);
                                                            echo form_hidden('e_ed_empno',$e_ed_empno);
                                                        ?>    
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>品名:</label>
                                                        <select name="e_name" class="form-control">
                                                                <option>毒品粉末、碎塊</option>
                                                                <option>藥碇</option>
                                                                <option>梅碇</option>
                                                                <option>即溶包</option>
                                                                <option>香菸(含菸蒂)</option>
                                                                <option>菸草</option>
                                                                <option>濾嘴</option>
                                                                <option>吸食器</option>
                                                                <option>吸管</option>
                                                                <option>卡片</option>
                                                                <option>括盤</option>
                                                                <option>殘渣袋</option>
                                                                <option>棉花棒</option>
                                                                <option>大麻</option>
                                                                <option>手機殼</option>
                                                                <option>植株</option>
                                                                <option>種子</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>數量</label><input name="e_count" class="form-control" >
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>單位</label>
                                                        <select name="e_unit" class="form-control">
                                                            <option>包</option>
                                                            <option>顆</option>
                                                            <option>支</option>
                                                            <option>張</option>
                                                            <option>個</option>
                                                            <option>組</option>
                                                            <option>株</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>初驗淨重</label><input name="e_1_N_W" class="form-control" >
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>持有人</label>
														<br/>
                                                        <?php 
														
														foreach ($opt as $key => $value) {
															$data = array(
																'name'          => 'e_s_ic[]',
																'value'         => $key,	
															);
															echo '<div class="checkbox">
																	<label>
																	'.form_checkbox($data) . $value.'
																	</label>
																</div>';
														}
														// echo form_dropdown('e_s_ic',$opt,'' , 'class="form-control"');
														
														?>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <button type="button" name="add" id="add" class="btn btn-success">新增成分</button>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                        <div class="form-group col-md-2">
                                                            <label>初驗毒品成分</label>
                                                            <select name="df[]" class="form-control">
                                                                <option>海洛因</option>
                                                                <option>安非他命</option>
                                                                <option>搖頭丸(MDMA)</option>
                                                                <option>愷他命(KET)</option>
                                                                <option>一粒眠</option>
                                                                <option>大麻</option>
                                                                <option>卡西酮類</option>
                                                                <option>古柯鹼</option>
                                                                <option>無毒品反應</option>
                                                            </select>
                                                        </div>
														<!-- <div class="form-group col-md-1">
                                                            <label>數量</label>
                                                            <input type="number" name="df_num[]" class="form-control">
                                                        </div> -->
                                                </div>

                                                <div class="row" id="drug_s"></div>

                                            <?php 
                                            echo form_close();?>                                  
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $( "#newdrug1" ).validate({
                    rules: {
                        e_1_N_W: {
                            //required: true,
                            number: true,
                            maxlength: 6
                        },
                        e_count: {
                            //required: true,
                            digits: true,
                            maxlength: 6
                        },
                        e_s_ic: {
                            required: true,
                        },
                    },
                    messages: {
                        e_1_N_W: {
                            //required: "此欄位不得為空",
                            number: "請輸入數字",
                            maxlength: "請輸入合理重量",
                        },
                        e_count: {
                            //required: "此欄位不得為空",
                            digits: "請輸入數字",
                            maxlength: "請輸入合理重量",
                        },
                        e_s_ic: {
                            required: "請選擇犯嫌人",
                        },
                    }
                });        
            });
                $(document).ready(function(){      
                    var i=1;  
                    $('#add').click(function(){  
                       i++;  
                       
                       $('#drug_s').append(`<div id="row${i}"><div  class="form-group col-md-2" >
					   <select name="df[]" class="form-control"><option>海洛因</option><option>安非他命</option><option>搖頭丸(MDMA)</option><option>愷他命(KET)</option><option>一粒眠</option><option>大麻</option><option>卡西酮類</option><option>古柯鹼</option><option>無毒品反應</option></select>
					   </div>
					   
					   <div  class="form-group col-md-1" >
					   <button type="button" name="remove" id="${i}" class="btn btn-sm btn-danger btn_remove" style="margin-left: 2px;">X</button>
					   </div>

					   </div>
						`);  
                    });
                    $(document).on('click', '.btn_remove', function(){  
                       var button_id = $(this).attr("id");   
                       $('#row'+button_id+'').remove();  
                    });  
                });  
</script>
