            <div id="page-wrapper">
                <div class="container-fluid">

                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
									<h3 class="panel-title">列表清單</h3>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $data_table;?>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <!-- /.row -->
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#table1').DataTable({
                        dom: 'Bfrtip',
                        buttons: [ 
                            { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                            { extend: 'pageLength', text: '每頁顯示筆數' }
                        ],
                        lengthMenu: [
                            [ 10, 25, 50, -1 ],
                            [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                        ],
                        "language": {
                        "processing": "資料載入中...",
                        "lengthMenu": "每頁顯示 _MENU_ 筆",
                        "zeroRecords": "資料庫中未有相關資料。",
                        "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                        "infoEmpty": "資料庫中未有相關資料。",
                        "search": "搜尋:",
                        select: {
                        rows: "選取 %d 列"
                        },
                        "paginate": {
                            "first": "第一頁",
                            "last": "最後一頁",
                            "next": "下一頁",
                            "previous": "上一頁"
                        },
                        buttons: {
                            pageLength: {
                                _: "每頁顯示 %d 筆",
                                '-1': "顯示全部"
                            }
                        }
                    },
                        responsive: true
                });
            });
        </script>
