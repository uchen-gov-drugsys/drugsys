        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">

                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <a><input type="submit" value="新增" class="btn btn-success"  form="create_suspect1" id="save"></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">
									<h3 class="panel-title">案件編號： <?php echo $s_cnum; ?>  <span class="label label-success">犯嫌新增</span></h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <?php echo form_open_multipart('cases2/create_suspect1','id="create_suspect1"') ?>
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <label>犯嫌姓名</label>
                                                        <input class="form-control" name="s_name" >
                                                        <input id="link" type="hidden" name="link" value=" ">                 
                                                        <input id="s_cnum" type="hidden" name="s_cnum" value=<?php echo $s_cnum;?>>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>犯嫌身份證</label>
                                                        <input class="form-control" name="s_ic" id="s_ic" >
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>特徵</label>
                                                        <input class="form-control" name="s_feature" >
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>性別</label><br>
                                                        <?php
                                                            echo form_radio('s_gender', '男',false).'男';
                                                            echo form_radio('s_gender', '女',false).'女';
                                                        ?>      
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>綽號</label>
                                                        <input class="form-control" name="s_nname">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>外國籍</label>
                                                        <input class="form-control" value="中華民國" name="s_nation">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <label>出生年月日</label>
                                                        <input class="rcdate form-control" id="date" type="text" name="s_birth" widt='100px'>
														<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>教育程度</label>
                                                        <select name="s_edu" class="form-control">
                                                            <option>未受教育</option>
                                                            <option>國小</option>
                                                            <option>國中</option>
                                                            <option>高中</option>
                                                            <option>大學</option>
                                                            <option>碩士</option>
                                                            <option>博士</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>職業</label>
                                                        <input name="s_job" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>幫派堂口名稱</label>
                                                        <input name="s_gang" class="form-control"> 
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>列管應受尿液採檢人口</label>
                                                        <br>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="s_U_Col" id="optionsRadiosInline1" value="是" checked>是
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="s_U_Col" id="optionsRadiosInline2" value="否">否
                                                        </label> 
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                    <label>犯罪手法</label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="持有" checked=checked/>持有 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="意圖販售而持有" checked=checked/>意圖販售而持有 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="販賣" checked=checked/>販賣 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="運輸" checked=checked />運輸</label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="施用" checked=checked/>施用</label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="栽種" checked=checked />栽種 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="製造"checked=checked />製造 </label>
                                                        <label class="checkbox-inline"><input type="checkbox" name="s_CMethods[]" value="轉讓" checked=checked />轉讓 </label>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>戶籍地</label>
                                                        <div id="zipcode">
                                                        </div>
                                                        <textarea name="s_dpaddress" id="s_no" class="form-control" placeholder="路門牌"></textarea>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>現居地</label>
                                                        <label class='receiverchange checkbox-inline checkboxeach'><input id='receiverchange' class='receiverchange btn btn-default btn-sm' type='button' name='receiverchange' value='戶籍地與現居地相同'></label>
                                                        <div id="zipcode2">
                                                        </div>
                                                        <textarea name="s_rpaddress" id="s_no2" class="form-control"  placeholder="路門牌"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                        <div id="mark1" class="form-group">
                                                            <label>溯源可能性</label>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name='s_resource' id="optionsRadiosYes" value="yes" checked>是
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name='s_resource' id="optionsRadiosNo" value="no">否
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4" id="sel_td">
                                                        <label>未指認毒品來源-未指認原因</label>
                                                        <select id="sel" name='s_noresource' class="form-control">
                                                            <option selected value=" ">請選擇 </option>
                                                            <option value="忘記來源">忘記來源</option>
                                                            <option value="否認施用">否認施用</option>
                                                            <option value="國外購入">國外購入</option>
                                                            <option value="6">其他</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>犯嫌照片</label>
                                                        <input name="susppic" type="file">
														<span class="text-danger"><small>限上傳gif, jpg, png類型之檔案且檔案大小限制10MB</small> </span>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6"><h3 class="panel-title">指認毒品來源(溯源)</h3></div>
                                        <div class="col-md-6 text-right">
                                            <label style="float:right"><input id="source"  class="btn btn-default btn-sm" type="button" value="增加" /></label>                
                                        </div>                                                       
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php//echo $s_table;?>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6"><h3 class="panel-title">犯嫌持有門號</h3></div>
                                        <div class="col-md-6 text-right">
                                            <label style="float:right"><input id="phone" class="btn btn-default btn-sm" type="button" value="增加" /></label>              
                                        </div>                                                       
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php//echo $s_table;?>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6"><h3 class="panel-title">擁有社群帳號</h3></div>
                                        <div class="col-md-6 text-right">
                                            <label style="float:right"><input id="social"  class="btn btn-default btn-sm" type="button" value="增加" /></label>           
                                        </div>                                                       
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php//echo $s_table;?>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6"><h3 class="panel-title">交通工具</h3></div>
                                        <div class="col-md-6 text-right">
                                            <label style="float:right"><input id="car"  class="btn btn-default btn-sm" type="button" value="增加" /></label>          
                                        </div>                                                       
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php//echo $s_table;?>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
			$(".rcdate").change(function(){
				let strlen = $(this).val().length;
				if(strlen < 7 || strlen < "7")
				{
					if(strlen != 0)
						$(this).val("0"+$(this).val());
				}
				else
				{
					return false;
				}
					
			});
			$(".rcdate").keypress(function(){
				if($(this).val().length >= 7)
					return false;
			});
            $("#zipcode").twzipcode({
                "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_dpcounty',   // 預設值為 county
                'districtName' : 's_dpdistrict', // 預設值為 district
                'zipcodeName'  : 's_dpzipcode',
                'zipcodeSel'   : <?php if(isset($row->s_dpzipcode)){echo $row->s_dpzipcode;}else{echo '100';}  ?>
            });         
            
            $("#zipcode2").twzipcode({
                "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_rpcounty',   // 預設值為 county
                'districtName' : 's_rpdistrict', // 預設值為 district
                'zipcodeName'  : 's_rpzipcode',
                'zipcodeSel'   : <?php if(isset($row->s_rpzipcode)){echo $row->s_rpzipcode;}else{echo '100';}  ?>
            }); 
            $('#receiverchange').on('click', function() {
               // $("#zipcode2").empty();
                var city = $(zipcode).twzipcode('get', 'city');
                var zipc = $(zipcode).twzipcode('get', 'zipcode');
                console.log(zipc);   // 縣市
                if( zipc == ""){
                    alert("請務必選擇鄉鎮市區");return false;
                } 
                $("#zipcode2").twzipcode('set', zipc[0]);
                var sno=$("#s_no").val();

                // 移除空格確定是否空值
                if (!$.trim($("#s_no").val())) {
                    if( $("#s_no").val()==""){
                        $("#s_no").focus();
                        alert("請務必填入路門牌");return false;
                    }     
                }   
                $("#s_no2").val(sno);
            }); 
            
           $('input[name=s_resource]').change(function(){
                var value = $( this ).val();
                if(value=='yes'){
                    $('#sel').attr('disabled', true);
                }
                else{
                    $('#sel').attr('disabled', false);
                }
            }); 
            
           $("#sel").change(function(){
                switch (parseInt($(this).val())){
                    case 0:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 1:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 2:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 3:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 4:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 5:
                    $("#sel_td textarea").remove(); 
                    break;
                    case 6:
                    $("#sel_td").append($("<textarea id='del' name='s_noresource' class='form-control' placeholder='請描述其他未指認原因'></textarea>"));
                    break;
                }
            });              
 
            $(document).ready(function(){
                $('#sel').attr('disabled', true);
                $("#save").click(function (){
                    $("#link").val('cases2/edit/'+$("#s_cnum").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $("#source").click(function (){
                    $("#link").val('cases2/newsource/'+$("#s_ic").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $("#phone").click(function (){
                    $("#link").val('cases2/newphone/'+$("#s_ic").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $("#social").click(function (){
                    $("#link").val('cases2/newsocialmedia/'+$("#s_ic").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $("#car").click(function (){
                    $("#link").val('cases2/newcar/'+$("#s_ic").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $( "#create_suspect1" ).validate({
                    rules: {
                        s_name: {
                            required: true,
                            minlength: 2
                        },
                        s_birth: {
                            required: true,
                        },
                        s_ic: {
                            required: true,
                            minlength: 9
                        },
                        susppic: {
                            extension: "jpg|jpeg|png|gif"
                        },
                        s_dpdistrict: {
                            required: true,
                        },
                        s_dpaddress: {
                            required: true,
                        },
                    },
                    messages: {
                        s_name: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        s_birth: {
                            required: "此欄位不得為空",
                        },
                        s_ic: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        susppic: {
                            extension: "只接受jpg|jpeg|png|gif"
                        },
                        s_dpdistrict: {
                            required: "此欄位不得為空",
                        },
                        s_dpaddress: {
                            required: "此欄位不得為空",
                        },
                    }
                });        
            // $('#date').datepicker({//bootstrap datepicker 1.9v 民國修改版
            //     format: "yyyy-mm-dd", //twy為民國
            //     autoclose: true,
            //     todayHighlight: true,
            //     language: 'zh-TW'      
            // });
            });            
        </script>
    </body>
</html>
