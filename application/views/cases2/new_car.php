        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h4><?php //echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="createcar"></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							<a><input type="submit" value="儲存" class="btn btn-warning" form="createcar"></a>
                        </div>
                    </div>
                    
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">
									<h3 class="panel-title">交通工具</h3>
                                </div>
                                <div class="panel-body">
								<?php echo form_open_multipart('Cases2/createcar','id="createcar"') ?>
									<div class="row">
                                        <div class="col-md-12 text-right">
											<button type="button" name="add" id="add" class="btn btn-default btn-sm">新增車輛</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
											<div id="car">
												<div class="row" style="border-left:5px solid #337ab7;margin-left:20px;margin-bottom:10px;">
													<div class="form-group col-md-2">
														<label>交通工具種類</label>
														<select name="v_type[]" class="form-control">
															<option value="機車">機車</option>
															<option value="客車">客車</option>
															<option value="貨車">貨車</option>
														</select>
													</div>
													<div class="form-group col-md-2">
														<label>車牌號碼</label>
														<input name="v_license_no[]" class="form-control">
														<?php echo form_hidden('s_ic',$s_ic);?>
														<?php echo form_hidden('s_num',$s_num);?>
													</div>
													<div class="form-group col-md-2">
														<label>顏色</label>
														<input name="v_color[]" class="form-control">
													</div>
												</div>
											</div>
                                                
                                        </div>
                                    </div>
								<?php echo form_close(); ?>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
            $(document).ready(function(){
                var i=1;  
                $('#add').click(function(){  
                    i++;  
					$("#car").append(`<div class="row" id="row${i}" style="border-left:5px solid${((i%2) == 1)?'#337ab7':'#5bc0de'};margin-left:20px;margin-bottom:10px;">
                                                    <div class="form-group col-md-2">
                                                        <label>交通工具種類</label>
                                                        <select name="v_type[]" class="form-control">
                                                            <option value="機車">機車</option>
                                                            <option value="客車">客車</option>
                                                            <option value="貨車">貨車</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>車牌號碼</label>
                                                        <input name="v_license_no[]" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label>顏色</label>
                                                        <input name="v_color[]" class="form-control">   
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <button type="button" name="remove" id="${i}" class="btn btn-danger btn_remove">刪除</button>   
                                                    </div>
                                                </div>`);
                    // $('#car').append('<tr id="row'+i+'">'+
                    //         '<td><label>交通工具種類</label>'+
                    //         '<select name="v_type[]" class="form-control">'+
                    //             '<option value="機車">機車</option>'+
                    //             '<option value="客車">客車</option>'+
                    //             '<option value="貨車">貨車</option>'+
                    //         '</select>'+
                    //         '</td><td><label>車牌號碼</label><input name="v_license_no[]" class="form-control"></td>'+
                    //         '<td><label>顏色</label><input name="v_color[]" class="form-control"></td>'+
                    //         '<td>'+
                    //         '<button type="button" name="remove" id="'+i+'"'+
                    //         'class="btn btn-danger btn_remove">X</button>'+
                    //     '</td>'+
                    //     '</tr>');  
                });
                $(document).on('click', '.btn_remove', function(){  
                    var button_id = $(this).attr("id");   
                    $('#row'+button_id+'').remove();  
                });  
                $( "#createcar" ).validate({
                    rules: {
                        "v_license_no[]": {
                            required: true,
                        },
                    },
                    messages: {
                        "v_license_no[]": {
                            required: "此欄位不得為空",
                        },
                    }
                });        

            });
            
        </script>
    </body>
</html>
