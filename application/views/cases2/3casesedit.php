        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li></li>
                    <li></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							
							<!-- <button class="btn btn-default" data-toggle="modal" data-target="#filereview" data-whatever="文件檢視">文件檢視</button> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
									<div class="row">
										<div class="col-md-6">
										可裁罰案件
										</div>
										<div class="col-md-6 text-right">
											<input type="submit" value="儲存" class="btn btn-warning btn-sm"id="update">
										</div>
									</div>
                                    
                                </div>
                                <div class="panel-body">
                                <?php echo form_open_multipart('Cases2/updateSusp3','id="create_suspect1"') ?>
                                    <div class="row">
										<div class="form-group col-md-3">
											<label>分局發文字號</label>
											<?php 
												if(!isset($susp->s_go_no)){
															echo form_input('s_go_no','', 'class="form-control"');
														}
														else {
															echo form_input('s_go_no',$susp->s_go_no, 'class="form-control"');
														}
											?>
										</div>
										<div class="form-group col-md-3 ">
											<label>涉刑事案件司法文書</label>
											<?php 
													if(!isset($susp->sp_doc)){
														echo form_upload('sp_doc');
													}
													else {
														echo anchor_popup('drugdoc/' . $susp->sp_doc, '</br>司法文書');
														echo form_hidden('sp_doc',$susp->sp_doc);
													}
											?>  
											<span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制10MB</small> </span>
										</div>
										<div class="form-group col-md-3 col-md-offset-3">
											<label>處分書狀態</label>
											<?php echo form_dropdown('s_sac_state', $opt ,$susp->s_sac_state , 'class="form-control" ');?>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label>姓名</label>
											<?php echo form_input('s_name',$susp->s_name, 'class="form-control"')?>
										</div>
										<div class="form-group col-md-3">
											<label>出生日期</label>
											<input name='s_birth' class="rcdate form-control" value='<?php echo (isset($susp->s_birth))?((strlen($susp->s_birth) > 7 && $susp->s_birth != '0000-00-00')?str_pad(((int)substr($susp->s_birth, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($susp->s_birth, 5, 2).substr($susp->s_birth, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md')?>' type="text"><span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
										</div>
										<div class="form-group col-md-3">
											<label>身分證字號</label>
											<?php echo form_input('s_ic',$susp->s_ic, 'class="form-control"')?>
											
										</div>
										<div class="form-group col-md-12">
											<label>戶籍地址</label>
											<?php //echo form_input('s_dpzipcode',$susp->s_dpzipcode, 'class="form-control"')?>
											<?php //echo form_input('s_dpcounty',$susp->s_dpcounty, 'class="form-control"')?>
											<?php //echo form_input('s_dpdistrict',$susp->s_dpdistrict, 'class="form-control"')?>
											<?php echo form_input('s_dpaddress',$susp->s_dpzipcode.$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress, 'class="form-control"')?>
											
											<input type="hidden" id='link' name='link' class="form-control">
											<input type="hidden" name='s_num' class="form-control" value=<?php echo $susp->s_num?>>
											<input type="hidden" id='c_num' name='c_num' class="form-control" value=<?php echo $cases->c_num?> >
										</div>
										<div class="form-group col-md-12">
											<label>現住地址</label>
											<?php //echo form_input('s_rpzipcode',$susp->s_rpzipcode, 'class="form-control"')?>
											<?php //echo form_input('s_rpcounty',$susp->s_rpcounty, 'class="form-control"')?>
											<?php //echo form_input('s_rpdistrict',$susp->s_rpdistrict, 'class="form-control"')?>
											<?php echo form_input('s_rpaddress',$susp->s_rpzipcode.$susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress, 'class="form-control"')?>
											
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label>查獲單位</label>
											<?php echo form_input('s_office',$cases->s_office, 'class="form-control" readonly')?>
										</div>
										<div class="form-group col-md-3">
											<label>查獲時間</label>
											<input name='s_date' class="rcdate form-control" value=<?php echo (isset($cases->s_date))?((strlen($cases->s_date) > 7 && $cases->s_date != '0000-00-00')?str_pad(((int)substr($cases->s_date, 0, 4)- 1911),3,"0",STR_PAD_LEFT).substr($cases->s_date, 5, 2).substr($cases->s_date, 8, 2):((int)date('Y') - 1911).date('md')):((int)date('Y') - 1911).date('md')?> type="text">
											<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
										</div>
										<div class="form-group col-md-6">
											<label>查獲地點</label>
											<?php echo form_textarea('r_county',$cases->s_place, 'class="form-control" readonly style="height:8em;"')?>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3">
											<label>尿液編號</label>
											<?php echo form_input('s_utest_num',$susp->s_utest_num, 'class="form-control"')?>
										</div>
										<div class="form-group col-md-3">
											<label>尿檢級別成分</label>
											<?php 
												foreach ($susp2 as $ss){
													if($ss->sc_num == null){}
													else{
														echo '<br>'.$ss->sc_level .'級' . $ss->sc_ingredient;
													}
												}?>
										</div>
										<div class="form-group col-md-3">
											<label>持有毒品</label>
											<?php 
												foreach ($drug1 as $df){
													if($df->df_num == null){}
													else{
														echo '<br>'.$df->df_level .'級' . $df->df_ingredient;
													}
												}
											?>
										</div>
										<div class="form-group col-md-3">
											<input type="submit" value="前往修改成分與毒品" class="btn btn-info btn-sm" form="create_suspect1" id="editsusp">
										</div>
									</div>  
									</form>                  
                                </div>
                                <div class="modal fade" id="filereview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">文件檢視</h5>
                                            </div>
                                            <div class="modal-body">
                                                
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                        <div class="col-lg-4">
							<div class="row">
								<div class="col-md-3">
									<label>文件預覽：</label>
								</div>
								<div class="col-md-9 text-right">
									<a class="pdfopener" href="#" id="test1"><button class="btn btn-default btn-sm" data-dismiss="modal">戶役政/筆錄/搜扣</button></a>
									<a class="pdfopener" href="#" id="otherfiles"><button class="btn btn-default btn-sm" data-dismiss="modal">其他檔案</button></a>
									<a class="pdfopener" href="#" id="test3"><button class="btn btn-default btn-sm" data-dismiss="modal">毒報</button></a>
									<a class="pdfopener" href="#" id="test2"><button class="btn btn-default btn-sm" data-dismiss="modal">尿報</button></a>
								</div>
							</div>
							
                            <iframe style="width:100%;height:800px" frameborder="0" name="testpdf" id="testpdf"></iframe>

                            <iframe style="width:100%;height:800px" frameborder="0" name="songda" id="songda"></iframe>

                            <iframe style="width:100%;height:800px" frameborder="0" name="chufen" id="chufen"></iframe>

                            <iframe style="width:100%;height:800px" frameborder="0" name="niao" id="niao"></iframe>
                            <!-- <div id="media1" class="panel panel-default">
                                <div id="media1" class="panel-body">
                                    <a class="pdfopener" href="#" id="close"><button class="btn btn-default">close</button>
                                    <a id="testpdf" class='media' href=<?php //echo base_url("uploads/") . $cases->doc_file ?>></a>
                                    <a id="songda" class='media' href=<?php //echo base_url("uploads/") . $cases->other_doc ?>></a>
                                    <a id="chufen" class='media' href=<?php //echo base_url("utest/") . $susp->s_utest_doc ?>></a>
                                    <a id="niao" class='media' href=<?php //echo base_url("drugdoc/") . $drug->e_doc ?>></a>
                                </div>
                            </div> -->
                        </div>            
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript"> 
            $(document).ready(function() {
				$(".rcdate").change(function(){
					let strlen = $(this).val().length;
					if(strlen < 7 || strlen < "7")
					{
						if(strlen != 0)
							$(this).val("0"+$(this).val());
					}
					else
					{
						return false;
					}
						
				});
				$(".rcdate").keypress(function(){
					if($(this).val().length >= 7)
						return false;
				});
                $('a.media').media({width:1200, height:700});  
                $('#media1').hide();
                $('.media').hide();
                
                $("#editsusp").click(function (){
                    $("#link").val('cases2/editCases2/'+$("#c_num").val());
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $("#update").click(function (){
                    $("#link").val('cases2/list3Cases');
                    //alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $( "#create_suspect1" ).validate({
                    rules: {
                        'sp_doc': {
                            extension: "pdf|doc|docx"
                        },
                    },
                    messages: {
                        'sp_doc': {
                            extension: "只接受pdf|doc|docx類型",
                        },
                    }
                });
                var table = $('#example').DataTable({
                });
                // Add event listener for opening and closing details
                $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                    }
                });

                $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                // Get the column API object
                var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                    });
                    $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                });
              
               
            });    
            
            $(function() {
                $('.pdfopener').click(function(e) {
                    e.preventDefault();
                    var id = $(this).attr('id');
                    switch(id){
                        case 'test1':
							var $iFrame=$("#testpdf");
                			$iFrame.prop("src","<?php echo base_url("uploads/") . $cases->doc_file;?>");
                            $('.media').hide();  
                            $('#media1').show();  
                            $('#testpdf').show();  

							$('#chufen').hide();
							$('#songda').hide();
							$('#niao').hide();
                            break;
                        case 'test2':
							var $iFrame=$("#chufen");
                			$iFrame.prop("src","<?php echo base_url("utest/") . $susp->s_utest_doc;?>");
                            $('.media').hide();  
                            $('#media1').show();  
                            $('#chufen').show();  

							$('#testpdf').hide();
							$('#songda').hide();
							$('#niao').hide();
                            break;
                        case 'otherfiles':
							var $iFrame=$("#songda");
                			$iFrame.prop("src","<?php echo base_url("uploads/") . $cases->other_doc;?>");
                            $('.media').hide();  
                            $('#media1').show();  
                            $('#songda').show();  

							$('#testpdf').hide();
							$('#chufen').hide();
							$('#niao').hide();
                            break;
                        case 'test3':
							var $iFrame=$("#niao");
                			$iFrame.prop("src","<?php echo base_url("drugdoc/") . $cases->drug_doc;?>");
                            $('.media').hide();  
                            $('#media1').show();  
                            $('#niao').show();  

							$('#testpdf').hide();
							$('#chufen').hide();
							$('#songda').hide();
                            break;
                        case 'close':
                            $('iframe').hide();  
                            break;
                    }
                });
            });            
            </script>
