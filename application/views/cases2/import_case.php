            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
													<!-- <button class="btn btn-default" id="updateSanc" onclick="importEvent();">更新/載入資料</button> -->
                        </div>
                    </div>

                    <!-- /.row -->
                    <div class="row">
												<div class="col-lg-6">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    案件新增
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
											
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label>選擇現有案件</label>
															<?php echo form_dropdown('select-cases', $opt_cases, '', 'class="form-control" onchange="loadExistCases($(this))"') ?>
														</div>
													</div>
												</div>
                                                <?php echo form_open_multipart('cases2/createcases','id="newcases"') ?>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>查獲單位 <span class="text-danger">*</span></label>
                                                        <?php echo form_error('s_office');?>
                                                        <?php echo form_input('s_office',$s_office, 'class="form-control" readonly')?>   
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>案件編號 <span class="text-danger">*</span></label>
                                                        <?php echo form_error('c_num');?>
                                                        <?php echo form_input('c_num','', 'class="form-control" id="cnum" readonly')?>                            
                                                        <input id="link" type="hidden" name="link" value=" ">   
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>查獲時間 <span class="text-danger">*</span><?php echo form_error('s_date');?></label>
                                                        <?php echo form_input('s_date','', 'class="rcdate form-control" id="date" readonly')?>  
																												<span class="text-danger"><small>(如：民國60年1月1日，請輸入0600101)</small> </span>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>收案單位 <span class="text-danger">*</span></label>
                                                        <?php if($r_office == "保安警察大隊" || $r_office == "捷運警察隊" || $s_office == "特勤中隊"){
                                                            echo form_dropdown('r_office',$opt ,$r_office , 'class="form-control" id="sel" readonly');
                                                        }                            
                                                        else
                                                            echo form_input('r_office',$r_office, 'class="form-control" readonly')?> 
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>查獲地點 <span class="text-danger">*</span><?php echo form_error('r_zipcode');?></label>
                                                        <div id="zipcode">
                                                        </div>
                                                        <div class="form-group">
                                                            <input style="text-transform: uppercase;" name="r_address" value="<?php echo set_value('r_address');?>" class="form-control" placeholder="XX路XX門牌" readonly>     
																	<label>
																		<?php echo form_error('r_address');?>
																	</label>                                                           
                                                        </div> 
														
                                                    </div>
                                                    <div class="form-group col-md-6" id ="sel_td">
                                                        <label>偵破過程 <span class="text-danger">*</span></label>
														<?php echo form_input('detection_process','', 'class="form-control" readonly'); ?>
                                                        <!-- <select name="detection_process" id="sel" class="form-control" readonly>
                                                            <option value="路檢攔查">路檢攔查</option>
                                                            <option value="臨檢">臨檢</option>
                                                            <option value="專案勤務(無令狀)">專案勤務(無令狀)</option>
                                                            <option value="毒品調驗人口">毒品調驗人口</option>
                                                            <option value="拘票拘提">拘票拘提</option>
                                                            <option value="持票搜索">持票搜索</option>
                                                            <option value="通知到案">通知到案</option>
                                                            <option value="借訊(提)">借訊(提)</option>
                                                            <option value="報案(110)">報案(110)</option>
                                                            <option value="99">其他</option>
                                                        </select> -->
														<label>營業場所名稱</label>
                                                        <input style="text-transform: uppercase;" name="s_place" class="form-control" placeholder="例:XX旅館 / XX酒店" readonly> 
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-4 hidden">
														<label>戶役政/筆錄/搜扣</label>
														<?php echo form_upload('doc_file'); ?>    
														<span class="text-danger"><small>限上傳pdf, doc, docx類型之檔案且檔案大小限制5MB</small> </span>                                                      
                                                    </div>
                                                    <div class="form-group col-md-4 hidden">
                                                        
                                                        <label>其他檔案(逮捕通知書/拘票/同意書/地檢署處分命令)</label>
																												<input type='file' name='other_doc' >
                                                        <span class="text-danger"><small>限上傳pdf,doc,docx類型之檔案且檔案大小限制5MB</small> </span>
                                                    </div>
													<div class="form-group col-md-4 hidden">
														<label>毒品照片(所有)</label>
														<input type='file' name='drugpic'>
														<span class="text-danger"><small>限上傳jpg, jpeg, png, gif, pdf, doc, docx類型之檔案且檔案大小限制5MB</small> </span>
                                                    </div>
                                                </div>
												<input type="hidden" name="s_ic" value="">
												<input type="submit" class="btn btn-default pull-right" value="送出">
                                            <?php echo form_close();?>                                  
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
						</div>
						<div class="col-lg-6">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row ">
                                        <div class="col-md-6">犯嫌清單</div>
                                        <!-- <div class="col-md-6 text-right">
                                            <label><input id="susp"  class="btn btn-sm btn-default" type="button" value="新增" /></label>
                                        </div>                                                        -->
                                    </div>
                                </div>
                                <div class="panel-body">																		
                                    <div class="table-responsive">
										<div class="lds-dual-ring hidden"></div>
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
					<div class="row">
						<div class="col-md-6">
							<h5>已選取</h5>
							<div id="select-wrap">

							</div>
						</div>
					</div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
			<!-- 移轉作業視窗 -->
			<div class="modal fade" id="trandferModal" tabindex="-1" role="dialog" aria-labelledby="paydateModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header bg-primary">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">移轉作業</h4>
						</div>
						<?php echo form_open('#','id="transferform"'); ?>
						<div class="modal-body">							
							<div class="form-group">
								<label>移轉單位</label>
								<br>
								<select name="roffice" id="roffice" style="width: 100%" class="form-control js-example-basic-single js-states input-lg" ></select>
								<select name="rofficeunit" id="rofficeunit" style="width: 100%" class="form-control js-example-basic-single js-states input-lg" ></select>
							</div>
						</div>
						<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
						<?php echo form_submit('save_dpdate', '移轉', "class='btn btn-default'"); ?>
						<!-- <button id='no' class="btn btn-default" >儲存</button> -->
						</div>
						<?php  echo form_close();  ?>
					</div>
				</div>
			</div> 
        </div>
        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            /*$(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });*/
            $(document).ready(function() {
							chooseAry = [];
              var table = $('#table1').DataTable({
				columnDefs: [ {
						targets:   0,
						visible: false
				},{
						className: 'select-checkbox',
						targets:   1,
				} ],
				select: {
						style:    'os',
						selector: 'td:first-child',
						style: 'multi'
				},
				"fnRowCallback": function (nRow, aData, iDisplayIndex) {
						$(nRow).attr("data-id", aData[0]);
						return nRow;
				},
                dom: 'Bfrtip',
                buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
                ],
                lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                ],
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    }
                }, 
              });

			$('#table1 tbody').on( 'click', 'tr', function () {
					$(this).toggleClass('selected');
					
					if($(this).hasClass('selected'))
					{
							chooseAry.push({
									userid: $(this).children('td:eq(2)').text(),
									username: $(this).children('td:eq(3)').text(),                    
							})
							nowChooseEvent(chooseAry)
					}
					else
					{
							delAry = {
								userid: $(this).children('td:eq(2)').text(),
								username: $(this).children('td:eq(3)').text(),
									
							};
							chooseAry.forEach(function(item, index, arr) {
									if(JSON.stringify(item) === JSON.stringify(delAry)) {
											return arr.splice(index, 1);
									}
							});
							nowChooseEvent(chooseAry)
					}
			} );
			$('#trandferModal').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) // Button that triggered the modal
				var href = button.data('href') // Extract info from data-* attributes
				// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				var modal = $(this)
				modal.find('#transferform').attr('action', '<?php echo base_url('/');?>'+href);
			})
			$('#roffice').select2({
				minimumResultsForSearch: Infinity,
				placeholder: "選擇其他分局",
				ajax: {
					url: '<?php echo base_url("Cases2/getotherdep") ?>',
					dataType: 'json',
					data: function (params) {
					var query = {
						unselected:'',
						search: params.term,
						type: 'public'
					}

					// Query parameters will be ?search=[term]&type=public
					return query;
					}
				}
			});
			$('#rofficeunit').select2({
				minimumResultsForSearch: Infinity,
				placeholder: "選擇其他派出所/大隊",
				ajax: {
					url: '<?php echo base_url("Cases2/getotherdepunit") ?>',
					dataType: 'json',
					data: function (params) {
					var query = {
						unselected:'',
						parent: $('#roffice').val(),
						search: params.term,
						// type: 'public'
					}

					// Query parameters will be ?search=[term]&type=public
					return query;
					}
				}
			});
			$('#roffice').change(function(){
				$('#rofficeunit').val('').change(); 
			});
			$('#trandferModal').on('hide.bs.modal', function (event) {
				$('#roffice').val('').change(); 
				$('#rofficeunit').val('').change(); 
			});

              // Add event listener for opening and closing details
              $('#table1 tbody').on('click', 'td.details-control', function() {
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                  // This row is already open - close it
                  row.child.hide();
                  tr.removeClass('shown');
                } else {
                  // Open this row
                  row.child(format(row.data())).show();
                  tr.addClass('shown');
                }
              });

              $('a.toggle-vis').on('click', function(e) {
                e.preventDefault();

                // Get the column API object
                var column = table.column($(this).attr('data-column'));

                // Toggle the visibility
                column.visible(!column.visible());
              });
              $('.list_view input[type="checkbox"]').on('change', function(e) {


                // Get the column API object
                var col = table.column($(this).attr('data-target'));

                // Toggle the visibility
                col.visible(!col.visible());
              });
            });
						function importEvent(){
							$('.lds-dual-ring').removeClass('hidden');
										$.ajax({            
												type: 'POST',
												url: '<?php echo base_url('Cases2/importdata'); ?>',
												// dataType: 'json',
												contentType: false,
												cache: false,
												processData:false,
												beforeSend:function(){
														$("#loadingTxt").text("資料匯入中...請稍候...請勿關掉或跳出畫面喔！電腦會壞掉!");
												},
												error:function(){
														$('.lds-dual-ring').addClass('hidden');
														$("#uploadtxt").text("資料匯入出現錯誤，請重新執行操作！");
												},
												success: function(resp){
														$('.lds-dual-ring').addClass('hidden');
														if(resp == 'ok'){
																$("#uploadtxt").text("資料匯入成功，可以進一步搜尋資料囉！");
																location.reload();
														}else if(resp == 'error'){
																$("#uploadtxt").text("資料匯入失敗，請重新執行操作！");
														}
												},
												complete:function(){
														$("#uploadtxt").text("資料匯入成功，可以進一步搜尋資料囉！");
												}
										});
								
								}
								function nowChooseEvent(ary)
								{
										str = '';
										$("input[name='s_ic']").val(JSON.stringify(chooseAry));
										ary.forEach(function(item, index, arr) {
												str += '<span style="border-bottom:1px solid #d4d4d4;margin-right:20px;">'+item['userid']+' '+item['username']+'</span>';
										});
										$("#select-wrap").html(str);
								}
								function loadExistCases(obj)
								{
									let formdata = new FormData();
									formdata.append('caseid', obj.val())
									$.ajax({            
										type: 'POST',
										url: '<?php echo base_url('Cases2/getCaseData') ?>',
										data: formdata,
										dataType: 'json',
										processData : false, 
										contentType: false,
										cache: false,
										success: function(resp){
											if(resp.status == 200)
											{
												$("input[name='s_office']").val(resp.data[0].s_office);
												$("input[name='c_num']").val(resp.data[0].c_num);
												$("input[name='s_date']").val(resp.data[0].s_date);
												$("input[name='r_office']").val(resp.data[0].r_office);
												$("input[name='r_address']").val(resp.data[0].r_address);
												$("input[name='detection_process']").val(resp.data[0].detection_process);
												$("input[name='s_place']").val(resp.data[0].s_place);

											}
											else
											{
												alert(resp.msg);
											}
											
										}
									});
								}
        </script>
