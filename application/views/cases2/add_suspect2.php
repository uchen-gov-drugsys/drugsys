        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="create_suspect1" id="save"></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    </br>新增犯嫌
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <?php echo form_open_multipart('cases2/createSusp2','id="create_suspect1"') ?>          
                                            <table class="table">
                                                <tbody id="drugall" class="form-group">
                                                    <tr>
                                                        <td><label>姓名</label>
                                                            <?php echo form_input('s_name','', 'class="form-control"')?>                            
                                                            <input id="link" type="hidden" name="link" value=" ">                           
                                                            <input id="s_num" type="hidden" name="s_num"> 
                                                                <input id="s_cnum" type="hidden" name="s_cnum" value=<?php echo $s_cnum;?>> 
                                                        </td>
                                                        <td><label>證號</label>
                                                            <?php echo form_input('s_ic','', 'class="form-control" id="s_ic"')?>                            
                                                        </td>
                                                        <td><label>尿液編號</label><?php echo form_input('s_utest_num','', 'class="form-control"')?></td>
                                                        <td style="width:300px">
                                                            <div id="drug_s">
                                                                <label>尿液成分</label>
                                                                    <?php 
                                                                    foreach ($susp2 as $susp2)
                                                                    {
                                                                        echo '<div id="row">';
                                                                        echo form_dropdown('sc[]',$opt ,$susp2->sc_level .'級'. $susp2->sc_ingredient , 'class="form-control" id="sel"');
                                                                        echo form_hidden('sc_num[]',$susp2->sc_num); 
                                                                        echo anchor('cases2/deleteSuspCk/'. $susp2->sc_num,'X','class="btn btn-danger btn_remove"');
                                                                        //echo '<button type="button" class="btn btn-danger btn_remove">X</button></';
                                                                        echo '</div>';
                                                                    }   
                                                                    ?>                                                            
                                                            </div>
                                                            <label><input type="button" value="新增成分" class="btn btn-success" id="new"></label>
                                                        </td>
                                                        <td  style="width:10px">
                                                            <label>尿報</br></label>
                                                                <?php 
                                                                    echo form_upload('s_utest_doc');
                                                            ?>                                                            
                                                        </td>
                                                        <td>
                                                            <label>處分書狀態</label>
                                                            <?php echo form_dropdown('s_sac_state', $opt2 ,'' , 'class="form-control" ');?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript"> 
            $(document).ready(function(){
                $("#new").click(function (){
                    $("#link").val('1');
                    alert($("#link").val());
                    $("#create_suspect1").submit();
                });
                $( "#create_suspect1" ).validate({
                    rules: {
                        s_name: {
                            required: true,
                            minlength: 2
                        },
                        s_ic: {
                            required: true,
                            minlength: 9
                        },
                        sc_no: {
                            required: true,
                        },
                    },
                    messages: {
                        s_name: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        s_ic: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        sc_no: {
                            required: "此欄位不得為空",
                        },
                    }
                });        
            });            
        </script>
    </body>
</html>
