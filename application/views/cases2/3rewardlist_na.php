        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    
                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
                            <button id='yes' class="btn btn-success">建立新專案</button>
                            <button class="btn btn-default"data-toggle="modal" data-target="#exampleModal">加入舊專案</button>
                        </div>
                    </div>

                    

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                  列表清單
                                </div>
                                <!-- /.panel-heading -->
                                <?php echo form_open_multipart('cases2/addRewardProject_na','id="sp_checkbox"') ?>  
                                <div class="panel-body">
									<div class="row" style="margin:20px 0px;">
										<div class="col-md-6 text-right">
											<div class="col-lg-10">
												<div class="form-group">
													<div class="input-group">
														<span class="input-group-addon">開始日期</span>
														<input type='text'  readonly id='search_fromdate' class="datepicker form-control" placeholder='開始日期'>
														<span class="input-group-addon">結束日期</span>
														<input type='text' readonly id='search_todate' class="datepicker form-control" placeholder='結束日期'>
													</div><!-- /input-group -->
												</div>
											</div><!-- /.col-lg-11 -->
											<div class="col-lg-2">
												<div class="input-group">
													<input type='button' class="btn btn-default" id="btn_search" value="搜尋">
												</div>
											</div>
										</div>
									</div>
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>
                                    <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                    <input id="enum" type="hidden" name="enum" value=''> 
                                    <input id="s_status" type="hidden" name="s_status" value=''> 
                                </div>
								<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label>舊專案</label>
												<?php echo form_dropdown('rp_num',$opt ,'', 'class="form-control" id="sel"')?>                                                            
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
											<button id='no' class="btn btn-default" >加入舊專案</button>
										</div>
										</div>
									</div>
									</div> 
								</form>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function(){

            // Datapicker 
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd'
            });
    
            var table = $('#table1').DataTable({
                dom: 'Bfrtip',
                 buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
                ],
                lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                ],
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                //"order": [[ 6, "desc" ]],
                'ajax': {
                    'url':'<?php echo base_url("Cases2/rewardApplication")?>',
                    'data': function(data){
                        // Read values
                        var from_date = $('#search_fromdate').val();
                        var to_date = $('#search_todate').val();

                        // Append to data
                        data.searchByFromdate = from_date;
                        data.searchByTodate = to_date;
                    }
                },
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    }
                },
                'columns': [
                    { data: 's_num' },
                    { data: 's_cnum' },
                    { data: 's_ic' },
                    { data: 's_name' },
                    { data: 's_date' },
                    { data: 'address' },
                    { data: 's_office' },
                    { data: 'ddc_ingredient'},
                    { data: 'drug2nw'},
                    { data: 'drug2rw'},
                    { data: 'drug2pnw'},
                    { data: 's_CMethods' },
                    { data: 's_reward_status' },
                ],
                              'columnDefs': [
                 {
                    'orderable': false,
                    'targets': [9],
                 },
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']]
            });
            
                $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });
            
            $('#btn_search').click(function(){
                table.draw();
            });
           
            $("#yes").click(function (){
                $("#s_status").val('1');
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                $("#sp_checkbox").submit();
            });
            $('#sp_checkbox').on('submit', function(e){
                var form = $(this);
                var url = form.attr('action');
                
                var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
                //alert($('#s_cnum').val());
                //e.preventDefault();
            });
        });        
    </script>
