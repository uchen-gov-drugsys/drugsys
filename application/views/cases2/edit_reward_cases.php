        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h4><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h4></li>
                    <li></li> -->
                </ul>
            <?php $this->load->view($nav);?>

            <div id="page-wrapper">
                <div class="container-fluid">
					<div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							<a><?php //echo form_submit('', '儲存', 'class="btn btn-warning" form="casesedit" id="list"');?></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
									<div class="row">
										<div class="col-md-6">
											證物清單
										</div>
										<div class="col-md-6 text-right">
											<!-- <input id="drug" class="btn btn-success btn-sm" type="button" value="新增" /> -->
										</div>
									</div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $drug_table;?>
                                        <input type="hidden" name='c_num' id='c_num' class="form-control" value=<?php echo $cnum?> >
                                        <input type="hidden" name='link' id='link' class="form-control" >
                                    </div>                       
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <script type="text/javascript">
            $(document).ready(function(){
                $("#drug").click(function (){
                    $("#link").val('cases2/createdrug2_num/'+$("#c_num").val());
                    alert($("#link").val());
                    $("#casesedit").submit();
                });
                $( "#casesedit" ).validate({
                    rules: {
                        rm_num: {
                            digits: true,
                        },
                    },
                    messages: {
                    }
                });        
            });            
        </script>
