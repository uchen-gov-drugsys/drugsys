        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 

                    <div class="row" style="margin-top:35px;letter-spacing:5px;">
                        <div class="col-md-6">
                            <blockquote style="margin-bottom:10px;">
                                <p><?php echo $title;?></p>
                            </blockquote>
                        </div>
                        <div class="col-md-6 text-right">
							<button id='reject' class="btn btn-danger" onclick="#">退回</button>
                            <button id='yes' class="btn btn-success" onclick="#">確認送出裁罰</button>
                            <button id='no' class="btn btn-info" onclick="#">免裁罰</button>
							<button id='no1' class="btn btn-info" onclick="#">移送法院</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                  列表清單
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
									<?php echo form_open_multipart('Cases2/update3cases','id="sp_checkbox"') ?>
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>
                                    <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                    <input id="s_status" type="hidden" name="s_status" value=''> 
									<?php echo form_close(); ?>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
               </div>
                        <!-- /.col-lg-12 -->
                        <!-- <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <a class="media" href="test.pdf"></a>    
                                </div>
                            </div>
                        </div>    -->
                    <!-- /.row -->
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
			chooseAry = [];	
           table = $('#table1').DataTable({
                dom: 'Bfrtip',
				columnDefs: [ {
						targets:   0,
						visible: true
				},{
						targets:   1,
						visible: false
				},{
						className: 'select-checkbox',
						targets:   0,
				} ],
				select: {
						style:    'os',
						selector: 'td:first-child',
						style: 'multi'
				},
                 buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
                ],
                lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                ],
				"fnRowCallback": function (nRow, aData, iDisplayIndex) {
						$(nRow).attr("data-id", aData[1]);
						return nRow;
				},
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    }
                },              
              'order': [[1, 'asc']]
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                
                    //alert("Submitted");
					if(chooseAry.length > 0)
					{
						$("#s_status").val('1');
						$('#s_cnum').val(chooseAry.join(","));
						$("#sp_checkbox").submit();
					}
					else
					{
						alert('請勾選要確認裁罰之案件。');
						return false;
					}
                    
            });
            $("#no").click(function (){
                
                    //alert("Submitted");
					if(chooseAry.length > 0)
					{
						$("#s_status").val('0');
						$('#s_cnum').val(chooseAry.join(","));
						$("#sp_checkbox").submit();
					}
					else
					{
						alert('請勾選要免裁罰之案件。');
						return false;
					}
                   
            });
			$("#no1").click(function (){
                
				//alert("Submitted");
				if(chooseAry.length > 0)
				{
					$("#s_status").val('3');
					$('#s_cnum').val(chooseAry.join(","));
					$("#sp_checkbox").submit();
				}
				else
				{
					alert('請勾選要移送(法院)裁處之案件。');
					return false;
				}
			   
		});
			$("#reject").click(function (){
                
					//alert("Submitted");
					if(chooseAry.length > 0)
					{
						$("#s_status").val('2');
						$('#s_cnum').val(chooseAry.join(","));
						$("#sp_checkbox").submit();
					}
					else
					{
						alert('請勾選要退回之案件。');
						return false;
					}
				
			});

           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
              var form = this;

              var rows_selected = table.column(0).checkboxes.selected();

              // Iterate over all selected checkboxes
              /*$.each(rows_selected, function(index, rowId){
                 // Create a hidden element
                 $(form).append(
                     $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'id[]')
                        .val(rowId)
                 );
              });*/
                $('#example-console-rows').text(rows_selected.join(","));
                // $('#s_cnum').val(rows_selected.join(","));
              
                // Output form data to a console     
                //$('#example-console-form').text($(form).serialize());
               
                // Remove added elements
                $('input[name="id\[\]"]', form).remove();
               
                // Prevent actual form submission
                //e.preventDefault();
           });

		   $('#table1 tbody').on( 'click', 'tr', function () {
					$(this).toggleClass('selected');
					
					if($(this).hasClass('selected'))
					{
							chooseAry.push($(this).attr("data-id"))
					}
					else
					{
							delAry = $(this).attr("data-id");
							chooseAry.forEach(function(item, index, arr) {
									if(JSON.stringify(item) === JSON.stringify(delAry)) {
											return arr.splice(index, 1);
									}
							});
					}
			} );
        });
</script>
