        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
            
                <div class="container-fluid"> 
                    <blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>
                    </blockquote>
                    <div class="row">
						<div class="col-md-12">
							<a href="<?php echo base_url('PayTaipei/downloadcsv'); ?>" class="btn btn-default">下載CSV檔</a>
						</div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
            
        
    });
    function tranfer2ADyear(date)
    {
        if(date.length == 6)
        {
            ad = (parseInt(date.substr(0, 2))) + 1911;
            return ad.toString() + '-' + date.substr(2, 2) + '-' + date.substr(4, 2);
        }
        else if(date.length == 7)
        {
            ad = (parseInt(date.substr(0, 3))) + 1911;
            return ad.toString() + '-' + date.substr(3, 2) + '-' + date.substr(5, 2);
        }
        else
        {
            return '';
        }
    }
    function uploadEvent(obj, type){
        $('#loading').removeClass('hidden');
        $("#loadingTxt").text("資料上傳中...請稍候...請勿關掉或跳出畫面喔！電腦會壞掉!");
        let api = '#';
        switch (type) {
            case 'CF': // 裁罰
                api = '<?php echo base_url(); ?>Integration/uploadfiles';
                break;
            case 'CW': // 帳務
                api = '<?php echo base_url(); ?>Integration/uploadCWfiles';
                break;
            case 'ML': // 執行命令
                api = '<?php echo base_url(); ?>Integration/uploadMLfiles';
                break;
            case 'ES': // 移送
                api = '<?php echo base_url(); ?>Integration/uploadESfiles';
                break;
            case 'PC': // 憑證
                api = '<?php echo base_url(); ?>Integration/uploadPCfiles';
                break;
            case 'CS': // 撤註銷
                api = '<?php echo base_url(); ?>Integration/uploadCSfiles';
                break;
            default:
                api = '#';
                break;
        }
        let formdata = new FormData();
        formdata.append('f_year', obj.attr('data-year'));
        formdata.append('f_belong', obj.attr('data-belong'));
        formdata.append('upload_file', obj.parent().parent().find("input[name=year_"+obj.attr('data-year')+"]")[0].files[0]);
        $.ajax({            
            type: 'POST',
            url: api,
            data: formdata,
            // dataType: 'json',
            processData : false, 
            contentType: false,
            cache: false,
            error:function(){
                console.log('error')
            },
            success: function(resp){
                console.log(resp);
                
                if(resp.indexOf('ok') > -1)
                {
                  location.reload();
                }
            },
            complete:function(resp){
              console.log('complete')
              $('#loading').addClass('hidden');
              $("#uploadtxt").text("");
            }
        });
        
    }
    
    </script>
