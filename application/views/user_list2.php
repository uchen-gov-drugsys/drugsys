            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </br>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $data_table;?>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script type="text/javascript">
        $(document).ready(function(){
            $('#table1').DataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                //"search": {
                //    "search": "科技犯罪偵查隊"
                //},
                'ajax': {
                  'url':'<?php echo base_url("login/testjson")?>'
                },
                "language": //把文字變為中文
                {  
                    "sProcessing": "處理中...",  
                    "sLengthMenu": "顯示 _MENU_ 項結果",  
                    "sZeroRecords": "沒有匹配結果",  
                    "sInfo": "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",  
                    "sInfoEmpty": "顯示第 0 至 0 項結果，共 0 項",  
                    "sInfoFiltered": "(由 _MAX_ 項結果過濾)",  
                    "sInfoPostFix": "",  
                    "sSearch": "搜索:",  
                    "sUrl": "",  
                    "sEmptyTable": "表中數據為空",  
                    "sLoadingRecords": "載入中...",  
                    "sInfoThousands": ",",  
                    "oPaginate": {  
                        "sFirst": "首頁",  
                        "sPrevious": "上頁",  
                        "sNext": "下頁",  
                        "sLast": "末頁"  
                    },  
                    "oAria": {  
                        "sSortAscending": ": 以升序排列此列",  
                        "sSortDescending": ": 以降序排列此列"  
                    }  
                },
                'columns': [
                    { data: 'em_mailId' },
                    { data: 'em_lastname' },
                    { data: 'em_office' },
                    { data: 'em_job' },
                    { data: 'em_roffice' },
                    { data: 'em_IsAdmin' },
                    { data: 'em_change_code' },
                ]
            });
        });        
        </script>