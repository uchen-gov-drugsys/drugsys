        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><a><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                            <div class="panel panel-default">
                                <div class="panel-heading list_view">
                                <form action="index" method="post">
                                    <div class="form-group">
                                        處分書編號：<input type="text" name="cnum"/>
                                        姓名：<input style="width:80px" type="text" name="name"/>
                                        身份證字號：<input type="text" name="ic"/>
                                        虛擬帳號：<input style="width:240px" type="text" name="BVC"/>
                                        <!--選擇日期區間:<label><input  id="dater" class="form-control" type="text"  name="datepicker"/></label>-->
                                        <input type="submit" value="查询"/>
                                    </div>
                                </form>
                                   <!-- <input type="checkbox" name="list"  data-target ="1"  checked> 處分書編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 人員編號
                                    <input type="checkbox" name="list" data-target ="3" checked> 姓名
                                    <input type="checkbox" name="list" data-target ="4" checked> 證號
                                    <input type="checkbox" name="list" data-target ="5" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="6" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="7" checked> 犯罪手法
                                    <input type="checkbox" name="list" data-target ="8" checked> 毒品號
                                    <input type="checkbox" name="list" data-target ="9" checked> 成分
                                    <input type="checkbox" name="list" data-target ="10" checked> 級數
                                    <input type="checkbox" name="list" data-target ="11" checked> 淨重
                                    <input type="checkbox" name="list" data-target ="12" checked> 純質淨重
                                    <input type="checkbox" name="list" data-target ="13" checked> 建議金額
                                    -->
                                </div>
                            <form action=<?php echo base_url("Acc_cert/addAcc_Project") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php  echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>舊專案</label>
                                        <?php echo form_dropdown('fp_num',$opt ,'', 'class="form-control"')?>                                                            
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button id='no' class="btn btn-default" >加入舊專案</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                                <div class="modal fade" id="newcases" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="othernewLabel">新增每日帳作業</h5>
                                                <select name='project_pay_type' id="sel" class="form-control">
                                                    <option value="金融">金融</option>
                                                    <option value="支匯票">支匯票</option>
                                                </select>
                                                <select name='project_type' id="sel1" class="form-control">
                                                    <option value="罰緩">罰緩</option>
                                                    <option value="怠金">怠金</option>
                                                    <option hidden value="罰緩/怠金">罰緩/怠金</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-default" id='yes'>確認</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
            $("#dater").daterangepicker(
            {
            startDate: '2020-09-01',  
            locale: {
                  format: 'YYYY-MM-DD'
                }
            } 
            );
            var table = $('#table1').DataTable({
                "searching": false,
                'columnDefs': [
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']],
                /*dom: 'Bfrtip',
                buttons: [
                    'copy', {
                extend: 'csv',
                text: 'CSV',
                bom : true}, 'excel', 'pdf'
                ]    */
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
           
    });
    $("#sel").change(function(){
        if($(this).val()=='支匯票'){
            $("#sel1").val('罰緩/怠金');
            $("#sel1").hide();
        }
        else{
            $("#sel1").val('罰緩');
            $("#sel1").show();
        }
    });

    </script>
