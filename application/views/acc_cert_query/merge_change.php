        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title;?></h4></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php echo form_open_multipart('Acc_cert/save_merge_change','id="save"') ?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <br><table class="table">
                                                    <tbody id="drugall" class="form-group">
                                                        <tr>
                                                            <td>
                                                                <label>處分書編號</label>
                                                                <input name="f_cnum" class="form-control" value= "<?php if(isset($fcp->surc_no)) echo $fcp->surc_no; 
                                                                else echo $fcp->fd_num
                                                                ?> "readonly>
                                                                <input id="f_snum" type="hidden" name="f_snum" value='<?php echo $fcp->f_snum ?>'> 
                                                                <input id="f_num" type="hidden" name="f_num" value='<?php echo $fcp->f_num ?>'> 
                                                                <input id="fcp_no" type="hidden" name="fcp_no" value='<?php echo $fcp->fcp_no ?>'> 
                                                            </td>
                                                            <td>
                                                                <label>姓名</label>
                                                                <input name="s_name" class="form-control" value= "<?php echo $fcp->s_name ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>身份證編號</label>
                                                                <input name="f_sic" class="form-control" value= "<?php echo $fcp->s_ic ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>年度罰緩金額</label>
                                                                <input name="f_damount" class="form-control" value= "<?php echo $amount ?> ">
                                                            </td>
                                                            <td>
                                                                <label>完納金額</label>
                                                                <input name="f_payamount" class="form-control" value= "<?php echo $payamount ?> ">
                                                           </td>
                                                            <td>
                                                                <label>移送案號</label>
                                                                <input class="form-control" value= "<?php echo $fcp->ft_no ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>分署</label>
                                                                <input class="form-control" value= "<?php echo $fcp->s_roffice ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>執行費</label>
                                                                <input name="f_fee" class="form-control" value= "<?php echo $fee ?> ">
                                                            </td>
                                                            <td>
                                                                <label>轉正類型</label>
                                                                <input name="f_change_type" class="form-control" value= "轉正合併"readonly>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
            var all=1;
            function CreateUploadAll()
            {
                all++;
                var tr=document.createElement('tr');
                var html='<tr>'+
                            '<td style="padding:5px">'+
                            '<label>社群軟體名稱</label>'+
                            '<input name="social_media_name[]" class="form-control" >'+
                            '</td><td style="padding:5px">'+
                            '<label>社群帳號1</label><input name="social_media_ac1[]" class="form-control" ></td>'+
                            '<td style="padding:5px"><label>社群帳號2</label>'+
                            '<input name="social_media_ac2[]"class="form-control"></td>'+
                            '<td style="padding:5px"><label>社群帳號3</label><input  name="social_media_ac3[]" class="form-control" ></td>'+
                            '<td>'+
                            '<label><input style="float: left;" class="btn btn-danger" type="button" onclick="RemoveAddAll('+all+')"'+
                            'value="X" /></label>'+
                    '</td>'+
                    '</tr>'
                tr.innerHTML=html;
                //div.setAttribute("class","form-control");
                tr.setAttribute("id","drugrow"+all);
                //div.id="upDiv"+p;
                document.getElementById('drugall').appendChild(tr);
            }
            function RemoveAddAll(id)
            {
                 var div=document.getElementById('drugrow'+id);
                 var div2=document.getElementById('drugall');
                 div2.removeChild(div);
            }    
        </script>
    </body>
</html>
