        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                            <div class="panel panel-default">
                                <div class="panel-heading list_view">
                                <form action="list_SearchALl" method="post">
                                    <div class="form-group">
                                        姓名：<input type="text" name="name"/>
                                        身份證字號：<input type="text" name="ic"/><br>
                                        選擇日期區間:<label><input  id="dater"  type="text"  name="datepicker"/></label>
                                        <input type="submit" value="查询"/>
                                    </div>
                                </form>
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 案件編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 人員編號
                                    <input type="checkbox" name="list" data-target ="3" checked> 姓名
                                    <input type="checkbox" name="list" data-target ="4" checked> 證號
                                    <input type="checkbox" name="list" data-target ="5" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="6" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="7" checked> 犯罪手法
                                    <input type="checkbox" name="list" data-target ="8" checked> 毒品號
                                    <input type="checkbox" name="list" data-target ="9" checked> 成分
                                    <input type="checkbox" name="list" data-target ="10" checked> 級數
                                    <input type="checkbox" name="list" data-target ="11" checked> 淨重
                                    <input type="checkbox" name="list" data-target ="12" checked> 純質淨重
                                    <input type="checkbox" name="list" data-target ="13" checked> 建議金額
                                </div>
                            <?php echo form_open_multipart('reward/editRewardProject','id="sp_checkbox"') ?>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <!--table style="width:1600px" border="0" cellpadding="3" cellspacing="0" class="table table-striped table-bordered table-hover" id="table1">
                                            <thead><th width="5%">案件編號</th>
                                            <th width="5%">身份證</th>
                                            <th width="5%">犯嫌人姓名</th>
                                            <th width="5%">查獲時間</th>
                                            <th width="5%">查獲地點</th>
                                            <th width="5%">查獲單位</th>
                                            <th width="5%">持有毒品</th>
                                            <th width="5%">毒品成分</th>
                                            <th width="5%">淨重</th>
                                            <th width="5%">純質淨重</th>
                                            <th width="5%">核發價格</th></thead>
                                            <tbody-->
                                        <?php  echo $s_table;?>
                                        <?php /*foreach ($query as $susp):
                                            $drug_doc = $this->getsqlmod->get1DrugwithIC($susp->s_ic)->result();
                                        ?>
                                        <tr>
                                            <td><?php echo $susp->s_num ?></td>
                                            <td><?php echo $susp->s_ic ?></td>
                                            <td><?php echo $susp->s_name ?></td>
                                            <td><?php echo $susp->s_date ?></td>
                                            <td><?php echo $susp->r_county.$susp->r_district.$susp->r_zipcode.$susp->r_address ?></td>
                                            <td><?php echo $susp->s_office ?></td>
                                            <td><?php  foreach ($drug_doc as $drug): 
                                                echo $drug->e_id.'<br>'; 
                                                endforeach;
                                            ?></td>
                                            <td><?php  foreach ($drug_doc as $drug): 
                                                echo $drug->e_id.'<br>'; 
                                                endforeach;
                                            ?></td>
                                            <td><?php echo $susp->s_num ?></td>
                                            <td><?php echo $susp->s_num ?></td>
                                            <td><?php echo $susp->s_num ?></td>

                                        </tr>
                                        <?php endforeach;*/?>
                                        </tbody>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>舊專案</label>
                                        <?php echo form_dropdown('rp_num',$opt ,'', 'class="form-control" id="sel"')?>                                                            
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button id='no' class="btn btn-default" >加入舊專案</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                            </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
            $("#dater").daterangepicker(
            {
            //startDate: '2020-09-01',  
            locale: {
                  format: 'YYYY-MM-DD'
                }
            } 
            );
            var table = $('#table1').DataTable({
                'columnDefs': [
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']],
                dom: 'Bfrtip',
                buttons: [
                    'copy', {
                extend: 'csv',
                text: 'CSV',
                bom : true}, 'excel', 'pdf'
                ]     
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
    });
    </script>
