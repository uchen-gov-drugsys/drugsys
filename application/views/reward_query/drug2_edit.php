        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h4></li>
                </ul>
            <?php $this->load->view($nav);?>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default"></br>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <?php echo form_open_multipart('reward/update_drug2','id="newdrug1"') ?>
                                            <table class="table">
                                                <tbody id="drugall" class="form-group">
                                                    <tr>
                                                        <td style="width:150px"><label>證物號</label>
                                                        <?php 
                                                            echo form_input('e_id',$e_id, 'class="form-control" readonly');                                                        
                                                            echo form_hidden('e_c_num',$e_c_num);                                                        
                                                        ?>                    
                                                        <input id="link" type="hidden" name="link" value=" ">                           
                                                        </td>
                                                        <td  style="width:80px">
                                                            <label>品名:</label>
                                                            <?php echo form_dropdown('e_name',$nameopt , $drugAll->e_name , 'class="form-control" id="sel"')?>                                                            
                                                        </td>
                                                        <td style="width:80px"><label>量數</label>
                                                            <?php echo form_dropdown('e_type',$countopt , $drugAll->e_type , 'class="form-control"')?>                                                                                
                                                        </td>
                                                        <td style="width:80px"><label>數量</label>
                                                        <?php 
                                                            echo form_input('e_count',$drugAll->e_count, 'class="form-control" id="e_count"');                                                        
                                                        ?>                    
                                                        </td>
                                                        <td style="width:80px"><label>初驗淨重</label>
                                                        <?php 
                                                            echo form_input('e_1_N_W',$drugAll->e_1_N_W, 'class="form-control" id="e_1_N_W"');                                                        
                                                        ?>                    
                                                        </td>
                                                        <td id="drug_s">
                                                            <label>初驗毒品成分</label>
                                                            <label><button type="button" name="add" id="add" class="btn btn-success">新增成分</button></label>
                                                            <?php 
                                                            foreach ($drugck as $drugck)
                                                            {
                                                                echo '<div id="row">';
                                                                echo form_dropdown('df[]',$nwopt , $drugck->df_ingredient , 'class="form-control" id="sel"');
                                                                echo anchor('cases2/deleteCk/'. $drugck->df_num,'X','class="btn btn-danger btn_remove"');
                                                                //echo '<button type="button" class="btn btn-danger btn_remove">X</button></';
                                                                echo '</div>';
                                                            }   
                                                            ?>                                                            
                                                        </td>
                                                        <td  style="width:120px">
                                                            <label>持有人</label>
                                                            <?php echo form_dropdown('e_s_ic',$test , $drugAll->e_s_ic , 'class="form-control"')?>                                                            
                                                        </td>
                                                        <td style="width:300px">
                                                            <label>檢驗毒品級別成分</label>
                                                            <?php 
                                                            foreach ($drugck2 as $drugck2)
                                                            {
                                                                echo '<div id="row">';
                                                                echo form_dropdown('df2[]',$drug_opt , $drugck2->ddc_level .'級' .$drugck2->ddc_ingredient , 'class="form-control" id="sel"');
                                                                echo form_hidden('ddc_num[]',$drugck2->ddc_num); 
                                                                echo '<label>純質淨重(g)</label>';
                                                                echo form_input('ddc_nw[]',$drugck2->ddc_nw, 'class="form-control"');                                                        
                                                                echo anchor('cases2/deleteCk2/'. $drugck2->ddc_num,'X','class="btn btn-danger btn_remove"');
                                                                //echo '<button type="button" class="btn btn-danger btn_remove">X</button></';
                                                                echo '</div>';
                                                            }   
                                                            ?>                                                            
                                                        </td>
                                                        <td  style="width:30px">
                                                            <label>毒報</br></label>
                                                                <?php 
                                                                if($drugAll->e_doc == ""){
                                                                    echo form_upload('e_doc');
                                                                }
                                                                else {
                                                                    echo anchor_popup('drugdoc/' . $drugAll->e_doc, '</br>毒報');
                                                                    echo form_hidden('e_doc',$drugAll->e_doc);
                                                                    //echo form_hidden('e_doc',$drugAll->e_s_ic);
                                                                }
                                                            ?>                                                            
                                                        </td>
                                                        <td>
                                                            <label><?php echo form_submit('', '新增成分', 'class="btn btn-success" form="newdrug1" id="ddc"');?></label>
                                                        </td>
                                                </tr>
                                           </tbody>
                                        </table>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
                $(document).ready(function(){      
                    var i=1;  
                    $('#add').click(function(){  
                       i++;  
                       $('#drug_s').append('<div id="row'+i+'">'+
                        '<select name="df[]" class="form-control"><option>海洛因</option><option>安非他命</option>'+
                        '<option>搖頭丸(MDMA)</option><option>愷他命(KET)</option><option>一粒眠</option>'+
                        '<option>大麻</option><option>卡西酮類</option><option>古柯鹼</option><option>無毒品反應</option>'+
                        '</select><button type="button" name="remove" id="'+i+'"'+
                        'class="btn btn-danger btn_remove">X</button></div>');  
                    });
                    $(document).on('click', '.btn_remove', function(){  
                       var button_id = $(this).attr("id");   
                       $('#row'+button_id+'').remove();  
                    });  
                    $( "#newdrug1" ).validate({
                        rules: {
                            e_1_N_W: {
                                //required: true,
                                digits: true,
                                maxlength: 6
                            },
                            e_s_ic: {
                                required: true,
                            },
                            'df2[]': {
                                required: true,
                            },
                        },
                        messages: {
                            e_1_N_W: {
                                //required: "此欄位不得為空",
                                digits: "請輸入數字",
                                maxlength: "請輸入合理重量",
                            },
                            e_s_ic: {
                                required: "請選擇犯嫌人",
                            },
                        }
                    });        
                    $("#ddc").click(function (){
                        $("#link").val('1');
                        //alert($("#link").val());
                        $("#newdrug1").submit();
                    });        
                });  
    </script>
    </body>
</html>
