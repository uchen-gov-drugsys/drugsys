                    <?php 
                        //$hidden = array('c_num' => $c_num, 's_office' => $s_office, 'e_ed_empno' =>e_ed_empno);
                        ini_set('display_errors','off');   
                        if (!$this -> session -> userdata('uic')){
                            $this->output
                                    ->set_status_header(403)
                                    ->set_content_type('text/html')
                                    ->set_output(file_get_contents( $this->load->view('403')))
                                    ->_display();

                                    sleep(5);
                                    redirect('login/index','refresh');
                            exit;
                        }
                        if($this -> session -> userdata('em_priority') != '3' ){
                            
                            redirect('login/logout','refresh');
                            exit;
                        }
                        echo form_open('disciplinary_c/createcases_num','id="newcases_num"');
                    ?>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <?php if(preg_match("/a/i", $this->session-> userdata('3permit'))){?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Disciplinary_c' || $this->uri->segment(1) == 'disciplinary_c')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 處分書管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Disciplinary_c' || $this->uri->segment(1) == 'disciplinary_c')?'in':''); ?>">
                                    <?php if(preg_match("/a/i", $this->session-> userdata('3permit'))){?><li>
                                        <a href="javascript:{}" onclick="document.getElementById('newcases_num').submit(); return false;">本轄案件新增(他機關查獲)</a>
                                    </li><?php echo form_close(); }?>
                                    <li>
                                        <a href=<?php echo base_url("disciplinary_c/listdp") ?>>點收本轄案件</a>
                                    </li>
									<li class="<?php echo (($this->uri->segment(2) == 'listDpProject' || $this->uri->segment(2) == 'listDpProject_Ready')?'active':''); ?>">
                                        <a href="#">案件作業<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(2) == 'listDpProject' || $this->uri->segment(2) == 'listDpProject_Ready')?'in':''); ?>">
											<li>
												<a href=<?php echo base_url("disciplinary_c/listDpProject") ?>>專案處理</a>
											</li>
											<li>
												<a href=<?php echo base_url("disciplinary_c/listDpProject_Ready") ?>>送批列表</a>
											</li>
                                        </ul>
                                    </li>
                                    
                                    
									
                                    <!-- <li class="<?php //echo (($this->uri->segment(2) == 'listdelivery')?'active':''); ?>">
                                        <a href="#">送達情形登錄<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php //echo (($this->uri->segment(2) == 'listdelivery')?'in':''); ?>">
                                            <li><a href=<?php //echo base_url("disciplinary_c/listdelivery") ?>>送達情形登錄</a></li>
                                        </ul>
                                    </li> -->
                                    <li class="<?php echo (($this->uri->segment(2) == 'listDpRollbackProject' || $this->uri->segment(2) == 'listDpProject_Ready')?'active':''); ?>">
                                        <a href="#">重處作業<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(2) == 'listDpRollbackProject' || $this->uri->segment(2) == 'listDpProject_Ready')?'in':''); ?>">
											<li><a href=<?php echo base_url("disciplinary_c/listDpRollbackProject") ?>>專案處理</a></li>
											<li><a href=<?php echo base_url("disciplinary_c/listDpRollbackProject_Ready") ?>>送批列表</a></li>

                                            <!-- <li><a href=<?php //echo base_url("disciplinary_c/listPublicity") ?>>未送達列表</a></li>
                                            <li><a href=<?php //echo base_url("disciplinary_c/listPublicity1") ?>>公示送達專案列表</a></li> -->
                                        </ul>
                                    </li>
									<?php $overdate = $this->getsqlmod->getdiff_fd()->result()[0]; 
									?>
									<li>
										<a href=<?php echo base_url("disciplinary_c/listdelivery") ?>>送達情形登錄 <span class="badge" style="background-color: #a94442;"><?php echo $overdate->overdate;?></span></a>
									</li>
									<li class="<?php echo (($this->uri->segment(2) == 'listPublicity1' || $this->uri->segment(2) == 'listPublicityProject')?'active':''); ?>">
										<a href=<?php echo base_url("disciplinary_c/listPublicity1") ?>>公示送達</a>
									</li>
                                    <li><a href=<?php echo base_url("disciplinary_c/listPetitions") ?>>訴願案件、行政訴訟案件</a></li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li><?php }?>
                            <?php if(!preg_match("/a/i", $this->session-> userdata('3permit'))){?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Disciplinary_c' || $this->uri->segment(1) == 'disciplinary_c')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 處分書管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Disciplinary_c' || $this->uri->segment(1) == 'disciplinary_c')?'in':''); ?>">
                                    <li class="<?php echo (($this->uri->segment(2) == 'listdelivery')?'active':''); ?>">
                                        <a href="#">送達情形登錄<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(2) == 'listdelivery')?'in':''); ?>">
                                            <li><a href=<?php echo base_url("disciplinary_c/listdelivery") ?>>送達情形登錄</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li><?php }?>
                            <?php if(preg_match("/b/i", $this->session-> userdata('3permit'))){?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Surcharges' || $this->uri->segment(1) == 'surcharges')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 怠金處分書管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Surcharges' || $this->uri->segment(1) == 'surcharges')?'in':''); ?>">
                                    <li><a href=<?php echo base_url("surcharges/surcharges_receive") ?>>怠金收文處理</a></li>
                                    <li><a href=<?php echo base_url("surcharges/index") ?>>待處理怠金案件</a></li>
									<li class="<?php echo (($this->uri->segment(2) == 'listProject')?'active':''); ?>">
                                        <a href="#">案件作業<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(2) == 'listProject')?'in':''); ?>">
											<li>
												<a href=<?php echo base_url("surcharges/listProject") ?>>專案列表</a>
											</li>
											<li>
												<a href=<?php echo base_url("surcharges/listDpProject_Ready") ?>>送批列表</a>
											</li>
                                        </ul>
                                    </li>
									<li class="<?php echo ''; ?>">
                                        <a href="#">重處作業<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo ''; ?>">
											<li><a href=<?php echo base_url("surcharges/listDpRollbackProject") ?>>專案處理</a></li>
											<li><a href=<?php echo base_url("surcharges/listDpRollbackProject_Ready") ?>>送批列表</a></li>

                                            <!-- <li><a href=<?php //echo base_url("disciplinary_c/listPublicity") ?>>未送達列表</a></li>
                                            <li><a href=<?php //echo base_url("disciplinary_c/listPublicity1") ?>>公示送達專案列表</a></li> -->
                                        </ul>
                                    </li>
									<?php $overdate_surc = $this->getsqlmod->getdiff_surc()->result()[0];
									?>
									<li>
										<a href=<?php echo base_url("surcharges/listdelivery") ?>>送達情形登錄 <span class="badge" style="background-color: #a94442;"><?php echo $overdate_surc->overdate;?></span></a>
									</li>
									<li class="<?php echo ''; ?>">
										<a href=<?php echo base_url("surcharges/listPublicity1") ?>>公示送達</a>
									</li>
                                    <!-- <li><a href=<?php //echo base_url("surcharges/listProject") ?>>怠金專案列表</a></li> -->
                                    <!-- <li class="<?php //echo (($this->uri->segment(2) == 'listdelivery_Surc' || $this->uri->segment(2) == 'listPetitions')?'active':''); ?>">
                                        <a href="../案件處分書/casebook_送達情況.html">聲明異議及送達情形登錄<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php //echo (($this->uri->segment(2) == 'listdelivery_Surc' || $this->uri->segment(2) == 'listPetitions')?'in':''); ?>">
                                            <li><a href=<?php //echo base_url("surcharges/listdelivery_Surc") ?>>送達情形登錄</a></li>
                                            <li><a href=<?php //echo base_url("surcharges/listPetitions") ?>>聲明異議</a></li>
                                        </ul>
                                    </li> -->
                                </ul>
                                <!-- /.nav-second-level -->
                            </li><?php }?>
                            <?php if(!preg_match("/b/i", $this->session-> userdata('3permit'))){?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Surcharges' || $this->uri->segment(1) == 'surcharges')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 怠金處分書管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Surcharges' || $this->uri->segment(1) == 'surcharges')?'in':''); ?>">
                                    <li class="<?php echo (($this->uri->segment(2) == 'listdelivery_Surc')?'active':''); ?>">
                                        <a href="../案件處分書/casebook_送達情況.html">送達情形登錄<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(2) == 'listdelivery_Surc')?'in':''); ?>">
                                            <li><a href=<?php echo base_url("surcharges/listdelivery_Surc") ?>>送達情形登錄</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li><?php }?>
                            <?php if(preg_match("/c/i", $this->session-> userdata('3permit'))){?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Call' || $this->uri->segment(1) == 'call' || $this->uri->segment(1) == 'Fine_traf' || $this->uri->segment(1) == 'Surc_traf')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 移送作業系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Call' || $this->uri->segment(1) == 'call')?'in':''); ?>">
                                    <li><a href=<?php echo base_url("Call/index") ?>>待催繳罰緩案件</a></li>
                                    <li class="<?php echo (($this->uri->segment(2) == 'ProjectList' || $this->uri->segment(2) == 'listdelivery')?'active':''); ?>">
                                        <a href="#">待催專案<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(2) == 'ProjectList' || $this->uri->segment(2) == 'listdelivery')?'in':''); ?>">
                                            <li><a href=<?php echo base_url("Call/ProjectList") ?>>待催專案</a></li>
                                            <li><a href=<?php echo base_url("Call/listdelivery") ?>>所有已催繳案件</a></li>
                                        </ul>
                                    </li>
									<li><a href=<?php echo base_url("Fine_traf/txtUploadIndex") ?>>執行署文件更新</a></li>
									<li><a href=<?php echo base_url("Fine_traf/execCommandIndex") ?>>執行命令登記</a></li>
                                    <li class="<?php echo (($this->uri->segment(1) == 'Fine_traf' && ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == 'ProjectList'))?'active':''); ?>">
                                        <a href="#">待移送罰緩作業<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(1) == 'Fine_traf' && ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == 'ProjectList'))?'in':''); ?>">
                                            <li><a href=<?php echo base_url("Fine_traf/index") ?>>待移送罰緩案件</a></li>
                                            <li><a href=<?php echo base_url("Fine_traf/ProjectList") ?>>移送專案作業</a></li>
                                        </ul>
                                    </li>
									
                                    <li class="<?php echo (($this->uri->segment(1) == 'Surc_traf' && ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == 'ProjectList'))?'active':''); ?>">
                                        <a href="#">待移送怠金作業<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(1) == 'Surc_traf' && ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == 'ProjectList'))?'in':''); ?>">
                                            <li><a href=<?php echo base_url("Surc_traf/index") ?>>待移送怠金案件</a></li>
                                            <li><a href=<?php echo base_url("Surc_traf/ProjectList") ?>>移送怠金專案作業</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li><?php }?>
                            <?php if(preg_match("/d/i", $this->session-> userdata('3permit'))){?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Acc_cert' || $this->uri->segment(1) == 'acc_cert')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 帳務憑證系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Acc_cert' || $this->uri->segment(1) == 'acc_cert')?'in':''); ?>">

                                    <li class="<?php echo (($this->uri->segment(2) == 'index' || $this->uri->segment(2) == 'indexFineCProject')?'active':''); ?>">
                                        <a href="#">新增專案<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(2) == 'index' || $this->uri->segment(2) == 'indexFineCProject')?'in':''); ?>">
                                             <li><a href=<?php echo base_url("acc_cert/index") ?>>新增每日金融支匯票作業</a></li>
                                             <li><a href=<?php echo base_url("acc_cert/indexFineCProject") ?>>新增轉正退費專案</a></li>
                                        </ul>
                                        
                                    </li>
                                    <li class="<?php echo (($this->uri->segment(2) == 'listFineProject' || $this->uri->segment(2) == 'listFineCProject')?'active':''); ?>">
                                        <a href="#">每日帳專案<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(2) == 'listFineProject' || $this->uri->segment(2) == 'listFineCProject')?'in':''); ?>">
                                             <li><a href=<?php echo base_url("acc_cert/listFineProject") ?>>金融支匯票專案</a></li>
                                             <li><a href=<?php echo base_url("acc_cert/listFineCProject") ?>>轉正退費專案</a></li>
                                        </ul>
                                        
                                    </li>
                                    <li class="<?php echo (($this->uri->segment(2) == 'listfp3' || $this->uri->segment(2) == 'listFineReport')?'active':''); ?>">
                                        <a href="../帳務憑證/cc_轉正作業.html">查詢作業<span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level <?php echo (($this->uri->segment(2) == 'listfp3' || $this->uri->segment(2) == 'listFineReport')?'in':''); ?>">
                                             <li><a href=<?php echo base_url("acc_cert/listfp3") ?>>罰鍰、怠金案件收繳查詢</a></li>
                                             <!-- <li><a href=<?php //echo base_url("acc_cert/listFineCProject") ?>>轉正專案</a></li> -->
                                             <li><a href=<?php echo base_url("acc_cert/listFineReport") ?>>報表查詢</a></li>
                                             <!--li><a href="//localhost/main2/acc_cert/listFineCsearch">轉正歷史查詢</a></li-->
                                        </ul>
                                    </li>
                                    <li><a href=<?php echo base_url("acc_cert/CertSearch") ?>>憑證查詢</a></li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li><?php }?>
                            <?php if(!preg_match("/d/i", $this->session-> userdata('3permit'))){?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Acc_cert' || $this->uri->segment(1) == 'acc_cert')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 帳務憑證系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Acc_cert' || $this->uri->segment(1) == 'acc_cert')?'in':''); ?>">
                                    <li><a href=<?php echo base_url("acc_cert/CertSearch") ?>>憑證查詢</a></li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li><?php }?>
                            <?php if(preg_match("/e/i", $this->session-> userdata('3permit'))){?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Reward' || $this->uri->segment(1) == 'reward')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 獎金管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Reward' || $this->uri->segment(1) == 'reward')?'in':''); ?>">
                                    <li><a href=<?php echo base_url("reward/listRewardProject") ?>>獎金審核</a></li>
                                    <li><a href=<?php echo base_url("reward/list3RewardProject") ?>>獎金審核專案</a></li>
                                    <li><a href=<?php echo base_url("reward/listRewardProject_fin") ?>>已送出審核專案</a></li>
                                    <li><a href=<?php echo base_url("reward/list_SearchALl") ?>>查詢案件</a></li>
                                </ul>
                            </li><?php }?>
                            <?php if(!preg_match("/e/i", $this->session-> userdata('3permit'))){?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Reward' || $this->uri->segment(1) == 'reward')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 獎金管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Reward' || $this->uri->segment(1) == 'reward')?'in':''); ?>">
                                    <li><a href=<?php echo base_url("reward/list_SearchALl") ?>>查詢已送出審核案件</a></li>
                                </ul>
                            </li><?php }?>
                            <li class="<?php echo (($this->uri->segment(1) == 'Drug_evi_fin' || $this->uri->segment(1) == 'drug_evi_fin')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 證物管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Drug_evi_fin' || $this->uri->segment(1) == 'drug_evi_fin')?'in':''); ?>">
                                    <?php if(preg_match("/f/i", $this->session-> userdata('3permit'))){?><li>
                                        <a href=<?php echo base_url("drug_evi_fin") ?>>證物出入庫</a>
                                    </li><?php }?>
                                    <li>
                                        <a href=<?php echo base_url("drug_evi_fin/listevirec") ?>>出入庫日誌記錄</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?php echo (($this->uri->segment(1) == 'Record' || $this->uri->segment(1) == 'record')?'active':''); ?>">
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 日誌管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level <?php echo (($this->uri->segment(1) == 'Record' || $this->uri->segment(1) == 'record')?'in':''); ?>">
                                    <li><a href=<?php echo base_url("record/cases") ?>>案件處分書系統日誌</a></li>
                                    <li><a href=<?php echo base_url("record/surc") ?>>怠金處分書管理系統日誌</a></li>
                                    <li><a href=<?php echo base_url("record/traf") ?>>移送作業系統日誌</a></li>
                                    <li><a href=<?php echo base_url("record/acc") ?>>帳務憑證系統日誌</a></li>
                                    <li><a href=<?php echo base_url("record/reward") ?>>獎金系統日誌</a></li>
                                </ul>      
                            </li>
                            <?php if(preg_match("/s/i", $this->session-> userdata('3permit'))) {?><li>
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 測試<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li><a href=<?php echo base_url("record/permit") ?>>3階帳號測試</a></li>
                                    <li><a href=<?php echo base_url("cancel/Test") ?>>註銷測試</a></li>
                                    <li><a href=<?php echo base_url("test_excelupdate/test_update") ?>>excel更新測試</a></li>
                                    <li><a href="http://10.1.252.151/AD_login.php">測試登入</a></li>
                                    <li><a href="https://ssp.eportal.npa.gov.tw/portal">測試警政署</a></li>
                                    <li><a href="https://doc.gov.taipei/tcqb">測試公文</a></li>
                                </ul>      
                            </li>
                            <?php }?>
                            <?php if(preg_match("/s/i", $this->session-> userdata('3permit'))) {?><li>
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 資料整合<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li><a href=<?php echo base_url("Integration/index") ?>>整合入口(罰鍰)</a></li>
                                    <li><a href=<?php echo base_url("Integration/process") ?>>檔案處理(罰鍰)</a></li>
                                    <li><a href=<?php echo base_url("IntegrationDJ/index") ?>>整合入口(怠金)</a></li>
                                    <li><a href=<?php echo base_url("IntegrationDJ/process") ?>>檔案處理(怠金)</a></li>
									<li><a href=<?php echo base_url("PayTaipei/index") ?>>PayTaipei文件下載</a></li>
                                </ul>      
                            </li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i><?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href=<?php echo base_url("login/changepw") ?>><i class="fa fa-sign-out fa-fw"></i> 更改密碼</a>
                            </li>
                            <li><a href=<?php echo base_url("login/logout") ?>><i class="fa fa-sign-out fa-fw"></i> 登出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>

