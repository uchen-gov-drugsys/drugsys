        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <!--li><a href=<?php echo base_url('PDFcreate/surc_Call/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >財產清查函</button></a></li>
                    <li><a href=<?php echo base_url('PDFcreate/surc_Call/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >財產清查清冊</button></a></li-->
                    <li><a href=<?php echo base_url('PDFcreate/surc_Traf_surc/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >行政執行函</button></a></li>
                    <li><a href=<?php echo base_url('Surc_traf/exportCSV/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >發文簿</button></a></li>
                    <li><a href=<?php echo base_url('PDFcreate/surc_TrafSurc/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >移送書</button></a></li>
                    <li><a href=<?php echo base_url('Surc_traf/fine_doc_project_fin_no/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >處分書</button></a></li>
                    <li><a href=<?php echo base_url('PDFcreate/traf_delivery_surc/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >送達證書</button></a></li>
                    <!--li><a href=<?php// echo base_url('XMLcreate/downloadsignxml/'.$sp)?>><button type="button" class="btn btn-default"style="padding:0px 0px;" >催繳函</button></a></li-->
                    <li><a href= <?php echo base_url("Surc_traf/ProjectList/")?>><button  class="btn btn-default"style="padding:0px 0px;" >回到專案</button></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                            <div class="panel panel-default">
                            <form action="http://localhost/main2/cases2/editRewardProject" id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                        </form>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           var table = $('#table1').DataTable({
               "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    },
                ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']]
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
    });
    </script>
