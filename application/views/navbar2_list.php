        <?php 
        ini_set('display_errors','off');   
            if (!$this -> session -> userdata('uic')){
                $this->output
                        ->set_status_header(403)
                        ->set_content_type('text/html')
                        ->set_output(file_get_contents( $this->load->view('403')))
                        ->_display();

                        sleep(5);
                        redirect('login/index','refresh');
                exit;
            }
            if($this -> session -> userdata('em_priority') != '2' ){
                            
                redirect('login/logout','refresh');
                exit;
            }
        ?>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <!-- <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h4></li>
                    <li><a><?php //echo form_submit('', '新增案件', 'class="btn btn-default" style="padding:0px 0px;"form="newcases_num"');?></a></li>
                </ul> -->
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                </ul>
                    <?php 
                        //$hidden = array('c_num' => $c_num, 's_office' => $s_office, 'e_ed_empno' =>e_ed_empno);
                        
                        //echo form_open('cases2/createcases_num','id="newcases_num"');
                    ?>
                <!-- /.navbar-top-links -->
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 案件管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <!-- <li>
                                        <a href="javascript:{}" onclick="document.getElementById('newcases_num').submit(); return false;">一階案件新增</a>
                                        <?php //echo form_close();?>
                                    </li>
                                    <li>
                                        <a href=<?php //echo base_url("cases2/listSouceCases") ?>>一階溯源資料</span></a>
                                    </li> -->
									<!-- <li>
                                        <a href=<?php //echo base_url("cases2/addNewCases") ?>>新增案件</span></a>
                                    </li> -->
                                    <li>
                                        <a href=<?php echo base_url("cases2/listCases") ?>>二階案件查詢/編輯</a>
                                    </li>
                                    <?php if(isset($IsAdmin)&&$IsAdmin=="1"){ ?><li>
                                        <a href="#">三階移送裁罰<span class="fa arrow"></span></a>
                                            <ul class="nav nav-second-level">
                                                <li>
                                                    <a href=<?php echo base_url("cases2/list3Cases") ?>>三階移送裁罰列表</a>
                                                </li>
                                                <li>
                                                    <a href=<?php echo base_url("cases2/list3CasesCX") ?>>查詢免罰及法院案件 </a>
                                                </li>
                                            </ul>                 
                                    </li><?php }?>
                                </ul>
                                    </li>
                            <li>
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 獎金管理<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href=<?php echo base_url("cases2/list3Rewardlist") ?>>獎金申請</a>
                                    </li>
                                    <li>
                                        <a href=<?php echo base_url("cases2/listRewardProject") ?>>一階獎金專案</a>
                                    </li>
                                    <?php if(isset($IsAdmin)&&$IsAdmin=="1"){ ?><li>
                                        <a href=<?php echo base_url("cases2/list3RewardProject") ?>>獎金專案</a>
                                    </li><?php }?>
                                    <li>
                                        <a href=<?php echo base_url("cases2/rewardstate") ?>>獎金案件狀態</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 證物管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href=<?php echo base_url("cases2/listevi") ?>>證物出入庫</a>
                                    </li>
                                    <li>
                                        <a href=<?php echo base_url("cases2/listevirec") ?>>出入庫日誌記錄</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <?php if(isset($IsAdmin)&&$IsAdmin=="1"){echo '<li><a href='.base_url("cases2/admin").'>管理權限/修改密碼</span></a></li>';} ?>
                                <?php if(isset($IsAdmin)&&$IsAdmin=="1"){echo '<li><a href='.base_url("cases2/change").'>管理權限/人員異動</span></a></li>';} ?>
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href=<?php echo base_url("login/changepw") ?>><i class="fa fa-sign-out fa-fw"></i> 更改密碼</a>
                            </li>
                            <li><a href=<?php echo base_url("cases2/logout") ?>><i class="fa fa-sign-out fa-fw"></i> 登出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>

