        <?php 
        ini_set('display_errors','off');   
            if (!$this -> session -> userdata('uic')){
                $this->output
                        ->set_status_header(403)
                        ->set_content_type('text/html')
                        ->set_output(file_get_contents( $this->load->view('403')))
                        ->_display();

                        sleep(5);
                        redirect('login/index','refresh');
                exit;
            }
            if($this -> session -> userdata('em_priority') != '1' ){
                            
                redirect('login/logout','refresh');
                exit;
            }
        ?>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h4><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h4></li>
                    <li><a><?php //echo form_submit('', '新增案件', 'class="btn btn-default" style="padding:0px 0px;"form="newcases_num"');?></a></li> -->
                </ul>
                    <?php 
                        //$hidden = array('c_num' => $c_num, 's_office' => $s_office, 'e_ed_empno' =>e_ed_empno);
                        echo form_open('cases/createcases_num','id="newcases_num"');
                    ?>
                <!-- /.navbar-top-links -->
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 案件管理系統<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="javascript:{}" onclick="document.getElementById('newcases_num').submit(); return false;">案件新增</a>
                                    </li>
                                    <li>
                                        <a href="listCases">案件列表</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-align-justify fa-fw"></i> 獎金管理<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href=<?php echo base_url("cases/list3Rewardlist") ?>>獎金申請</a>
                                    </li>
                                    <li>
                                        <a href=<?php echo base_url("cases/listRewardProject") ?>>一階獎金專案</a>
                                    </li>
                                    <li>
                                        <a href=<?php echo base_url("cases/list3RewardProject") ?>>獎金專案</a>
                                    </li>
                                    <li>
                                        <a href=<?php echo base_url("cases/rewardstate") ?>>獎金案件狀態</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <?php echo '<li><a href='.base_url("cases/admin").'>管理權限/修改密碼</span></a></li>'; ?>
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href=<?php echo base_url("login/changepw") ?>><i class="fa fa-sign-out fa-fw"></i> 更改密碼</a>
                            </li>
                            <li><a href=<?php echo base_url("login/logout") ?>><i class="fa fa-sign-out fa-fw"></i> 登出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>

