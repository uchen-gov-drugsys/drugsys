        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                    <!-- <li><a><button id='yes' class="btn btn-default"style="padding:0px 0px;" >確認修改</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p><span class="text-warning">【批次編輯】</span>金融支匯票專案，帳務專案：<mark><?php echo $fpid; ?></mark></p>
                    </blockquote>
                    <div class="row">
                        <div class="col-lg-4 text-left">
                            <a href="<?php echo base_url("Acc_cert/listfp1/".$f_no) ?>" class="btn btn-default">回上一頁</a>
                        </div>
                        <div class="col-md-4 text-center">
                            <h3><span class="label label-default"><?php echo $fp_paytype . $fp_type; ?></span></h3>
                        </div>
                        <div class="col-lg-4 text-right">
                            <input type="button" class="btn btn-default" id="saveAllBT" value="全部儲存" data-id="<?php echo $f_no_all; ?>" onclick="saveEvent('muti',$(this));"/>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px;">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading list_view">
                                  列表清單
                                </div>
                                <div class="panel-body">                                    
                                    <div class="table-responsive" style="height:250px">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                           </div>
                            <!-- /.panel -->
                            
                        </div>
                        
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row" style="margin-top:10px;">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                批次編輯作業
                                </div>
                                <div class="panel-body">
                                    <form id="editfrom">
                                        <?php 
                                        usort($data, function($a, $b){ 
                                            // return strcmp($a[0]->f_userid,$b[0]->f_userid);
                                            return ($a[0]->sort_no < $b[0]->sort_no)? -1 : 1; 
                                        }); 
                                        $i = 0;
                                        foreach($data as $row){ 
                                            $i++;?>
                                        <div class="panel panel-default">
                                            <div class="panel-body data-row row_<?php echo $i; ?>">
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                    <label class="">案件編號</label>
                                                    <strong class="text-primary col-md-12 text-right" id="ed_caseid" name="ed_caseid"><?php echo $row[0]->f_caseid; ?></strong>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label class="">受處分人姓名</label>
                                                    <strong class="text-primary col-md-12 text-right" id="ed_username" name="ed_username"><?php echo $row[0]->f_username. ((isset($row[0]->f_turnsub_log) && !empty($row[0]->f_turnsub_log))?'<br/><small class="text-warning">自'.explode('_',$row[0]->f_turnsub_log)[2].'轉正</small>':''); ?></strong>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label class="">身份證編號</label>
                                                    <strong class="text-primary col-md-12 text-right" id="ed_userid" name="ed_userid"><?php echo $row[0]->f_userid; ?></strong>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label class="">罰鍰/怠金</label>
                                                    <strong class="text-primary col-md-12 text-right" id="ed_amount" name="ed_amount"><?php echo (($row[0]->type === 'A')?(int)$row[0]->f_amount * 10000:$row[0]->f_amount); ?></strong>
                                                    <input type="hidden" name="f_amount[<?php echo $row[0]->f_no;?>]" value="<?php echo (($row[0]->type === 'A')?(int)$row[0]->f_amount * 10000:$row[0]->f_amount); ?>">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label class="">未繳金額</label>
                                                    <strong class="text-dnager col-md-12 text-right" id="ed_balance" name="ed_balance"><?php echo ((((($row[0]->type === 'A')?((int)$row[0]->f_amount * 10000):$row[0]->f_amount) - (int)$row[0]->f_doneamount - (int)$row[0]->f_paymoney) >= 0)?((($row[0]->type === 'A')?((int)$row[0]->f_amount * 10000):$row[0]->f_amount) - (int)$row[0]->f_doneamount - (int)$row[0]->f_paymoney):0); ?></strong>
                                                    <input type="hidden" name="f_balance[<?php echo $row[0]->f_no;?>]" value="<?php echo ((((($row[0]->type === 'A')?((int)$row[0]->f_amount * 10000):$row[0]->f_amount) - (int)$row[0]->f_doneamount - (int)$row[0]->f_paymoney) >= 0)?((($row[0]->type === 'A')?((int)$row[0]->f_amount * 10000):$row[0]->f_amount) - (int)$row[0]->f_doneamount - (int)$row[0]->f_paymoney):0); ?>">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label class="">繳款日期</label>
                                                    <strong class="text-primary col-md-12 text-right" id="ed_today" name="ed_today">
                                                        <?php echo $fp_date;?>
                                                    </strong>
                                                    <input type="hidden" name="f_paydate[<?php echo $row[0]->f_no;?>]" value="<?php echo $fp_date;?>">
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-2">
                                                    <label>繳納方式</label>
                                                    <?php 
                                                        $payway_temp = ($row[0]->fpart_type)?$row[0]->fpart_type:'金融(匯款)';
                                                    ?>
                                                    <select name="f_payway[<?php echo $row[0]->f_no;?>]" id="payway"  class="form-control" >                                                    
                                                        <option value="金融(匯款)" <?php echo ($payway_temp == '金融(匯款)')?'selected':''; ?>>金融(匯款)</option>
                                                        <option value="金融(存現)" <?php echo ($payway_temp == '金融(存現)')?'selected':''; ?>>金融(存現)</option>
                                                        <option value="金融(網銀/ATM)" <?php echo ($payway_temp == '金融(網銀/ATM)')?'selected':''; ?>>金融(網銀/ATM)</option>
                                                        <option value="金融(Pay.taipei)" <?php echo ($payway_temp == '金融(Pay.taipei)')?'selected':''; ?>>金融(Pay.taipei)</option>
                                                        <option value="金融(轉收)" <?php echo ($payway_temp == '金融(轉收)')?'selected':''; ?>>金融(轉收)</option>
                                                    </select>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label>繳納金額 <span class="text-danger">*</span></label>
                                                    <input type="number" value="<?php echo $row[0]->f_fee + $row[0]->f_turnsub + $row[0]->f_refund + (($row[0]->fpart_amount)?$row[0]->fpart_amount:0); ?>" class="form-control" name="t_payamount[<?php echo $row[0]->f_no;?>]" id="t_payamount"  onchange="computeEvent($(this))"/>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label><strong>(-)</strong> 執行費</label>
                                                    <input type="number" class="form-control" name="f_paycost[<?php echo $row[0]->f_no;?>]" id="paycost" onchange="computeEvent($(this))" value="<?php echo $row[0]->f_fee;?>"/>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label><a href="#" id="turnaroundlabel" class="text-reset" data-toggle="modal" data-target="#chooseModal" data-whatever="<?php echo $row[0]->f_username; ?>" data-caseid="<?php echo $row[0]->f_caseid; ?>" data-fno="<?php echo $row[0]->f_no; ?>" data-index="<?php echo $i; ?>" onclick="chooseEvent($(this))"><strong>(-)</strong> 轉正 </a><!--<small class="text-danger">(扣除請用負數)</small>--></label>
                                                    <input type="number"  class="form-control" name="f_turnaround[<?php echo $row[0]->f_no;?>]" id="turnaround" onchange="computeEvent($(this))" value="<?php echo $row[0]->f_turnsub;?>"/>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label><strong>(-)</strong> 退費 <!--<small class="text-danger">(扣除請用負數)</small>--></label>
                                                    <input type="number" class="form-control" name="f_returnamount[<?php echo $row[0]->f_no;?>]" id="returnamount" onchange="computeEvent($(this))" value="<?php echo $row[0]->f_refund;?>"/>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label><strong>(=)</strong> 核銷金額</label>
                                                    <input type="number" class="form-control" name="f_paymoney[<?php echo $row[0]->f_no;?>]" id="payamount"  value="<?php echo ($row[0]->fpart_amount)?$row[0]->fpart_amount:0;?>" readonly/>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label>繳納狀態</label>
                                                    <select  class="form-control" name="f_status[<?php echo $row[0]->f_no;?>]" id="paystatus" value="<?php echo (($row[0]->f_status)?$row[0]->f_status:'分期') ?>">
                                                        <option value="分期" <?php echo (($row[0]->f_status == '分期')?"selected":"") ?>>分期</option>
                                                        <option value="完納" <?php echo (($row[0]->f_status == '完納')?"selected":"") ?>>完納</option>
                                                    </select>
                                                    </div>
													<div class="form-group col-md-2">
                                                    <label>案件狀態</label>
                                                    <select  class="form-control" name="f_cstatus[<?php echo $row[0]->f_no;?>]" id="paycstatus" value="<?php echo (($row[0]->f_cstatus)?$row[0]->f_cstatus:(($row[0]->f_movelog)?'移送':'')) ?>">
														<option value="" <?php echo (($row[0]->f_cstatus == '' || null == $row[0]->f_cstatus)?"selected":"") ?>>無</option>
                                                        <option value="移送" <?php echo (($row[0]->f_cstatus == '移送')?"selected":"") ?> >移送</option>
                                                        <option value="發憑證" <?php echo (($row[0]->f_cstatus == '發憑證')?"selected":"") ?> >發憑證</option>
                                                        <option value="再移送" <?php echo (($row[0]->f_cstatus == '再移送')?"selected":"") ?> >再移送</option>
                                                    </select>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label>來源</label>
                                                    <?php 
                                                        $source_temp = ($row[0]->f_source)?explode("_",$row[0]->f_source)[0]:'義務人';
                                                        
                                                    ?>
                                                    <select name="source[<?php echo $row[0]->f_no;?>]" id="source"  class="form-control" onload="changeUnitEvent($(this))" onchange="changeUnitEvent($(this))" >
                                                        <option value="義務人" <?php echo ($source_temp == '義務人')?'selected':''; ?>>義務人</option>
                                                        <option value="銀行" <?php echo ($source_temp == '銀行')?'selected':''; ?>>銀行</option>
                                                        <option value="機關" <?php echo ($source_temp == '機關')?'selected':''; ?>>機關</option>
                                                        <option value="其他" <?php echo ($source_temp == '其他')?'selected':''; ?>>其他</option>
                                                    </select>
                                                    </div>
                                                    <div class="form-group col-md-2 <?php echo ($source_temp != '義務人')?'':'hidden'; ?>" id="source_wrap">
                                                    <label>常用行庫/分署</label>
                                                    <select name="source_unit[<?php echo $row[0]->f_no;?>]" id="source_unit"  class="form-control" >
                                                    <?php
                                                        switch ($source_temp) {
                                                            case '義務人':
                                                                break;
                                                            case '銀行':
                                                                $selectAry = array(
                                                                    "臺灣銀行",
                                                                    "中華郵政股份有限公司",
                                                                    "中國信託商業銀行股份有限公司",
                                                                    "國泰世華商業銀行存匯作業管理部",
                                                                    "合作金庫商業銀行",
                                                                    "玉山銀行個金集中部",
                                                                    "華南商業銀行股份有限公司",
                                                                    "台北富邦商業銀行股份有限公司個金作業服務部",
                                                                    "臺灣銀行股份有限公司",
                                                                    "聯邦商業銀行",
                                                                    "台新國際商業銀行股份有限公司",
                                                                    "台中商業銀行總行",
                                                                    "臺灣土地銀行股份有限公司",
                                                                    "臺灣中小企業銀行",
                                                                    "臺中商業銀行總行",
                                                                    "臺灣新光商業銀行股份有限公司集中作業部",
                                                                    "兆豐國際商業銀行股份有限公司",
                                                                    "日盛國際商業銀行股份有限公司作業處",
                                                                    "滙豐(台灣)商業銀行股份有限公司",
                                                                    "第一商業銀行總行",
                                                                    "渣打國際商業銀行股份有限公司",
                                                                    "元大商業銀行股份有限公司",
                                                                    "永豐商業銀行作業處",
                                                                    "華泰商業銀行股份有限公司",
                                                                    "彰化商業銀行有限公司作業處",
                                                                    "上海商業儲蓄銀行台北票據匯款理中心",
                                                                    "安泰商業銀行",
                                                                    "遠東國際商業銀行",
                                                                    "星展(臺灣)商業銀行",
                                                                    "板信商業銀行作業服務部",
                                                                    "陽信商業銀行作業中心",
                                                                    "花旗(台灣)商業銀行股份有限公司"
                                                                );
                                                                foreach ($selectAry as $key => $value) {
                                                                    echo "<option value=".$value."".((explode("_",$row[0]->f_source)[1] == $value)?'selected':'').">".$value."</option>";
                                                                }
                                                                break;
                                                            case '機關':
                                                                $selectAry1 = array(
                                                                    "法務部行政執行署臺北分署",
                                                                    "法務部行政執行署新北分署",
                                                                    "法務部行政執行署桃園分署",
                                                                    "法務部行政執行署新竹分署",
                                                                    "法務部行政執行署臺中分署",
                                                                    "法務部行政執行署彰化分署",
                                                                    "法務部行政執行署嘉義分署",
                                                                    "法務部行政執行署臺南分署",
                                                                    "法務部行政執行署高雄分署",
                                                                    "法務部行政執行署屏東分署",
                                                                    "法務部行政執行署花蓮分署",
                                                                    "法務部行政執行署宜蘭分署",
                                                                    "法務部行政執行署士林分署"
                                                                );
                                                                $selectAry2 = array(
                                                                    "法務部矯正署臺北監獄",
                                                                    "法務部矯正署桃園監獄",
                                                                    "法務部矯正署桃園女子監獄",
                                                                    "法務部矯正署新竹監獄",
                                                                    "法務部矯正署臺中監獄",
                                                                    "法務部矯正署臺中女子監獄",
                                                                    "法務部矯正署彰化監獄",
                                                                    "法務部矯正署雲林監獄",
                                                                    "法務部矯正署雲林第二監獄",
                                                                    "法務部矯正署嘉義監獄",
                                                                    "法務部矯正署臺南監獄",
                                                                    "法務部矯正署臺南第二監獄",
                                                                    "法務部矯正署明德外役監獄",
                                                                    "法務部矯正署高雄監獄",
                                                                    "法務部矯正署高雄第二監獄",
                                                                    "法務部矯正署高雄女子監獄",
                                                                    "法務部矯正署屏東監獄",
                                                                    "法務部矯正署臺東監獄",
                                                                    "法務部矯正署花蓮監獄",
                                                                    "法務部矯正署自強外役監獄",
                                                                    "法務部矯正署宜蘭監獄",
                                                                    "法務部矯正署基隆監獄",
                                                                    "法務部矯正署澎湖監獄",
                                                                    "法務部矯正署綠島監獄",
                                                                    "法務部矯正署金門監獄",
                                                                    "法務部矯正署八德外役監獄"
                                                                );
                                                                echo "<option disabled>行政執行機關</option>";
                                                                foreach ($selectAry1 as $key => $value) {
                                                                    echo "<option value=".$value."".((explode("_",$row[0]->f_source)[1] == $value)?'selected':'').">".$value."</option>";
                                                                }
                                                                echo "<option disabled>矯正機關</option>";
                                                                foreach ($selectAry2 as $key => $value) {
                                                                    echo "<option value=".$value."".((explode("_",$row[0]->f_source)[1] == $value)?'selected':'').">".$value."</option>";
                                                                }
                                                                break;
                                                            default:
                                                                # code...
                                                                break;
                                                        }
                                                    ?>
                                                    </select>
                                                    <input type="text"  class="form-control" placeholder="分行名稱/公司名" name="source_other[<?php echo $row[0]->f_no;?>]" id="source_other"  value="<?php echo ($source_temp != '義務人')?explode("_",$row[0]->f_source)[2]:'' ?>"/>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                    <label>備註</label>
                                                    <input class="form-control" name="f_paycomment[<?php echo $row[0]->f_no;?>]" id="paycomment" value="<?php echo ($row[0]->fp_comment)?$row[0]->fp_comment:''; ?>"/>
                                                    </div>
                                                </div>
                                                <div class="row text-right">
                                                    <div class="col-md-12">
                                                        <input type="button" class="btn btn-warning" id="saveBT" data-id="<?php echo $row[0]->f_no;?>" data-ser="<?php echo $row[0]->fpart_num;?>" value="儲存" onclick="saveEvent('single',$(this));"/>
                                                    </div>                                      
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </form>
                                </div>
                            </div>
                            <!-- /.panel --> 
                                <!-- </br>
                                <div class="panel-heading list_view">
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 案件編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 姓名
                                    <input type="checkbox" name="list" data-target ="3" checked> 身份證編號
                                    <input type="checkbox" name="list" data-target ="4" checked> 年度罰緩金額
                                    <input type="checkbox" name="list" data-target ="5"> 完納金額
                                    <input type="checkbox" name="list" data-target ="6"> 分期資訊
                                    <input type="checkbox" name="list" data-target ="7" checked> 移送案號
                                    <input type="checkbox" name="list" data-target ="8"> 分署
                                    <input type="checkbox" name="list" data-target ="9"> 憑證核發日期
                                    <input type="checkbox" name="list" data-target ="10"> 憑證編號
                                    <input type="checkbox" name="list" data-target ="11"> 註銷保留款
                                    <input type="checkbox" name="list" data-target ="12"> 虛擬帳號
                                </div> -->
                            <!-- <form action=<?php //echo base_url("Acc_cert/editfplist") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php //echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                                <input id="f_no" type="hidden" name="f_no" value='<?php //echo $f_no ?>'> 
                            </form> -->
                           <!-- </div> -->
                            <!-- /.panel -->
                        </div>
                           
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->

                    <!-- chooseModal -->
                    <div class="modal fade" id="chooseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog  modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">選取待轉正之帳務資料</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive" style="height:350px">
                                    <table id="chooseTable" class="table table-bordered" style="width:1600px;">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>類型</th>
                                                <th>案件編號</th>
                                                <th>受處分人姓名</th>
                                                <th>身份證編號</th>
                                                <th>
                                                罰鍰(萬)/怠金(元)                                        
                                                </th>
                                                <th>完納日期</th>
                                                <th>完納金額</th>
                                                <th>繳款日期</th>
                                                <th>繳款金額</th>
                                                <th>分期Log</th>
                                                <th>移送Log</th>
                                                <th>憑證Log</th>
                                                <th>撤銷註銷Log</th>
                                                <th>執行命令Log</th>
                                                <th>備註</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                            </div> 
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                            <button type="button" class="btn btn-info" data-pjid="<?php echo $f_no; ?>" data-caseid="" data-fno="" data-index="" id="addProjectBT" onclick="addProjectEvent($(this))">加入</button>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    "targets": [ 18 ],
					"visible": false
                 }
              ],
            //   'select': {
            //      'style': 'multi'
            //   },
              'order': [[18, 'asc']],
                dom: 'f',
                searching: false,
             "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $(nRow).attr("data-id", aData[0]);
                $("td:first", nRow).html(iDisplayIndex + 1); //自動序號
				if(iDisplayIndex >= 0)
				{
						if(aData[13].indexOf('<?php echo $fp_date; ?>') > -1 && aData[13].indexOf('<?php echo $fp_date; ?>已繳0元') == -1 && aData[13].indexOf('無分期繳款記錄') == -1)
						{
							$(nRow).addClass('bg-success');
						}
							
						if(aData[6].indexOf('<?php echo $fp_date; ?>') > -1)
						{
							$(nRow).addClass('bg-success');
						}
						
						if(aData[11] == 0 && aData[5] == 0)
						{
							$(nRow).removeClass('bg-success');
						}
				}
                return nRow;
              },
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                },
           });
           $('#chooseTable').css('width','1600px');
           $('#chooseTable tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
            } );
                // table.column(5).visible(false);
                // table.column(6).visible(false);
                // table.column(8).visible(false);
                // table.column(9).visible(false);
                // table.column(10).visible(false);
                // table.column(11).visible(false);
                // table.column(12).visible(false);
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });
            // $("#table1 tbody tr").each(function(index,element)
            // {
            //     now = new Date();
            //     if($(this).children('td:eq(13)').text().indexOf('<?php //echo $fp_date; ?>') > -1 && $(this).children('td:eq(13)').text().indexOf('<?php //echo $fp_date; ?>已繳0元') == -1 && $(this).children('td:eq(13)').text().indexOf('無分期繳款記錄') == -1)
            //     {
            //         $(this).addClass('bg-success');
            //     }
            //     else if($(this).children('td:eq(6)').text().indexOf('<?php //echo $fp_date; ?>') > -1)
            //     {
            //         $(this).addClass('bg-success');
            //     }
            // })
            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
    });

    function saveEvent(type, obj)
    {
    //   console.log(new FormData($("#editfrom")[0]));
    //   return false;
      if(type.indexOf("single") > -1)
      {
			if(!$('#t_payamount').val())			
			{
				swal({
					title: "警告!",
					text: "繳納金額為必填欄位",
					icon: "warning"
				});
				return false;
			}

            data_row = obj.parent().parent().parent();
            now = new Date()
            var formData = new FormData();
            formData.append('f_no', obj.attr('data-id'));
			formData.append('fpart_num', obj.attr('data-ser'));
            formData.append('f_paymoney', data_row.find("#payamount").val());
            formData.append('f_payway', data_row.find("#payway").val());
            formData.append('f_paycost', data_row.find("#paycost").val());
            formData.append('f_turnaround', data_row.find("#turnaround").val());
            formData.append('f_returnamount', data_row.find("#returnamount").val());
            formData.append('f_paycomment', data_row.find("#paycomment").val());
            formData.append('f_paydate', data_row.find("#ed_today").text());
            formData.append('f_balance', data_row.find("#ed_balance").text());
            formData.append('f_amount', data_row.find("#ed_amount").text());
            formData.append('f_status', data_row.find("#paystatus").val());
			formData.append('f_cstatus', data_row.find('#paycstatus').val());
            formData.append('t_payamount', data_row.find("#t_payamount").val());
            formData.append('source', data_row.find("#source").val());
            formData.append('source_unit', data_row.find("#source_unit").val());
            formData.append('source_other', data_row.find("#source_other").val());
            formData.append('project_id', '<?php echo $fpid?>');

            $.ajax({            
                    type: 'POST',
                    url: '../../Acc_cert/add_lisfp1_paypart',
                    data: formData,
                    // dataType: 'json',
                    processData : false, 
                    contentType: false,
                    cache: false,
                    error:function(){
                    console.log('error')
                    },
                    success: function(resp){
                        console.log('success')
                        if(resp == 'ok')
                        {
                            location.reload();
                        }
                        
                        
                    },
                    complete:function(resp){
                    }
                });
      }
      else
      {
            data = obj.attr("data-id").split(',');
            var formData = new FormData($("#editfrom")[0]);
            formData.append('id', obj.attr("data-id"));
			formData.append('fpart_num', obj.attr('data-ser'));
            formData.append('project_id', '<?php echo $fpid?>');
            $.ajax({            
                    type: 'POST',
                    url: '../../Acc_cert/add_lisfp1_ed_paypart',
                    data: formData,
                    // dataType: 'json',
                    processData : false, 
                    contentType: false,
                    cache: false,
                    error:function(){
                    console.log('error')
                    },
                    success: function(resp){
                        console.log('success')
                        if(resp == 'ok')
                        {
                            location.reload();
                        }
                        
                        
                    },
                    complete:function(resp){
                    }
                });
            
      }
      
    }
    function changeUnitEvent(obj)
    {
       source = obj.val()
       sourcewrap = obj.parent().parent().find("#source_wrap")
       source_uni = {}
       sourcewrap.find('#source_unit').html('');
       if(source.indexOf('義務人') > -1)
       {
        sourcewrap.addClass('hidden')
       }
       else
       {
        sourcewrap.removeClass('hidden')
        $.ajax({            
            type: 'POST',
            url: '../../Acc_cert/getFine_useful_source?source='+source,
            // data: formData,
            dataType: 'json',
            processData : false, 
            contentType: false,
            cache: false,
            success: function(resp){
              source_useful = resp.data
              
              if(source_useful.length > 0)
              {
                sourcewrap.find('#source_unit').append("<option disabled>常用項目</option>");
                sourcewrap.find('#source_unit').append("<option value='null'>無</option>");
                for (let index = 0; index < source_useful.length; index++) {
                    sourcewrap.find('#source_unit')
                        .append($('<option>', { value : source_useful[index]["source_uni"] })
                        .text(source_useful[index]["source_uni"]));
                  
                }
              }
            },
            complete:function(resp){
              if(source.indexOf('銀行') > -1)
              {
                source_uni = { 
                "臺灣銀行":"臺灣銀行",
                "中華郵政股份有限公司":"中華郵政股份有限公司",
                "中國信託商業銀行股份有限公司":"中國信託商業銀行股份有限公司",
                "國泰世華商業銀行存匯作業管理部":"國泰世華商業銀行存匯作業管理部",
                "合作金庫商業銀行":"合作金庫商業銀行",
                "玉山銀行個金集中部":"玉山銀行個金集中部",
                "華南商業銀行股份有限公司":"華南商業銀行股份有限公司",
                "台北富邦商業銀行股份有限公司個金作業服務部":"台北富邦商業銀行股份有限公司個金作業服務部",
                "臺灣銀行股份有限公司":"臺灣銀行股份有限公司",
                "聯邦商業銀行":"聯邦商業銀行",
                "台新國際商業銀行股份有限公司":"台新國際商業銀行股份有限公司",
                "台中商業銀行總行":"台中商業銀行總行",
                "臺灣土地銀行股份有限公司":"臺灣土地銀行股份有限公司",
                "臺灣中小企業銀行":"臺灣中小企業銀行",
                "臺中商業銀行總行":"臺中商業銀行總行",
                "臺灣新光商業銀行股份有限公司集中作業部":"臺灣新光商業銀行股份有限公司集中作業部",
                "兆豐國際商業銀行股份有限公司":"兆豐國際商業銀行股份有限公司",
                "日盛國際商業銀行股份有限公司作業處":"日盛國際商業銀行股份有限公司作業處",
                "滙豐(台灣)商業銀行股份有限公司":"滙豐(台灣)商業銀行股份有限公司",
                "第一商業銀行總行":"第一商業銀行總行",
                "渣打國際商業銀行股份有限公司":"渣打國際商業銀行股份有限公司",
                "元大商業銀行股份有限公司":"元大商業銀行股份有限公司",
                "永豐商業銀行作業處":"永豐商業銀行作業處",
                "華泰商業銀行股份有限公司":"華泰商業銀行股份有限公司",
                "彰化商業銀行有限公司作業處":"彰化商業銀行有限公司作業處",
                "上海商業儲蓄銀行台北票據匯款理中心":"上海商業儲蓄銀行台北票據匯款理中心",
                "安泰商業銀行":"安泰商業銀行",
                "遠東國際商業銀行":"遠東國際商業銀行",
                "星展(臺灣)商業銀行":"星展(臺灣)商業銀行",
                "板信商業銀行作業服務部":"板信商業銀行作業服務部",
                "陽信商業銀行作業中心":"陽信商業銀行作業中心",
                "花旗(台灣)商業銀行股份有限公司":"花旗(台灣)商業銀行股份有限公司"
                };
                sourcewrap.find('#source_unit').append("<option disabled>選擇其他</option>");
                $.each(source_uni, function(key, value) {
                    sourcewrap.find('#source_unit').append($('<option>', { value : key })
                          .text(value));
                });
              }        
              else if(source.indexOf('機關') > -1)
              {
                source_uni1 = { 
                "法務部行政執行署臺北分署":"法務部行政執行署臺北分署",
                "法務部行政執行署新北分署":"法務部行政執行署新北分署",
                "法務部行政執行署桃園分署":"法務部行政執行署桃園分署",
                "法務部行政執行署新竹分署":"法務部行政執行署新竹分署",
                "法務部行政執行署臺中分署":"法務部行政執行署臺中分署",
                "法務部行政執行署彰化分署":"法務部行政執行署彰化分署",
                "法務部行政執行署嘉義分署":"法務部行政執行署嘉義分署",
                "法務部行政執行署臺南分署":"法務部行政執行署臺南分署",
                "法務部行政執行署高雄分署":"法務部行政執行署高雄分署",
                "法務部行政執行署屏東分署":"法務部行政執行署屏東分署",
                "法務部行政執行署花蓮分署":"法務部行政執行署花蓮分署",
                "法務部行政執行署宜蘭分署":"法務部行政執行署宜蘭分署",
                "法務部行政執行署士林分署":"法務部行政執行署士林分署"
                };
                source_uni2 = { 
                  "法務部矯正署臺北監獄":"法務部矯正署臺北監獄",
                  "法務部矯正署桃園監獄":"法務部矯正署桃園監獄",
                  "法務部矯正署桃園女子監獄":"法務部矯正署桃園女子監獄",
                  "法務部矯正署新竹監獄":"法務部矯正署新竹監獄",
                  "法務部矯正署臺中監獄":"法務部矯正署臺中監獄",
                  "法務部矯正署臺中女子監獄":"法務部矯正署臺中女子監獄",
                  "法務部矯正署彰化監獄":"法務部矯正署彰化監獄",
                  "法務部矯正署雲林監獄":"法務部矯正署雲林監獄",
                  "法務部矯正署雲林第二監獄":"法務部矯正署雲林第二監獄",
                  "法務部矯正署嘉義監獄":"法務部矯正署嘉義監獄",
                  "法務部矯正署臺南監獄":"法務部矯正署臺南監獄",
                  "法務部矯正署臺南第二監獄":"法務部矯正署臺南第二監獄",
                  "法務部矯正署明德外役監獄":"法務部矯正署明德外役監獄",
                  "法務部矯正署高雄監獄":"法務部矯正署高雄監獄",
                  "法務部矯正署高雄第二監獄":"法務部矯正署高雄第二監獄",
                  "法務部矯正署高雄女子監獄":"法務部矯正署高雄女子監獄",
                  "法務部矯正署屏東監獄":"法務部矯正署屏東監獄",
                  "法務部矯正署臺東監獄":"法務部矯正署臺東監獄",
                  "法務部矯正署花蓮監獄":"法務部矯正署花蓮監獄",
                  "法務部矯正署自強外役監獄":"法務部矯正署自強外役監獄",
                  "法務部矯正署宜蘭監獄":"法務部矯正署宜蘭監獄",
                  "法務部矯正署基隆監獄":"法務部矯正署基隆監獄",
                  "法務部矯正署澎湖監獄":"法務部矯正署澎湖監獄",
                  "法務部矯正署綠島監獄":"法務部矯正署綠島監獄",
                  "法務部矯正署金門監獄":"法務部矯正署金門監獄",
                  "法務部矯正署八德外役監獄":"法務部矯正署八德外役監獄"

                };


                sourcewrap.find('#source_unit').append("<option disabled>行政執行機關</option>");
                $.each(source_uni1, function(key, value) {
                    sourcewrap.find('#source_unit').append($('<option>', { value : key })
                          .text(value));
                });
                sourcewrap.find('#source_unit').append("<option disabled>矯正機關</option>");
                $.each(source_uni2, function(key, value) {
                    sourcewrap.find('#source_unit')
                          .append($('<option>', { value : key })
                          .text(value));
                });
              }
              else
              {
                
                sourcewrap.find('#source_unit').change(function(){
                  sourcewrap.find('#source_other').val($(this).val());
                });
              }
              
            }
        });


       }
    }
    function computeEvent(obj)
    {
        row_data = obj.parent().parent();
      t_pay_amount = parseInt(row_data.find("#t_payamount").val())
      paycost = parseInt(row_data.find("#paycost").val())
      turnaround = parseInt(row_data.find("#turnaround").val())
      returnamount = parseInt(row_data.find("#returnamount").val())

      row_data.find("#payamount").val(t_pay_amount - paycost - turnaround - returnamount);
    }
    function chooseEvent(obj)
    {
      name = obj.attr('data-whatever');
      pjid = '<?php echo $fpid?>';
      $("#addProjectBT").attr('data-caseid', obj.attr('data-caseid'))
      $("#addProjectBT").attr('data-fno', obj.attr('data-fno'))
      $("#addProjectBT").attr('data-index', obj.attr('data-index'))
      $('#chooseTable').css('width','1600px');
      chooseTable = $('#chooseTable').DataTable({	
                "destroy":true,	
                dom: 'Bf',
                buttons: [ 
                    { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                    { extend: 'pageLength', text: '每頁顯示筆數' }
                ],
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                ],	
                "bProcessing": true, //顯示『資料載入中』							
                "sAjaxSource": "../../Acc_cert/get_fine_in_person?name=" + name + "&pjid=" + pjid, // API
                "aoColumns": [
                    { "mData": "type" },
                    { "mData": "type",
                        "mRender":function(val,type,row){
                            if(val=='A'){
                                return `<span class="label label-danger">罰鍰</span>`;
                            }else{
                                return `<span class="label label-warning">怠金</span>`;
                            }
                        } 
                    },
                    { "mData": "f_caseid"},
                    { "mData": "f_username"},
                    { "mData": "f_userid"},
                    { "mData": "f_amount", "sWidth": '80px'},
                    { "mData": "f_donedate",
                        "mRender":function(val,type,row){
                            return (val == '0000-00-00')?'':tranfer2RCyear(val);
                        } 
                    },
                    { "mData": "f_doneamount"},
                    { "mData": "f_paydate",
                        "mRender":function(val,type,row){
                            return (val == '0000-00-00')?'':tranfer2RCyear(val);
                        } 
                    },
                    { "mData": "f_paymoney"},
                    { "mData": "f_paylog",
                        "mRender":function(val,type,row){
                          return ((val)?val.replace(/\r\n|\n|\r/g,"<br/>"):"");
                        }},
                    { "mData": "f_movelog",
						"mRender": function(val,type,row){
							if(val)
							{
								let calcmovelog = val.split(/\r\n|\r|\n/);
								if(calcmovelog.length > 1)
								{
									calcmovelog.sort(function(a, b) {
										var movedateA = a.split('_')[0] * 1; 
										var movedateB = b.split('_')[0] * 1; 
										if (movedateA < movedateB) {
											return 1;
										}
										if (movedateA > movedateB) {
											return -1;
										}

										// names must be equal
										return 0;
									});

									return calcmovelog.join('<br/>');
								}
								else
								{
									return val;
								}
								
							}
							else
							{
								return val;
							}
							
						}
					},
                    { "mData": "f_cardlog"},
                    { "mData": "f_dellog"},
                    { "mData": "f_execlog",
                        "mRender":function(val,type,row){
                          return ((val)?val.replace(/\r\n|\n|\r/g,"<br/>"):"");
                        } 
                    },
                    { "mData": "f_comment"}
                ],                
                "bAutoWidth": true,
                "ordering":false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    $(nRow).attr("data-id", aData.f_no);
                    $("td:first", nRow).html(iDisplayIndex + 1); //自動序號

                    let movedate = (aData.f_movelog)?tranfer2ADyear((aData.f_movelog.split("\n")[aData.f_movelog.split("\n").length -1]).split('_')[0]):'';
                    let execdate = (aData.f_execlog)?tranfer2ADyear((aData.f_execlog.split("\n")[aData.f_execlog.split("\n").length -1]).split('_')[0]):'';
                    let donedate = (aData.f_donedate !== '0000-00-00')?aData.f_donedate:'';

                    if(new Date(movedate).getTime() > new Date(donedate).getTime() )
                    {
                        $(nRow).addClass('bg-danger');
                    }
                    if(new Date(execdate).getTime() > new Date(donedate).getTime() )
                    {
                        $(nRow).addClass('bg-warning');
                    }
                    
                    if(aData.f_comment !== "")
                    {
                        $(nRow).addClass('bg-info');
                    }
                    if(aData.f_dellog)
                    {
                        if(!$(nRow).children('td:eq(2)').has( "span" ).length)
                        {
                            $(nRow).children('td:eq(2)').append('<span class="text-danger">(註)</span>')
                        }                        
                    }
                    return nRow;
                },
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                },  
                
            });
            
            
    }
    function tranfer2ADyear(date)
    {
        if(date.length == 6)
        {
            ad = (parseInt(date.substr(0, 2))) + 1911;
            return ad.toString() + '-' + date.substr(2, 2) + '-' + date.substr(4, 2);
        }
        else if(date.length == 7)
        {
            ad = (parseInt(date.substr(0, 3))) + 1911;
            return ad.toString() + '-' + date.substr(3, 2) + '-' + date.substr(5, 2);
        }
        else
        {
            return '';
        }
    }
    function addProjectEvent(obj)
    {
        if(parseInt($("input[name='t_payamount["+obj.attr("data-fno")+"]']").val()) <= 0)
        {
          alert("請填寫繳納金額，不可為零！")
          return false;
        }

        var rows_selected = $.map(chooseTable.rows('.selected').nodes(), function (item) {
                return $(item).attr("data-id");
            });  
        if(rows_selected.length <= 0)
        {
          alert("請選擇至少一筆要轉正的帳務資料！")
          return false;
        }   
        var amountAry = $.map(chooseTable.rows('.selected').nodes(), function (item) {
                return {'type':(($(item).children('td:eq(2)').text().indexOf('A') > -1)?'怠金':'罰鍰'),'amount':(($(item).children('td:eq(2)').text().indexOf('A') > -1)?$(item).children('td:eq(5)').text():(parseInt($(item).children('td:eq(5)').text())*10000)), 'doneamount':$(item).children('td:eq(7)').text(), 'payamount':$(item).children('td:eq(9)').text()};
            }); 

        project_id = obj.attr("data-pjid");
        case_id = obj.attr("data-caseid");
        f_no = obj.attr("data-fno");
        row_index = obj.attr("data-index");
        var formData = new FormData();
        formData.append('project_id', project_id);
        formData.append('s_cnum', rows_selected.join(","));
        formData.append('caseid',case_id);
        formData.append('t_payamount', (parseInt($("input[name='t_payamount["+f_no+"]']").val()) - parseInt($("input[name='f_paycost["+f_no+"]']").val()) - parseInt($("input[name='f_balance["+f_no+"]']").val())));
        formData.append('amountAry',  JSON.stringify(amountAry));
        formData.append('paydate', $("input[name='f_paydate["+f_no+"]']").val());
        formData.append('pj_type', '<?php echo $fp_type; ?>');

        $.ajax({            
            type: 'POST',
            url: '../../Acc_cert/addAcc_Project_by_person',
            data: formData,
            // dataType: 'json',
            processData : false, 
            contentType: false,
            cache: false,
            error:function(){
                console.log('error')
            },
            success: function(resp){
                if(resp == 'ok')
                {
                    data_row = $(".row_"+parseInt(row_index));
                    var formData = new FormData();
                    formData.append('f_no', data_row.find("#saveBT").attr('data-id'));
					formData.append('fpart_num', data_row.find("#saveBT").attr('data-ser'));
                    formData.append('f_paymoney', ((parseInt(data_row.find("#t_payamount").val())-parseInt(data_row.find("#paycost").val())-parseInt(data_row.find("#ed_balance").text()) - parseInt(data_row.find("#returnamount").val())) >= 0)?parseInt(data_row.find("#ed_balance").text()):data_row.find("#payamount").val());
                    formData.append('f_payway', data_row.find("#payway").val());
                    formData.append('f_paycost', data_row.find("#paycost").val());
                    formData.append('f_turnaround', data_row.find("#turnaround").val());
                    formData.append('f_returnamount', data_row.find("#returnamount").val());
                    formData.append('f_paycomment', data_row.find("#paycomment").val());
                    formData.append('f_paydate', data_row.find("#ed_today").text().replace(/(^[\s]*)/g, ""));
                    formData.append('f_balance', data_row.find("#ed_balance").text());
                    formData.append('f_amount', data_row.find("#ed_amount").text());
                    formData.append('f_status', data_row.find("#paystatus").val());
					formData.append('f_cstatus', data_row.find('#paycstatus').val());
                    formData.append('t_payamount', data_row.find("#t_payamount").val());
                    formData.append('source', data_row.find("#source").val());
                    formData.append('source_unit', data_row.find("#source_unit").val());
                    formData.append('source_other', data_row.find("#source_other").val());
                    formData.append('project_id', '<?php echo $fpid?>');

                    $.ajax({            
                            type: 'POST',
                            url: '../../Acc_cert/add_lisfp1_paypart',
                            data: formData,
                            // dataType: 'json',
                            processData : false, 
                            contentType: false,
                            cache: false,
                            error:function(){
                            console.log('error')
                            },
                            success: function(resp){
                                console.log('success')
                                if(resp == 'ok')
                                {
                                    location.href = window.location.search + "," + rows_selected.join(",")
                                }
                                
                                
                            },
                            complete:function(resp){
                            }
                        });
                }
            },
            complete:function(resp){
              console.log('complete')
            }
        });
    }
	  /** 轉民國年 */
	  function tranfer2RCyear(date)
	{
		if(!date)
			return false;
		date = date.split('-');
		date = (parseInt(date[0]) - 1911) + "-" + date[1] + "-" + date[2];
			return date;                
	} 
    </script>
