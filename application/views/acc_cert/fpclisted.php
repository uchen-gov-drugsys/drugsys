        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                    <!-- <li><a><button id='yes' class="btn btn-default"style="padding:0px 0px;" >回到列表</button></a></li>
                    <li><a><button  id='mergesel' style="padding:0px 0px;" class="btn btn-default" data-toggle="modal" data-target="#com" data-whatever="合併">合併</button></a></li>
                    <li><a><button id='transfer' class="btn btn-default"style="padding:0px 0px;" >單筆轉移</button></a></li>
                    <li><a><button id='disassemble' class="btn btn-default"style="padding:0px 0px;" >分解</button></a></li> -->
                    <!--li><a><button id='disassemble' class="btn btn-default"style="padding:0px 0px;" >拆解</button></a></li-->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p><span class="text-warning">【編輯】</span>轉正退費專案，帳務專案：<mark><?php echo $fpid; ?></mark></p>
                    </blockquote>
                    <div class="row">
                        <div class="col-md-6 text-left">
                              <a href=<?php echo base_url("Acc_cert/listFineCProject") ?> type="button" class="btn btn-default " >回上一頁</a>                                                          
                        </div>
                        <div class="col-md-6 text-right">
                            <button id='returnBT' class="btn btn-info" data-toggle="modal" data-target="#returnFineModal" disabled="true">退費</button>     
                            <button type="button" class="btn btn-danger" id="delFineBT" disabled="true">刪除</button>                                                     
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading list_view">
                                列表清單
                                    
                                </div>
                                <form action=<?php echo base_url("Acc_cert/editfcplist1") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                    <div class="panel-body">
                                        <div class="table-responsive" style="height:300px">
                                            <?php echo $s_table;?>
                                        </div>                       
                                    </div>
                                    <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                    <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                    <input id="s_status" type="hidden" name="s_status" value=''> 
                                    <input id="f_no" type="hidden" name="f_no" value='<?php echo $f_no ?>'> 
                                    <div class="modal fade" id="com" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="othernewLabel">合併欄位</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table" id="example">
                                                        <tbody>
                                                            <tr>
                                                                <td>處分書編號</td>
                                                                <td><select id="select1" name="selcnum" class="form-control"></select></td>
                                                                <td><button id='merge' type="button" class="btn btn-default">帶入</button></td>
                                                            </tr>
                                                        </tbody> 
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                            
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-md-4 ">
                              <button type="button" class="btn btn-success btn-block"  id='mergesel' data-toggle="modal" data-target="#chooseModal" disabled="true">合併</button> 
                        </div>
                        <div class="col-md-4">
                            <button id='transfer' class="btn btn-warning btn-block" data-toggle="modal" data-target="#chooseModal"  disabled="true">單筆轉移</button> 
                        </div>
                        <div class="col-md-4">
                            <button id='disassemble' data-toggle="modal" data-target="#chooseModal" class="btn btn-danger btn-block" disabled="true">分解</button> 
                        </div>
                    </div>
                    <hr>
                    <div class="container-fluid">
                        <div id="selectdata-wrap">
                        
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-2 col-md-offset-5"  style="background-color:#f8f9fa;border-radius: 20px;margin-top:10px;margin-bottom:10px;">
                            <h3 class=" text-center" id="transfertype"></h3>
                            <h3 class="text-center" ><span class="fa fa-caret-down"></span></h3>
                        </div>
                        
                    </div>
                    <div class="container-fluid">
                        <form id="turnsubForm">
                            <div id="transferdata-wrap">
                            
                            </div>
                        </form>
                    </div>
                    <div class="row text-right">
                        <div class="col-md-12">
                        <hr>
                        <input type="button" class="btn btn-warning" id="saveBT" data-id="" value="儲存" disabled="true"/>
                        </div>                                      
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
            <div class="modal fade" id="returnFineModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h4 class="modal-title" id="exampleModalLabel">退費作業</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="addEmptyFineForm">
                        <div class="row">
                            <div class="form-group col-md-4">  
                                <label><small>案件編號</small></label> 
                                <p id="return_caseid"></p>
                            </div>
                            <div class="form-group col-md-4">  
                                <label><small>受處分人姓名</small></label> 
                                <p id="return_username"></p>
                            </div>
                            <div class="form-group col-md-4">  
                                <label><small>身份證編號</small></label> 
                                <p id="return_userid"></p>
                            </div>
                            <div class="form-group col-md-4">  
                                <label><small>罰鍰/怠金</small></label> 
                                <p id="return_amount" class="text-primary"></p>
                            </div>
                            <div class="form-group col-md-4">  
                                <label><small>未繳金額</small></label> 
                                <p id="return_balance" class="text-danger"></p>
                            </div>
                            <div class="form-group col-md-4">  
                                <label><small>繳費日期</small></label> 
                                <p><?php echo $fcp_createdate; ?></p>
                                <input type="hidden" class="form-control" name="return_paydate" id="return_paydate" value="<?php echo $fcp_createdate; ?>"/> 
                            </div>
                            <div class="form-group col-md-12">  
                                <label>退費金額 <small class="text-danger">(扣除請用負數)</small></label> 
                                <input type="number" class="form-control" name="returnmoney" id="returnmoney"/>                           
                            </div>
                            <div class="form-group col-md-12">  
                                <label>備註</label> 
                                <input type="text" class="form-control" name="return_comment" id="return_comment"/>                           
                            </div>
                            <input type="hidden" name="em_f_no" id="em_f_no">
                            <input type="hidden" name="em_f_project_id" id="em_f_project_id" value="<?php echo $fpid; ?>">
                        </div>                                        
                        </form>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                        <button  class="btn btn-default" onclick="addReturnFund()">儲存</button>
                    </div>
                    </div>
                </div>
                </div>   
                <!-- emptyFineModal       END-->
            <!-- chooseModal -->
            <div class="modal fade" id="chooseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog  modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">選取待轉正之帳務資料</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                    <div class="col-md-12" style="padding: 10px 5px;">
                            <form id="filter2Form" style="border: 1px solid #eee;border-radius: 5px;padding: 10px 25px 0px 25px;">
                            <span class="text-muted" style="
                                position: absolute;
                                right: 50px;
                                top: 0px;
                                background-color: #fff;
                                padding: 0px 20px;
                            ">搜尋</span>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    類型：<select type="text" name="type" id="f_type_2" class="form-control" style="width:12em;">
                                        <option value='id'>身分證字號</option>
                                        <option value='name'>姓名</option>
                                        <option value='virtualcode'>虛擬帳號</option>
                                    </select>                                        
                                    <!-- <input type="submit" value="查询"/> -->
                                    
                                    <!--選擇日期區間:<label><input  id="dater" class="form-control" type="text"  name="datepicker"/></label>-->                                        
                                </div>
                                <div class="form-group col-md-5">
                                    關鍵字：<input type="text" class="form-control" name="condition2" style="width:12em;" id="f_keyword">
                                </div>
                                <div class="col-md-2">
                                    <div class="text-right" style="margin-top: 15px;">
                                        <input type="button" id="search2BT" value="查詢"  class="btn btn-default"/>
                                    </div>
                                </div>
                            </div>                                        
                            </form>
                        </div>
                    </div>
                        
                      <div class="table-responsive" style="height:350px">
                            <table id="chooseTable" class="table table-bordered" style="width:1600px;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>類型</th>
                                        <th>案件編號</th>
                                        <th>受處分人姓名</th>
                                        <th>身份證編號</th>
                                        <th>
                                        罰鍰(萬)/怠金(元)                                        
                                        </th>
                                        <th>完納日期</th>
                                        <th>完納金額</th>
                                        <th>繳款日期</th>
                                        <th>繳款金額</th>
                                        <th>分期Log</th>
                                        <th>移送Log</th>
                                        <th>憑證Log</th>
                                        <th>撤銷註銷Log</th>
                                        <th>執行命令Log</th>
                                        <th>備註</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                      </div> 
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                    <button type="button" id="addprojectBT" class="btn btn-info" data-limit="" data-pjid="<?php echo $fpid; ?>" onclick="addProjectEvent($(this))">加入</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           table = $('#table1').DataTable({
              'columnDefs': [
                    {
                        "targets": [ 19 ],
                        "visible": false,
                        // 'orderable': true,
                    }
                //  {
                //     'orderable': false,
                //     'targets': [9],
                //  },
                //  {
                //     'targets': [0],
                //     'checkboxes': {
                //        'selectRow': true
                //     }
                //  }
              ],
              'select': {
                 'style': 'muti'
              },
              'order': [[19, 'asc']],
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $(nRow).attr("data-id", aData[0]);
                $(nRow).attr("data-caseid", aData[2].replace('<span class="text-danger"> (註)</span>', ''));
                $("td:first", nRow).html(iDisplayIndex + 1); //自動序號
                return nRow;
              },
              "searching": false,
              dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text: '每頁顯示筆數' }
              ],
              lengthMenu: [
                  [ 10, 25, 50, -1 ],
                  [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
              ],
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                },
           });
           chooseTable = $('#chooseTable').DataTable({
            "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                },
           });
           $("#table1 tbody tr").each(function(index,element)
           {
             if($(this).children('td:eq(14)').text().indexOf('<?php echo $fcp_createdate; ?>') > -1)
             {
               $(this).addClass('bg-success');
             }

             if($(this).children('td:eq(6)').text().indexOf('<?php echo $fcp_createdate; ?>') > -1)
             {
               $(this).addClass('bg-success');
             }
           })

           $('#table1 tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');

                selectrows= $('#table1 tbody tr.selected').length
                if(selectrows == 0)
                {
                    $("#transfer").attr('disabled', true);
                    $("#mergesel").attr('disabled', true);
                    $("#disassemble").attr('disabled', true);
                }
                else if(selectrows == 1){
                    $("#transfer").attr('disabled', false);
                    $("#mergesel").attr('disabled', true);
                    $("#disassemble").attr('disabled', false);
                }
                else
                {
                    $("#transfer").attr('disabled', true);
                    $("#mergesel").attr('disabled', false);
                    $("#disassemble").attr('disabled', false);
                }

                if(selectrows == 1)
                {
                    $("#returnBT").attr('disabled', false);
                  $("#delFineBT").attr('disabled', false);
                }
                else
                {
                    $("#returnBT").attr('disabled', true);
                  $("#delFineBT").attr('disabled', true);
                }

            })
            $('#chooseTable tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
                
            } );
            $("#search2BT").click(function(){
                chooseEvent('2', $("#f_type_2").val(), $("#f_keyword").val());
            });
            $('#chooseModal').on('hidden.bs.modal', function (event) {
              $("#f_type_2").val('id')
              $("#f_keyword").val('')
              chooseTable.clear().draw();
            })
                // table.column(5).visible(false);
                // table.column(6).visible(false);
                // table.column(8).visible(false);
                // table.column(9).visible(false);
                // table.column(10).visible(false);
                // table.column(11).visible(false);
                // table.column(12).visible(false);
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                $("#sp_checkbox").submit();
            });
            $("#merge").click(function (){
                $("#s_status").val('merge');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#mergesel").click(function (){
                // var rows_selected = table.column(0).checkboxes.selected();
                // //alert(rows_selected);
                // jQuery.each( rows_selected, function( i, val ) {
                //     $('#select1').append('<option value="'+val.split('<br>')[0]+'"> '+val+' </option>');        
                // });
                $("#addprojectBT").attr('data-limit', 'M');
            });
            $("#transfer").click(function (event){
                // $("#s_status").val('transfer');
                //     //alert("Submitted");
                // $("#sp_checkbox").submit();
                event.preventDefault();
                $("#addprojectBT").attr('data-limit', '1');
            });
            $("#disassemble").click(function (){
                // $("#s_status").val('disassemble');
                //     //alert("Submitted");
                // $("#sp_checkbox").submit();
                $("#addprojectBT").attr('data-limit', '-1');
            });

            $("#saveBT").click(function(){
                saveEvent();
            });
            
            $("#returnBT").click(function(){
                loadReturnModalEvent();
            });
            $("#delFineBT").on('click', function(){
              var rows_selected = $.map(table.rows('.selected').nodes(), function (item) {
                    if($(item).hasClass('bg-success'))
                    {
                      swal({
                          title: "警告!",
                          text: "所選資料含有已核銷之內容，請先移除該選取",
                          icon: "warning"
                      });
                      return false;
                    }
                    else
                    {
                        return $(item).attr("data-id");
                    }
                });
               
                if(rows_selected.indexOf(false) > -1)
                  return false;

              if(rows_selected.length > 0)
              {
                delEvent(rows_selected.join(","));
              }
              else
              {
                swal({
                        title: "錯誤!",
                        text: "請選擇至少一筆要刪除的資料列!",
                        icon: "danger",
                    });
              }
              
            })
           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
                if($.isEmptyObject($('#s_cnum').val())){
                    alert('請先選擇案件');
                    e.preventDefault();
                }
                else{
                }
           });
    });
    function chooseEvent(condition, type, keyword)
    {
      
      pjid = '<?php echo $fpid?>';
      $('#chooseTable').css('width','1600px');
      chooseTable = $('#chooseTable').DataTable({	
                "destroy":true,	
                dom: 'Bfrtip',
                buttons: [ 
                    { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                    { extend: 'pageLength', text: '每頁顯示筆數' }
                ],
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                ],	
                "bProcessing": true, //顯示『資料載入中』							
                "sAjaxSource": `../../Acc_cert/filterAccAll?condition=${condition}&type=${type}&keyword=${keyword}`, // API
                "aoColumns": [
                    { "mData": "type" },
                    { "mData": "type",
                        "mRender":function(val,type,row){
                            if(val=='A'){
                                return `<span class="label label-danger">罰鍰</span>`;
                            }else{
                                return `<span class="label label-warning">怠金</span>`;
                            }
                        } 
                    },
                    { "mData": "f_caseid"},
                    { "mData": "f_username"},
                    { "mData": "f_userid"},
                    { "mData": "f_amount", "sWidth": '80px'},
                    { "mData": "f_donedate",
                        "mRender":function(val,type,row){
                            return (val == '0000-00-00')?'':tranfer2RCyear(val);
                        } 
                    },
                    { "mData": "f_doneamount"},
                    { "mData": "f_paydate",
                        "mRender":function(val,type,row){
                            return (val == '0000-00-00')?'':tranfer2RCyear(val);
                        } 
                    },
                    { "mData": "f_paymoney"},
                    { "mData": "f_paylog",
                        "mRender":function(val,type,row){
                          return ((val)?val.replace(/\r\n|\n|\r/g,"<br/>"):"");
                        } 
                    },
                    { "mData": "f_movelog",
						"mRender": function(val,type,row){
							if(val)
							{
								let calcmovelog = val.split(/\r\n|\r|\n/);
								if(calcmovelog.length > 1)
								{
									calcmovelog.sort(function(a, b) {
										var movedateA = a.split('_')[0] * 1; 
										var movedateB = b.split('_')[0] * 1; 
										if (movedateA < movedateB) {
											return 1;
										}
										if (movedateA > movedateB) {
											return -1;
										}

										// names must be equal
										return 0;
									});

									return calcmovelog.join('<br/>');
								}
								else
								{
									return val;
								}
								
							}
							else
							{
								return val;
							}
							
						}
					},
                    { "mData": "f_cardlog"},
                    { "mData": "f_dellog"},
                    { "mData": "f_execlog",
                        "mRender":function(val,type,row){
                          return ((val)?val.replace(/\r\n|\n|\r/g,"<br/>"):"");
                        } 
                    },
                    { "mData": "f_comment"}
                ],                
                "bAutoWidth": true,
                "ordering":false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    $(nRow).attr("data-id", aData.f_no);
                    $("td:first", nRow).html(iDisplayIndex + 1); //自動序號

                    let movedate = (aData.f_movelog)?tranfer2ADyear((aData.f_movelog.split("\n")[aData.f_movelog.split("\n").length -1]).split('_')[0]):'';
                    let execdate = (aData.f_execlog)?tranfer2ADyear((aData.f_execlog.split("\n")[aData.f_execlog.split("\n").length -1]).split('_')[0]):'';
                    let donedate = (aData.f_donedate !== '0000-00-00')?aData.f_donedate:'';

                    if(new Date(movedate).getTime() > new Date(donedate).getTime() )
                    {
                        $(nRow).addClass('bg-danger');
                    }
                    if(new Date(execdate).getTime() > new Date(donedate).getTime() )
                    {
                        $(nRow).addClass('bg-warning');
                    }
                    
                    if(aData.f_comment !== "")
                    {
                        $(nRow).addClass('bg-info');
                    }
                    if(aData.f_dellog)
                    {
                        if(!$(nRow).children('td:eq(2)').has( "span" ).length)
                        {
                            $(nRow).children('td:eq(2)').append('<span class="text-danger">(註)</span>')
                        }                        
                    }
                    return nRow;
                },
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                },  
                
            });
            
            
    }
    function tranfer2ADyear(date)
    {
        if(date.length == 6)
        {
            ad = (parseInt(date.substr(0, 2))) + 1911;
            return ad.toString() + '-' + date.substr(2, 2) + '-' + date.substr(4, 2);
        }
        else if(date.length == 7)
        {
            ad = (parseInt(date.substr(0, 3))) + 1911;
            return ad.toString() + '-' + date.substr(3, 2) + '-' + date.substr(5, 2);
        }
        else
        {
            return '';
        }
    }
    function computeEvent(obj)
    {
        datarow = obj.parent().parent().parent();
      t_pay_amount = parseInt(datarow.find("#t_payamount").val())
      paycost = parseInt(datarow.find("#paycost").val())
      returnamount = parseInt(datarow.find("#returnamount").val())
      balance = parseInt(datarow.find("#balance").text())

        if((t_pay_amount - paycost + returnamount) > balance)
        {
            swal({
                title: "注意!",
                text: "核銷金額不可大於未繳金額",
                icon: "warning",
            });
            datarow.find("#t_payamount").parent().addClass('has-error has-feedback').append(`<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>`)
            datarow.find("#payamount").val('0');
            return false;
        }
        else
        {
            datarow.find("#t_payamount").parent().removeClass('has-error has-feedback')
            datarow.find(".glyphicon-remove").remove()
            datarow.find("#payamount").val(t_pay_amount - paycost + returnamount);
        }
        
      
    }
    function addProjectEvent(obj)
    {
        limit = obj.attr("data-limit");
        ori_rows_tamount = 0;
        var ori_rows_selected = $.map(table.rows('.selected').nodes(), function (item) {
                return $(item).attr("data-id");
            }); 
        var rows_selected = $.map(chooseTable.rows('.selected').nodes(), function (item) {
                return $(item).attr("data-id");
            });  
        if(rows_selected.length <= 0)
        {
          swal({
                title: "注意!",
                text: "請選擇至少一筆要轉正的帳務資料！",
                icon: "warning",
            });
          
          return false;
        }
        else
        {
            if(limit == "1" && rows_selected.length > 1)
            {
                swal({
                    title: "注意!",
                    text: "單筆移轉限制選取一筆資料！",
                    icon: "warning",
                });

                return false;
            }
            else if(limit == "M" && rows_selected.length > ori_rows_selected.length)
            {
                swal({
                    title: "注意!",
                    text: "合併限制選取"+ori_rows_selected.length+"筆以下(含)資料數！",
                    icon: "warning",
                });

                return false;
            }
            else if(limit == "-1" && rows_selected.length < ori_rows_selected.length)
            {
                swal({
                    title: "注意!",
                    text: "分解限制選取"+ori_rows_selected.length+"筆以上(含)資料數！",
                    icon: "warning",
                });

                return false;
            }
        }   
        
        selectrows= table.rows('.selected').data().toArray();
        listEvent('#selectdata-wrap', selectrows, limit, event)
        transferrows= chooseTable.rows('.selected').data().toArray(); 
        listEvent('#transferdata-wrap', transferrows, limit, event)
    }
    function listEvent(content, datarows, type, event){
        event.preventDefault();
        $("#saveBT").attr('disabled', false);
        str = "";
        
        $(content).html('');
        switch (type) {
            case '1':
                $("#transfertype").html('單筆移轉');
                break;
            case '-1':
                $("#transfertype").html('分解');
                break;
            case 'M':
                $("#transfertype").html('合併');
                break;
            default:
                break;
        }        
        datarows.forEach(element => {
            // console.log(element);
            if(content != '#selectdata-wrap')
            {
                str = `<div class="row data-row" style="border-left:5px solid ${((element.type == "A")?"#d9534f":((element.type == "B")?"#f0ad4e":"#337ab7"))};">
                        <div class="col-md-6" >
                            <p>
                                <span class="text-muted">案件編號：${element.f_caseid}</span> 
                            </p>
                            <h4>
                                ${element.f_username} <small class="text-info">（${element.f_userid}）</small> 
                            </h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <p class="text-muted">
                                繳款日期：<span><?php echo $fcp_createdate; ?></span> 
                            </p>
                            <p>
                                罰鍰/怠金：<span class="text-info" id="amount">${((element.type == 'A')?(parseInt(element.f_amount)*10000) : element.f_amount)}</span> 
                                未繳金額：<span class="text-danger" id="balance">${((element.type == 'A')?(parseInt(element.f_amount)*10000)-element.f_doneamount-element.f_paymoney : element.f_amount-element.f_doneamount-element.f_paymoney)}</span> 
                            </p>
                        </div>
                        <!-- /.col-md-6 -->
                        <div class="col-md-12">
                            
                            <div class="form-group col-md-3">
                            <label>繳納金額</label>
                                <input type="number" value="0" class="form-control" name="t_payamount[]" id="t_payamount" onchange="computeEvent($(this))"/>
                                
                            </div>
                            <div class="form-group col-md-3">
                            <label>執行費</label>
                                <input type="number" value="0" class="form-control" name="paycost[]" id="paycost" onchange="computeEvent($(this))"/>
                            </div>
                            <div class="form-group col-md-3">
                                <label>退費 <small class="text-danger">(扣除請用負數)</small></label>
                                <input type="number" value="0" class="form-control" name="returnamount[]" id="returnamount" onchange="computeEvent($(this))"/>
                            
                            </div>
                            <div class="form-group col-md-3">
                                <label>核銷金額</label>
                                <input type="number" value="0" class="form-control" name="payamount[]" id="payamount" readonly/>
                            </div>
                            <input type="hidden" value="${element.f_no}"  class="form-control" name="f_no[]" id="f_no" />
                        </div>
                    </div>`;
                    $(content).append(str);
            }
            else
            {
                ori_rows_tamount = ori_rows_tamount + parseInt(element[7]) + parseInt(element[8]) + parseInt(element[10]) + parseInt(element[11]) + parseInt(element[12]);
                str = `<div class="row" style="border-left:5px solid ${((element[1].indexOf('罰鍰') > -1)?"#d9534f":((element[1].indexOf('怠金') > -1)?"#f0ad4e":"#337ab7"))};">
                        <div class="col-md-6" >
                            <p>
                                <span class="text-muted">案件編號：${element[2]}</span> 
                            </p>
                            <h4>
                                ${element[3]} <small class="text-info">（${element[4]}）</small> 
                            </h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <p class="text-muted">
                                繳款日期：<span><?php echo $fcp_createdate; ?></span> 
                            </p>
                            <p>
                                罰鍰/怠金：<span class="text-info">${element[5]}</span> 
                                未繳金額：<span class="text-danger">${element[5]-element[7]-element[8]}</span> 
                            </p>
                        </div>
                        <!-- /.col-md-6 -->
                        <div class="col-md-12">
                            <div class="form-group col-md-2">
                            <label>完納金額</label>
                                <p class="text-muted">${element[7]}</p>
                            </div>
                            <div class="form-group col-md-2">
                            <label>已繳金額</label>
                                <p class="text-muted">${element[8]}</p>
                            </div>
                            <div class="form-group col-md-2">
                            <label>執行費</label>
                                <p class="text-muted">${element[10]}</p>
                            </div>
                            <div class="form-group col-md-2">
                            <label>轉正金額</label>
                                <p class="text-muted">${element[11]}</p>
                            </div>
                            <div class="form-group col-md-2">
                            <label>退費金額</label>
                                <p class="text-muted">${element[12]}</p>
                            </div>
                        </div>
                    </div>`;
                    $(content).append(str);
            }
            
        });
        $('#chooseModal').modal('hide');
        // $("#transfer").click();
    }
    
    function saveEvent(){
        if($(".data-row").find('.has-error').length > 0)
        {
            return false;
        }
        rows_tamount = 0;
        var ori_rows_selected = $.map(table.rows('.selected').nodes(), function (item) {
                return $(item).attr("data-id");
            });
        var ori_rows_selected_caseid = $.map(table.rows('.selected').nodes(), function (item) {
                return $(item).attr("data-caseid");
            }); 
        var rows_selected = $.map(chooseTable.rows('.selected').nodes(), function (item) {
                
                return $(item).attr("data-id");
            }); 
        $(".data-row").each(function(el, index){
            paycost = $(this).find('input[name="paycost[]"]').val()
            returnamount = $(this).find('input[name="returnamount[]"]').val()
            payamount = $(this).find('input[name="payamount[]"]').val()

            rows_tamount = rows_tamount + parseInt(paycost) + parseInt(returnamount) + parseInt(payamount);
        })
        if(rows_tamount > ori_rows_tamount)
        {
            swal({
                title: "注意!",
                text: "轉正金額不可超過原已繳金額！",
                icon: "warning",
            });

            return false;
        }

        var formData = new FormData($("#turnsubForm")[0])

        formData.append('ori_rows_selected', ori_rows_selected.join(","))
        formData.append('ori_rows_selected_caseid', ori_rows_selected_caseid.join(","))
        formData.append('rows_selected', rows_selected.join(","))
        formData.append('fpid', '<?php echo $fpid; ?>') 
        formData.append('paydate', '<?php echo $fcp_createdate; ?>')
        formData.append('rows_tamount', rows_tamount)
        formData.append('max_sort', <?php echo $max_sort ?>);

        $.ajax({            
                    type: 'POST',
                    url: '../../Acc_cert/add_Fine_change_project_body',
                    data: formData,
                    // dataType: 'json',
                    processData : false, 
                    contentType: false,
                    cache: false,
                    error:function(){
                        console.log('error')
                    },
                    success: function(resp){
                        console.log('success');
                        if(resp !== 'error')
                            location.reload();
                        else
                            return false;
                    },
                    complete:function(resp){
                        // location.href = `../${resp.responseText}`;
                        console.log('complete')
                    }
                });
    }
    function delEvent(param){
    swal({
            title: "確定刪除?",
            text: "一經刪除還可加回本專案",
            icon: "warning",
            buttons: ["取消", "確定"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var formData = new FormData();
                formData.append('row_id', param);
                formData.append('fpid', '<?php echo $fpid; ?>')
                $.ajax({            
                    type: 'POST',
                    url: '../../Acc_cert/delete_Fine_project_body',
                    data: formData,
                    // dataType: 'json',
                    processData : false, 
                    contentType: false,
                    cache: false,
                    error:function(){
                        console.log('error')
                    },
                    success: function(resp){
                        console.log('success');
                        if(resp !== 'error')
                            location.reload();
                        else
                            return false;
                    },
                    complete:function(resp){
                        // location.href = `../${resp.responseText}`;
                        console.log('complete')
                    }
                });
            } else {
                return false;
            }
        });
   }
   function loadReturnModalEvent(){
       const selectdata = table.rows('.selected').data().toArray();
       $("#return_caseid").text(selectdata[0][2])
       $("#return_username").text(selectdata[0][3])
       $("#return_userid").text(selectdata[0][4])
       $("#return_amount").text(selectdata[0][5])
       $("#return_balance").text(selectdata[0][9])
       $("#em_f_no").val(selectdata[0][0])
   }

   function addReturnFund(){
        var formData = new FormData();
        formData.append('returnmoney', $("#returnmoney").val());
        formData.append('return_comment', $("#return_comment").val())
        formData.append('em_f_no', $("#em_f_no").val());
        formData.append('em_f_project_id', $("#em_f_project_id").val());
        formData.append('return_paydate', $("#return_paydate").val())
        $.ajax({            
            type: 'POST',
            url: '../../Acc_cert/add_returnfund_in_person',
            data: formData,
            // dataType: 'json',
            processData : false, 
            contentType: false,
            cache: false,
            error:function(){
                console.log('error')
            },
            success: function(resp){
                console.log('success');
                if(resp !== 'error')
                    location.reload();
                else
                    return false;
            },
            complete:function(resp){
                // location.href = `../${resp.responseText}`;
                console.log('complete')
            }
        });
   }
   /** 轉民國年 */
	function tranfer2RCyear(date)
	{
		if(!date)
			return false;
		date = date.split('-');
		date = (parseInt(date[0]) - 1911) + "-" + date[1] + "-" + date[2];
			return date;                
	}  
    </script>
    
