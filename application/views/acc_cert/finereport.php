        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p>報表查詢</p>
                    </blockquote>
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    搜尋作業
                                </div>       
                                <div class="panel-body">
                                    <form class="" id="searchForm" name="spreadsheet" >
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>請選擇時間起訖</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">起始時間</span>
                                                        <input type="text" class="form-control" id="startdate" name="startdate" aria-describedby="basic-addon3">
                                                        <span class="input-group-addon">結束時間</span>
                                                        <input type="text" class="form-control" id="enddate" name="enddate" aria-describedby="basic-addon3">
                                                    </div>                                           
                                                </div>  
                                                                                              
                                            </div>                                            
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#r1" aria-controls="r1" role="tab" data-toggle="tab">罰鍰怠金月報表</a></li>
                        <li role="presentation"><a href="#r2" aria-controls="r2" role="tab" data-toggle="tab">收入憑證月報表</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="r1" style="padding:5px 10px;">
                            
                            <h4 class="text-center">罰鍰怠金月報表
                            </h4>
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                <label>請選擇類型</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="types" id="types1" value="A" checked> 罰鍰
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="types" id="types2" value="B"> 怠金
                                    </label>
                                </div>
                                <input type="button" class="btn btn-primary" value="查詢" id="searchBT" onclick="reloadEvent()"/>
                            </div>
                             
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-info" style="margin-top:15px;height:100%;">
                                        <div class="panel-heading">
                                            報表結果
                                        </div>       
                                        <div id="loading" class="row hidden" style="
                                                position: absolute;
                                                width: 100%;
                                                height: 100%;
                                                background-color: rgba(0,0,0,0.3);
                                                z-index: 1030;
                                            ">
                                                <div class="loader" id="loader-1"></div>
                                                <p class="text-danger text-center" id="loadingTxt" style="background-color:#fff;"></p>
                                        </div>
                                        <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <button class="btn btn-default" id="r1_s1BT">下載(Excel)</button>
                                                <button class="btn btn-default" id="r1_s2BT">明細表</button>
                                            </div>
                                        </div>
                                        <hr>
                                        <iframe class="hidden" style="width:100%;height:600px" frameborder="0" name="reportframe" id="reportframe"></iframe>
                                        </div>
                                    </div>
                                    <!-- /.panel -->
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <div role="tabpanel" class="tab-pane" id="r2" style="padding:5px 10px;">
                            <h4 class="text-center">收入憑證月報表
                            <br/><input type="button" class="btn btn-primary" value="查詢" id="searchBT" onclick="reload2Event()"/></h4>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-info" style="margin-top:15px;height:100%;">
                                        <div class="panel-heading">
                                            報表結果
                                        </div>       
                                        <div id="loading2" class="row hidden" style="
                                                position: absolute;
                                                width: 100%;
                                                height: 100%;
                                                background-color: rgba(0,0,0,0.3);
                                                z-index: 1030;
                                            ">
                                                <div class="loader" id="loader-1"></div>
                                                <p class="text-danger text-center" id="loadingTxt" style="background-color:#fff;"></p>
                                        </div>
                                        <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                 
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <button class="btn btn-default" id="r2_s1BT">下載(Excel)</button>
                                            </div>
                                        </div>
                                        <hr>
                                        <iframe class="hidden" style="width:100%;height:600px" frameborder="0" name="reportframe2" id="reportframe2"></iframe>
                                        </div>
                                    </div>
                                    <!-- /.panel -->
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
            var latayesr = new Date().getFullYear()-1;
            var currentYear = new Date().getFullYear();
            var currentMonth = new Date().getMonth();
            var currentDay = new Date().getDay();
            var date = currentYear+'-'+currentMonth+'-'+currentDay;

            
            if(currentMonth == 0){
                currentYear = currentYear-1;
                date =currentYear +'-'+12+'-'+currentDay;
            }
            $("#dater").daterangepicker(
            {
            //startDate: date,  
            locale: {
                  format: 'YYYY-MM-DD'
                }
            } 
            );
            $("#startdate, #enddate").datepicker(
                {
                    todayHighlight: true,
                    format: "yyyy-mm-dd",
                    locale: {
                        format: 'YYYY-MM-DD'
                    }
                } 
            );
            now = new Date();
            firsthdate = now.getFullYear() + '-' + (((now.getMonth()+1) > 9)?(now.getMonth()+1):('0' + (now.getMonth()+1).toString())) + '-01';
            today = now.getFullYear() + '-' + (((now.getMonth()+1) > 9)?(now.getMonth()+1):('0' + (now.getMonth()+1).toString())) + '-' +(((now.getDate()) > 9)?(now.getDate()):('0' + (now.getDate()).toString()));
            $("#startdate").val(firsthdate);
            $("#enddate").val(today);
           var table = $('#table1').DataTable({
                 "searching": false,
                'columnDefs': [
                 {
                    'orderable': false,
                    'targets': [9],
                 },
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']],
                dom: 'Bfrtip',
                buttons: [
                     {
                extend: 'csv',
                text: 'CSV',
                bom : true}
                ]     
           });
                table.column(5).visible(false);
                table.column(6).visible(false);
                table.column(8).visible(false);
                table.column(9).visible(false);
                table.column(10).visible(false);
                table.column(11).visible(false);
                table.column(12).visible(false);
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#fpart").click(function (){
                $("#status").val('0');
                    //alert($("#status").val());
                $("#search").submit();
            });
            $("#fcomplete").click(function (){
                $("#status").val('1');
                    //alert("Submitted");
                $("#search").submit();
            });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert($("#status").val());
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
        
        $("#r1_s1BT").click(function(){
            download_r1_Event()
        })
        $("#r1_s2BT").click(function(){
            download_r1s2_Event()
        })
        $("#r2_s1BT").click(function(){
            download_r2s1_Event()
        })
        
    });
    function tranfer2ADyear(date)
    {
        if(date.length == 6)
        {
            ad = (parseInt(date.substr(0, 2))) + 1911;
            return ad.toString() + '-' + date.substr(2, 2) + '-' + date.substr(4, 2);
        }
        else if(date.length == 7)
        {
            ad = (parseInt(date.substr(0, 3))) + 1911;
            return ad.toString() + '-' + date.substr(3, 2) + '-' + date.substr(5, 2);
        }
        else
        {
            return '';
        }
    }
    function reloadEvent(){
        if(!$("#startdate").val())
            return false;
        if(!$("#enddate").val())
            return false;

        $('#loading').removeClass('hidden');
        var $iFrame=$("#reportframe");
        $iFrame.prop("src","../PDFcreate/finereport_print?param=" + $("#startdate").val() + "," + $("#enddate").val()+ "&rtype="+$("input[name=types]:checked").val());
        if (!/*@aijquery@*/0) {  // 瀏覽器為IE
            $iFrame[0].onload = function(){     
                $iFrame.removeClass('hidden');
                $('#loading').addClass('hidden');
            };  
        }else{  
            $iFrame[0].onreadystatechange = function(){
                if (iframe.readyState == "complete"){  
                    $iFrame.removeClass('hidden');
                    $('#loading').addClass('hidden');
                }  
            };  
        }
        
    }
    function reload2Event(){
        if(!$("#startdate").val())
            return false;
        if(!$("#enddate").val())
            return false;

        $('#loading2').removeClass('hidden');
        var $iFrame=$("#reportframe2");
        $iFrame.prop("src","../PDFcreate/finereport_print_receipt?param=" + $("#startdate").val() + "," + $("#enddate").val());
        if (!/*@aijquery@*/0) {  // 瀏覽器為IE
            $iFrame[0].onload = function(){     
                $iFrame.removeClass('hidden');
                $('#loading2').addClass('hidden');
            };  
        }else{  
            $iFrame[0].onreadystatechange = function(){
                if (iframe.readyState == "complete"){  
                    $iFrame.removeClass('hidden');
                    $('#loading2').addClass('hidden');
                }  
            };  
        }
        
    }
    function download_r1_Event()
    {
        location.href = "../PhpspreadsheetController/export_fineReport_r1?param=" + $("#startdate").val() + "," + $("#enddate").val()+ "&rtype="+$("input[name=types]:checked").val();
    }
    function download_r1s2_Event()
    {
        location.href = "../PhpspreadsheetController/export_fineReport_r1_s2?param=" + $("#startdate").val() + "," + $("#enddate").val()+ "&rtype="+$("input[name=types]:checked").val();
    }
    function download_r2s1_Event()
    {
        location.href = "../PhpspreadsheetController/export_fineReport_r2?param=" + $("#startdate").val() + "," + $("#enddate").val();
    }
    
    </script>
