        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
									<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                    <!-- <li><a href="//localhost/main2/acc_cert/listfp2_ed/<?php echo $fpid?>"><button id='yes' class="btn btn-default"style="padding:0px 0px;" >編輯</button></a></li>
                    <li><a><button id='yes' class="btn btn-default"style="padding:0px 0px;" data-toggle="modal" data-target="#exampleModal">輸入公文號</button></a></li>
                    <li><a href="//localhost/main2/acc_cert/listFineProject/"><button id='yes' class="btn btn-default"style="padding:0px 0px;" >回到專案</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <!-- <form action=<?php //echo base_url("Acc_cert/insertofficenum") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">輸入公文號</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>輸入公文號</label>
                                        <?php //echo form_input('fp_num',$fp_num, 'class="form-control"')?>                                                            
                                        <?php //echo form_hidden('fp_no',$fpid)?>                                                            
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button  class="btn btn-default" >輸入公文號</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                        </form>     -->
                        <div class="modal fade" id="emptyFineModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-primary">
                                  <h4 class="modal-title" id="exampleModalLabel">新增作廢收據</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <form id="addEmptyFineForm">
                                    <div class="row">
                                        <div class="form-group col-md-12">  
                                          <label>繳費日期</label> 
                                          <p><?php echo $fp_date; ?></p>
                                          <input type="hidden" class="form-control" name="em_f_paydate" id="em_f_paydate" value="<?php echo $fp_date; ?>"/>                           
                                        </div>
                                        <div class="form-group col-md-12">  
                                          <label>收據號碼</label> 
                                          <input type="text" class="form-control" name="em_f_receipt_no" id="em_f_receipt_no"/>                           
                                        </div>
                                        <div class="form-group col-md-12">  
                                          <label>收據面額</label> 
                                          <input type="number" class="form-control" name="em_f_paymoney" id="em_f_paymoney"/>                           
                                        </div>
                                        <input type="hidden" name="em_f_project_id" id="em_f_project_id" value="<?php echo $fpid; ?>">
                                    </div>                                        
                                  </form>
                                    
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                                  <button  class="btn btn-default" onclick="addInvalidreceiptEvent()">新增</button>
                                </div>
                              </div>
                            </div>
                          </div>   
                          <!-- emptyFineModal       END-->
                <div class="container-fluid"> 
                <blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p><span class="text-warning">【編輯】</span>金融支匯票專案，帳務專案：<mark><?php echo $fpid; ?></mark></p>
                    </blockquote>
                  <div class="row">
                        <div class="col-md-4 text-left">
                              <a href=<?php echo base_url("Acc_cert/listFineProject") ?> type="button" class="btn btn-default " >回上一頁</a>                                                          
                        </div>
                        <div class="col-md-4 text-center">
                          <h3><span class="label label-default"><?php echo $fp_paytype; ?></span></h3>
                        </div>
                        <?php if(!isset($fp_status)) {?>
                        <div class="col-md-4 text-right">
                              <a href="#"<?php //echo base_url("Acc_cert/listfp1_ed/".$fpid) ?> type="button" class="btn btn-warning" id='yes'>批次編輯</a>
                              <button type="button" class="btn btn-danger" id="delFineBT" disabled="true">刪除</button> 
                              <button type="button" class="btn btn-default" id="addNewFineBT" data-toggle="modal" data-target="#emptyFineModal">新增作廢收據</button> 
                              <!-- <button type="button" class="btn btn-success" id="addOldPjBT" data-toggle="modal" data-target="#exampleModal">新增公文號</button>                                                                     -->
                        </div>
                        <?php } ?>
                    </div>
                    <div class="row" >
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading list_view">
                                  <div class="row">
                                      <div class="col-sm-6">
                                        <span>列表清單</span> 
                                      </div>
                                      
                                      <!-- <input type="checkbox" name="list"  data-target ="1"  checked> 處分書編號
                                      <input type="checkbox" name="list" data-target ="2" checked> 人員編號
                                      <input type="checkbox" name="list" data-target ="3" checked> 姓名
                                      <input type="checkbox" name="list" data-target ="4" checked> 證號
                                      <input type="checkbox" name="list" data-target ="5" checked> 查獲時間
                                      <input type="checkbox" name="list" data-target ="6" checked> 查獲地點
                                      <input type="checkbox" name="list" data-target ="7" checked> 犯罪手法
                                      <input type="checkbox" name="list" data-target ="8" checked> 毒品號
                                      <input type="checkbox" name="list" data-target ="9" checked> 成分
                                      <input type="checkbox" name="list" data-target ="10" checked> 級數
                                      <input type="checkbox" name="list" data-target ="11" checked> 淨重
                                      <input type="checkbox" name="list" data-target ="12" checked> 純質淨重
                                      <input type="checkbox" name="list" data-target ="13" checked> 建議金額 -->
                                      <div class="col-sm-6 text-right">
                                        <h4 style="margin:0px;padding:0px;"><span class="label">今日繳款額：<?php echo $total; ?></span></h4>
                                      </div>
                                  </div>
                                </div>
                                <div class="panel-body">
                                    
                                    <div class="row">
                                    
                                      <div class="col-sm-12 text-center">
                                      <?php if(!isset($fp_status)) {?>
                                        <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default" id="sort_up" onclick="sortEvent('up')">
                                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                          </button>
                                          <button type="button" class="btn btn-default" id="sort_down" onclick="sortEvent('down')">
                                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                          </button>
                                        </div>
                                        <?php }?> 
                                      </div>
                                    </div>
                                    
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                           </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                      <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                              單獨編輯作業
                            </div>
                            <div class="panel-body">
                              <form id="editfrom">
                                <div class="row">
                                    <div class="form-group col-md-2">
                                      <label class="">案件編號</label>
                                      <strong class="text-primary col-md-12 text-right" id="ed_caseid"></strong>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label class="">受處分人姓名</label>
                                      <strong class="text-primary col-md-12 text-right" id="ed_username"></strong>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label class="">身份證編號</label>
                                      <strong class="text-primary col-md-12 text-right" id="ed_userid"></strong>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label class="">罰鍰/怠金</label>
                                      <strong class="text-primary col-md-12 text-right" id="ed_amount"></strong>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label class="">未繳金額</label>
                                      <strong class="text-dnager col-md-12 text-right" id="ed_balance"></strong>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label class="">繳款日期</label>
                                      <strong class="text-primary col-md-12 text-right" id="ed_today"></strong>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-2">
                                      <label>繳納方式</label>
                                      <select name="payway" id="payway"  class="form-control" onchange="changeWayEvent($(this))" <?php echo (!isset($fp_status)?'':'readonly'); ?>>
                                        <option value="支票">支票</option>
                                        <option value="匯票">匯票</option>
                                      </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                    <label>繳納金額 <span class="text-danger">*</span></label>
                                      <input type="number" value="0" class="form-control" name="t_payamount" id="t_payamount" onchange="computeEvent()" <?php echo (!isset($fp_status)?'':'readonly'); ?>/>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label><strong>(-)</strong> 執行費</label>
                                      <input type="number" value="0" class="form-control" name="paycost" id="paycost" onchange="computeEvent()" <?php echo (!isset($fp_status)?'':'readonly'); ?>/>
                                    </div>
                                    <div class="form-group col-md-2">
                                    <?php if(!isset($fp_status)) {?>
                                      <label><a href="#" id="turnaroundlabel" class="text-reset" data-toggle="modal" data-target="#chooseModal" data-whatever=""><strong>(-)</strong> 轉正</a><!--<small class="text-danger">(扣除請用負數)</small>--></label>
                                      <?php }else{ ?>
                                        <label><strong>(-)</strong> 轉正 <!--<small class="text-danger">(扣除請用負數)</small>--></label>
                                      <?php }?>
                                      <input type="number" value="0" class="form-control" name="turnaround" id="turnaround" onchange="computeEvent()" <?php echo (!isset($fp_status)?'':'readonly'); ?>/>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label><strong>(-)</strong> 退費 <!--<small class="text-danger">(扣除請用負數)</small>--></label>
                                      <input type="number" value="0" class="form-control" name="returnamount" id="returnamount" onchange="computeEvent()" <?php echo (!isset($fp_status)?'':'readonly'); ?>/>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label><strong>(=)</strong> 核銷金額</label>
                                      <input type="number" value="0" class="form-control" name="payamount" id="payamount" readonly/>
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-2">
                                      <label>來源</label>
                                      <select name="source" id="source"  class="form-control" onchange="changeUnitEvent($(this))" <?php echo (!isset($fp_status)?'':'readonly'); ?>>
                                        <option value="義務人">義務人</option>
                                        <option value="銀行">銀行</option>
                                        <option value="機關">機關</option>
                                        <option value="其他">其他</option>
                                      </select>
                                    </div>
                                    <div class="form-group col-md-2 hidden" id="source_wrap">
                                      <label>常用行庫/分署</label>
                                      <select name="source_unit" id="source_unit"  class="form-control" <?php echo (!isset($fp_status)?'':'readonly'); ?>>
                                      </select>
                                      <input type="text"  class="form-control" name="source_other" id="source_other" placeholder="分行名稱/公司名" value="" <?php echo (!isset($fp_status)?'':'readonly'); ?>/>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label>公文文號</label>
                                      <input class="form-control" name="office_no" id="office_no" <?php echo (!isset($fp_status)?'':'readonly'); ?>/>
                                    </div>
                                    <div class="form-group col-md-2" id="ticket_wrap">
                                      <label>支匯票號碼</label>
                                      <input class="form-control" name="ticket_no" id="ticket_no" <?php echo (!isset($fp_status)?'':'readonly'); ?>/>
                                    </div>
                                    <div class="form-group col-md-2" id="receipt_wrap">
                                      <label>收據號碼</label>
                                      <input class="form-control" name="receipt_no" id="receipt_no" <?php echo (!isset($fp_status)?'':'readonly'); ?>/>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label>繳納狀態</label>
                                      <select name="paystatus" id="paystatus"  class="form-control" <?php echo (!isset($fp_status)?'':'readonly'); ?>>
                                          <option value="分期">分期</option>
                                          <option value="完納">完納</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="row">
																		<div class="form-group col-md-2">
                                      <label>案件狀態</label>
                                      <select name="paycstatus" id="paycstatus"  class="form-control" <?php echo (!isset($fp_status)?'':'readonly'); ?>>
																					<option value="">無</option>
                                          <option value="移送" selected>移送</option>
                                        	<option value="發憑證">發憑證</option>
                                          <option value="再移送">再移送</option>
                                      </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                      <label>備註</label>
                                      <input class="form-control" name="paycomment" id="paycomment" <?php echo (!isset($fp_status)?'':'readonly'); ?>/>
                                    </div>
                                </div>
                                <?php if(!isset($fp_status)) {?>
                                <div class="row text-right">
                                  <div class="col-md-12">
                                    <input type="button" class="btn btn-warning" id="saveBT" data-id="" value="儲存" />
                                  </div>                                      
                                </div>
                                <?php } ?>
                              </form>
                            </div>
                        </div>
                        <!-- /.panel -->    
                      </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
                <!-- chooseModal -->
                <div class="modal fade" id="chooseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog  modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">選取待轉正之專案</h4>
                      </div>
                      <div class="modal-body">
                          <div class="table-responsive" style="height:350px">
                                <table id="chooseTable" class="table table-bordered" style="width:1600px;">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>類型</th>
                                            <th>案件編號</th>
                                            <th>受處分人姓名</th>
                                            <th>身份證編號</th>
                                            <th>
                                            罰鍰(萬)/怠金(元)                                        
                                            </th>
                                            <th>完納日期</th>
                                            <th>完納金額</th>
                                            <th>繳款日期</th>
                                            <th>繳款金額</th>
                                            <th>分期Log</th>
                                            <th>移送Log</th>
                                            <th>憑證Log</th>
                                            <th>撤銷註銷Log</th>
                                            <th>執行命令Log</th>
                                            <th>備註</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                          </div> 
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                        <button type="button" class="btn btn-info" data-pjid="<?php echo $fpid; ?>" onclick="addProjectEvent($(this))">加入</button>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           table = $('#table1').DataTable({
              'columnDefs': [
                 {
                      "targets": [ 21 ],
                        "visible": false
                 },
								 {
                      "targets": [ 1 ],
                        "visible": false
                 },
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              // 'select': {
              //    'style': 'multi'
              // },
              'order': [[21, 'asc']],
              "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $(nRow).attr("data-id", aData[1]);

								if(iDisplayIndex >= 0)
								{
										if(aData[15].indexOf('<?php echo $fp_date; ?>') > -1 && aData[15].indexOf('<?php echo $fp_date; ?>已繳0元') == -1 && aData[15].indexOf('無分期繳款記錄') == -1)
										{
											$(nRow).addClass('bg-success');
										}
											
										if(aData[7].indexOf('<?php echo $fp_date; ?>') > -1)
										{
											$(nRow).addClass('bg-success');
										}
										
										if(aData[12] == 0 && aData[6] == 0)
										{
											$(nRow).removeClass('bg-success');
										}

										if(aData[16])
										{
											let calcmovelog = aData[16].split(/\r\n|\r|\n/);
											if(calcmovelog.length > 1)
											{
												calcmovelog.sort(function(a, b) {
													var movedateA = a.split('_')[0] * 1; 
													var movedateB = b.split('_')[0] * 1; 
													if (movedateA < movedateB) {
														return 1;
													}
													if (movedateA > movedateB) {
														return -1;
													}

													// names must be equal
													return 0;
												});

												aData[16] = calcmovelog.join('<br/>');
											}
											else
											{
												aData[16] = aData[16];
											}
											
										}
										else
										{
											aData[16] = aData[16];
										}
								}
                return nRow;
              },
              "searching": false,
              dom: 'Bfrtip',
              buttons: [ 
                  { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                  { extend: 'pageLength', text:'顯示全部'/*text: '每頁顯示筆數'*/ }
              ],
              lengthMenu: [
                  [ -1, 10, 25, 50 ],
                  [ '顯示全部', '10 筆', '25 筆', '50 筆'  ]
              ],
							"pageLength": -1,
              "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    select: {
                      rows: "選取 %d 列"
                    },
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                },
           });
           $('#chooseTable').css('width','1600px');
           $('#chooseTable tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
            } );
            // $("#table1 tbody input.dt-checkboxes").click(function(){
            //   if($("#table1 tbody input.dt-checkboxes:checked").length > 0)
            //     {
            //       $("#delFineBT").attr('disabled', false);
            //     }
            //     else
            //     {
            //       $("#delFineBT").attr('disabled', true);
            //     }
            // })
          //  $("#table1 tbody tr").each(function(index,element)
          //  {
          //     now = new Date();
          //     if($(this).children('td:eq(14)').text().indexOf('<?php //echo $fp_date; ?>') > -1 && $(this).children('td:eq(14)').text().indexOf('<?php //echo $fp_date; ?>已繳0元') == -1 && $(this).children('td:eq(14)').text().indexOf('無分期繳款記錄') == -1)
          //     {
          //      $(this).addClass('bg-success');
          //    }

          //    if($(this).children('td:eq(6)').text().indexOf('<?php //echo $fp_date; ?>') > -1)
          //    {
          //      $(this).addClass('bg-success');
          //    }

					// 	 if($(this).children('td:eq(11)').text() == '0' && $(this).children('td:eq(5)').text() == '0')
					// 	 {
					// 			$(this).removeClass('bg-success');
					// 	 }
          //  })
           $('#table1 tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');

                    $("#ed_caseid").text('');
                    $("#ed_username").text('');
                    $("#turnaroundlabel").attr('data-whatever', '');
                    $("#ed_userid").text('');
                    $("#ed_amount").text(''); 
                    $("#ed_balance").text('');                    
                    $("#ed_today").text('');
                    $("#paystatus").val('分期');
										$("#paycstatus").val('');
                    $("#saveBT").attr('data-id', '');
										$("#saveBT").attr('data-ser', '');
                    $("#delFineBT").attr('disabled', true);

                    $("#office_no").val('');
                    $("#ticket_no").val('');
                    $("#receipt_no").val('');
                    $("#editfrom")[0].reset();
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');

                    now = new Date()
                    selectrows= table.rows('.selected').data().toArray();      
                    
                    $("#ed_caseid").html((($.trim(selectrows[0][3]) != '&nbsp;')?selectrows[0][3]:''));
                    $("#ed_username").html(selectrows[0][4]);
                    $("#turnaroundlabel").attr('data-whatever', selectrows[0][4]);
                    $("#ed_userid").text((($.trim(selectrows[0][5]) != '&nbsp;')?selectrows[0][5]:''));
                    $("#ed_amount").text(selectrows[0][6]);   
                    $("#ed_balance").text(selectrows[0][10]);                 
                    $("#ed_today").text("<?php echo $fp_date; ?>");
                    $("#paystatus").val('分期');
										$("#paycstatus").val((($.trim(selectrows[0][16]) != '&nbsp;')?'移送':'無'));
                    $("#saveBT").attr('data-id', selectrows[0][0]);
										$("#saveBT").attr('data-ser', selectrows[0][1]);
                    $("#delFineBT").attr('disabled', false);

                    loadEvent(selectrows[0][0],selectrows[0][1], "<?php echo $fpid; ?>");
                }
            } );
            $('#chooseModal').on('shown.bs.modal', function (event) {
              chooseEvent();
            })
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

                  $("#yes").click(function (){
                      // $("#s_status").val('1');
                          //alert("Submitted");
                      // $("#sp_checkbox").submit();
                      var rows_selected = $.map($("#table1 tbody input.dt-checkboxes"), function (item) {
                        if($(item).prop('checked'))
                          return $(item).parent().parent().attr("data-id");
                      });
                      if(rows_selected.length > 0)
                      {
                        location.href = `../../Acc_cert/listfp2_ed/<?php echo $fpid?>?param=${rows_selected.join(",")}`;
                      }
                      else
                      {
                        return false;
                      }
                      
                      
                  });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
           $("#saveBT").click(function(){
								if($('#t_payamount').val())
								{
									// if(parseInt($('#t_payamount').val()) == 0)
									// {
									// 	swal({
                  //         title: "警告!",
                  //         text: "繳納金額不可為『零』",
                  //         icon: "warning"
                  //     });
									// 	return false;
									// }
									// else
									// {
										saveEvent($(this));
									// }
									
								}
								else
								{
									swal({
                          title: "警告!",
                          text: "繳納金額為必填欄位",
                          icon: "warning"
                      });
									return false;
								}
            });
            $("#delFineBT").on('click', function(){
              var rows_selected = $.map(table.rows('.selected').nodes(), function (item) {
                  if($(item).hasClass('bg-success'))
                    {
                      swal({
                          title: "警告!",
                          text: "所選資料含有已核銷之內容，請先移除該選取",
                          icon: "warning"
                      });
                      return false;
                    }
                    else
                    {
                      return $(item).attr("data-id");
                    }
                    
                });
               
                if(rows_selected.indexOf(false) > -1)
                  return false;

              if(rows_selected.length > 0)
              {
                delEvent(rows_selected.join(","));
              }
              else
              {
                swal({
                        title: "錯誤!",
                        text: "請選擇至少一筆要刪除的資料列!",
                        icon: "danger",
                    });
              }
              
            })
						$("#editfrom input[type='number']").bind('change', function(){
							if(!$(this).val())
								$(this).val(0);
						});
    });
    function saveEvent(obj)
    {
      // console.log('123')
      // return false;
      //obj.val('儲存中...').attr('disabled', true);
      var formData = new FormData();
      formData.append('f_no', obj.attr('data-id'));
			formData.append('fpart_num', obj.attr('data-ser'));
      formData.append('f_paymoney', $("#payamount").val());
      formData.append('f_payway', $("#payway").val());
      formData.append('f_paycost', $("#paycost").val());
      formData.append('f_turnaround', $("#turnaround").val());
      formData.append('f_returnamount', $("#returnamount").val());
      formData.append('f_paycomment', $("#paycomment").val());
      formData.append('t_payamount', $("#t_payamount").val());
      formData.append('f_paydate', $("#ed_today").text());
      formData.append('f_balance', $("#ed_balance").text());
      formData.append('f_amount', $("#ed_amount").text());
      formData.append('f_status', $("#paystatus").val());
			formData.append('f_cstatus', $('#paycstatus').val());
      formData.append('source', $("#source").val());
      formData.append('source_unit', $("#source_unit").val());
      formData.append('source_other', $("#source_other").val());
      formData.append('f_office_num', $("#office_no").val());
      formData.append('f_ticket_no', $("#ticket_no").val());
      formData.append('f_receipt_no', $("#receipt_no").val());
      formData.append('project_id', '<?php echo $fpid?>');

      $.ajax({            
            type: 'POST',
            url: '../../Acc_cert/add_lisfp2_paypart',
            data: formData,
            // dataType: 'json',
            processData : false, 
            contentType: false,
            cache: false,
            error:function(){
              console.log('error')
            },
            success: function(resp){
                console.log('success')
                if(resp == 'ok')
                {
                  location.reload();
                }
                
                
            },
            complete:function(resp){
            }
        });
    }
    function changeUnitEvent(obj)
    {
       source = obj.val()
       source_uni = {}
       $('#source_unit').html('');
       if(source.indexOf('義務人') > -1)
       {
        $("#source_wrap").addClass('hidden')
       }
       else
       {
          $("#source_wrap").removeClass('hidden')
          $.ajax({            
            type: 'POST',
            url: '../../Acc_cert/getFine_useful_source?source='+source,
            // data: formData,
            dataType: 'json',
            processData : false, 
            contentType: false,
            cache: false,
            success: function(resp){
              source_useful = resp.data
              
              if(source_useful.length > 0)
              {
                $('#source_unit').append("<option disabled>常用項目</option>");
                $('#source_unit').append("<option value='null'>無</option>");
                for (let index = 0; index < source_useful.length; index++) {
                  $('#source_unit')
                        .append($('<option>', { value : source_useful[index]["source_uni"] })
                        .text(source_useful[index]["source_uni"]));
                  
                }
              }
            },
            complete:function(resp){
              if(source.indexOf('銀行') > -1)
              {
                source_uni = { 
                "臺灣銀行": "臺灣銀行",
                "中華郵政股份有限公司":"中華郵政股份有限公司",
                "中國信託商業銀行股份有限公司":"中國信託商業銀行股份有限公司",
                "國泰世華商業銀行存匯作業管理部":"國泰世華商業銀行存匯作業管理部",
                "合作金庫商業銀行":"合作金庫商業銀行",
                "玉山銀行個金集中部":"玉山銀行個金集中部",
                "華南商業銀行股份有限公司":"華南商業銀行股份有限公司",
                "台北富邦商業銀行股份有限公司個金作業服務部":"台北富邦商業銀行股份有限公司個金作業服務部",
                "臺灣銀行股份有限公司":"臺灣銀行股份有限公司",
                "聯邦商業銀行":"聯邦商業銀行",
                "台新國際商業銀行股份有限公司":"台新國際商業銀行股份有限公司",
                "台中商業銀行總行":"台中商業銀行總行",
                "臺灣土地銀行股份有限公司":"臺灣土地銀行股份有限公司",
                "臺灣中小企業銀行":"臺灣中小企業銀行",
                "臺中商業銀行總行":"臺中商業銀行總行",
                "臺灣新光商業銀行股份有限公司集中作業部":"臺灣新光商業銀行股份有限公司集中作業部",
                "兆豐國際商業銀行股份有限公司":"兆豐國際商業銀行股份有限公司",
                "日盛國際商業銀行股份有限公司作業處":"日盛國際商業銀行股份有限公司作業處",
                "滙豐(台灣)商業銀行股份有限公司":"滙豐(台灣)商業銀行股份有限公司",
                "第一商業銀行總行":"第一商業銀行總行",
                "渣打國際商業銀行股份有限公司":"渣打國際商業銀行股份有限公司",
                "元大商業銀行股份有限公司":"元大商業銀行股份有限公司",
                "永豐商業銀行作業處":"永豐商業銀行作業處",
                "華泰商業銀行股份有限公司":"華泰商業銀行股份有限公司",
                "彰化商業銀行有限公司作業處":"彰化商業銀行有限公司作業處",
                "上海商業儲蓄銀行台北票據匯款理中心":"上海商業儲蓄銀行台北票據匯款理中心",
                "安泰商業銀行":"安泰商業銀行",
                "遠東國際商業銀行":"遠東國際商業銀行",
                "星展(臺灣)商業銀行":"星展(臺灣)商業銀行",
                "板信商業銀行作業服務部":"板信商業銀行作業服務部",
                "陽信商業銀行作業中心":"陽信商業銀行作業中心",
                "花旗(台灣)商業銀行股份有限公司":"花旗(台灣)商業銀行股份有限公司"
                };
                $('#source_unit').append("<option disabled>選擇其他</option>");
                $.each(source_uni, function(key, value) {
                    $('#source_unit')
                          .append($('<option>', { value : key })
                          .text(value));
                });
              }        
              else if(source.indexOf('機關') > -1)
              {
                source_uni1 = { 
                "法務部行政執行署臺北分署":"法務部行政執行署臺北分署",
                "法務部行政執行署新北分署":"法務部行政執行署新北分署",
                "法務部行政執行署桃園分署":"法務部行政執行署桃園分署",
                "法務部行政執行署新竹分署":"法務部行政執行署新竹分署",
                "法務部行政執行署臺中分署":"法務部行政執行署臺中分署",
                "法務部行政執行署彰化分署":"法務部行政執行署彰化分署",
                "法務部行政執行署嘉義分署":"法務部行政執行署嘉義分署",
                "法務部行政執行署臺南分署":"法務部行政執行署臺南分署",
                "法務部行政執行署高雄分署":"法務部行政執行署高雄分署",
                "法務部行政執行署屏東分署":"法務部行政執行署屏東分署",
                "法務部行政執行署花蓮分署":"法務部行政執行署花蓮分署",
                "法務部行政執行署宜蘭分署":"法務部行政執行署宜蘭分署",
                "法務部行政執行署士林分署":"法務部行政執行署士林分署"
                };
                source_uni2 = { 
                  "法務部矯正署臺北監獄":"法務部矯正署臺北監獄",
                  "法務部矯正署桃園監獄":"法務部矯正署桃園監獄",
                  "法務部矯正署桃園女子監獄":"法務部矯正署桃園女子監獄",
                  "法務部矯正署新竹監獄":"法務部矯正署新竹監獄",
                  "法務部矯正署臺中監獄":"法務部矯正署臺中監獄",
                  "法務部矯正署臺中女子監獄":"法務部矯正署臺中女子監獄",
                  "法務部矯正署彰化監獄":"法務部矯正署彰化監獄",
                  "法務部矯正署雲林監獄":"法務部矯正署雲林監獄",
                  "法務部矯正署雲林第二監獄":"法務部矯正署雲林第二監獄",
                  "法務部矯正署嘉義監獄":"法務部矯正署嘉義監獄",
                  "法務部矯正署臺南監獄":"法務部矯正署臺南監獄",
                  "法務部矯正署臺南第二監獄":"法務部矯正署臺南第二監獄",
                  "法務部矯正署明德外役監獄":"法務部矯正署明德外役監獄",
                  "法務部矯正署高雄監獄":"法務部矯正署高雄監獄",
                  "法務部矯正署高雄第二監獄":"法務部矯正署高雄第二監獄",
                  "法務部矯正署高雄女子監獄":"法務部矯正署高雄女子監獄",
                  "法務部矯正署屏東監獄":"法務部矯正署屏東監獄",
                  "法務部矯正署臺東監獄":"法務部矯正署臺東監獄",
                  "法務部矯正署花蓮監獄":"法務部矯正署花蓮監獄",
                  "法務部矯正署自強外役監獄":"法務部矯正署自強外役監獄",
                  "法務部矯正署宜蘭監獄":"法務部矯正署宜蘭監獄",
                  "法務部矯正署基隆監獄":"法務部矯正署基隆監獄",
                  "法務部矯正署澎湖監獄":"法務部矯正署澎湖監獄",
                  "法務部矯正署綠島監獄":"法務部矯正署綠島監獄",
                  "法務部矯正署金門監獄":"法務部矯正署金門監獄",
                  "法務部矯正署八德外役監獄":"法務部矯正署八德外役監獄"

                };


                $('#source_unit').append("<option disabled>行政執行機關</option>");
                $.each(source_uni1, function(key, value) {
                    $('#source_unit')
                          .append($('<option>', { value : key })
                          .text(value));
                });
                $('#source_unit').append("<option disabled>矯正機關</option>");
                $.each(source_uni2, function(key, value) {
                    $('#source_unit')
                          .append($('<option>', { value : key })
                          .text(value));
                });
              }
              else
              {
                
                $('#source_unit').change(function(){
                  $("#source_other").val($(this).val());
                });
              }
              
            }
        });
       }
        
    }
    function changeWayEvent(obj)
    {
       payway = obj.val()
       if(payway.indexOf('支票') > -1)
       {
          $("#receipt_wrap").removeClass('hidden')
       }
       else
       {
          $("#receipt_wrap").addClass('hidden')
       }
    }
    function computeEvent()
    {
      t_pay_amount = (!$("#t_payamount").val())?0:parseInt($("#t_payamount").val())
      paycost = (!$("#paycost").val())?0:parseInt($("#paycost").val())
      turnaround = (!$("#turnaround").val())?0:parseInt($("#turnaround").val())
      returnamount = (!$("#returnamount").val())?0:parseInt($("#returnamount").val())

      $("#payamount").val(t_pay_amount - paycost - turnaround - returnamount);
    }
    function chooseEvent()
    {
      name = $("#turnaroundlabel").attr('data-whatever');
      pjid = '<?php echo $fpid?>';
      $('#chooseTable').css('width','1600px');
      chooseTable = $('#chooseTable').DataTable({	
                "destroy":true,	
                dom: 'Bfrtip',
                buttons: [ 
                    { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                    { extend: 'pageLength', text: '每頁顯示筆數' }
                ],
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                ],	
                "bProcessing": true, //顯示『資料載入中』							
                "sAjaxSource": "../../Acc_cert/get_fine_in_person?name=" + name + "&pjid=" + pjid, // API
                "aoColumns": [
                    { "mData": "type" },
                    { "mData": "type",
                        "mRender":function(val,type,row){
                            if(val=='A'){
                                return `<span class="label label-danger">罰鍰</span>`;
                            }else if(val=='B'){
                                return `<span class="label label-warning">怠金</span>`;
                            }else{
                              return '';
                            }
                        } 
                    },
                    { "mData": "f_caseid"},
                    { "mData": "f_username"},
                    { "mData": "f_userid"},
                    { "mData": "f_amount", "sWidth": '80px'},
                    { "mData": "f_donedate",
                        "mRender":function(val,type,row){
                            return (val == '0000-00-00')?'':tranfer2RCyear(val);
                        } 
                    },
                    { "mData": "f_doneamount"},
                    { "mData": "f_paydate",
                        "mRender":function(val,type,row){
                            return (val == '0000-00-00')?'':tranfer2RCyear(val);
                        } 
                    },
                    { "mData": "f_paymoney"},
                    { "mData": "f_paylog",
                        "mRender":function(val,type,row){
                          return ((val)?val.replace(/\r\n|\n|\r/g,"<br/>"):"");
                        } 
                    },
                    { "mData": "f_movelog",
											"mRender": function(val,type,row){
												if(val)
												{
													let calcmovelog = val.split(/\r\n|\r|\n/);
													if(calcmovelog.length > 1)
													{
														calcmovelog.sort(function(a, b) {
															var movedateA = a.split('_')[0] * 1; 
															var movedateB = b.split('_')[0] * 1; 
															if (movedateA < movedateB) {
																return 1;
															}
															if (movedateA > movedateB) {
																return -1;
															}

															// names must be equal
															return 0;
														});

														return calcmovelog.join('<br/>');
													}
													else
													{
														return val;
													}
													
												}
												else
												{
													return val;
												}
												
											}
										},
                    { "mData": "f_cardlog"},
                    { "mData": "f_dellog"},
                    { "mData": "f_execlog",
                        "mRender":function(val,type,row){
                          return ((val)?val.replace(/\r\n|\n|\r/g,"<br/>"):"");
                        } 
                    },
                    { "mData": "f_comment"}
                ],                
                "bAutoWidth": true,
                "ordering":false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    $(nRow).attr("data-id", aData.f_no);
                    $("td:first", nRow).html(iDisplayIndex + 1); //自動序號

                    let movedate = (aData.f_movelog)?tranfer2ADyear((aData.f_movelog.split("\n")[aData.f_movelog.split("\n").length -1]).split('_')[0]):'';
                    let execdate = (aData.f_execlog)?tranfer2ADyear((aData.f_execlog.split("\n")[aData.f_execlog.split("\n").length -1]).split('_')[0]):'';
                    let donedate = (aData.f_donedate !== '0000-00-00')?aData.f_donedate:'';

                    if(new Date(movedate).getTime() > new Date(donedate).getTime() )
                    {
                        $(nRow).addClass('bg-danger');
                    }
                    if(new Date(execdate).getTime() > new Date(donedate).getTime() )
                    {
                        $(nRow).addClass('bg-warning');
                    }
                    
                    if(aData.f_comment !== "")
                    {
                        $(nRow).addClass('bg-info');
                    }
                    if(aData.f_dellog)
                    {
                        if(!$(nRow).children('td:eq(2)').has( "span" ).length)
                        {
                            $(nRow).children('td:eq(2)').append('<span class="text-danger">(註)</span>')
                        }                        
                    }
                    return nRow;
                },
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                },  
                
            });
            
            
    }
    function tranfer2ADyear(date)
    {
        if(date.length == 6)
        {
            ad = (parseInt(date.substr(0, 2))) + 1911;
            return ad.toString() + '-' + date.substr(2, 2) + '-' + date.substr(4, 2);
        }
        else if(date.length == 7)
        {
            ad = (parseInt(date.substr(0, 3))) + 1911;
            return ad.toString() + '-' + date.substr(3, 2) + '-' + date.substr(5, 2);
        }
        else
        {
            return '';
        }
    }
    function addProjectEvent(obj)
    {
      if(parseInt($("#t_payamount").val()) <= 0)
        {
          alert("請填寫繳納金額，不可為零！")
          return false;
        }
        var rows_selected = $.map(chooseTable.rows('.selected').nodes(), function (item) {
                return $(item).attr("data-id");
            });  
        if(rows_selected.length <= 0)
        {
          alert("請選擇至少一筆要轉正的帳務資料！")
          return false;
        }   
        
        var amountAry = $.map(chooseTable.rows('.selected').nodes(), function (item) {
                return {'type':(($(item).children('td:eq(2)').text().indexOf('A') > -1)?'怠金':'罰鍰'),'amount':(($(item).children('td:eq(2)').text().indexOf('A') > -1)?$(item).children('td:eq(5)').text():(parseInt($(item).children('td:eq(5)').text())*10000)), 'doneamount':$(item).children('td:eq(7)').text(), 'payamount':$(item).children('td:eq(9)').text()};
            }); 

        project_id = obj.attr("data-pjid");
        var formData = new FormData();
        formData.append('project_id', project_id);
        formData.append('s_cnum', rows_selected.join(","));
        formData.append('caseid',$("#ed_caseid").text());
        formData.append('t_payamount', (parseInt($("#t_payamount").val()) - parseInt($("#paycost").val()) - parseInt($("#ed_balance").text())));
        formData.append('amountAry',  JSON.stringify(amountAry));
        formData.append('paydate', $("#ed_today").text());
        formData.append('pj_type', '<?php echo $fp_type; ?>');

        $.ajax({            
            type: 'POST',
            url: '../../Acc_cert/addAcc_Project_by_person',
            data: formData,
            // dataType: 'json',
            processData : false, 
            contentType: false,
            cache: false,
            error:function(){
                console.log('error')
            },
            success: function(resp){
                if(resp == 'ok')
                {
                  var formData = new FormData();
                  formData.append('f_no', $("#saveBT").attr('data-id'));
									formData.append('fpart_num', obj.attr('data-ser'));
                  formData.append('f_paymoney', ((parseInt($("#t_payamount").val())-parseInt($("#paycost").val())-parseInt($("#ed_balance").text()) - parseInt($("#returnamount").val())) >= 0)?parseInt($("#ed_balance").text()):$("#payamount").val());
                  formData.append('f_payway', $("#payway").val());
                  formData.append('f_paycost', $("#paycost").val());
                  formData.append('f_turnaround', $("#turnaround").val());
                  formData.append('f_returnamount', $("#returnamount").val());
                  formData.append('f_paycomment', $("#paycomment").val());
                  formData.append('t_payamount', $("#t_payamount").val());
                  formData.append('f_paydate', $("#ed_today").text());
                  formData.append('f_balance', $("#ed_balance").text());
                  formData.append('f_amount', $("#ed_amount").text());
                  formData.append('f_status', $("#paystatus").val());
									formData.append('f_cstatus', $('#paycstatus').val());
                  formData.append('source', $("#source").val());
                  formData.append('source_unit', $("#source_unit").val());
                  formData.append('source_other', $("#source_other").val());
                  formData.append('project_id', '<?php echo $fpid?>');

                  $.ajax({            
                        type: 'POST',
                        url: '../../Acc_cert/add_lisfp2_paypart',
                        data: formData,
                        // dataType: 'json',
                        processData : false, 
                        contentType: false,
                        cache: false,
                        error:function(){
                          console.log('error')
                        },
                        success: function(resp){
                            console.log('success')
                            if(resp == 'ok')
                            {
                              location.reload();
                            }
                            
                            
                        },
                        complete:function(resp){
                        }
                    });
                }
                else
                {
                  console.log(resp)
                }
            },
            complete:function(resp){
              console.log('complete')
            }
        });
    }
    function addInvalidreceiptEvent()
    {
        $.ajax({            
            type: 'POST',
            url: '../../Acc_cert/add_invaild_receipt',
            data: new FormData($("#addEmptyFineForm")[0]),
            // dataType: 'json',
            processData : false, 
            contentType: false,
            cache: false,
            error:function(){
                console.log('error')
            },
            success: function(resp){
                if(resp == 'ok')
                {
                  location.reload();
                }
                else
                {
                  alert("核銷金額不可為零");
                }
            },
            complete:function(resp){
              console.log('complete')
            }
        });
    }
    function loadEvent(id, fpart_num, pjid){
        var formData = new FormData();
        formData.append('f_no', id);
				formData.append('fpart_num', fpart_num);
        formData.append('pjid', pjid);

        $.ajax({            
              type: 'POST',
              url: '../../Acc_cert/get_finepart_in_person',
              data: formData,
              dataType: 'json',
              processData : false, 
              contentType: false,
              cache: false,
              error:function(){
                console.log('error')
              },
              success: function(resp){
                respdata = resp.data;
                $("#payway").val(((respdata[0]['fpart_type'])?respdata[0]['fpart_type']:"支票"));
                if((respdata[0]['fpart_type']))
                {
                  if(respdata[0]['fpart_type'].indexOf('支票') > -1)
                  {
                      $("#receipt_wrap").removeClass('hidden')
                  }
                  else
                  {
                      $("#receipt_wrap").addClass('hidden')
                  }
                }
                $("#paycost").val(respdata[0]['f_fee']);
                $("#turnaround").val(respdata[0]['f_turnsub']);
                $("#returnamount").val(respdata[0]['f_refund']);
                $("#payamount").val(((respdata[0]['fpart_amount'])?respdata[0]['fpart_amount']:'0'));
                $("#paystatus").val(((respdata[0]['f_status'])?respdata[0]['f_status']:'分期'));
								$('#paycstatus').val(((respdata[0]['f_cstatus'])?respdata[0]['f_cstatus']:((respdata[0]['f_movelog'])?'移送':'')));
                $("#paycomment").val(((respdata[0]['fp_comment'])?respdata[0]['fp_comment']:''));
                $("#office_no").val(((respdata[0]['f_office_num'])?respdata[0]['f_office_num']:''));
                $("#ticket_no").val(((respdata[0]['f_ticket_no'])?respdata[0]['f_ticket_no']:''));
                $("#receipt_no").val(((respdata[0]['f_receipt_no'])?respdata[0]['f_receipt_no']:''));

                if(respdata[0]['f_source'])
                {
                  source = respdata[0]['f_source'].split('_')[0]
                  source_unit = respdata[0]['f_source'].split('_')[1]
                  source_other = respdata[0]['f_source'].split('_')[2]
                  
                  if(source.indexOf('義務人') > -1)
                  {
                    $("#source_wrap").addClass('hidden')
                  }
                  else
                  {
                    $("#source_wrap").removeClass('hidden')
                  }
                  $("#source").val(source)
                  changeUnitEvent($("#source"))
                  setTimeout(() => {
                    $("#source_unit").val(source_unit)
                  }, 500);
                  
                  $("#source_other").val(source_other)
                }
                else
                {
                  $("#source").val("義務人")
                  changeUnitEvent($("#source"))
                }

                payamount = parseInt($("#payamount").val())
                paycost = parseInt($("#paycost").val())
                turnaround = parseInt($("#turnaround").val())
                returnamount = parseInt($("#returnamount").val())

                $("#t_payamount").val(paycost + turnaround + returnamount + payamount);
              },
              complete:function(resp){
              }
          });
   }
   function delEvent(param){
    swal({
            title: "確定刪除?",
            text: "一經刪除還可加回本專案",
            icon: "warning",
            buttons: ["取消", "確定"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var formData = new FormData();
                formData.append('row_ser', param);
                formData.append('fpid', '<?php echo $fpid; ?>')
                $.ajax({            
                    type: 'POST',
                    url: '../../Acc_cert/delete_Fine_project_body',
                    data: formData,
                    // dataType: 'json',
                    processData : false, 
                    contentType: false,
                    cache: false,
                    error:function(){
                        console.log('error')
                    },
                    success: function(resp){
                        console.log('success');
                        if(resp !== 'error')
                            location.reload();
                        else
                            return false;
                    },
                    complete:function(resp){
                        // location.href = `../${resp.responseText}`;
                        console.log('complete')
                    }
                });
            } else {
                return false;
            }
        });
   }
   function sortEvent(type)
   {
    const row_id = table.row('.selected').data()[1];
     if(type.indexOf('down') > -1)
     {
        if($("#table1 tbody tr.selected")[0].rowIndex == $("#table1 tbody tr").length)
          return false;
        
        const current_index = $("#table1 tbody tr.selected")[0].rowIndex -1;
        let ch_data = table.row(current_index + 1).data();
        let ori_data = table.row('.selected').data();
				let ori_class = ($("#table1 tbody tr:eq("+(current_index)+")").hasClass('bg-success'))?true:false;
				let ch_class = ($("#table1 tbody tr:eq("+(current_index + 1)+")").hasClass('bg-success'))?true:false;
				
				if((ori_class && ch_class) || (!ori_class && !ch_class))
				{}
				else if(ori_class)
				{
					$("#table1 tbody tr:eq("+(current_index + 1)+")").addClass('bg-success');
					$("#table1 tbody tr:eq("+(current_index)+")").removeClass('bg-success');
				}
				else
				{
					$("#table1 tbody tr:eq("+(current_index)+")").addClass('bg-success');
					$("#table1 tbody tr:eq("+(current_index + 1)+")").removeClass('bg-success');
				}

        table.row(current_index + 1).data(ori_data);
        table.row(current_index).data(ch_data);
        $("#table1 tbody tr").removeClass('selected');
        $("#table1 tbody tr:eq("+(current_index + 1)+")").addClass('selected');
     }
     else
     {
        if($("#table1 tbody tr.selected")[0].rowIndex == 1)
          return false;

          const current_index = $("#table1 tbody tr.selected")[0].rowIndex -1;
          let ch_data = table.row(current_index - 1).data();
          let ori_data = table.row('.selected').data();
					let ori_class = ($("#table1 tbody tr:eq("+(current_index)+")").hasClass('bg-success'))?true:false;
					let ch_class = ($("#table1 tbody tr:eq("+(current_index - 1)+")").hasClass('bg-success'))?true:false;
					
					if((ori_class && ch_class) || (!ori_class && !ch_class))
					{}
					else if(ori_class)
					{
						$("#table1 tbody tr:eq("+(current_index - 1)+")").addClass('bg-success');
						$("#table1 tbody tr:eq("+(current_index)+")").removeClass('bg-success');
					}
					else
					{
						$("#table1 tbody tr:eq("+(current_index)+")").addClass('bg-success');
						$("#table1 tbody tr:eq("+(current_index - 1)+")").removeClass('bg-success');
					}

          table.row(current_index - 1).data(ori_data);
          table.row(current_index).data(ch_data);
          $("#table1 tbody tr").removeClass('selected');
          $("#table1 tbody tr:eq("+(current_index - 1)+")").addClass('selected');
     }

     
      var formData = new FormData();
      formData.append('row_id', row_id);
      formData.append('fpid', '<?php echo $fpid; ?>');
      formData.append('sort_type', type);
      $.ajax({            
          type: 'POST',
          url: '../../Acc_cert/fine_project_body_sort',
          data: formData,
          // dataType: 'json',
          processData : false, 
          contentType: false,
          cache: false,
          error:function(){
              console.log('error')
          },
          success: function(resp){
              console.log('success');
              // if(resp !== 'error')
              //     location.reload();
              // else
              //     return false;
          },
          complete:function(resp){
              // location.href = `../${resp.responseText}`;
              console.log('complete')
          }
      });
   }
	 /** 轉民國年 */
	function tranfer2RCyear(date)
	{
		if(!date)
			return false;
		date = date.split('-');
		date = (parseInt(date[0]) - 1911) + "-" + date[1] + "-" + date[2];
			return date;                
	}  
    </script>
