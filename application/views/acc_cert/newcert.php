        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="newcert"></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    </br>寫入憑證
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?php echo form_open_multipart('Acc_cert/updatecert/','id="newcert"') ?>          
                                        <div class="col-lg-12">
                                                <table class="table">
                                                    <tbody id="phone_r" class="form-group">
                                                        <tr>
                                                            <td style="padding:5px">
                                                                <label>憑證編號</label>
                                                                <?php echo form_input('f_certno',$row->f_certno, 'class="form-control" id="p_no"');  ?>                                                      
                                                                <input id="f_num " type="hidden" name="f_num" value=<?php echo $row->f_num ;?>> 
                                                            </td>
                                                            <td style="padding:5px">
                                                                <label>憑證金額</label>
                                                                <?php echo form_input('f_certamount',$row->f_certamount, 'class="form-control" id="p_oppw"');  ?>                                                      
                                                            </td>
                                                            <td style="padding:5px">
                                                                <label>憑證核發日期</label>
                                                                <input id="f_certIssue " type="date" name="f_certIssue" class="form-control" value=<?php echo $row->f_certIssue ;?>> 
                                                            </td>
                                                            <td style="padding:5px">
                                                                <label>憑證文件上傳</label>
                                                                <?php if($row->f_certdoc ==null){echo form_upload('f_certdoc', 'class="form-control" id="sel"');}
                                                                else  {echo anchor_popup('certdoc/' . $row->f_certdoc,'憑證文件');}
                                                                ?>                                                            
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../../js/metisMenu.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-twzipcode@1.7.14/jquery.twzipcode.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../../js/startmin.js"></script>
        <script src="../../js/jquery.multi-select.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>    
        <script type="text/javascript">
            $(document).ready(function(){
                $("#img1").hide();
                $(".thumbnail").click(function(){
                $("#img1").toggle();
                });
                $( "#create_phone" ).validate({
                    rules: {
                        p_no: {
                            required: true,
                            digits: true,
                            minlength: 10
                        },
                        "pr_phone[]": {
                            digits: true,
                            minlength: 10
                        },
                        p_oppw: {
                            digits: true,
                            minlength: 3
                        },
                        p_picpw: {
                            digits: true,
                        },
                        p_imei: {
                            digits: true,
                            minlength: 10
                        },
                    },
                    messages: {
                        p_no: {
                            required: "此欄位不得為空",
                            digits: "請輸入數字",
                            minlength: "請完整輸入",
                        },
                        "pr_phone[]": {
                            digits: "請輸入數字",
                            minlength: "請完整輸入",
                        },
                        p_oppw: {
                            digits: "請輸入數字",
                        },
                        p_picpw: {
                            digits: "請輸入數字",
                        },
                        p_imei: {
                            digits: "請輸入數字",
                            minlength: "至少輸入10碼"
                        },
                    }
                });        
            });
            function RemoveAddPhone(id)
            {
                phone_r--;
                var div=document.getElementById('phone_r'+id);
                var div2=document.getElementById('phone_r');
                div2.removeChild(div);
            } 
            var phone_r = 0;
            function CreateUploadPhone_R()
            {
                phone_r++;
                if(phone_r < 10){
                var td=document.createElement('tr');
                var html='<tr><td style="padding:5px"><label>通聯方向</label><select name="pr_path[]" class="form-control">'+
                        '<option>撥出</option><option>撥入</option></select></td><td style="padding:5px"><label>電話</label>'+
                        '<input name="pr_phone[]" class="form-control" ></td><td style="padding:5px"><label>姓名</label>'+
                        '<input name="pr_name[]" class="form-control" ></td><td style="padding:5px"><label>通聯時間</label>'+
                        '<input name="pr_time[]" class="form-control" input="date" type = "date"></td><td style="padding:5px">'+
                                                                '<label>與犯嫌關係</label>'+
                                                                '<input name="pr_relationship[]" class="form-control" >'+
                                                            '</td>'+
                                                            '<td style="padding:5px">'+
                                                                '<label>被告知毒品上手</label>'+
                                                                '<select name="pr_has_drug[]" class="form-control">'+
                                                                    '<option>是</option>'+
                                                                    '<option>否</option>'+
                                                                '</select>'+
                                                            '</td>'+
                                                            '<td style="padding:5px">'+
                                                                '<label>其他毒品使用者</label>'+
                                                                '<select name="pr_user[]" class="form-control">'+
                                                                   ' <option>是</option>'+
                                                                    '<option>否</option>'+
                                                                '</select>'+
                                                            '</td>'+
                                                            '<td style="padding:5px">'+
                                                                '<label>其他販毒者</label>'+
                                                                '<select name="pr_seller[]" class="form-control">'+
                                                                    '<option>是</option>'+
                                                                    '<option>否</option>'+
                                                                '</select>'+
                                                            '</td>'+
                                                            '<td>'+
                                                               '<label><input style="float: left;" class="btn btn-default" type="button" onclick="RemoveAddPhone('+phone_r+')"'+
                                                                'value="刪除" /></label>'+
                                                            '</td>'+
                    '</tr>'
                    td.innerHTML=html;
                    td.setAttribute("id","phone_r"+phone_r);
                    //div.id="upDiv"+p;
                    document.getElementById('phone_r').appendChild(td);
                }
                else{
                    alert('通聯記錄最多10筆');
                }
            }
        </script>
    </body>
</html>
