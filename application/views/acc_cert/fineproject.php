        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p><?php echo $sub_title; ?></p>
                    </blockquote>
                    <div class="row">
                        <div class="col-lg-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
											<div class="row">
												<div class="col-md-6">
												列表清單
												</div>
												<div class="col-md-6 text-right">
												<button class="btn btn-default btn-sm" data-toggle="modal" data-target="#newcases" data-whatever="新增至每日帳專案">新增空白專案</button>
												</div>
											</div>
                                            
                                        </div>
                                        <!-- <form action=<?php //echo base_url("Acc_cert/addAcc_Project") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8"> -->
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <?php  echo $s_table;?>
                                                </div>                       
                                            </div>
                                            <!-- <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                            <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                            <input id="s_status" type="hidden" name="s_status" value=''> 
                                        </form> -->
                                    </div>
                                    <!-- /.panel -->
                        </div>
                        <div class="col-lg-12">
                            <a class="btn btn-default" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            查看舊專案
                            </a>
                            <div class="collapse" id="collapseExample">
                                <div class="well">
                                    <div class="table-responsive">
                                        <?php  echo $s_table_done;?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                                <div class="modal fade" id="documentModal" tabindex="-1" role="dialog" aria-labelledby="documentModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="documentModalLabel">輸入公文號</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>輸入公文號</label>
                                                    <input type="text" name="fp_num_no" id="fp_num_no" class="form-control">
                                                                    
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                                                <input type="button"  class="btn btn-default" data-id="" id="updateOfficenoBT" value="儲存" onclick="saveEvent($(this), 0)"/>
                                            </div>
                                        </div>
                                    </div>
                                    </div> 
                            <!-- </form> -->
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
			<div class="modal fade" id="newcases" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="othernewLabel">新增每日帳作業</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>日期</label>
								<div class="input-group date">
									<input type="text" class="form-control"  id="pickPjname">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>專案類型</label>
								<select name='project_pay_type' id="sel" class="form-control">
									<option value="金融">金融</option>
									<option value="支匯票">支匯票</option>
								</select>
								<select name='project_type' id="sel1" class="form-control">
									<option value="罰鍰">罰鍰</option>
									<option value="怠金">怠金</option>
									<option hidden value="罰鍰/怠金">罰鍰/怠金</option>
								</select>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" data-dismiss="modal">關閉</button>
							<button class="btn btn-default" id='yes'>確認</button>
						</div>
					</div>
				</div>
			</div>
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
			$("#pickPjname").datepicker(
                {
                    todayHighlight: true,
                    format: "yyyy-mm-dd",
                    locale: {
                        format: 'YYYY-MM-DD'
                    }
                } 
            );
            $("#dater").daterangepicker(
            {
            startDate: '2020-09-01',  
            locale: {
                  format: 'YYYY-MM-DD'
                }
            } 
            );
            var i = 1;
            var table = $('#table1').DataTable({
                 "searching": false,
                "bProcessing": true, //顯示『資料載入中』
                'columnDefs': [
                 {
                    'targets': [0],
                    "render": function(data, type, row, meta) {
                        return (i++) + `<br/> <span class="fa fa-trash text-danger" aria-hidden="true" style="cursor:pointer;" onclick="delEvent('${data}')"></span>`;
                    }
                    // 'checkboxes': {
                    //    'selectRow': true
                    // }
                 }
              ],
            //   'select': {
            //      'style': 'multi'
            //   },
            //   'order': [[1, 'asc']],
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                
                    // $(nRow).attr("data-id", aData[0]);
                    // return nRow;
                },
              "language": {
                        "processing": "資料載入中...",
                        "lengthMenu": "每頁顯示 _MENU_ 筆",
                        "zeroRecords": "資料庫中未有相關資料。",
                        "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                        "infoEmpty": "資料庫中未有相關資料。",
                        "search": "搜尋:",
                        "paginate": {
                            "first": "第一頁",
                            "last": "最後一頁",
                            "next": "下一頁",
                            "previous": "上一頁"
                        }
                    }
                /*dom: 'Bfrtip',
                buttons: [
                    'copy', {
                extend: 'csv',
                text: 'CSV',
                bom : true}, 'excel', 'pdf'
                ]    */
           });
           table2 = $('#table2').DataTable({
                "searching": true,
                "bProcessing": true, //顯示『資料載入中』
            //   'select': {
            //      'style': 'multi'
            //   },
              'order': [[3, 'desc']],
              "language": {
                        "processing": "資料載入中...",
                        "lengthMenu": "每頁顯示 _MENU_ 筆",
                        "zeroRecords": "資料庫中未有相關資料。",
                        "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                        "infoEmpty": "資料庫中未有相關資料。",
                        "search": "搜尋:",
                        "paginate": {
                            "first": "第一頁",
                            "last": "最後一頁",
                            "next": "下一頁",
                            "previous": "上一頁"
                        }
                    },
                // dom: 'Bfrtip',
                /*buttons: [
                    'copy', {
                extend: 'csv',
                text: 'CSV',
                bom : true}, 'excel', 'pdf'
                ]    */
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            

           // Handle form submission event
        

        //    $('#documentModal').on('show.bs.modal', function (event) {
        //         var button = $(event.relatedTarget) // Button that triggered the modal
        //         var fp_num = button.data('whatever') // Extract info from data-* attributes
                
        //         var modal = $(this)
        //         modal.find('#updateOfficenoBT').attr('data-id', fp_num)
        //     })
        $('#documentModal').on('hide.bs.modal', function (event) {
            $("#fp_num_no").val('');
        });
            $("#updateOfficenoBT").click(function(){
                // saveEvent($(this));
            });
			$("#yes").click(function (){
				addProjectEvent('1');
			});
           
    });
    function modalEvent(id, default_fp_num = '')
    {
        $("#fp_num_no").val(default_fp_num);
        $('#updateOfficenoBT').attr('data-id', id);
    }
    function saveEvent(obj, pj_type = 0)
    {
            var formData = new FormData();
            formData.append('fp_no', obj.attr('data-id'));

            if(pj_type < 0)
                formData.append('fp_num', '-1');
            else
                formData.append('fp_num', $("#fp_num_no").val());

            $.ajax({            
                type: 'POST',
                url: '../Acc_cert/insertofficenum',
                data: formData,
                // dataType: 'json',
                processData : false, 
                contentType: false,
                cache: false,
                error:function(){
                    console.log('error')
                },
                success: function(resp){
                    console.log('success');
                    if(resp !== 'error')
                        location.reload();
                        // return true;
                    else
                        return false;
                },
                complete:function(resp){
                    // location.href = `../${resp.responseText}`;
                    console.log('complete')
                }
            });
            
    }
    function delEvent(id){
        swal({
            title: "確定刪除該每日帳專案?",
            text: "注意一經刪除將不能復原!",
            icon: "warning",
            buttons: ["取消", "確定"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var formData = new FormData();
                formData.append('fp_num', id);
                formData.append('pj_type', '<?php echo $pj_type; ?>');

                $.ajax({            
                    type: 'POST',
                    url: '../Acc_cert/delete_Fine_project',
                    data: formData,
                    // dataType: 'json',
                    processData : false, 
                    contentType: false,
                    cache: false,
                    error:function(){
                        console.log('error')
                    },
                    success: function(resp){
                        console.log('success');
                        if(resp !== 'error')
                            location.reload();
                        else
                            return false;
                    },
                    complete:function(resp){
                        // location.href = `../${resp.responseText}`;
                        console.log('complete')
                    }
                });
            } else {
                return false;
            }
        });
        
    }
	function checkhasdone(url)
	{
		swal({
            title: "請確定資料皆有輸入完成囉?",
            text: "",
            icon: "warning",
            buttons: ["等等我在看一下！", "都輸入好囉！"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
				location.href = url;
            } else {
                return false;
            }
        });
	}
	function addProjectEvent(s_status)
    {            
            var formData = new FormData();
            formData.append('project_name', $('#pickPjname').val());
            formData.append('project_pay_type', $("#sel").val());
            formData.append('project_type', $("#sel1").val());

            $.ajax({            
                type: 'POST',
                url: '../Acc_cert/addAcc_Empty_Project',
                data: formData,
                // dataType: 'json',
                processData : false, 
                contentType: false,
                cache: false,
                complete:function(resp){
                    location.reload();
                }
            });
            
    }
    $("#sel").change(function(){
        if($(this).val()=='支匯票'){
            $("#sel1").val('罰鍰/怠金');
            $("#sel1").hide();
        }
        else{
            $("#sel1").val('罰鍰');
            $("#sel1").show();
        }
    });

    </script>
