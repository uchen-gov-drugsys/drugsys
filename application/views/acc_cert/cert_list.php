        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
					<blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></p>
                    </blockquote>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
								<div class="panel-heading">
									<div class="row">
										<div class="col-md-6">
										列表清單
										</div>
										<div class="col-md-6 text-right">
										</div>
									</div>
									
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<?php  echo $s_table;?>
									</div>                       
								</div>
                                <!-- <div class="panel-heading list_view">
                                <form action="CertSearch" id="search" method="post">
                                    <div class="form-group">
                                        移送案號:<label><input id="ft_no"  type="text" name="ft_no"/></label>
                                        選擇日期區間:<label><input  id="dater"  type="text"  name="datepicker"/></label>
                                        <input type="text" name="cnum" hidden/>
                                        <input style="width:80px" type="text" name="name" hidden/>
                                        <input type="text" name="ic" hidden/>
                                        <input style="width:240px" type="text" name="BVC" hidden/>
                                        <input type="text" name="s_go" hidden/><br>
                                        <input type="text" id="status" name="status" hidden/><br>
                                        選擇日期區間:<label><input  id="dater" class="form-control" type="text"  name="datepicker"/></label>
                                        <input type="submit" value="查询"/><br>
                                    </div>
                                </form>
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 原處分書號
                                    <input type="checkbox" name="list" data-target ="2" checked> 姓名
                                    <input type="checkbox" name="list" data-target ="3" checked> 身份證編號
                                    <input type="checkbox" name="list" data-target ="4" checked> 年度罰緩金額
                                    <input type="checkbox" name="list" data-target ="5"> 完納金額
                                    <input type="checkbox" name="list" data-target ="6"> 分期資訊
                                    <input type="checkbox" name="list" data-target ="7" checked> 移送案號
                                    <input type="checkbox" name="list" data-target ="8"> 分局
                                    <input type="checkbox" name="list" data-target ="9"> 憑證編號
                                    <input type="checkbox" name="list" data-target ="10"> 憑證核發日期
                                    <input type="checkbox" name="list" data-target ="11"> 註銷保留款
                                    <input type="checkbox" name="list" data-target ="12"> 虛擬帳號
                                </div> -->
                            <!-- <form action=<?php //echo base_url("Acc_cert/addAcc_CProject") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php //echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                                <input id="f_no" type="hidden" name="f_no" value='<?php //echo $f_no ?>'> 
                           </div> -->
                            <!-- /.panel -->
                        </div>
                            <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>舊專案</label>
                                        <?php echo form_dropdown('fp_num',$opt ,'', 'class="form-control"')?>                                                            
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button id='no' class="btn btn-default" >加入舊專案</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                            </form> -->
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
            var latayesr = new Date().getFullYear()-1;
            var currentYear = new Date().getFullYear();
            var currentMonth = new Date().getMonth();
            var currentDay = new Date().getDay();
            var date = currentYear+'-'+currentMonth+'-'+currentDay;
            if(currentMonth == 0){
                currentYear = currentYear-1;
                date =currentYear +'-'+12+'-'+currentDay;
            }
            $("#dater").daterangepicker(
            {
            //startDate: date,  
            locale: {
                  format: 'YYYY-MM-DD'
                }
            } 
            );
			var table = $('#table1').DataTable({
				"language": {
                        "processing": "資料載入中...",
                        "lengthMenu": "每頁顯示 _MENU_ 筆",
                        "zeroRecords": "資料庫中未有相關資料。",
                        "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                        "infoEmpty": "資料庫中未有相關資料。",
                        "search": "搜尋:",
                        "paginate": {
                            "first": "第一頁",
                            "last": "最後一頁",
                            "next": "下一頁",
                            "previous": "上一頁"
                        }
                    },
            //      "searching": false,
                // 'columnDefs': [
                //  {
                //     'orderable': false,
                //     'targets': [9],
                //  },
                //  {
                //     'targets': [0]
                    // 'checkboxes': {
                    //     'selectRow': true
                    // }
            //      }
            //   ],
            //   'select': {
            //      'style': 'multi'
            //   },
              'order': [[0, 'desc']],
              "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $("td:first", nRow).html(iDisplayIndex + 1); //自動序號
                return nRow;
            },
            //     dom: 'Bfrtip',
            //     buttons: [
            //          {
            //     extend: 'csv',
            //     text: 'CSV',
            //     bom : true}
            //     ]     
           });
                // table.column(5).visible(false);
                // table.column(6).visible(false);
                // table.column(8).visible(false);
                // table.column(9).visible(false);
                // table.column(10).visible(false);
                // table.column(11).visible(false);
                // table.column(12).visible(false);
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#fpart").click(function (){
                $("#status").val('0');
                    //alert($("#status").val());
                $("#search").submit();
            });
            $("#fcomplete").click(function (){
                $("#status").val('1');
                    //alert("Submitted");
                $("#search").submit();
            });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert($("#status").val());
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
    });
    </script>
