        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h4><?php echo $title;?></h4></li>
                    <li><a><input type="submit" value="存儲" class="btn btn-default" style="padding:0px 0px;" form="save"></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php echo form_open_multipart('Acc_cert/save_disassemble_change','id="save"') ?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12"><br>
                                            <label><input style="float: left;" class="btn btn-default" type="button" onclick="CreateUploadAll()"
                                                                value="新增輸入" /></label>
                                                <br><table class="table">
                                                    <tbody id="drugall" class="form-group">
                                                        <tr>
                                                            <td>
                                                                <label>案件編號</label>
                                                                <input name="f_cnum[]" class="form-control" value= "<?php  if(isset($fcp->surc_no)) echo $fcp->surc_no; 
                                                                else echo $fcp->fd_num; ?> "disabled>
                                                                <input id="f_num[]" type="hidden" name="f_num[]" value='<?php echo $fcp->f_num ?>' disabled> 
                                                                <input id="f_snum[]" type="hidden" name="f_snum[]" value='<?php echo $fcp->f_snum ?>' disabled> 
                                                                <input id="f_sic[]" type="hidden" name="f_sic[]" value='<?php echo $fcp->f_sic ?>' disabled> 
                                                                <input id="f_BVC[]" type="hidden" name="f_BVC[]" value='<?php echo $fcp->f_BVC ?>' disabled> 
                                                                <input id="fcp_no[]" type="hidden" name="fcp_no[]" value='<?php echo $fcp->fcp_no ?>' disabled> 
                                                                <input id="f_project_num[]" type="hidden" name="f_project_num[]" value='<?php echo $fcp->f_project_num ?>' disabled> 
                                                                <input id="f_payday[]" type="hidden" name="f_payday[]" value='<?php echo $fcp->f_payday ?>' disabled> 
                                                                <input id="f_paytype[]" type="hidden" name="f_paytype[]" value='<?php echo $fcp->f_paytype ?>' disabled> 
                                                                <input id="f_refund[]" type="hidden" name="f_refund[]" value='<?php echo $fcp->f_refund ?>' disabled> 
                                                                <input id="f_turnsub[]" type="hidden" name="f_turnsub[]" value='<?php echo $fcp->f_turnsub ?>' disabled> 
                                                                <input id="f_certno[]" type="hidden" name="f_certno[]" value='<?php echo $fcp->f_certno ?>' disabled> 
                                                                <input id="f_certDate[]" type="hidden" name="f_certDate[]" value='<?php echo $fcp->f_certDate ?>' disabled> 
                                                                <input id="f_check_unit[]" type="hidden" name="f_check_unit[]" value='<?php echo $fcp->f_check_unit ?>' disabled> 
                                                                <input id="f_check_receipt[]" type="hidden" name="f_check_receipt[]" value='<?php echo $fcp->f_check_receipt ?>' disabled> 
                                                                <input id="f_check_unit[]" type="hidden" name="f_check_unit[]" value='<?php echo $fcp->f_check_unit ?>' disabled> 
                                                                <input id="f_check_officenum[]" type="hidden" name="f_check_officenum[]" value='<?php echo $fcp->f_check_officenum ?>'disabled> 
                                                                <input id="f_check_checknum[]" type="hidden" name="f_check_checknum[]" value='<?php echo $fcp->f_check_checknum ?>' disabled> 
                                                                <input id="f_remark[]" type="hidden" name="f_remark[]" value='<?php echo $fcp->f_remark ?>'> 
                                                                <input id="f_cancelAmount[]" type="hidden" name="f_cancelAmount[]" value='<?php echo $fcp->f_cancelAmount ?>' disabled> 
                                                            </td>
                                                            <td>
                                                                <label>姓名</label>
                                                                <input name="s_name[]" class="form-control" value= "<?php echo $fcp->s_name ?> "disabled>
                                                            </td>
                                                            <td>
                                                                <label>身份證編號</label>
                                                                <input name="f_sic[]" class="form-control" value= "<?php echo $fcp->s_ic ?> "disabled>
                                                            </td>
                                                            <td>
                                                                <label>年度罰緩金額</label>
                                                                <input name="f_damount[]" class="form-control" value= "<?php echo $fcp->f_damount ?> " disabled>
                                                            </td>
                                                            <td>
                                                                <label>年度怠金金額</label>
                                                                <input name="f_samount[]" class="form-control" value= "<?php echo $fcp->f_samount ?> " disabled>
                                                            </td>
                                                            <td>
                                                                <label>完納金額</label>
                                                                <input name="f_payamount[]" class="form-control" value= "<?php echo $fcp->f_payamount ?> " disabled>
                                                           </td>
                                                            <td>
                                                                <label>移送案號</label>
                                                                <input class="form-control" value= "<?php //echo $fcp->ft_no ?> "disabled>
                                                            </td>
                                                            <td>
                                                                <label>分署</label>
                                                                <input class="form-control" value= "<?php echo $fcp->s_roffice ?> "disabled>
                                                            </td>
                                                            <td>
                                                                <label>執行費</label>
                                                                <input name="f_fee[]" class="form-control" value= "<?php echo $fcp->f_fee ?> " disabled>
                                                            </td>
                                                            <td>
                                                                <label>轉正類型</label>
                                                                <input type="text" name="f_change_type[]" value="拆解" class="form-control" disabled>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>案件編號</label><?php $i=1; ?>
                                                                <input name="f_cnum[]" class="form-control" value= "<?php if(isset($fcp->surc_no)) echo $fcp->surc_no.'-'.$i++; 
                                                                else echo $fcp->fd_num.'-'.$i++;
                                                                ?> "readonly>
                                                                <input id="f_cnum1" type="hidden" name="f_cnum1" value='<?php echo $fcp->f_cnum ?>'> 
                                                                <input id="f_num[]" type="hidden" name="f_num[]" value='<?php echo $fcp->f_num ?>'> 
                                                                <input id="f_snum[]" type="hidden" name="f_snum[]" value='<?php echo $fcp->f_snum ?>'> 
                                                                <input id="f_sic[]" type="hidden" name="f_sic[]" value='<?php echo $fcp->f_sic ?>'> 
                                                                <input id="f_BVC[]" type="hidden" name="f_BVC[]" value='<?php echo $fcp->f_BVC ?>'> 
                                                                <input id="fcp_no[]" type="hidden" name="fcp_no[]" value='<?php echo $fcp->fcp_no ?>'> 
                                                                <input id="f_project_num[]" type="hidden" name="f_project_num[]" value='<?php echo $fcp->f_project_num ?>'> 
                                                                <input id="f_payday[]" type="hidden" name="f_payday[]" value='<?php echo $fcp->f_payday ?>'> 
                                                                <input id="f_paytype[]" type="hidden" name="f_paytype[]" value='<?php echo $fcp->f_paytype ?>'> 
                                                                <input id="f_refund[]" type="hidden" name="f_refund[]" value='<?php echo $fcp->f_refund ?>'> 
                                                                <input id="f_turnsub[]" type="hidden" name="f_turnsub[]" value='<?php echo $fcp->f_turnsub ?>'> 
                                                                <input id="f_certno[]" type="hidden" name="f_certno[]" value='<?php echo $fcp->f_certno ?>'> 
                                                                <input id="f_certDate[]" type="hidden" name="f_certDate[]" value='<?php echo $fcp->f_certDate ?>'> 
                                                                <input id="f_check_unit[]" type="hidden" name="f_check_unit[]" value='<?php echo $fcp->f_check_unit ?>'> 
                                                                <input id="f_check_receipt[]" type="hidden" name="f_check_receipt[]" value='<?php echo $fcp->f_check_receipt ?>'> 
                                                                <input id="f_check_unit[]" type="hidden" name="f_check_unit[]" value='<?php echo $fcp->f_check_unit ?>'> 
                                                                <input id="f_check_officenum[]" type="hidden" name="f_check_officenum[]" value='<?php echo $fcp->f_check_officenum ?>'> 
                                                                <input id="f_check_checknum[]" type="hidden" name="f_check_checknum[]" value='<?php echo $fcp->f_check_checknum ?>'> 
                                                                <input id="f_remark[]" type="hidden" name="f_remark[]" value='<?php echo $fcp->f_remark ?>'> 
                                                                <input id="f_cancelAmount[]" type="hidden" name="f_cancelAmount[]" value='<?php echo $fcp->f_cancelAmount ?>'> 
                                                            </td>
                                                            <td>
                                                                <label>姓名</label>
                                                                <input name="s_name[]" class="form-control" value= "<?php echo $fcp->s_name ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>身份證編號</label>
                                                                <input name="f_sic[]" class="form-control" value= "<?php echo $fcp->s_ic ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>年度罰緩金額</label>
                                                                <input name="f_damount[]" type="number" class="form-control ">
                                                            </td>
                                                            <td>
                                                                <label>年度怠金金額</label>
                                                                <input name="f_samount[]" type="number" class="form-control">
                                                            </td>
                                                            <td>
                                                                <label>完納金額</label>
                                                                <input name="f_payamount[]" type="number" class="form-control">
                                                           </td>
                                                            <td>
                                                                <label>移送案號</label>
                                                                <input class="form-control" value= "<?php //echo $fcp->ft_no ?> "disabled>
                                                            </td>
                                                            <td>
                                                                <label>分署</label>
                                                                <input class="form-control" value= "<?php echo $fcp->s_roffice ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>執行費</label>
                                                                <input name="f_fee[]" type="number" class="form-control">
                                                            </td>
                                                            <td>
                                                                <label>轉正類型</label>
                                                                <input type="text" name="f_change_type[]" value="拆解" class="form-control"readonly>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label>案件編號</label>
                                                                <input name="f_cnum[]" class="form-control" value= "<?php if(isset($fcp->surc_no)) echo $fcp->surc_no.'-'.$i++; 
                                                                else echo $fcp->fd_num.'-'.$i++;
                                                                ?> "readonly>
                                                                <input id="f_num[]" type="hidden" name="f_num[]" value='<?php echo $fcp->f_num ?>'> 
                                                                <input id="f_snum[]" type="hidden" name="f_snum[]" value='<?php echo $fcp->f_snum ?>'> 
                                                                <input id="f_sic[]" type="hidden" name="f_sic[]" value='<?php echo $fcp->f_sic ?>'> 
                                                                <input id="f_BVC[]" type="hidden" name="f_BVC[]" value='<?php echo $fcp->f_BVC ?>'> 
                                                                <input id="fcp_no[]" type="hidden" name="fcp_no[]" value='<?php echo $fcp->fcp_no ?>'> 
                                                                <input id="f_project_num[]" type="hidden" name="f_project_num[]" value='<?php echo $fcp->f_project_num ?>'> 
                                                                <input id="f_payday[]" type="hidden" name="f_payday[]" value='<?php echo $fcp->f_payday ?>'> 
                                                                <input id="f_paytype[]" type="hidden" name="f_paytype[]" value='<?php echo $fcp->f_paytype ?>'> 
                                                                <input id="f_refund[]" type="hidden" name="f_refund[]" value='<?php echo $fcp->f_refund ?>'> 
                                                                <input id="f_turnsub[]" type="hidden" name="f_turnsub[]" value='<?php echo $fcp->f_turnsub ?>'> 
                                                                <input id="f_certno[]" type="hidden" name="f_certno[]" value='<?php echo $fcp->f_certno ?>'> 
                                                                <input id="f_certDate[]" type="hidden" name="f_certDate[]" value='<?php echo $fcp->f_certDate ?>'> 
                                                                <input id="f_check_unit[]" type="hidden" name="f_check_unit[]" value='<?php echo $fcp->f_check_unit ?>'> 
                                                                <input id="f_check_receipt[]" type="hidden" name="f_check_receipt[]" value='<?php echo $fcp->f_check_receipt ?>'> 
                                                                <input id="f_check_unit[]" type="hidden" name="f_check_unit[]" value='<?php echo $fcp->f_check_unit ?>'> 
                                                                <input id="f_check_officenum[]" type="hidden" name="f_check_officenum[]" value='<?php echo $fcp->f_check_officenum ?>'> 
                                                                <input id="f_check_checknum[]" type="hidden" name="f_check_checknum[]" value='<?php echo $fcp->f_check_checknum ?>'> 
                                                                <input id="f_remark[]" type="hidden" name="f_remark[]" value='<?php echo $fcp->f_remark ?>'> 
                                                                <input id="f_cancelAmount[]" type="hidden" name="f_cancelAmount[]" value='<?php echo $fcp->f_cancelAmount ?>'> 
                                                            </td>
                                                            <td>
                                                                <label>姓名</label>
                                                                <input name="s_name[]" class="form-control" value= "<?php echo $fcp->s_name ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>身份證編號</label>
                                                                <input name="f_sic[]" class="form-control" value= "<?php echo $fcp->s_ic ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>年度罰緩金額</label>
                                                                <input name="f_damount[]" type="number" class="form-control ">
                                                            </td>
                                                            <td>
                                                                <label>年度怠金金額</label>
                                                                <input name="f_samount[]" type="number" class="form-control">
                                                            </td>
                                                            <td>
                                                                <label>完納金額</label>
                                                                <input name="f_payamount[]" type="number" class="form-control">
                                                           </td>
                                                            <td>
                                                                <label>移送案號</label>
                                                                <input class="form-control" value= "<?php //echo $fcp->ft_no ?> "disabled>
                                                            </td>
                                                            <td>
                                                                <label>分署</label>
                                                                <input class="form-control" value= "<?php echo $fcp->s_roffice ?> "readonly>
                                                            </td>
                                                            <td>
                                                                <label>執行費</label>
                                                                <input name="f_fee[]" type="number" class="form-control">
                                                            </td>
                                                            <td>
                                                                <label>轉正類型</label>
                                                                <input type="text" name="f_change_type[]" value="拆解" class="form-control"readonly>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    </form>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
            var all=1;
            var i=2;
            function CreateUploadAll()
            {
                all++;
                i++;
                if(i>5){
                    alert('不能增加超過5筆')
                    return;
                }
                var tr=document.createElement('tr');
                var html='<tr>'+
                    '<td>'+
                     '<label>案件編號</label>'+
                     '<input name="f_cnum[]" class="form-control" value= "<?php if(isset($fcp->surc_no)) echo $fcp->surc_no; else echo $fcp->fd_num; ?> -'+i+'"'+
                     '<input name="f_cnum[]" class="form-control" value= "<?php echo $fcp->fd_num."-"?>'+i+' "readonly>'+
                     '<input id="f_num[]" type="hidden" name="f_num[]" value="<?php echo $fcp->f_num ?>"> '+
                     '<input id="f_snum[]" type="hidden" name="f_snum[]" value="<?php echo $fcp->f_snum ?>"> '+
                     '<input id="f_sic[]" type="hidden" name="f_sic[]" value="<?php echo $fcp->f_sic ?>"> '+
                     '<input id="f_BVC[]" type="hidden" name="f_BVC[]" value="<?php echo $fcp->f_BVC ?>"> '+
                     '<input id="fcp_no[]" type="hidden" name="fcp_no[]" value="<?php echo $fcp->fcp_no ?>"> '+
                     '<input id="f_project_num[]" type="hidden" name="f_project_num[]" value="<?php echo $fcp->f_project_num ?>"> '+
                     '<input id="f_payday[]" type="hidden" name="f_payday[]" value="<?php echo $fcp->f_payday ?>"> '+
                     '<input id="f_paytype[]" type="hidden" name="f_paytype[]" value="<?php echo $fcp->f_paytype ?>"> '+
                     '<input id="f_refund[]" type="hidden" name="f_refund[]" value="<?php echo $fcp->f_refund ?>"> '+
                     '<input id="f_turnsub[]" type="hidden" name="f_turnsub[]" value="<?php echo $fcp->f_turnsub ?>"> '+
                     '<input id="f_certno[]" type="hidden" name="f_certno[]" value="<?php echo $fcp->f_certno ?>"> '+
                     '<input id="f_certDate[]" type="hidden" name="f_certDate[]" value="<?php echo $fcp->f_certDate ?>"> '+
                     '<input id="f_check_unit[]" type="hidden" name="f_check_unit[]" value="<?php echo $fcp->f_check_unit ?>"> '+
                     '<input id="f_check_receipt[]" type="hidden" name="f_check_receipt[]" value="<?php echo $fcp->f_check_receipt ?>"> '+
                     '<input id="f_check_unit[]" type="hidden" name="f_check_unit[]" value="<?php echo $fcp->f_check_unit ?>"> '+
                     '<input id="f_check_officenum[]" type="hidden" name="f_check_officenum[]" value="<?php echo $fcp->f_check_officenum ?>"> '+
                     '<input id="f_check_checknum[]" type="hidden" name="f_check_checknum[]" value="<?php echo $fcp->f_check_checknum ?>"> '+
                     '<input id="f_remark[]" type="hidden" name="f_remark[]" value="<?php echo $fcp->f_remark ?>"> '+
                     '<input id="f_cancelAmount[]" type="hidden" name="f_cancelAmount[]" value="<?php echo $fcp->f_cancelAmount ?>"> '+
                    '</td>'+
                    '<td><label>姓名</label><input name="s_name[]" class="form-control" value= "<?php echo $fcp->s_name ?> "readonly></td>'+
                    '<td><label>身份證編號</label><input name="f_sic[]" class="form-control" value= "<?php echo $fcp->s_ic ?> "readonly></td>'+
                    '<td><label>年度罰緩金額</label><input name="f_damount[]" class="form-control "></td>'+
                    '<td><label>年度怠金金額</label><input name="f_samount[]" class="form-control"></td>'+
                    '<td><label>完納金額</label><input name="f_payamount[]" class="form-control"></td>'+
                    '<td><label>移送案號</label><input class="form-control" value= "<?php //echo $fcp->ft_no ?> "readonly></td>'+
                    '<td><label>分署</label><input class="form-control" value= "<?php echo $fcp->s_roffice ?> "readonly></td>'+
                    '<td><label>執行費</label><input name="f_fee[]" class="form-control"></td>'+
                    '<td><label>轉正類型</label><input type="text" name="f_change_type[]" value="拆解" class="form-control"readonly></td>'+
                    '<td><label><input style="float: left;" class="btn btn-danger" type="button" onclick="RemoveAddAll('+all+')"'+
                    'value="X" /></label></td>'+
                    '</tr>'
                tr.innerHTML=html;
                //div.setAttribute("class","form-control");
                tr.setAttribute("id","drugrow"+all);
                //div.id="upDiv"+p;
                document.getElementById('drugall').appendChild(tr);
            }
            function RemoveAddAll(id)
            {
                 var div=document.getElementById('drugrow'+id);
                 var div2=document.getElementById('drugall');
                 div2.removeChild(div);
            }    
        </script>
    </body>
</html>
