        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
					<li><h4 style="margin-left:15px;margin-top:21px;letter-spacing:10px;">我是測試系統</h4></li>
                    <!-- <li><h3 class="text-white"><?php //echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li> -->
                    <!-- <li><a><button style="padding:0px 0px;" class="btn btn-default" data-toggle="modal" data-target="#newcases" data-whatever="新增至每日帳專案">新增至每日帳專案</button></a></li>
                    <li><a><button class="btn btn-default" style="padding:0px 0px;" data-toggle="modal" data-target="#exampleModal">加入舊專案</button></a></li> -->
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p>新增轉正作業</p>
                    </blockquote>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default" >
                                <div class="panel-heading">
                                    搜尋作業
                                </div>
                                <div id="loading" class="row hidden" style="
                                        position: absolute;
                                        width: 100%;
                                        height: 100%;
                                        background-color: rgba(0,0,0,0.3);
                                        z-index: 1030;
                                    ">
                                        <div class="loader" id="loader-1"></div>
                                        <p class="text-danger text-center" id="loadingTxt" style="background-color:#fff;"></p>
                                </div>
                                <div class="row">
                                <div class="col-md-6" style="padding: 10px 25px;">
                                        <form id="filter2Form" style="border: 1px solid #eee;border-radius: 5px;padding: 10px 25px 0px 25px;">
                                        <span class="text-muted" style="
                                            position: absolute;
                                            right: 50px;
                                            top: 0px;
                                            background-color: #fff;
                                            padding: 0px 20px;
                                        ">搜尋一</span>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                類型：<select type="text" name="type" id="f_type_2" class="form-control" style="width:12em;">
                                                    <option value='id'>身分證字號</option>
                                                    <option value='name'>姓名</option>
                                                    <option value='movenum'>移送案號</option>
                                                    <option value='virtualcode'>虛擬帳號</option>
                                                </select>                                        
                                                <!-- <input type="submit" value="查询"/> -->
                                                
                                                <!--選擇日期區間:<label><input  id="dater" class="form-control" type="text"  name="datepicker"/></label>-->                                        
                                            </div>
                                            <div class="form-group col-md-6">
                                                關鍵字：<input type="text" class="form-control" name="condition2" style="width:12em;" id="f_keyword">
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <input type="button" id="search2BT" value="查詢"  class="btn btn-default"/>
                                        </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6" style="padding: 10px 25px;">
                                        <form id="filterForm" style="border: 1px solid #eee;border-radius: 5px;padding: 10px 25px 0px 25px;">
                                            <span class="text-muted" style="
                                            position: absolute;
                                            right: 50px;
                                            top: 0px;
                                            background-color: #fff;
                                            padding: 0px 20px;
                                        ">搜尋零</span>
                                            <div class="row">
                                                <div class="form-group col-md-6" style="margin-bottom:0px;">
                                                    類型：<select type="text" name="type" id="f_type"  class="form-control" style="width:12em;">
                                                        <option value='A'>罰鍰</option>
                                                        <option value='B'>怠金</option>
                                                    </select>
                                                    <!-- 選擇日期區間:<label><input  id="dater"  type="text"  name="datepicker"/></label> -->
                                                    
                                                    <!-- <input type="submit" value="查询"/> -->
                                                    
                                                    <!--選擇日期區間:<label><input  id="dater" class="form-control" type="text"  name="datepicker"/></label>-->
                                                    
                                                </div>
                                                <div class="form-group col-md-6">
                                                年度：<select type="text" name="year" id="f_year" class="form-control" style="width:12em;">
                                                        <option value='98'>98</option>
                                                        <option value='99'>99</option>
                                                        <option value='100'>100</option>
                                                        <option value='101'>101</option>
                                                        <option value='102'>102</option>
                                                        <option value='103'>103</option>
                                                        <option value='104'>104</option>
                                                        <option value='105'>105</option>
                                                        <option value='106'>106</option>
                                                        <option value='107'>107</option>
                                                        <option value='108'>108</option>
                                                        <option value='109'>109</option>
                                                        <option value='110'>110</option>
                                                    </select>
                                                    
                                                </div>
                                                <div class="text-right">
                                                    <input type="button" id="searchBT" value="查詢" class="btn btn-default"/>
                                                </div>
                                                
                                            </div>
                                            <input type="text" name="cnum" hidden/>
                                            <input style="width:80px" type="text" name="name" hidden/>
                                            <input type="text" name="ic" hidden/>
                                            <input style="width:240px" type="text" name="BVC" hidden/>
                                            <input type="text" name="s_go" hidden/>
                                            <input type="text" id="status" name="status" hidden/>
                                        </form>
                                    </div>
                                    
                                </div>
                           </div>
                            <!-- /.panel .panel-warning-->
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    新增作業
                                </div>
                                <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="button" class="btn btn-success" id="addPjBT" >新增至轉正退費專案</button>
                                        <button type="button" class="btn btn-default" id="addOldPjBT" data-toggle="modal" data-target="#exampleModal">加入舊專案</button>
                                        
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <div class="row" id="table-button-wrap"></div>
                                        <table id="searchTable" class=" table table-bordered" style="width:1600px">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>類型</th>
                                                    <th>案件編號</th>
                                                    <th>受處分人姓名</th>
                                                    <th>身份證編號</th>
                                                    <th>
                                                    罰鍰(萬)/怠金(元) <br>
                                                    
                                                    </th>
                                                    <th>完納日期</th>
                                                    <th>完納金額</th>
                                                    <th>繳款日期</th>
                                                    <th>繳款金額</th>
                                                    <th>分期Log</th>
                                                    <th>移送Log</th>
                                                    <th>憑證Log</th>
                                                    <th>撤銷註銷Log</th>
                                                    <th>執行命令Log</th>
                                                    <th>備註</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </div>
                                </div>

                                <!-- <form action=<?php echo '#';//echo base_url("Acc_cert/addAcc_Project") ?> id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8"> -->
                                <!-- <div class="panel-body">
                                    <div class="table-responsive">
                                    <?php  //echo $s_table;?>
                                    </div>                       
                                </div> -->
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="s_status" type="hidden" name="s_status" value=''> 
                                </div>
                            </div>
                             <!-- /.panel .panel-success-->
                        </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>舊專案</label>
                                        <?php echo form_dropdown('fp_num',$opt ,'', 'class="form-control" id="fp_num"')?>                                                            
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button id='no' class="btn btn-default" >加入舊專案</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                                <div class="modal fade" id="newcases" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="othernewLabel">新增每日帳作業</h5>
                                                <select name='project_pay_type' id="sel" class="form-control">
                                                    <option value="金融">金融</option>
                                                    <!-- <option value="支匯票">支匯票</option> -->
                                                </select>
                                                <select name='project_type' id="sel1" class="form-control">
                                                    <option value="罰鍰">罰鍰</option>
                                                    <option value="怠金">怠金</option>
                                                    <option hidden value="罰鍰/怠金">罰鍰/怠金</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" data-dismiss="modal">關閉</button>
                                                <button class="btn btn-default" id='yes'>確認</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="showImgModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <img class="img-responsive show" src=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- </form> -->
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

            <!-- 已選帳務顯示窗 -->
            <div id="chooseDataWin" class="panel panel-default" style="width:250px;height:200px;background-color:rgba(255,255,255,0.9);position:fixed;left:10px;bottom:20px;z-index: 1050;">
                <div class="panel-body">
                    <h5>
                        <div class="col-sm-6">
                            <span>已選取</span>
                        </div>
                        <div class="col-sm-6 text-right">
                            <form id="printform" action="../PhpspreadsheetController/export_choose_fines" method="post">
                                <input type="hidden" name="datarows" id="datarows">
                                    <small class="label label-info" id="printBT" style="cursor:pointer;" >下載</small>
                            </form>
                        </div>
                    </h5>
                    <hr>
                    <div id="chooseData" class="col-sm-12"  style="overflow-y:scroll;height: 130px;padding:0px'">
                    </div>
                </div>
                
            </div>
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
            $("#dater").daterangepicker(
            {
            startDate: '2020-09-01',  
            locale: {
                  format: 'YYYY-MM-DD'
                }
            } 
            );
            var table = $('#table1').DataTable({
                "searching": false,
                'columnDefs': [
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']],
                /*dom: 'Bfrtip',
                buttons: [
                    'copy', {
                extend: 'csv',
                text: 'CSV',
                bom : true}, 'excel', 'pdf'
                ]    */
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });
          

        // 初始化Datatable載入資料
    $('#searchTable thead tr').clone(true).prependTo( '#searchTable thead' );
        $('#searchTable thead tr:eq(0) th').each( function (i) {
            var title = $(this).text();
            if(i == 2 || i == 3 || i == 4 || i == 11)
            {
                $(this).html( '<input type="text" placeholder="搜尋 '+title+'" />' );
            }
            else
            {
                $(this).html('');
            }
            
    
            $( 'input', this ).on( 'keyup change', function () {
                if ( searchTable.column(i).search() !== this.value ) {
                    searchTable
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
        var searchTable = $('#searchTable').DataTable({
            dom: 'f',
            "destroy":true,	
            "ordering":false,
            fixedHeader: true,
            "language": {
                        "processing": "資料載入中...",
                        "lengthMenu": "每頁顯示 _MENU_ 筆",
                        "zeroRecords": "資料庫中未有相關資料。",
                        "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                        "infoEmpty": "資料庫中未有相關資料。",
                        "search": "搜尋:",
                        "paginate": {
                            "first": "第一頁",
                            "last": "最後一頁",
                            "next": "下一頁",
                            "previous": "上一頁"
                        }
                    }
        });
        selectedAmount = 0; 
        payAmount = 0;
        chooseAry = [];

		searchTable.on('select ', function(e, dt, type, indexes) {
			calcAmountEvent();
        });
        $('#searchTable tbody').on( 'click', 'tr', function () {
            $(this).toggleClass('selected');
            
            if($(this).hasClass('selected'))
            {
                v1 = $(this).children('td:eq(1)').text(); // 類型
                v2 = $(this).children('td:eq(5)').text(); // 罰鍰/怠金 金額
                p1 = $(this).children('td:eq(7)').text(); // 完納金額
                p2 = $(this).children('td:eq(9)').text(); // 繳款金額

                if(v1 == '罰鍰')
                {
                    selectedAmount = (selectedAmount + (parseInt(v2) * 10000));                    
                }
                else if(v1 == '怠金')
                {
                    selectedAmount = (selectedAmount + (parseInt(v2)));
                }

                payAmount = (payAmount + parseInt(p1) + parseInt(p2));

                chooseAry.push({
                    type: $(this).children('td:eq(1)').text(),
                    caseid: $(this).children('td:eq(2)').text(),
                    username: $(this).children('td:eq(3)').text().replace('處分書移送書',''),
                    userid: $(this).children('td:eq(4)').text(),
                    amount: (($(this).children('td:eq(1)').text() == '罰鍰')?parseInt($(this).children('td:eq(5)').text())*10000:$(this).children('td:eq(5)').text()),
                    donedate: $(this).children('td:eq(6)').text(),
                    doneamount: $(this).children('td:eq(7)').text(),
                    paydate: $(this).children('td:eq(8)').text(),
                    payamount: $(this).children('td:eq(9)').text(),
                    paylog: $(this).children('td:eq(10)').text().replace('\n','<br>'),
                    movelog: $(this).children('td:eq(11)').text().replace('\n','<br>'),
                    cardlog: $(this).children('td:eq(12)').text().replace('\n','<br>'),
                    dellog: $(this).children('td:eq(13)').text().replace('\n','<br>'),
                    execlog: $(this).children('td:eq(14)').text().replace('\n','<br>'),
                    comment: $(this).children('td:eq(15)').text()
                })
                nowChooseEvent(chooseAry)
                // $("#chooseData").append('<p style="border-bottom:1px solid #d4d4d4;"><span class="label '+(($(this).children('td:eq(1)').text() == '罰鍰')?'label-danger':'label-warning')+'">'+$(this).children('td:eq(1)').text()+'</span> '+$(this).children('td:eq(2)').text()+' '+$(this).children('td:eq(3)').text().replace('處分書移送書','')+ '' +$(this).children('td:eq(4)').text()+'</p>');
            }
            else
            {
                v1 = $(this).children('td:eq(1)').text(); // 類型
                v2 = $(this).children('td:eq(5)').text(); // 罰鍰/怠金 金額
                p1 = $(this).children('td:eq(7)').text(); // 完納金額
                p2 = $(this).children('td:eq(9)').text(); // 繳款金額

                if(v1 == '罰鍰')
                {
                    selectedAmount = (selectedAmount - (parseInt(v2) * 10000));
                }
                else if(v1 == '怠金')
                {
                    selectedAmount = (selectedAmount - (parseInt(v2)));
                }
                payAmount = (payAmount - parseInt(p1) - parseInt(p2));

                delAry = {
                    type: $(this).children('td:eq(1)').text(),
                    caseid: $(this).children('td:eq(2)').text(),
                    username: $(this).children('td:eq(3)').text().replace('處分書移送書',''),
                    userid: $(this).children('td:eq(4)').text(),
                    amount: (($(this).children('td:eq(1)').text() == '罰鍰')?parseInt($(this).children('td:eq(5)').text())*10000:$(this).children('td:eq(5)').text()),
                    donedate: $(this).children('td:eq(6)').text(),
                    doneamount: $(this).children('td:eq(7)').text(),
                    paydate: $(this).children('td:eq(8)').text(),
                    payamount: $(this).children('td:eq(9)').text(),
                    paylog: $(this).children('td:eq(10)').text().replace('\n','<br>'),
                    movelog: $(this).children('td:eq(11)').text().replace('\n','<br>'),
                    cardlog: $(this).children('td:eq(12)').text().replace('\n','<br>'),
                    dellog: $(this).children('td:eq(13)').text().replace('\n','<br>'),
                    execlog: $(this).children('td:eq(14)').text().replace('\n','<br>'),
                    comment: $(this).children('td:eq(15)').text()
                };
                chooseAry.forEach(function(item, index, arr) {
                    if(JSON.stringify(item) === JSON.stringify(delAry)) {
                        return arr.splice(index, 1);
                    }
                });
                nowChooseEvent(chooseAry)
            }
            $('.selectedAmount').html('應罰: <br/>' + selectedAmount);
            $('.payAmount').html('已繳: <br/>' + payAmount);
            $('.balanceAmount').html('剩餘: <br/>' + (selectedAmount - payAmount));
            // countAmount();
        } );
           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = searchTable.column(0).selected();
            console.log(rows_selected.join(","));
                // $('#s_cnum').val(rows_selected.join(","));
                // $('input[name="id\[\]"]', form).remove();
            var allrows= searchTable.column(0).data();
            console.log(allrows.join(","))
                // $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
        
        $("#importBT").click(function(){
            $('#loading').removeClass('hidden');
            importEvent();
        });

        $("#searchBT").click(function(){
            chooseAry = [];
            nowChooseEvent(chooseAry);
            $("#addPjBT").attr('disabled', false);
            $("#addOldPjBT").attr('disabled', false);
            $('#searchTable').DataTable().clear().destroy();
            filterEvent('1', $("#f_type").val(), $("#f_year").val());
            resetEvent();
        });

        $("#search2BT").click(function(){
            chooseAry = [];
            nowChooseEvent(chooseAry);
            $("#addPjBT").attr('disabled', false);
            $("#addOldPjBT").attr('disabled', false);
            $('#searchTable').DataTable().clear().destroy();
            filterEvent('2', $("#f_type_2").val(), $("#f_keyword").val());
            resetEvent();
        });
           
        $("#addPjBT").click(function (){
            addProjectEvent('1');
        });

        $("#no").click(function (){
            addProjectEvent('0');
        });

        $('#showImgModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            var modal = $(this)
            modal.find('.modal-body img').attr('src',"../assets/pics/" + recipient + ".jpg")
        })
    });
    function tranfer2ADyear(date)
    {
        if(date.length == 6)
        {
            ad = (parseInt(date.substr(0, 2))) + 1911;
            return ad.toString() + '-' + date.substr(2, 2) + '-' + date.substr(4, 2);
        }
        else if(date.length == 7)
        {
            ad = (parseInt(date.substr(0, 3))) + 1911;
            return ad.toString() + '-' + date.substr(3, 2) + '-' + date.substr(5, 2);
        }
        else
        {
            return '';
        }
    }
    
    function filterEvent(condition, type, keyword){  
        selectedAmount = 0; 
        payAmount = 0;    
        $("#searchTable thead tr:eq(0)").remove();
        $("#searchTable tfoot tr:eq(0)").remove();
        $('#searchTable thead tr').clone(true).prependTo( '#searchTable thead' );
        $('#searchTable thead tr:eq(0)').clone(true).prependTo( '#searchTable tfoot' );
        $('#searchTable thead tr:eq(0) th').each( function (i) {
            var title = $(this).text();
            if(i == 2 || i == 3 || i == 4 || i == 11)
            {
                $(this).html( '<input type="text" placeholder="搜尋 '+title+'" />' );
            }
            else if(i == 5){
                $(this).html(`<span class="text-danger selectedAmount" style="font-size:10px;" id="selectedAmount">應罰:</span>`);
            }
            else if(i == 7 ){
                $(this).html(`<span class="text-danger payAmount" style="font-size:10px;" id="payAmount">已繳:</span>`);
            }
            else if(i == 9 ){
                $(this).html(`<span class="text-danger balanceAmount" style="font-size:10px;" id="balanceAmount">剩餘:</span>`);
            }
            else
            {
                $(this).html('');
            }
            $("#filtertype").bind('change', calcAmountEvent);
			$('#searchTable thead tr:eq(0) th input').bind('keyup change', function(){
				chooseAry = [];
            	nowChooseEvent(chooseAry);
				$('#searchTable tbody tr').removeClass('selected');
				calcAmountEvent();
			});
    
            $( 'input', this ).on( 'keyup change', function () {
                if ( searchTable.column(i).search() !== this.value ) {
                    searchTable
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
        $('#searchTable tfoot tr:eq(0) th').each( function (i) {
            if(i == 5){
                $(this).html(`<span class="text-danger selectedAmount" style="font-size:10px;" id="selectedAmount">應罰:</span>`);
            }else if(i == 7 ){
                $(this).html(`<span class="text-danger payAmount" style="font-size:10px;" id="payAmount">已繳:</span>`);
            }
            else if(i == 9 ){
                $(this).html(`<span class="text-danger balanceAmount" style="font-size:10px;" id="balanceAmount">剩餘:</span>`);
            }else
            {
                $(this).html('');
            }
        });
        searchTable = $('#searchTable').DataTable({	
                "destroy":true,	
                // searchPanes: {
                //     // layout: 'columns-4',
                //     // orderable: false,
                //     viewTotal: true,
                //     // columns: [2, 3, 4, 11]
                // },
                dom: 'Bfrtip',
                buttons: [ 
                    { extend: 'colvis', text: '隱藏欄位', columns: ':gt(0)' }, 
                    { extend: 'pageLength', text: '每頁顯示筆數' }
                ],
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10 筆', '25 筆', '50 筆', '顯示全部' ]
                ],
                // columnDefs: [
                //     {
                //         searchPanes: {
                //             show: true
                //         },
                //         // targets: [2, 3, 4, 11]
                //     }
                // ],					
                "bProcessing": true, //顯示『資料載入中』							
                "sAjaxSource": `../Acc_cert/filterAccAll?condition=${condition}&type=${type}&keyword=${keyword}`, // API
                "aoColumns": [
                    { "mData": "type" },
                    { "mData": "type",
                        "mRender":function(val,type,row){
                            if(val=='A'){
                                return `<span class="label label-danger">罰鍰</span>`;
                            }else{
                                return `<span class="label label-warning">怠金</span>`;
                            }
                        } 
                    },
                    { "mData": "f_caseid"},
                    { "mData": "f_username"},
                    { "mData": "f_userid"},
                    { "mData": "f_amount", "sWidth": '80px'},
                    { "mData": "f_donedate",
                        "mRender":function(val,type,row){
                            return (val == '0000-00-00')?'':tranfer2RCyear(val);
                        } 
                    },
                    { "mData": "f_doneamount"},
                    { "mData": "f_paydate",
                        "mRender":function(val,type,row){
                            return (val == '0000-00-00')?'':tranfer2RCyear(val);
                        } 
                    },
                    { "mData": "f_paymoney"},
                    { "mData": "f_paylog"},
                    { "mData": "f_movelog",
						"mRender": function(val,type,row){
							if(val)
							{
								let calcmovelog = val.split(/\r\n|\r|\n/);
								if(calcmovelog.length > 1)
								{
									calcmovelog.sort(function(a, b) {
										var movedateA = a.split('_')[0] * 1; 
										var movedateB = b.split('_')[0] * 1; 
										if (movedateA < movedateB) {
											return 1;
										}
										if (movedateA > movedateB) {
											return -1;
										}

										// names must be equal
										return 0;
									});

									return calcmovelog.join('<br/>');
								}
								else
								{
									return val;
								}
								
							}
							else
							{
								return val;
							}
							
						}
					},
                    { "mData": "f_cardlog"},
                    { "mData": "f_dellog"},
                    { "mData": "f_execlog"},
                    { "mData": "f_comment"}
                ],                
                "bAutoWidth": true,
                // "orderCellsTop": true,
                // "fixedHeader": true,
                // "order": [[0, "desc"],[ 1, "asc" ]], //資料排序，0->第一欄;1->第二欄
                "ordering":false,
                fixedHeader: true,
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    $(nRow).attr("data-id", aData.f_no);
                    $("td:first", nRow).html(iDisplayIndex + 1); //自動序號

                    let movedate = (aData.f_movelog)?tranfer2ADyear((aData.f_movelog.split("\n")[aData.f_movelog.split("\n").length -1]).split('_')[0]):'';
                    let execdate = (aData.f_execlog)?tranfer2ADyear((aData.f_execlog.split("\n")[aData.f_execlog.split("\n").length -1]).split('_')[0]):'';
                    let donedate = (aData.f_donedate !== '0000-00-00')?aData.f_donedate:'';

                    if(new Date(movedate).getTime() > new Date(donedate).getTime() )
                    {
                        $(nRow).addClass('bg-danger');
                    }
                    if(new Date(execdate).getTime() > new Date(donedate).getTime() )
                    {
                        $(nRow).addClass('bg-warning');
                    }
                    
                    if(aData.f_comment !== "")
                    {
                        $(nRow).addClass('bg-info');
                    }

                    if(aData.f_abmormal == 1 || aData.f_abmormal == '1')
                    {
                        if(!$(nRow).children('td:eq(2)').has( "span" ).length)
                        {
                            $(nRow).children('td:eq(2)').append('<span class="label label-success">已處理</span>')
                        }                        
                    }
                    if(!$(nRow).children('td:eq(3)').has( "div" ).length)
                    {
                        $(nRow).children('td:eq(3)').append('<div><span class="label label-default" style="cursor: pointer" data-toggle="modal" data-target="#showImgModal" data-whatever="'+((aData.type == "A")?"罰":"怠")+aData.f_caseid+'">處分書</span><span class="label label-default" style="margin-left:5px;cursor: pointer" data-toggle="modal" data-target="#showImgModal" data-whatever="行'+aData.f_caseid+'">移送書</span></div>')
                    }

                    return nRow;
                },
                "fnDrawCallback": function ( oSettings ) {
                    // $(oSettings.nTHead.children[2]).hide();
                    // console.log(oSettings.nTHead.children[1])
                },
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search": "搜尋:",
                    "paginate": {
                        "first": "第一頁",
                        "last": "最後一頁",
                        "next": "下一頁",
                        "previous": "上一頁"
                    },
                    buttons: {
                        pageLength: {
                            _: "每頁顯示 %d 筆",
                            '-1': "顯示全部"
                        }
                    },
                    // searchPanes: {
                    //     title: {
                    //         _: '條件選取 - %d',
                    //         0: '未選取任何條件'
                    //     },
                    //     clearMessage: '清除條件'
                    // }
                },  
                
            });
            
            
            
    }
	function calcAmountEvent(obj = null)
	{
		selectedAmount = 0; 
		payAmount = 0;
		chooseAry = [];
		$('#searchTable tbody tr').each(function(index, el){
			if($(this).hasClass('selected'))
			{
				v1 = $(this).children('td:eq(1)').text(); // 類型
				v2 = $(this).children('td:eq(5)').text(); // 罰鍰/怠金 金額
				p1 = $(this).children('td:eq(7)').text(); // 完納金額
				p2 = $(this).children('td:eq(9)').text(); // 繳款金額

				if(v1 == '罰鍰')
				{
					selectedAmount = (selectedAmount + (parseInt(v2) * 10000));                    
				}
				else if(v1 == '怠金')
				{
					selectedAmount = (selectedAmount + (parseInt(v2)));
				}

				payAmount = (payAmount + parseInt(p1) + parseInt(p2));

				chooseAry.push({
					type: $(this).children('td:eq(1)').text(),
					caseid: $(this).children('td:eq(2)').text(),
					username: $(this).children('td:eq(3)').text().replace('處分書移送書',''),
					userid: $(this).children('td:eq(4)').text(),
					amount: (($(this).children('td:eq(1)').text() == '罰鍰')?parseInt($(this).children('td:eq(5)').text())*10000:$(this).children('td:eq(5)').text()),
					donedate: $(this).children('td:eq(6)').text(),
					doneamount: $(this).children('td:eq(7)').text(),
					paydate: $(this).children('td:eq(8)').text(),
					payamount: $(this).children('td:eq(9)').text(),
					paylog: $(this).children('td:eq(10)').text().replace('\n','<br>'),
					movelog: $(this).children('td:eq(11)').text().replace('\n','<br>'),
					cardlog: $(this).children('td:eq(12)').text().replace('\n','<br>'),
					dellog: $(this).children('td:eq(13)').text().replace('\n','<br>'),
					execlog: $(this).children('td:eq(14)').text().replace('\n','<br>'),
					comment: $(this).children('td:eq(15)').text()
				})
				nowChooseEvent(chooseAry)
			}
			else
			{
				v1 = $(this).children('td:eq(1)').text(); // 類型
				v2 = $(this).children('td:eq(5)').text(); // 罰鍰/怠金 金額
				p1 = $(this).children('td:eq(7)').text(); // 完納金額
				p2 = $(this).children('td:eq(9)').text(); // 繳款金額

				if(v1 == '罰鍰')
				{
					selectedAmount = (selectedAmount - (parseInt(v2) * 10000));
				}
				else if(v1 == '怠金')
				{
					selectedAmount = (selectedAmount - (parseInt(v2)));
				}
				payAmount = (payAmount - parseInt(p1) - parseInt(p2));

				delAry = {
					type: $(this).children('td:eq(1)').text(),
					caseid: $(this).children('td:eq(2)').text(),
					username: $(this).children('td:eq(3)').text().replace('處分書移送書',''),
					userid: $(this).children('td:eq(4)').text(),
					amount: (($(this).children('td:eq(1)').text() == '罰鍰')?parseInt($(this).children('td:eq(5)').text())*10000:$(this).children('td:eq(5)').text()),
					donedate: $(this).children('td:eq(6)').text(),
					doneamount: $(this).children('td:eq(7)').text(),
					paydate: $(this).children('td:eq(8)').text(),
					payamount: $(this).children('td:eq(9)').text(),
					paylog: $(this).children('td:eq(10)').text().replace('\n','<br>'),
					movelog: $(this).children('td:eq(11)').text().replace('\n','<br>'),
					cardlog: $(this).children('td:eq(12)').text().replace('\n','<br>'),
					dellog: $(this).children('td:eq(13)').text().replace('\n','<br>'),
					execlog: $(this).children('td:eq(14)').text().replace('\n','<br>'),
					comment: $(this).children('td:eq(15)').text()
				};
				chooseAry.forEach(function(item, index, arr) {
					if(JSON.stringify(item) === JSON.stringify(delAry)) {
						return arr.splice(index, 1);
					}
				});
				nowChooseEvent(chooseAry)

				selectedAmount = (selectedAmount < 0)?0:selectedAmount;
				payAmount = (payAmount < 0)?0:payAmount;
			}
			$('.selectedAmount').html('應罰: <br/>' + selectedAmount);
			$('.payAmount').html('已繳: <br/>' + payAmount);
			$('.balanceAmount').html('剩餘: <br/>' + (selectedAmount - payAmount));
		});
	}
	/** 轉民國年 */
	function tranfer2RCyear(date)
	{
		if(!date)
			return false;
		date = date.split('-');
		date = (parseInt(date[0]) - 1911) + "-" + date[1] + "-" + date[2];
			return date;                
	}  
    function addProjectEvent(s_status)
    {
            var rows_selected = $.map(searchTable.rows('.selected').nodes(), function (item) {
                return $(item).attr("data-id");
            });                
            var allrows= searchTable.rows('.selected').data().toArray();
            
            var formData = new FormData();
            formData.append('s_status', s_status);
            formData.append('project_type', $("#sel1").val());
            formData.append('s_cnum', rows_selected.join(","));
            formData.append('s_cnum1', JSON.stringify(allrows) );
            formData.append('fp_num', $("#fp_num").val());

            $.ajax({            
                type: 'POST',
                url: '../Acc_cert/addAcc_CProject',
                data: formData,
                // dataType: 'json',
                processData : false, 
                contentType: false,
                cache: false,
                error:function(){
                    console.log('error')
                },
                success: function(resp){
                    
                    console.log(resp);
                },
                complete:function(resp){
                    swal({
                        title: "成功!",
                        text: "已加入轉正退費!",
                        icon: "success",
                        buttons: {
                            goto: {
                            text: "前往轉正退費專案",
                            value: "goto",
                            },
                            ok: {
                            text: "好",
                            value: true,
                            },
                        },
                    }).then((value) => {
                        switch (value) {                        
                            case "goto":
                            location.href = `../${resp.responseText}`;
                            break;                        
                            default:
                        }
                    });
                    nowChooseEvent([]);
                    $('#searchTable tr').removeClass('selected');
                    console.log(resp)
                }
            });
            
    }
    function processOKEvent()
    {
        if(confirm('確定選擇的資料皆已處理完成？'))
        {
            var rows_selected = $.map(searchTable.rows('.selected').nodes(), function (item) {
                return $(item).attr("data-id");
            });                
            var allrows= searchTable.rows('.selected').data().toArray();
            
            var formData = new FormData();
            formData.append('s_cnum', rows_selected.join(","));

            $.ajax({            
                type: 'POST',
                url: '../Acc_cert/update_Fine_abnormal',
                data: formData,
                // dataType: 'json',
                processData : false, 
                contentType: false,
                cache: false,
                error:function(){
                    console.log('error')
                },
                success: function(resp){
                    
                    console.log(resp);
                },
                complete:function(resp){
                    if(resp.responseText == 'ok')
                    {
                        $('#searchTable').DataTable().clear().destroy();
                        filterEvent('3', '', $("#showall").prop('checked'));
                    }
                    else
                    {
                        alert('請選擇需更動的資料');
                    }
                    console.log(resp)
                }
            });
        }
            
            
    }
    function nowChooseEvent(ary)
    {
        str = '';

        ary.forEach(function(item, index, arr) {
            str += '<p style="border-bottom:1px solid #d4d4d4;"><span class="label '+((item['type'] == '罰鍰')?'label-danger':'label-warning')+'">'+item['type']+'</span> '+item['caseid']+' '+item['username'].replace('處分書移送書','')+ '' +item['amount']+'</p>';
        });
        $("#chooseData").html(str);
    }
    function resetEvent(){
        $("#f_type").val('A');
        $("#f_year").val('98');
        $("#f_type_2").val('id');
        $("#f_keyword").val('');
        $("#showall").prop('checked', false);
    }
    $("#sel").change(function(){
        if($(this).val()=='支匯票'){
            $("#sel1").val('罰鍰/怠金');
            $("#sel1").hide();
        }
        else{
            $("#sel1").val('罰鍰');
            $("#sel1").show();
        }
    });
    </script>
