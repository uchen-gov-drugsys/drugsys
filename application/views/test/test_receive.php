        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                    <li><a><input type="submit" value="確認上傳" class="btn btn-default" style="padding:0px 0px;" form="updatecases" id="yes"></a></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <form action=<?php echo base_url("Test_excelupdate/receiveupdate")?> id="updatecases"  enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <div class="container-fluid">
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    </br>
                                </div>                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th> </th>
                                                    <th> </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="odd gradeX">
                                                    <td>excel上傳</td>
                                                    <td><input class="form-controls" name="excel" type="file"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
            <!-- /#page-wrapper -->
            </div>
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
            $(document).ready(function() {
                $('#fadai').hide();//用ifelse判斷是否大於20歲
                //if($('#s_birth').val())
                var age = 100;
                $("#s_birth").change(function(){
                    var today = new Date();
                    var birthdate = new Date($('#s_birth').val());
                    age = today.getFullYear() - birthdate.getFullYear();
                    var m = today.getMonth() - birthdate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
                        age--;
                    }
                });                
            $( "#addcases" ).validate({
                    rules: {
                        s_name: {
                            required: true,
                            minlength: 2
                        },
                        s_ic: {
                            required: true,
                            minlength: 9
                        },
                        s_birth: {
                            required: true,
                        },
                        s_dpdistrict: {
                            required: true,
                        },
                        s_dpaddress: {
                            required: true,
                        },
                    },
                    messages: {
                        s_name: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        s_ic: {
                            required: "此欄位不得為空",
                            minlength: "請完整輸入",
                        },
                        s_dpdistrict: {
                            required: "此欄位不得為空",
                        },
                        s_dpaddress: {
                            required: "此欄位不得為空",
                        },
                    }
            });  
                $("#yes").click(function (){
                    $("#s_status").val('1');
                    if(age < 20){
                        $('#fadai').show();
                        if($('#fadaival').val()!=""){
                            $("#addcases").submit();
                        }else{alert('受處分人未滿20歲，請輸入法代人資料');}
                    }else{
                        $("#addcases").submit();
                    }
                });
                $("#no").click(function (){
                    $("#s_status").val('0');
                    if(age < 20){
                        $('#fadai').show();
                        if($('#fadaival').val()!=""){
                            $("#addcases").submit();
                        }else{alert('受處分人未滿20歲，請輸入法代人資料');}
                    }else{
                        $("#addcases").submit();
                    }
                });
                $("#drug").click(function (){
                    $("#s_status").val('88');
                    $("#link").val('disciplinary_c/editdrug/'+$("#c_num").val());
                    if(age < 20){
                        $('#fadai').show();
                        if($('#fadaival').val()!=""){
                            $("#addcases").submit();
                        }else{alert('受處分人未滿20歲，請輸入法代人資料');}
                    }else{
                        $("#addcases").submit();
                    }
                });
                $("#susp").click(function (){
                    $("#s_status").val('99');
                    //alert($("#link").val());
                    if(age < 20){
                        $('#fadai').show();
                        if($('#fadaival').val()!=""){
                            $("#addcases").submit();
                        }else{alert('受處分人未滿20歲，請輸入法代人資料');}
                    }else{
                        $("#addcases").submit();
                    }
                });
        });  
            $("#zipcode").twzipcode({
            "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_dpcounty',   // 預設值為 county
                'districtName' : 's_dpdistrict', // 預設值為 district
                'zipcodeName'  : 's_dpzipcode' // 預設值為 zipcode
            });         
            $("#zipcode2").twzipcode({
            "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_rpcounty',   // 預設值為 county
                'districtName' : 's_rpdistrict', // 預設值為 district
                'zipcodeName'  : 's_rpzipcode' // 預設值為 zipcode
            });  
            $("#zipcode3").twzipcode({
            "zipcodeIntoDistrict": true,
            "css": ["city form-control", "town form-control"],
                'countyName'   : 's_fadai_county',   // 預設值為 county
                'districtName' : 's_fadai_district', // 預設值為 district
                'zipcodeName'  : 's_fadai_zipcode' // 預設值為 zipcode
            }); 
 
                $('#receiverchange').on('click', function() {
                   // $("#zipcode2").empty();
                    var city = $(zipcode).twzipcode('get', 'city');
                    var zipc = $(zipcode).twzipcode('get', 'zipcode');
                    //console.log(zipc);   // 縣市
                    if( zipc == ""){
                        alert("請務必選擇鄉鎮市區");return false;
                    } 
                    $("#zipcode2").twzipcode('set', zipc[0]);
                    var sno=$("#s_no").val();

                    // 移除空格確定是否空值
                    if (!$.trim($("#s_no").val())) {
                        if( $("#s_no").val()==""){
                            $("#s_no").focus();
                            alert("請務必填入路門牌");return false;
                        }     
                    }   
                    $("#s_no2").val(sno);
                }); 
            
                $( "#addcases" ).validate({
                    rules: {
                        other_doc: {
                            extension: "pdf|doc|docx"
                        },
                        excel: {
                            extension: "csv|xlsx"
                        },
                        surc_basenum: {
                            required: true,
                        },
                    },
                    messages: {
                        other_doc: {
                            extension: "只接受pdf|doc|docx"
                        },
                        excel: {
                            extension: "只接受csv或xlsx"
                        },
                    }
                });        
        </script>
