        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
            <?php 
            function filter_by_value($array, $index, $value){
                if(is_array($array) && count($array)>0) 
                {
                    foreach(array_keys($array) as $key){
                        $temp[$key] = $array[$key][$index];
                        
                        if ($temp[$key] == $value){
                            $newarray[$key] = $array[$key];
                        }
                    }
                  }
              return $newarray;
            }
            $start_year = 99;
            $now_year = date('Y')-1911;
            ?>
                <div class="container-fluid"> 
                    <blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p>整合入口</p>
                    </blockquote>
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="loading" class="row hidden" style="
                                        position: absolute;
                                        width: 100%;
                                        height: 100%;
                                        background-color: rgba(0,0,0,0.3);
                                        z-index: 1030;
                                    ">
                                    <div class="row" style="position: fixed;left:50%;">
                                        <div class="loader" id="loader-1"></div>
                                        <p class="text-danger text-center" id="loadingTxt" style="background-color:#fff;"></p>
                                    
                                    </div>
                                </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading text-center"><h3>裁罰</h3> </div>
                                <div class="panel-body">
                                    <div class="row" style="margin-top:2rem;">
                                        <div class=" col-md-12 ">
                                            
                                            <form class="form-horizontal">
                                                <?php 
                                                // var_dump($data);
                                                $filenameA = filter_by_value($data, 'f_belong', 'A');
                                                // var_dump($filename);
                                                
                                                for ($i=$start_year; $i <= $now_year ; $i++) { 
                                                    
                                                    echo '<div class="form-group col-md-6">
                                                            <label class="col-md-3 text-right">'.$i.'年</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input type="file" name="year_'.$i.'"  class="form-control" id="year_'.$i.'">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-default" type="button" data-year="'.$i.'" data-belong="A" onclick="uploadEvent($(this), \'CF\')">上傳</button>
                                                                    </span>
                                                                </div><!-- /input-group -->';
                                                        $matchA = false;
                                                        foreach($filenameA as $k=>$v){
                                                            if(array_search($i, $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                $matchA = true;
                                                            }
                                                        }            
                                                        if(!$matchA)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                    echo '</div>                                                
                                                        </div>';
                                                }
                                            ?>

                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-success">
                                <div class="panel-heading text-center"><h3>帳務</h3> </div>
                                <div class="panel-body">
                                    <div class="row" style="margin-top:2rem;">

                                        <div class=" col-md-12">
                                            <form class="form-horizontal">
                                                <?php 
                                                $filenameB = filter_by_value($data, 'f_belong', 'B');
                                                
                                            ?>
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">整合年度-罰鍰完納(108以前年度)</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_108" class="form-control"
                                                                id="year_108">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"
                                                                    data-year="108" data-belong="B"
                                                                    onclick="uploadEvent($(this), 'CW')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php 
                                                        $matchB = false;
                                                        foreach($filenameB as $k=>$v){
                                                            if(array_search('108', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                $matchB = true;
                                                            }
                                                        }            
                                                        if(!$matchB)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">(罰鍰)109分期暨完納收繳登記簿</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_109" class="form-control"
                                                                id="year_109">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"
                                                                    data-year="109" data-belong="B"
                                                                    onclick="uploadEvent($(this), 'CW')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php 
                                                        $matchB = false;
                                                        foreach($filenameB as $k=>$v){
                                                            if(array_search('109', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                break;
                                                                $matchB = true;
                                                            }
                                                        }            
                                                        if(!$matchB)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">(罰鍰)110分期暨完納收繳登記簿</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_110" class="form-control"
                                                                id="year_110">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"
                                                                    data-year="110" data-belong="B"
                                                                    onclick="uploadEvent($(this),'CW')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php 
                                                        $matchB = false;
                                                        foreach($filenameB as $k=>$v){
                                                            if(array_search('110', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                break;
                                                                $matchB = true;
                                                            }
                                                        }            
                                                        if(!$matchB)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                        ?>
                                                    </div>
                                                </div>
												<div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">(罰鍰)111分期暨完納收繳登記簿</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_111" class="form-control"
                                                                id="year_111">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"
                                                                    data-year="111" data-belong="B"
                                                                    onclick="uploadEvent($(this),'CW')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php 
                                                        $matchB = false;
                                                        foreach($filenameB as $k=>$v){
                                                            if(array_search('111', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                break;
                                                                $matchB = true;
                                                            }
                                                        }            
                                                        if(!$matchB)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                        ?>
                                                    </div>
                                                </div>
												<div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">CP99to109.xlsx</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_109special" class="form-control"
                                                                id="year_109special">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"
                                                                    data-year="109special" data-belong="B"
                                                                    onclick="uploadEvent($(this),'CW')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php 
                                                        $matchB = false;
                                                        foreach($filenameB as $k=>$v){
                                                            if(array_search('109special', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                break;
                                                                $matchB = true;
                                                            }
                                                        }            
                                                        if(!$matchB)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                        ?>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading text-center"><h3>執行命令</h3> </div>
                                <div class="panel-body">
                                    <div class="row" style="margin-top:2rem;">
                                            <div class=" col-md-12">
                                                <form class="form-horizontal">
                                                    <?php 
                                                    $filenameC = filter_by_value($data, 'f_belong', 'C');
                                                    ?>
                                                    <div class="form-group col-md-6">
                                                        <label class="col-md-3 text-right">執行命令</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input type="file" name="year_110" class="form-control" id="year_110">
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-default" type="button" data-year="110" data-belong="C"
                                                                        onclick="uploadEvent($(this),'ML')">上傳</button>
                                                                </span>
                                                            </div><!-- /input-group -->
                                                            <?php 
                                                            $matchC = false;
                                                            foreach($filenameC as $k=>$v){
                                                                if(array_search('110', $v))
                                                                {
                                                                    echo "<h5>
                                                                                <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                            </h5>", PHP_EOL;
                                                                    $matchC = true;
                                                                }
                                                            }            
                                                            if(!$matchC)
                                                            {
                                                                echo "<h5>
                                                                                <small>目前檔案：</small>
                                                                            </h5>", PHP_EOL;
                                                            }     
                                                            ?>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                </div>
                            </div>
                            <div class="panel panel-warning">
                                <div class="panel-heading text-center"><h3>移送</h3> </div>
                                <div class="panel-body">
                                    <div class="row" style="margin-top:2rem;">
                                        <div class=" col-md-12">
                                            <form class="form-horizontal">
                                                <?php 
                                                // var_dump($data);
                                                $filenameD = filter_by_value($data, 'f_belong', 'D');
                                                // var_dump($filename);
                                                
                                                for ($i=$start_year; $i <= $now_year ; $i++) { 
                                                    
                                                    echo '<div class="form-group col-md-6">
                                                            <label class="col-md-3 text-right">'.$i.'年'.(($i == 99 || $i == 109)?'<span class="text-danger">(略過)不用上傳</span>':'').'</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <input type="file" name="year_'.$i.'"  class="form-control" id="year_'.$i.'">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-default" type="button" data-year="'.$i.'" data-belong="D" onclick="uploadEvent($(this),\'ES\')">上傳</button>
                                                                    </span>
                                                                </div><!-- /input-group -->';
                                                        $matchD = false;
                                                        foreach($filenameD as $k=>$v){
                                                            if(array_search($i, $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                $matchD = true;
                                                            }
                                                        }            
                                                        if(!$matchD)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                    echo '</div>                                                
                                                        </div>';
                                                }

                                                
                                                ?>
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">99年再移送</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_ATRS109"  class="form-control" id="year_ATRS109">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button" data-year="ATRS109" data-belong="D" onclick="uploadEvent($(this),'ES')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php
                                                        $matchD = false;
                                                        foreach($filenameD as $k=>$v){
                                                            if(array_search('ATRS109', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                $matchD = true;
                                                            }
                                                        }            
                                                        if(!$matchD)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        } 
                                                        ?>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-danger">
                                <div class="panel-heading text-center"><h3>憑證</h3> </div>
                                <div class="panel-body">
                                    <div class="row" style="margin-top:2rem;">
                                        <div class=" col-md-12">
                                            <form class="form-horizontal">
                                            <?php 
                                                $filenameE = filter_by_value($data, 'f_belong', 'E');
                                                
                                            ?>
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">債權憑證-全-持續更新</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_110" class="form-control" id="year_110">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button" data-year="110" data-belong="E"
                                                                    onclick="uploadEvent($(this),'PC')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php 
                                                        $matchE = false;
                                                        foreach($filenameE as $k=>$v){
                                                            if(array_search('110', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                $matchE = true;
                                                            }
                                                        }            
                                                        if(!$matchE)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">110年憑證統計表_本機</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_109" class="form-control" id="year_109">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button" data-year="109" data-belong="E"
                                                                    onclick="uploadEvent($(this),'PC')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php 
                                                        $matchE = false;
                                                        foreach($filenameE as $k=>$v){
                                                            if(array_search('109', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                $matchE = true;
                                                            }
                                                        }            
                                                        if(!$matchE)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                        ?>
                                                    </div>
                                                </div>
												<div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">111年憑證統計表_本機</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_111" class="form-control" id="year_111">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button" data-year="111" data-belong="E"
                                                                    onclick="uploadEvent($(this),'PC')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php 
                                                        $matchE = false;
                                                        foreach($filenameE as $k=>$v){
                                                            if(array_search('111', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                $matchE = true;
                                                            }
                                                        }            
                                                        if(!$matchE)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                        ?>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading text-center"><h3>撤註銷</h3> </div>
                                <div class="panel-body">
                                    <div class="row" style="margin-top:2rem;">
                                        <div class=" col-md-12">
                                            <form class="form-horizontal">
                                                <?php 
                                                    $filenameF = filter_by_value($data, 'f_belong', 'F');
                                                ?>
                                                <div class="form-group col-md-6">
                                                    <label class="col-md-3 text-right">99to109撤銷清查</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <input type="file" name="year_110" class="form-control" id="year_110">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button" data-year="110" data-belong="F"
                                                                    onclick="uploadEvent($(this),'CS')">上傳</button>
                                                            </span>
                                                        </div><!-- /input-group -->
                                                        <?php 
                                                        $matchF = false;
                                                        foreach($filenameF as $k=>$v){
                                                            if(array_search('110', $v))
                                                            {
                                                                echo "<h5>
                                                                            <small>目前檔案：".$v['f_ori_filename'] ." (".$v['f_upload_date'].")</small>
                                                                        </h5>", PHP_EOL;
                                                                $matchF = true;
                                                            }
                                                        }            
                                                        if(!$matchF)
                                                        {
                                                            echo "<h5>
                                                                            <small>目前檔案：</small>
                                                                        </h5>", PHP_EOL;
                                                        }     
                                                        ?>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
            
        
    });
    function tranfer2ADyear(date)
    {
        if(date.length == 6)
        {
            ad = (parseInt(date.substr(0, 2))) + 1911;
            return ad.toString() + '-' + date.substr(2, 2) + '-' + date.substr(4, 2);
        }
        else if(date.length == 7)
        {
            ad = (parseInt(date.substr(0, 3))) + 1911;
            return ad.toString() + '-' + date.substr(3, 2) + '-' + date.substr(5, 2);
        }
        else
        {
            return '';
        }
    }
    function uploadEvent(obj, type){
        $('#loading').removeClass('hidden');
        $("#loadingTxt").text("資料上傳中...請稍候...請勿關掉或跳出畫面喔！電腦會壞掉!");
        let api = '#';
        switch (type) {
            case 'CF': // 裁罰
                api = '<?php echo base_url(); ?>Integration/uploadfiles';
                break;
            case 'CW': // 帳務
                api = '<?php echo base_url(); ?>Integration/uploadCWfiles';
                break;
            case 'ML': // 執行命令
                api = '<?php echo base_url(); ?>Integration/uploadMLfiles';
                break;
            case 'ES': // 移送
                api = '<?php echo base_url(); ?>Integration/uploadESfiles';
                break;
            case 'PC': // 憑證
                api = '<?php echo base_url(); ?>Integration/uploadPCfiles';
                break;
            case 'CS': // 撤註銷
                api = '<?php echo base_url(); ?>Integration/uploadCSfiles';
                break;
            default:
                api = '#';
                break;
        }
        let formdata = new FormData();
        formdata.append('f_year', obj.attr('data-year'));
        formdata.append('f_belong', obj.attr('data-belong'));
        formdata.append('upload_file', obj.parent().parent().find("input[name=year_"+obj.attr('data-year')+"]")[0].files[0]);
        $.ajax({            
            type: 'POST',
            url: api,
            data: formdata,
            // dataType: 'json',
            processData : false, 
            contentType: false,
            cache: false,
            error:function(){
                console.log('error')
            },
            success: function(resp){
                console.log(resp);
                
                if(resp.indexOf('ok') > -1)
                {
                  location.reload();
                }
            },
            complete:function(resp){
              console.log('complete')
              $('#loading').addClass('hidden');
              $("#uploadtxt").text("");
            }
        });
        
    }
    
    </script>
