        <style>
            .step {
                width: 250px;
                height: 50px;
                padding:5px;
                margin: 0 auto;
                border:1px solid #dfdfdf;
                border-radius: 10px;
                line-height: 40px;
                color: #c4c4c4;
                text-align: center;
                margin-bottom:20px;
            }
            .step.active {
                border:1px solid #FF254A;
                color: #FF254A;
            }
            .col-md-3 {
                height: 200px;
                margin-bottom: 40px;
            }
        </style>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <blockquote style="margin-top:35px;letter-spacing:5px;">
                        <p>檔案處理</p>
                    </blockquote>
                    <div class="row">
                        <div class="col-md-6">
                            <form id="process_one_form" method="post" onsubmit="process_one(event)">
                                <div class="row">
                                    
                                    <div class="col-md-12">
                                    <label>請勾選產生的年度區間</label> <br>
                                    <?php
                                        foreach ($DJ_data as $DJkey => $DJvalue) {
                                            echo '<label class="checkbox-inline">
                                            <input type="checkbox" id="'.$DJvalue['f_year'].'" name="fhgroup[]" value="'.$DJvalue['f_filename'].'"> '.$DJvalue['f_year'].'</label>';
                                        }
                                    ?>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-primary" type="submit" id="process_one_BT" value="產生年度怠金案件資料" />                           
                                    </div>
                                    <!-- /.col-lg-12 -->
                                    
                                </div>
                                <!-- /.row -->
                                <div class="row" style="margin-top:20px;">
                                    <div class="col-lg-12">
                                        <div class="progress progress-one">
                                            <div class="progress-bar progress-bar-one progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-1"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_one_msg"></div>
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form id="process_two_form" method="post" onsubmit="process_two(event)">
								<div class="row">
                                    <div class="col-md-12">
                                    <label>請勾選產生的年度區間</label> <br>
                                    <?php
                                        foreach ($ES_data as $ESkey => $ESvalue) {
                                            echo '<label class="checkbox-inline">
                                            <input type="checkbox" id="'.$ESvalue['f_year'].'" name="esgroup[]" value="'.$ESvalue['f_filename'].'"> '.$ESvalue['f_year'].'</label>';
                                        }
                                    ?>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-primary" type="submit" id="process_two_BT" value="產生TRSdj99to108" />                           
                                    </div>
                                    
                                    
                                </div>
                                
                                <div class="row" style="margin-top:20px;">
                                    <div class="col-lg-12">
                                        <div class="progress progress-two">
                                            <div class="progress-bar progress-bar-two progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-2"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_two_msg"></div>
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> 
                    <hr>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-info btn-lg btn-block" onclick="oneKeyProcess(event)">一鍵產生年度總表_怠金</button>
                            <span class="timer text-primary"></span>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="step step-1">
                                <span>Step1:讀取所有怠金案件</span>
                            </div>    
                            <form id="process_three_form" class="text-center" method="post" onsubmit="process_three(event)">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="progress progress-three">
                                            <div class="progress-bar progress-bar-three progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-3"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_three_msg"></div>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-default" type="submit" id="process_three_BT" value="Step1:讀取所有怠金案件" />                           
                                    </div>
                                    <!-- /.col-lg-12 -->
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3">
                            <div class="step step-2">
                                <span>Step2:讀取所有完納案件</span>
                            </div>    
                            <form id="process_four_form"  class="text-center" method="post" onsubmit="process_four(event)">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="progress progress-four">
                                            <div class="progress-bar progress-bar-four progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-4"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_four_msg"></div>
                                        
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-default" type="submit" id="process_four_BT" value="Step2:讀取所有完納案件" />                           
                                    </div>
                                    <!-- /.col-lg-12 -->
                                    
                                </div>
                                <!-- /.row -->
                            </form>
                        </div>
                        <!-- <div class="col-md-3">
                            <div class="step step-x">
                                <span>StepX:讀取所有分期完納LOG</span>
                            </div>    
                            <form id="process_five_form"  class="text-center" method="post" onsubmit="process_five(event)">                            
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="progress progress-five">
                                            <div class="progress-bar progress-bar-five progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-5"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_five_msg"></div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-default" type="submit" id="process_five_BT" value="StepX:讀取所有分期完納LOG" />                           
                                    </div>
                                    
                                </div>
                            </form>
                        </div> -->
                        <div class="col-md-3">
                            <div class="step step-3">
                                <span>Step3:讀取讀取所有分期資料</span>
                            </div>    
                            <form id="process_six_form"  class="text-center" method="post" onsubmit="process_six(event)">                            
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="progress progress-six">
                                            <div class="progress-bar progress-bar-six progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-6"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_six_msg"></div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-default" type="submit" id="process_six_BT" value="Step3:讀取讀取所有分期資料" />                           
                                    </div>
                                    <!-- /.col-lg-12 -->
                                    
                                </div>
                                <!-- /.row -->
                            </form>
                        </div>
                        <div class="col-md-3">
                            <div class="step step-4">
                                <span>Step4:讀取所有移送案號</span>
                            </div>    
                            <form id="process_seven_form" class="text-center" method="post" onsubmit="process_seven(event)">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="progress progress-seven">
                                            <div class="progress-bar progress-bar-seven progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-7"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_seven_msg"></div>
                                        
                                    </div>
                                </div>                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-default" type="submit" id="process_seven_BT" value="Step4:讀取所有移送案號" />                           
                                    </div>
                                    <!-- /.col-lg-12 -->
                                    
                                </div>
                                <!-- /.row -->
                            </form>
                        </div>
                        <div class="col-md-3">
                            <div class="step step-5">
                                <span>Step5:讀取憑證資料</span>
                            </div>    
                            <form id="process_eight_form" class="text-center" method="post" onsubmit="process_eight(event)">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="progress progress-eight">
                                            <div class="progress-bar progress-bar-eight progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-8"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_eight_msg"></div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-default" type="submit" id="process_eight_BT" value="Step5:讀取憑證資料" />                           
                                    </div>
                                    <!-- /.col-lg-12 -->
                                    
                                </div>
                                <!-- /.row -->
                            </form>
                        </div>
                        <div class="col-md-3">
                            <div class="step step-6">
                                <span>Step6:讀取撤銷及註銷資料</span>
                            </div>    
                            <form id="process_nine_form" class="text-center" method="post" onsubmit="process_nine(event)">
                                <div class="row" style="margin-top:20px;">
                                    <div class="col-lg-12">
                                        <div class="progress progress-nine">
                                            <div class="progress-bar progress-bar-nine progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-9"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_nine_msg"></div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-default" type="submit" id="process_nine_BT" value="Step6:讀取撤銷及註銷資料" />                           
                                    </div>
                                    <!-- /.col-lg-12 -->
                                    
                                </div>
                                <!-- /.row -->
                            </form>
                        </div>
                        <div class="col-md-3">
                            <div class="step step-7">
                                <span>Step7:讀取執行命令資料</span>
                            </div>    
                            <form id="process_ten_form" class="text-center" method="post" onsubmit="process_ten(event)">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="progress progress-ten">
                                            <div class="progress-bar progress-bar-ten progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-10"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_ten_msg"></div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-default" type="submit" id="process_ten_BT" value="Step7:讀取執行命令資料" />                           
                                    </div>
                                    <!-- /.col-lg-12 -->
                                    
                                </div>
                                <!-- /.row -->
                            </form>
                        </div>
                        <div class="col-md-3">
                            <div class="step step-8">
                                <span>產生99to109整合年度總表_怠金</span>
                            </div>    
                            <form id="process_eleven_form" class="text-center" method="post" onsubmit="process_eleven(event)">
                                <div class="row" style="margin-top:20px;">
                                    <div class="col-lg-12">
                                        <div class="progress progress-eleven">
                                            <div class="progress-bar progress-bar-eleven progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="progress-text progress-text-11"></span>    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="process_eleven_msg"></div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input class="btn btn-default" type="submit" id="process_eleven_BT" value="產生99to109整合年度總表_怠金" />                           
                                    </div>
                                    <!-- /.col-lg-12 -->
                                    
                                </div>
                                <!-- /.row -->
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
            window.onbeforeunload = function(e) {
                var dialogText = '確定關閉視窗嗎？';
                e.returnValue = dialogText;
                return dialogText;
            };
        });
    function process_one(event, stop=false)
    {
        event.preventDefault();
        let formdata = new FormData();
        let fhAry = $.map($('input[name="fhgroup[]"]:checked'), function(a) {
            return $(a).val();
        });

        formdata.append('years', JSON.stringify(fhAry));

        if(fhAry.length == 0)
            return false;

        $('#process_one_msg').html('');
        $('.progress-bar-one').css('width', '5%');
        $(".progress-text-1").text('5%');

        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/integratDJ",
                method: "POST",
                // data: $(this).serialize(),
                data: formdata,
                processData : false, 
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $('#process_one_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-one', ".progress-text-1", '#process_one_form', '#process_one_BT', '#process_one_msg');
                    }, 1000);

                    
                }
            })
    }
    function process_two(event, stop=false)
    {
        event.preventDefault();
        
        let formdata = new FormData();
        let esAry = $.map($('input[name="esgroup[]"]:checked'), function(a) {
            return $(a).val();
        });

        formdata.append('years', JSON.stringify(esAry));
        if(esAry.length == 0)
                    return false;

        $('#process_two_msg').html('');
        $('.progress-bar-two').css('width', '5%');
        $(".progress-text-2").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/integratES",
                method: "POST",
                // data: $(this).serialize(),
                data: formdata,
                processData : false, 
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $('#process_two_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-two', ".progress-text-2", '#process_two_form', '#process_two_BT', '#process_two_msg');
                    }, 1000);
                }
            })
    }
    function process_three(event, stop=false)
    {
        event.preventDefault();
        $('.step-1').addClass('active');
        $('#process_three_msg').html('');
        $('.progress-bar-three').css('width', '5%');
        $(".progress-text-3").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/readDJ",
                method: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    $('#process_three_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    $('.step-1').removeClass('active');
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-three', ".progress-text-3", '#process_three_form', '#process_three_BT', '#process_three_msg');
                    }, 1000);

                    if(stop)
                    {
                        process_four(event, stop);
                    }
                }
            })
    }
    function process_four(event, stop=false)
    {
        event.preventDefault();
        $('.step-2').addClass('active');
        $('#process_four_msg').html('');
        $('.progress-bar-four').css('width', '5%');
        $(".progress-text-4").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/readCW_done",
                method: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    $('#process_four_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    $('.step-2').removeClass('active');
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-four', ".progress-text-4", '#process_four_form', '#process_four_BT', '#process_four_msg');
                    }, 1000);

                    if(stop)
                    {
                        process_six(event, stop);
                    }
                }
            })
    }
    function process_five(event, stop=false)
    {
        event.preventDefault();
        $('.step-x').addClass('active');
        $('#process_five_msg').html('');
        $('.progress-bar-five').css('width', '5%');
        $(".progress-text-5").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>Integration/readCW_partdone",
                method: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    $('#process_five_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    $('.step-x').removeClass('active');
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-five', ".progress-text-5", '#process_five_form', '#process_five_BT', '#process_five_msg');
                    }, 1000);
                    if(stop)
                    {
                        process_six(event, stop);
                    }
                }
            })
    }
    function process_six(event, stop=false)
    {
        event.preventDefault();
        $('.step-3').addClass('active');
        $('#process_six_msg').html('');
        $('.progress-bar-six').css('width', '5%');
        $(".progress-text-6").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/readCW_part",
                method: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    $('#process_six_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    $('.step-3').removeClass('active');
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-six', ".progress-text-6", '#process_six_form', '#process_six_BT', '#process_six_msg');
                    }, 1000);

                    if(stop)
                    {
                        process_seven(event, stop);
                    }
                }
            })
    }
    function process_seven(event, stop=false)
    {
        event.preventDefault();
        $('.step-4').addClass('active');
        $('#process_seven_msg').html('');
        $('.progress-bar-seven').css('width', '5%');
        $(".progress-text-7").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/readES",
                method: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    $('#process_seven_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    $('.step-4').removeClass('active');
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-seven', ".progress-text-7", '#process_seven_form', '#process_seven_BT', '#process_seven_msg');
                    }, 1000);

                    if(stop)
                    {
                        process_eight(event, stop);
                    }
                }
            })
    }
    function process_eight(event, stop=false)
    {
        event.preventDefault();
        $('.step-5').addClass('active');
        $('#process_eight_msg').html('');
        $('.progress-bar-eight').css('width', '5%');
        $(".progress-text-8").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/readPC",
                method: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    $('#process_eight_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    $('.step-5').removeClass('active');
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-eight', ".progress-text-8", '#process_eight_form', '#process_eight_BT', '#process_eight_msg');
                    }, 1000);

                    if(stop)
                    {
                        process_nine(event, stop);
                    }
                }
            })
    }
    function process_nine(event, stop=false)
    {
        event.preventDefault();
        $('.step-6').addClass('active');
        $('#process_nine_msg').html('');
        $('.progress-bar-nine').css('width', '5%');
        $(".progress-text-9").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/readCS",
                method: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    $('#process_nine_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    $('.step-6').removeClass('active');
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-nine', ".progress-text-9", '#process_nine_form', '#process_nine_BT', '#process_nine_msg');
                    }, 1000);

                    if(stop)
                    {
                        process_ten(event, stop);
                    }
                }
            })
    }
    function process_ten(event, stop=false)
    {
        event.preventDefault();
        $('.step-7').addClass('active');
        $('#process_ten_msg').html('');
        $('.progress-bar-ten').css('width', '5%');
        $(".progress-text-10").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/readML",
                method: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    $('#process_ten_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    $('.step-7').removeClass('active');
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-ten', ".progress-text-10", '#process_ten_form', '#process_ten_BT', '#process_ten_msg');
                    }, 1000);
                    if(stop)
                    {
                        process_eleven(event, stop);
                    }
                }
            })
    }
    function process_eleven(event, stop=false)
    {
        event.preventDefault();
        $('.step-8').addClass('active');
        $('#process_eleven_msg').html('');
        $('.progress-bar-eleven').css('width', '5%');
        $(".progress-text-11").text('5%');
        $.ajax({
                url: "<?php echo base_url(); ?>IntegrationDJ/exportDJ",
                method: "POST",
                data: $(this).serialize(),
                beforeSend: function () {
                    $('#process_eleven_BT').attr('disabled', 'disabled');
                    // $('.progress').css('display', 'block');
                },
                success: function (data) {
                    $('.step-8').removeClass('active');
                    var percentage = 0;

                    var timer = setInterval(function () {
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, data, '.progress-bar-eleven', ".progress-text-11", '#process_eleven_form', '#process_eleven_BT', '#process_eleven_msg');
                    }, 1000);
                    if(stop)
                    {
                        clearInterval(dealtimer)
                    }
                }
            })
    }
    function oneKeyProcess(event)
    {
        
        dealTimer();
        process_three(event, true);
        // clearInterval(dealtimer)
    }
    function progress_bar_process(percentage, timer, filename, progress_bar, progress_text, process_form, process_BT, process_msg) {
    	$(progress_bar).css('width', percentage + '%');
        $(progress_text).text(percentage + '%');
    	if (percentage > 100) {
    		clearInterval(timer);
    		// $(process_form)[0].reset();
    		// $('.progress').css('display', 'none');
    		$(progress_bar).css('width', '0%');
    		$(process_BT).attr('disabled', false);
    		$(process_msg).html(`<strong>檔案已產生</strong> 檔案名稱：<a href="<?php echo base_url("/integradoc/DJ/"); ?>${filename}">${filename}</a>`);
    		// setTimeout(function () {
    		// 	$('#process_one_msg').html('');
    		// }, 5000);
    	}
    }
    function dealTimer()
    {
        min = 0;
        dealtimer = setInterval(function(){
            min += 1;

            var h = Math.floor(min / 3600);
            var m = Math.floor(Math.floor(min % 3600) / 60);
            var s = min % 60;

            $(".timer").text("處理時間：" + h + "小時" + m + "分" + s + "秒");
        }, 1000);
    }
    </script>
