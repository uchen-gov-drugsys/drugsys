        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">
        <style>
            strong { 
                font-weight: bold;
                color:black;
            }
        </style>
                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><a href><?php echo $title?></a></li>
                    <li><a href=<?php echo base_url('PDFcreate/fine_doc/'.$sp->fd_snum)?>><button style="padding:0px 0px;" class="btn btn-default" >下載處分書</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" form="updateready">修改</button></a></li>
                    <?php if($prev != NULL){?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='prev'>上一個</button></a></li>
                    <?php }else{ ?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">第一筆</button></a></li>
                    <?php } ?>
                    <?php if($next != NULL){?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='next'>下一個</button></a></li>
                    <?php }else{ ?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">最後一筆</button></a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('Disciplinary_c/listdp1_ready/'.$id)?>"><button style="padding:0px 0px;" class="btn btn-default" >返回列表</button></a></li>
                    <!--<li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" >送批</button></a></li> 先拿掉 怕使用者沒確認修改完直接送批-->
                </ul>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href= <?php echo base_url("login/logout") ?>><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
                <!-- /.navbar-static-side -->
                <div id="page-wrapper" style="margin-left:20px;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="page-header"></p>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                <!-- /.navbar-static-side -->
                    <form action="http://localhost/main2/disciplinary_c/updateready" id="updateready" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#fast" data-toggle="tab">快速檢視</a>
                                        </li>
                                        <li><a href="#home" data-toggle="tab">公文/在監/基資標籤</a>
                                        </li>
                                        <li><a href="#report" data-toggle="tab">毒報/尿報</a>
                                        </li>
                                        <li><a href="#fin" data-toggle="tab">開罰/發文</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div style="HEIGHT: 770px; BACKGROUND-COLOR: #FFFFFF; overflow-y:scroll;" class="tab-content">
                                        <div class="tab-pane fade in active" id="fast">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td colspan='2'> 毒品成分級別</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2'> <input type="text" id="drug" name="drug" class="form-control" value="<?php echo $drug_ingredient?>"/>
                                                        <input id="nextvalue" type="hidden" name="next" value=""> 
                                                        <input id="prevvalue" type="hidden" name="prev" value=""> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2'>尿液成分級別</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2'><?php echo form_input('sc_ingredient',$sc_ingredient, 'class="form-control"')?></td>
                                                </tr>
                                                <tr>
                                                    <td>處分書類型</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><select id="seltype" class="form-control">
                                                        <option selected>請選擇</option>
                                                        <option value="0">type1</option>
                                                        <option value="1">type2</option>
                                                        <option value="2">type3</option>
                                                    </select></td>
                                                    <td><select id="seltype2" class="form-control"></select>
                                                    <button type="button" class="sub2 btn btn-default">產生列句</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <textarea name="fd_dis_msg" id="messageSpan2" class="form-control"><?php echo $sp->fd_dis_msg?></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>第幾次裁罰</td>
                                                    <td>繳納金額</td>
                                                </tr>
                                                <tr>
                                                    <td><select id="seldate" class="form-control">
                                                        <option selected>請選擇</option>
                                                        <option value="0">第一次</option>
                                                        <option value="1">第二次</option>
                                                        <option value="2">第三次</option>
                                                        <option value="2">第四次</option>
                                                        <option value="2">第五次</option>
                                                        <option value="2">第六次</option>
                                                        <option value="2">第七次</option>
                                                        <option value="2">第七次</option>
                                                    </select></td>
                                                    <td><input class="form-control" id="pmoney" name="f_damount" value="<?php echo $fine->f_damount ?>"></td>
                                                </tr>
                                                <tr>
                                                    <td>所在監所</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php echo form_dropdown('s_prison',$opt,$susp->s_prison,'class="form-control"') ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="tab-pane fade" id="home">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>受處分人姓名</td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo form_input('s_name',$susp->s_name, 'class="form-control"')?></td>
                                                </tr>
                                                <tr>
                                                    <td>出生日期</td>
                                                </tr>
                                                <tr>
                                                    <td><input name="s_birth" type="date" class="form-control" value="<?php echo $susp->s_birth?>"></td>
                                                </tr>
                                                <tr>
                                                    <td>身份證字號</td>
                                                </tr>
                                                <tr>
                                                    <?php echo form_hidden('s_num',$susp->s_num)?>
                                                    <?php echo form_hidden('s_cnum',$susp->s_cnum)?>
                                                    <?php echo form_hidden('s_dp_project',$susp->s_dp_project)?>
                                                    <td><?php echo form_input('s_ic',$susp->s_ic, 'class="form-control"')?></td>
                                                </tr>
                                                <tr>
                                                    <td>聯絡電話</td>
                                                </tr>
                                                <tr>
                                                    <td><?php
                                                        if(isset($phone)) {
                                                            echo form_input('p_no',$phone->p_no, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_input('p_no','', 'class="form-control"');
                                                        }
                                                    ?></td>
                                                </tr>
                                                <tr>
                                                    <td>發文地址</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <input class="form-control" value="<?php echo $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress ?>">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="tab-pane fade" id="report">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>物品</td>
                                                    <td>數量</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php echo form_dropdown('e_name',$opt1,$drug->e_name,'class="form-control" id=sel') ?>
                                                    </td>
                                                    <td><?php echo form_input('e_count',$drug->e_count, 'class="form-control"')?></td>
                                                </tr>
                                                <tr>
                                                    <td>單位</td>
                                                    <td>級數、成分</td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo form_dropdown('e_type',$opt2,$drug->e_type,'class="form-control" id="sel3"') ?></td>
                                                    <td><input type="text" id="drug" name="drug" class="form-control" value="<?php echo $drug_ingredient?>"/></td>
                                                </tr>
                                                <tr>
                                                    <td>樣態1</td>
                                                    <td>樣態2</td>
                                                </tr>
                                                <tr>
                                                    <td><select class="form-control" id="sel1" name="sel1"> 
                                                          <option value="沾">沾</option>
                                                          <option value="含">含</option>
                                                          <option value=" ">沒有</option>
                                                        </select></td>
                                                    <td><select id="sel2" name="sel2" class="form-control"> 
                                                        <option value="(採集自車內)">採集自車內</option>
                                                        <option value="(採集自隨身包包內)">採集自隨身包包內</option>
                                                    </select></td>
                                                </tr>
                                                <tr>
                                                    <td>描述文字</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <textarea name="fd_drug_message" id="messageSpan" class="form-control"><?php echo $sp->fd_drug_msg ?> </textarea>
                                                        <button id='sub' type="button" class="btn btn-default">重新產生</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="tab-pane fade" id="fin">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td colspan='2'>尿液成分級別</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2'><?php echo form_input('sc_ingredient',$sc_ingredient, 'class="form-control"')?></td>
                                                </tr>
                                                <tr>
                                                    <td>處分書類型</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><select id="seltypef" class="form-control">
                                                        <option selected>請選擇</option>
                                                        <option value="0">type1</option>
                                                        <option value="1">type2</option>
                                                        <option value="2">type3</option>
                                                    </select></td>
                                                    <td><select id="seltypef2" class="form-control"></select>
                                                    <button type="button" class="sub2f btn btn-default">產生列句</button></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <textarea name="fd_dis_msgf" id="messageSpan2f" class="form-control"><?php echo $sp->fd_dis_msg?></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>是否要罰</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <input type="radio" name="fd_wantdis" id="optionsRadios1" value="是" checked>是
                                                        <input type="radio" name="fd_wantdis" id="optionsRadios2" value="否">否
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <div class="col-lg-8">
                            <div class="panel pa8nel-default">
                                <div class="panel-body" style="HEIGHT: 770px; BACKGROUND-COLOR: #FFFFFF; overflow-y:scroll;">
                                    <p>
                                        列管編號：<?php echo $sp->fd_num?> 郵寄 以稿代簽 限制開放
                                        第二層決行 檔號：<?php echo date('Y')-1911 ?>/07270399 保存年限：3年<strong></strong>
                                    </p>
                                    <p>
                                        校對： &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                        監印： &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                        發文： &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                    </p>
                                    <p>
                                        正本：<?php echo $sp->fd_target?>君 <?php echo $sp->fd_zipcode ?>&nbsp<?php echo $sp->fd_address ?>
                                    </p>
                                    <p>
                                        副本：我是測試系統<?php echo $susp->s_roffice?>、我是測試系統刑事警察大隊、臺北市政府毒品危害防制中心</strong>
                                    </p>
                                    <table width="890" cellspacing="0" cellpadding="0" border="1">
                                        <tbody>
                                            <tr>
                                                <td colspan="8" width="890">
                                                    <p align="center">
                                                        我是測試系統 違反毒品危害防制條例案件處分書(稿)
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="890">
                                                    <p>
                                                        發文日期字號：<?php echo (date('Y')-1911).'年'?> <?php echo date('m', strtotime($sp->fd_date)).'月' ?> <?php echo date('d', strtotime($sp->fd_date)).'日' ?>北市警刑毒緝字第<?php echo$sp->fd_send_num?>號
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" width="890">
                                                    <p>
                                                        依據：我是測試系統<?php echo $susp->s_roffice?><?php echo (date('Y', strtotime($sp->fd_date))-1911) .'年'?> <?php echo date('m', strtotime($sp->fd_date)).'月' . date('d', strtotime($sp->fd_date)) ?>日北市警<?php echo $susp->s_roffice1 ?>分刑字第<?php echo $susp->s_fno?>號
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="128">
                                                    <p align="center">
                                                        受處分人
                                                    </p>
                                                </td>
                                                <td colspan="2" width="122">
                                                    <p align="center">
                                                        姓名
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong><?php echo $susp->s_name?></strong>
                                                    </p>
                                                </td>
                                                <td width="49">
                                                    <p align="center">
                                                        性別
                                                    </p>
                                                </td>
                                                <td width="50">
                                                    <p>
                                                       <strong> <?php echo $susp->s_gender?></strong>
                                                    </p>
                                                </td>
                                                <td width="70" valign="top">
                                                    <p>
                                                        出生
                                                    </p>
                                                    <p>
                                                        年月日
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <p>
                                                        <strong>民國<?php echo (date('Y', strtotime($susp->s_birth))-1911) .'年'?>
                                                        <?php echo (date('m', strtotime($susp->s_birth))) .'月'?>
                                                        <?php echo (date('d', strtotime($susp->s_birth))) .'日'?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="122" valign="top">
                                                    <p>
                                                        身分證
                                                    </p>
                                                    <p>
                                                        統一編號
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                        <strong><?php echo $susp->s_ic?></strong>
                                                    </p>
                                                </td>
                                                <td colspan="3" width="169" valign="top">
                                                    <p>
                                                        其他足資辨別之
                                                    </p>
                                                    <p>
                                                        特徵及聯絡電話
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <p>
                                                        <strong><?php 
                                                            if(isset($phone)) {
                                                                echo '0'.$phone->p_no;
                                                            }else {}?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">
                                                    <p align="center">
                                                        地址
                                                    </p>
                                                </td>
                                                <td width="70">
                                                    <p>
                                                        現住地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                        <strong><?php echo $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress ?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="70">
                                                    <p>
                                                        戶籍地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                        <strong><?php echo $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress ?></strong>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="4" width="128">
                                                    <p align="center">
                                                        法　定
                                                    </p>
                                                    <p align="center">
                                                        代理人
                                                    </p>
                                                </td>
                                                <td colspan="2" width="122">
                                                    <p align="center">
                                                        姓名
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                    </p>
                                                </td>
                                                <td width="49">
                                                    <p align="center">
                                                        性別
                                                    </p>
                                                </td>
                                                <td width="50">
                                                    <p align="center">
                                                    </p>
                                                </td>
                                                <td width="70" valign="top">
                                                    <p>
                                                        出生
                                                    </p>
                                                    <p>
                                                        年月日
                                                    </p>
                                                </td>
                                                <td width="330">
                                                    <p>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="122" valign="top">
                                                    <p>
                                                        身分證
                                                    </p>
                                                    <p>
                                                        統一編號
                                                    </p>
                                                </td>
                                                <td width="141">
                                                    <p>
                                                    </p>
                                                </td>
                                                <td colspan="3" width="169" valign="top">
                                                    <p>
                                                        其他足資辨別之
                                                    </p>
                                                    <p>
                                                        特徵及聯絡電話
                                                    </p>
                                                </td>
                                                <td width="330">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" width="52">
                                                    <p align="center">
                                                        地址
                                                    </p>
                                                </td>
                                                <td width="70">
                                                    <p>
                                                        現住地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="70">
                                                    <p>
                                                        戶籍地
                                                    </p>
                                                </td>
                                                <td colspan="5" width="640">
                                                    <p>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        主旨
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
                                                        受處分人處罰：
                                                    </p>
                                                    <p>
                                                        一、新臺幣<strong><?php echo ($fine->f_damount/10000) ?></strong>萬元整
                                                        </p>
                                                    <p>
                                                        二、毒品危害 <strong>6 </strong>小時。(講習時間詳見下方「毒品講習」欄位)
                                                    </p>
                                                    <p>
                                                        三、<strong><?php echo $drug2->ddc_level.'級'.'『'.$drug2->ddc_ingredient.'』'.$drug->e_name.$drug->e_count.$drug->e_type?></strong>沒入。
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        事實
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762" height="100px">
                                                <?php if(!isset($sp->fd_fact_text)||($sp->fd_fact_text=null)||($sp->fd_fact_text=' ')){?>
                                                    <textarea name="fd_fact_text" style="height:100px" class="form-control">受處分人<?php echo $susp->s_name.$cases->s_date?>許，在本市<?php echo $cases->r_county.$cases->r_district.$cases->r_address ?>，為<?php echo $cases->s_office ?>員警查獲持有<?php echo $sp->fd_drug_msg?>，經採集毒品及尿液檢體送專業單位鑑驗，均呈第<?php echo $drug2->ddc_level?>級毒品「<?php echo $drug2->ddc_ingredient?>」反應，有鑑驗報告書為證，違反毒品危害防制條例足堪確認。<?php echo $sp->fd_dis_msg ?></textarea>
                                                <?php }else{?>
                                                    <textarea name="fd_fact_text" style="height:100px" class="form-control"><?php echo $sp->fd_fact_text?></textarea>     
                                                <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        理由及
                                                    </p>
                                                    <p align="center">
                                                        法令依據
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
                                                        ■
                                                        依據毒品危害防制條例第十一條之一第二項：「無正當理由持有或施用第三級或第四級毒品者，處新臺幣一萬元以上五萬元以下罰鍰，並應限期令接受四小時以上八小時以下之毒品危害講習。」
                                                    </p>
                                                    <p>
                                                        ■
                                                        依據毒品危害防制條例第十八條第一項後段：「查獲之第三、四級毒品及製造或施用毒品之器具，無正當理由而擅自持有者，均沒入銷燬之。」
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        繳納期限及方式
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
                                                        一、罰鍰限於<strong>民國<?php echo (date('Y', strtotime($fine->f_date))-1911) .'年'?><?php echo (date('m', strtotime($fine->f_date))) .'月'?>
                                                        <?php echo (date('d', strtotime($fine->f_date))) .'日'?></strong>前選擇下列方式之一繳款：
                                                    </p>
                                                    <p>
                                                        (一)以自動化設備(ATM、網路ATM、網路銀行)匯款至「虛擬帳號」(限本處分書)。
                                                    </p>
                                                    <p>
                                                        (二)至金融機構臨櫃繳款至「臨櫃帳戶」。
                                                    </p>
                                                    <p>
                                                        二、自動化設備匯款方式：(一)選擇【繳費】服務 (二)輸入轉入銀行代號：<strong>012</strong>
                                                        (三)輸入本案虛擬帳號：<strong><?php echo $fine->f_BVC?></strong>
                                                    </p>
                                                    <p>
                                                        三、臨櫃繳款方式：行庫：<strong>台北富邦銀行公庫處，</strong>帳號:<strong> 16112470361019</strong><strong>，</strong>戶名:<strong>我是測試系統刑事警察大隊</strong>，並於<strong><u>備註(附言)欄位</u>填寫受處分人「姓名、</strong>                    <strong>身分證字號、電話</strong><strong>」</strong>，俾利辦理銷案。
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        <strong>毒品講習</strong>
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
                                                        一、<strong>可選擇參與實體講習或線上課程(每年可參與三次，惟應以申請不同套裝課程)。</strong>
                                                    </p>
                                                    <p>
                                                        <strong>二、講習時間：</strong>
                                                        <strong>民國<?php echo (date('Y', strtotime($sp->fd_lec_date))-1911) .'年'?><?php echo (date('m', strtotime($sp->fd_lec_date))) .'月'?>
                                                        <?php echo (date('d', strtotime($sp->fd_lec_date))) .'日'?></strong>
                                                        <strong><?php echo substr($sp->fd_lec_time,0,5) ?>分（請攜帶有相片之證件報到，逾時將無法入場），地點：<?php echo $sp->fd_lec_place ?></strong>
                                                        。如要選擇線上課程，請至臺北毒防官網https://nodrug.gov.taipei，點選：熱門服務\三四級毒品裁罰講習\線上講習，詳閱操作畫面等相關資料，依指示完成線上課程。有關講習方面問題請詢問臺北市政府毒品危害防制中心，電話02-23754068。
                                                    </p>
                                                    <p>
                                                        三、不依規定參加講習時，依行政執行法第三十條規定，依其情節輕重處新臺幣五千元以上三十萬元以下怠金。
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="128">
                                                    <p align="center">
                                                        注意事項
                                                    </p>
                                                </td>
                                                <td colspan="7" width="762">
                                                    <p>
                                                        一、對本處分（裁處）書如有不服，自本件處分（裁處）書達到之次日起30日內，書寫訴願書，以正本向本局（地址：10042臺北市中正區延平南路96號）遞送，並將副本抄送本府法務局（地址：臺北市市府路1號8樓東北區）。
                                                    </p>
                                                    <p>
                                                        二、承辦人：«承辦人»、電話：(02）2393-2397。
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p>
                                        承辦單位 (2539) 核稿 決行
                                    </p>            
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div>
                    </form>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <script>
            $("#next").click(function (){
                $("#nextvalue").val('<?php echo $next ?>');
               // alert($("#nextvalue").val());
                $("#updateready").submit();
            });
            $("#prev").click(function (){
                $("#prevvalue").val('<?php echo $prev ?>');
                //alert($("#prevvalue").val());
                $("#updateready").submit();
            });
            $('#sub').click(function (){
                
                var arr =[];
                var level =[];
                var drugn =[];
                var all =[];
                var drug =$('#drug').val().split(';');
                for(let i=0;i<drug.length;i++){
                    level[i]=drug[i].substr(0,1);
                    drugn[i]=drug[i].substr(2);
                    all[i]="第"+level[i]+"級毒品「"+drugn[i]+"」";
                    console.log(all[i]);
                }
                $sel=$('#sel').val();
                $sel1=$('#sel1').val();
                $sel2=$('#sel2').val();
                $sel3=$('#sel3').val();
                $("input[name='e_count']").each(function(){
                    arr.push($(this).val());
                })
                if(drug.length>1||drug.length<3){
                    $("#messageSpan").val($sel1+all[0]+"及"+all[1]+$sel+arr[0]+$sel3+$sel2);
                }
                //console.log(arr,$sel,$sel2);
                
            });
            $("#seldate").change(function(){
                $("#seldatef").val($("#seldate").val());
            switch (parseInt($(this).val())){
                case 0: 
                $("#pmoney value").remove();
                $("#pmoney").val("20000");
                break;
                case 1: 
                $("#pmoney value").remove();
                $("#pmoney").val("30000");
                break;
                case 2: 
                $("#pmoney value").remove();
                $("#pmoney").val("50000");
                break;
            }});
            $('.sub2').click(function (){
                $seltype2=$('#seltype2').val();
                $("#messageSpan2").val($seltype2);
                //console.log($seltype2);
                
            });
            $("#seltype").change(function(){
            switch (parseInt($(this).val())){
                case 0: 
                $("#seltypef").val($("#seltype").val());
                $("#seltype2 option").remove();
                $("#seltype2").append($("<option value='原本局108年4月10日北市警刑毒緝第1-83--45411號處分書作廢(列管編號:1081106)"
                                    + "'>上年度處分書至本年度重發</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>單純不起訴處分</option>"
                                    ));
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='原本局108年4月10日北市警刑毒緝第1-83--45411號處分書作廢(列管編號:1081106)"
                                    + "'>上年度處分書至本年度重發</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>單純不起訴處分</option>"
                                    ));
                break;
                case 1: 
                $("#seltypef").val($("#seltype").val());
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署處分緩起訴2年，且應至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>臺北地檢緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。經臺灣士林地方檢察署處分緩起訴2年，且應至臺灣士林地方檢察署指定醫院於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>士林緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>已經勒戒</option>"
                                    ));
                $("#seltype2 option").remove();
                $("#seltype2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署處分緩起訴2年，且應至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>臺北地檢緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。經臺灣士林地方檢察署處分緩起訴2年，且應至臺灣士林地方檢察署指定醫院於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>士林緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>已經勒戒</option>"
                                    ));
                break;
                case 2: 
                $("#seltypef").val($("#seltype").val());
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署刑事簡易判決處有期徒刑貳月，併此敘明。"
                                    + "'>刑事簡易判決</option>"
                                    +"<option value='受處分人另涉及第一級毒品部分。業經臺灣臺北地方檢察署刑事判決處有期徒刑七月。"
                                    + "'>刑事判決</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，由親屬主動向員警報案發現犯行，故不予處罰。"
                                    + "'>親屬不罰</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，主動向員警報案坦承犯行，故不予處罰。"
                                    + "'>自首</option>"
                                    ));
                $("#seltype2 option").remove();
                $("#seltype2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署刑事簡易判決處有期徒刑貳月，併此敘明。"
                                    + "'>刑事簡易判決</option>"
                                    +"<option value='受處分人另涉及第一級毒品部分。業經臺灣臺北地方檢察署刑事判決處有期徒刑七月。"
                                    + "'>刑事判決</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，由親屬主動向員警報案發現犯行，故不予處罰。"
                                    + "'>親屬不罰</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，主動向員警報案坦承犯行，故不予處罰。"
                                    + "'>自首</option>"
                                    ));
                break;
            }   
            });
            $("#seldatef").change(function(){
            switch (parseInt($(this).val())){
                case 0: 
                $("#pmoneyf value").remove();
                $("#pmoneyf").val("20000");
                break;
                case 1: 
                $("#pmoneyf value").remove();
                $("#pmoneyf").val("30000");
                break;
                case 2: 
                $("#pmoneyf value").remove();
                $("#pmoneyf").val("50000");
                break;
            }});
            $('.sub2f').click(function (){
                $seltypef2=$('#seltypef2').val();
                $("#messageSpan2f").val($seltypef2);
                //console.log($seltype2);
                
            });
            $("#seltypef").change(function(){
            switch (parseInt($(this).val())){
                case 0: 
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='原本局108年4月10日北市警刑毒緝第1-83--45411號處分書作廢(列管編號:1081106)"
                                    + "'>上年度處分書至本年度重發</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>單純不起訴處分</option>"
                                    ));
                break;
                case 1: 
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署處分緩起訴2年，且應至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>臺北地檢緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。經臺灣士林地方檢察署處分緩起訴2年，且應至臺灣士林地方檢察署指定醫院於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>士林緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>已經勒戒</option>"
                                    ));
                break;
                case 2: 
                $("#seltypef2 option").remove();
                $("#seltypef2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署刑事簡易判決處有期徒刑貳月，併此敘明。"
                                    + "'>刑事簡易判決</option>"
                                    +"<option value='受處分人另涉及第一級毒品部分。業經臺灣臺北地方檢察署刑事判決處有期徒刑七月。"
                                    + "'>刑事判決</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，由親屬主動向員警報案發現犯行，故不予處罰。"
                                    + "'>親屬不罰</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，主動向員警報案坦承犯行，故不予處罰。"
                                    + "'>自首</option>"
                                    ));
                break;
            }   
            });
            $("#zipcode").twzipcode({
            "zipcodeIntoDistrict": true,
            "css": ["city form-control", "town form-control"],
            "countyName": "city", // 指定城市 select name
            "districtName": "town" // 指定地區 select name
            });         
            $("#zipcode2").twzipcode({
            "zipcodeIntoDistrict": true,
            "css": ["city form-control", "town form-control"],
            "countyName": "city", // 指定城市 select name
            "districtName": "town" // 指定地區 select name
            });  
            $(function() {  
               $('a.media').media({width:1200, height:700});  
            });  
        </script>

    </body>
</html>
