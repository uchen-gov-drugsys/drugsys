        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
            <?php echo form_open_multipart('disciplinary_c/uploaddpdate','id="uploaddpdate"') ?>
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                            <div class="panel panel-default">
                                <!--div class="panel-heading list_view">
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 案件編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 人員編號
                                    <input type="checkbox" name="list" data-target ="3" checked> 姓名
                                    <input type="checkbox" name="list" data-target ="4" checked> 證號
                                    <input type="checkbox" name="list" data-target ="5" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="6" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="7" checked> 犯罪手法
                                    <input type="checkbox" name="list" data-target ="8" checked> 毒品號
                                    <input type="checkbox" name="list" data-target ="9" checked> 成分
                                    <input type="checkbox" name="list" data-target ="10" checked> 級數
                                    <input type="checkbox" name="list" data-target ="11" checked> 淨重
                                    <input type="checkbox" name="list" data-target ="12" checked> 純質淨重
                                    <input type="checkbox" name="list" data-target ="13" checked> 建議金額
                                </div-->
                            <form action="http://localhost/main2/" id="sp_checkbox" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                                <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                                <input id="s_cnum1" type="hidden" name="s_cnum1" value=''> 
                                <input id="dp_status" type="hidden" name="dp_status" value='<?php echo$dp_status?>'> 
                                <input id="dp_num" type="hidden" name="dp_num" value='<?php echo$dp_num?>'> 
                           </div>
                            <!-- /.panel -->
                        </div>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">輸入發文日期</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>輸入發文日期</label>
                                        <input name="dp_send_date" type="date" class="form-control">                                                            
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button id='no' class="btn btn-default" >確認修改</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
            </form>
                <?php echo form_open_multipart('disciplinary_c/uploadpublicdoc') ?>
                            <div class="modal fade" id="uploadlfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">上傳公文函(第一頁)</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-group">
                                        Type Folder Name:<input type="text" name="foldername" /><br/><br/>
                                        <input id="id" type="hidden" name="id" value='<?php echo$id?>'> 
                                        Select Folder to Upload: <input type="file" name="files[]" id="files" multiple directory="" webkitdirectory="" moxdirectory="" /><br/><br/>                                      </div>
                                    <input type="Submit" value="Upload" name="upload" />
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button  class="btn btn-default" >確認上傳</button>
                                  </div>
                                </div>
                              </div>
                            </div> 
            </form>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
                $( "#uploaddpdate" ).validate({
                    rules: {
                        dp_send_date: {
                            required: true,
                        },
                    },
                    messages: {
                        dp_send_date: {
                            required: "此欄位不得為空",
                        },
                    }
                });        
           var table = $('#table1').DataTable({
               "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    },
                ]
           });
            $("#edit").click(function (){
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);     
                //alert($('#s_cnum1').val());
                window.location.href = '<?php echo base_url("disciplinary_c/editSanc/") ?>' +$('#s_cnum1').val();
                //$("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);     
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);
                //alert($('#s_cnum1').val());
            e.preventDefault();
           });
    });
    </script>
