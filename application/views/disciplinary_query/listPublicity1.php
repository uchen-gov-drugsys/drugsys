        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
            <form action="http://localhost/main2/disciplinary_c/uploaddp_doc" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                </form>
                <div class="container-fluid"> 
                    <div class="row">
                        <div class="col-lg-11">
                            <br>
                        <?php echo form_open_multipart('disciplinary_c/updatepublicproject','id="sp_checkbox"') ?>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                        <input id="s_cnum1" type='hidden' name="s_cnum1" value=''> 
                                    </div>                       
                                </div>
                           </div>
                            <!-- /.panel -->
                        </div>
                            </form>
                        <!-- /.col-lg-12 -->
                    </div>
                   </form> <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': [0],
                    'checkboxes': {
                       'selectRow': false
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              'order': [[1, 'asc']]
           });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                $("#sp_checkbox").submit();
            });

           // Handle form submission event
        $('#sp_checkbox').on('submit', function(e){
            var form = $(this);
            var url = form.attr('action');
            
            var rows_selected = table.column(0).checkboxes.selected();
                $('#s_cnum').val(rows_selected.join(","));
                $('input[name="id\[\]"]', form).remove();
            var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows.join(","));
            //e.preventDefault();
           });
    });
    </script>
