        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 

                        if (!$this -> session -> userdata('uic') && $this->uri->segment(1) != 'login' && $this->uri->segment(1) != '' && $this->uri->segment(2) != ''){
                            $this->output
                                    ->set_status_header(403)
                                    ->set_content_type('text/html')
                                    ->set_output(file_get_contents( $this->load->view('403')))
                                    ->_display();
                            redirect('login/index','refresh');
                            exit;
                        }
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><a href="#"><?php echo $title?></a></li>
                    <!--li><a href="#"><button style="padding:0px 0px;" id='odoc' class="btn btn-default" >公文(函)</button></a></li-->
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='result'>監所查詢結果</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='drugdoc'>毒報</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='suspdoc'>尿報</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='casedoc'>筆錄/搜扣/戶役政</button></a></li>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='judoc'>司法文書(判決書/不起訴處分書/緩起訴/他縣市裁罰書)</button></a></li>
                    <?php if($prev != NULL){?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='prev'>上一個</button></a></li>
                    <?php }else{ ?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">第一筆</button></a></li>
                    <?php } ?>
                    <?php if($next != NULL){?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" id='next'>下一個</button></a></li>
                    <?php }else{ ?>
                        <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default">最後一筆</button></a></li>
                    <?php } ?>
                    <li><a href="#"><button style="padding:0px 0px;" class="btn btn-default" form="updateSanc">存儲</button></a></li>
                </ul>
                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <?php echo $user?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="<?php echo base_url("login/logout") ?>"><i class="fa fa-sign-out fa-fw"></i> 登出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
                <!-- /.navbar-static-side -->
            <div id="page-wrapper" style="margin-left:20px;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="page-header"></p>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <div class="row">
                        <form action="http://localhost/main2/disciplinary_c/updateSanc" id="updateSanc" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#gongwen" data-toggle="tab">公文</a></li>
                                        <li><a href="#zaijian" data-toggle="tab">基本資料/在監</a></li>
                                        <li><a href="#fadai" data-toggle="tab">法代人(未年滿20歲開啟)</a></li>
                                        <li><a href="#report" data-toggle="tab">毒報/尿報</a></li>
                                        <li><a href="#fin" data-toggle="tab">開罰</a></li>
                                        <li><a href="#fawen" data-toggle="tab">發文</a></li>
                                        <li><a href="#jiang" data-toggle="tab">講習</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div  class="tab-content">
                                        <div class="tab-pane fade in active" id="gongwen">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>處分書編號</td>
                                                    <td>發文日</td>
                                                    <td>發文字號</td>
                                                    <td>移送分局</td>
                                                    <td>依據單位(字)</td>
                                                    <td>依據年月日</td>
                                                    <td>依據字號</td>
                                                </tr>
                                                <tr>
                                                    <td><?php
                                                        if(isset($sp)) {
                                                            echo form_input('fd_num',$sp->fd_num, 'class="form-control" readonly');
                                                        }
                                                        else {
                                                            echo form_input('fd_num',$fd_num, 'class="form-control" readonly');
                                                        }
                                                    ?></td>
                                                    <td><input name="fd_date" type="date" class="form-control" value="<?php if(isset($sp))echo $sp->fd_date;
                                                        else echo '';
                                                    ?>"></td>
                                                    <td><?php
                                                        if(isset($sp)) {
                                                            echo form_input('fd_send_num',$sp->fd_send_num, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_input('fd_send_num','', 'class="form-control"');
                                                        }
                                                    ?></td>
                                                    <td><?php echo form_input('s_roffice',$cases->r_office, 'class="form-control" readonly')?></td>
                                                    <td><?php echo form_input('s_roffice1',$cases->r_office1, 'class="form-control" readonly')?></td>
                                                    <td><input name="s_fdate" type="date" class="form-control" value="<?php echo $susp->s_fdate?>"></td>
                                                    <td>
                                                        <?php echo form_input('s_fno',$susp->s_fno, 'class="form-control"')?>
                                                        <input id="nextvalue" type="hidden" name="next" value=""> 
                                                        <input id="prevvalue" type="hidden" name="prev" value=""> 
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="tab-pane fade" id="zaijian">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>受處分人姓名</td>
                                                    <td>出生日期</td>
                                                    <td>身份證字號</td>
                                                    <td>聯絡電話</td>
                                                    <td colspan="2">
                                                        <div class="radio">
                                                            <input id="now" name="ad" type="radio" value="<?php echo $susp->s_rpcounty.$susp->s_rpdistrict.$susp->s_rpaddress ?>">現居地地址(勾選是否送達)
                                                        </div>
                                                    </td>
                                                    <td colspan="2">
                                                        <div class="radio">
                                                        <label>
                                                            <input id="orgin" name="ad" type="radio" checked value=<?php echo $susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress ?>">戶籍地址(勾選是否送達)
                                                        </label>
                                                        </div>
                                                    </td>
                                                    <!-- <td colspan="2">
                                                        <div class="radio">
                                                        <label>
                                                            <input id="prison" name="ad" type="radio" value="">所在監所(勾選是否送達)
                                                        </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        監所查詢結果上傳
                                                    </td>-->
                                                </tr>
                                                <tr>
                                                    <td><?php echo form_input('s_name',$susp->s_name, 'class="form-control"')?></td>
                                                    <td><input id="s_birth" name="s_birth" type="date" class="form-control" value="<?php echo $susp->s_birth?>"></td>
                                                    <?php echo form_hidden('s_num',$susp->s_num)?>
                                                    <?php echo form_hidden('s_cnum',$susp->s_cnum)?>
                                                    <?php echo form_hidden('s_dp_project',$susp->s_dp_project)?>
                                                    <td><?php echo form_input('s_ic',$susp->s_ic, 'class="form-control"')?></td>
                                                    <td><?php
                                                        if(isset($phone)) {
                                                            echo form_input('p_no',$phone->p_no, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_input('p_no','', 'class="form-control"');
                                                        }
                                                    ?></td>
                                                    <td colspan="2">
                                                        <div id="zipcode"></div>
                                                        <input class="form-control" placeholder="e.g.XX街/路XX號" value="<?php echo$susp->s_rpaddress ?>">
                                                    </td>
                                                    <td colspan="2">
                                                        <div id="zipcode2"></div>
                                                        <input class="form-control" placeholder="e.g.XX街/路XX號" value="<?php echo$susp->s_dpaddress ?>">
                                                    </td>
                                                    <td colspan="2">
                                                        <?php echo form_dropdown('s_prison',$opt,$susp->s_prison,'class="form-control"') ?>
                                                    </td>
                                                    <td>
                                                        <?php if($susp->s_prison_doc == null){ echo form_upload('s_prison_doc');}
                                                            else{
                                                                echo '已上傳';
                                                            }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="tab-pane fade" id="fadai">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>姓名</td>
                                                    <td>性別</td>
                                                    <td>身份證字號</td>
                                                    <td>出生日期</td>
                                                    <td>聯絡電話</td>
                                                    <td>地址:</td>
                                                </tr>
                                                <tr>
                                                    <td><?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_name',$suspfadai->s_fadai_name, 'class="form-control" id="s_fadai_name"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_name','', 'class="form-control" id="s_fadai_name"');
                                                        }?>
                                                    </td>
                                                    <td><?php 
                                                        $options = array(
                                                            '男'=> '男',
                                                            '女'=> '女',
                                                        );
                                                        if(isset($suspfadai)) {
                                                            echo form_dropdown('s_fadai_gender',$options,$suspfadai->s_fadai_gender, 'class="form-control" id="s_fadai_gender"');
                                                        }
                                                             else {
                                                                 echo form_dropdown('s_fadai_gender',$options,'', 'class="form-control" id="s_fadai_gender"');
                                                        }?>
                                                    </td>
                                                    <td><?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_ic',$suspfadai->s_fadai_ic, 'class="form-control" id="s_fadai_ic"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_ic','', 'class="form-control" id="s_fadai_ic"');
                                                        }?>
                                                    </td>
                                                    <td><input id="s_fadai_birth" name="s_fadai_birth" type="date" class="form-control" value="<?php if(isset($suspfadai)){ echo $suspfadai->s_fadai_birth;} 
                                                    else echo "";
                                                    ?>"></td>
                                                    <td><?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_phone',$suspfadai->s_fadai_phone, 'class="form-control" id="s_fadai_phone"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_phone','', 'class="form-control" id="s_fadai_phone"');
                                                        }?>
                                                    </td>
                                                            <td>
                                                                <div id="zipcode3">
                                                                </div>
                                                            </td>
                                                    <td colspan="2"><?php if(isset($suspfadai)) {
                                                            echo form_input('s_fadai_address',$suspfadai->s_fadai_address, 'class="form-control" id="s_fadai_address" placeholder="e.g.XX路XX門牌"');
                                                        }
                                                             else {
                                                                 echo form_input('s_fadai_address','', 'class="form-control" id="s_fadai_address" placeholder="e.g.XX路XX門牌"');
                                                        }?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="tab-pane fade" id="report">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>物品</td>
                                                    <td>數量</td>
                                                    <td>單位</td>
                                                    <td>級數、成分</td>
                                                    <td>樣態1</td>
                                                    <td>樣態2</td>
                                                    <td>描述文字</td>
                                                    <td></td>
                                                    <td>尿液編號</td>
                                                    <td>級數、成分</td>
                                                    <td>是否要罰</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php echo form_dropdown('e_name',$opt1,$drug->e_name,'class="form-control" id=sel') ?>
                                                    </td>
                                                    <td><?php echo form_input('e_count',$drug->e_count, 'class="form-control"')?></td>
                                                    <td><?php echo form_dropdown('e_type',$opt2,$drug->e_type,'class="form-control" id="sel3"') ?></td>
                                                    <td><input type="text" id="drug" name="drug" class="form-control" value="<?php echo $drug_ingredient?>"/></td>
                                                    <td><select class="form-control" id="sel1" name="sel1"> 
                                                          <option value="沾">沾</option>
                                                          <option value="含">含</option>
                                                          <option value=" ">沒有</option>
                                                        </select></td>
                                                    <td><select id="sel2" name="sel2" class="form-control"> 
                                                        <option value="(採集自車內)">採集自車內</option>
                                                        <option value="(採集自隨身包包內)">採集自隨身包包內</option>
                                                    </select></td>
                                                    <td colspan="2">
                                                        <textarea name="fd_drug_message" id="messageSpan" class="form-control"><?php if(isset($sp->fd_drug_msg))echo $sp->fd_drug_msg;
                                                            else echo '';
                                                        ?></textarea>
                                                        <button id='sub' type="button" class="btn btn-default">重新產生</button>
                                                    </td>
                                                    <td><?php echo form_input('s_utest_num',$susp->s_utest_num, 'class="form-control"')?></td>
                                                    <td><?php echo form_input('sc_ingredient',$sc_ingredient, 'class="form-control"')?></td>
                                                    <td colspan="2">
                                                        <input type="radio" name="fd_wantdis" id="optionsRadios1" value="是" checked>是
                                                        <input type="radio" name="fd_wantdis" id="optionsRadios2" value="否">否
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="tab-pane fade" id="fin">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>罰鍰</td>
                                                    <td>繳款日</td>
                                                    <td>第幾次</td>
                                                    <td >刑事&不罰情形</td>
                                                    <td>尿液編號</td>
                                                    <td colspan="2">級數、成分</td>
                                                </tr>
                                                <tr>
                                                    <td><?php
                                                        if(isset($fine)) {
                                                            echo form_input('f_damount',$fine->f_damount, 'class="form-control" id="pmoney" readonly required="required"');
                                                        }
                                                        else {
                                                            echo form_input('f_damount','', 'class="form-control" id="pmoney" readonly required="required"');
                                                        }
                                                    ?></td>
                                                    <td><input name="f_date" type="date" class="form-control" value="<?php if(isset($fine))echo $fine->f_date;
                                                        else echo '';
                                                    ?>"></td>
                                                    <td><select id="seldate" class="form-control">
                                                        <option selected>請選擇</option>
                                                        <option value="0">第一次</option>
                                                        <option value="1">第二次</option>
                                                        <option value="2">第三次</option>
                                                        <option value="2">第四次</option>
                                                        <option value="2">第五次</option>
                                                        <option value="2">第六次</option>
                                                        <option value="2">第七次</option>
                                                        <option value="2">第七次</option>
                                                    </select></td>
                                                    <td><select id="seltype" class="form-control">
                                                        <option selected>請選擇</option>
                                                        <option value="0">type1</option>
                                                        <option value="1">type2</option>
                                                        <option value="2">type3</option>
                                                    </select></td>
                                                    <td><?php echo form_input('s_utest_num',$susp->s_utest_num, 'class="form-control"')?></td>
                                                    <td><?php echo form_input('sc_ingredient',$sc_ingredient, 'class="form-control"')?></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td><td></td><td></td>
                                                    <td><select id="seltype2" class="form-control"></select>
                                                    <button type="button" class="sub2 btn btn-default">產生列句</button></td>
                                                    <td colspan="5">
                                                        <textarea name="fd_dis_msg" id="messageSpan2" class="form-control"><?php if(isset($sp))echo $sp->fd_dis_msg;
                                                        else echo '';
                                                    ?></textarea>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="tab-pane fade" id="fawen">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>發文對象</td>
                                                    <td>發文地址</td>
                                                    <td>郵遞區號</td>
                                                    <td>警政署系統編號</td>
                                                    <td>承辦人姓名</td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo form_input('fd_target',$susp->s_name, 'class="form-control"')?></td>
                                                    <td><?php echo form_input('fd_address',$susp->s_dpcounty.$susp->s_dpdistrict.$susp->s_dpaddress, 'class="form-control" id="address"')?></td>
                                                    <td><?php echo form_input('fd_zipcode',$susp->s_dpzipcode, 'class="form-control" id="zipcodefin"')?></td>
                                                    <td><input name="fd_psystem" type="text" class="form-control"></td>
                                                    <td><?php echo form_input('fd_empno',$empno, 'class="form-control"')?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="tab-pane fade" id="jiang">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>講習時數</td>
                                                    <td>講習時間</td>
                                                    <td>講習地點</td>
                                                </tr>
                                                <tr>
                                                    <td><?php
                                                        if(isset($sp)) {
                                                            echo form_input('fd_lec_time',$sp->fd_lec_time, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_input('fd_lec_time','09:30-16:30', 'class="form-control"');
                                                        }
                                                    ?></td>
                                                    <td><?php
                                                        $options1 = array(
                                                            '2020-04-10'=> '04/10',
                                                            '2020-05-15'=> '05/15',
                                                            '2020-06-15'=> '06/15',
                                                            '2020-11-27'=> '11/27',
                                                            '2020-12-02'=> '12/2',
                                                            '2020-12-04'=> '12/4',
                                                            '2020-12-11'=> '12/11',
                                                            '2020-12-18'=> '12/18',
                                                        );
                                                        if(isset($sp)) {
                                                            echo form_dropdown('fd_lec_date',$options1,$sp->fd_lec_date, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_dropdown('fd_lec_date',$options,'', 'class="form-control"');
                                                        }
                                                    ?></td>
                                                    <td><?php
                                                        if(isset($sp)) {
                                                            echo form_input('fd_lec_address',$sp->fd_lec_address, 'class="form-control"');
                                                        }
                                                             else {
                                                                 echo form_input('fd_lec_address','臺北市中正區青島東路8號', 'class="form-control"');
                                                        }
                                                    ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                            <a class="media" id="pdfviewod" href=<?php echo base_url('/result/S36C-120093013580-1-18.pdf')?>></a>
                            <a class="media" id="pdfviewdrug" href=<?php echo base_url('/drugdoc/'.$drug->e_doc)?>></a>    
                            <a class="media" id="pdfviewsusp" href=<?php echo base_url('/utest/'.$susp->s_utest_doc)?>></a>    
                            <a class="media" id="pdfviewcase" href=<?php echo base_url('/uploads/'.$cases->doc_file)?>></a>    
                            <a class="media" id="pdfviewjud" href=<?php echo base_url('/jiansuo/'.$susp->s_prison_doc)?>></a>    
                            <a class="media" id="pdfviewresult" href=<?php echo base_url('/jiansuo/'.$susp->s_prison_doc)?>></a>    
                        <!-- /.col-lg-12 -->
                        </form>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <script>
            $("#next").click(function (){
                $("#nextvalue").val('<?php echo $next ?>');
                //alert($("#nextvalue").val());
                $("#updateSanc").submit();
            });
            $("#prev").click(function (){
                $("#prevvalue").val('<?php echo $prev ?>');
                //alert($("#prevvalue").val());
                $("#updateSanc").submit();
            });
            $("#drugdoc").click(function (){
                $('#pdfviewdrug').show();  
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewresult').hide();     
                $('#pdfviewjud').hide();     
            });
            $("#suspdoc").click(function (){
                $('#pdfviewsusp').show();  
                $('#test').hide();  
                $('#pdfviewdrug').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewresult').hide();     
                $('#pdfviewjud').hide();     
            });
            $("#odoc").click(function (){
                $('#pdfviewod').show();  
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewdrug').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewresult').hide();     
                $('#pdfviewjud').hide();     
            });
            $("#casedoc").click(function (){
                $('#pdfviewcase').show();  
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewdrug').hide();  
                $('#pdfviewod').hide();  
                $('#pdfviewresult').hide();     
                $('#pdfviewjud').hide();     
            });
            $("#result").click(function (){
                $('#pdfviewresult').show();  
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewdrug').hide();     
                $('#pdfviewod').hide();     
                $('#pdfviewjud').hide();     
            });
            $("#judoc").click(function (){
                $('#pdfviewjud').show();  
                $('#test').hide();  
                $('#pdfviewsusp').hide();  
                $('#pdfviewdrug').hide();  
                $('#pdfviewcase').hide();  
                $('#pdfviewod').hide();     
                $('#pdfviewresult').hide();     
            });

            /*$("#next").click(function (){
                var allrows= table.column(0).data();
                $('#s_cnum1').val(allrows[0]);     
                //alert($('#s_cnum1').val());
                window.location.href = "//localhost/main2/disciplinary_c/editSanc/"+$('#s_cnum1').val();
                //$("#sp_checkbox").submit();
            });*/
            $('#sub').click(function (){
                
                var arr =[];
                var level =[];
                var drugn =[];
                var all =[];
                var fin = '';
                var drug =$('#drug').val().split(';');
                for(let i=0;i<drug.length-1;i++){
                    level[i]=drug[i].substr(0,1);
                    drugn[i]=drug[i].substr(2);
                    all[i]="第"+level[i]+"級毒品「"+drugn[i]+"」";
                    fin = all[i];
                    if(drug.length > 2) fin= fin + "及" + fin;
                    else fin = all[i];
                    console.log(drug.length);
                }
                $sel=$('#sel').val();
                $sel1=$('#sel1').val();
                $sel2=$('#sel2').val();
                $sel3=$('#sel3').val();
                $("input[name='e_count']").each(function(){
                    arr.push($(this).val());
                })
                $("#messageSpan").val($sel1+fin+$sel+arr[0]+$sel3+$sel2);
                //console.log(arr,$sel,$sel2);
                
            });
            $('.sub2').click(function (){
                $seltype2=$('#seltype2').val();
                $("#messageSpan2").val($seltype2);
                //console.log($seltype2);
                
            });
            $("#seldate").change(function(){
            switch (parseInt($(this).val())){
                case 0: 
                $("#pmoney value").remove();
                $("#pmoney").val("20000");
                break;
                case 1: 
                $("#pmoney value").remove();
                $("#pmoney").val("30000");
                break;
                case 2: 
                $("#pmoney value").remove();
                $("#pmoney").val("50000");
                break;
            }});
            $("#seltype").change(function(){
            switch (parseInt($(this).val())){
                case 0: 
                $("#seltype2 option").remove();
                $("#seltype2").append($("<option value='原本局108年4月10日北市警刑毒緝第1-83--45411號處分書作廢(列管編號:1081106)"
                                    + "'>上年度處分書至本年度重發</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>單純不起訴處分</option>"
                                    ));
                break;
                case 1: 
                $("#seltype2 option").remove();
                $("#seltype2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署處分緩起訴2年，且應至臺北市立聯合醫院松德院區於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>臺北地檢緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分。經臺灣士林地方檢察署處分緩起訴2年，且應至臺灣士林地方檢察署指定醫院於1年之治療期程內完成戒癮治療，併此敘明。"
                                    + "'>士林緩起訴戒癮治療</option>"
                                    +"<option value='受處分人另涉及第二級毒品部分，業已接受觀察、勒戒，經臺灣臺北地方檢察署不起訴處分，倂此敘明"
                                    + "'>已經勒戒</option>"
                                    ));
                break;
                case 2: 
                $("#seltype2 option").remove();
                $("#seltype2").append($("<option value='受處分人另涉及第二級毒品部分。業經臺灣臺北地方檢察署刑事簡易判決處有期徒刑貳月，併此敘明。"
                                    + "'>刑事簡易判決</option>"
                                    +"<option value='受處分人另涉及第一級毒品部分。業經臺灣臺北地方檢察署刑事判決處有期徒刑七月。"
                                    + "'>刑事判決</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，由親屬主動向員警報案發現犯行，故不予處罰。"
                                    + "'>親屬不罰</option>"
                                    +"<option value='因本案受處分人與犯罪遭發覺前，主動向員警報案坦承犯行，故不予處罰。"
                                    + "'>自首</option>"
                                    ));
                break;
            }
            });
            $("#zipcode2").twzipcode({
                "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_dpcounty',   // 預設值為 county
                'districtName' : 's_dpdistrict', // 預設值為 district
                'zipcodeName'  : 's_dpzipcode',// 預設值為 zipcode
                <?php if(isset($susp->s_dpzipcode)) echo "'zipcodeSel'   : ".$susp->s_dpzipcode; ?>
            });         
            
            $("#zipcode").twzipcode({
                "zipcodeIntoDistrict": true,
                "css": ["city form-control", "district form-control"],
                'countyName'   : 's_rpcounty',   // 預設值為 county
                'districtName' : 's_rpdistrict', // 預設值為 district
                'zipcodeName'  : 's_rpzipcode', // 預設值為 zipcode
                <?php if(isset($susp->s_rpzipcode)) echo "'zipcodeSel'   : ".$susp->s_rpzipcode; ?>
            }); 
            $("#zipcode3").twzipcode({
                "css": ["city form-control", "district form-control"],
                "zipcodeIntoDistrict": true,
                'countyName'   : 's_fadai_county',   // 預設值為 county
                'districtName' : 's_fadai_district', // 預設值為 district
                'zipcodeName'  : 's_fadai_zipcode', // 預設值為 zipcode
                'zipcodeSel'   : <?php if(isset($suspfadai->s_fadai_zipcode)){echo $suspfadai->s_fadai_zipcode;}else{echo '100';}  ?>
            });  
            
            $(function() {  
               $('a#pdfviewdrug').media({width:1400, height:800});  
               $('a#pdfviewod').media({width:1400, height:800});  
               $('a#pdfviewsusp').media({width:1400, height:800});  
               $('a#pdfviewcase').media({width:1400, height:800});  
               $('a#pdfviewresult').media({width:1400, height:800});  
               $('a#pdfviewjud').media({width:1400, height:800});  
               $('#pdfviewdrug').hide();  
               $('#pdfviewod').hide();  
               $('#pdfviewsusp').hide();  
               $('#pdfviewcase').hide();  
               $('#pdfviewresult').hide();  
               $('#pdfviewjud').hide();  
            });  
            
            $(document).ready(function (){
                $('#updateSanc').on('submit', function(e){
                    var age = 100;
                    var today = new Date();
                    var birthdate = new Date($('#s_birth').val());
                    age = today.getFullYear() - birthdate.getFullYear();
                    var m = today.getMonth() - birthdate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
                        age--;
                    }
                    //alert(age);
                    //e.preventDefault();
                    if( age < 20 ){
                        //alert('1');
                        if($('#s_fadai_ic').val()!="" && $('#s_fadai_name').val()!=""&& $('#s_fadai_address').val()!=""&& $('#s_fadai_phone').val()!=""){
                            //$("#updateSanc").submit();
                            //alert('2');
                            //e.preventDefault();
                        }else{
                            alert('受處分人未滿20歲，請輸入法代人資料');
                                e.preventDefault();
                            }
                    }
                    else{
                        //alert(age);
                            //$("#updateSanc").submit();
                        //e.preventDefault();
                    }
                });
            });
        </script>
    </body>
</html>
