        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar bg-primary navbar-fixed-top" role="navigation">

                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><?php 
                        $image_properties = array(
                            'src' => 'img/logo.gif',
                            'alt' => 'Me，demonstrating how to eat 4 slices of pizza at one time',
                            'class' => 'post_images',
                            'width' => '50',
                            'height' => '50',
                            'href' => 'listCases',
                        );     
                        echo img($image_properties);
                    ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-center navbar-top-links bg-primary">
                    <li><h3><?php echo $title ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";?></h3></li>
                </ul>
            <?php $this->load->view($nav);?>
            <div id="page-wrapper">
                <div class="container-fluid"> 
                    <div class="row"><br>
                        <div class="col-lg-11">
                            <div class="panel panel-default">
                            <?php echo form_open_multipart('disciplinary_c/updatedpproject','id="sp_checkbox"') ?>
                                <div class="panel-heading list_view">
                                    <input type="checkbox" name="list"  data-target ="1"  checked> 案件編號
                                    <input type="checkbox" name="list" data-target ="2" checked> 發文字號
                                    <input type="checkbox" name="list" data-target ="3" checked> 受處分人
                                    <input type="checkbox" name="list" data-target ="4" checked> 身份證號
                                    <input type="checkbox" name="list" data-target ="5" checked> 移送分局
                                    <input type="checkbox" name="list" data-target ="6" checked> 依據單位(字)
                                    <input type="checkbox" name="list" data-target ="7" checked> 戶籍
                                    <input type="checkbox" name="list" data-target ="8" checked> 現住地址
                                    <input type="checkbox" name="list" data-target ="9" checked> 出生日期
                                    <input type="checkbox" name="list" data-target ="10" checked> 查獲時間
                                    <input type="checkbox" name="list" data-target ="11" checked> 查獲地點
                                    <input type="checkbox" name="list" data-target ="12" checked> 查獲單位
                                    <input type="checkbox" name="list" data-target ="13" checked> 檢驗成分
                                    <input type="checkbox" name="list" data-target ="14" checked> 尿液編號
                                    <input type="checkbox" name="list" data-target ="15" checked> 持有毒品
                                    <input type="checkbox" name="list" data-target ="16" checked> 毒報
                                    <input type="checkbox" name="list" data-target ="17" checked> 尿報
                                    <input type="checkbox" name="list" data-target ="18" checked> 編輯
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <?php echo $s_table;?>
                                    </div>                       
                                </div>
                    <input id="s_cnum" type="hidden" name="s_cnum" value=''> 
                    <input id="s_status" type="hidden" name="s_status" value=''> 
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">加入舊專案</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          <div class="form-group">
                                            <label>舊專案</label>
                                            <?php echo form_dropdown('dp_num',$opt ,'', 'class="form-control" id="sel"')?>                                                            
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button id='no' class="btn btn-default" >加入舊專案</button>
                                      </div>
                                    </div>
                                </div>
                            </div>         
                            </form>   
               </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <a class="media" href="test.pdf"></a>    
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>                    
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript"> 
        $(document).ready(function (){
           var table = $('#table1').DataTable({
              'columnDefs': [
                 {
                    'targets': 0,
                    'checkboxes': {
                       'selectRow': true
                    }
                 }
              ],
              'select': {
                 'style': 'multi'
              },
              "width":"100px",'order': [[1, 'asc']]
           });
                  // Add event listener for opening and closing details
                  $('#example tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                    } else {
                      // Open this row
                      row.child(format(row.data())).show();
                      tr.addClass('shown');
                    }
                  });

                  $('a.toggle-vis').on('click', function(e) {
                    e.preventDefault();

                    // Get the column API object
                    var column = table.column($(this).attr('data-column'));

                    // Toggle the visibility
                    column.visible(!column.visible());
                  });
                  $('.list_view input[type="checkbox"]').on('change', function(e) {


                    // Get the column API object
                    var col = table.column($(this).attr('data-target'));

                    // Toggle the visibility
                    col.visible(!col.visible());
                  });

            $("#yes").click(function (){
                $("#s_status").val('1');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
            $("#no").click(function (){
                $("#s_status").val('0');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });
            $("#yes1").click(function (){
                $("#s_status").val('2');
                    //alert("Submitted");
                    $("#sp_checkbox").submit();
            });

           // Handle form submission event
           $('#sp_checkbox').on('submit', function(e){
              var form = this;

              var rows_selected = table.column(0).checkboxes.selected();

              // Iterate over all selected checkboxes
              /*$.each(rows_selected, function(index, rowId){
                 // Create a hidden element
                 $(form).append(
                     $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'id[]')
                        .val(rowId)
                 );
              });*/
                $('#example-console-rows').text(rows_selected.join(","));
                $('#s_cnum').val(rows_selected.join(","));
              
                // Output form data to a console     
                //$('#example-console-form').text($(form).serialize());
               
                // Remove added elements
                $('input[name="id\[\]"]', form).remove();
               
                // Prevent actual form submission
                //e.preventDefault();
           });
        });
</script>
